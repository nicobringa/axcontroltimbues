﻿Imports Negocio
Imports Entidades.Constante

Public Class CtrlDinamismoPorteriaIngreso



#Region "METODOS"
    Public Sub SetBarreraEstado(ByVal _BarreraEstado As BarreraEstado)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing


        If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then

            picVisible = picBarreraArriba_Ingreso
            picNoVisible = picBarreraAbajo_Ingreso
            modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, False)
        ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo

            picVisible = picBarreraAbajo_Ingreso
            picNoVisible = picBarreraArriba_Ingreso
            modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, False)
        ElseIf _BarreraEstado = BarreraEstado.Falla Then

            picNoVisible = Nothing
            picVisible = Nothing
            modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, True)

        End If

        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If

    End Sub

    Public Sub SetSensorEstado(ByVal _SensorEstado As Integer)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing

        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picSensor_IngresoNoCortando
            picNoVisible = picSensor_IngresoCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picSensor_IngresoCortando
            picNoVisible = picSensor_IngresoNoCortando
        End If




        'If _SensorEstado = SensorEstado.Deshabilitado Then



        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        Else
            'Oculto las dos barreras
            picVisible = picSensor_IngresoCortando
            picNoVisible = picSensor_IngresoNoCortando

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub

    Public Sub SetVisibleBarrera(visible As Boolean)
        modDelegado.setVisibleCtrl(Me, picBarreraAbajo_Ingreso, visible)
        modDelegado.setVisibleCtrl(Me, picBarreraArriba_Ingreso, visible)
        modDelegado.setVisibleCtrl(Me, picSensor_IngresoCortando, visible)
        modDelegado.setVisibleCtrl(Me, picSensor_IngresoNoCortando, visible)
        modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, visible)
    End Sub

#End Region





End Class
