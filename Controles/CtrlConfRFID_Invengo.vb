﻿Imports System
Imports System.Windows.Forms
Imports IRP1 = Invengo.NetAPI.Protocol.IRP1

Public Class CtrlConfRFID_Invengo

    Private _ConfRFIDInvengo As Entidades.Config_lector_rfid_invengo
    Private nConfig As Negocio.Config_lector_rfid_invengoN
    Private WithEvents _LectorRFID_Invengo As Negocio.LectorRFID_Invengo

    Public Sub New(ByVal oConf As Entidades.Config_lector_rfid_invengo, ByVal lectorRFID As Negocio.LectorRFID_Invengo, ByVal configInvengo As Negocio.Config_lector_rfid_invengoN)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        Me._ConfRFIDInvengo = oConf
        Me._LectorRFID_Invengo = lectorRFID
        Me.nConfig = configInvengo
        CargarConfig()

    End Sub



#Region "Eventos"




#End Region


#Region "Métodos"

    '' IRP1.ReadTag.ReadMemoryBank rmb = (IRP1.ReadTag.ReadMemoryBank)Enum.Parse(TypeOf(IRP1.ReadTag.ReadMemoryBank), cbReadMB.Items[cbReadMB.SelectedIndex].ToString());

    ''Private rmb As IRP1.ReadTag.ReadMemoryBank = CType([Enum].Parse(TypeOf (IRP1.ReadTag.ReadMemoryBank.), cbReadMB.Items(cbReadMB.SelectedIndex).ToString()), IRP1.ReadTag.ReadMemoryBank)



    Dim rmb As IRP1.ReadTag.ReadMemoryBank '= IRP1.ReadTag.ReadMemoryBank.EPC_6C


    Public Sub CargarConfig()

        ''If (_LectorRFID_Invengo.PowerValue = Nothing) Then

        _LectorRFID_Invengo.DetectarConfig()

        If Not IsNothing(_LectorRFID_Invengo.ValoresPW) Then
                Dim i As Integer = 0
                Do While (i < _LectorRFID_Invengo.ValoresPW.Count)
                    cboPowerDbm.Items.Add(_LectorRFID_Invengo.ValoresPW(i))
                    If (_LectorRFID_Invengo.ValoresPW(i) = _LectorRFID_Invengo.PowerValue.ToString()) Then
                        cboPowerDbm.SelectedIndex = i
                    End If
                    i = i + 1
                Loop
            End If
        ''Else

        ''End If

        txtNombre.Text = _ConfRFIDInvengo.nombre
        cboReaderMode.SelectedIndex = _ConfRFIDInvengo.modoLectura
        numUpDwTiempoLectura.Value = _ConfRFIDInvengo.tiempoLectura

    End Sub

    Private Sub btnAgrConfiguracion_Click(sender As Object, e As EventArgs) Handles btnAgrConfiguracion.Click

        'Está deshabilitado el aplicar config del poder antena y modo lectura hasta encontrar la forma de configurarlo.
        GuardarEnBD()

    End Sub


    'Guarda los cambios en BD Y aplica los cambios en el lector
    Private Sub GuardarEnBD()

        If Not IsNothing(_LectorRFID_Invengo) Then
            Try
                _ConfRFIDInvengo.nombre = txtNombre.Text
                _ConfRFIDInvengo.modoLectura = cboReaderMode.SelectedIndex
                _ConfRFIDInvengo.potenciaAntena = cboPowerDbm.Text
                _ConfRFIDInvengo.tiempoLectura = numUpDwTiempoLectura.Value
                nConfig.Update(_ConfRFIDInvengo)
                _LectorRFID_Invengo.AplicarConfig(cboPowerDbm.Text, numUpDwTiempoLectura.Value)
                numUpDwTiempoLectura.Enabled = False
                MsgBox("Configuración aplicada")
                btnAgrConfiguracion.Enabled = False
            Catch ex As Exception
                MsgBox("No se pudo aplicar la configuración")
            End Try
        End If

    End Sub

#End Region




End Class
