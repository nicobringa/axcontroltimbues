﻿Imports System.Delegate
Imports System.Windows.Forms
Imports System.Drawing

Public Module modDelegado


    Delegate Sub DelSetTextLabel(ByRef ctrl As System.Windows.Forms.Control, ByVal texto As String, ByVal lbl As Label, ByVal color As Color)
    Public Sub SetTextLabel(ByRef ctrl As System.Windows.Forms.Control, ByVal texto As String, ByVal lbl As Label, ByVal color As Color)
        If lbl.InvokeRequired Then
            Dim Delegado As New DelSetTextLabel(AddressOf SetTextLabel)
            ctrl.Invoke(Delegado, New Object() {ctrl, texto, lbl, color})
        Else
            lbl.Text = texto
            lbl.ForeColor = color
        End If
    End Sub

    Delegate Sub DelSetlVisiblePic(ByRef ctrl As System.Windows.Forms.Control, ByVal estado As Boolean, ByVal picture As PictureBox)
    Public Sub setVisiblePic(ByRef ctrl As System.Windows.Forms.Control, ByVal estado As Boolean, ByVal picture As PictureBox)
        If picture.InvokeRequired Then
            Dim Delegado As New DelSetlVisiblePic(AddressOf setVisiblePic)
            ctrl.Invoke(Delegado, New Object() {ctrl, estado, picture})
        Else
            picture.Visible = estado
        End If
    End Sub

    Delegate Sub DelSetlVisibleCtrl(ByRef ctrlContenedor As System.Windows.Forms.Control, ByRef ctrlVisible As System.Windows.Forms.Control, ByVal visible As Boolean)
    ''' <summary>
    ''' Permite mostrar o ocultar un control
    ''' </summary>
    ''' <param name="ctrlContenedor">Control que contiene al control que se quiere ocultar o mostrar</param>
    ''' <param name="ctrlVisible">Control que se va ocultar o mostrar</param>
    ''' <param name="visible">TRUE si se va a mostrar el control FALSE si se va ocultar</param>
    ''' <remarks></remarks>
    Public Sub setVisibleCtrl(ByRef ctrlContenedor As System.Windows.Forms.Control, ByRef ctrlVisible As System.Windows.Forms.Control, ByVal visible As Boolean)
        If ctrlVisible.InvokeRequired Then
            Dim Delegado As New DelSetlVisibleCtrl(AddressOf setVisibleCtrl)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlVisible, visible})
        Else
            ctrlVisible.Visible = visible
        End If
    End Sub

    Delegate Sub DelAddCtrlFLP(ByVal ctrlContenedor As Control, ByVal ctrlAgregar As Control, ByVal FLP As FlowLayoutPanel)
    Public Sub AddCtrlFLP(ByVal ctrlContenedor As Control, ByVal ctrlAgregar As Control, ByVal FLP As FlowLayoutPanel)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelAddCtrlFLP(AddressOf AddCtrlFLP)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlAgregar, FLP})
        Else
            FLP.Controls.Add(ctrlAgregar)
        End If
    End Sub

    Delegate Sub DelRemoveAtCtrlFLP(ByVal ctrlContenedor As Control, ByVal Index As Integer, ByVal FLP As FlowLayoutPanel)
    Public Sub RemoveAtCtrlFLP(ByVal ctrlContenedor As Control, ByVal Index As Integer, ByVal FLP As FlowLayoutPanel)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelRemoveAtCtrlFLP(AddressOf RemoveAtCtrlFLP)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, Index, FLP})
        Else
            FLP.Controls.RemoveAt(Index)
        End If
    End Sub

    Delegate Sub DelRemoveCtrlFLP(ByVal ctrlContenedor As Control, ByVal ctrlRemove As Control, ByVal FLP As FlowLayoutPanel)
    Public Sub RemoveCtrlFLP(ByVal ctrlContenedor As Control, ByVal ctrlRemove As Control, ByVal FLP As FlowLayoutPanel)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelRemoveCtrlFLP(AddressOf RemoveCtrlFLP)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlRemove, FLP})
        Else
            FLP.Controls.Remove(ctrlRemove)
        End If
    End Sub

    Delegate Sub DelClearCtrlFLP(ByVal ctrlContenedor As Control, ByVal FLP As FlowLayoutPanel)
    Public Sub ClearCtrlFLP(ByVal ctrlContenedor As Control, ByVal FLP As FlowLayoutPanel)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelClearCtrlFLP(AddressOf ClearCtrlFLP)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, FLP})
        Else
            FLP.Controls.Clear()
        End If
    End Sub

    Delegate Sub DelSetBackGraoundButton(ByVal ctrlContenedor As Control, ByVal btn As Button, ByVal _Color As Color)
    Public Sub SetBackGroundButton(ByVal ctrlContenedor As Control, ByVal btn As Button, ByVal _Color As Color)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelSetBackGraoundButton(AddressOf SetBackGroundButton)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, btn, _Color})

        Else
            btn.BackColor = _Color

        End If

    End Sub

    Delegate Sub DelSetBackColorCtrl(ByVal ctrlContenedor As Control, ByVal ctrlBackColor As Control, ByVal _Color As Color)
    Public Sub SetBackColorCtrl(ByVal ctrlContenedor As Control, ByVal ctrlBackColor As Control, ByVal _Color As Color)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelSetBackColorCtrl(AddressOf SetBackColorCtrl)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlBackColor, _Color})

        Else
            ctrlBackColor.BackColor = _Color

        End If

    End Sub

    Delegate Sub DelSetWithCtrl(ByVal ctrlContenedor As Control, ByVal ctrlWith As Control, ByVal _With As Integer)
    Public Sub SetWithCtrl(ByVal ctrlContenedor As Control, ByVal ctrlWith As Control, ByVal _With As Integer)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelSetWithCtrl(AddressOf SetWithCtrl)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlWith, _With})

        Else
            ctrlWith.Width = _With

        End If

    End Sub

    Delegate Sub DelSetRefreshCtrl(ByVal ctrlContenedor As Control, ByVal ctrlRefresh As Control)
    Public Sub SetRefreshCtrl(ByVal ctrlContenedor As Control, ByVal ctrlRefresh As Control)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelSetRefreshCtrl(AddressOf SetRefreshCtrl)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlRefresh})

        Else
            ctrlRefresh.Refresh()

        End If

    End Sub

    Delegate Sub DelSetEnable(ByVal ctrlContenedor As Control, ByVal ctrlEnable As Control, ByVal Enabled As Boolean)
    Public Sub SetEnable(ByVal ctrlContenedor As Control, ByVal ctrlEnable As Control, ByVal Enabled As Boolean)
        If ctrlContenedor.InvokeRequired Then
            Dim Delegado As New DelSetEnable(AddressOf SetEnable)
            ctrlContenedor.Invoke(Delegado, New Object() {ctrlContenedor, ctrlEnable, Enabled})

        Else
            ctrlEnable.Enabled = Enabled

        End If

    End Sub


    Delegate Sub SetButtonColorCallback(ByRef ctrl As System.Windows.Forms.Control, ByVal [btn] As System.Windows.Forms.Button, ByVal [color] As Color)

    Public Sub SetButtonColor(ByRef ctrl As System.Windows.Forms.Control, ByVal [btn] As System.Windows.Forms.Button, ByVal [color] As Color)

        If [btn].InvokeRequired Then
            Dim d As New SetButtonColorCallback(AddressOf SetButtonColor)
            ctrl.Invoke(d, New Object() {ctrl, [btn], [color]})
        Else
            [btn].BackColor = [color]
        End If
    End Sub


    Delegate Sub SetPictureVisibleCallback(ByRef ctrl As System.Windows.Forms.Control, ByVal [picture] As System.Windows.Forms.PictureBox, ByVal [visible] As Boolean)

    Public Sub SetPictureVisible(ByRef ctrl As System.Windows.Forms.Control, ByVal [picture] As System.Windows.Forms.PictureBox, ByVal [visible] As Boolean)

        If [picture].InvokeRequired Then
            Dim d As New SetPictureVisibleCallback(AddressOf SetPictureVisible)
            ctrl.Invoke(d, New Object() {ctrl, [picture], [visible]})
        Else
            [picture].Visible = [visible]
        End If
    End Sub

    Delegate Sub SetTextCallback(ByRef ctrl As System.Windows.Forms.Control, ByVal [textbox] As System.Windows.Forms.TextBox, ByVal [text] As String)

    Public Sub SetText(ByRef ctrl As System.Windows.Forms.Control, ByVal [textbox] As System.Windows.Forms.TextBox, ByVal [text] As String)

        If [textbox].InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetText)
            ctrl.Invoke(d, New Object() {ctrl, [textbox], [text]})
        Else
            [textbox].Text = [text]
        End If
    End Sub


    Delegate Sub SetLedColorCallback(ByRef ctrl As System.Windows.Forms.Control, ByVal [led] As System.Windows.Forms.Label, ByVal [color] As Color)

    Public Sub SetLedColor(ByRef ctrl As System.Windows.Forms.Control, ByVal [led] As System.Windows.Forms.Label, ByVal [color] As Color)

        If [led].InvokeRequired Then
            Dim d As New SetLedColorCallback(AddressOf SetLedColor)
            ctrl.Invoke(d, New Object() {ctrl, [led], [color]})
        Else
            [led].BackColor = [color]
        End If
    End Sub

    Delegate Sub SetLabelWidthCallback(ByRef ctrl As System.Windows.Forms.Control, ByVal [label] As System.Windows.Forms.Label, ByVal [ancho] As Integer)

    Public Sub SetLabelWidth(ByRef ctrl As System.Windows.Forms.Control, ByVal [label] As System.Windows.Forms.Label, ByVal [ancho] As Integer)

        If [label].InvokeRequired Then
            Dim d As New SetLabelWidthCallback(AddressOf SetLabelWidth)
            ctrl.Invoke(d, New Object() {ctrl, [label], [ancho]})
        Else
            [label].Width = [ancho]
        End If
    End Sub


    Delegate Sub SetTextLbCallback(ByRef ctrl As System.Windows.Forms.Control, ByVal [label] As System.Windows.Forms.Label, ByVal [text] As String)

    Public Sub SetTextLabel(ByRef ctrl As System.Windows.Forms.Control, ByVal [label] As System.Windows.Forms.Label, ByVal [text] As String)

        If [label].InvokeRequired Then
            Dim d As New SetTextLbCallback(AddressOf SetTextLabel)
            ctrl.Invoke(d, New Object() {ctrl, [label], [text]})
        Else
            [label].Text = [text]

        End If
    End Sub

    Delegate Sub SetImagePictureCallBack(ByRef ctrl As System.Windows.Forms.Control, ByVal ctrlPicture As System.Windows.Forms.PictureBox, ByVal Image As System.Drawing.Image)
    Public Sub SetImagePicture(ByRef ctrl As System.Windows.Forms.Control, ByVal ctrlPicture As System.Windows.Forms.PictureBox, ByVal Image As System.Drawing.Image)
        If ctrl.InvokeRequired Then
            Dim d As New SetImagePictureCallBack(AddressOf SetImagePicture)
            ctrl.Invoke(d, New Object() {ctrl, ctrlPicture, Image})
        Else
            ctrlPicture.Image = Image
        End If
    End Sub


    Delegate Sub SetClearRows(ByRef ctrl As System.Windows.Forms.Control, ByVal grilla As System.Windows.Forms.DataGridView)
    Public Sub ClearRows(ByRef ctrl As System.Windows.Forms.Control, ByVal grilla As System.Windows.Forms.DataGridView)
        If ctrl.InvokeRequired Then
            Dim d As New SetClearRows(AddressOf ClearRows)
            ctrl.Invoke(d, New Object() {ctrl, grilla})
        Else
            grilla.Rows.Clear()
        End If
    End Sub


    Delegate Sub setAddRows(ByRef ctrl As System.Windows.Forms.Control, ByVal grilla As System.Windows.Forms.DataGridView, ByVal dato1 As String, ByVal dato2 As String, ByVal dato3 As String, ByVal dato4 As String, ByVal dato5 As String, ByVal dato6 As String, ByVal dato7 As String, ByVal dato8 As String, ByVal dato9 As String)
    Public Sub AddRows(ByRef ctrl As System.Windows.Forms.Control, ByVal grilla As System.Windows.Forms.DataGridView, ByVal dato1 As String, ByVal dato2 As String, ByVal dato3 As String, ByVal dato4 As String, ByVal dato5 As String, ByVal dato6 As String, ByVal dato7 As String, ByVal dato8 As String, ByVal dato9 As String)
        If ctrl.InvokeRequired Then
            Dim d As New setAddRows(AddressOf AddRows)
            ctrl.Invoke(d, New Object() {ctrl, grilla, dato1, dato2, dato3, dato4, dato5, dato6, dato7, dato8, dato9})
        Else
            grilla.Rows.Add(dato1, dato2, dato3, dato4, dato5, dato6, dato7, dato8, dato9)
        End If
    End Sub

    Delegate Sub showFormCallBack(ByRef ctrl As System.Windows.Forms.Control, ByRef frm As System.Windows.Forms.Form)
    Public Sub showForm(ByRef ctrl As System.Windows.Forms.Control, ByRef frm As System.Windows.Forms.Form)
        If ctrl.InvokeRequired Then
            Dim d As New showFormCallBack(AddressOf showForm)
            ctrl.Invoke(d, New Object() {ctrl, frm})
        Else
            frm.ShowDialog()
        End If
    End Sub


    Delegate Sub incrementProgress(ByVal frm As System.Windows.Forms.Form, ByVal bar As System.Windows.Forms.ProgressBar)

    Public Sub progress(ByVal frm As System.Windows.Forms.Form, ByVal bar As System.Windows.Forms.ProgressBar)
        If frm.InvokeRequired Then
            Dim d As New incrementProgress(AddressOf progress)
            frm.Invoke(d, New Object() {frm, bar})
        Else
            bar.PerformStep()
        End If
    End Sub

End Module
