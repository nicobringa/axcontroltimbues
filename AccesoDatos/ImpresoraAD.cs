﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;


namespace AccesoDatos
{
    public class ImpresoraAD
    {
        public Impresora update_config(Impresora impresora)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {

                    var oConsulta = (from tabla in oContext.Impresoras
                                     where tabla.idImpresora == impresora.idImpresora
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        
                        oConsulta.ip = impresora.ip;
                        oConsulta.port = impresora.port;


                        oContext.SaveChanges();
                        return impresora;
                    }
                    else
                    {
                        return new Impresora();
                    }

                   
   

                }
            }
            catch (Exception ex)
            {
               
                return null;
            }
        }

       

        public Impresora getOne(string ip)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Impresoras.Where(a => a.ip == ip).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Impresora getOneTag(string tag)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Impresoras.Where(a => a.tag == tag).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Impresora> getAll()
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Impresoras.OrderByDescending(a => a.tag).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
