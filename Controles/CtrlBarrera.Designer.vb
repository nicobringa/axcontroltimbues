﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlBarrera
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlBarrera))
        Me.pbBarreraDeshab = New System.Windows.Forms.PictureBox()
        Me.btnConfig = New System.Windows.Forms.Button()
        Me.lbTAG = New System.Windows.Forms.Label()
        Me.EP = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.pbBarreraDeshab, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbBarreraDeshab
        '
        Me.pbBarreraDeshab.AccessibleDescription = ""
        Me.pbBarreraDeshab.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbBarreraDeshab.BackColor = System.Drawing.Color.Transparent
        Me.pbBarreraDeshab.Image = Global.Controles.My.Resources.Resources.DeshabilitarSensor
        Me.pbBarreraDeshab.Location = New System.Drawing.Point(42, 20)
        Me.pbBarreraDeshab.Name = "pbBarreraDeshab"
        Me.pbBarreraDeshab.Size = New System.Drawing.Size(26, 28)
        Me.pbBarreraDeshab.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbBarreraDeshab.TabIndex = 118
        Me.pbBarreraDeshab.TabStop = False
        '
        'btnConfig
        '
        Me.btnConfig.AccessibleDescription = "Barrera 1 Balanza 1"
        Me.btnConfig.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConfig.BackColor = System.Drawing.Color.Red
        Me.btnConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConfig.Image = Global.Controles.My.Resources.Resources.Configuration19
        Me.btnConfig.Location = New System.Drawing.Point(14, 21)
        Me.btnConfig.Name = "btnConfig"
        Me.btnConfig.Size = New System.Drawing.Size(26, 25)
        Me.btnConfig.TabIndex = 117
        Me.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnConfig.UseVisualStyleBackColor = False
        '
        'lbTAG
        '
        Me.lbTAG.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbTAG.BackColor = System.Drawing.Color.Goldenrod
        Me.lbTAG.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTAG.ForeColor = System.Drawing.Color.White
        Me.EP.SetIconPadding(Me.lbTAG, -50)
        Me.lbTAG.Location = New System.Drawing.Point(0, 0)
        Me.lbTAG.Name = "lbTAG"
        Me.lbTAG.Size = New System.Drawing.Size(73, 20)
        Me.lbTAG.TabIndex = 119
        Me.lbTAG.Text = "TAG"
        Me.lbTAG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'EP
        '
        Me.EP.BlinkRate = 2000
        Me.EP.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.EP.ContainerControl = Me
        Me.EP.Icon = CType(resources.GetObject("EP.Icon"), System.Drawing.Icon)
        '
        'CtrlBarrera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.lbTAG)
        Me.Controls.Add(Me.pbBarreraDeshab)
        Me.Controls.Add(Me.btnConfig)
        Me.Name = "CtrlBarrera"
        Me.Size = New System.Drawing.Size(73, 52)
        CType(Me.pbBarreraDeshab, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pbBarreraDeshab As System.Windows.Forms.PictureBox
    Friend WithEvents btnConfig As System.Windows.Forms.Button
    Friend WithEvents lbTAG As System.Windows.Forms.Label
    Friend WithEvents EP As System.Windows.Forms.ErrorProvider

End Class
