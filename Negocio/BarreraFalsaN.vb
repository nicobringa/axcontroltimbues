﻿Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades
Imports Entidades.Constante

Public Class BarreraFalsaN

    Public Function OperBarreraManual(ByVal idSector As Integer, ByVal operacion As Integer, ByVal usuario As String, ByVal idAuditoriaWS As Integer) As Boolean

        Dim Comando As Constante.BarreraComando = GetComandoOperacion(operacion)
        'Dim NombrePLC As String = Entidades.Constante.getNombrePLC(Constante.PuestoTrabajo.Porteria)

        If EjecComandoBarreras(idSector, Comando, usuario) Then

            Dim msjAuditoria As String = "Barrera Manual Ejecutada Correctamente idSector: " & idSector & " operacion: " & operacion & " Usuario: " & usuario

            Negocio.AuditoriaN.AddAuditoria(msjAuditoria, "", 0, 0, usuario, acciones.WS_OperBarreraManual, idAuditoriaWS)

            Return True
        Else
            Return False
        End If

    End Function

    Public Function controloFalla(ByVal idSector As Integer) As Boolean

        Try
            Dim datoWordIntAD As New AccesoDatos.DatoWordIntAD

            Dim oTag As Entidades.DATO_WORDINT = datoWordIntAD.getOneTags(idSector, ".COMANDO")
            If oTag.valor = Constante.BarreraEstado.Falla Then
                Return False ' si está en falla devuelvo falso
            Else
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function
    ''' <summary>
    ''' Permite obtener el comando apartir de la operacion
    ''' </summary>
    ''' <param name="operacion">operacion que se quiere saber el comando</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetComandoOperacion(ByVal operacion As Integer) As Constante.BarreraComando

        Return If(operacion = Entidades.Constante.OperacionBarrera.SubirBarrera, Constante.BarreraComando.SubirManual, Constante.BarreraComando.BajarManual)
    End Function


    ''' <summary>
    ''' Permite ejecutar un comando de barrera
    ''' </summary>
    ''' <param name="idSector">Sector que se va ejecutar el comando(Ingreso o Egreso)</param>
    ''' <param name="_Comando">Tipo de comnado a ejecutar</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EjecComandoBarreras(ByVal idSector As Integer, ByVal _Comando As Constante.BarreraComando, ByVal Usuario As String) As Boolean
        Dim xRet As Boolean
        'Obtengo el nombre de tag segun la barrera que se quiere ejecutar el comando
        'Dim NombreTAG As String = If(_Sector = Entidades.Constante.IngresoEgreso.Ingreso, Porteria.COMANDO_BARRERA_INGRESO, Porteria.COMANDO_BARRERA_EGRESO)


        Dim datoWordIntAD As New AccesoDatos.DatoWordIntAD
        Dim DatoWordIntN As New Negocio.DatoWordIntN

        Dim oTag As Entidades.DATO_WORDINT = datoWordIntAD.getOneTags(idSector, ".COMANDO")

        If DatoWordIntN.Escribir(oTag.id, _Comando).id = 0 Then
            xRet = False
        Else
            AuditoriaEjecComando(idSector, _Comando, Usuario)
            xRet = True
        End If



        Return xRet
    End Function

    Public Sub AuditoriaEjecComando(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _Comando As Constante.BarreraComando, ByVal Usuario As String)

        Dim textComando As String
        Dim textBarrera As String

        If _Comando = BarreraComando.PermitirIngresar Then
            textComando = "Permitir ingresar"
        ElseIf _Comando = BarreraComando.SubirManual Then
            textComando = "Subir Manual"
        Else ' Cerrar Manual
            textComando = "Cerrar Manual"
        End If

        textBarrera = If(_Barrera = Entidades.Constante.IngresoEgreso.Ingreso, "Ingreso", "Egreso")

        Dim MsjAuditoria As String = "Comando " & _Comando.ToString() & " ejecutado " & "Sector " & textBarrera

        Negocio.AuditoriaN.AddAuditoria(MsjAuditoria, "", 0, 0, Usuario, acciones.Ejec_Comando)


    End Sub
End Class
