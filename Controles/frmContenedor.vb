﻿Public Class frmContenedor
    Sub New(ByRef ctrl As Controles.CtrlAutomationObjectsBase)
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.Width = ctrl.Width
        Me.Height = ctrl.Height + 40
        pnlContenedor.Controls.Add(ctrl)
    End Sub

    Private Sub frmContenedor_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
        Me.Hide()
    End Sub
End Class