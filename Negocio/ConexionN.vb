﻿Public Class ConexionN

    Public Shared Function BD_AxControlConectada() As Boolean
        Dim oBD As New AccesoDatos.ConexionAD
        Return oBD.ConexionAxCotrol()
    End Function

    Public Shared Function BD_DriverConectada() As Boolean
        Dim oBD As New AccesoDatos.ConexionAD
        Return oBD.ConexionAxDriver()
    End Function

End Class
