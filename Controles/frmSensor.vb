﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Public Class frmSensor

#Region "Variables de Instancia"
    Dim oDatoIntN As DatoWordIntN
#End Region

#Region "Propiedades"
    Private cmd As New DATO_WORDINT
    Public Property Comando() As DATO_WORDINT ' 1= Habilitar; 2= Deshabilitar
        Get
            Return cmd
        End Get
        Set(ByVal value As DATO_WORDINT)
            cmd = value
        End Set
    End Property

    Private newDescripcion As String
    Public Property Descripcion() As String
        Get
            Return newDescripcion
        End Get
        Set(ByVal value As String)
            newDescripcion = value
            txtDescripcion.Text = value
        End Set
    End Property



    Private _EstadoWord As DATO_WORDINT
    Public Property EstadoWord() As DATO_WORDINT
        Get
            Return _EstadoWord
        End Get
        Set(ByVal value As DATO_WORDINT)
            _EstadoWord = value

            If _EstadoWord.VALOR = SensorInfrarojoEstado.ConPresencia Then
                modDelegado.SetText(Me, txtEstado, "Leyendo")

            ElseIf _EstadoWord.VALOR = SensorInfrarojoEstado.Sinprecencia Then
                modDelegado.SetText(Me, txtEstado, "Sin lectura")

            ElseIf _EstadoWord.VALOR = SensorInfrarojoEstado.Deshabilitado Then
                modDelegado.SetText(Me, txtEstado, "Deshabilitado")

            End If

        End Set
    End Property

    Private _Habilitacion As DATO_WORDINT
    Public Property oHabilitacion() As DATO_WORDINT
        Get
            Return _Habilitacion
        End Get
        Set(ByVal value As DATO_WORDINT)
            _Habilitacion = value
        End Set
    End Property

    Private tmp As New DATO_WORDINT
    Public Property TiempoRet() As DATO_WORDINT
        Get
            Return tmp
        End Get
        Set(ByVal value As DATO_WORDINT)
            tmp = value
            If value.VALOR <> tmp.VALOR Then

                modDelegado.SetText(Me, txtTiempoRetardo, tmp.VALOR.ToString)
            End If
        End Set
    End Property
#End Region

#Region "Métodos"



    Public Sub New(oEstado As DATO_WORDINT, oTiempoRet As DATO_WORDINT, oHabilitacion As DATO_WORDINT, Nombre As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oDatoIntN = New DatoWordIntN
        Me.EstadoWord = oEstado
        'Me.EstadoWord = Nothing

        Me.oHabilitacion = oHabilitacion
        Me.Descripcion = Nombre
        Me.TiempoRet = oTiempoRet
        txtTiempoRetardo.Text = oTiempoRet.VALOR



        txtPLC.Text = oEstado.FK_PLC
        txtSector.Text = oEstado.ID_SECTOR
    End Sub



    Private Sub btnHabilitar_Click(sender As System.Object, e As System.EventArgs)
        Comando.valor = 1
        cmd = oDatoIntN.Escribir(Comando.ID, Comando.VALOR)
    End Sub

    Private Sub btnDeshabilitar_Click(sender As System.Object, e As System.EventArgs)
        Comando.valor = 0
        cmd = oDatoIntN.Escribir(Comando.id, Comando.valor)
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)

        If oAutenticacion.ShowDialog = DialogResult.OK Then
            Dim descripAuditoria As String



            If IsNumeric(txtTiempoRetardo.Text) Then TiempoRet.valor = txtTiempoRetardo.Text
            tmp = oDatoIntN.Escribir(TiempoRet.id, TiempoRet.valor)

            descripAuditoria = String.Format("Se actualizo el tiempo de retorno del sensor {0} a {1} ", TiempoRet.tag, TiempoRet.valor)
            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, oAutenticacion.Usuario, Constante.acciones.habilitacionSensor, 0)
            Me.Close()

        End If
    End Sub

    Private Sub btnHabilitarSensorPosicion_Click(sender As Object, e As EventArgs) Handles btnHabilitar.Click, btnDeshabilitar.Click

        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog <> DialogResult.OK Then Return

        Dim btn As Button = sender
        Dim oComandoEscribir As Constante.SensorHabilitacion = SensorHabilitacion.Habilitar
        Select Case btn.Name
            Case btnHabilitar.Name
                oComandoEscribir = SensorHabilitacion.Habilitar
            Case btnDeshabilitar.Name
                oComandoEscribir = SensorHabilitacion.Deshabilitar
        End Select

        Dim nHabDeshabSensorPosicion As New DatoWordIntN
        nHabDeshabSensorPosicion.Escribir(Me.oHabilitacion.id, oComandoEscribir)
        MessageBox.Show("Operación realizada con exito", "Comando", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()

    End Sub



#End Region


End Class