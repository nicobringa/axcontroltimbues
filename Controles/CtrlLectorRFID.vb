﻿Imports Impinj.OctaneSdk
Imports Negocio.LectorRFID_SpeedWay
Imports System.Threading
Public Class CtrlLectorRFID

    Private WithEvents _LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay
    Private hControlEstado As Thread
    Private _RunnigLector As Boolean
    Public Property Running() As Boolean
        Get
            Return _RunnigLector
        End Get
        Set(ByVal value As Boolean)
            _RunnigLector = value
            If value Then
                btnStar.BackColor = Color.Red
                SetBtn(btnStar, "Stop", Color.Red)
                SetTextLabel(Negocio.LectorRFID_SpeedWay.TEXT_START, lblLectorLectura, Negocio.LectorRFID_SpeedWay.COLOR_START)
            Else
                btnStar.BackColor = Color.Blue
                SetBtn(btnStar, "Start", Color.Blue)
                SetTextLabel(Negocio.LectorRFID_SpeedWay.TEXT_STOP, lblLectorLectura, Negocio.LectorRFID_SpeedWay.COLOR_STOP)
            End If
        End Set
    End Property


    'Private _Antena1Conectada As Boolean
    'Public Property Antena1Conectada() As Boolean
    '    Get
    '        Return _Antena1Conectada
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Antena1Conectada = value
    '        Dim lbl As Label = lblEstadoAntena1
    '        If value Then
    '            lbl.Text = Negocio.LectorRFID.TEXT_CONECTADO
    '            lbl.ForeColor = Negocio.LectorRFID.COLOR_CONECTADO
    '        Else 'Desconectado
    '            lbl.Text = Negocio.LectorRFID.TEXT_DESCONECTADO
    '            lbl.ForeColor = Negocio.LectorRFID.COLOR_DESCONECTADO
    '        End If
    '    End Set
    'End Property


    'Private _Antena2Conectada As Boolean
    'Public Property Antena2Conectada() As Boolean
    '    Get
    '        Return _Antena2Conectada
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Antena2Conectada = value
    '    End Set
    'End Property


    'Private _Antena3Conectada As Boolean
    'Public Property Antena3Conectada() As Boolean
    '    Get
    '        Return _Antena3Conectada
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Antena3Conectada = value
    '    End Set
    'End Property


    'Private _Antena4Conectada As Boolean
    'Public Property Antena4Conectada() As Boolean
    '    Get
    '        Return _Antena4Conectada
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Antena4Conectada = value
    '    End Set
    'End Property



#Region "Delegados"
    Delegate Sub DelSetTextLabel(ByVal texto As String, ByVal lbl As Label, ByVal _Color As Drawing.Color)
    Private Sub SetTextLabel(ByVal texto As String, ByVal lbl As Label, ByVal _Color As Drawing.Color)
        If lbl.InvokeRequired Then
            Dim Delegado As New DelSetTextLabel(AddressOf SetTextLabel)
            Me.Invoke(Delegado, New Object() {texto, lbl, _Color})
        Else
            lbl.Text = texto
            lbl.ForeColor = _Color
        End If
    End Sub

    Delegate Sub DelSetBtn(ByVal btn As Button, ByVal texto As String, ByVal _Color As Drawing.Color)
    Private Sub SetBtn(ByVal btn As Button, ByVal texto As String, ByVal _Color As Drawing.Color)
        If btn.InvokeRequired Then
            Dim Delegado As New DelSetBtn(AddressOf SetBtn)
            Me.Invoke(Delegado, New Object() {btn, texto, _Color})
        Else
            btn.Text = texto
            btn.ForeColor = Color.White
            btn.BackColor = _Color
        End If
    End Sub
#End Region


#Region "Constructor"


    Public Sub New()
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
    End Sub

    Public Sub New(ByVal lectorRFID As Negocio.LectorRFID_SpeedWay)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        Me._LectorRFID_SpeedWay = lectorRFID
        RefrescarFormulario()
    End Sub

#End Region

#Region "Metodos"

    Public Sub CargarLectorRFID(ByVal lectorRFID As Negocio.LectorRFID_SpeedWay)
        Me._LectorRFID_SpeedWay = lectorRFID
        RefrescarFormulario()

    End Sub


    Private Sub RefrescarFormulario()
        While True
            If IsNothing(_LectorRFID_SpeedWay) Then Return
            If Not IsNothing(_LectorRFID_SpeedWay.Ip) Then
                If _LectorRFID_SpeedWay.IsConnected Then
                    btnConectarDesconectar.Text = "Desconectar"
                    SetTextLabel("Conectado", lblLectorConexion, Color.GreenYellow)
                    VerEstadoAntena()
                Else
                    btnConectarDesconectar.Text = "Conectar"
                    SetTextLabel("Desconectado", lblLectorConexion, Color.OrangeRed)
                End If
            End If


            If _LectorRFID_SpeedWay.LectorConfigurado Then
                txtIP.Text = _LectorRFID_SpeedWay.Ip
                txtNombre.Text = _LectorRFID_SpeedWay.Nombre
                txtDescripcion.Text = _LectorRFID_SpeedWay.Descripcion
                CargarConfiguracion()

                If _LectorRFID_SpeedWay.IsConnected Then
                    Running = _LectorRFID_SpeedWay.isRunning
                End If


            Else
                txtNombre.Text = _LectorRFID_SpeedWay.Nombre
                MessageBox.Show("El Lector RFID no se encuentra configurado para su funcionamiento", "Configuracón RFID", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If


        End While



    End Sub

    Private Sub CargarConfiguracion()
        flpConf.Controls.Clear()

        If Not IsNothing(_LectorRFID_SpeedWay) Then
            For Each oConf As Entidades.Config_Lector_RFID In _LectorRFID_SpeedWay.LectorRFID.config_lector_rfid
                'Creo el control de configuracion
                Dim ctrlConf As New Controles.CtrlConfRFID(oConf)
                flpConf.Controls.Add(ctrlConf)
            Next
        End If
    End Sub

    Private Sub VerEstadoAntena()
        Dim LectorEstado As Status = _LectorRFID_SpeedWay.QueryStatus

        For Each AntSatus As AntennaStatus In LectorEstado.Antennas

            If AntSatus.PortNumber = 1 Then
                SetLabelAntena(lblEstadoAntena1, AntSatus.IsConnected)
            ElseIf AntSatus.PortNumber = 2 Then
                SetLabelAntena(lblEstadoAntena2, AntSatus.IsConnected)
            ElseIf AntSatus.PortNumber = 3 Then
                SetLabelAntena(lblEstadoAntena3, AntSatus.IsConnected)
            Else ' 4
                SetLabelAntena(lblEstadoAntena4, AntSatus.IsConnected)
            End If

        Next

        lblTemperatura.Text = LectorEstado.TemperatureInCelsius & "°"

    End Sub

    Private Sub SetLabelAntena(ByVal lbl As Label, ByVal AntenaConectada As Boolean)
        If AntenaConectada Then
            lbl.Text = Negocio.LectorRFID_SpeedWay.TEXT_CONECTADO
            lbl.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_CONECTADO
        Else 'Antena Desconectada
            lbl.Text = Negocio.LectorRFID_SpeedWay.TEXT_DESCONECTADO
            lbl.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_DESCONECTADO
        End If

    End Sub

#End Region

#Region "Eventos"
    Private Sub btnConectarDesconectar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConectarDesconectar.Click
        Try
            _LectorRFID_SpeedWay.GuardarConf()
            If btnConectarDesconectar.Text = "Desconectar" Then
                Me._LectorRFID_SpeedWay.Disconnect()
            Else ' Conectar
                Me._LectorRFID_SpeedWay.Connect(Me._LectorRFID_SpeedWay.Ip)
            End If

            RefrescarFormulario()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ctrl Lector RFID", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txt_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.Leave, txtIP.Leave
        Dim txt As TextBox = CType(sender, TextBox)

        Select Case txt.Name
            Case "txtNombre"
               ' _LectorRFID_SpeedWay.Nombre = txt.Text
            Case "txtIP"
                ' _LectorRFID_SpeedWay.Ip = txtIP.Text
        End Select
    End Sub

    Private Sub ctrlLectorRFID_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        CargarCombos()
        cmbModelo.SelectedIndex = -1
        'If LectorRFID.IsConnected Then Running = LectorRFID.isRunning



    End Sub
    Private Sub CargarCombos()
        Dim oModeloLectorN As New Negocio.Modelo_LectorRfidN


    End Sub
#Region "Eventos Lector"

    Private Sub Lectura_Start() Handles _LectorRFID_SpeedWay.ReaderStarted
        Running = True
    End Sub

    Private Sub Lectura_Stop() Handles _LectorRFID_SpeedWay.ReaderStopped
        SetTextLabel("Stop", lblLectorLectura, Color.OrangeRed)
        Running = False
    End Sub

    Private Sub ConexionPerdida() Handles _LectorRFID_SpeedWay.ConnectionLost
        SetTextLabel("Desconectado", lblLectorConexion, Color.OrangeRed)
    End Sub

#End Region

#End Region


    Private Sub btnAgrConfiguracion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgrConfiguracion.Click
        Dim oConfiguracion As New Entidades.CONFIG_LECTOR_RFID
        'Aplico configuracion por defecto
        oConfiguracion.SELECT_READER_MODE = ReaderMode.AutoSetDenseReader
        oConfiguracion.SELECT_REPORT_MODE = ReportMode.WaitForQuery
        oConfiguracion.SELECT_SEARCH = SearchMode.DualTarget
        oConfiguracion.SELECT_SESSION = 1
        oConfiguracion.ID_LECTOR_RFID = Me._LectorRFID_SpeedWay.LectorRFID.ID_LECTOR_RFID
        If IsNothing(_LectorRFID_SpeedWay.LectorRFID.config_lector_rfid) Then _LectorRFID_SpeedWay.LectorRFID.config_lector_rfid = New List(Of Entidades.Config_Lector_RFID)

        _LectorRFID_SpeedWay.LectorRFID.config_lector_rfid.Add(oConfiguracion)

        CargarConfiguracion()
    End Sub

    Private Sub Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Guardar.Click
        Try
            Me._LectorRFID_SpeedWay.GuardarConf()
            MessageBox.Show("Operacion realizada con exito", "Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GuardarConf", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try


    End Sub


    Private Sub btnStar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStar.Click
        If _LectorRFID_SpeedWay.IsConnected Then
            If btnStar.Text = "Start" Then
                Dim frm As New FrmSeleccionConfRFID(_LectorRFID_SpeedWay.LectorRFID.config_lector_rfid)
                If frm.ShowDialog = DialogResult.OK Then
                    Try
                        _LectorRFID_SpeedWay.AplicarConfiguracion(frm._ConfSeleccionada)
                        _LectorRFID_SpeedWay.StartRead()
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "Aplicar Configuración", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Try
                End If
            Else
                _LectorRFID_SpeedWay.StopRead()
            End If

        Else
            MessageBox.Show("No se encuentra conectado el lector", "Star", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub


End Class
