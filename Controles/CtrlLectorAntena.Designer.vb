﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlLectorAntena
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnlAntenas = New System.Windows.Forms.Panel()
        Me.lblLector = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.btnConectarDesconectar = New System.Windows.Forms.Button()
        Me.txtIP = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tpEstado = New System.Windows.Forms.TabPage()
        Me.btnRefrescar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblEstadoPuerto4 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblEstadoPuerto3 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblEstadoPuerto2 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblEstadoPuerto1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblTemperatura = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena4 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena3 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena2 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.tpConf = New System.Windows.Forms.TabPage()
        Me.flpConf = New System.Windows.Forms.FlowLayoutPanel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.cmbModelo = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnStar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblLectorConexion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblLectorLectura = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAgrConfiguracion = New System.Windows.Forms.Button()
        Me.Guardar = New System.Windows.Forms.Button()
        Me.cbxAutoConectarse = New System.Windows.Forms.CheckBox()
        Me.cbxAutoStart = New System.Windows.Forms.CheckBox()
        Me.tpEstado.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpConf.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAntenas
        '
        Me.pnlAntenas.Location = New System.Drawing.Point(890, 54)
        Me.pnlAntenas.Name = "pnlAntenas"
        Me.pnlAntenas.Size = New System.Drawing.Size(360, 461)
        Me.pnlAntenas.TabIndex = 80
        '
        'lblLector
        '
        Me.lblLector.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLector.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(147, Byte), Integer), CType(CType(184, Byte), Integer))
        Me.lblLector.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLector.ForeColor = System.Drawing.Color.White
        Me.lblLector.Location = New System.Drawing.Point(888, 2)
        Me.lblLector.Name = "lblLector"
        Me.lblLector.Size = New System.Drawing.Size(365, 45)
        Me.lblLector.TabIndex = 79
        Me.lblLector.Text = "-"
        Me.lblLector.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(3, 10)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(440, 31)
        Me.txtNombre.TabIndex = 67
        '
        'btnConectarDesconectar
        '
        Me.btnConectarDesconectar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnConectarDesconectar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConectarDesconectar.Location = New System.Drawing.Point(568, 48)
        Me.btnConectarDesconectar.Name = "btnConectarDesconectar"
        Me.btnConectarDesconectar.Size = New System.Drawing.Size(154, 34)
        Me.btnConectarDesconectar.TabIndex = 66
        Me.btnConectarDesconectar.Text = "Conectar"
        Me.btnConectarDesconectar.UseVisualStyleBackColor = False
        '
        'txtIP
        '
        Me.txtIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIP.Location = New System.Drawing.Point(40, 52)
        Me.txtIP.Name = "txtIP"
        Me.txtIP.Size = New System.Drawing.Size(215, 26)
        Me.txtIP.TabIndex = 68
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 20)
        Me.Label1.TabIndex = 69
        Me.Label1.Text = "IP:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(3, 112)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(879, 52)
        Me.txtDescripcion.TabIndex = 74
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 20)
        Me.Label2.TabIndex = 75
        Me.Label2.Text = "Descripción:"
        '
        'tpEstado
        '
        Me.tpEstado.Controls.Add(Me.btnRefrescar)
        Me.tpEstado.Controls.Add(Me.GroupBox2)
        Me.tpEstado.Controls.Add(Me.lblTemperatura)
        Me.tpEstado.Controls.Add(Me.Label9)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena4)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena3)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena2)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena1)
        Me.tpEstado.Controls.Add(Me.Label8)
        Me.tpEstado.Controls.Add(Me.PictureBox4)
        Me.tpEstado.Controls.Add(Me.Label7)
        Me.tpEstado.Controls.Add(Me.PictureBox3)
        Me.tpEstado.Controls.Add(Me.Label6)
        Me.tpEstado.Controls.Add(Me.PictureBox2)
        Me.tpEstado.Controls.Add(Me.Label5)
        Me.tpEstado.Controls.Add(Me.PictureBox1)
        Me.tpEstado.Location = New System.Drawing.Point(4, 22)
        Me.tpEstado.Name = "tpEstado"
        Me.tpEstado.Padding = New System.Windows.Forms.Padding(3)
        Me.tpEstado.Size = New System.Drawing.Size(871, 293)
        Me.tpEstado.TabIndex = 1
        Me.tpEstado.Text = "Estado"
        Me.tpEstado.UseVisualStyleBackColor = True
        '
        'btnRefrescar
        '
        Me.btnRefrescar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnRefrescar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefrescar.Location = New System.Drawing.Point(711, 6)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(154, 34)
        Me.btnRefrescar.TabIndex = 83
        Me.btnRefrescar.Text = "Refrescar"
        Me.btnRefrescar.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblEstadoPuerto4)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.lblEstadoPuerto3)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.lblEstadoPuerto2)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.lblEstadoPuerto1)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 181)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(129, 100)
        Me.GroupBox2.TabIndex = 76
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "GPIO"
        '
        'lblEstadoPuerto4
        '
        Me.lblEstadoPuerto4.AutoSize = True
        Me.lblEstadoPuerto4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoPuerto4.ForeColor = System.Drawing.Color.Blue
        Me.lblEstadoPuerto4.Location = New System.Drawing.Point(99, 76)
        Me.lblEstadoPuerto4.Name = "lblEstadoPuerto4"
        Me.lblEstadoPuerto4.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoPuerto4.TabIndex = 77
        Me.lblEstadoPuerto4.Text = "-"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(14, 76)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 20)
        Me.Label13.TabIndex = 76
        Me.Label13.Text = "Puerto 4: "
        '
        'lblEstadoPuerto3
        '
        Me.lblEstadoPuerto3.AutoSize = True
        Me.lblEstadoPuerto3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoPuerto3.ForeColor = System.Drawing.Color.Blue
        Me.lblEstadoPuerto3.Location = New System.Drawing.Point(99, 56)
        Me.lblEstadoPuerto3.Name = "lblEstadoPuerto3"
        Me.lblEstadoPuerto3.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoPuerto3.TabIndex = 75
        Me.lblEstadoPuerto3.Text = "-"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(14, 56)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(87, 20)
        Me.Label16.TabIndex = 74
        Me.Label16.Text = "Puerto 3: "
        '
        'lblEstadoPuerto2
        '
        Me.lblEstadoPuerto2.AutoSize = True
        Me.lblEstadoPuerto2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoPuerto2.ForeColor = System.Drawing.Color.Blue
        Me.lblEstadoPuerto2.Location = New System.Drawing.Point(99, 36)
        Me.lblEstadoPuerto2.Name = "lblEstadoPuerto2"
        Me.lblEstadoPuerto2.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoPuerto2.TabIndex = 73
        Me.lblEstadoPuerto2.Text = "-"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(14, 36)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 20)
        Me.Label14.TabIndex = 72
        Me.Label14.Text = "Puerto 2: "
        '
        'lblEstadoPuerto1
        '
        Me.lblEstadoPuerto1.AutoSize = True
        Me.lblEstadoPuerto1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoPuerto1.ForeColor = System.Drawing.Color.Blue
        Me.lblEstadoPuerto1.Location = New System.Drawing.Point(99, 16)
        Me.lblEstadoPuerto1.Name = "lblEstadoPuerto1"
        Me.lblEstadoPuerto1.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoPuerto1.TabIndex = 71
        Me.lblEstadoPuerto1.Text = "-"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(14, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(82, 20)
        Me.Label11.TabIndex = 64
        Me.Label11.Text = "Puerto 1:"
        '
        'lblTemperatura
        '
        Me.lblTemperatura.AutoSize = True
        Me.lblTemperatura.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTemperatura.Location = New System.Drawing.Point(705, 233)
        Me.lblTemperatura.Name = "lblTemperatura"
        Me.lblTemperatura.Size = New System.Drawing.Size(67, 37)
        Me.lblTemperatura.TabIndex = 75
        Me.lblTemperatura.Text = "50°"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(627, 184)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(222, 37)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "Temperatura:"
        '
        'lblEstadoAntena4
        '
        Me.lblEstadoAntena4.AutoSize = True
        Me.lblEstadoAntena4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena4.Location = New System.Drawing.Point(138, 149)
        Me.lblEstadoAntena4.Name = "lblEstadoAntena4"
        Me.lblEstadoAntena4.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena4.TabIndex = 73
        Me.lblEstadoAntena4.Text = "-"
        '
        'lblEstadoAntena3
        '
        Me.lblEstadoAntena3.AutoSize = True
        Me.lblEstadoAntena3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena3.Location = New System.Drawing.Point(138, 111)
        Me.lblEstadoAntena3.Name = "lblEstadoAntena3"
        Me.lblEstadoAntena3.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena3.TabIndex = 72
        Me.lblEstadoAntena3.Text = "-"
        '
        'lblEstadoAntena2
        '
        Me.lblEstadoAntena2.AutoSize = True
        Me.lblEstadoAntena2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena2.Location = New System.Drawing.Point(138, 73)
        Me.lblEstadoAntena2.Name = "lblEstadoAntena2"
        Me.lblEstadoAntena2.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena2.TabIndex = 71
        Me.lblEstadoAntena2.Text = "-"
        '
        'lblEstadoAntena1
        '
        Me.lblEstadoAntena1.AutoSize = True
        Me.lblEstadoAntena1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena1.Location = New System.Drawing.Point(138, 35)
        Me.lblEstadoAntena1.Name = "lblEstadoAntena1"
        Me.lblEstadoAntena1.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena1.TabIndex = 70
        Me.lblEstadoAntena1.Text = "-"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(45, 149)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(87, 20)
        Me.Label8.TabIndex = 69
        Me.Label8.Text = "Antena 4:"
        '
        'PictureBox4
        '
        Me.PictureBox4.Location = New System.Drawing.Point(6, 143)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox4.TabIndex = 68
        Me.PictureBox4.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(45, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 20)
        Me.Label7.TabIndex = 67
        Me.Label7.Text = "Antena 3:"
        '
        'PictureBox3
        '
        Me.PictureBox3.Location = New System.Drawing.Point(6, 105)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox3.TabIndex = 66
        Me.PictureBox3.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(45, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 20)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "Antena 2:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(6, 67)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 64
        Me.PictureBox2.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(45, 35)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 20)
        Me.Label5.TabIndex = 63
        Me.Label5.Text = "Antena 1:"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(6, 29)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'tpConf
        '
        Me.tpConf.Controls.Add(Me.flpConf)
        Me.tpConf.Location = New System.Drawing.Point(4, 22)
        Me.tpConf.Name = "tpConf"
        Me.tpConf.Padding = New System.Windows.Forms.Padding(3)
        Me.tpConf.Size = New System.Drawing.Size(871, 293)
        Me.tpConf.TabIndex = 0
        Me.tpConf.Text = "Configuración"
        Me.tpConf.UseVisualStyleBackColor = True
        '
        'flpConf
        '
        Me.flpConf.AutoScroll = True
        Me.flpConf.BackColor = System.Drawing.Color.Aqua
        Me.flpConf.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpConf.Location = New System.Drawing.Point(3, 3)
        Me.flpConf.Name = "flpConf"
        Me.flpConf.Size = New System.Drawing.Size(865, 287)
        Me.flpConf.TabIndex = 11
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tpConf)
        Me.TabControl1.Controls.Add(Me.tpEstado)
        Me.TabControl1.Location = New System.Drawing.Point(3, 168)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(879, 319)
        Me.TabControl1.TabIndex = 76
        '
        'cmbModelo
        '
        Me.cmbModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModelo.FormattingEnabled = True
        Me.cmbModelo.Location = New System.Drawing.Point(342, 57)
        Me.cmbModelo.Name = "cmbModelo"
        Me.cmbModelo.Size = New System.Drawing.Size(215, 21)
        Me.cmbModelo.TabIndex = 77
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(269, 55)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 20)
        Me.Label10.TabIndex = 78
        Me.Label10.Text = "Modelo:"
        '
        'btnStar
        '
        Me.btnStar.BackColor = System.Drawing.Color.Blue
        Me.btnStar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStar.ForeColor = System.Drawing.Color.White
        Me.btnStar.Location = New System.Drawing.Point(6, 19)
        Me.btnStar.Name = "btnStar"
        Me.btnStar.Size = New System.Drawing.Size(101, 54)
        Me.btnStar.TabIndex = 57
        Me.btnStar.Text = "Start"
        Me.btnStar.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnStar)
        Me.GroupBox1.Location = New System.Drawing.Point(775, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(109, 81)
        Me.GroupBox1.TabIndex = 70
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Estado de Lectura"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(101, 20)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Lector RFID:"
        '
        'lblLectorConexion
        '
        Me.lblLectorConexion.AutoSize = True
        Me.lblLectorConexion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLectorConexion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLectorConexion.Location = New System.Drawing.Point(110, 3)
        Me.lblLectorConexion.Name = "lblLectorConexion"
        Me.lblLectorConexion.Size = New System.Drawing.Size(14, 20)
        Me.lblLectorConexion.TabIndex = 1
        Me.lblLectorConexion.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(211, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Lectura RFID:"
        '
        'lblLectorLectura
        '
        Me.lblLectorLectura.AutoSize = True
        Me.lblLectorLectura.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLectorLectura.ForeColor = System.Drawing.Color.Aqua
        Me.lblLectorLectura.Location = New System.Drawing.Point(327, 3)
        Me.lblLectorLectura.Name = "lblLectorLectura"
        Me.lblLectorLectura.Size = New System.Drawing.Size(14, 20)
        Me.lblLectorLectura.TabIndex = 3
        Me.lblLectorLectura.Text = "-"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(37, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lblLectorLectura)
        Me.Panel1.Controls.Add(Me.lblLectorConexion)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(0, 490)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(888, 26)
        Me.Panel1.TabIndex = 71
        '
        'btnAgrConfiguracion
        '
        Me.btnAgrConfiguracion.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAgrConfiguracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgrConfiguracion.ForeColor = System.Drawing.Color.Black
        Me.btnAgrConfiguracion.Location = New System.Drawing.Point(449, 2)
        Me.btnAgrConfiguracion.Name = "btnAgrConfiguracion"
        Me.btnAgrConfiguracion.Size = New System.Drawing.Size(177, 39)
        Me.btnAgrConfiguracion.TabIndex = 72
        Me.btnAgrConfiguracion.Text = "Agregar Configuración"
        Me.btnAgrConfiguracion.UseVisualStyleBackColor = False
        '
        'Guardar
        '
        Me.Guardar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Guardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guardar.Location = New System.Drawing.Point(632, 2)
        Me.Guardar.Name = "Guardar"
        Me.Guardar.Size = New System.Drawing.Size(137, 39)
        Me.Guardar.TabIndex = 73
        Me.Guardar.Text = "Guardar"
        Me.Guardar.UseVisualStyleBackColor = False
        '
        'cbxAutoConectarse
        '
        Me.cbxAutoConectarse.AutoSize = True
        Me.cbxAutoConectarse.Location = New System.Drawing.Point(287, 88)
        Me.cbxAutoConectarse.Name = "cbxAutoConectarse"
        Me.cbxAutoConectarse.Size = New System.Drawing.Size(104, 17)
        Me.cbxAutoConectarse.TabIndex = 81
        Me.cbxAutoConectarse.Text = "Auto conectarse"
        Me.cbxAutoConectarse.UseVisualStyleBackColor = True
        '
        'cbxAutoStart
        '
        Me.cbxAutoStart.AutoSize = True
        Me.cbxAutoStart.Location = New System.Drawing.Point(398, 88)
        Me.cbxAutoStart.Name = "cbxAutoStart"
        Me.cbxAutoStart.Size = New System.Drawing.Size(71, 17)
        Me.cbxAutoStart.TabIndex = 82
        Me.cbxAutoStart.Text = "Auto start"
        Me.cbxAutoStart.UseVisualStyleBackColor = True
        '
        'CtrlLectorAntena
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cbxAutoStart)
        Me.Controls.Add(Me.cbxAutoConectarse)
        Me.Controls.Add(Me.pnlAntenas)
        Me.Controls.Add(Me.lblLector)
        Me.Controls.Add(Me.Guardar)
        Me.Controls.Add(Me.btnAgrConfiguracion)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmbModelo)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtIP)
        Me.Controls.Add(Me.btnConectarDesconectar)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "CtrlLectorAntena"
        Me.Size = New System.Drawing.Size(1250, 518)
        Me.tpEstado.ResumeLayout(False)
        Me.tpEstado.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpConf.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlAntenas As Panel
    Friend WithEvents lblLector As Label
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents btnConectarDesconectar As Button
    Friend WithEvents txtIP As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents tpEstado As TabPage
    Friend WithEvents lblTemperatura As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblEstadoAntena4 As Label
    Friend WithEvents lblEstadoAntena3 As Label
    Friend WithEvents lblEstadoAntena2 As Label
    Friend WithEvents lblEstadoAntena1 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents Label7 As Label
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label6 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label5 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents tpConf As TabPage
    Friend WithEvents flpConf As FlowLayoutPanel
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents cmbModelo As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents btnStar As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents lblLectorConexion As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblLectorLectura As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnAgrConfiguracion As Button
    Friend WithEvents Guardar As Button
    Friend WithEvents cbxAutoConectarse As CheckBox
    Friend WithEvents cbxAutoStart As CheckBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lblEstadoPuerto4 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents lblEstadoPuerto3 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents lblEstadoPuerto2 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents lblEstadoPuerto1 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents btnRefrescar As Button
End Class
