﻿Imports Impinj.OctaneSdk
Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades



''' <summary>
''' Clase que permite instanciar un lector de RFID SpeedWay
''' </summary>
''' <remarks></remarks>
Public Class AxSpeedWayR : Inherits  ImpinjReader '  Hereda de la clase del fabricante Impinj
    ''' <summary>
    ''' Nombre de del control de usuario que utiliza el lector
    ''' </summary>
    ''' <remarks></remarks>
    Private _NombreControlUsuario As String

#Region "Contructor"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="NombreControlUsuario">nombre del control que utiliza el lector</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal NombreControlUsuario As String)
        _NombreControlUsuario = NombreControlUsuario

        'Coloco los ajustes guardados
        'SetAjustes()

    End Sub
#End Region
#Region "Propiedades"
    Public Event ReporteError(ByVal msjError As String)

    Private _NombreLector As String
    Public Property NombreLector() As String
        Get
            Return _NombreLector
        End Get
        Set(ByVal value As String)
            _NombreLector = value
        End Set
    End Property

    ''' <summary>
    ''' Coleccion de antenas
    ''' </summary>
    ''' <remarks></remarks>
    Public ListAntenas As New List(Of Antenas)
    ''' <summary>
    ''' Permite obtener o establcer la direccion de ip del lector
    ''' </summary>
    ''' <remarks></remarks>
    Private _IpAddress As String
    Public Property IpAddress As String
        Get
            Return _IpAddress
        End Get
        Set(ByVal value As String)
            _IpAddress = value
        End Set
    End Property
    Private _SelectReaderMode As Integer
    Public Property SelectReaderMode() As Integer
        Get
            Return _SelectReaderMode
        End Get
        Set(ByVal value As Integer)
            _SelectReaderMode = value
        End Set
    End Property
    Private _SelectSeachMode As Integer
    Public Property SelectSearch() As Integer
        Get
            Return _SelectSeachMode
        End Get
        Set(ByVal value As Integer)
            _SelectSeachMode = value
        End Set
    End Property
    Private _SelectSession As Integer
    Public Property SelectSession() As Integer
        Get
            Return _SelectSession
        End Get
        Set(ByVal value As Integer)
            _SelectSession = value
        End Set
    End Property
    Private _IsStart As Boolean = False
    Public Property IsStart() As Boolean
        Get
            Return _IsStart
        End Get
        Set(ByVal value As Boolean)
            _IsStart = value
        End Set
    End Property

    ''' <summary>
    ''' Si tiene un configuracion definida por el usuario
    ''' True - Configuracion definida por el usuario
    ''' False- no tiene configuracion definida por el usuario , tiene la configuracion por defecto
    ''' </summary>
    ''' <remarks></remarks>
    Private _TieneCofigUser As Boolean
    Public Property TieneConfigUser() As Boolean
        Get
            Return _TieneCofigUser
        End Get
        Set(ByVal value As Boolean)
            _TieneCofigUser = value
        End Set
    End Property

    ''' <summary>
    ''' OBbtiene o establece el reporte seleccionado , para luego guardarlo en el archivo XML
    ''' </summary>
    ''' <remarks></remarks>
    Private _SelectReportMode As Integer
    Public Property SelectReportMode() As Integer
        Get
            Return _SelectReportMode
        End Get
        Set(ByVal value As Integer)
            _SelectReportMode = value
        End Set
    End Property



#End Region
#Region "Metodos"
    ''' <summary>
    ''' Permite guardar en un archivo xml la configuración del lector y Aplicar la nueva configuracion
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GuardarAplicar()

        Try
            'Creo el objeto que permite guardo en el archivo
            Dim speedXml As New SpeedWayXml
            'Paso los datos a guar en el archivo xml

            speedXml.NombreLector = Me.NombreLector
            speedXml.IpAddress = Me.IpAddress
            speedXml.SelectReaderMode = Me.SelectReaderMode
            speedXml.ListAntenas = Me.ListAntenas
            speedXml.SelectSearch = Me.SelectSearch
            speedXml.SelectSession = Me.SelectSession
            speedXml.SelectReportMode = Me.SelectReportMode

            'My.Application.Info.DirectoryPath
            Dim serializer As New XmlSerializer(GetType(SpeedWayXml))
            'Si no existe el archivo lo crea y si existe lo sobrecribe
            Dim textWriter As TextWriter = New StreamWriter(My.Application.Info.DirectoryPath & "\" & "AxSpeedWayRConfig" & _NombreControlUsuario)
            serializer.Serialize(textWriter, speedXml)
            textWriter.Close()
            SetAjustes()
        Catch ex As Exception

        End Try

    End Sub
    Public Function Conectar() As Boolean
        Return SetAjustes()
    End Function
#End Region



    ''' <summary>
    ''' Modifica los ajustes del lector por defecto por los que estan guardado en archivo xml
    ''' </summary>
    ''' <remarks></remarks>
    Public Function SetAjustes() As Boolean
        Try
            'Obtengo la configuracion guardada en el archivo
            Dim oSpeedWayXml As SpeedWayXml = GetSpeedWayXml()

            If Not IsNothing(oSpeedWayXml) Then
                Me.NombreLector = oSpeedWayXml.NombreLector
                'Paso la ip de coneccion
                Me.IpAddress = oSpeedWayXml.IpAddress
                'Me conecto para configurar los ajustes guardados
                Me.Connect(Me.IpAddress)
                'Obtengo la configuracion 
                Dim readerSettings As Settings = Me.QueryDefaultSettings
                'Seteo el modo de lectura
                Me.SelectReaderMode = oSpeedWayXml.SelectReaderMode
                readerSettings.ReaderMode = Me.SelectReaderMode
                'Seteo el modo de busqueda
                Me.SelectSearch = oSpeedWayXml.SelectSearch
                readerSettings.SearchMode = Me.SelectSearch ' Me.SelectSearch
                'Seteo la seccion 
                readerSettings.Session = oSpeedWayXml.SelectSession
                Me.SelectSession = oSpeedWayXml.SelectSession
                'Seteo el modo de reporte
                readerSettings.Report.Mode = oSpeedWayXml.SelectReportMode
                Me.SelectReportMode = oSpeedWayXml.SelectReportMode

                'Deshabilito todas las atenas
                readerSettings.Antennas.DisableAll()
                'Habilito la tenas correspondiente y seteo su power y sensibilidad
                For Each antena As AxSpeedWayR.Antenas In oSpeedWayXml.ListAntenas
                    readerSettings.Antennas.GetAntenna(antena.NumeroAntena).IsEnabled = True
                    readerSettings.Antennas.GetAntenna(antena.NumeroAntena).TxPowerInDbm = antena.TxPower
                    readerSettings.Antennas.GetAntenna(antena.NumeroAntena).RxSensitivityInDbm = antena.RxSensitivity
                Next
                'Paso las atenas establecida en el archivo de objeto al objeto instanciado
                Me.ListAntenas = oSpeedWayXml.ListAntenas

                '###################CONFIGURACIONES A IMPLEMENTAR ###################
                'readerSettings.Report.Mode = ReportMode.WaitForQuery ' Reporta lo leido cuando realizas un consulta
                readerSettings.Report.IncludeAntennaPortNumber = True
                'readerSettings.Report.Mode = ReportMode.BatchAfterStop 'Constantamente 
                '#################################################################

                '#################PARA PODER LEER EL LUGAR DE MEMORIA TID#######################
                'Crear una operación de leer la etiqueta para la memoria TID .
                Dim ReadTID As New TagReadOp
                'Leer desde la memoria TID
                ReadTID.MemoryBank = MemoryBank.Tid
                ' Leer 3 ( 16 - bit) palabras
                ReadTID.WordCount = 3  '# LOS TAG DE URUGUAY OCUPAN 3 PALABRAS(WORDS)#
                ' A partir de la palabra 1 # LOS TAG DE URUGUAY EMPIEZAN APARTIR DE LA PABALA 1#
                ReadTID.WordPointer = 1
                'Añadir estas operaciones al lector como optimizados Leer ops .
                'Optimizados Leer ops se aplican a todas las etiquetas , a diferencia
                ' Tag Operación secuencias, que se puede aplicar a las etiquetas específicas.
                ' Revolución Speedway soporta hasta dos operaciones de lectura Optimizado .
                readerSettings.Report.OptimizedReadOps.Add(ReadTID)

                'Aplico los ajustes
                Me.ApplySettings(readerSettings)
                '#######   Me.Disconnect()

                Me.TieneConfigUser = True
                Return True
            Else ' no tiene archivo de configuracion del usuario
                Me.TieneConfigUser = False
                RaiseEvent ReporteError("El lector RFID no se encuentra configurado")
                Return False
            End If
        Catch ex As OctaneSdkException
            Me.Disconnect()
            RaiseEvent ReporteError(ex.Message)
            'Throw New OctaneSdkException(ex.Message)
            Return False
        Catch ex As Exception
            Me.Disconnect()
            RaiseEvent ReporteError(ex.Message)
            Return False
            'Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetSpeedWayXml() As SpeedWayXml
        Try
            Dim deserializer As New XmlSerializer(GetType(SpeedWayXml))
            Dim textReader As TextReader = New StreamReader(My.Application.Info.DirectoryPath & "\" & "AxSpeedWayRConfig" & _NombreControlUsuario)
            Dim oSpeedWayXml As SpeedWayXml
            oSpeedWayXml = DirectCast(deserializer.Deserialize(textReader), SpeedWayXml)
            textReader.Close()

            Return oSpeedWayXml
        Catch ex As Exception
            Return Nothing
        End Try

    End Function



#Region "Clases Hijas"
    ''' <summary>
    ''' Objeto que se va guardar en el archivo xml
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SpeedWayXml

        Private _NombreLector As String
        Public Property NombreLector() As String
            Get
                Return _NombreLector
            End Get
            Set(ByVal value As String)
                _NombreLector = value
            End Set
        End Property


        ''' <summary>
        ''' Coleccion de antena del dispositivo
        ''' </summary>
        ''' <remarks></remarks>
        Public ListAntenas As New List(Of Antenas)

        ''' <summary>
        ''' Ip del lector que se va a guar en el archivo xml
        ''' </summary>
        ''' <remarks></remarks>
        Private _IpAddress As String
        Public Property IpAddress() As String
            Get
                Return _IpAddress
            End Get
            Set(ByVal value As String)
                _IpAddress = value
            End Set
        End Property


        Private _SelectReaderMode As Integer
        Public Property SelectReaderMode() As Integer
            Get
                Return _SelectReaderMode
            End Get
            Set(ByVal value As Integer)
                _SelectReaderMode = value
            End Set
        End Property

        Private _SelectSearch As Integer
        Public Property SelectSearch() As Integer
            Get
                Return _SelectSearch
            End Get
            Set(ByVal value As Integer)
                _SelectSearch = value
            End Set
        End Property

        Private _SelectSession As Integer
        Public Property SelectSession() As Integer
            Get
                Return _SelectSession
            End Get
            Set(ByVal value As Integer)
                _SelectSession = value
            End Set
        End Property

        Private _SelectReportMode As Integer
        Public Property SelectReportMode() As Integer
            Get
                Return _SelectReportMode
            End Get
            Set(ByVal value As Integer)
                _SelectReportMode = value
            End Set
        End Property

    End Class


    ''' <summary>
    ''' Clase de antenas
    ''' </summary>
    ''' <remarks></remarks>
    Public Class Antenas

        ''' <summary>
        ''' establecer o obtener el  Numero de antena
        ''' </summary>
        ''' <remarks></remarks>
        Private _NumeroAntena As Int16
        Public Property NumeroAntena() As Int16
            Get
                Return _NumeroAntena
            End Get
            Set(ByVal value As Int16)
                _NumeroAntena = value
            End Set
        End Property

        ''' <summary>
        ''' Permite obtener o establecer el power de la antena
        ''' </summary>
        ''' <remarks></remarks>
        Private _TxPower As Int16
        Public Property TxPower As Int16
            Get
                Return _TxPower
            End Get
            Set(ByVal value As Int16)
                _TxPower = value
            End Set
        End Property

        ''' <summary>
        ''' Permite obtener o establecer la sensibilidad de la antena
        ''' </summary>
        ''' <remarks></remarks>
        Private _RxSensitivity As Int16
        Public Property RxSensitivity() As Int16
            Get
                Return _RxSensitivity
            End Get
            Set(ByVal value As Int16)
                _RxSensitivity = value
            End Set
        End Property


        Private _Observacion As String
        Public Property Observacion() As String
            Get
                Return _Observacion
            End Get
            Set(ByVal value As String)
                _Observacion = value
            End Set
        End Property


    End Class

#End Region



End Class
