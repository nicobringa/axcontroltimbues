﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorDobleBarreraFalsa
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.BARRERA1 = New Controles.CtrlBarrera()
        Me.BARRERA2 = New Controles.CtrlBarrera()
        Me.SuspendLayout()
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(450, 43)
        Me.lblNombre.TabIndex = 174
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(244, 40)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 212)
        Me.CtrlPanelNotificaciones1.TabIndex = 173
        '
        'BARRERA1
        '
        Me.BARRERA1.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.BARRERA1.BitMonitoreo = CType(0, Short)
        Me.BARRERA1.Estado = False
        Me.BARRERA1.GPIO = False
        Me.BARRERA1.ID_CONTROL_ACCESO = 0
        Me.BARRERA1.ID_PLC = 0
        Me.BARRERA1.ID_SECTOR = 0
        Me.BARRERA1.Inicializado = False
        Me.BARRERA1.Location = New System.Drawing.Point(81, 170)
        Me.BARRERA1.Name = "BARRERA1"
        Me.BARRERA1.patenteLeida = Nothing
        Me.BARRERA1.PLC = Nothing
        Me.BARRERA1.Size = New System.Drawing.Size(62, 52)
        Me.BARRERA1.TabIndex = 172
        Me.BARRERA1.tiempoPatenteLeidas = 0
        '
        'BARRERA2
        '
        Me.BARRERA2.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.BARRERA2.BitMonitoreo = CType(0, Short)
        Me.BARRERA2.Estado = False
        Me.BARRERA2.GPIO = False
        Me.BARRERA2.ID_CONTROL_ACCESO = 0
        Me.BARRERA2.ID_PLC = 0
        Me.BARRERA2.ID_SECTOR = 0
        Me.BARRERA2.Inicializado = False
        Me.BARRERA2.Location = New System.Drawing.Point(81, 112)
        Me.BARRERA2.Name = "BARRERA2"
        Me.BARRERA2.patenteLeida = Nothing
        Me.BARRERA2.PLC = Nothing
        Me.BARRERA2.Size = New System.Drawing.Size(62, 52)
        Me.BARRERA2.TabIndex = 175
        Me.BARRERA2.tiempoPatenteLeidas = 0
        '
        'CtrlSectorDobleBarreraFalsa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.BARRERA2)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.BARRERA1)
        Me.Name = "CtrlSectorDobleBarreraFalsa"
        Me.Size = New System.Drawing.Size(451, 254)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Public WithEvents BARRERA1 As CtrlBarrera
    Public WithEvents BARRERA2 As CtrlBarrera
End Class
