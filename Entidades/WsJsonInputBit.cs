﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entidades
{
    /// <summary>
    /// CLASE QUE CONTIENE LAS CLASES QUE REPRESENTAN LOS DATOS ENVIADOS A BIT EN 
    /// FORMATO JSON
    /// </summary>
    public class WsJsonInputBit
    {

        #region SILOS
        /// <summary>
        /// Clase que representa una alarma generada en el plc de automatización
        /// esta clase es enviada en formato JSON
        /// </summary>
        public class AlarmaSilo
        {
            public static string _NOMBRE = "AlarmaSilo";

            //{"idSilo":0,"idAlarma":0,"mensaje":null,"valor":0.0}
            /// <summary>
            /// Identificador único por cada silo de la planta
            /// </summary>
            ///
            public Int64 idSilo { get; set; }
            /// <summary>
            /// 1= Nivel Bajo en Silo, 2= Nivel Alto en Silo, ….. 
            /// </summary>
            public Int16 idAlarma { get; set; }
            /// <summary>
            /// Mensaje de la Alarma
            /// </summary>
            public string mensaje { get; set; }
            ///Valor de la variable que genera la alarma
            public double valor { get; set; }
            /// <summary>
            /// id registro DE INTERFACE creado
            /// </summary>
            public int idRegistroWS { get; set; }
        }


        /// <summary>
        /// Clase que representa la respuesta del comando enviado por BIT en el WS AltaModSilo
        /// </summary>
        public class RtaAltaModSilo
        {
            public static string _NOMBRE = "RtaAltaModSilo";
            /// <summary>
            /// Id generado por BIT. Utilizado por axControl para retornar la respuesta haciendo referencia al Id de comando generado por BIT
            /// </summary>
            public int idComandoRta { get; set; }
            /// <summary>
            /// 1= Procesado, 2=Error en axControl, 3=Error en PLC S71200, 4=Error en PCS7, 5=TimeOut 
            /// </summary>
            public int idEstadoRta { get; set; }
            /// <summary>
            /// Mensaje para describir el estado de la respuesta
            /// </summary>
            public string mensajeRta { get; set; }
            /// <summary>
            /// id generado en la base de datos de interface ws
            /// </summary>
            public int idRegistroWS { get; set; }

        }

        #endregion

        #region DESCARGA
        /// <summary>
        /// Evento generado por la automatización enviado a en formato json
        /// </summary>
        public class EventoOTDesc
        {
            public static string _NOMBRE = "EventoOTDesc";

            /// <summary>
            /// Número de OT
            /// </summary>
            public int nrOTEvento { get; set; }
            /// <summary>
            /// 1= Recepción de OT, 2= Activación de OT, 3= Inicio de OT, 4= Pausa de OT, 5=Fin de OT, ….
            /// </summary>
            public int idEvento { get; set; }
            /// <summary>
            /// Mensaje para describir el evento
            /// </summary>
            public string mensajeEvento { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al Peso del Batch
            /// </summary>
            public double valorEvento1 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 1
            /// </summary>
            public double valorEvento2 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 2
            /// </summary>
            public double valorEvento3 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 3
            /// </summary>
            public double valorEvento4 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 4
            /// </summary>
            public double valorEvento5 { get; set; }
            /// <summary>
            /// Id autoincremental generado por Aumax para que BIT verifique que no se dupliquen eventos.
            /// </summary>
            public int idComando { get; set; }
            /// <summary>
            /// id registro DE INTERFACE creado
            /// </summary>
            public int idRegistroWS { get; set; }
        }

        #endregion

        #region CARGA
        public class EventoOTCarga
        {
            /// <summary>
            /// Número de OT
            /// </summary>
            public int numOTEvento { get; set; }
            /// <summary>
            /// 1= Recepción de OT, 2= Activación de OT, 3= Inicio de OT, 4= Pausa de OT, 5=Fin de OT, 6=Fin de Batch, ….
            /// </summary>
            public int idEvento { get; set; }
            /// <summary>
            /// Mensaje para describir el evento
            /// </summary>
            public string mensajeEvento { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al Peso del Batch
            /// </summary>
            public double valorEvento1 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 1
            /// </summary>
            public double valorEvento2 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 2
            /// </summary>
            public double valorEvento3 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 3
            /// </summary>
            public double valorEvento4 { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Si es Fin de Batch(6), este valor corresponde al % de apertura del Origen 4
            /// </summary>
            public double valorEvento5 { get; set; }
            /// <summary>
            /// Id autoincremental generado por Aumax para que BIT verifique que no se dupliquen eventos.
            /// </summary>
            public int idComando { get; set; }

        }
        #endregion



    }
}