﻿Public Class FrmAuditoria

#Region "PROPIEDADES"
    Private sinFecha As DateTime = New DateTime(2000, 1, 1)
#End Region


#Region "CONSTRUCTOR"
    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Dim oAuditoriasN As New Negocio.AuditoriaN
        'Dim listAuditorias As List(Of Entidades.Auditoria) = oAuditoriasN.GetAllGeneral
        ''inicializo el combo acción en TODOS para que nunca vaya vacio
        cbAccion.SelectedIndex = 0
        ''Inicializo las fecha de tal manera que me muestre todo lo de hoy, si busco sin filtros
        dtpDesde.Value = DateTime.Now.Date
        dtpHasta.Value = DateTime.Now.Date.AddDays(1)
        ''Cargo todas las auditorias en la grilla
        'RefrescarGrilla(listAuditorias)
    End Sub
#End Region

#Region "EVENTOS FORM"
    ''' <summary>
    ''' Al filtrar envio los datos que tengo para refrescar la grilla
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnFiltrar_Click(sender As Object, e As EventArgs) Handles btnFiltrar.Click
        Dim oAuditoriasN As New Negocio.AuditoriaN
        Dim sector As Integer = 0
        Dim transaccion As Integer = 0

        ''si sector o transaccion estan vacios va cero, ya que el campo vacio genera error en los integer
        If txtSector.Text.Length > 0 Then sector = txtSector.Text
        If txtTransaccion.Text.Length > 0 Then transaccion = txtTransaccion.Text
        ''cargo la lista de auditorias con los resultados de la consulta con filtros
        Dim listAuditorias As List(Of Entidades.Auditoria) = oAuditoriasN.GetFiltro(dtpDesde.Value.Date, dtpHasta.Value.Date, txtTag.Text, sector, transaccion, cbAccion.SelectedIndex, txtDescripcion.Text)

        RefrescarGrilla(listAuditorias)
    End Sub

    ''' <summary>
    ''' Habilito/Deshabiito los campos de fecha para tomarse en el filtro
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub chkHabilitaFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkHabilitaFecha.CheckedChanged

        If dtpDesde.Enabled = False Then
            ''si esta habilitado por defecto toma la fecha de hoy
            dtpDesde.Value = DateTime.Now.Date
            dtpHasta.Value = DateTime.Now.Date.AddDays(1)
            dtpDesde.Enabled = True
            dtpHasta.Enabled = True
        Else
            ''si no está habilitado toma un fecha generica que la consulta de filtro ignorará
            dtpDesde.Value = sinFecha
            dtpHasta.Value = sinFecha
            dtpDesde.Enabled = False
            dtpHasta.Enabled = False
        End If


    End Sub
    ''' <summary>
    ''' Controlo que en los campos numericos solo se ingresen numeros
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtSector_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSector.KeyPress, txtTransaccion.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
#End Region


#Region "METODOS"
    ''' <summary>
    ''' Permite refrescar la grilla de Auditorias
    ''' </summary>
    Private Sub RefrescarGrilla(ByVal oAuditorias As List(Of Entidades.Auditoria))
        ''Limpio la grilla siempre al iniciar o filtrar
        dgAuditorias.Rows.Clear()
        ''solo si tengo items en la lista sigo
        If Not IsNothing(oAuditorias) Then
            ''recorro la lista inicial o filtrada
            For Each Auditorias As Entidades.Auditoria In oAuditorias
                ''agrego fila por fila los campos a la grilla
                dgAuditorias.Rows.Add(Auditorias.ID_AUDITORIA, Auditorias.DESCRIPCION, Auditorias.FECHA, Auditorias.SECTOR, Auditorias.ID_TRANSACCION, Auditorias.USUARIO, Auditorias.ACCION, Auditorias.TAG_RFID, Auditorias.ID_AUDITORIA_WS)
                Dim UltIndex As Integer = dgAuditorias.RowCount - 1
                dgAuditorias.Rows(UltIndex).Tag = Auditorias
            Next
        End If
    End Sub

#End Region

#Region "SUBPROCESOS"

#End Region



End Class