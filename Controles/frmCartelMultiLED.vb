﻿Imports Negocio
Imports Entidades
Imports System.IO.Ports
Imports System.Windows.Forms

Imports Entidades.Constante
Public Class frmCartelMultiLED
#Region "Variables de Instancia"
    Dim oCartel As Entidades.CARTEL_MULTILED
    Dim oCartelN As CartelMultiLedN
    Dim oldIp As String
    Dim CtrlCartel As New Controles.CtrlCartelMultiLED
#End Region
#Region "Eventos"
    Public Event NuevoMensaje(ByVal mensaje As String)
    Public Event Update_(ByVal cartel As Entidades.CARTEL_MULTILED)
#End Region

    Public Sub New(ByVal CtrlCartel As Controles.CtrlCartelMultiLED)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        CtrlCartel = CtrlCartel
        oCartelN = New CartelMultiLedN
        Me.oldIp = CtrlCartel.oCartel.IP
        oCartel = CtrlCartel.oCartel
        Consultar()
    End Sub


    Private Sub Consultar()

        txtIP.Text = Me.oCartel.IP
        txtPuerto.Text = Me.oCartel.PORT

    End Sub

    Public Sub SeleccionarItemCombo(ByVal CmbCombo As ComboBox, ByVal Id As Integer)
        For I As Integer = 0 To CmbCombo.Items.Count - 1
            CmbCombo.SelectedIndex = I
            If CType(CmbCombo.SelectedItem, ItemCombo).Id = Id Then
                I = CmbCombo.Items.Count - 1
            End If
        Next
    End Sub

    Private Sub btnEscribirCartel_Click(sender As System.Object, e As System.EventArgs) Handles btnEscribirCartel.Click

    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            GuardarDatos()
            RaiseEvent Update_(oCartel)
        Else
            Me.Close()
        End If
    End Sub

    Private Function GuardarDatos() As Boolean
        Try

            oCartel.IP = txtIP.Text
            oCartel.PORT = CInt(txtPuerto.Text)
            Dim descripAuditoria As String
            descripAuditoria = "Configuro cartel MultiLED: " + oCartel.IP
            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionCartel, 0)
            oCartel = oCartelN.Update_Config(oCartel)
            If oCartel.IP <> "" Then
                MsgBox("Configuración del Cartel actualizada correctamente", MsgBoxStyle.Information, "axControl")
                Return True
            Else
                MsgBox("La Configuración del Cartel no se pudo actualizar. Intente nuevamente o solicite asistencia técnica", MsgBoxStyle.Exclamation, "axControl")
                Return False
            End If
        Catch ex As Exception
            MsgBox("Los valores de Configuración del Cartel no corresponden. Revise e intente nuevamente.", MsgBoxStyle.Exclamation, "axControl")
            Return False
        Finally
            Me.Close()
        End Try

    End Function

End Class