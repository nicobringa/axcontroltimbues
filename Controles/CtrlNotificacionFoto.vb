﻿Public Class CtrlNotificacionFoto

    Public Event ClickCerrarNotificacion(ByVal ctrl As Control)

    Public Sub New(ByVal numBalanza As Int16, foto As Bitmap)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        Negocio.modDelegado.SetTextLabel(Me, lblFecha, DateTime.Now.ToString("hh:mm") + "Balanza " & numBalanza)

        Negocio.modDelegado.SetImagePicture(Me, pbFoto, foto)

    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        RaiseEvent ClickCerrarNotificacion(Me)

    End Sub



End Class
