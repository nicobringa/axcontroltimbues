﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class AxRespuesta
    {
        public long ID { get; set; }

        public int idSector { get; set; }

        public Boolean habilitado { get; set; }

        public string mensaje { get; set; }

        public long idTransaccion { get; set; }

        public long nroTurno { get; set; }

        public short operacion { get; set; }

        public String usuario { get; set; }

        public string tagRFID { get; set; }

        public int motivo { get; set; }

        public DatosDescarga datosDescarga { get; set; }




        public AxRespuesta()
        {

        }

        public AxRespuesta(int idSector, Boolean habilitado, string mensaje, long idTransaccion)
        {
            this.idSector = idSector;
            this.habilitado = habilitado;
            this.mensaje = mensaje;
            this.idTransaccion = idTransaccion;
        }




        public override string ToString()
        {
            return " Habilitacion : " + habilitado + "\n" +
                    " Mensaje: " + mensaje + "\n" +
                    " idTrasaccion: " + idTransaccion + "\n" +
                    "idSector: " + idSector + "\n";


        }

        public enum ID_MOTIVO
        {
            /// <summary>
            /// El camión se encuentra dado de alta en el circuito  
            /// pero esta mal posicionado (calle o sector)
            /// </summary>
            MAL_POSICIONADO = 2,
            /// <summary>
            /// EL tag del camión no esta dado de alta en el circuito.
            /// </summary>
            TAG_NO_EXISTE = 1
        }

    }
}
