﻿Public Class ConfRFID

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    ''' <summary>
    ''' Coleccion de antena del dispositivo
    ''' </summary>
    ''' <remarks></remarks>
    Public ListAntenas As New List(Of Antenas)

    ''' <summary>
    ''' Ip del lector que se va a guar en el archivo xml
    ''' </summary>
    ''' <remarks></remarks>
    Private _IpAddress As String
    Public Property IpAddress() As String
        Get
            Return _IpAddress
        End Get
        Set(ByVal value As String)
            _IpAddress = value
        End Set
    End Property

    Private _SelectReaderMode As Integer
    Public Property SelectReaderMode() As Integer
        Get
            Return _SelectReaderMode
        End Get
        Set(ByVal value As Integer)
            _SelectReaderMode = value
        End Set
    End Property

    Private _SelectSearch As Integer
    Public Property SelectSearch() As Integer
        Get
            Return _SelectSearch
        End Get
        Set(ByVal value As Integer)
            _SelectSearch = value
        End Set
    End Property

    Private _SelectSession As Integer
    Public Property SelectSession() As Integer
        Get
            Return _SelectSession
        End Get
        Set(ByVal value As Integer)
            _SelectSession = value
        End Set
    End Property

    Private _SelectReportMode As Integer
    Public Property SelectReportMode() As Integer
        Get
            Return _SelectReportMode
        End Get
        Set(ByVal value As Integer)
            _SelectReportMode = value
        End Set
    End Property

    ''' <summary>
    ''' Clase de antenas
    ''' </summary>
    ''' <remarks></remarks>
    Public Class Antenas

        ''' <summary>
        ''' establecer o obtener el  Numero de antena
        ''' </summary>
        ''' <remarks></remarks>
        Private _NumeroAntena As Int16
        Public Property NumeroAntena() As Int16
            Get
                Return _NumeroAntena
            End Get
            Set(ByVal value As Int16)
                _NumeroAntena = value
            End Set
        End Property

        ''' <summary>
        ''' Permite obtener o establecer el power de la antena
        ''' </summary>
        ''' <remarks></remarks>
        Private _TxPower As Int16
        Public Property TxPower As Int16
            Get
                Return _TxPower
            End Get
            Set(ByVal value As Int16)
                _TxPower = value
            End Set
        End Property

        ''' <summary>
        ''' Permite obtener o establecer la sensibilidad de la antena
        ''' </summary>
        ''' <remarks></remarks>
        Private _RxSensitivity As Int16
        Public Property RxSensitivity() As Int16
            Get
                Return _RxSensitivity
            End Get
            Set(ByVal value As Int16)
                _RxSensitivity = value
            End Set
        End Property


        Private _Observacion As String
        Public Property Observacion() As String
            Get
                Return _Observacion
            End Get
            Set(ByVal value As String)
                _Observacion = value
            End Set
        End Property


    End Class

End Class
