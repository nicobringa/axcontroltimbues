﻿Public Class CtrlMenu


    Public Event ClickMenu()
    Public Event ClickDriverPLC()





    Private _HabilitarScroll As Boolean

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()




    End Sub

    Public Sub inicializar()

        'Negocio.modDelegado.SetTextLabel(Me, Negocio.ModSesion.PUESTO_TRABAJO.nombre, lblComputadora, Color.Black)

        'Negocio.modDelegado.SetTextLabel(Me, Entidades.Constante.VERSION, lblVersion, Color.Black)

        'Dim strTipoPuestoTrabajo As String = If(Negocio.PUESTO_TRABAJO.servidor, "Servidor", "Cliente")
        'Negocio.modDelegado.SetTextLabel(Me, strTipoPuestoTrabajo, lblTipoPuestoTrabajo, Color.Black)

    End Sub


    Public Property HabilitarScroll() As Boolean
        Get
            Return _HabilitarScroll
        End Get
        Set(ByVal value As Boolean)
            _HabilitarScroll = value

        End Set
    End Property



    Private Sub BtnMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu.Click

        RaiseEvent ClickMenu()
    End Sub

   
    Private Sub btnDriverPLC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDriverPLC.Click

        RaiseEvent ClickDriverPLC()

    End Sub

    Private Sub pbAumax_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbAumax.Click
        Dim proceso As New System.Diagnostics.Process
        With proceso
            .StartInfo.FileName = "http://www.aumax.com.ar/"
            .Start()
        End With
    End Sub

    Private Sub pbFacebook_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbFacebook.Click
        Dim proceso As New System.Diagnostics.Process
        With proceso
            .StartInfo.FileName = "https://www.facebook.com/AUMAXSRLvm"
            .Start()
        End With
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If MessageBox.Show("¿Desea cerrar la aplicación?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Application.ExitThread()
        End If

    End Sub


End Class
