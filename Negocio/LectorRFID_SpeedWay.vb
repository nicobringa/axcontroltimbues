﻿Imports Impinj.OctaneSdk
Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades
Imports System.Drawing
Imports Entidades.Constante
Imports System.Threading

Public Class LectorRFID_SpeedWay : Inherits ImpinjReader '  Hereda de la clase del fabricante Impinj




#Region "Propiedades"

    Dim DireccionArchivo As String = My.Application.Info.DirectoryPath 'Entidades.Constante.CARPETA_CONFIGURACION

    Public Shared COLOR_DESCONECTADO As Color = Color.Red
    Public Shared COLOR_CONECTADO As Color = Color.LimeGreen
    Public Shared COLOR_SIN_CONFIGURACION As Color = Color.LightYellow


    Public Shared COLOR_START As Color = Color.LightBlue
    Public Shared COLOR_STOP As Color = Color.Red

    Public Shared TEXT_CONECTADO As String = "Conectado"
    Public Shared TEXT_DESCONECTADO As String = "Desconectado"
    Public Shared TEXT_SIN_CONFIGURACION As String = "Sin Configuración"
    Public Shared TEXT_START As String = "Start"
    Public Shared TEXT_STOP As String = "Stop"

    Public reader As New ImpinjReader()
    Private _Nombre As String

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property


    Private _Ip As String
    Public Property Ip() As String
        Get
            Return _Ip
        End Get
        Set(ByVal value As String)
            _Ip = value
        End Set
    End Property

    Private _LectorRFID As Entidades.LECTOR_RFID
    Public Property LectorRFID() As Entidades.LECTOR_RFID
        Get
            Return _LectorRFID
        End Get
        Set(ByVal value As Entidades.LECTOR_RFID)
            _LectorRFID = value
        End Set
    End Property


    Private _LectorConfigurado As Boolean
    Public Property LectorConfigurado() As Boolean
        Get
            Return _LectorConfigurado
        End Get
        Set(ByVal value As Boolean)
            _LectorConfigurado = value
        End Set
    End Property


    Private _IsStar As Boolean
    Public Property IsStar() As Boolean
        Get
            Return _IsStar
        End Get
        Set(ByVal value As Boolean)
            _IsStar = value
        End Set
    End Property


    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Private hReconexion As Thread


#End Region

#Region "Enum"

    ''' <summary>
    ''' Los tipos de estado de conexion que tiene el lectOr RFID
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum EstadoConexion
        Conectado = 1
        Desconectado = 2
        ''' <summary>
        ''' Es cuando no se ah configurado el lector
        ''' </summary>
        ''' <remarks></remarks>
        SinConfiguracion = 3
    End Enum

    ''' <summary>
    ''' Estado de lectura del lector RFID
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum EstadoLectura
        Start = 1
        xStop = 2
    End Enum



#End Region

#Region "Mis Eventos"
    ''' <summary>
    ''' Evento que se ejecuta cuando una antena del lector leyo un TAG
    ''' </summary>
    ''' <param name="NumTag">Numer de tag o cod que contiene el tag</param>
    ''' <param name="NumAntena">Antena que leyo el TAG</param>
    ''' <param name="NombreLector">Nombre del lector RFID</param>
    ''' <remarks></remarks>
    Public Event TagLeido(ByVal NumTag As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, Manual As Boolean)

    Public Event ErrorRFID(Msj As String, SpeedWarRFID As LectorRFID_SpeedWay, idSector As Integer)

    Public Event InfoRFID(Msj As String, SpeedWarRFID As LectorRFID_SpeedWay, idSector As Integer)

    Public Event EventGPIO(Puerto As Integer, Estado As Boolean, SpeedWarRFID As LectorRFID_SpeedWay)

    Private WithEvents _LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay
#End Region

#Region "Contructor"

    ''' <summary>
    '''Crear un LECTOR RFID PARA PODER TRABAJAR 
    ''' Compatible con lectores SpeedWay 420 y 220 IMPINJ
    ''' </summary>
    ''' <param name="oLectorRFID"></param>
    ''' <remarks></remarks>


    Public Sub New(ByVal oLectorRFID As Entidades.LECTOR_RFID)




        If Not IsNothing(oLectorRFID) Then ' si encontro una configuración

            'Paso lo datos al objeto

            Me.Ip = oLectorRFID.IP
            Me.Nombre = oLectorRFID.NOMBRE
            Me.LectorRFID = oLectorRFID


            LectorConfigurado = True
            ' Me.Connect(oLectorConf.IP) ' M Conecto

        Else 'No tiene un configuración establecida el lector

            'Throw New Exception("No tiene un configuración incial")
            LectorConfigurado = False

        End If



    End Sub
#End Region

#Region "Metodos"

    Public Function GuardarConf() As Boolean


        Me.LectorRFID.NOMBRE = Me.Nombre
        Me.LectorRFID.IP = Me.Ip

        Me.LectorRFID.DESCRIPCION = Me.Descripcion

        Dim nLectorRFID As New Negocio.LectorRFID_N

        Return nLectorRFID.Update(LectorRFID)

    End Function


    Public Sub Conectar()

        If Not LectorConfigurado Then Throw New Exception("El lector no se encuentra configurado para conectarse")
        Me.Connect(Me.Ip) ' M Conecto
        Dim nLector As New Negocio.LectorRFID_N

        Dim oLector As Entidades.LECTOR_RFID = nLector.GetOne(Me.Ip)
        oLector.CONECTADO = True
        nLector.Update(oLector)


    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AplicarConfiguracion(Conf As Entidades.CONFIG_LECTOR_RFID)

        Me.StopRead()



        Dim readerSettings As Settings = Me.QueryDefaultSettings

        readerSettings.ReaderMode = Conf.SELECT_READER_MODE
        readerSettings.SearchMode = Conf.SELECT_SEARCH
        readerSettings.Session = Conf.SELECT_SESSION
        readerSettings.Report.Mode = Conf.SELECT_REPORT_MODE
        readerSettings.Report.IncludePeakRssi = True

        'Deshabilito todas las atenas
        readerSettings.Antennas.DisableAll()

        'Habilito la tenas correspondiente y seteo su power y sensibilidad
        For Each antena As Entidades.ANTENAS_RFID In Conf.ANTENAS_RFID
            readerSettings.Antennas.GetAntenna(antena.NUM_ANTENA).IsEnabled = True
            readerSettings.Antennas.GetAntenna(antena.NUM_ANTENA).TxPowerInDbm = antena.TX_POWER
            readerSettings.Antennas.GetAntenna(antena.NUM_ANTENA).RxSensitivityInDbm = antena.RX_SENSITIVITY
        Next
        readerSettings.Report.IncludeAntennaPortNumber = True

        '#################PARA PODER LEER EL LUGAR DE MEMORIA TID#######################
        'Crear una operación de leer la etiqueta para la memoria TID .
        Dim ReadTID As New TagReadOp
        'Leer desde la memoria TID
        ReadTID.MemoryBank = MemoryBank.Tid
        ' Leer 3 ( 16 - bit) palabras
        ReadTID.WordCount = 6  'Cantidad de palabras a leer en la memoria TID 1 PALABLA = 4 CARACTERES
        ' A partir de que palabra empieza a leer
        ReadTID.WordPointer = 0
        'Añadir estas operaciones al lector como optimizados Leer ops .
        'Optimizados Leer ops se aplican a todas las etiquetas , a diferencia
        ' Tag Operación secuencias, que se puede aplicar a las etiquetas específicas.
        ' Revolución Speedway soporta hasta dos operaciones de lectura Optimizado .
        readerSettings.Report.OptimizedReadOps.Add(ReadTID)

        readerSettings.Keepalives.Enabled = True
        readerSettings.Keepalives.PeriodInMs = 6000 '3000

        '' Habilitar el modo de monitor de enlace.
        '' Si nuestra aplicación no responde a
        ''cinco mensajes de actividad consecutivos ,
        ''El lector se cerrará la conexión de red .
        readerSettings.Keepalives.EnableLinkMonitorMode = True
        readerSettings.Keepalives.LinkDownThreshold = 10 '5

        'If Conf.GPIO_HABILITADO Then 'Si el gpio esta habilitado

        '    readerSettings.Gpos.GetGpo(1).Mode = GpoMode.Normal
        '    readerSettings.Gpos.GetGpo(2).Mode = GpoMode.Normal
        '    readerSettings.Gpos.GetGpo(3).Mode = GpoMode.Normal
        '    readerSettings.Gpos.GetGpo(4).Mode = GpoMode.Normal
        '    AddHandler Me.GpiChanged, AddressOf ControlarGPIO

        'End If

        ''Si el tipo de lectura es por corte de espira, activo el GPI sino no hace falta
        If LectorRFID.TIPO_LECTURA = Convert.ToInt16(Constante.TipoLectura.Corte_Espira) Then
            'Me.Start()
            'readerSettings.Gpis.GetGpi(1).IsEnabled = True
            'readerSettings.Gpis.GetGpi(1).DebounceInMs = LectorRFID.TIEMPO_LECTURA * 1000
            'readerSettings.AutoStart.Mode = AutoStartMode.GpiTrigger
            'readerSettings.AutoStart.GpiPortNumber = 1
            ''Le digo que va a iniciar (autoStart) cuando el corte del sensor sea false
            'readerSettings.AutoStart.GpiLevel = False
            'readerSettings.AutoStop.Mode = AutoStopMode.GpiTrigger
            'readerSettings.AutoStop.GpiPortNumber = 1
            ''Le digo que va a iniciar (autoStop) cuando el corte del sensor sea true
            'readerSettings.AutoStop.GpiLevel = True


            AddHandler Me.GpiChanged, AddressOf ControlarGPIO
            ''Apenas conecto llamo a la función que dispara el evento de estado

        End If

        'Aplico los ajustes
        Me.ApplySettings(readerSettings)

    End Sub


    ''' <summary>
    ''' Permite que el lector comience a leer
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StartRead()
        If Not isRunning() Then
            Me.Start()
            Me.IsStar = True
        End If
    End Sub

    ''' <summary>
    ''' Permite que el lector pare la lectura
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StopRead()
        If isRunning() Then
            Me.Stop()
            Me.IsStar = False
        End If
    End Sub

    ''' <summary>
    ''' Obtiene el estado de lectura del lector
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isRunning() As Boolean
        Dim Status As Status = Me.QueryStatus() ' Obtengo el estado del lector
        Return Status.IsSingulating()
    End Function

    Private Sub Iniciar()
        If Me.LectorRFID.AUTO_CONECTARSE Then
            'Esta configurado para AUTO_CONECTARSE
            Dim hConectarse As New Threading.Thread(AddressOf Conectarse)
            hConectarse.IsBackground = True
            hConectarse.Start()
        End If
    End Sub

    Public Function Conectarse(Optional intReco As Integer = 0) As Integer

        Try

            If Not Me.IsConnected Then
                If My.Computer.Network.Ping(Me.Ip) Then
                    Conectar()
                    Me.AplicarConfiguracion(Me.LectorRFID.CONFIG_LECTOR_RFID(0))

                    If Me.LectorRFID.AUTO_START Then

                        If (LectorRFID.TIPO_LECTURA = Convert.ToInt16(Constante.TipoLectura.Constante)) Then
                            Me.Start()
                        Else
                            Dim LectorEstado As Status = Me.QueryStatus
                            ''Busco los puertos GPI del lector actual
                            For Each itemGpio As GpiStatus In LectorEstado.Gpis
                                ''Me paro en el que tenga puerto nuemro 1
                                If (itemGpio.PortNumber = 1) Then
                                    ''Disparo el evento que ejecuta ControlGPIO
                                    RaiseEvent EventGPIO(1, itemGpio.State, Me)
                                    ''Si está en false inicia la lectura
                                    If (itemGpio.State = False) Then
                                        Me.Start()
                                    Else
                                        Me.Stop()
                                    End If
                                    ''Actualizo dinamismo
                                    ActualizarEstadoSensorLectura(itemGpio.State)
                                End If

                            Next
                        End If


                    End If
                    If Not IsNothing(hReconexion) Then
                        If hReconexion.IsAlive Then hReconexion.Abort()
                        hReconexion = Nothing
                    End If
                    Return 1
                Else
                    If (intReco = 0) Then
                        RaiseEvent ErrorRFID("El lector no se encuentra en la red. IP: " + Me.LectorRFID.IP, Me, Me.LectorRFID.ID_LECTOR_RFID)
                    End If

                    Return 0
                End If
            End If
        Catch ex As Exception

            Dim nSector As New Negocio.SectorN
            Dim oSector As SECTOR = nSector.GetOne(Me.LectorRFID.ID_LECTOR_RFID)

            If Not IsNothing(oSector) Then
                RaiseEvent ErrorRFID("Error al conectarse con lector. IP: " + Me.LectorRFID.IP + ". Sector: " + oSector.DESCRIPCION, Me, Me.LectorRFID.ID_LECTOR_RFID)
            Else
                RaiseEvent ErrorRFID("Error al conectarse con lector. IP: " + Me.LectorRFID.IP, Me, Me.LectorRFID.ID_LECTOR_RFID)
            End If


            If IsNothing(hReconexion) Then
                hReconexion = New Thread(AddressOf ReconectarLector)
                hReconexion.IsBackground = True
            End If



            If hReconexion.IsAlive = False Then
                hReconexion.Start()  'Lo comento porque Me genera Error cuando inicio el programa NFB
            End If

            Return 0
        End Try

    End Function



    ''' <summary>
    ''' Permite reconectar el lector 
    ''' </summary>
    Private Sub ReconectarLector()
        Dim intento As Integer = 0
        Dim reconexion As Integer = 0

        RaiseEvent InfoRFID(String.Format("Reconectar lector RFID: {0}({1})", Me.Name, Me.Ip), Me, Me.LectorRFID.ID_LECTOR_RFID)

        While Not Me.IsConnected ' Mientra no este conectado

            If Conectarse(1) Then

                ' ModificarAlarma(LectorRFID.ID_CONTROL_ACCESO, 1, LectorRFID.ID_CONTROL_ACCESO, LectorRFID.NOMBRE)
                RaiseEvent InfoRFID(String.Format("Se establecio la conexión con el lector RFID {0}-({1})", Me.Nombre, Me.Ip), Me, Me.LectorRFID.ID_LECTOR_RFID)
            Else
                intento += 1
                If intento < 30 Then
                    reconexion = 1000 * intento
                Else

                    'GenerarAlarma("El lector no se encuentra conectado", LectorRFID.ID_CONTROL_ACCESO, LectorRFID.ID_CONTROL_ACCESO, LectorRFID.NOMBRE)
                    reconexion = 30000
                End If

                If intento = 1 Then
                    RaiseEvent ErrorRFID(String.Format("No se pudo establecer conexión con el lector RFID: {0} intento: {1}. Reconexión en {2} seg",
                                 Me.Ip, intento, reconexion / 1000), Me, Me.LectorRFID.ID_LECTOR_RFID)
                End If

            End If

            Threading.Thread.Sleep(reconexion)
        End While


    End Sub


    'Public Overridable Sub GenerarAlarma(detalle As String, idControlAcceso As Integer, idSector As Integer, objeto As String)
    '    Dim nAlarmas As New Negocio.AlarmaN

    '    'Si existe la alarma no la genera de vuelta
    '    If nAlarmas.ExisteAlarma(idSector, idControlAcceso, objeto, detalle) Then Return
    '    Dim oAlarma As New Entidades.Alarma

    '    oAlarma.activa = True
    '    oAlarma.reconocida = False
    '    oAlarma.fechaAparicion = DateTime.Now
    '    oAlarma.tag = Name
    '    oAlarma.idSector = idSector
    '    oAlarma.idControlAcceso = idControlAcceso
    '    oAlarma.detalle = detalle

    '    nAlarmas.Guardar(oAlarma)
    'End Sub
    ''' <summary>
    ''' Permite modificar el estado de una alarma
    ''' </summary>
    ''' <param name="idControlAcceso"></param>
    ''' <param name="tipoFalla"></param>
    ''' <param name="idSector"></param>
    ''' <param name="objeto"></param>
    'Public Overridable Sub ModificarAlarma(idControlAcceso As Integer, tipoFalla As Integer, idSector As Integer, objeto As String)
    '    Dim nAlarmas As New Negocio.AlarmaN
    '    Dim oAlarma As New Entidades.Alarma
    '    Dim idAlarma As Int32
    '    idAlarma = nAlarmas.BuscarIdAlarma(idSector, idControlAcceso, tipoFalla, objeto)
    '    If idAlarma > 0 Then
    '        oAlarma = nAlarmas.GetOne(idAlarma)

    '        oAlarma.activa = False
    '        oAlarma.fechaDesaparicion = DateTime.Now

    '        nAlarmas.Guardar(oAlarma)
    '    End If
    'End Sub

    ''' <summary>
    ''' Permite consultar el estado de todos los puertos.
    ''' Si uno de los puerto esta en true empieza la lectura y detiene la lectura si todos los puertos estan en false
    ''' Si es true o false puede cambiar dependiendo si el sensor es normal cerrado o abierto
    ''' </summary>
    Public Sub ConsultarSensoresLectura(TipoConexion As TipoConexion)

        If Not Me.IsConnected Then Return 'Si no esta conectado termino el procedimiento

        'Verifico el estado de los demas puertos para saber si tengo que parar la lectura
        'Obtengo el estao del lector
        Dim Status As Status = Me.QueryStatus()
        'Bandera para saber si tengo que parar la lectura
        Dim StopRead As Boolean = True

        Console.WriteLine("############# ESTADO PUERTO #############")
        Console.WriteLine("")
        For Each itemGpi As GpiStatus In Status.Gpis 'Recorro los puertos del lector
            Console.WriteLine(String.Format("Puerto:{0}|Estado: {1}", itemGpi.PortNumber, itemGpi.State))
            Dim estadoNormal As Boolean = If(TipoConexion = TipoConexion.NormalAbierto, False, True)
            'Si esta en True
            If itemGpi.State <> estadoNormal Then
                Dim PuertoHabilitado As Boolean = False
                'Consulto si el puerto esta habilitado en la configuracion para trabajar
                Select Case itemGpi.PortNumber
                    Case 1
                        PuertoHabilitado = Me.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO1
                    Case 2
                        PuertoHabilitado = Me.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO2
                    Case 3
                        PuertoHabilitado = Me.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO3
                    Case 4
                        PuertoHabilitado = Me.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO4
                End Select

                If PuertoHabilitado Then 'Si el puerto esta habilitado
                    'Si el puerto esta habilitado para trabajar y el estado del puerto es True NO PARO LA LECTURA
                    StopRead = False
                End If
            End If

        Next
        Console.WriteLine("StopRead = " & StopRead)

        If StopRead Then Me.StopRead() Else Me.StartRead()

    End Sub

    Public Function GetOneEtadoSensor(numPuerto As Integer) As Boolean

        If Me.IsConnected Then
            'Obtengo el estao del lector
            Dim Status As Status = Me.QueryStatus()

            For Each itemGpi As GpiStatus In Status.Gpis 'Recorro los puertos del lector

                If itemGpi.PortNumber = numPuerto Then
                    Return itemGpi.State
                End If

            Next
        End If

        Return False
    End Function

#End Region

#Region "Eventos SDK"

    Private Sub Lector_ReporteTag(ByVal sender As ImpinjReader, ByVal report As TagOpReport) Handles Me.TagOpComplete

        For Each result As Impinj.OctaneSdk.TagOpResult In report ' Recorro todos los reportes de tag leidos
            Try

                If TypeOf result Is TagReadOpResult Then

                    Dim ReadResult As TagReadOpResult = result ' Resultado de los leidos

                    Dim NumAntena As Int16 = ReadResult.Tag.AntennaPortNumber 'Obtengo la antena que leyo el tag
                    'Dim NumTag As String = ReadResult.Data.ToHexString() ' Obtengo el numero de tag leidos
                    Dim epc As String = ReadResult.Tag.Epc.ToHexString()
                    Dim TID As String = ReadResult.Data.ToHexString()
                    Dim rssiFiltro As Integer = Me.LectorRFID.CONFIG_LECTOR_RFID(0).RSSI

                    If rssiFiltro <> 0 Then
                        'Ingoro el tag que el rssi no es mayor o igual  al rssi permitido
                        If Not ReadResult.Tag.PeakRssiInDbm >= rssiFiltro Then Return
                    End If
                    Dim db As New Entidades.AxControlORAEntities
                    Dim filtro = (From f In db.CONFIGURACION Select f).SingleOrDefault



                    If TID.Length > 0 Then ' And TID.Length > 0 Then
                        Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                        nUltTagLeido.Guardar(TID, Me.LectorRFID.ID_CONTROL_ACCESO)
                        ''If epc.Contains(filtro.FILTRO_EPC) Then
                        RaiseEvent TagLeido(TID, NumAntena, Me.LectorRFID, False)

                        ''End If
                    End If
                End If

            Catch ex As Exception

            End Try
        Next



    End Sub

    Private Sub Lector_ReporteTag(ByVal sender As ImpinjReader, ByVal report As TagReport) Handles Me.TagsReported


    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando se perdio la conexión
    ''' </summary>
    Private Sub OnConnectionLost() Handles Me.ConnectionLost
        Me.Disconnect()
        RaiseEvent ErrorRFID("Se perdio la conexión con el lector RFID :" & Me.Nombre, Me, Me.LectorRFID.ID_LECTOR_RFID)

        If Me.LectorRFID.AUTO_CONECTARSE Then ReconectarLector()


    End Sub

    'agrego hilo que controla el estado del sensor GPIO
    Public Sub ControlarGPIO(ByVal sender As ImpinjReader, ByVal e As GpiEvent)
        Try
            Console.WriteLine(String.Format("Puerto:{0}|Estado: {1}", e.PortNumber, e.State))

            RaiseEvent EventGPIO(e.PortNumber, e.State, Me)

            ''Si está en false inicia la lectura
            If (e.State = False) Then
                Try
                    Me.Start()
                Catch ex As Exception
                    Me.StartRead()
                End Try
            Else
                Me.Stop()
            End If
            ''Esta funcion sirve para actualizar el estado de la espira en BD para el dinamismo
            ActualizarEstadoSensorLectura(e.State)
        Catch ex As Exception

        End Try


    End Sub


    Public Sub ActualizarEstadoSensorLectura(ByVal state As Boolean)
        Dim nDatoWord As New Negocio.DatoWordIntN
        Dim oDato As New DATO_WORDINT

        oDato = nDatoWord.GetOne(Convert.ToInt16(Me.LectorRFID.ID_LECTOR_RFID), "S_LECTURA.Estado")
        If Not IsNothing(oDato) Then

            If state = True Then
                ''Si el estado es TRUE no está cortando, y no inicia la lectura
                nDatoWord.Escribir(oDato.ID, Convert.ToInt16(Constante.EstadoSensor.SensorSinPresencia))
            Else
                ''Si el estado es FALSE está cortando e inicia la lectura
                nDatoWord.Escribir(oDato.ID, Convert.ToInt16(Constante.EstadoSensor.SensorConPresencia))
            End If
        End If
    End Sub


    Public Sub ProgramEpc(ByVal currentEpc As String, ByVal newEpc As String)

        If (currentEpc.Length Mod 4 <> 0) OrElse (newEpc.Length Mod 4 <> 0) Then Throw New Exception("EPCs must be a multiple of 16 bits (4 hex chars)")
        Console.WriteLine("Adding a write operation to change the EPC from :")
        Console.WriteLine("{0} to {1}" & vbLf, currentEpc, newEpc)
        Dim seq As TagOpSequence = New TagOpSequence()
        seq.TargetTag.MemoryBank = MemoryBank.Epc
        seq.TargetTag.BitPointer = BitPointers.Epc
        seq.TargetTag.Data = currentEpc
        Dim writeEpc As TagWriteOp = New TagWriteOp()
        writeEpc.Id = 123
        writeEpc.MemoryBank = MemoryBank.Epc
        writeEpc.Data = TagData.FromHexString(newEpc)
        writeEpc.WordPointer = WordPointers.Epc
        seq.Ops.Add(writeEpc)

        Me.AddOpSequence(seq)
    End Sub





#End Region








End Class
