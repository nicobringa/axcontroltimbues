﻿Imports AccesoDatos
Imports Entidades

Public Class DatoWordIntN

    Public Function Add(ByVal oDato As DATO_WORDINT) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.add(oDato)
    End Function

    Public Function Delete(ByVal oDato As DATO_WORDINT) As Boolean
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.delete(oDato)
    End Function

    Public Function Update(ByVal oDato As DATO_WORDINT) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.update(oDato)
    End Function

    Public Function GetOne(ByVal id As Integer) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getOne(id)
    End Function

    Public Function GetOneTagIdSector(tag As String, idSector As Integer) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getOneTagIdSector(tag, idSector)
    End Function

    Public Function GetAllPorSector(ByVal idSector As Integer, nroDB As Integer) As List(Of DATO_WORDINT)
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getAllPorSector(idSector, nroDB)
    End Function

    Public Function GetAllPorSector(ByVal idSector As Integer) As List(Of DATO_WORDINT)
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getAllPorSector(idSector)
    End Function

    Public Function GetOne(ByVal idSector As Integer, ByVal tag As String) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getOne(idSector, tag)
    End Function

    Public Function GetAll() As List(Of DATO_WORDINT)
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getAll
    End Function

    Public Function GetAll(ByVal idPLC As Integer) As List(Of DATO_WORDINT)
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getAll(idPLC)
    End Function

    Public Function Leer(ByVal idTAG As Integer) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.leer(idTAG)
    End Function

    Public Function Escribir(ByVal idTAG As Integer, ByVal valor As Integer) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.escribir(idTAG, valor)
    End Function
    Public Function Escribir(oDato As DATO_WORDINT) As DATO_WORDINT
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.escribir(oDato.ID, oDato.VALOR)
    End Function

    Public Function GetAllTags(ByVal idPLC As Integer, ByVal nombreTag As String, ByVal idSector As Integer) As List(Of DATO_WORDINT)
        Dim oDatoAD As New DatoWordIntAD
        Return oDatoAD.getAllTags(idPLC, nombreTag, idSector)
    End Function
End Class
