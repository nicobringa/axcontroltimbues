﻿Imports Negocio

Imports Entidades.Constante
Public Class CtrlDinamismoCalado

    ''' <summary>
    ''' Dinamismo de sensores con los estados del PLC
    ''' </summary>
    ''' <param name="_SensorEstado"></param>
    Public Sub SetSensorEstado(ByVal _SensorEstado As Integer)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing

        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picSLecturaNoCortando
            picNoVisible = picSLecturaCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picSLecturaCortando
            picNoVisible = picSLecturaNoCortando
        End If




        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        Else
            'Oculto las dos barreras
            picVisible = picSLecturaNoCortando
            picNoVisible = picSLecturaCortando

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub



End Class
