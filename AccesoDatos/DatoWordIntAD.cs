﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class DatoWordIntAD
    {
        public DATO_WORDINT add(DATO_WORDINT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    oContext.DATO_WORDINT.Add(oDato);
                    oContext.SaveChanges();
                    return oDato;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }

        public bool delete(DATO_WORDINT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.ID == oDato.ID).FirstOrDefault();
                    if (oConsulta != null)
                    {
                        oContext.DATO_WORDINT.Remove(oConsulta);
                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DATO_WORDINT update(DATO_WORDINT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_WORDINT
                                     where tabla.ID == oDato.ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FK_PLC = oDato.FK_PLC;
                        oConsulta.FLAG_RW = oDato.FLAG_RW;
                        oConsulta.NUM_WORD = oDato.NUM_WORD;
                        oConsulta.NUM_DB = oDato.NUM_DB;
                        oConsulta.QC = oDato.QC;
                        oConsulta.TAG = oDato.TAG;
                        oConsulta.VALOR = oDato.VALOR;

                        oContext.SaveChanges();
                        return oDato;
                    }
                    else
                    {
                        return new DATO_WORDINT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }

        public DATO_WORDINT getOne(int ID)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.ID == ID).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }

        public DATO_WORDINT getOneTagIdSector(string tag, int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.TAG == tag && a.ID_SECTOR == idSector).SingleOrDefault();
                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }

        public DATO_WORDINT getOne(int idSector, string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.TAG == tag && a.ID_SECTOR == idSector).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }

        public List<DATO_WORDINT> getAll()
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_WORDINT>();
            }
        }

        public List<DATO_WORDINT> getAll(int idplc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.FK_PLC == idplc).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_WORDINT>();
            }
        }

        public List<DATO_WORDINT> getAllPorSector(int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_WORDINT>();
            }
        }

        public List<DATO_WORDINT> getAllPorSector(int idSector, int nroDB)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.NUM_DB == nroDB).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_WORDINT>();
            }
        }

        public DATO_WORDINT leer(int ID)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.ID == ID).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        if (oConsulta.FLAG_RW == true)
                        {
                            return oConsulta;
                        }
                        else
                        {
                            return new DATO_WORDINT();
                        }
                    }
                    else
                    {
                        return new DATO_WORDINT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }

        public DATO_WORDINT escribir(int ID, int valor)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_WORDINT
                                     where tabla.ID == ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FLAG_RW = true;
                        oConsulta.VALOR = valor;

                        oContext.SaveChanges();
                        return oConsulta;

                    }
                    else
                    {
                        return new DATO_WORDINT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }


        }

        public List<DATO_WORDINT> getAllTags(int idplc, string nombreTag, int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.FK_PLC == idplc && a.TAG.Contains(nombreTag) && a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_WORDINT>();
            }
        }

        public DATO_WORDINT getOneTags(int idSector, string extension)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.TAG.Contains(extension) && a.ID_SECTOR == idSector).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }


        public DATO_WORDINT getOneTagsBarrera(int idSector, string extension, string extension2)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && (a.TAG.Contains(extension) || a.TAG.Contains(extension2))).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }
        public DATO_WORDINT getOneTagsCompleto(int idSector, string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_WORDINT.Where(a => a.TAG == tag && a.ID_SECTOR == idSector).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_WORDINT();
            }
        }
    }
}
