﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class ModeloBalanzaAD
    {
        public List<ModeloBalanza> getAll()
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.ModelosBalanza.OrderByDescending(a => a.id).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<ModeloBalanza>();
            }
        }
    }
}
