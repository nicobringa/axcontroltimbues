﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorInterfaceDescarga
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabInterface = New System.Windows.Forms.TabControl()
        Me.TabLog = New System.Windows.Forms.TabPage()
        Me.dgLog = New System.Windows.Forms.DataGridView()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colLog = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.TabInterface.SuspendLayout()
        Me.TabLog.SuspendLayout()
        CType(Me.dgLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabInterface
        '
        Me.TabInterface.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabInterface.Controls.Add(Me.TabLog)
        Me.TabInterface.Location = New System.Drawing.Point(7, 35)
        Me.TabInterface.Name = "TabInterface"
        Me.TabInterface.SelectedIndex = 0
        Me.TabInterface.Size = New System.Drawing.Size(904, 464)
        Me.TabInterface.TabIndex = 190
        '
        'TabLog
        '
        Me.TabLog.Controls.Add(Me.dgLog)
        Me.TabLog.Location = New System.Drawing.Point(4, 22)
        Me.TabLog.Name = "TabLog"
        Me.TabLog.Padding = New System.Windows.Forms.Padding(3)
        Me.TabLog.Size = New System.Drawing.Size(896, 438)
        Me.TabLog.TabIndex = 0
        Me.TabLog.Text = "LOG"
        Me.TabLog.UseVisualStyleBackColor = True
        '
        'dgLog
        '
        Me.dgLog.AllowUserToAddRows = False
        Me.dgLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFecha, Me.colLog})
        Me.dgLog.Location = New System.Drawing.Point(6, 6)
        Me.dgLog.Name = "dgLog"
        Me.dgLog.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgLog.Size = New System.Drawing.Size(871, 426)
        Me.dgLog.TabIndex = 3
        '
        'colFecha
        '
        Me.colFecha.FillWeight = 20.0!
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        '
        'colLog
        '
        Me.colLog.HeaderText = "Log"
        Me.colLog.Name = "colLog"
        '
        'lblNombre
        '
        Me.lblNombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(3, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(908, 32)
        Me.lblNombre.TabIndex = 189
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlSectorInterfaceDescarga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TabInterface)
        Me.Controls.Add(Me.lblNombre)
        Me.Name = "CtrlSectorInterfaceDescarga"
        Me.Size = New System.Drawing.Size(911, 502)
        Me.TabInterface.ResumeLayout(False)
        Me.TabLog.ResumeLayout(False)
        CType(Me.dgLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabInterface As TabControl
    Friend WithEvents TabLog As TabPage
    Private WithEvents dgLog As DataGridView
    Private WithEvents colFecha As DataGridViewTextBoxColumn
    Private WithEvents colLog As DataGridViewTextBoxColumn
    Protected WithEvents lblNombre As Label
End Class
