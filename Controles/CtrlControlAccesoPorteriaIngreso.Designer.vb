﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlControlAccesoPorteriaIngreso
    Inherits Controles.CtrlAutomationObjectsPorteria

    ''' <summary>
    ''' Oculta el dinamismo de la barrera de ingreso y sus controles
    ''' </summary>
    Private _EnableBarreraIngreso As Boolean
    Public Property BarreraIngreso() As Boolean
        Get
            Return _EnableBarreraIngreso
        End Get
        Set(ByVal value As Boolean)
            _EnableBarreraIngreso = value
        End Set
    End Property

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoPorteriaIngreso))
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.CtrlAtenaLeyendoIngreso = New Controles.CtrlAntenaLeyendo()
        Me.CartelLED_INGRESO = New Controles.CtrlCartelMultiLED()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.CAM_INGRESO = New Controles.CtrlCamara()
        Me.S_INGRESO = New Controles.CtrlSensor()
        Me.S_LECTURA_INGRESO = New Controles.CtrlSensor()
        Me.B_INGRESO = New Controles.CtrlBarrera()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.btnLecturaManualIngreso = New System.Windows.Forms.Button()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlDinamismoPorteriaIngreso1 = New Controles.CtrlDinamismoPorteriaIngreso()
        Me.SuspendLayout()
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(150, 251)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 194
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CtrlAtenaLeyendoIngreso
        '
        Me.CtrlAtenaLeyendoIngreso.Location = New System.Drawing.Point(150, 129)
        Me.CtrlAtenaLeyendoIngreso.Name = "CtrlAtenaLeyendoIngreso"
        Me.CtrlAtenaLeyendoIngreso.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendoIngreso.TabIndex = 167
        Me.CtrlAtenaLeyendoIngreso.UltTag = Nothing
        '
        'CartelLED_INGRESO
        '
        Me.CartelLED_INGRESO.BitMonitoreo = CType(0, Short)
        Me.CartelLED_INGRESO.Conectado = False
        Me.CartelLED_INGRESO.GPIO = False
        Me.CartelLED_INGRESO.ID_CONTROL_ACCESO = 0
        Me.CartelLED_INGRESO.ID_PLC = 0
        Me.CartelLED_INGRESO.ID_SECTOR = 0
        Me.CartelLED_INGRESO.Inicializado = False
        Me.CartelLED_INGRESO.Location = New System.Drawing.Point(3, 484)
        Me.CartelLED_INGRESO.Name = "CartelLED_INGRESO"
        Me.CartelLED_INGRESO.patenteLeida = Nothing
        Me.CartelLED_INGRESO.PLC = Nothing
        Me.CartelLED_INGRESO.Size = New System.Drawing.Size(150, 100)
        Me.CartelLED_INGRESO.TabIndex = 166
        Me.CartelLED_INGRESO.tiempoPatenteLeidas = 0
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(534, 43)
        Me.lblNombre.TabIndex = 165
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(246, 41)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(49, 44)
        Me.btnAyuda.TabIndex = 164
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(246, 544)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 40)
        Me.btnReset.TabIndex = 163
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'CAM_INGRESO
        '
        Me.CAM_INGRESO.AutoSize = True
        Me.CAM_INGRESO.BitMonitoreo = CType(0, Short)
        Me.CAM_INGRESO.frmCamara = Nothing
        Me.CAM_INGRESO.GPIO = False
        Me.CAM_INGRESO.ID_CONTROL_ACCESO = 0
        Me.CAM_INGRESO.ID_PLC = 0
        Me.CAM_INGRESO.ID_SECTOR = 0
        Me.CAM_INGRESO.Inicializado = False
        Me.CAM_INGRESO.Location = New System.Drawing.Point(0, 96)
        Me.CAM_INGRESO.Name = "CAM_INGRESO"
        Me.CAM_INGRESO.patenteLeida = Nothing
        Me.CAM_INGRESO.PLC = Nothing
        Me.CAM_INGRESO.Size = New System.Drawing.Size(86, 60)
        Me.CAM_INGRESO.TabIndex = 161
        Me.CAM_INGRESO.tiempoPatenteLeidas = 0
        '
        'S_INGRESO
        '
        Me.S_INGRESO.BitMonitoreo = CType(0, Short)
        Me.S_INGRESO.Estado = False
        Me.S_INGRESO.GPIO = False
        Me.S_INGRESO.ID_CONTROL_ACCESO = 0
        Me.S_INGRESO.ID_PLC = 0
        Me.S_INGRESO.ID_SECTOR = 0
        Me.S_INGRESO.Inicializado = False
        Me.S_INGRESO.Location = New System.Drawing.Point(185, 477)
        Me.S_INGRESO.Name = "S_INGRESO"
        Me.S_INGRESO.patenteLeida = Nothing
        Me.S_INGRESO.PLC = Nothing
        Me.S_INGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_INGRESO.TabIndex = 160
        Me.S_INGRESO.tiempoPatenteLeidas = 0
        '
        'S_LECTURA_INGRESO
        '
        Me.S_LECTURA_INGRESO.BitMonitoreo = CType(0, Short)
        Me.S_LECTURA_INGRESO.Estado = False
        Me.S_LECTURA_INGRESO.GPIO = False
        Me.S_LECTURA_INGRESO.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_INGRESO.ID_PLC = 0
        Me.S_LECTURA_INGRESO.ID_SECTOR = 0
        Me.S_LECTURA_INGRESO.Inicializado = False
        Me.S_LECTURA_INGRESO.Location = New System.Drawing.Point(159, 528)
        Me.S_LECTURA_INGRESO.Name = "S_LECTURA_INGRESO"
        Me.S_LECTURA_INGRESO.patenteLeida = Nothing
        Me.S_LECTURA_INGRESO.PLC = Nothing
        Me.S_LECTURA_INGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA_INGRESO.TabIndex = 159
        Me.S_LECTURA_INGRESO.tiempoPatenteLeidas = 0
        '
        'B_INGRESO
        '
        Me.B_INGRESO.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_INGRESO.BitMonitoreo = CType(0, Short)
        Me.B_INGRESO.Estado = False
        Me.B_INGRESO.GPIO = False
        Me.B_INGRESO.ID_CONTROL_ACCESO = 0
        Me.B_INGRESO.ID_PLC = 0
        Me.B_INGRESO.ID_SECTOR = 0
        Me.B_INGRESO.Inicializado = False
        Me.B_INGRESO.Location = New System.Drawing.Point(73, 432)
        Me.B_INGRESO.Name = "B_INGRESO"
        Me.B_INGRESO.patenteLeida = Nothing
        Me.B_INGRESO.PLC = Nothing
        Me.B_INGRESO.Size = New System.Drawing.Size(62, 52)
        Me.B_INGRESO.TabIndex = 158
        Me.B_INGRESO.tiempoPatenteLeidas = 0
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(4, 46)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(152, 44)
        Me.btnLectorRfid.TabIndex = 107
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'btnLecturaManualIngreso
        '
        Me.btnLecturaManualIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManualIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualIngreso.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualIngreso.Location = New System.Drawing.Point(150, 207)
        Me.btnLecturaManualIngreso.Name = "btnLecturaManualIngreso"
        Me.btnLecturaManualIngreso.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualIngreso.TabIndex = 106
        Me.btnLecturaManualIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualIngreso.UseVisualStyleBackColor = False
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 590)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(533, 75)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 2
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(298, 40)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(235, 550)
        Me.CtrlPanelNotificaciones1.TabIndex = 1
        '
        'CtrlDinamismoPorteriaIngreso1
        '
        Me.CtrlDinamismoPorteriaIngreso1.BackgroundImage = CType(resources.GetObject("CtrlDinamismoPorteriaIngreso1.BackgroundImage"), System.Drawing.Image)
        Me.CtrlDinamismoPorteriaIngreso1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlDinamismoPorteriaIngreso1.BitMonitoreo = CType(0, Short)
        Me.CtrlDinamismoPorteriaIngreso1.GPIO = False
        Me.CtrlDinamismoPorteriaIngreso1.ID_CONTROL_ACCESO = 0
        Me.CtrlDinamismoPorteriaIngreso1.ID_PLC = 0
        Me.CtrlDinamismoPorteriaIngreso1.ID_SECTOR = 0
        Me.CtrlDinamismoPorteriaIngreso1.Inicializado = False
        Me.CtrlDinamismoPorteriaIngreso1.Location = New System.Drawing.Point(-3, 43)
        Me.CtrlDinamismoPorteriaIngreso1.Name = "CtrlDinamismoPorteriaIngreso1"
        Me.CtrlDinamismoPorteriaIngreso1.patenteLeida = Nothing
        Me.CtrlDinamismoPorteriaIngreso1.PLC = Nothing
        Me.CtrlDinamismoPorteriaIngreso1.Size = New System.Drawing.Size(298, 547)
        Me.CtrlDinamismoPorteriaIngreso1.TabIndex = 0
        Me.CtrlDinamismoPorteriaIngreso1.tiempoPatenteLeidas = 0
        '
        'CtrlControlAccesoPorteriaIngreso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.CtrlAtenaLeyendoIngreso)
        Me.Controls.Add(Me.CartelLED_INGRESO)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.CAM_INGRESO)
        Me.Controls.Add(Me.S_INGRESO)
        Me.Controls.Add(Me.S_LECTURA_INGRESO)
        Me.Controls.Add(Me.B_INGRESO)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.btnLecturaManualIngreso)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.CtrlDinamismoPorteriaIngreso1)
        Me.Name = "CtrlControlAccesoPorteriaIngreso"
        Me.Size = New System.Drawing.Size(534, 665)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlDinamismoPorteriaIngreso1 As CtrlDinamismoPorteriaIngreso
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents btnLecturaManualIngreso As Button
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents S_INGRESO As CtrlSensor
    Friend WithEvents S_LECTURA_INGRESO As CtrlSensor
    Public WithEvents B_INGRESO As CtrlBarrera
    Friend WithEvents CAM_INGRESO As CtrlCamara
    Friend WithEvents btnReset As Button
    Friend WithEvents btnAyuda As Button
    Protected WithEvents lblNombre As Label
    Friend WithEvents CartelLED_INGRESO As CtrlCartelMultiLED
    Friend WithEvents CtrlAtenaLeyendoIngreso As CtrlAntenaLeyendo
    Friend WithEvents btnAlarma As Button
End Class
