﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlImpresora
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnPapelEnImpresora = New System.Windows.Forms.Button()
        Me.lbTAG = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnConf = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnPapelEnImpresora
        '
        Me.btnPapelEnImpresora.BackColor = System.Drawing.Color.White
        Me.btnPapelEnImpresora.Enabled = False
        Me.btnPapelEnImpresora.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPapelEnImpresora.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPapelEnImpresora.Image = Global.Controles.My.Resources.Resources.ImpresoraCalado
        Me.btnPapelEnImpresora.Location = New System.Drawing.Point(0, 36)
        Me.btnPapelEnImpresora.Name = "btnPapelEnImpresora"
        Me.btnPapelEnImpresora.Size = New System.Drawing.Size(114, 46)
        Me.btnPapelEnImpresora.TabIndex = 102
        Me.btnPapelEnImpresora.UseVisualStyleBackColor = False
        '
        'lbTAG
        '
        Me.lbTAG.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbTAG.BackColor = System.Drawing.Color.MediumBlue
        Me.lbTAG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTAG.ForeColor = System.Drawing.Color.White
        Me.lbTAG.Location = New System.Drawing.Point(0, 0)
        Me.lbTAG.Name = "lbTAG"
        Me.lbTAG.Size = New System.Drawing.Size(114, 20)
        Me.lbTAG.TabIndex = 105
        Me.lbTAG.Text = "TAG"
        Me.lbTAG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEstado
        '
        Me.lblEstado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEstado.BackColor = System.Drawing.Color.Red
        Me.lblEstado.ForeColor = System.Drawing.Color.White
        Me.lblEstado.Location = New System.Drawing.Point(33, 21)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(81, 20)
        Me.lblEstado.TabIndex = 104
        Me.lblEstado.Text = "Desconectado"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnConf
        '
        Me.btnConf.BackColor = System.Drawing.Color.Transparent
        Me.btnConf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConf.Image = Global.Controles.My.Resources.Resources.Configuration19
        Me.btnConf.Location = New System.Drawing.Point(0, 21)
        Me.btnConf.Name = "btnConf"
        Me.btnConf.Size = New System.Drawing.Size(32, 20)
        Me.btnConf.TabIndex = 103
        Me.btnConf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnConf.UseVisualStyleBackColor = False
        '
        'CtrlImpresora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lbTAG)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnConf)
        Me.Controls.Add(Me.btnPapelEnImpresora)
        Me.Name = "CtrlImpresora"
        Me.Size = New System.Drawing.Size(114, 82)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnPapelEnImpresora As Button
    Friend WithEvents lbTAG As Label
    Friend WithEvents lblEstado As Label
    Friend WithEvents btnConf As Button
End Class
