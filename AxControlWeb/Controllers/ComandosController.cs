﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Controles;
namespace AxControlWeb.Controllers
{
    public class ComandosController : Controller
    {
        private AxDriverORAEntities db = new AxDriverORAEntities();


        public void GenerarComando(int idSector, int idComando, string tag)
        {
            var axComando = (from ac in db.DATO_WORDINT where ac.ID_SECTOR == idSector && ac.TAG == tag select ac).FirstOrDefault();
            if (axComando != null)
            {
                axComando.VALOR = idComando;
                axComando.FLAG_RW = true;
                db.SaveChanges();
            }

        }
        public void GenerarComandoDword(int idSector, int idComando, string tag)
        {
            var axComando = (from ac in db.DATO_DWORDDINT where ac.ID_SECTOR == idSector && ac.TAG == tag select ac).FirstOrDefault();
            if (axComando != null)
            {
                axComando.VALOR = idComando;
                axComando.FLAG_RW = true;
                db.SaveChanges();
            }

        }

        public void GenerarComandoCartel(int idComando, int idSector, string mensaje)
        {
            try
            {
                AxControlORAEntities dbControl = new AxControlORAEntities();
                var control = (from c in dbControl.SECTOR where c.ID_SECTOR == idSector select c).FirstOrDefault();
                int ID_CONTROL = (int)control.ID_CONTROL_ACCESO;

                COMANDO newComando = new COMANDO();
                newComando.ID_CONTROL = ID_CONTROL;
                newComando.ID_SECTOR = idSector;
                newComando.PROCESADO = 0;
                newComando.TAG = idComando.ToString();
                newComando.MENSAJE = mensaje;
                newComando.ID_TIPO_COMANDO = (int)Constante.TIPO_COMANDO.ESCRIBIR_CARTEL;
                dbControl.COMANDO.Add(newComando);
                dbControl.SaveChanges();

                dbControl.Dispose();
                dbControl = null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void TagLeidoManual(int idSector, string tag)
        {

            try
            {
                AxControlORAEntities dbControl = new AxControlORAEntities();
                var control = (from c in dbControl.SECTOR where c.ID_SECTOR == idSector select c).FirstOrDefault();
                int ID_CONTROL = (int)control.ID_CONTROL_ACCESO;

                COMANDO newComando = new COMANDO();
                newComando.ID_CONTROL = ID_CONTROL;
                newComando.ID_SECTOR = idSector;
                newComando.PROCESADO = 0;
                newComando.TAG = tag;
                dbControl.COMANDO.Add(newComando);
                dbControl.SaveChanges();

                dbControl.Dispose();
                dbControl = null;
            }
            catch (Exception ex)
            {

            }

        }

        public void ComandoControl(int idSector, string tag, int idTipoComando)
        {

            try
            {
                AxControlORAEntities dbControl = new AxControlORAEntities();
                var control = (from c in dbControl.SECTOR where c.ID_SECTOR == idSector select c).FirstOrDefault();
                int ID_CONTROL = (int)control.ID_CONTROL_ACCESO;

                COMANDO newComando = new COMANDO();
                newComando.ID_CONTROL = ID_CONTROL;
                newComando.ID_SECTOR = idSector;
                newComando.PROCESADO = 0;
                newComando.TAG = tag;
                newComando.ID_TIPO_COMANDO = idTipoComando;
                dbControl.COMANDO.Add(newComando);
                dbControl.SaveChanges();

                dbControl.Dispose();
                dbControl = null;
            }
            catch (Exception ex)
            {

            }

        }

        // GET: Comandos
        public ActionResult Index()
        {
            var dATO_WORDINT = db.DATO_WORDINT.Include(d => d.PLC);
            return View(dATO_WORDINT.ToList());
        }

        // GET: Comandos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DATO_WORDINT dATO_WORDINT = db.DATO_WORDINT.Find(id);
            if (dATO_WORDINT == null)
            {
                return HttpNotFound();
            }
            return View(dATO_WORDINT);
        }

        // GET: Comandos/Create
        public ActionResult Create()
        {
            ViewBag.FK_PLC = new SelectList(db.PLC, "ID", "NOMBRE");
            return View();
        }

        // POST: Comandos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,TAG,NUM_WORD,VALOR,QC,FLAG_RW,FK_PLC,RW,ID_SECTOR,NUM_DB")] DATO_WORDINT dATO_WORDINT)
        {
            if (ModelState.IsValid)
            {
                db.DATO_WORDINT.Add(dATO_WORDINT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_PLC = new SelectList(db.PLC, "ID", "NOMBRE", dATO_WORDINT.FK_PLC);
            return View(dATO_WORDINT);
        }

        // GET: Comandos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DATO_WORDINT dATO_WORDINT = db.DATO_WORDINT.Find(id);
            if (dATO_WORDINT == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_PLC = new SelectList(db.PLC, "ID", "NOMBRE", dATO_WORDINT.FK_PLC);
            return View(dATO_WORDINT);
        }

        // POST: Comandos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TAG,NUM_WORD,VALOR,QC,FLAG_RW,FK_PLC,RW,ID_SECTOR,NUM_DB")] DATO_WORDINT dATO_WORDINT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dATO_WORDINT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_PLC = new SelectList(db.PLC, "ID", "NOMBRE", dATO_WORDINT.FK_PLC);
            return View(dATO_WORDINT);
        }

        // GET: Comandos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DATO_WORDINT dATO_WORDINT = db.DATO_WORDINT.Find(id);
            if (dATO_WORDINT == null)
            {
                return HttpNotFound();
            }
            return View(dATO_WORDINT);
        }

        // POST: Comandos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DATO_WORDINT dATO_WORDINT = db.DATO_WORDINT.Find(id);
            db.DATO_WORDINT.Remove(dATO_WORDINT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
