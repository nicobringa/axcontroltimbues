﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class AxInterfaceWs
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function HelloAumax() As String
        Return "Hello from Aumax"
    End Function
    <WebMethod()>
    Public Function AltaModSilo(jsonInput As String) As Int64

        Dim wsInput = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.AltaModSilo)(jsonInput)

        Return AddInterface(wsInput.nombre, jsonInput, wsInput.idComando)
    End Function

    <WebMethod()>
    Public Function AltaOTDesc(jsonInput As String) As Int64
        Dim wsInput = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.AltaOTDesc)(jsonInput)

        Return AddInterface(wsInput.nombre, jsonInput, wsInput.comando)
    End Function

    <WebMethod()>
    Public Function EventoOTDescToElectroluz(jsonInput As String) As Int64
        Dim wsInput = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.EventoOTDescToElectroluz)(jsonInput)

        Return AddInterface(wsInput.nombre, jsonInput, Nothing)
    End Function

    <WebMethod()>
    Public Function AltaOTCarga(jsonInput As String) As Int64
        Dim wsInput = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.AltaOTCarga)(jsonInput)

        Return AddInterface(wsInput.nombre, jsonInput, wsInput.comando)
    End Function

    <WebMethod()>
    Public Function EventoOTCargaToElectroluz(jsonInput As String) As Int64
        Dim wsInput = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.EventoOTCargaToElectroluz)(jsonInput)

        Return AddInterface(wsInput.nombre, jsonInput, Nothing)
    End Function


    Private Function AddInterface(nombreWS As String, jsonInput As String, comando As Int64?) As Int64

        Dim db As New Entidades.AxControlORAEntities

        Dim oInterfaceWS As New Entidades.INTERFACE_WS

        oInterfaceWS.FECHA = DateTime.Now
        oInterfaceWS.ID_COMANDO = comando
        oInterfaceWS.WEB_SERVICE = nombreWS
        oInterfaceWS.JSON_INPUT = jsonInput
        oInterfaceWS.ID_ESTADO = Entidades.Constante.ESTADO_REGISTRO.SIN_PROCESAR

        Dim nInterface As New Negocio.InterfaceN
        oInterfaceWS = nInterface.Add(oInterfaceWS)

        Return oInterfaceWS.ID_REGISTRO

    End Function

End Class