﻿Imports Entidades.ConstanteBalanza
Imports Negocio.modDelegado
Public Class CtrlEstadoBalanza
    Private COLOR_ERROR As Color = Color.Red
    Private COLOR_HABILITADO As Color = Color.Green
    Private COLOR_OCUPADA As Color = Color.Gray


    Public Sub SetEstado(ByVal EstadoBalanza As Entidades.ConstanteBalanza.EstadoBalanza)
        Dim TextEstado As String = ""

        If EstadoBalanza = Entidades.ConstanteBalanza.EstadoBalanza.BarrerasCerrada Then ' = 0

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_HABILITADO)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), "HABILITADA")
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        ElseIf EstadoBalanza = Entidades.ConstanteBalanza.EstadoBalanza.HabilitadaIngresarBarrera1 Or
            EstadoBalanza = Entidades.ConstanteBalanza.EstadoBalanza.HabilitadaIngresarBarrera2 Then

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.HABILITADA_INGRESAR)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)


        ElseIf EstadoBalanza = EstadoBalanza.BalanzaCeroNoConectadaBarrera1 Or
            EstadoBalanza = EstadoBalanza.BalanzaCeroNoConectadaBarrera2 Then '<--- BALANZA EN CERO O NO CONECTADA (15-65)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_ERROR)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.BALANZA_CERO_NO_CONECTADA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        ElseIf EstadoBalanza = EstadoBalanza.AvanzarCamionBarrera1 Or
            EstadoBalanza = EstadoBalanza.AvanzarCamionBarrera2 Then '<--- AVANZA CAMION(20-70)


            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.AVANZA_CAMION)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        ElseIf EstadoBalanza = EstadoBalanza.RetrocedaCamionBarrera1 Or
            EstadoBalanza = EstadoBalanza.RetrocedaCamionBarrera2 Then '<--- RETROCEDA CAMION(25-75)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.RETROCEDA_CAMION)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        ElseIf EstadoBalanza = EstadoBalanza.EstabilizandoPesoBarrera1 Or
            EstadoBalanza = EstadoBalanza.EstabilizandoPesoBarrera2 Then '<--- ESTABILIZANDO PESO(30-80)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.ESTABILIZANDO_PESO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)


        ElseIf EstadoBalanza = EstadoBalanza.PesoMaximoBarrera1 Or
            EstadoBalanza = EstadoBalanza.PesoMaximoBarrera2 Then '<---PESO MAXIMO(33-85)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_ERROR)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.PESO_MAXIMO_ALZANZADO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        ElseIf EstadoBalanza = EstadoBalanza.TomarPesoBarrera1 Or
            EstadoBalanza = EstadoBalanza.TomarPesoBarrera2 Then ' <--- TOMAR PESO(35-90)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.TOMAR_PESO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        ElseIf EstadoBalanza = EstadoBalanza.PermitirSalirBarrera1 Or
     EstadoBalanza = EstadoBalanza.PermitirSalirBarrera2 Then ' <--- PERMITIR SALIR

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.PERMITIR_SALIR_BALANZA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)


        ElseIf EstadoBalanza = EstadoBalanza.BajaBarrera1Salida Or EstadoBalanza = EstadoBalanza.BajaBarrera2Salida Then '<--- BAJAR BARRERA (45-100)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.BAJA_BARRERA_SALIDA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)

        End If


    End Sub



    Private Sub lblEstado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblEstado.Click

    End Sub
End Class
