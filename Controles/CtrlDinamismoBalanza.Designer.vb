﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoBalanza
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlDinamismoBalanza))
        Me.picBAL2_BAR1_ABAJO = New System.Windows.Forms.PictureBox()
        Me.picBAL2_BAR2_ABAJO = New System.Windows.Forms.PictureBox()
        Me.picBAL1_BAR2_ABAJO = New System.Windows.Forms.PictureBox()
        Me.picBAL1_BAR1_ABAJO = New System.Windows.Forms.PictureBox()
        Me.picBAL2_BAR1_ARRIBA = New System.Windows.Forms.PictureBox()
        Me.picBAL2_BAR2_ARRIBA = New System.Windows.Forms.PictureBox()
        Me.picBAL1_BAR2_ARRIBA = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.picBAL1_BAR1_ARRIBA = New System.Windows.Forms.PictureBox()
        Me.picBAL1_S1NoCortando = New System.Windows.Forms.PictureBox()
        Me.picBAL1_S1Cortando = New System.Windows.Forms.PictureBox()
        Me.picBAL2_S1NoCortando = New System.Windows.Forms.PictureBox()
        Me.picBAL2_S1Cortando = New System.Windows.Forms.PictureBox()
        Me.picBAL1_S2NoCortando = New System.Windows.Forms.PictureBox()
        Me.picBAL1_S2Cortando = New System.Windows.Forms.PictureBox()
        Me.picBAL2_S2Cortando = New System.Windows.Forms.PictureBox()
        Me.picBAL2_S2NoCortando = New System.Windows.Forms.PictureBox()
        Me.lblFalla_BAL2_BAR2 = New System.Windows.Forms.Label()
        Me.lblFalla_BAL1_BAR2 = New System.Windows.Forms.Label()
        Me.ToolTipInformacion = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ToolTipFalla = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblFalla_BAL2_BAR1 = New System.Windows.Forms.Label()
        Me.lblFalla_BAL1_BAR1 = New System.Windows.Forms.Label()
        Me.ToolTipDeshabilitado = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.picBAL2_BAR1_ABAJO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_BAR2_ABAJO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_BAR2_ABAJO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_BAR1_ABAJO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_BAR1_ARRIBA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_BAR2_ARRIBA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_BAR2_ARRIBA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_BAR1_ARRIBA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_S1NoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_S1Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_S1NoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_S1Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_S2NoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL1_S2Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_S2Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAL2_S2NoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picBAL2_BAR1_ABAJO
        '
        Me.picBAL2_BAR1_ABAJO.AccessibleDescription = "Balanza Derecha, Barrera ingreso baja"
        Me.picBAL2_BAR1_ABAJO.BackgroundImage = Global.Controles.My.Resources.Resources.BIDB
        Me.picBAL2_BAR1_ABAJO.Location = New System.Drawing.Point(511, 372)
        Me.picBAL2_BAR1_ABAJO.Name = "picBAL2_BAR1_ABAJO"
        Me.picBAL2_BAR1_ABAJO.Size = New System.Drawing.Size(224, 96)
        Me.picBAL2_BAR1_ABAJO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_BAR1_ABAJO.TabIndex = 8
        Me.picBAL2_BAR1_ABAJO.TabStop = False
        '
        'picBAL2_BAR2_ABAJO
        '
        Me.picBAL2_BAR2_ABAJO.AccessibleDescription = "Balanza Derecha, Barrera ingreso baja"
        Me.picBAL2_BAR2_ABAJO.BackgroundImage = Global.Controles.My.Resources.Resources.BEDB
        Me.picBAL2_BAR2_ABAJO.Location = New System.Drawing.Point(432, 300)
        Me.picBAL2_BAR2_ABAJO.Name = "picBAL2_BAR2_ABAJO"
        Me.picBAL2_BAR2_ABAJO.Size = New System.Drawing.Size(93, 46)
        Me.picBAL2_BAR2_ABAJO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_BAR2_ABAJO.TabIndex = 7
        Me.picBAL2_BAR2_ABAJO.TabStop = False
        '
        'picBAL1_BAR2_ABAJO
        '
        Me.picBAL1_BAR2_ABAJO.AccessibleDescription = "Balanza Izquierda, Barrera Egreso baja"
        Me.picBAL1_BAR2_ABAJO.BackgroundImage = Global.Controles.My.Resources.Resources.BEIB
        Me.picBAL1_BAR2_ABAJO.Location = New System.Drawing.Point(234, 299)
        Me.picBAL1_BAR2_ABAJO.Name = "picBAL1_BAR2_ABAJO"
        Me.picBAL1_BAR2_ABAJO.Size = New System.Drawing.Size(94, 46)
        Me.picBAL1_BAR2_ABAJO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_BAR2_ABAJO.TabIndex = 6
        Me.picBAL1_BAR2_ABAJO.TabStop = False
        '
        'picBAL1_BAR1_ABAJO
        '
        Me.picBAL1_BAR1_ABAJO.AccessibleName = "Balanza Izquierda, Barrera ingreso baja"
        Me.picBAL1_BAR1_ABAJO.BackgroundImage = Global.Controles.My.Resources.Resources.BIIB
        Me.picBAL1_BAR1_ABAJO.Location = New System.Drawing.Point(15, 372)
        Me.picBAL1_BAR1_ABAJO.Name = "picBAL1_BAR1_ABAJO"
        Me.picBAL1_BAR1_ABAJO.Size = New System.Drawing.Size(245, 98)
        Me.picBAL1_BAR1_ABAJO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_BAR1_ABAJO.TabIndex = 5
        Me.picBAL1_BAR1_ABAJO.TabStop = False
        '
        'picBAL2_BAR1_ARRIBA
        '
        Me.picBAL2_BAR1_ARRIBA.AccessibleDescription = "Balanza Derecha, Barrera ingreso Arriba"
        Me.picBAL2_BAR1_ARRIBA.Image = Global.Controles.My.Resources.Resources.BIDA
        Me.picBAL2_BAR1_ARRIBA.Location = New System.Drawing.Point(670, 196)
        Me.picBAL2_BAR1_ARRIBA.Name = "picBAL2_BAR1_ARRIBA"
        Me.picBAL2_BAR1_ARRIBA.Size = New System.Drawing.Size(62, 270)
        Me.picBAL2_BAR1_ARRIBA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_BAR1_ARRIBA.TabIndex = 4
        Me.picBAL2_BAR1_ARRIBA.TabStop = False
        '
        'picBAL2_BAR2_ARRIBA
        '
        Me.picBAL2_BAR2_ARRIBA.AccessibleDescription = "Balanza Derecha Barrera Egreso Arriba"
        Me.picBAL2_BAR2_ARRIBA.Image = Global.Controles.My.Resources.Resources.BEDA
        Me.picBAL2_BAR2_ARRIBA.Location = New System.Drawing.Point(499, 223)
        Me.picBAL2_BAR2_ARRIBA.Name = "picBAL2_BAR2_ARRIBA"
        Me.picBAL2_BAR2_ARRIBA.Size = New System.Drawing.Size(32, 117)
        Me.picBAL2_BAR2_ARRIBA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_BAR2_ARRIBA.TabIndex = 3
        Me.picBAL2_BAR2_ARRIBA.TabStop = False
        '
        'picBAL1_BAR2_ARRIBA
        '
        Me.picBAL1_BAR2_ARRIBA.AccessibleDescription = "Barrera Ingreso Bal"
        Me.picBAL1_BAR2_ARRIBA.Image = CType(resources.GetObject("picBAL1_BAR2_ARRIBA.Image"), System.Drawing.Image)
        Me.picBAL1_BAR2_ARRIBA.Location = New System.Drawing.Point(233, 232)
        Me.picBAL1_BAR2_ARRIBA.Name = "picBAL1_BAR2_ARRIBA"
        Me.picBAL1_BAR2_ARRIBA.Size = New System.Drawing.Size(28, 106)
        Me.picBAL1_BAR2_ARRIBA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_BAR2_ARRIBA.TabIndex = 9
        Me.picBAL1_BAR2_ARRIBA.TabStop = False
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(129, 448)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 23)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Balanza 1"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(502, 448)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 23)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Balanza 2"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picBAL1_BAR1_ARRIBA
        '
        Me.picBAL1_BAR1_ARRIBA.AccessibleDescription = "Balanza Izquierda, Barrera Ingreso Arriba"
        Me.picBAL1_BAR1_ARRIBA.Image = Global.Controles.My.Resources.Resources.BIIA
        Me.picBAL1_BAR1_ARRIBA.Location = New System.Drawing.Point(7, 189)
        Me.picBAL1_BAR1_ARRIBA.Name = "picBAL1_BAR1_ARRIBA"
        Me.picBAL1_BAR1_ARRIBA.Size = New System.Drawing.Size(81, 278)
        Me.picBAL1_BAR1_ARRIBA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_BAR1_ARRIBA.TabIndex = 1
        Me.picBAL1_BAR1_ARRIBA.TabStop = False
        Me.picBAL1_BAR1_ARRIBA.Tag = "Barrera Ingreso Arriba"
        '
        'picBAL1_S1NoCortando
        '
        Me.picBAL1_S1NoCortando.AccessibleDescription = "Sensor Ingreso Balanza Izquierda"
        Me.picBAL1_S1NoCortando.Image = Global.Controles.My.Resources.Resources.S_NoCortandoBIBI1
        Me.picBAL1_S1NoCortando.Location = New System.Drawing.Point(77, 430)
        Me.picBAL1_S1NoCortando.Name = "picBAL1_S1NoCortando"
        Me.picBAL1_S1NoCortando.Size = New System.Drawing.Size(245, 18)
        Me.picBAL1_S1NoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_S1NoCortando.TabIndex = 52
        Me.picBAL1_S1NoCortando.TabStop = False
        '
        'picBAL1_S1Cortando
        '
        Me.picBAL1_S1Cortando.AccessibleDescription = "Sensor Ingreso Balanza Izquierda"
        Me.picBAL1_S1Cortando.Image = Global.Controles.My.Resources.Resources.S_CortandoBIBI
        Me.picBAL1_S1Cortando.Location = New System.Drawing.Point(79, 434)
        Me.picBAL1_S1Cortando.Name = "picBAL1_S1Cortando"
        Me.picBAL1_S1Cortando.Size = New System.Drawing.Size(257, 17)
        Me.picBAL1_S1Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_S1Cortando.TabIndex = 53
        Me.picBAL1_S1Cortando.TabStop = False
        '
        'picBAL2_S1NoCortando
        '
        Me.picBAL2_S1NoCortando.AccessibleDescription = "Sensor Ingreso Balanza Izquierda"
        Me.picBAL2_S1NoCortando.Image = Global.Controles.My.Resources.Resources.S_NoCortandoBIBD
        Me.picBAL2_S1NoCortando.Location = New System.Drawing.Point(385, 431)
        Me.picBAL2_S1NoCortando.Name = "picBAL2_S1NoCortando"
        Me.picBAL2_S1NoCortando.Size = New System.Drawing.Size(293, 18)
        Me.picBAL2_S1NoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_S1NoCortando.TabIndex = 54
        Me.picBAL2_S1NoCortando.TabStop = False
        '
        'picBAL2_S1Cortando
        '
        Me.picBAL2_S1Cortando.AccessibleDescription = "Sensor Ingreso Balanza Izquierda"
        Me.picBAL2_S1Cortando.Image = Global.Controles.My.Resources.Resources.S_CortandoBIBD
        Me.picBAL2_S1Cortando.Location = New System.Drawing.Point(408, 431)
        Me.picBAL2_S1Cortando.Name = "picBAL2_S1Cortando"
        Me.picBAL2_S1Cortando.Size = New System.Drawing.Size(269, 17)
        Me.picBAL2_S1Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_S1Cortando.TabIndex = 55
        Me.picBAL2_S1Cortando.TabStop = False
        '
        'picBAL1_S2NoCortando
        '
        Me.picBAL1_S2NoCortando.AccessibleDescription = "Sensor Ingreso Balanza Izquierda"
        Me.picBAL1_S2NoCortando.Image = Global.Controles.My.Resources.Resources.S_NoCortandoBEBI
        Me.picBAL1_S2NoCortando.Location = New System.Drawing.Point(252, 328)
        Me.picBAL1_S2NoCortando.Name = "picBAL1_S2NoCortando"
        Me.picBAL1_S2NoCortando.Size = New System.Drawing.Size(117, 9)
        Me.picBAL1_S2NoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_S2NoCortando.TabIndex = 56
        Me.picBAL1_S2NoCortando.TabStop = False
        '
        'picBAL1_S2Cortando
        '
        Me.picBAL1_S2Cortando.AccessibleDescription = "Sensor Ingreso Balanza Izquierda"
        Me.picBAL1_S2Cortando.Image = Global.Controles.My.Resources.Resources.S_CortandoBEBI
        Me.picBAL1_S2Cortando.Location = New System.Drawing.Point(255, 329)
        Me.picBAL1_S2Cortando.Name = "picBAL1_S2Cortando"
        Me.picBAL1_S2Cortando.Size = New System.Drawing.Size(122, 7)
        Me.picBAL1_S2Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL1_S2Cortando.TabIndex = 57
        Me.picBAL1_S2Cortando.TabStop = False
        '
        'picBAL2_S2Cortando
        '
        Me.picBAL2_S2Cortando.AccessibleDescription = ""
        Me.picBAL2_S2Cortando.Image = Global.Controles.My.Resources.Resources.S_CortandoBEBD
        Me.picBAL2_S2Cortando.Location = New System.Drawing.Point(394, 330)
        Me.picBAL2_S2Cortando.Name = "picBAL2_S2Cortando"
        Me.picBAL2_S2Cortando.Size = New System.Drawing.Size(124, 7)
        Me.picBAL2_S2Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_S2Cortando.TabIndex = 58
        Me.picBAL2_S2Cortando.TabStop = False
        '
        'picBAL2_S2NoCortando
        '
        Me.picBAL2_S2NoCortando.AccessibleDescription = ""
        Me.picBAL2_S2NoCortando.Image = Global.Controles.My.Resources.Resources.S_NoCortandoBEBD
        Me.picBAL2_S2NoCortando.Location = New System.Drawing.Point(396, 330)
        Me.picBAL2_S2NoCortando.Name = "picBAL2_S2NoCortando"
        Me.picBAL2_S2NoCortando.Size = New System.Drawing.Size(123, 10)
        Me.picBAL2_S2NoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAL2_S2NoCortando.TabIndex = 59
        Me.picBAL2_S2NoCortando.TabStop = False
        '
        'lblFalla_BAL2_BAR2
        '
        Me.lblFalla_BAL2_BAR2.AccessibleDescription = "Balanza 2 Barrera 2"
        Me.lblFalla_BAL2_BAR2.AutoSize = True
        Me.lblFalla_BAL2_BAR2.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAL2_BAR2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAL2_BAR2.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAL2_BAR2.Location = New System.Drawing.Point(495, 305)
        Me.lblFalla_BAL2_BAR2.Name = "lblFalla_BAL2_BAR2"
        Me.lblFalla_BAL2_BAR2.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAL2_BAR2.TabIndex = 91
        Me.lblFalla_BAL2_BAR2.Text = "Falla"
        Me.lblFalla_BAL2_BAR2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipFalla.SetToolTip(Me.lblFalla_BAL2_BAR2, "Barrera está en falla ¡Por favor revisar!")
        '
        'lblFalla_BAL1_BAR2
        '
        Me.lblFalla_BAL1_BAR2.AccessibleDescription = "Balanza 1 Barrera 2"
        Me.lblFalla_BAL1_BAR2.AutoSize = True
        Me.lblFalla_BAL1_BAR2.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAL1_BAR2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAL1_BAR2.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAL1_BAR2.Location = New System.Drawing.Point(229, 306)
        Me.lblFalla_BAL1_BAR2.Name = "lblFalla_BAL1_BAR2"
        Me.lblFalla_BAL1_BAR2.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAL1_BAR2.TabIndex = 92
        Me.lblFalla_BAL1_BAR2.Text = "Falla"
        Me.lblFalla_BAL1_BAR2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipFalla.SetToolTip(Me.lblFalla_BAL1_BAR2, "Barrera está en falla ¡Por favor revisar!")
        '
        'ToolTipInformacion
        '
        Me.ToolTipInformacion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ToolTipInformacion.IsBalloon = True
        Me.ToolTipInformacion.ShowAlways = True
        Me.ToolTipInformacion.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTipInformacion.ToolTipTitle = "Balanza"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(8, 336)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 37)
        Me.Label6.TabIndex = 101
        Me.Label6.Text = "1"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(722, 336)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 37)
        Me.Label8.TabIndex = 102
        Me.Label8.Text = "1"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(199, 254)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(36, 37)
        Me.Label9.TabIndex = 103
        Me.Label9.Text = "2"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(532, 258)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 37)
        Me.Label10.TabIndex = 104
        Me.Label10.Text = "2"
        '
        'ToolTipFalla
        '
        Me.ToolTipFalla.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ToolTipFalla.IsBalloon = True
        Me.ToolTipFalla.ShowAlways = True
        Me.ToolTipFalla.ToolTipIcon = System.Windows.Forms.ToolTipIcon.[Error]
        Me.ToolTipFalla.ToolTipTitle = "Balanza"
        '
        'lblFalla_BAL2_BAR1
        '
        Me.lblFalla_BAL2_BAR1.AccessibleDescription = "Balanza 2 Barrera 1"
        Me.lblFalla_BAL2_BAR1.AutoSize = True
        Me.lblFalla_BAL2_BAR1.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAL2_BAR1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAL2_BAR1.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAL2_BAR1.Location = New System.Drawing.Point(684, 406)
        Me.lblFalla_BAL2_BAR1.Name = "lblFalla_BAL2_BAR1"
        Me.lblFalla_BAL2_BAR1.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAL2_BAR1.TabIndex = 94
        Me.lblFalla_BAL2_BAR1.Text = "Falla"
        Me.lblFalla_BAL2_BAR1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipFalla.SetToolTip(Me.lblFalla_BAL2_BAR1, "Barrera está en falla ¡Por favor revisar!")
        '
        'lblFalla_BAL1_BAR1
        '
        Me.lblFalla_BAL1_BAR1.AccessibleDescription = "Balanza 1 Barrera 1"
        Me.lblFalla_BAL1_BAR1.AutoSize = True
        Me.lblFalla_BAL1_BAR1.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAL1_BAR1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAL1_BAR1.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAL1_BAR1.Location = New System.Drawing.Point(20, 406)
        Me.lblFalla_BAL1_BAR1.Name = "lblFalla_BAL1_BAR1"
        Me.lblFalla_BAL1_BAR1.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAL1_BAR1.TabIndex = 93
        Me.lblFalla_BAL1_BAR1.Text = "Falla"
        Me.lblFalla_BAL1_BAR1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipFalla.SetToolTip(Me.lblFalla_BAL1_BAR1, "Barrera está en falla ¡Por favor revisar!")
        '
        'ToolTipDeshabilitado
        '
        Me.ToolTipDeshabilitado.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ToolTipDeshabilitado.IsBalloon = True
        Me.ToolTipDeshabilitado.ShowAlways = True
        Me.ToolTipDeshabilitado.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning
        Me.ToolTipDeshabilitado.ToolTipTitle = "Balanza"
        '
        'CtrlDinamismoBalanza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(225, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.BalanzaSinBarrera
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.picBAL2_S2NoCortando)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblFalla_BAL2_BAR1)
        Me.Controls.Add(Me.lblFalla_BAL1_BAR1)
        Me.Controls.Add(Me.lblFalla_BAL1_BAR2)
        Me.Controls.Add(Me.lblFalla_BAL2_BAR2)
        Me.Controls.Add(Me.picBAL1_S2NoCortando)
        Me.Controls.Add(Me.picBAL2_S1NoCortando)
        Me.Controls.Add(Me.picBAL1_S1NoCortando)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.picBAL1_BAR2_ARRIBA)
        Me.Controls.Add(Me.picBAL1_BAR1_ARRIBA)
        Me.Controls.Add(Me.picBAL2_BAR2_ARRIBA)
        Me.Controls.Add(Me.picBAL2_BAR1_ARRIBA)
        Me.Controls.Add(Me.picBAL1_S1Cortando)
        Me.Controls.Add(Me.picBAL1_S2Cortando)
        Me.Controls.Add(Me.picBAL2_S1Cortando)
        Me.Controls.Add(Me.picBAL2_S2Cortando)
        Me.Controls.Add(Me.picBAL2_BAR1_ABAJO)
        Me.Controls.Add(Me.picBAL2_BAR2_ABAJO)
        Me.Controls.Add(Me.picBAL1_BAR2_ABAJO)
        Me.Controls.Add(Me.picBAL1_BAR1_ABAJO)
        Me.Name = "CtrlDinamismoBalanza"
        Me.Size = New System.Drawing.Size(801, 601)
        CType(Me.picBAL2_BAR1_ABAJO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_BAR2_ABAJO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_BAR2_ABAJO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_BAR1_ABAJO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_BAR1_ARRIBA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_BAR2_ARRIBA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_BAR2_ARRIBA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_BAR1_ARRIBA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_S1NoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_S1Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_S1NoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_S1Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_S2NoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL1_S2Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_S2Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAL2_S2NoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picBAL2_BAR2_ARRIBA As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_BAR1_ARRIBA As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL1_BAR1_ABAJO As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL1_BAR2_ABAJO As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_BAR2_ABAJO As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_BAR1_ABAJO As System.Windows.Forms.PictureBox





    Friend WithEvents picBAL1_BAR2_ARRIBA As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents picBAL1_BAR1_ARRIBA As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL1_S1NoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL1_S1Cortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_S1NoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_S1Cortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL1_S2NoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL1_S2Cortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_S2Cortando As System.Windows.Forms.PictureBox
    Friend WithEvents picBAL2_S2NoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents lblFalla_BAL2_BAR2 As System.Windows.Forms.Label
    Friend WithEvents lblFalla_BAL1_BAR2 As System.Windows.Forms.Label
    Friend WithEvents ToolTipInformacion As System.Windows.Forms.ToolTip
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ToolTipFalla As System.Windows.Forms.ToolTip
    Friend WithEvents ToolTipDeshabilitado As System.Windows.Forms.ToolTip
    Friend WithEvents lblFalla_BAL2_BAR1 As Label
    Friend WithEvents lblFalla_BAL1_BAR1 As Label
End Class
