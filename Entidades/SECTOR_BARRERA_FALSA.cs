﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class SECTOR_BARRERA_FALSA
    {
        public int ID_SECTOR { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<int> ID_CONTROL_ACCESO { get; set; }
        public string DESCRIPCION { get; set; }
        public int Barrera { get; set; }
    }
}
