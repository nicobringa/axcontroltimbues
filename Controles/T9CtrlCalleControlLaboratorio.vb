﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Public Class T9CtrlCalleControlLaboratorio

#Region "PROPIEDADES"


    Private _idControlAcceso As Integer = 0
    Public Property ID_CONTROL_ACCESO() As Integer
        Get
            Return _idControlAcceso
        End Get
        Set(ByVal value As Integer)
            _idControlAcceso = value
        End Set
    End Property

    Private _RFID As Boolean
    Public Property RFID() As Boolean
        Get
            Return _RFID
        End Get
        Set(ByVal value As Boolean)
            _RFID = value
        End Set
    End Property

    Private ControlAcceso As Entidades.CONTROL_ACCESO
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private hMonitoreo As Thread
    Private UltIdNotificacion As Integer
    Private oLector As New Entidades.LECTOR_RFID
    Private nLector As New Negocio.LectorRFID_N

    Private ListaCtrlSectorPorteria As List(Of CtrlSectorPorteriaUnidireccional)
    'Private nAlarma As New Negocio.AlarmaN
#End Region

#Region "CONSTRUCTOR"

    Public Sub New(ID_CONTROL_ACCESO As Integer, ID_PLC As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_PLC = ID_PLC

    End Sub

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()



    End Sub

#End Region

#Region "EVENTOS FORM"


#End Region


#Region "METODOS"

    ''' <summary>
    ''' Metodo para incializar todas las variables de estado del objeto
    ''' </summary>
    Public Overrides Sub Inicializar()

        If Me.ID_CONTROL_ACCESO = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
                "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID un control de acceso")

            Return
        End If

        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)

        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If
        Dim nombreControlAcceso As String = String.Format("{0} - ({1})", Me.ControlAcceso.NOMBRE, Me.ControlAcceso.ID_CONTROL_ACCESO)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreControlAcceso)

        'Inicializo como servidor
        IniServidor()

        'Ejecuto el hilo de monitoreo
        If Not IsNothing(hMonitoreo) Then
            If hMonitoreo.IsAlive Then hMonitoreo.Abort()
            hMonitoreo = Nothing
        End If

        inicializarCtrl()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()


    End Sub



    Public Sub InicializarWS()
        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)
        inicializarCtrl()
    End Sub

    ''' <summary>
    ''' Inicializar el control de acceso como servidor
    ''' </summary>
    Public Sub IniServidor()
        'Cargo los lectores RFID
        CargarLectoresRFID()

    End Sub
    Public Sub IniCliente()
        CargarLectorRFIDCliente()
    End Sub

    Private Sub CargarLectorRFIDCliente()

        Dim oLectores As New List(Of Entidades.LECTOR_RFID)
        oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
        CtrlEstadoLectoresRFID1.CargarLectoresCliente(oLectores)
    End Sub

    ''' <summary>
    ''' Permite buscar los lectores RFID cargados en la base de datos
    ''' y cargarlos al sistema
    ''' </summary>
    Private Sub CargarLectoresRFID()
        'Busco los lectores
        Dim nLectorEFID As New Negocio.LectorRFID_N

        Dim listLectorRFID As List(Of Entidades.LECTOR_RFID) = nLectorEFID.GetAll(Me.ID_CONTROL_ACCESO)
        'listLectorRFID.Clear()

        If IsNothing(listLectorRFID) Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
                "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN DE PROCEDIMIENTO
        ElseIf listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
                "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")
            Return 'FIN DE PROCEDIMIENTO
        End If

        If listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
                "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN 
        End If

        Me.ListRFID_SpeedWay = Nothing
        'Recorro los lectores agregados para el puesto de trabajo
        For Each LectorRFID As Entidades.LECTOR_RFID In listLectorRFID
            'Creo el lector RFID SpeedWay para su utilización
            Dim RFID_SpeedWay As New Negocio.LectorRFID_SpeedWay(LectorRFID)

            If (IsNothing(Me.ListRFID_SpeedWay)) Then Me.ListRFID_SpeedWay = New List(Of Negocio.LectorRFID_SpeedWay)
            'Agrego el LectorRFID a la lista
            Me.ListRFID_SpeedWay.Add(RFID_SpeedWay)
            'Agrego los escuchadores para los evento del lector
            AddHandler RFID_SpeedWay.TagLeido, AddressOf Me.TagLeido
            AddHandler RFID_SpeedWay.ErrorRFID, AddressOf Me.ErrorRFID
            AddHandler RFID_SpeedWay.InfoRFID, AddressOf Me.InfoRFID


        Next

        CtrlEstadoLectoresRFID1.CargarLectores(Me.ListRFID_SpeedWay)
    End Sub

    ''' <summary>
    ''' Permite conectar el lector si esta desconectado
    ''' </summary>
    Private Sub ConectarLectoresRFID()

        'Recorro los lectores RFID Configurado
        For Each RFID_SpeedWay As LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            'Si el lector no esta conectado
            If Not RFID_SpeedWay.IsConnected Then
                Try
                    oLector = nLector.GetOne(RFID_SpeedWay.Ip)
                    oLector.CONECTADO = RFID_SpeedWay.Conectarse()
                    nLector.Update(oLector)
                Catch ex As Exception
                    'Surgio un error en la conexion
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, ex.Message)
                End Try
            Else
                'El lector ya se encuentra conectado
                Dim tmp As String = "[ " + DateTime.Now + " ] INFO: " + String.Format("El lector {0} se encuentra conectado", RFID_SpeedWay.Nombre)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            End If
        Next

    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        flpSectores.Controls.Clear()
        ListaCtrlSectorPorteria = New List(Of CtrlSectorPorteriaUnidireccional)
        Dim ctrlSectorPorteria As CtrlSectorPorteriaUnidireccional
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR
            ctrlSectorPorteria = New CtrlSectorPorteriaUnidireccional(itemSector)
            flpSectores.Controls.Add(ctrlSectorPorteria)
            ListaCtrlSectorPorteria.Add(ctrlSectorPorteria)
            AddHandler ctrlSectorPorteria.EventTagLeidoManual, AddressOf Me.TagLeidoManual
        Next
    End Sub




#End Region

#Region "SUBPROCESOS"

    ''' <summary>
    ''' Permite realizar el monitoreo del control de acceso revisando todos los sectores , barreras , lectores RFID
    ''' asignados
    ''' </summary>
    Public Sub Monitoreo()

        'Apenas comienza el hilo conecto los lectores
        If Not IsNothing(Me.ListRFID_SpeedWay) Then ConectarLectoresRFID()

        While True

            Try

                Dim oListaComando As New List(Of Entidades.COMANDO)
                Dim oComandoN As New Negocio.ComandoN


                Dim oLector As New LECTOR_RFID
                Dim oLectorN As New Negocio.LectorRFID_N

                oListaComando = oComandoN.GetAll(Me.ID_CONTROL_ACCESO)
                For Each item As COMANDO In oListaComando

                    Select Case item.ID_TIPO_COMANDO
                        Case CInt(Constante.TIPO_COMANDO.REINICIAR_CONTROL)

                        Case CInt(Constante.TIPO_COMANDO.REINICIAR_SECTOR)

                        Case CInt(Constante.TIPO_COMANDO.HABILITAR_TODO)

                        Case CInt(Constante.TIPO_COMANDO.ESCRIBIR_CARTEL)

                        Case CInt(Constante.TIPO_COMANDO.LECTURA_MANUAL)
                            oLector = oLectorN.GetOne(Convert.ToInt32(item.ID_SECTOR))
                            TagLeido(item.TAG, 1, oLector, True)

                        Case Else 'Generar auditoría de que no se pudo identificar el tipo de comando

                    End Select

                    oComandoN.ActualizarComando(item.ID_COMANDO)
                Next
                CtrlEstadoLectoresRFID1.RefrescarEstadosRFID()
            Catch ex As Exception
                Dim msj As String = String.Format("{0} - {1}", "[Monitoreo]", ex.Message)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)

            End Try

            Thread.Sleep(Me.ControlAcceso.MONITOREO)
        End While

    End Sub


#End Region

#Region "LECTOR RFID"

    ''' <summary>
    ''' Evento que se ejecuta cuando se leyo un tag 
    ''' </summary>
    ''' <param name="TagRFID"></param>
    ''' <param name="NumAntena"></param>
    ''' <remarks></remarks>
    Public Sub TagLeido(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim SUB_TAG As String = "[TagLeido]"
        Console.WriteLine("TAG RFID LEIDO: " & TagRFID)

        Dim nSector As New Negocio.SectorN
        Dim ID_SECTOR As Integer = 0

        'Busco el sector al que corresponde la antena
        Dim nAntena As New Negocio.Antena_RfidN
        Dim oAntena As Entidades.ANTENAS_RFID = Nothing '= nAntena.GetOne(NumAntena, LectorRFID.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)

        Dim oConfigSeleccionada As Entidades.CONFIG_LECTOR_RFID = (From a In LectorRFID.CONFIG_LECTOR_RFID Where a.SELECCIONADO = True).SingleOrDefault()

        If Not IsNothing(oConfigSeleccionada) Then
            oAntena = nAntena.GetOne(NumAntena, oConfigSeleccionada.ID_CONF_LECTOR_RFID)
        End If

        If IsNothing(oAntena) Then
            Dim tmp As String = String.Format("{0} La antena {1} no esta habilitada", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        Else
            If Not IsNothing(oAntena.ID_SECTOR) Then ID_SECTOR = oAntena.ID_SECTOR

        End If



        If ID_SECTOR = 0 Then 'No tiene un sector asignado la antena
            Dim tmp As String = String.Format("{0} La antena {1} no tiene ningun sector asignado", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        End If

        Dim oCtrlSector As CtrlSectorPorteriaUnidireccional = (From a In ListaCtrlSectorPorteria Where a.ID_SECTOR = ID_SECTOR).SingleOrDefault()





        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = oCtrlSector.CtrlAtenaLeyendo
        If Not IsNothing(ctrlAntLeyendo) Then
            ctrlAntLeyendo.TagLeyendo(TagRFID)
        Else
            Dim tmp As String = String.Format("{0} No se encontro el CtrlAntenaLeyendo", SUB_TAG)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End If

        'Obtengo el buffer
        Dim Buffer As Entidades.BufferTag = oCtrlSector.Buffer

        'Agrego al buffer al tag leido y  obtengo el dialog cuando agregue al buffer el tagrfid
        Dim _BufferDIalog As Entidades.BufferTag.BufferDialog = Buffer.AddTagLeido(TagRFID)

        If _BufferDIalog = BufferTag.BufferDialog.TagLeido Then Return ' Termino el procedimiento
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.LecturaTag, "[ " + DateTime.Now + " ] TAG LEÍDO : " & TagRFID,
                                                     ID_SECTOR, TagRFID)


        'EJECUTO EL PROCEDIMINETO "EstaHabilitado" PARA SABER SI EL TAG LEIDO ESTA HABILITADO A INGRESAR O SALIR DE LA PORTERIA
        oCtrlSector.EstaHabilitado(TagRFID)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta para  informa un error en el Lector RFID
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub ErrorRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, Msj)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando el lector envia un mensaje de información
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub InfoRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, Msj, idSector)
    End Sub

    Public Sub EventoGPO(Puerto As Integer, Estado As Boolean, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay)

        If Estado = False Then 'Si el estado del sensor es TRUE (Cortando)

            If Not LectorRFID_SpeedWay.IsStar Then 'Y no esta leyendo

                LectorRFID_SpeedWay.StartRead() ' Comenzar a leer

            End If

        Else ' Estado = False (No cortando)
            LectorRFID_SpeedWay.ConsultarSensoresLectura(TipoConexion.NormalAbierto)

        End If


    End Sub

    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub

#End Region

#Region "Otros Eventos"

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Negocio.AuditoriaN.AddAuditoria("Reseteo Control de acceso Balanza", "", Me.ID_CONTROL_ACCESO, 0, WS_ERRO_USUARIO, Constante.acciones.ResetControlAcceso, 0)
        If Not IsNothing(ListRFID_SpeedWay) Then
            For Each lector As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
                lector.Disconnect()
            Next
            ListRFID_SpeedWay.Clear()
        End If
        Inicializar()
    End Sub

    Private Sub btnLectorRfid_Click(sender As Object, e As EventArgs) Handles btnLectorRfid.Click

    End Sub

#End Region
End Class
