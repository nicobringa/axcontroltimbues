﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Newtonsoft.Json;
namespace AxControlWeb.Controllers
{
    public class LECTOR_RFIDController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();

        public string GetObjLectorJson(int idControlAcceso)
        {
            return JsonConvert.SerializeObject(db.LECTOR_RFID.Where(a => a.ID_CONTROL_ACCESO == idControlAcceso).FirstOrDefault(), Formatting.Indented,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        public string GetConfigLectorJson(int idLectorRFID)
        {
            return JsonConvert.SerializeObject(db.CONFIG_LECTOR_RFID.Where(a => a.ID_LECTOR_RFID == idLectorRFID).FirstOrDefault(), Formatting.Indented,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        public string GetListAntenasJson(int idConfiguracion)
        {
            List<ANTENAS_RFID> ListAntenas = db.ANTENAS_RFID.Where(a => a.ID_CONF_LECTOR_RFID == idConfiguracion).ToList();
            return JsonConvert.SerializeObject(ListAntenas, Formatting.Indented,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }



        //GET
        public ActionResult ConfigurarLectorRFID(int idControlAcceso)
        {
            LECTOR_RFID oLector = db.LECTOR_RFID.Where(a => a.ID_CONTROL_ACCESO == idControlAcceso).FirstOrDefault();

            List<MODELO_LECTOR_RFID> ListModelosLector = db.MODELO_LECTOR_RFID.ToList();
            ViewBag.ListModelosLector = ListModelosLector;
            return View();
        }

        [HttpPost]
        public ActionResult ConfigurarLectorRFID(objLectorRFID objLector)
        {

            return View();
        }

        public PartialViewResult EstadoLector(int? idSector)
        {
            var LectorRFID = db.LECTOR_RFID.FirstOrDefault(a => a.ID_LECTOR_RFID == idSector);
            ViewBag.LectorRFID = LectorRFID;
            return PartialView(LectorRFID);
        }

        // GET: Lectores_RFID/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LECTOR_RFID lector_rfid = db.LECTOR_RFID.Find(id);
            if (lector_rfid == null)
            {
                return HttpNotFound();
            }
            CONFIG_LECTOR_RFID oConfig = db.CONFIG_LECTOR_RFID.Where(a => a.ID_LECTOR_RFID == lector_rfid.ID_LECTOR_RFID).FirstOrDefault();
            ViewBag.oConfig = oConfig;
            List<ANTENAS_RFID> ListAntena = db.ANTENAS_RFID.Where(a => a.ID_CONF_LECTOR_RFID == oConfig.ID_CONF_LECTOR_RFID).ToList();
            ViewBag.ListAntena = ListAntena;
            ViewBag.ID_MODELO_LECTOR = new SelectList(db.MODELO_LECTOR_RFID, "ID_MODELO_LECTOR_RFID", "NOMBRE", lector_rfid.ID_MODELO_LECTOR);
            ViewBag.ID_CONTROL_ACCESO = new SelectList(db.CONTROL_ACCESO, "ID_CONTROL_ACCESO", "NOMBRE", lector_rfid.ID_CONTROL_ACCESO);
            ViewBag.TIPO_LECTURA = new SelectList(db.TIPO_LECTURA, "ID_TIPO_LECTURA", "DESCRIPCION", lector_rfid.TIPO_LECTURA);
            return View(lector_rfid);
        }

        // POST: Lectores_RFID/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "ID_LECTOR_RFID,NOMBRE,IP,ID_MODELO_LECTOR,TIPO_LECTURA,ID_CONTROL_ACCESO,AUTO_CONECTARSE,AUTO_START")]LECTOR_RFID lector_rfid)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lector_rfid).State = EntityState.Modified;
                lector_rfid.CONECTADO = 0;
                lector_rfid.LEYENDO = 0;
                db.SaveChanges();
                //return RedirectToAction("Index");
                ComandosController oComC = new ComandosController();
                //oComC.fnGenerarComando("AplicarConfigRFID_10", "");
                return RedirectToAction("Editar", "LECTOR_RFID", new { id = lector_rfid.ID_LECTOR_RFID });
                //return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
            }
            ViewBag.config_lector_rfid = db.CONFIG_LECTOR_RFID.Where(a => a.ID_LECTOR_RFID == lector_rfid.ID_LECTOR_RFID).FirstOrDefault();
            ViewBag.idControlAcceso = new SelectList(db.CONTROL_ACCESO, "idControlAcceso", "nombre", lector_rfid.ID_CONTROL_ACCESO);
            ViewBag.idModeloLectorRFID = new SelectList(db.MODELO_LECTOR_RFID, "idModeloLectorRFID", "nombre", lector_rfid.ID_MODELO_LECTOR);
            return View(lector_rfid);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
