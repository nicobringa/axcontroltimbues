﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfLectoresRFID
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfLectoresRFID))
        Me.flpLectoresRFID = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.flpLectoresRFID.SuspendLayout()
        Me.SuspendLayout()
        '
        'flpLectoresRFID
        '
        Me.flpLectoresRFID.AutoScroll = True
        Me.flpLectoresRFID.Controls.Add(Me.FlowLayoutPanel2)
        Me.flpLectoresRFID.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpLectoresRFID.Location = New System.Drawing.Point(0, 0)
        Me.flpLectoresRFID.Name = "flpLectoresRFID"
        Me.flpLectoresRFID.Size = New System.Drawing.Size(1284, 602)
        Me.flpLectoresRFID.TabIndex = 0
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(200, 0)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'FrmConfLectoresRFID
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1284, 602)
        Me.Controls.Add(Me.flpLectoresRFID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmConfLectoresRFID"
        Me.Text = "FrmConfLectoresRFID"
        Me.flpLectoresRFID.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents flpLectoresRFID As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
End Class
