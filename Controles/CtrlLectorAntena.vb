﻿Imports Impinj.OctaneSdk
Imports Protocolo.LectorRFID
Public Class CtrlLectorAntena
#Region "PROPIEDADES"
    Private oModeloLectorN As New Negocio.Modelo_LectorRfidN
#End Region
    Dim i, cantAntenas As Integer

    Dim lblNombreAntena(99) As Label
    Dim txtNombreSector(99) As TextBox
    Dim btnAsignar(99) As Button
    Dim modeloActual As Integer
    Dim oLector As New Entidades.Lector_RFID
    Dim oLectorN As New Negocio.LectorRFID_N
    ' Dim oAsignacion As New Entidades.asignacion_antena
    Dim oAsignacionN As New Negocio.Asignacion_AntenaN
    Dim _nombreSector As String
    Dim nLectorInvNeg As Negocio.LectorRFID_Invengo

#Region "Constructor"

    Public Sub New()
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
    End Sub

    Public Sub New(ByVal lectorRFID As Negocio.LectorRFID_SpeedWay)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        Me._LectorRFID_SpeedWay = lectorRFID
        RefrescarFormularioSpeedway()
    End Sub

    Public Sub New(ByVal lectorRFID As Negocio.LectorRFID_Invengo)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        Me._LectorRFID_Invengo = lectorRFID
        RefrescarFormularioInvengo()
    End Sub

#End Region

#Region "EVENTOS FORM"

    Private WithEvents _LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay
    Private WithEvents _LectorRFID_Invengo As Negocio.LectorRFID_Invengo
    ' Private WithEvents _Config_lector_rfid_invengoN As New Negocio.Config_lector_rfid_invengoN

    Private _RunnigLector As Boolean
    Public Property Running() As Boolean
        Get
            Return _RunnigLector
        End Get
        Set(ByVal value As Boolean)
            _RunnigLector = value
            If value Then
                btnStar.BackColor = Color.Red
                SetBtn(btnStar, "Stop", Color.Red)
                SetTextLabel(Negocio.LectorRFID_SpeedWay.TEXT_START, lblLectorLectura, Negocio.LectorRFID_SpeedWay.COLOR_START)
            Else
                btnStar.BackColor = Color.Blue
                SetBtn(btnStar, "Start", Color.Blue)
                SetTextLabel(Negocio.LectorRFID_SpeedWay.TEXT_STOP, lblLectorLectura, Negocio.LectorRFID_SpeedWay.COLOR_STOP)
            End If
        End Set
    End Property
    ''' <summary>
    ''' Cuando cambio el texto del nombre del lector, lo replico en el label de las antenas
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        lblLector.Text = txtNombre.Text
    End Sub

    Private Sub cmbModelo_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbModelo.SelectionChangeCommitted
        CargarControles(cmbModelo.SelectedValue)
    End Sub

    Private Sub CtrlLectorAntena_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarCombos()
        cmbModelo.SelectedIndex = -1
    End Sub
#End Region


#Region "METODOS"

    Private Sub CargarCombos()
        Dim oModeloLectorN As New Negocio.Modelo_LectorRfidN
        oModeloLectorN.CargarCombo(cmbModelo)
    End Sub

    ''' <summary>
    ''' Evento click de TODOS los botones del control de antenas, que al llamar al formulario de lista de sectores le envía la antena de la que se hizo click.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnAsignar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)



        If txtIP.Text.Length = 0 Then
            MsgBox("Antes de asignar la antena, debe ingresar el IP del lector")
            txtIP.Focus()
            Exit Sub
        End If

        oLector = oLectorN.GetOne(txtIP.Text)

        Dim frm As New frmListaSectores(oLector.ID_LECTOR_RFID, sender.Tag.ToString(), True)
        frm.ShowDialog()
        CargarControles(cmbModelo.SelectedValue)
    End Sub

    Private Sub btnAsignarPuerto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)



        If txtIP.Text.Length = 0 Then
            MsgBox("Antes de asignar la antena, debe ingresar el IP del lector")
            txtIP.Focus()
            Exit Sub
        End If

        oLector = oLectorN.GetOne(txtIP.Text)

        Dim frm As New frmListaSectores(oLector.ID_LECTOR_RFID, sender.Tag.ToString(), False)
        frm.ShowDialog()
        CargarControles(cmbModelo.SelectedValue)
    End Sub
#End Region

#Region "SUBPROCESOS"

#End Region


#Region "Configuración Lector"

#Region "Delegados"

    Delegate Sub DelSetTextLabel(ByVal texto As String, ByVal lbl As Label, ByVal _Color As Drawing.Color)
    Private Sub SetTextLabel(ByVal texto As String, ByVal lbl As Label, ByVal _Color As Drawing.Color)
        If lbl.InvokeRequired Then
            Dim Delegado As New DelSetTextLabel(AddressOf SetTextLabel)
            Me.Invoke(Delegado, New Object() {texto, lbl, _Color})
        Else
            lbl.Text = texto
            lbl.ForeColor = _Color
        End If
    End Sub

    Delegate Sub DelSetBtn(ByVal btn As Button, ByVal texto As String, ByVal _Color As Drawing.Color)
    Private Sub SetBtn(ByVal btn As Button, ByVal texto As String, ByVal _Color As Drawing.Color)
        If btn.InvokeRequired Then
            Dim Delegado As New DelSetBtn(AddressOf SetBtn)
            Me.Invoke(Delegado, New Object() {btn, texto, _Color})
        Else
            btn.Text = texto
            btn.ForeColor = Color.White
            btn.BackColor = _Color
        End If
    End Sub
#End Region



#Region "Metodos"

    Public Sub CargarLectorRFID(ByVal lectorRFID As Negocio.LectorRFID_SpeedWay)
        Me._LectorRFID_SpeedWay = lectorRFID
        RefrescarFormularioSpeedway()

    End Sub




    Private Sub RefrescarFormularioSpeedway()

        If IsNothing(_LectorRFID_SpeedWay) Then Return

        If _LectorRFID_SpeedWay.IsConnected Then
            btnConectarDesconectar.Text = "Desconectar"
            SetTextLabel("Conectado", lblLectorConexion, Color.GreenYellow)
            VerEstadoAntenaPuerto()
        Else
            btnConectarDesconectar.Text = "Conectar"
            SetTextLabel("Desconectado", lblLectorConexion, Color.OrangeRed)
        End If

        If _LectorRFID_SpeedWay.LectorConfigurado Then
            txtIP.Text = _LectorRFID_SpeedWay.Ip
            txtNombre.Text = _LectorRFID_SpeedWay.Nombre
            txtDescripcion.Text = _LectorRFID_SpeedWay.Descripcion
            cbxAutoConectarse.Checked = _LectorRFID_SpeedWay.LectorRFID.AUTO_CONECTARSE
            cbxAutoStart.Checked = _LectorRFID_SpeedWay.LectorRFID.AUTO_START

            CargarConfiguracion()

            If _LectorRFID_SpeedWay.IsConnected Then
                Running = _LectorRFID_SpeedWay.isRunning
            End If


        Else
            txtNombre.Text = _LectorRFID_SpeedWay.Nombre
            MessageBox.Show("El Lector RFID no se encuentra configurado para su funcionamiento", "Configuracón RFID", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    Private Sub RefrescarFormularioInvengo()

        btnAgrConfiguracion.Visible = False

        If IsNothing(_LectorRFID_Invengo.readerInvengo) Then Return
        'If IsNothing(_LectorRFID_Invengo) Then Return
        If _LectorRFID_Invengo.isConnected() Then
            btnConectarDesconectar.Text = "Desconectar"
            SetTextLabel("Conectado", lblLectorConexion, Color.GreenYellow)
            'VerEstadoAntenaPuerto()
        Else
            btnConectarDesconectar.Text = "Conectar"
            SetTextLabel("Desconectado", lblLectorConexion, Color.OrangeRed)
        End If

        If _LectorRFID_Invengo.LectorConfigurado Then
            txtIP.Text = _LectorRFID_Invengo.Ip
            txtNombre.Text = _LectorRFID_Invengo.Nombre
            'txtDescripcion.Text = _LectorRFID_Invengo.Descripcion
            cbxAutoConectarse.Checked = _LectorRFID_Invengo.LectorRFID.AUTO_CONECTARSE
            cbxAutoStart.Checked = _LectorRFID_Invengo.LectorRFID.AUTO_START


            CargarConfiguracion()

            If _LectorRFID_Invengo.isConnected() Then
                Running = _LectorRFID_Invengo.isConnected()
            End If


        Else
            txtNombre.Text = _LectorRFID_Invengo.Nombre
            MessageBox.Show("El Lector RFID no se encuentra configurado para su funcionamiento", "Configuracón RFID", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    Private Sub CargarConfiguracion()
        flpConf.Controls.Clear()

        If Not IsNothing(_LectorRFID_SpeedWay) Then
            For Each oConf As Entidades.Config_Lector_RFID In _LectorRFID_SpeedWay.LectorRFID.config_lector_rfid
                'Creo el control de configuracion
                Dim ctrlConf As New Controles.CtrlConfRFID(oConf)
                flpConf.Controls.Add(ctrlConf)
            Next
        Else
            'If Not IsNothing(_LectorRFID_Invengo) Then
            '    For Each oConf As Entidades.Config_lector_rfid_invengo In _LectorRFID_Invengo.LectorRFID.config_lector_rfid_invengo

            '        'Creo el control de configuracion
            '        Dim ctrlConf As New Controles.CtrlConfRFID_Invengo(oConf, _LectorRFID_Invengo, _Config_lector_rfid_invengoN)
            '        flpConf.Controls.Add(ctrlConf)

            '    Next
            'End If
        End If
    End Sub


    Private Sub SetLabelAntena(ByVal lbl As Label, ByVal AntenaConectada As Boolean)
        If AntenaConectada Then
            lbl.Text = Negocio.LectorRFID_SpeedWay.TEXT_CONECTADO
            lbl.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_CONECTADO
        Else 'Antena Desconectada
            lbl.Text = Negocio.LectorRFID_SpeedWay.TEXT_DESCONECTADO
            lbl.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_DESCONECTADO
        End If

    End Sub



    Private Sub VerEstadoAntenaPuerto()
        Try

            Dim LectorEstado As Status = _LectorRFID_SpeedWay.QueryStatus

            For Each AntSatus As AntennaStatus In LectorEstado.Antennas

                If AntSatus.PortNumber = 1 Then
                    SetLabelAntena(lblEstadoAntena1, AntSatus.IsConnected)
                ElseIf AntSatus.PortNumber = 2 Then
                    SetLabelAntena(lblEstadoAntena2, AntSatus.IsConnected)
                ElseIf AntSatus.PortNumber = 3 Then
                    SetLabelAntena(lblEstadoAntena3, AntSatus.IsConnected)
                Else ' 4
                    SetLabelAntena(lblEstadoAntena4, AntSatus.IsConnected)
                End If

            Next

            lblTemperatura.Text = LectorEstado.TemperatureInCelsius & "°"

            For Each itemGpio As GpiStatus In LectorEstado.Gpis
                Dim lblEstadoPuerto As New Label
                Select Case itemGpio.PortNumber

                    Case 1
                        lblEstadoPuerto = lblEstadoPuerto1
                    Case 2
                        lblEstadoPuerto = lblEstadoPuerto2
                    Case 3
                        lblEstadoPuerto = lblEstadoPuerto3
                    Case 4
                        lblEstadoPuerto = lblEstadoPuerto4

                    Case Else
                        lblEstadoPuerto = Nothing
                End Select

                If Not IsNothing(lblEstadoPuerto) Then lblEstadoPuerto.Text = If(itemGpio.State, "1", "0")


            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message & " - " & ex.StackTrace)
        End Try
    End Sub

    ''' <summary>
    ''' Evento que carga los controles dinamicamente de acuerdo al modelo de lector que se haya seleccionado.
    ''' </summary>
    ''' <param name="modelo"></param>
    Private Sub CargarControles(ByVal modelo As Integer)
        Try


            ''Limpio primero para que no superponga los controles
            pnlAntenas.Controls.Clear()
            cantAntenas = oModeloLectorN.GetCantAnetnas(modelo)
            oLector = Me._LectorRFID_SpeedWay.LectorRFID
            'Si no tiene configuracion no puede asignar los sectores a las antenas y sectores
            If IsNothing(oLector.config_lector_rfid(0)) Then Return



            For i = 1 To cantAntenas
                ''Agrego un label, para mostrar el nombre de la antena a asignar
                lblNombreAntena(i) = New Label
                lblNombreAntena(i).Width = 77
                lblNombreAntena(i).Height = 17
                lblNombreAntena(i).AutoSize = False
                lblNombreAntena(i).BorderStyle = BorderStyle.None
                lblNombreAntena(i).Font = New Font("Arial", 10, FontStyle.Bold)
                lblNombreAntena(i).Location = New Point(7, i * 32)
                lblNombreAntena(i).Text = "Antena " & i
                Me.pnlAntenas.Controls.Add(lblNombreAntena(i))

                ''Muestro el nombre del sector, que se le está asignado a la antena
                txtNombreSector(i) = New TextBox
                txtNombreSector(i).Width = 176
                txtNombreSector(i).Height = 20
                txtNombreSector(i).AutoSize = False
                txtNombreSector(i).ReadOnly = True
                txtNombreSector(i).BorderStyle = BorderStyle.FixedSingle
                txtNombreSector(i).Font = New Font("Arial", 10, FontStyle.Bold)
                txtNombreSector(i).Location = New Point(85, i * 32)

                ''Busco el lector actual para operar posteriormente
                If txtIP.Text.Length > 0 Then oLector = oLectorN.GetOne(txtIP.Text)
                ''busco si ya tiene sector asignado la antena 

                Dim nAntena As New Negocio.Antena_RfidN
                Dim oAntena As Entidades.ANTENAS_RFID = Nothing
                If oLector.CONFIG_LECTOR_RFID.Count > 0 Then oAntena = nAntena.GetOne(i, oLector.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)


                If IsNothing(oAntena) Then
                    txtNombreSector(i).Text = ""
                Else
                    If Not IsNothing(oAntena.ID_SECTOR) Then
                        Dim nSector As New Negocio.SectorN
                        oAntena.SECTOR = nSector.GetOne(oAntena.ID_SECTOR)
                        txtNombreSector(i).Text = oAntena.sector.nombre
                    End If
                End If
                Me.pnlAntenas.Controls.Add(txtNombreSector(i))


                ''Agrego un botón, para realizar la asignación
                btnAsignar(i) = New Button
                btnAsignar(i).Width = 70
                btnAsignar(i).Height = 23
                btnAsignar(i).AutoSize = False
                btnAsignar(i).Font = New Font("Arial", 10, FontStyle.Bold)
                btnAsignar(i).Location = New Point(270, i * 32)
                btnAsignar(i).Text = "Asignar"
                btnAsignar(i).Tag = i
                AddHandler btnAsignar(i).Click, AddressOf btnAsignar_Click
                Me.pnlAntenas.Controls.Add(btnAsignar(i))



            Next


            For i = 1 To 4
                Dim lblNombrePuerto As New Label
                Dim txtPuertoSector As New TextBox
                Dim btnAsignarSector As New Button


                lblNombrePuerto.Width = 77
                lblNombrePuerto.Height = 17
                lblNombrePuerto.AutoSize = False
                lblNombrePuerto.BorderStyle = BorderStyle.None
                lblNombrePuerto.Font = New Font("Arial", 10, FontStyle.Bold)
                lblNombrePuerto.Location = New Point(7, (i + cantAntenas) * 32)
                lblNombrePuerto.Text = "Puerto " & i
                Me.pnlAntenas.Controls.Add(lblNombrePuerto)


                txtPuertoSector.Width = 176
                txtPuertoSector.Height = 20
                txtPuertoSector.AutoSize = False
                txtPuertoSector.ReadOnly = True
                txtPuertoSector.BorderStyle = BorderStyle.FixedSingle
                txtPuertoSector.Font = New Font("Arial", 10, FontStyle.Bold)
                txtPuertoSector.Location = New Point(85, (i + cantAntenas) * 32)

                Dim nPuerto As New Negocio.Puerto_RfidN
                Dim oPuerto As Entidades.PUERTO_RFID = nPuerto.GetOne(i, oLector.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)
                If Not IsNothing(oPuerto) Then txtPuertoSector.Text = oPuerto.sector.nombre

                Me.pnlAntenas.Controls.Add(txtPuertoSector)

                ''Agrego un botón, para realizar la asignación

                btnAsignarSector.Width = 70
                btnAsignarSector.Height = 23
                btnAsignarSector.AutoSize = False
                btnAsignarSector.Font = New Font("Arial", 10, FontStyle.Bold)
                btnAsignarSector.Location = New Point(270, (i + cantAntenas) * 32)
                btnAsignarSector.Text = "Asignar"
                btnAsignarSector.Tag = i
                AddHandler btnAsignarSector.Click, AddressOf btnAsignarPuerto_Click

                Me.pnlAntenas.Controls.Add(btnAsignarSector)


            Next

        Catch ex As Exception
            Dim errores As String = ex.Message
            'Throw
        End Try
    End Sub


#End Region

#Region "Eventos"
    Private Sub btnConectarDesconectar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConectarDesconectar.Click

        If Not IsNothing(_LectorRFID_Invengo) Then
            If txtIP.Text.StartsWith(_LectorRFID_Invengo.Ip) Then
                Try
                    If btnConectarDesconectar.Text = "Desconectar" Then
                        Me._LectorRFID_Invengo.StopRead()
                        Me._LectorRFID_Invengo.Disconnect()
                        btnStar.BackColor = Negocio.LectorRFID_Invengo.COLOR_START
                        btnStar.Text = Negocio.LectorRFID_Invengo.TEXT_START
                    Else ' Conectar
                        Me._LectorRFID_Invengo.Conectar()
                        If cbxAutoStart.Checked Then
                            Me._LectorRFID_Invengo.StartRead()
                            btnStar.BackColor = Negocio.LectorRFID_Invengo.COLOR_STOP
                            btnStar.Text = Negocio.LectorRFID_Invengo.TEXT_STOP
                        End If
                    End If
                    RefrescarFormularioInvengo()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Ctrl Lector RFID", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        End If

        If Not IsNothing(_LectorRFID_SpeedWay) Then
            If txtIP.Text = _LectorRFID_SpeedWay.Ip Then
                Try
                    If btnConectarDesconectar.Text = "Desconectar" Then
                        Me._LectorRFID_SpeedWay.Disconnect()
                    Else ' Conectar
                        Me._LectorRFID_SpeedWay.Connect(Me._LectorRFID_SpeedWay.Ip)
                    End If
                    RefrescarFormularioSpeedway()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Ctrl Lector RFID", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        End If

    End Sub

    Private Sub txt_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.Leave, txtIP.Leave
        Dim txt As TextBox = CType(sender, TextBox)

        If IsNothing(_LectorRFID_SpeedWay) Then Return
        Select Case txt.Name
            Case "txtNombre"
                _LectorRFID_SpeedWay.Nombre = txt.Text
                _LectorRFID_SpeedWay.LectorRFID.nombre = txt.Text
            Case "txtIP"
                _LectorRFID_SpeedWay.Ip = txtIP.Text
                _LectorRFID_SpeedWay.LectorRFID.ip = txtIP.Text

        End Select
    End Sub

    Private Sub ctrlLectorRFID_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CargarCombos()
        cmbModelo.SelectedIndex = -1

        If Not IsNothing(Me._LectorRFID_SpeedWay) Then
            cmbModelo.SelectedValue = Me._LectorRFID_SpeedWay.LectorRFID.MODELO_LECTOR_RFID.ID_MODELO_LECTOR_RFID
            CargarControles(cmbModelo.SelectedValue)
        End If

        If Not IsNothing(Me._LectorRFID_Invengo) Then
            cmbModelo.SelectedValue = Me._LectorRFID_Invengo.LectorRFID.MODELO_LECTOR_RFID.ID_MODELO_LECTOR_RFID
        End If

    End Sub

#Region "Eventos Lector"

    Private Sub Lectura_Start() Handles _LectorRFID_SpeedWay.ReaderStarted
        Running = True
    End Sub

    Private Sub Lectura_Stop() Handles _LectorRFID_SpeedWay.ReaderStopped
        SetTextLabel("Stop", lblLectorLectura, Color.OrangeRed)
        Running = False
    End Sub

    Private Sub ConexionPerdida() Handles _LectorRFID_SpeedWay.ConnectionLost
        SetTextLabel("Desconectado", lblLectorConexion, Color.OrangeRed)
    End Sub

#End Region

#End Region


    Private Sub btnAgrConfiguracion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgrConfiguracion.Click
        If Not IsNothing(_LectorRFID_SpeedWay) Then
            Dim oConfiguracion As New Entidades.CONFIG_LECTOR_RFID
            'Aplico configuracion por defecto
            oConfiguracion.SELECT_READER_MODE = ReaderMode.AutoSetDenseReader
            oConfiguracion.SELECT_REPORT_MODE = ReportMode.WaitForQuery
            oConfiguracion.SELECT_SEARCH = SearchMode.DualTarget
            oConfiguracion.SELECT_SESSION = 1
            oConfiguracion.ID_LECTOR_RFID = Me._LectorRFID_SpeedWay.LectorRFID.ID_LECTOR_RFID
            If IsNothing(_LectorRFID_SpeedWay.LectorRFID.config_lector_rfid) Then _LectorRFID_SpeedWay.LectorRFID.config_lector_rfid = New List(Of Entidades.Config_Lector_RFID)

            _LectorRFID_SpeedWay.LectorRFID.config_lector_rfid.Add(oConfiguracion)
        Else

        End If
        CargarConfiguracion()
    End Sub

    Private Sub Guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Guardar.Click

        If Not IsNothing(_LectorRFID_Invengo) Then
            Try
                If Me._LectorRFID_Invengo.GuardarConf() Then
                    MessageBox.Show("Operacion realizada con exito", "Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("Surgio un error, no se pudo realizar la operación", "Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "GuardarConf", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        Else
            MessageBox.Show("No hay lector conectado")
        End If

        If Not IsNothing(_LectorRFID_SpeedWay) Then
            Try
                If Me._LectorRFID_SpeedWay.GuardarConf() Then
                    MessageBox.Show("Operacion realizada con exito", "Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("Surgio un error, no se pudo realizar la operación", "Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "GuardarConf", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End If


    End Sub

    Private Sub cbxAutoConectarse_CheckedChanged(sender As Object, e As EventArgs) Handles cbxAutoConectarse.CheckedChanged
        If Not IsNothing(_LectorRFID_SpeedWay) Then
            _LectorRFID_SpeedWay.LectorRFID.AUTO_CONECTARSE = cbxAutoConectarse.Checked
        End If
        If Not IsNothing(_LectorRFID_Invengo) Then
            _LectorRFID_Invengo.LectorRFID.AUTO_CONECTARSE = cbxAutoConectarse.Checked
        End If
    End Sub

    Private Sub cbxAutoStart_CheckedChanged(sender As Object, e As EventArgs) Handles cbxAutoStart.CheckedChanged
        If Not IsNothing(_LectorRFID_SpeedWay) Then
            _LectorRFID_SpeedWay.LectorRFID.AUTO_START = cbxAutoStart.Checked
        End If
        If Not IsNothing(_LectorRFID_Invengo) Then
            _LectorRFID_Invengo.LectorRFID.AUTO_START = cbxAutoStart.Checked
        End If
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        VerEstadoAntenaPuerto()
    End Sub

    Private Sub btnStar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStar.Click

        If Not IsNothing(_LectorRFID_SpeedWay) Then
            If _LectorRFID_SpeedWay.IsConnected Then
                If btnStar.Text = "Start" Then
                    Dim frm As New FrmSeleccionConfRFID(_LectorRFID_SpeedWay.LectorRFID.config_lector_rfid.ToList())
                    If frm.ShowDialog = DialogResult.OK Then
                        Try
                            _LectorRFID_SpeedWay.AplicarConfiguracion(frm._ConfSeleccionada)
                            _LectorRFID_SpeedWay.StartRead()
                            btnStar.BackColor = Negocio.LectorRFID_SpeedWay.COLOR_STOP
                            btnStar.Text = Negocio.LectorRFID_SpeedWay.TEXT_STOP
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "Aplicar Configuración", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End Try
                    End If
                Else
                    _LectorRFID_SpeedWay.StopRead()
                    btnStar.BackColor = Negocio.LectorRFID_SpeedWay.COLOR_START
                    btnStar.Text = Negocio.LectorRFID_SpeedWay.TEXT_START
                End If
            Else
                MessageBox.Show("No se encuentra conectado el lector", "Star", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

        If Not IsNothing(_LectorRFID_Invengo) Then
            If _LectorRFID_Invengo.isConnected() Then
                If btnStar.Text = "Start" Then
                    Try
                        _LectorRFID_Invengo.StopRead()
                        _LectorRFID_Invengo.Disconnect()
                        _LectorRFID_Invengo.Conectar()
                        _LectorRFID_Invengo.StartRead()
                        btnStar.BackColor = Negocio.LectorRFID_Invengo.COLOR_STOP
                        btnStar.Text = Negocio.LectorRFID_Invengo.TEXT_STOP
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "Aplicar Configuración", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Try
                Else
                    _LectorRFID_Invengo.StopRead()
                    btnStar.BackColor = Negocio.LectorRFID_Invengo.COLOR_START
                    btnStar.Text = Negocio.LectorRFID_Invengo.TEXT_START
                End If
            Else
                MessageBox.Show("No se encuentra conectado el lector", "Star", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If


    End Sub



#End Region

End Class
