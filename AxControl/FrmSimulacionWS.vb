﻿Imports Entidades.Constante
Public Class FrmSimulacionWS



    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' RefrescarComboSector(cbSectoresEstaHabilitado)

    End Sub



    Private Sub RefrescarComboSector(ByVal combo As ComboBox)
        'combo.DataSource = [Enum].GetValues(GetType(Integer))


    End Sub

    Private Sub btnEnviarEstaHabilitado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviarEstaHabilitado.Click

        Try
            Dim idSector As Integer = txtidSectorWS.Text

            Dim AxRespuesta As Entidades.AxRespuesta = Negocio.WebServiceBitN.HabilitarTag(idSector, txtTag.Text, cbxSimularRespuesta.Checked)


            Respuesta(AxRespuesta)
        Catch ex As Exception

            MessageBox.Show(ex.Message, "EnviarEstaHabilitado", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
     
    End Sub

    Private Sub btnEnviarPesoObtenido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviarPesoObtenido.Click

        Try
            Dim idSector As Integer = txtidSectorWS.Text
            Dim axRespuesta As Entidades.AxRespuesta = Negocio.WebServiceBitN.RegistrarPesada(idSector, txtTag.Text, txtPeso.Text, txtIdTraccionPesoObtenido.Text, cbxSimularRespuesta.Checked)

            Respuesta(axRespuesta)
        Catch ex As Exception

            MessageBox.Show(ex.Message, "Peso Obtenido", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try



    End Sub

    Private Sub Respuesta(ByVal axRespuesta As Entidades.AxRespuesta)
        txtidSector.Text = axRespuesta.idSector
        txtHabilitado.Text = axRespuesta.habilitado
        txtIdTrasaccion.Text = axRespuesta.idTransaccion
        txtMensaje.Text = axRespuesta.mensaje
    End Sub
End Class