﻿
Imports System.Threading
Imports Negocio
Imports Entidades.Constante

Imports Negocio.LectorRFID_SpeedWay

Public Class CtrlDinamismoPorteria


#Region "Propiedades"


    Private COLOR_DESCONECTADO As Color = Color.Red
    Private COLOR_CONECTADO As Color = Color.LimeGreen

    Private COLOR_START As Color = Color.Blue
    Private COLOR_STOP As Color = Color.Red


    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
            modDelegado.SetTextLabel(Me, value, lblNombre, Color.Black)
        End Set
    End Property

    'Hilos
    ''' <summary>
    ''' Hilo donde la antena esta leyendo mostrando el efecto de color en le btnLeyendoIngreso antena
    ''' </summary>
    ''' <remarks></remarks>
    Private hAntLeyendoIngreso As Thread
    ''' <summary>
    ''' Hilo donde la antena esta leyendo mostrando el efecto de color en le btnLeyendoEgreso antena
    ''' </summary>
    ''' <remarks></remarks>
    Private hAntLeyendoEgreso As Thread





#End Region

#Region "Eventos"
    Public Event ClickConfCarteles(ByVal CartelPorteria As CartelesPorteria)
    Public Event CLickConfLectorRFID()
    Public Event ClickReset()
    ''' <summary>
    ''' Evento que se ejecuta cuando se hace click en las fechas para subir o bajar una barrera
    ''' </summary>
    ''' <param name="_Barrera">Barrera que se va operar de forma manual</param>
    ''' <param name="_BarreraComando">el estado que se que</param>
    ''' <remarks></remarks>
    Public Event ClickBarreraManual(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _BarreraComando As Entidades.Constante.BarreraComando)
    Public Event ClickLecturaManual(ByVal BarreraIE As Entidades.Constante.IngresoEgreso)
    Public Event ClickHabilitarDeshabilitarBarrera(ByVal IngresoEgreo As Entidades.Constante.IngresoEgreso)
    Public Event BorrarBuffer(ByVal BarreraIE As Entidades.Constante.IngresoEgreso)


#End Region


#Region "Enum"


    Public Enum CartelLed
        Ingreso = 0
        Egreso = 1
    End Enum


#End Region

#Region "Eventos Buffer"
    Public Sub ClickBorrarBufferBal1()
        RaiseEvent BorrarBuffer(IngresoEgreso.Ingreso)
    End Sub

    Public Sub ClickBorrarBufferBal2()
        RaiseEvent BorrarBuffer(IngresoEgreso.Egreso)
    End Sub
#End Region


#Region "Contructor"
    Public Sub New()
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

    End Sub
#End Region

#Region "Metodos"


    Public Sub SetBarreraEstado(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _BarreraEstado As BarreraEstado)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing

        If _Barrera = Entidades.Constante.IngresoEgreso.Ingreso Then

            If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then

                picVisible = picBarreraArriba_Ingreso
                picNoVisible = picBarreraAbajo_Ingreso
                modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, False)
            ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo

                picVisible = picBarreraAbajo_Ingreso
                picNoVisible = picBarreraArriba_Ingreso
                modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, False)
            ElseIf _BarreraEstado = BarreraEstado.Falla Then

                picNoVisible = Nothing
                picVisible = Nothing
                modDelegado.setVisibleCtrl(Me, lblFallaBIngreso, True)

            End If


        Else   'Egreso

            If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then

                picVisible = picBarreraArriba_Egreso
                picNoVisible = picBarreraAbajo_Egreso
                modDelegado.setVisibleCtrl(Me, lblFallaBEgreso, False)

            ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
                picVisible = picBarreraAbajo_Egreso
                picNoVisible = picBarreraArriba_Egreso
                modDelegado.setVisibleCtrl(Me, lblFallaBEgreso, False)
            ElseIf _BarreraEstado = BarreraEstado.Falla Then
                picNoVisible = Nothing
                picVisible = Nothing
                modDelegado.setVisibleCtrl(Me, lblFallaBEgreso, True)
            End If

        End If

        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If

    End Sub

    Public Sub SetSensorEstado(ByVal _Sensor As Entidades.Constante.IngresoEgreso, ByVal _SensorEstado As Integer)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing


        If _Sensor = Entidades.Constante.IngresoEgreso.Ingreso Then

            'If _SensorEstado = SensorEstado.Deshabilitado Then
            '    picVisible = Nothing
            '    picNoVisible = Nothing

            If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                picVisible = picSensor_IngresoNoCortando
                picNoVisible = picSensor_IngresoCortando

            ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
                picVisible = picSensor_IngresoCortando
                picNoVisible = picSensor_IngresoNoCortando



            End If

        Else 'Egreso


            'If _SensorEstado = SensorEstado.Deshabilitado Then
            '    picVisible = Nothing
            '    picNoVisible = Nothing

            If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                picVisible = picSensor_EgresoNoCortando
                picNoVisible = picSensor_EgresoCortando

            ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
                picVisible = picSensor_EgresoCortando
                picNoVisible = picSensor_EgresoNoCortando

            End If


        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        Else
            'Oculto las dos barreras
            picVisible = If(_Sensor = Entidades.Constante.IngresoEgreso.Ingreso, picSensor_IngresoCortando, picSensor_EgresoCortando)
            picNoVisible = If(_Sensor = Entidades.Constante.IngresoEgreso.Ingreso, picSensor_IngresoNoCortando, picSensor_EgresoNoCortando)

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub


#End Region

#Region "Eventos FORM"

    Private Sub btnConfCartEgreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent ClickConfCarteles(CartelesPorteria.Egreso)
    End Sub

    Private Sub btnConfCartelIngreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent ClickConfCarteles(CartelesPorteria.Ingreso)
    End Sub

    Private Sub btnLectorRfid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent CLickConfLectorRFID()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent ClickReset()
    End Sub



    Private Sub btnLecturaManualIngreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent ClickLecturaManual(Entidades.Constante.IngresoEgreso.Ingreso)
    End Sub

    Private Sub btnLecturaManualEgreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent ClickLecturaManual(Entidades.Constante.IngresoEgreso.Egreso)
    End Sub

    Private Sub btnBorrarBufferIngreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent BorrarBuffer(Entidades.Constante.IngresoEgreso.Ingreso)
    End Sub

    Private Sub btnBorrarBufferEgreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RaiseEvent BorrarBuffer(Entidades.Constante.IngresoEgreso.Egreso)
    End Sub

    ''' <summary>
    ''' Cuando se de click en los botones de habilitar o deshabilitar sensor
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub HabilitarDeshabilitarBarreras_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    Dim btnHabilitarDeshabilitar As Button = sender
    '    'Si el boton que se hizo click es el boton de ingreso de los contario es el boton de egreso
    '    Dim IngresoEgreso As Entidades.Constante.IngresoEgreso = If(btnHabilitarDeshabilitar.Name = btnEstadoHabilitacionBarrIngreso.Name,
    '                                                                IngresoEgreso.Ingreso, IngresoEgreso.Egreso)

    '    RaiseEvent ClickHabilitarDeshabilitarBarrera(IngresoEgreso)

    'End Sub

#End Region

#Region "SubProcesos"

    Private Sub AntenaLeyendo(ByVal BarreraIE As Entidades.Constante.IngresoEgreso)

        'Busco el boton que esta leyendo
        ' Dim btnLeyendo As Button = If(BarreraIE = Barrera.Ingreso, btnLeyendoIngreso, btnLeyendoEgreso)

        'Cambio el balor de backGraund del boton



    End Sub

#End Region





End Class
