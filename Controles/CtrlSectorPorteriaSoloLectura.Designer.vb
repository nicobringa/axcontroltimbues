﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorPorteriaSoloLectura
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CtrlAtenaLeyendo = New Controles.CtrlAntenaLeyendo()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.S_LECTURA_RFID = New Controles.CtrlSensor()
        Me.btnLecturaManual = New System.Windows.Forms.Button()
        Me.CtrlSensor1 = New Controles.CtrlSensor()
        Me.SuspendLayout()
        '
        'CtrlAtenaLeyendo
        '
        Me.CtrlAtenaLeyendo.Location = New System.Drawing.Point(5, 47)
        Me.CtrlAtenaLeyendo.Name = "CtrlAtenaLeyendo"
        Me.CtrlAtenaLeyendo.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendo.TabIndex = 174
        Me.CtrlAtenaLeyendo.UltTag = Nothing
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(1, 1)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(450, 43)
        Me.lblNombre.TabIndex = 173
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(244, 44)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 212)
        Me.CtrlPanelNotificaciones1.TabIndex = 172
        '
        'S_LECTURA_RFID
        '
        Me.S_LECTURA_RFID.BitMonitoreo = CType(0, Short)
        Me.S_LECTURA_RFID.Estado = False
        Me.S_LECTURA_RFID.GPIO = False
        Me.S_LECTURA_RFID.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_RFID.ID_PLC = 0
        Me.S_LECTURA_RFID.ID_SECTOR = 0
        Me.S_LECTURA_RFID.Inicializado = False
        Me.S_LECTURA_RFID.Location = New System.Drawing.Point(88, 160)
        Me.S_LECTURA_RFID.Name = "S_LECTURA_RFID"
        Me.S_LECTURA_RFID.patenteLeida = Nothing
        Me.S_LECTURA_RFID.PLC = Nothing
        Me.S_LECTURA_RFID.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA_RFID.TabIndex = 171
        Me.S_LECTURA_RFID.tiempoPatenteLeidas = 0
        '
        'btnLecturaManual
        '
        Me.btnLecturaManual.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual.Location = New System.Drawing.Point(189, 47)
        Me.btnLecturaManual.Name = "btnLecturaManual"
        Me.btnLecturaManual.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual.TabIndex = 175
        Me.btnLecturaManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual.UseVisualStyleBackColor = False
        '
        'CtrlSensor1
        '
        Me.CtrlSensor1.BitMonitoreo = CType(0, Short)
        Me.CtrlSensor1.Estado = False
        Me.CtrlSensor1.GPIO = False
        Me.CtrlSensor1.ID_CONTROL_ACCESO = 0
        Me.CtrlSensor1.ID_PLC = 0
        Me.CtrlSensor1.ID_SECTOR = 0
        Me.CtrlSensor1.Inicializado = False
        Me.CtrlSensor1.Location = New System.Drawing.Point(100, 145)
        Me.CtrlSensor1.Name = "CtrlSensor1"
        Me.CtrlSensor1.patenteLeida = Nothing
        Me.CtrlSensor1.PLC = Nothing
        Me.CtrlSensor1.Size = New System.Drawing.Size(81, 45)
        Me.CtrlSensor1.TabIndex = 176
        Me.CtrlSensor1.tiempoPatenteLeidas = 0
        '
        'CtrlSectorPorteriaSoloLectura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.CtrlSensor1)
        Me.Controls.Add(Me.btnLecturaManual)
        Me.Controls.Add(Me.CtrlAtenaLeyendo)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Name = "CtrlSectorPorteriaSoloLectura"
        Me.Size = New System.Drawing.Size(452, 266)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnLecturaManual As Button
    Friend WithEvents CtrlAtenaLeyendo As CtrlAntenaLeyendo
    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents S_LECTURA_RFID As CtrlSensor
    Friend WithEvents CtrlSensor1 As CtrlSensor
End Class
