﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Descarga
    {
        public DATO_DWORDDINT pesoChofer { get; set; }
        public DATO_WORDINT habLecturaRFID { get; set; }
        public DATO_WORDINT estadoPlataforma { get; set; }
        public DATO_WORDINT estadoMotorPrincipal { get; set; }
        public DATO_WORDINT paso { get; set; }
        public DATO_WORDINT pasoOld { get; set; }
        public DATO_WORDINT nuevoCamion { get; set; }
        public DATO_DWORDDINT stMinPesoChofer { get; set; }
        public DATO_DWORDDINT stMaxPesoChofer { get; set; }


    }
}
