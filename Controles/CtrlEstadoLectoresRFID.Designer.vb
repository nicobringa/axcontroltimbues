﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlEstadoLectoresRFID
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgEstadoLectores = New System.Windows.Forms.DataGridView()
        Me.colNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConexion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colLectura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAntenas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgEstadoLectores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgEstadoLectores
        '
        Me.dgEstadoLectores.AllowUserToAddRows = False
        Me.dgEstadoLectores.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEstadoLectores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgEstadoLectores.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.dgEstadoLectores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstadoLectores.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colNombre, Me.colIp, Me.colConexion, Me.colLectura, Me.colAntenas})
        Me.dgEstadoLectores.Location = New System.Drawing.Point(0, 0)
        Me.dgEstadoLectores.Name = "dgEstadoLectores"
        Me.dgEstadoLectores.RowHeadersVisible = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        Me.dgEstadoLectores.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgEstadoLectores.Size = New System.Drawing.Size(481, 259)
        Me.dgEstadoLectores.TabIndex = 0
        '
        'colNombre
        '
        Me.colNombre.HeaderText = "Nombre"
        Me.colNombre.Name = "colNombre"
        '
        'colIp
        '
        Me.colIp.HeaderText = "Ip"
        Me.colIp.Name = "colIp"
        '
        'colConexion
        '
        Me.colConexion.HeaderText = "Conexion"
        Me.colConexion.Name = "colConexion"
        '
        'colLectura
        '
        Me.colLectura.HeaderText = "Lectura"
        Me.colLectura.Name = "colLectura"
        '
        'colAntenas
        '
        Me.colAntenas.HeaderText = "Antenas"
        Me.colAntenas.Name = "colAntenas"
        Me.colAntenas.ReadOnly = True
        '
        'CtrlEstadoLectoresRFID
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.dgEstadoLectores)
        Me.Name = "CtrlEstadoLectoresRFID"
        Me.Size = New System.Drawing.Size(481, 259)
        CType(Me.dgEstadoLectores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgEstadoLectores As DataGridView
    Friend WithEvents colNombre As DataGridViewTextBoxColumn
    Friend WithEvents colIp As DataGridViewTextBoxColumn
    Friend WithEvents colConexion As DataGridViewTextBoxColumn
    Friend WithEvents colLectura As DataGridViewTextBoxColumn
    Friend WithEvents colAntenas As DataGridViewTextBoxColumn
End Class
