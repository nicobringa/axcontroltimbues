﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
namespace AccesoDatos
{
    public class SectorAD
    {

        public List<Entidades.SECTOR> getAll()
        {
            using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
            {
                var oConsulta = from tabla in oContext.SECTOR
                                select new
                                {
                                    NombreSector = tabla.NOMBRE,
                                    NroSector = tabla.ID_SECTOR,
                                    NombreControlAcceso = tabla.ID_CONTROL_ACCESO

                                };


                return oConsulta.AsEnumerable().Select(t => new Entidades.SECTOR
                {
                    NOMBRE = t.NombreSector,
                    ID_SECTOR = t.NroSector,
                    ID_CONTROL_ACCESO = t.NombreControlAcceso

                }).ToList();

            }
        }

        public List<Entidades.SECTOR> getAllControl(int idControl)
        {
            using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
            {
                var oConsulta = from tabla in oContext.SECTOR
                                where tabla.ID_CONTROL_ACCESO == idControl
                                select new
                                {
                                    NombreSector = tabla.NOMBRE,
                                    NroSector = tabla.ID_SECTOR,
                                    NombreControlAcceso = tabla.ID_CONTROL_ACCESO

                                };


                return oConsulta.AsEnumerable().Select(t => new Entidades.SECTOR
                {
                    NOMBRE = t.NombreSector,
                    ID_SECTOR = t.NroSector,
                    ID_CONTROL_ACCESO = t.NombreControlAcceso

                }).ToList();

            }
        }

        public Entidades.SECTOR getOne(int idSector)
        {
            using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
            {
                var Consulta = oContext.SECTOR.Find(idSector);

                return Consulta;
            }
        }
    }
}
