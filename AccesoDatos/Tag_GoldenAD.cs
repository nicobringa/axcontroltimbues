﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccesoDatos
{
    public class Tag_GoldenAD
    {
        public Entidades.Tag_golden add(Entidades.Tag_golden oTagGolden)
        {

            try
            {
                using (Entidades.ax_ControlEntities oContext = new Entidades.ax_ControlEntities())
                {
                    oContext.tag_golden.Add(oTagGolden);
                    oContext.SaveChanges();
                    return oTagGolden;
                }
            }
            catch (Exception)
            {


                return null;
            }
        }

        public Entidades.Tag_golden getOne(string tagGolden)
        {
            try
            {
                using (Entidades.ax_ControlEntities oContext = new Entidades.ax_ControlEntities())
                {
                   return oContext.tag_golden.Find(tagGolden);
                  
                }
            }
            catch (Exception)
            {


                return null;
            }
        }
    }
}
