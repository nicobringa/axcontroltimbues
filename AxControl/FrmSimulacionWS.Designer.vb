﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSimulacionWS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnEnviarEstaHabilitado = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.txtIdTraccionPesoObtenido = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnEnviarPesoObtenido = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtidSector = New System.Windows.Forms.TextBox()
        Me.txtHabilitado = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtIdTrasaccion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMensaje = New System.Windows.Forms.TextBox()
        Me.txtidSectorWS = New System.Windows.Forms.TextBox()
        Me.lblNombreServidor = New System.Windows.Forms.Label()
        Me.txtTag = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxSimularRespuesta = New System.Windows.Forms.CheckBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, -1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(485, 47)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Simulación Web Service"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(4, 148)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(481, 128)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.btnEnviarEstaHabilitado)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(473, 102)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "EstaHabilitado"
        '
        'btnEnviarEstaHabilitado
        '
        Me.btnEnviarEstaHabilitado.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnEnviarEstaHabilitado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnviarEstaHabilitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarEstaHabilitado.ForeColor = System.Drawing.Color.White
        Me.btnEnviarEstaHabilitado.Location = New System.Drawing.Point(128, 30)
        Me.btnEnviarEstaHabilitado.Name = "btnEnviarEstaHabilitado"
        Me.btnEnviarEstaHabilitado.Size = New System.Drawing.Size(196, 31)
        Me.btnEnviarEstaHabilitado.TabIndex = 6
        Me.btnEnviarEstaHabilitado.Text = "Enviar"
        Me.btnEnviarEstaHabilitado.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.txtIdTraccionPesoObtenido)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.txtPeso)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.btnEnviarPesoObtenido)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(473, 102)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "PesoObtenido"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'txtIdTraccionPesoObtenido
        '
        Me.txtIdTraccionPesoObtenido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdTraccionPesoObtenido.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdTraccionPesoObtenido.Location = New System.Drawing.Point(179, 36)
        Me.txtIdTraccionPesoObtenido.Name = "txtIdTraccionPesoObtenido"
        Me.txtIdTraccionPesoObtenido.Size = New System.Drawing.Size(179, 26)
        Me.txtIdTraccionPesoObtenido.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(44, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(128, 24)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "id Trasacción:"
        '
        'txtPeso
        '
        Me.txtPeso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPeso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeso.Location = New System.Drawing.Point(179, 4)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(179, 26)
        Me.txtPeso.TabIndex = 9
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(115, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 24)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Peso:"
        '
        'btnEnviarPesoObtenido
        '
        Me.btnEnviarPesoObtenido.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnEnviarPesoObtenido.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnviarPesoObtenido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarPesoObtenido.ForeColor = System.Drawing.Color.White
        Me.btnEnviarPesoObtenido.Location = New System.Drawing.Point(128, 66)
        Me.btnEnviarPesoObtenido.Name = "btnEnviarPesoObtenido"
        Me.btnEnviarPesoObtenido.Size = New System.Drawing.Size(196, 31)
        Me.btnEnviarPesoObtenido.TabIndex = 7
        Me.btnEnviarPesoObtenido.Text = "Enviar"
        Me.btnEnviarPesoObtenido.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(12, 279)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 24)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Respuesta:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(12, 307)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 24)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "idSector:"
        '
        'txtidSector
        '
        Me.txtidSector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtidSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidSector.Location = New System.Drawing.Point(102, 306)
        Me.txtidSector.Name = "txtidSector"
        Me.txtidSector.Size = New System.Drawing.Size(78, 26)
        Me.txtidSector.TabIndex = 11
        '
        'txtHabilitado
        '
        Me.txtHabilitado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHabilitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHabilitado.Location = New System.Drawing.Point(290, 306)
        Me.txtHabilitado.Name = "txtHabilitado"
        Me.txtHabilitado.Size = New System.Drawing.Size(78, 26)
        Me.txtHabilitado.TabIndex = 13
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(186, 307)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 24)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Habilitado:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(12, 346)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 24)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "idTrasaccion:"
        '
        'txtIdTrasaccion
        '
        Me.txtIdTrasaccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdTrasaccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdTrasaccion.Location = New System.Drawing.Point(136, 346)
        Me.txtIdTrasaccion.Name = "txtIdTrasaccion"
        Me.txtIdTrasaccion.Size = New System.Drawing.Size(78, 26)
        Me.txtIdTrasaccion.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(12, 381)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 24)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Mensaje:"
        '
        'txtMensaje
        '
        Me.txtMensaje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMensaje.Location = New System.Drawing.Point(105, 382)
        Me.txtMensaje.Name = "txtMensaje"
        Me.txtMensaje.Size = New System.Drawing.Size(368, 26)
        Me.txtMensaje.TabIndex = 17
        '
        'txtidSectorWS
        '
        Me.txtidSectorWS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtidSectorWS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidSectorWS.Location = New System.Drawing.Point(87, 53)
        Me.txtidSectorWS.Name = "txtidSectorWS"
        Me.txtidSectorWS.Size = New System.Drawing.Size(70, 26)
        Me.txtidSectorWS.TabIndex = 19
        '
        'lblNombreServidor
        '
        Me.lblNombreServidor.AutoSize = True
        Me.lblNombreServidor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreServidor.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreServidor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.lblNombreServidor.Location = New System.Drawing.Point(12, 55)
        Me.lblNombreServidor.Name = "lblNombreServidor"
        Me.lblNombreServidor.Size = New System.Drawing.Size(69, 24)
        Me.lblNombreServidor.TabIndex = 18
        Me.lblNombreServidor.Text = "Sector:"
        '
        'txtTag
        '
        Me.txtTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTag.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTag.Location = New System.Drawing.Point(87, 87)
        Me.txtTag.Name = "txtTag"
        Me.txtTag.Size = New System.Drawing.Size(317, 26)
        Me.txtTag.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(27, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 24)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "TAG:"
        '
        'cbxSimularRespuesta
        '
        Me.cbxSimularRespuesta.AutoSize = True
        Me.cbxSimularRespuesta.Location = New System.Drawing.Point(4, 125)
        Me.cbxSimularRespuesta.Name = "cbxSimularRespuesta"
        Me.cbxSimularRespuesta.Size = New System.Drawing.Size(125, 17)
        Me.cbxSimularRespuesta.TabIndex = 7
        Me.cbxSimularRespuesta.Text = "Simular axRespuesta"
        Me.cbxSimularRespuesta.UseVisualStyleBackColor = True
        '
        'FrmSimulacionWS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(485, 442)
        Me.Controls.Add(Me.cbxSimularRespuesta)
        Me.Controls.Add(Me.txtTag)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtidSectorWS)
        Me.Controls.Add(Me.lblNombreServidor)
        Me.Controls.Add(Me.txtMensaje)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtIdTrasaccion)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtHabilitado)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtidSector)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmSimulacionWS"
        Me.Text = "FrmSimulacionWS"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btnEnviarEstaHabilitado As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnEnviarPesoObtenido As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtidSector As System.Windows.Forms.TextBox
    Friend WithEvents txtHabilitado As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtIdTrasaccion As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtMensaje As System.Windows.Forms.TextBox
    Friend WithEvents txtidSectorWS As System.Windows.Forms.TextBox
    Friend WithEvents lblNombreServidor As System.Windows.Forms.Label
    Friend WithEvents txtPeso As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTag As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtIdTraccionPesoObtenido As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbxSimularRespuesta As CheckBox
End Class
