﻿Imports Negocio
Imports Entidades
Imports Entidades.Constante
Public Class CtrlDinamismoDescarga
    Public Sub SetBarreraEstado(ByVal NumBarrera As Integer, ByVal _BarreraEstado As BarreraEstado)

        Dim picBarreraArriba As PictureBox = Nothing
        Dim picBarreraAbajo As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing

        Dim lblFalla As Label = Nothing



        If NumBarrera = 1 Then
            picBarreraArriba = picBarrera1Arriba
            picBarreraAbajo = picBarrera1Abajo

            lblFalla = lblFallaB1
        ElseIf NumBarrera = 2 Then

            picBarreraArriba = picBarrera2Arriba
            picBarreraAbajo = picBarrera2Abajo

            lblFalla = lblFallaB2
        End If


        If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then
            picVisible = picBarreraArriba
            picNoVisible = picBarreraAbajo

            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
            picVisible = picBarreraAbajo
            picNoVisible = picBarreraArriba

            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.Falla Then
            picNoVisible = Nothing
            picVisible = Nothing
            modDelegado.setVisibleCtrl(Me, lblFalla, True)

        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)

        End If


    End Sub

    Public Sub SetSensorEstado(TipoSensor As Entidades.Constante.tipoSensor, ByVal NumSensor As Integer, ByVal _SensorEstado As Integer)
        Dim picCortando As PictureBox = Nothing
        Dim picNoCortando As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing




        If TipoSensor = Constante.tipoSensor.Lectura Then
            picVisible = picSensorLecturaCortando
            If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                modDelegado.setVisiblePic(Me, False, picVisible)
            Else
                modDelegado.setVisiblePic(Me, True, picVisible)
            End If
        End If

        If NumSensor = 1 Then
            If TipoSensor = Constante.tipoSensor.Posicion Then
                picVisible = picSensor1Cortando
                If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                    modDelegado.setVisiblePic(Me, False, picVisible)
                Else
                    modDelegado.setVisiblePic(Me, True, picVisible)
                End If
            End If
        Else
            If TipoSensor = Constante.tipoSensor.Posicion Then
                picVisible = picSensorBar2Cortando
                If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                    modDelegado.setVisiblePic(Me, False, picVisible)
                Else
                    modDelegado.setVisiblePic(Me, True, picVisible)
                End If
            End If
        End If


        'If NumSensor = 1 Then
        '    picCortando = If(TipoSensor = Constante.tipoSensor.Lectura,
        '        picSensorLecturaCortando, picCortando)

        '    picNoCortando = If(TipoSensor = Constante.tipoSensor.Lectura,
        '        picSensorLecturaCortando, picNoCortando)


        'ElseIf NumSensor = 2 Then

        '    picCortando = If(TipoSensor = Constante.tipoSensor.Lectura,
        '        Nothing, picSensorBar1Cortando)

        '    picNoCortando = If(TipoSensor = Constante.tipoSensor.Lectura,
        '        Nothing, picSensorBar1SinCortar)


        'End If


        'If _SensorEstado = SensorInfrarojoEstado.SinLectura Then
        '    picVisible = picNoCortando
        '    picNoVisible = picCortando

        'ElseIf _SensorEstado = SensorInfrarojoEstado.ConLectura Then
        '    picVisible = picCortando
        '    picNoVisible = picNoCortando


        'End If


        'If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
        '    modDelegado.setVisiblePic(Me, True, picVisible)
        '    modDelegado.setVisiblePic(Me, False, picNoVisible)
        'Else
        '    'Oculto las dos barreras
        '    picVisible = picCortando
        '    picNoVisible = picNoCortando

        '    modDelegado.setVisiblePic(Me, False, picVisible)
        '    modDelegado.setVisiblePic(Me, False, picNoVisible)
        'End If


    End Sub

    Public Sub setPlataformaDescarga(TipoSensor As Entidades.Constante.tipoSensor, ByVal NumDescarga As Integer, ByVal _estado As Boolean)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing

        If _estado = True Then
            picVisible = picPlataformaDescarga
            picNoVisible = Nothing
        Else
            picVisible = Nothing
            picNoVisible = picPlataformaDescarga
        End If

        If Not IsNothing(picVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
        Else
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If
    End Sub

End Class
