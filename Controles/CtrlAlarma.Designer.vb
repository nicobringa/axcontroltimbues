﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlAlarma
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.picDescarga = New System.Windows.Forms.PictureBox()
        Me.picTicket = New System.Windows.Forms.PictureBox()
        Me.picBalanza = New System.Windows.Forms.PictureBox()
        Me.picCalado = New System.Windows.Forms.PictureBox()
        Me.picIngreso = New System.Windows.Forms.PictureBox()
        Me.ttNombre = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.picDescarga, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBalanza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picCalado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picIngreso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picDescarga
        '
        Me.picDescarga.Image = Global.Controles.My.Resources.Resources.Descarga
        Me.picDescarga.Location = New System.Drawing.Point(0, 0)
        Me.picDescarga.Name = "picDescarga"
        Me.picDescarga.Size = New System.Drawing.Size(79, 69)
        Me.picDescarga.TabIndex = 4
        Me.picDescarga.TabStop = False
        Me.ttNombre.SetToolTip(Me.picDescarga, "DESCARGA")
        Me.picDescarga.Visible = False
        '
        'picTicket
        '
        Me.picTicket.Image = Global.Controles.My.Resources.Resources.Ticket
        Me.picTicket.Location = New System.Drawing.Point(0, 0)
        Me.picTicket.Name = "picTicket"
        Me.picTicket.Size = New System.Drawing.Size(79, 69)
        Me.picTicket.TabIndex = 3
        Me.picTicket.TabStop = False
        Me.ttNombre.SetToolTip(Me.picTicket, "TICKET")
        Me.picTicket.Visible = False
        '
        'picBalanza
        '
        Me.picBalanza.Image = Global.Controles.My.Resources.Resources.BalanzaCamion
        Me.picBalanza.Location = New System.Drawing.Point(0, 0)
        Me.picBalanza.Name = "picBalanza"
        Me.picBalanza.Size = New System.Drawing.Size(79, 69)
        Me.picBalanza.TabIndex = 2
        Me.picBalanza.TabStop = False
        Me.ttNombre.SetToolTip(Me.picBalanza, "BALANZA")
        Me.picBalanza.Visible = False
        '
        'picCalado
        '
        Me.picCalado.Image = Global.Controles.My.Resources.Resources.CaladorRojo
        Me.picCalado.Location = New System.Drawing.Point(0, 0)
        Me.picCalado.Name = "picCalado"
        Me.picCalado.Size = New System.Drawing.Size(79, 69)
        Me.picCalado.TabIndex = 1
        Me.picCalado.TabStop = False
        Me.ttNombre.SetToolTip(Me.picCalado, "CALADO")
        Me.picCalado.Visible = False
        '
        'picIngreso
        '
        Me.picIngreso.Image = Global.Controles.My.Resources.Resources.Ingreso
        Me.picIngreso.Location = New System.Drawing.Point(0, 0)
        Me.picIngreso.Name = "picIngreso"
        Me.picIngreso.Size = New System.Drawing.Size(79, 69)
        Me.picIngreso.TabIndex = 0
        Me.picIngreso.TabStop = False
        Me.ttNombre.SetToolTip(Me.picIngreso, "INGRESO")
        Me.picIngreso.Visible = False
        '
        'CtrlAlarma
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.picDescarga)
        Me.Controls.Add(Me.picTicket)
        Me.Controls.Add(Me.picBalanza)
        Me.Controls.Add(Me.picCalado)
        Me.Controls.Add(Me.picIngreso)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Name = "CtrlAlarma"
        Me.Size = New System.Drawing.Size(78, 67)
        CType(Me.picDescarga, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTicket, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBalanza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picCalado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picIngreso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents picIngreso As PictureBox
    Friend WithEvents picCalado As PictureBox
    Friend WithEvents picBalanza As PictureBox
    Friend WithEvents picTicket As PictureBox
    Friend WithEvents picDescarga As PictureBox
    Friend WithEvents ttNombre As ToolTip
End Class
