﻿Public Class FrmLecturaManual
    Public Event TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.Lector_RFID, ByVal manual As Boolean)

    Private _idSector As Integer

    Public boInvengo As Boolean = False

    Public Sub New(ID_SECTOR As Integer)

        _idSector = ID_SECTOR
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        refrescarGrilla()

    End Sub

    Private Sub refrescarGrilla()

        If _idSector = 0 Then
            dgAntenas.Visible = False
            btnAceptar.Location = New Point(65, 47)
            Me.Size = New Size(342, 123)
        Else

            Dim nAntena As New Negocio.Antena_RfidN
            Dim listAntenas As List(Of Entidades.ANTENAS_RFID) = nAntena.GetAll(_idSector)

            For Each item As Entidades.ANTENAS_RFID In listAntenas
                Dim nConf As New Negocio.Configuracion_LectorRfidN
                item.CONFIG_LECTOR_RFID = nConf.GetOne(item.ID_CONF_LECTOR_RFID)
                dgAntenas.Rows.Add(item.NUM_ANTENA, item.CONFIG_LECTOR_RFID.LECTOR_RFID.NOMBRE, item)
            Next

        End If

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If _idSector = 0 Then
            boInvengo = True
            Me.Close()
        Else


            Dim selectAnt As Entidades.ANTENAS_RFID = GetAntenaCheck()

            If Not IsNothing(selectAnt) Then


                RaiseEvent TagLeidoManual(txtTagRFID.Text, selectAnt.NUM_ANTENA, selectAnt.CONFIG_LECTOR_RFID.LECTOR_RFID, True)
                Me.Close()
            Else
                MessageBox.Show("Seleccione una antena", "Lectura Manual", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

            End If
        End If

    End Sub


    Private Function GetAntenaCheck() As Entidades.ANTENAS_RFID

        For Each row As DataGridViewRow In dgAntenas.Rows
            If row.Cells(colSelect.Index).Value Then
                Return row.Cells(colItem.Index).Value
            End If
        Next
        Return Nothing
    End Function


End Class