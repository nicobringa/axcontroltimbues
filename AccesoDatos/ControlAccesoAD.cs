﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class ControlAccesoAD
    {
        /// <summary>
        /// Permite obtener un control de acceso
        /// </summary>
        /// <param name="idControlAcceso"></param>
        /// <returns></returns>
        public Entidades.CONTROL_ACCESO getOne(int idCartel)
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                return context.CONTROL_ACCESO.Find(idCartel);
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }

        }

        /// <summary>
        /// Obtengo todos los controles de acceso creados
        /// </summary>
        /// <param name="idControlAcceso"></param>
        /// <returns></returns>
        public List<Entidades.CONTROL_ACCESO> getAll()
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                return context.CONTROL_ACCESO.ToList();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }


        }

        public void SumarBitVida(int idControlAcceso)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                Entidades.CONTROL_ACCESO oControl = oContext.CONTROL_ACCESO.Where(a => a.ID_CONTROL_ACCESO == idControlAcceso).FirstOrDefault();
                if (oControl.BIT_VIDA >= 1000)
                {
                    oControl.BIT_VIDA = 0;
                }
                else
                {
                    oControl.BIT_VIDA++;
                }                
                oContext.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int? GetIDControlConIdSector (int idSector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                int? idContAcc = oContext.SECTOR.Where(a => a.ID_SECTOR == idSector).Select(a => a.ID_CONTROL_ACCESO).SingleOrDefault();
                return idContAcc;
            }
            catch (Exception e)
            {
                return 0;
                throw;
            }
        }
    }
}
