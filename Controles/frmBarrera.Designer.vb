﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmBarrera
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnDeshabilitarComando = New System.Windows.Forms.Button()
        Me.btnAutomaticoComando = New System.Windows.Forms.Button()
        Me.btnSubir = New System.Windows.Forms.Button()
        Me.BtnBajar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSector = New System.Windows.Forms.TextBox()
        Me.txtPLC = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnDeshabilitarSensorPosicion = New System.Windows.Forms.Button()
        Me.btnHabilitarSensorPosicion = New System.Windows.Forms.Button()
        Me.txtEstadoSensorPosicion = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.ForeColor = System.Drawing.Color.White
        Me.txtDescripcion.Location = New System.Drawing.Point(0, 0)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.ReadOnly = True
        Me.txtDescripcion.Size = New System.Drawing.Size(244, 28)
        Me.txtDescripcion.TabIndex = 117
        Me.txtDescripcion.Text = "Descripción"
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtEstado
        '
        Me.txtEstado.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtEstado.Enabled = False
        Me.txtEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstado.Location = New System.Drawing.Point(57, 34)
        Me.txtEstado.Multiline = True
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.ReadOnly = True
        Me.txtEstado.Size = New System.Drawing.Size(173, 21)
        Me.txtEstado.TabIndex = 128
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(6, 34)
        Me.Label23.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(45, 15)
        Me.Label23.TabIndex = 127
        Me.Label23.Text = "Estado"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnDeshabilitarComando)
        Me.GroupBox1.Controls.Add(Me.btnAutomaticoComando)
        Me.GroupBox1.Controls.Add(Me.btnSubir)
        Me.GroupBox1.Controls.Add(Me.BtnBajar)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 91)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(225, 145)
        Me.GroupBox1.TabIndex = 129
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Comando"
        '
        'btnDeshabilitarComando
        '
        Me.btnDeshabilitarComando.BackColor = System.Drawing.Color.Red
        Me.btnDeshabilitarComando.FlatAppearance.BorderSize = 0
        Me.btnDeshabilitarComando.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeshabilitarComando.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnDeshabilitarComando.ForeColor = System.Drawing.Color.White
        Me.btnDeshabilitarComando.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeshabilitarComando.Location = New System.Drawing.Point(114, 19)
        Me.btnDeshabilitarComando.Name = "btnDeshabilitarComando"
        Me.btnDeshabilitarComando.Size = New System.Drawing.Size(102, 30)
        Me.btnDeshabilitarComando.TabIndex = 146
        Me.btnDeshabilitarComando.Text = "&Deshabilitar"
        Me.btnDeshabilitarComando.UseVisualStyleBackColor = False
        '
        'btnAutomaticoComando
        '
        Me.btnAutomaticoComando.BackColor = System.Drawing.Color.Green
        Me.btnAutomaticoComando.FlatAppearance.BorderSize = 0
        Me.btnAutomaticoComando.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAutomaticoComando.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnAutomaticoComando.ForeColor = System.Drawing.Color.White
        Me.btnAutomaticoComando.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAutomaticoComando.Location = New System.Drawing.Point(6, 19)
        Me.btnAutomaticoComando.Name = "btnAutomaticoComando"
        Me.btnAutomaticoComando.Size = New System.Drawing.Size(102, 30)
        Me.btnAutomaticoComando.TabIndex = 145
        Me.btnAutomaticoComando.Text = "&Automatico"
        Me.btnAutomaticoComando.UseVisualStyleBackColor = False
        '
        'btnSubir
        '
        Me.btnSubir.BackColor = System.Drawing.Color.Transparent
        Me.btnSubir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubir.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubir.Location = New System.Drawing.Point(66, 55)
        Me.btnSubir.Name = "btnSubir"
        Me.btnSubir.Size = New System.Drawing.Size(42, 71)
        Me.btnSubir.TabIndex = 143
        Me.btnSubir.Text = "Subir"
        Me.btnSubir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSubir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.btnSubir.UseVisualStyleBackColor = False
        '
        'BtnBajar
        '
        Me.BtnBajar.BackColor = System.Drawing.Color.Transparent
        Me.BtnBajar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBajar.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.BtnBajar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnBajar.Location = New System.Drawing.Point(114, 55)
        Me.BtnBajar.Name = "BtnBajar"
        Me.BtnBajar.Size = New System.Drawing.Size(42, 71)
        Me.BtnBajar.TabIndex = 144
        Me.BtnBajar.Text = "Bajar"
        Me.BtnBajar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnBajar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.BtnBajar.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(40, 62)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 15)
        Me.Label1.TabIndex = 145
        Me.Label1.Text = "Sector:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSector
        '
        Me.txtSector.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSector.Enabled = False
        Me.txtSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSector.Location = New System.Drawing.Point(88, 59)
        Me.txtSector.Multiline = True
        Me.txtSector.Name = "txtSector"
        Me.txtSector.ReadOnly = True
        Me.txtSector.Size = New System.Drawing.Size(34, 21)
        Me.txtSector.TabIndex = 146
        Me.txtSector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPLC
        '
        Me.txtPLC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPLC.Enabled = False
        Me.txtPLC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPLC.Location = New System.Drawing.Point(163, 59)
        Me.txtPLC.Multiline = True
        Me.txtPLC.Name = "txtPLC"
        Me.txtPLC.ReadOnly = True
        Me.txtPLC.Size = New System.Drawing.Size(34, 21)
        Me.txtPLC.TabIndex = 148
        Me.txtPLC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(128, 63)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 15)
        Me.Label2.TabIndex = 147
        Me.Label2.Text = "PLC:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtEstadoSensorPosicion)
        Me.GroupBox2.Controls.Add(Me.btnDeshabilitarSensorPosicion)
        Me.GroupBox2.Controls.Add(Me.btnHabilitarSensorPosicion)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 242)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(225, 86)
        Me.GroupBox2.TabIndex = 152
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sensor de posición"
        '
        'btnDeshabilitarSensorPosicion
        '
        Me.btnDeshabilitarSensorPosicion.BackColor = System.Drawing.Color.Red
        Me.btnDeshabilitarSensorPosicion.FlatAppearance.BorderSize = 0
        Me.btnDeshabilitarSensorPosicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeshabilitarSensorPosicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnDeshabilitarSensorPosicion.ForeColor = System.Drawing.Color.White
        Me.btnDeshabilitarSensorPosicion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeshabilitarSensorPosicion.Location = New System.Drawing.Point(116, 19)
        Me.btnDeshabilitarSensorPosicion.Name = "btnDeshabilitarSensorPosicion"
        Me.btnDeshabilitarSensorPosicion.Size = New System.Drawing.Size(102, 30)
        Me.btnDeshabilitarSensorPosicion.TabIndex = 148
        Me.btnDeshabilitarSensorPosicion.Text = "&Deshabilitar"
        Me.btnDeshabilitarSensorPosicion.UseVisualStyleBackColor = False
        '
        'btnHabilitarSensorPosicion
        '
        Me.btnHabilitarSensorPosicion.BackColor = System.Drawing.Color.Green
        Me.btnHabilitarSensorPosicion.FlatAppearance.BorderSize = 0
        Me.btnHabilitarSensorPosicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHabilitarSensorPosicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnHabilitarSensorPosicion.ForeColor = System.Drawing.Color.White
        Me.btnHabilitarSensorPosicion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHabilitarSensorPosicion.Location = New System.Drawing.Point(8, 19)
        Me.btnHabilitarSensorPosicion.Name = "btnHabilitarSensorPosicion"
        Me.btnHabilitarSensorPosicion.Size = New System.Drawing.Size(102, 30)
        Me.btnHabilitarSensorPosicion.TabIndex = 147
        Me.btnHabilitarSensorPosicion.Text = "&Habilitar"
        Me.btnHabilitarSensorPosicion.UseVisualStyleBackColor = False
        '
        'txtEstadoSensorPosicion
        '
        Me.txtEstadoSensorPosicion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtEstadoSensorPosicion.Enabled = False
        Me.txtEstadoSensorPosicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstadoSensorPosicion.Location = New System.Drawing.Point(8, 59)
        Me.txtEstadoSensorPosicion.Multiline = True
        Me.txtEstadoSensorPosicion.Name = "txtEstadoSensorPosicion"
        Me.txtEstadoSensorPosicion.ReadOnly = True
        Me.txtEstadoSensorPosicion.Size = New System.Drawing.Size(210, 21)
        Me.txtEstadoSensorPosicion.TabIndex = 149
        Me.txtEstadoSensorPosicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'frmBarrera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(243, 340)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtPLC)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSector)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtEstado)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.txtDescripcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBarrera"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Control de Barrera"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtEstado As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents BtnBajar As Button
    Friend WithEvents btnSubir As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtSector As TextBox
    Friend WithEvents txtPLC As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnDeshabilitarComando As Button
    Friend WithEvents btnAutomaticoComando As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnDeshabilitarSensorPosicion As Button
    Friend WithEvents btnHabilitarSensorPosicion As Button
    Friend WithEvents txtEstadoSensorPosicion As TextBox
End Class
