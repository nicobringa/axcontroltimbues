﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entidades;

namespace axControl.Models
{
    public class objBalanza
    {
        string _Descripcion;
        public string Descripcion
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
            }
        }

        DATO_WORDINT _Estado;
        public Entidades.DATO_WORDINT Estado
        {
            get
            {
                return _Estado;
            }
            set
            {
                _Estado = value;
            }
        }

        DATO_WORDINT _Comando;
        public DATO_WORDINT Comando
        {
            get
            {
                return _Comando;
            }
            set
            {
                _Comando = value;
            }
        }

        DATO_DWORDDINT _Peso;
        public DATO_DWORDDINT Peso
        {
            get
            {
                return _Peso;
            }
            set
            {
                _Peso = value;
            }
        }

        DATO_WORDINT _PesoMax;
        public DATO_WORDINT PesoMax
        {
            get
            {
                return _PesoMax;
            }
            set
            {
                _PesoMax = value;
            }
        }

        DATO_WORDINT _TiempoEstabilidad;
        public DATO_WORDINT TiempoEstabilidad
        {
            get
            {
                return _TiempoEstabilidad;
            }
            set
            {
                _TiempoEstabilidad = value;
            }
        }

        DATO_WORDINT _DiferenciaEstabilidad;
        public DATO_WORDINT DiferenciaEstabilidad
        {
            get
            {
                return _DiferenciaEstabilidad;
            }
            set
            {
                _DiferenciaEstabilidad = value;
            }
        }

        DATO_WORDINT _LecturaRFID;
        public DATO_WORDINT LecturaRFID
        {
            get
            {
                return _LecturaRFID;
            }
            set
            {
                _LecturaRFID = value;
            }
        }

        DATO_WORDINT _EstadoOld;
        public DATO_WORDINT EstadoOld
        {
            get
            {
                return _EstadoOld;
            }
            set
            {
                _EstadoOld = value;
            }
        }

        DATO_BOOL _dbPesoNeto;
        public DATO_BOOL dbPesoNeto
        {
            get
            {
                return _dbPesoNeto;
            }
            set
            {
                _dbPesoNeto = value;
            }
        }

        DATO_BOOL _dbPesoNegativo;
        public DATO_BOOL dbPesoNegativo
        {
            get
            {
                return _dbPesoNegativo;
            }
            set
            {
                _dbPesoNegativo = value;
            }
        }

        DATO_BOOL _dbFueraRango;
        public DATO_BOOL dbFueraRango
        {
            get
            {
                return _dbFueraRango;
            }
            set
            {
                _dbFueraRango = value;
            }
        }

        DATO_BOOL _dbFueraEquilibrio;
        public DATO_BOOL dbFueraEquilibrio
        {
            get
            {
                return _dbFueraEquilibrio;
            }
            set
            {
                _dbFueraEquilibrio = value;
            }
        }

        DATO_BOOL _dbPesoMayor500;
        public DATO_BOOL dbPesoMayor500
        {
            get
            {
                return _dbPesoMayor500;
            }
            set
            {
                _dbPesoMayor500 = value;
            }
        }

        DATO_BOOL _dbPesoEstable;
        public DATO_BOOL dbPesoEstable
        {
            get
            {
                return _dbPesoEstable;
            }
            set
            {
                _dbPesoEstable = value;
            }
        }

        DATO_BOOL _dbPesoMenorDifEstabilidad;
        public DATO_BOOL dbPesoMenorDifEstabilidad
        {
            get
            {
                return _dbPesoMenorDifEstabilidad;
            }
            set
            {
                _dbPesoMenorDifEstabilidad = value;
            }
        }

        DATO_BOOL _dbPesoMaximoBal;
        public DATO_BOOL dbPesoMaximoBal
        {
            get
            {
                return _dbPesoMaximoBal;
            }
            set
            {
                _dbPesoMaximoBal = value;
            }
        }

        DATO_BOOL _dbErrorBal;
        public DATO_BOOL dbErrorBal
        {
            get
            {
                return _dbErrorBal;
            }
            set
            {
                _dbErrorBal = value;
            }
        }
    }
}