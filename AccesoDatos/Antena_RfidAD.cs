﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
namespace AccesoDatos
{
    public class Antena_RfidAD
    {


        public ANTENAS_RFID getOne(int numAntena , int idConf)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = (from tabla in oContext.ANTENAS_RFID
                                     where tabla.NUM_ANTENA == numAntena &&
                                     tabla.ID_CONF_LECTOR_RFID == idConf
                                     select tabla).SingleOrDefault();  //oContext.Antenas_RFID.Where(a => a.numAntena == numAntena && a.idConfiguracion == idConf).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new ANTENAS_RFID();
            }
        }

        public ANTENAS_RFID getOneXSector(int idSector)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = (from tabla in oContext.ANTENAS_RFID
                                     where tabla.ID_SECTOR == idSector
                                     select tabla).FirstOrDefault();  //oContext.Antenas_RFID.Where(a => a.numAntena == numAntena && a.idConfiguracion == idConf).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new ANTENAS_RFID();
            }
        }


        public ANTENAS_RFID Update(ANTENAS_RFID antena , int idConfiguracion  )
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.ANTENAS_RFID.Where(a => a.NUM_ANTENA == antena.NUM_ANTENA && 
                    a.ID_CONF_LECTOR_RFID == idConfiguracion).Single();

                    oConsulta.TX_POWER = antena.TX_POWER;
                    oConsulta.RX_SENSITIVITY = antena.RX_SENSITIVITY;
                    oConsulta.OBSERVACION = antena.OBSERVACION;
                    oConsulta.ID_SECTOR = antena.ID_SECTOR;

                    oContext.SaveChanges();
                    return oConsulta;
                }
            }
            catch (Exception)
            {
                return new ANTENAS_RFID();
            }
        }

        /// <summary>
        /// Permite obtener las antenas que estan asignadas a un sector
        /// </summary>
        /// <param name="idSector"></param>
        /// <returns></returns>
        public List<ANTENAS_RFID> getAll(int idSector)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = (from tabla in oContext.ANTENAS_RFID
                                     where tabla.ID_SECTOR == idSector
                                     select tabla).ToList();  //oContext.Antenas_RFID.Where(a => a.idSector == idSector).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// Permite pasar el objetna Antena obtenido con entity freamwork  a memoria 
        /// creando un nuevo objeto antena en memoria para que no dependa de una conexion con la base de datos
        /// </summary>
        private Entidades.ANTENAS_RFID toMemoria(Entidades.ANTENAS_RFID antenaEntity)
        {
            ANTENAS_RFID antenaRFID = new ANTENAS_RFID();
            antenaRFID.NUM_ANTENA = antenaEntity.NUM_ANTENA;
            antenaRFID.ID_SECTOR = antenaEntity.ID_SECTOR;
            antenaRFID.OBSERVACION = antenaEntity.OBSERVACION;
            antenaRFID.ID_CONF_LECTOR_RFID = antenaEntity.ID_SECTOR;

            SECTOR sector = new Entidades.SECTOR();

            sector.ID_SECTOR = antenaEntity.SECTOR.ID_SECTOR;
            sector.NOMBRE = antenaEntity.SECTOR.NOMBRE;
            antenaRFID.SECTOR = sector;

            Entidades.CONFIG_LECTOR_RFID conf = new CONFIG_LECTOR_RFID();
            conf.ID_CONF_LECTOR_RFID = antenaEntity.CONFIG_LECTOR_RFID.ID_CONF_LECTOR_RFID;
            conf.ID_CONF_LECTOR_RFID = antenaEntity.CONFIG_LECTOR_RFID.ID_CONF_LECTOR_RFID;
            conf.LECTOR_RFID = antenaEntity.CONFIG_LECTOR_RFID.LECTOR_RFID;
            antenaRFID.CONFIG_LECTOR_RFID = conf;
            return antenaRFID;
        }
    }
}
