﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlControlAccesoPorteria
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoPorteria))
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.B_INGRESO = New Controles.CtrlBarrera()
        Me.B_EGRESO = New Controles.CtrlBarrera()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.S_LECTURA_EGRESO = New Controles.CtrlSensor()
        Me.S_LECTURA_INGRESO = New Controles.CtrlSensor()
        Me.CtrlAtenaLeyendoIngreso = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAtenaLeyendoEgreso = New Controles.CtrlAntenaLeyendo()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnLecturaManualIngreso = New System.Windows.Forms.Button()
        Me.btnLecturaManualEgreso = New System.Windows.Forms.Button()
        Me.btnBajarBarIngreso = New System.Windows.Forms.Button()
        Me.btnSubirBarIngreso = New System.Windows.Forms.Button()
        Me.BtnBajarBarEgreso = New System.Windows.Forms.Button()
        Me.btnSubirBarEgreso = New System.Windows.Forms.Button()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.CtrlPorteriaDinamismo1 = New Controles.CtrlDinamismoPorteria()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CAM_POR1 = New Controles.CtrlCamara()
        Me.CAM_POR2 = New Controles.CtrlCamara()
        Me.S_INGRESO = New Controles.CtrlSensor()
        Me.S_EGRESO = New Controles.CtrlSensor()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(-3, 562)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(588, 61)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 93
        '
        'B_INGRESO
        '
        Me.B_INGRESO.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_INGRESO.Estado = False
        Me.B_INGRESO.ID_CONTROL_ACCESO = 0
        Me.B_INGRESO.ID_PLC = 0
        Me.B_INGRESO.ID_SECTOR = 0
        Me.B_INGRESO.Inicializado = False
        Me.B_INGRESO.Location = New System.Drawing.Point(348, 403)
        Me.B_INGRESO.Name = "B_INGRESO"
        Me.B_INGRESO.PLC = Nothing
        Me.B_INGRESO.Size = New System.Drawing.Size(62, 52)
        Me.B_INGRESO.TabIndex = 90
        '
        'B_EGRESO
        '
        Me.B_EGRESO.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_EGRESO.Estado = False
        Me.B_EGRESO.ID_CONTROL_ACCESO = 0
        Me.B_EGRESO.ID_PLC = 0
        Me.B_EGRESO.ID_SECTOR = 0
        Me.B_EGRESO.Inicializado = False
        Me.B_EGRESO.Location = New System.Drawing.Point(260, 403)
        Me.B_EGRESO.Name = "B_EGRESO"
        Me.B_EGRESO.PLC = Nothing
        Me.B_EGRESO.Size = New System.Drawing.Size(62, 52)
        Me.B_EGRESO.TabIndex = 89
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(588, -3)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(250, 626)
        Me.CtrlPanelNotificaciones1.TabIndex = 1
        '
        'S_LECTURA_EGRESO
        '
        Me.S_LECTURA_EGRESO.Estado = False
        Me.S_LECTURA_EGRESO.Gpio = False
        Me.S_LECTURA_EGRESO.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_EGRESO.ID_PLC = 0
        Me.S_LECTURA_EGRESO.ID_SECTOR = 0
        Me.S_LECTURA_EGRESO.Inicializado = False
        Me.S_LECTURA_EGRESO.Location = New System.Drawing.Point(241, 483)
        Me.S_LECTURA_EGRESO.Name = "S_LECTURA_EGRESO"
        Me.S_LECTURA_EGRESO.PLC = Nothing
        Me.S_LECTURA_EGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA_EGRESO.TabIndex = 100
        '
        'S_LECTURA_INGRESO
        '
        Me.S_LECTURA_INGRESO.Estado = False
        Me.S_LECTURA_INGRESO.Gpio = False
        Me.S_LECTURA_INGRESO.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_INGRESO.ID_PLC = 0
        Me.S_LECTURA_INGRESO.ID_SECTOR = 0
        Me.S_LECTURA_INGRESO.Inicializado = False
        Me.S_LECTURA_INGRESO.Location = New System.Drawing.Point(348, 483)
        Me.S_LECTURA_INGRESO.Name = "S_LECTURA_INGRESO"
        Me.S_LECTURA_INGRESO.PLC = Nothing
        Me.S_LECTURA_INGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA_INGRESO.TabIndex = 101
        '
        'CtrlAtenaLeyendoIngreso
        '
        Me.CtrlAtenaLeyendoIngreso.AutoSize = True
        Me.CtrlAtenaLeyendoIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAtenaLeyendoIngreso.Location = New System.Drawing.Point(419, 91)
        Me.CtrlAtenaLeyendoIngreso.Name = "CtrlAtenaLeyendoIngreso"
        Me.CtrlAtenaLeyendoIngreso.Size = New System.Drawing.Size(142, 106)
        Me.CtrlAtenaLeyendoIngreso.TabIndex = 102
        Me.CtrlAtenaLeyendoIngreso.UltTag = Nothing
        '
        'CtrlAtenaLeyendoEgreso
        '
        Me.CtrlAtenaLeyendoEgreso.AutoSize = True
        Me.CtrlAtenaLeyendoEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAtenaLeyendoEgreso.Location = New System.Drawing.Point(17, 96)
        Me.CtrlAtenaLeyendoEgreso.Name = "CtrlAtenaLeyendoEgreso"
        Me.CtrlAtenaLeyendoEgreso.Size = New System.Drawing.Size(142, 106)
        Me.CtrlAtenaLeyendoEgreso.TabIndex = 103
        Me.CtrlAtenaLeyendoEgreso.UltTag = Nothing
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(542, 3)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 106
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnLecturaManualIngreso
        '
        Me.btnLecturaManualIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManualIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualIngreso.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualIngreso.Location = New System.Drawing.Point(477, 51)
        Me.btnLecturaManualIngreso.Name = "btnLecturaManualIngreso"
        Me.btnLecturaManualIngreso.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualIngreso.TabIndex = 105
        Me.btnLecturaManualIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualIngreso.UseVisualStyleBackColor = False
        '
        'btnLecturaManualEgreso
        '
        Me.btnLecturaManualEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManualEgreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualEgreso.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualEgreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualEgreso.Location = New System.Drawing.Point(62, 51)
        Me.btnLecturaManualEgreso.Name = "btnLecturaManualEgreso"
        Me.btnLecturaManualEgreso.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualEgreso.TabIndex = 104
        Me.btnLecturaManualEgreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualEgreso.UseVisualStyleBackColor = False
        '
        'btnBajarBarIngreso
        '
        Me.btnBajarBarIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnBajarBarIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBajarBarIngreso.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.btnBajarBarIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBajarBarIngreso.Location = New System.Drawing.Point(486, 203)
        Me.btnBajarBarIngreso.Name = "btnBajarBarIngreso"
        Me.btnBajarBarIngreso.Size = New System.Drawing.Size(40, 54)
        Me.btnBajarBarIngreso.TabIndex = 99
        Me.btnBajarBarIngreso.UseVisualStyleBackColor = False
        '
        'btnSubirBarIngreso
        '
        Me.btnSubirBarIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnSubirBarIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubirBarIngreso.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubirBarIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubirBarIngreso.Location = New System.Drawing.Point(448, 203)
        Me.btnSubirBarIngreso.Name = "btnSubirBarIngreso"
        Me.btnSubirBarIngreso.Size = New System.Drawing.Size(40, 54)
        Me.btnSubirBarIngreso.TabIndex = 98
        Me.btnSubirBarIngreso.UseVisualStyleBackColor = False
        '
        'BtnBajarBarEgreso
        '
        Me.BtnBajarBarEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.BtnBajarBarEgreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBajarBarEgreso.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.BtnBajarBarEgreso.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnBajarBarEgreso.Location = New System.Drawing.Point(82, 203)
        Me.BtnBajarBarEgreso.Name = "BtnBajarBarEgreso"
        Me.BtnBajarBarEgreso.Size = New System.Drawing.Size(40, 54)
        Me.BtnBajarBarEgreso.TabIndex = 97
        Me.BtnBajarBarEgreso.UseVisualStyleBackColor = False
        '
        'btnSubirBarEgreso
        '
        Me.btnSubirBarEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnSubirBarEgreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubirBarEgreso.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubirBarEgreso.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubirBarEgreso.Location = New System.Drawing.Point(44, 203)
        Me.btnSubirBarEgreso.Name = "btnSubirBarEgreso"
        Me.btnSubirBarEgreso.Size = New System.Drawing.Size(40, 54)
        Me.btnSubirBarEgreso.TabIndex = 96
        Me.btnSubirBarEgreso.UseVisualStyleBackColor = False
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(3, 3)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 86
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'CtrlPorteriaDinamismo1
        '
        Me.CtrlPorteriaDinamismo1.BackColor = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CtrlPorteriaDinamismo1.BackgroundImage = CType(resources.GetObject("CtrlPorteriaDinamismo1.BackgroundImage"), System.Drawing.Image)
        Me.CtrlPorteriaDinamismo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlPorteriaDinamismo1.Location = New System.Drawing.Point(0, 0)
        Me.CtrlPorteriaDinamismo1.Name = "CtrlPorteriaDinamismo1"
        Me.CtrlPorteriaDinamismo1.Nombre = Nothing
        Me.CtrlPorteriaDinamismo1.Size = New System.Drawing.Size(588, 562)
        Me.CtrlPorteriaDinamismo1.TabIndex = 0
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(486, 3)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(49, 44)
        Me.btnAyuda.TabIndex = 151
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.Black
        Me.lblNombre.Location = New System.Drawing.Point(160, 5)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(320, 42)
        Me.lblNombre.TabIndex = 154
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CAM_POR1
        '
        Me.CAM_POR1.AutoSize = True
        Me.CAM_POR1.frmCamara = Nothing
        Me.CAM_POR1.Location = New System.Drawing.Point(419, 420)
        Me.CAM_POR1.Name = "CAM_POR1"
        Me.CAM_POR1.Size = New System.Drawing.Size(150, 60)
        Me.CAM_POR1.TabIndex = 155
        '
        'CAM_POR2
        '
        Me.CAM_POR2.AutoSize = True
        Me.CAM_POR2.frmCamara = Nothing
        Me.CAM_POR2.Location = New System.Drawing.Point(95, 420)
        Me.CAM_POR2.Name = "CAM_POR2"
        Me.CAM_POR2.Size = New System.Drawing.Size(150, 60)
        Me.CAM_POR2.TabIndex = 156
        '
        'S_INGRESO
        '
        Me.S_INGRESO.Estado = False
        Me.S_INGRESO.Gpio = False
        Me.S_INGRESO.ID_CONTROL_ACCESO = 0
        Me.S_INGRESO.ID_PLC = 0
        Me.S_INGRESO.ID_SECTOR = 0
        Me.S_INGRESO.Inicializado = False
        Me.S_INGRESO.Location = New System.Drawing.Point(486, 483)
        Me.S_INGRESO.Name = "S_INGRESO"
        Me.S_INGRESO.PLC = Nothing
        Me.S_INGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_INGRESO.TabIndex = 157
        '
        'S_EGRESO
        '
        Me.S_EGRESO.Estado = False
        Me.S_EGRESO.Gpio = False
        Me.S_EGRESO.ID_CONTROL_ACCESO = 0
        Me.S_EGRESO.ID_PLC = 0
        Me.S_EGRESO.ID_SECTOR = 0
        Me.S_EGRESO.Inicializado = False
        Me.S_EGRESO.Location = New System.Drawing.Point(73, 486)
        Me.S_EGRESO.Name = "S_EGRESO"
        Me.S_EGRESO.PLC = Nothing
        Me.S_EGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_EGRESO.TabIndex = 158
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(165, 157)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 194
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CtrlControlAccesoPorteria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.S_EGRESO)
        Me.Controls.Add(Me.S_INGRESO)
        Me.Controls.Add(Me.CAM_POR2)
        Me.Controls.Add(Me.CAM_POR1)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnLecturaManualIngreso)
        Me.Controls.Add(Me.btnLecturaManualEgreso)
        Me.Controls.Add(Me.CtrlAtenaLeyendoEgreso)
        Me.Controls.Add(Me.CtrlAtenaLeyendoIngreso)
        Me.Controls.Add(Me.S_LECTURA_INGRESO)
        Me.Controls.Add(Me.S_LECTURA_EGRESO)
        Me.Controls.Add(Me.btnBajarBarIngreso)
        Me.Controls.Add(Me.btnSubirBarIngreso)
        Me.Controls.Add(Me.BtnBajarBarEgreso)
        Me.Controls.Add(Me.btnSubirBarEgreso)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.B_INGRESO)
        Me.Controls.Add(Me.B_EGRESO)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.CtrlPorteriaDinamismo1)
        Me.Name = "CtrlControlAccesoPorteria"
        Me.Size = New System.Drawing.Size(838, 626)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlPorteriaDinamismo1 As CtrlDinamismoPorteria
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents BtnBajarBarEgreso As Button
    Friend WithEvents btnSubirBarEgreso As Button
    Friend WithEvents btnBajarBarIngreso As Button
    Friend WithEvents btnSubirBarIngreso As Button
    Friend WithEvents S_LECTURA_EGRESO As CtrlSensor
    Friend WithEvents S_LECTURA_INGRESO As CtrlSensor
    Friend WithEvents CtrlAtenaLeyendoIngreso As CtrlAntenaLeyendo
    Friend WithEvents CtrlAtenaLeyendoEgreso As CtrlAntenaLeyendo
    Friend WithEvents btnLecturaManualEgreso As Button
    Friend WithEvents btnLecturaManualIngreso As Button
    Public WithEvents B_EGRESO As CtrlBarrera
    Public WithEvents B_INGRESO As CtrlBarrera
    Friend WithEvents btnReset As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents lblNombre As Label
    Friend WithEvents CAM_POR1 As CtrlCamara
    Friend WithEvents CAM_POR2 As CtrlCamara
    Friend WithEvents S_INGRESO As CtrlSensor
    Friend WithEvents S_EGRESO As CtrlSensor
    Friend WithEvents btnAlarma As Button
End Class
