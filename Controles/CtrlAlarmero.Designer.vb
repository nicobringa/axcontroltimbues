﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlAlarmero
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgAlarmero = New System.Windows.Forms.DataGridView()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colControlAcceso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObjeto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAparicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colfechaReconocimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipoFalla = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colReconocer = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.dgAlarmero, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgAlarmero
        '
        Me.dgAlarmero.AllowUserToAddRows = False
        Me.dgAlarmero.AllowUserToDeleteRows = False
        Me.dgAlarmero.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgAlarmero.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgAlarmero.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgAlarmero.BackgroundColor = System.Drawing.SystemColors.ActiveBorder
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveBorder
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgAlarmero.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgAlarmero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgAlarmero.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colID, Me.colSector, Me.colControlAcceso, Me.colObjeto, Me.colDetalle, Me.colAparicion, Me.colfechaReconocimiento, Me.colTipoFalla, Me.colReconocer})
        Me.dgAlarmero.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgAlarmero.GridColor = System.Drawing.SystemColors.ActiveBorder
        Me.dgAlarmero.Location = New System.Drawing.Point(0, 0)
        Me.dgAlarmero.Name = "dgAlarmero"
        Me.dgAlarmero.ReadOnly = True
        Me.dgAlarmero.Size = New System.Drawing.Size(1386, 150)
        Me.dgAlarmero.TabIndex = 0
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        '
        'colSector
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        Me.colSector.DefaultCellStyle = DataGridViewCellStyle3
        Me.colSector.HeaderText = "Sector"
        Me.colSector.Name = "colSector"
        Me.colSector.ReadOnly = True
        '
        'colControlAcceso
        '
        Me.colControlAcceso.HeaderText = "Contol Acceso"
        Me.colControlAcceso.Name = "colControlAcceso"
        Me.colControlAcceso.ReadOnly = True
        '
        'colObjeto
        '
        Me.colObjeto.HeaderText = "Objeto"
        Me.colObjeto.Name = "colObjeto"
        Me.colObjeto.ReadOnly = True
        '
        'colDetalle
        '
        Me.colDetalle.HeaderText = "Detalle"
        Me.colDetalle.Name = "colDetalle"
        Me.colDetalle.ReadOnly = True
        '
        'colAparicion
        '
        Me.colAparicion.HeaderText = "Fecha Aparicion"
        Me.colAparicion.Name = "colAparicion"
        Me.colAparicion.ReadOnly = True
        '
        'colfechaReconocimiento
        '
        Me.colfechaReconocimiento.HeaderText = "Fecha reconocimiento"
        Me.colfechaReconocimiento.Name = "colfechaReconocimiento"
        Me.colfechaReconocimiento.ReadOnly = True
        '
        'colTipoFalla
        '
        Me.colTipoFalla.HeaderText = "Tipo Falla"
        Me.colTipoFalla.Name = "colTipoFalla"
        Me.colTipoFalla.ReadOnly = True
        '
        'colReconocer
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.colReconocer.DefaultCellStyle = DataGridViewCellStyle4
        Me.colReconocer.HeaderText = ""
        Me.colReconocer.Name = "colReconocer"
        Me.colReconocer.ReadOnly = True
        Me.colReconocer.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colReconocer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'CtrlAlarmero
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Controls.Add(Me.dgAlarmero)
        Me.Name = "CtrlAlarmero"
        Me.Size = New System.Drawing.Size(1386, 150)
        CType(Me.dgAlarmero, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgAlarmero As DataGridView
    Friend WithEvents colID As DataGridViewTextBoxColumn
    Friend WithEvents colSector As DataGridViewTextBoxColumn
    Friend WithEvents colControlAcceso As DataGridViewTextBoxColumn
    Friend WithEvents colObjeto As DataGridViewTextBoxColumn
    Friend WithEvents colDetalle As DataGridViewTextBoxColumn
    Friend WithEvents colAparicion As DataGridViewTextBoxColumn
    Friend WithEvents colfechaReconocimiento As DataGridViewTextBoxColumn
    Friend WithEvents colTipoFalla As DataGridViewTextBoxColumn
    Friend WithEvents colReconocer As DataGridViewButtonColumn
End Class
