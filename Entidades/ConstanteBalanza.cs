﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class ConstanteBalanza
    {
        //Datos enviados al PLC
        /// <summary>
        /// Barrera Igreso Balanza Izquierda
        /// 2= Abrir en manual, 3= Cerrar en manual
        /// </summary>
        public static string CMD_Bal1Bar1 = "Comando Bal1Bar1";
        /// <summary>
        /// Barrera Egreso Balanza Izquierda
        /// 2= Abrir en manual, 3= Cerrar en manual
        /// </summary>
        public static string CMD_Bal1Bar2 = "Comando Bal1Bar2";
        /// <summary>
        /// Barrera Ingreso Balanza Derecha
        /// 2= Abrir en manual, 3= Cerrar en manual
        /// </summary>
        public static string CMD_Bal2Bar1 = "Comando Bal2Bar1";
        /// <summary>
        /// Barrera Egreso Balanza Derecha
        /// 2= Abrir en manual, 3= Cerrar en manual
        /// </summary>
        public static string CMD_Bal2Bar2 = "Comando Bal2Bar2";

        /// <summary>
        /// Comando Balanza Izquierda
        /// (=2 Deshabilitar Balanza ( baja todas las barreras);=1 Habilitar Balanza; =4 Permitir Salir; =5 Permitir Entrada )
        /// </summary>B
        public static string CMD_BALANZA_1 = "Comando Balanza 1";
        /// <summary>
        /// Comando Balanza Derecha
        /// (=2 Deshabilitar Balanza ( baja todas las barreras);=1 Habilitar Balanza; =4 Permitir Salir; =5 Permitir Entrada )
        /// </summary>
        public static string CMD_BALANZA_2 = "Comando Balanza 2";

        /// <summary>
        /// Tiempo infrarrojo salida
        /// 0 a 255 (segundos)
        /// 
        /// </summary>
        public static string TIEMPO_INFARROJO_SALIDA = "Tiempo infrarrojo salida";
        /// <summary>
        /// Tiempo infrarrojo entrada
        /// 0 a 255 (segundos)
        /// 
        /// </summary>
        public static string TIEMPO_INFARROJO_ENTRADA = "Tiempo infrarrojo entrada";
        /// <summary>
        /// Habilitación Sensores de Cruce
        /// Bit0 = Habilitacion SIBI; Bit1 = Habilitacion SEBI; Bit2 = Habilitacion SIBD; Bit3 = Habilitacion SEBD;
        /// </summary>
        public static string HAB_SENSORES_CRUCE_BARRERAS = "Habilitación Sensores de Cruce y Barreras";

        /// <summary>
        /// Peso Maximo Permitido en Balanza (Tn*10)
        /// </summary>
        public static string PESO_MAX_PERMITIDO = "Peso Maximo Permitido en Balanza";
        
        /// <summary>
        /// Tiempo espera estabilidad
        /// </summary>
        public static string TIEMPO_ESPERA_ESTABILIDAD = "Tiempo espera estabilidad";
        /// <summary>
        /// Diferencia de estabilidad
        /// </summary>
        public static string DIFERENCIA_ESTABILIDAD = "Diferencia de estabilidad";

        //Datos  enviados al sistema desde el PLC
        /// <summary>
        /// (Balanza 1 Barrera 1)
        /// 
        /// 1=  barrera abierta, 0= barrera abajo, 2= en falla
        /// </summary>
        public static string ESTADO_B1B1 = "Estado Bal1Bar1";
        /// <summary>
        /// (Balanza 1 Barrera 2)
        /// 
        /// 1=  barrera abierta, 0= barrera abajo, 2= en falla
        /// </summary>
        public static string ESTADO_B1B2 = "Estado Bal1Bar2";
        /// <summary>
        /// (Balanza 2 Barrera 1)
        /// 
        /// 1=  barrera abierta, 0= barrera abajo, 2= en falla
        /// </summary>
        public static string ESTADO_B2B1 = "Estado Bal2Bar1";
        /// <summary>
        /// (Balanza 2 Barrera 2)
        /// 
        /// 1=  barrera abierta, 0= barrera abajo, 2= en falla
        /// </summary>
        public static string ESTADO_B2B2 = "Estado Bal2Bar2";

        /// <summary>
        /// Estado sensores de cruce y Balanzas
        /// Bit0 = Estado Sensor cruce BIBI(barrera ingreso balanza izquierda), 
        /// Bit1 = Estado Sensor cruce BEBI, 
        /// Bit2 = Estado Sensor cruce BIBD, 
        /// Bit3 = Estado Sensor cruce BEBD , 
        /// Bit4 = Estado Balanza Izquierda(=0, en movimiento, = 1, estable), 
        /// Bit5 = Estado Balanza Derecha
        /// </summary>
        public static string ESTADO_SENSORE_CRUCE_BALANZA = "Estado sensores de cruce y Balanzas";

        /// <summary>
        /// Estado  Balanza Izquierda
        /// (=0 Barreras cerradas; 
        /// =10 Habilitada para  ingresar; 
        /// =15 Balanza no esta cero o no conectada; 
        /// =20 Avanzar Camión; =25 Retroceder Camión; 
        /// =30 Estabilizando Peso; 
        /// =33 Peso máximo alcanzado; 
        /// =35 Tomar Peso; 
        /// =40 Permiti salir de la balanza)
        /// </summary>
        public static string ESTADO_BALANZA_1 = "Estado Balanza 1";

        /// <summary>
        /// Estado  Balanza Izquierda
        /// (=0 Barreras cerradas; 
        /// =10 Habilitada para  ingresar; 
        /// =15 Balanza no esta cero o no conectada; 
        /// =20 Avanzar Camión; =25 Retroceder Camión; 
        /// =30 Estabilizando Peso; 
        /// =33 Peso máximo alcanzado; 
        /// =35 Tomar Peso; 
        /// =40 Permiti salir de la balanza)
        /// </summary>
        public static string ESTADO_BALANZA_2 = "Estado Balanza 2";

        public static string BALANZA1_HABILITADA = "Balanza 1 Habilitada";
        public static string BALANZA2_HABILITADA = "Balanza 2 Habilitada";

        //Estado balanza
        public static string BARRERAS_CERRADAS = "Barreras cerradas";
        public static string HABILITADA_INGRESAR = "Habilitada a ingresar";
        public static string BALANZA_CERO_NO_CONECTADA = "Balanza no esta cero o no conectada";
        public static string AVANZA_CAMION = "Avanza Camión";
        public static string RETROCEDA_CAMION = "Retroceda camión";
        public static string ESTABILIZANDO_PESO = "Estabilizando Peso";
        public static string PESO_MAXIMO_ALZANZADO = "Peso maximo alcanzado";
        public static string TOMAR_PESO = "Tomar Peso";
        public static string PERMITIR_SALIR_BALANZA = "Permitir salir";
        public static string BAJA_BARRERA_SALIDA = "Baja berrera salida";
        public static string ESTADO_HABILITACION_BAL1 = "Balanza 1 Habilitada";
        public static string ESTADO_HABILITACION_BAL2 = "Balanza 2 Habilitada";
        public static string CIERRE_BARRERA_ENTRADA = "Cierra barrera de entrada";
        public static string MANUAL = "MANUAL";
        public static string DESHABILITADO = "DESHABILITADA";

        //MENSAJE A MOSTRAR EN EL CARTEL
        public static string MENSAJE_CARTEL_AVANCE = "AVANCE";
        public static string MENSAJE_CARTEL_LLAMAR = "LLAMAR";
        public static string MENSAJE_CARTEL_RETROCEDA = "Retroceda";
        public static string MENSAJE_CARTEL_PESO_MAXIMO = "Peso maximo";
        public static string MENSAJE_CARTEL_ESPERE = "ESPERE";
        public static string MENSAJE_CARTEL_FUERA_DE_SERVICIO = "FUERA DE SERVICIO";



        /// <summary>
        /// Cartel de balanza
        /// </summary>
        public enum Cartel
        {
            /// <summary>
            /// Cartel que esta mas serca de la barrera 1
            /// </summary>
            CartelBarrera1 = 1,
            /// <summary>
            /// Cartel que esta mas serca de la barrera 2
            /// </summary>
            CartelBarrera2 = 2
        }

        public enum Barrera
        {
            Barrera1 = 1,
            Barrera2 = 2
        }

        public enum SensorEstado
        {
            Cortando = 0,
            NoCortando = 1

        }

        /// <summary>
        /// EStado de la balanza 
        /// </summary>
        public enum EstadoBalanza
        {
            BarrerasCerrada = 0,

            HabilitadaIngresarBarrera1 = 10,
            HabilitadaIngresarBarrera2 = 60,

            BalanzaCeroNoConectadaBarrera1 = 15,
            BalanzaCeroNoConectadaBarrera2 = 65,

            AvanzarCamionBarrera1 = 20,
            AvanzarCamionBarrera2 = 70,

            RetrocedaCamionBarrera1 = 25,
            RetrocedaCamionBarrera2 = 75,

            EstabilizandoPesoBarrera1 = 30,
            EstabilizandoPesoBarrera2 = 80,

            PesoMaximoBarrera1 = 33,
            PesoMaximoBarrera2 = 85,

            TomarPesoBarrera1 = 35,
            TomarPesoBarrera2 = 90,

            PermitirSalirBarrera1 = 40,
            PermitirSalirBarrera2 = 95,

            BajaBarrera1Salida = 45,
            BajaBarrera2Salida = 100,


        }



        public enum EstadoSensor
        {
            Cortando = 0,
            NoCortando = 1

        }


        public enum EstadoHabilitacionBalanza
        {
           Habilitado =  1,
           Deshabilitado = 0
        }

        public enum EstadoPesoBalanza
        {
            Movimiento = 0,
            Estable = 1
        }

        /// <summary>
        /// (=2 Deshabilitar Balanza ( baja todas las barreras);=1 Habilitar Balanza; =4 Permitir Salir; =5 Permitir Ingreso Barrera 1; =6 Permitir Ingreso Barrera 2 )
        /// </summary>
        public enum ComandoBalanza
        {
            HabilitarBalanza = 1,
            /// <summary>
            /// Deshabilitar Balanza ( baja todas las barreras)
            /// </summary>
            DeshabilitarBalanza = 2,
            /// <summary>
            /// Da la orden de que el camion esta habilitado a salir de la balanza
            /// </summary>
            PermitirSalir = 4,
            /// <summary>
            /// Da la orden de que el camion esta habilitado a ingresar a balanza
            /// </summary>
            PermitirSalirManual=7,
            PermitirIngresoBarrera1 = 5,

            PermitirIngresoBarrera2 = 6

        }

        public enum BarreraEstado
        {
            Arriba = 1,
            Abajo = 0,
            Falla = 2
        }


        public enum BarreraComando
        {
            
            AbrirManual = 2,
            CerrarManual = 3
        }

        public enum BalanzaIE
        {
            Ingreso = 1,
            Egreso = 2
        }

        public enum Balanzas
        {
            Uno = 1,
            Dos = 2
        }

        public enum BarreraIE
        {
            Ingreso = 0,
            Egreso = 2
        }

        public enum Sensores
        {

            Uno = 0,
            Dos = 1
        }

        public enum BalanzaHabilitada
        {
            Habilitada = 1,
            Deshabilitada = 0
        }

        public enum BitHabilitacionSensoresBarreras
        {
            Sensor1Balanza1 = 0,
            Sensor2Balanza1 = 1,
            Sensor1Balanza2 = 2,
            Sensor2Balanza2 = 3,
            Barrera1Balanza1 = 4,
            Barrera2Balanza1 = 5,
            Barrera1Balanza2 = 6,
            Barrera2Balanza2 = 7,
        }

        public enum BitEstadoSensorCruceBalanza
        {
            Sensor1Bal1 = 0 ,
            Sensor2Bal1 = 1,
            Sensor1Bal2 = 2,
            Sensor2Bal2 = 3,
            EstadoPesoBalanza1 = 4,
            EstadoPesoBalanza2 = 5,
            EstadoFallaS1Bal1 = 6,
            EstadoFallaS2Bal1 = 7,
            EstadoFallaS1Bal2 = 8,
            EstadoFallaS2Bal2 = 9,
            Bal1NoConectada = 10,
            Bal2NoConectada = 11

        }

        /// <summary>
        /// Estado de conexion de la balanza
        /// </summary>
        public enum BalanzaConexion
        {
            Conectada = 0,
            NoConectada = 1
        }


        public static EstadoPesoBalanza getEstadoPesoBalanza(Int16 dato, Int16 bitPosicion)
        {
            int resultado;

            resultado = (dato & (int)Math.Pow(2, bitPosicion));

            //Si el resultado es mayor que 0 es porque el bit esta en 1 , y si es 0  el bit esta en 0
            // y si el bit esta en 1 el sensor esta de
            return resultado > 0 ? EstadoPesoBalanza.Estable : EstadoPesoBalanza.Movimiento;


        }

        public static BalanzaConexion getEstadoConexion(Int16 dato, Int16 bitPosicion)
        {
            int resultado;

            resultado = (dato & (int)Math.Pow(2, bitPosicion));

            //Si el resultado es mayor que 0 es porque el bit esta en 1 , y si es 0  el bit esta en 0
            // y si el bit esta en 1 el sensor esta de
            return resultado > 0 ? BalanzaConexion.NoConectada : BalanzaConexion.Conectada;


        }

      
    }
}
