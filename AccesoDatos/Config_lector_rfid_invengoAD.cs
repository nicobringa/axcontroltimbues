﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class Config_lector_rfid_invengoAD
    {
        public Config_lector_rfid_invengo update_config(Config_lector_rfid_invengo oConfig_lector_rfid_invengo)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {

                    var oConsulta = (from tabla in oContext.config_lector_rfid_invengo
                                     where tabla.idconfig_lector_rfid_invengo == oConfig_lector_rfid_invengo.idconfig_lector_rfid_invengo
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.nombre= oConfig_lector_rfid_invengo.nombre;
                        oConsulta.modoLectura= oConfig_lector_rfid_invengo.modoLectura;
                        oConsulta.tiempoLectura= oConfig_lector_rfid_invengo.tiempoLectura;
                        oConsulta.potenciaAntena= oConfig_lector_rfid_invengo.potenciaAntena;
                        

                        oContext.SaveChanges();
                        return oConfig_lector_rfid_invengo;
                    }
                    else
                    {
                        return new Config_lector_rfid_invengo();
                    }

                }
            }
            catch (Exception ex)
            {
                return new Config_lector_rfid_invengo();
            }
        }

        public Config_lector_rfid_invengo getOne(int id)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.config_lector_rfid_invengo.Where(a => a.idconfig_lector_rfid_invengo == id).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new Config_lector_rfid_invengo();
            }
        }
        

        public List<Config_lector_rfid_invengo> getAll()
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.config_lector_rfid_invengo.ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<Config_lector_rfid_invengo>();
            }
        }
    }
}
