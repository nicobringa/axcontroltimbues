﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class Puesto_TrabajoAD
    {


        public Entidades.PUESTO_TRABAJO add(Entidades.PUESTO_TRABAJO puestoTrabajo)
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                List<CONTROL_ACCESO> contextListControlAcceso = new List<CONTROL_ACCESO>();
                foreach (Entidades.CONTROL_ACCESO ca in puestoTrabajo.CONTROL_ACCESO)
                {
                    CONTROL_ACCESO contextControlAcceso = context.CONTROL_ACCESO.Find(ca.ID_CONTROL_ACCESO);
                    contextListControlAcceso.Add(contextControlAcceso);
                }

                puestoTrabajo.CONTROL_ACCESO = contextListControlAcceso;
                puestoTrabajo.SERVIDOR = false;
                context.PUESTO_TRABAJO.Add(puestoTrabajo);
                context.SaveChanges();
                return puestoTrabajo;
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }
        }

        /// <summary>
        /// Permite asignar los controles de accesos
        /// </summary>
        /// <param name="puestoTrabajo"></param>
        /// <returns></returns>
        public Entidades.PUESTO_TRABAJO addCotnrolAcceso(Entidades.PUESTO_TRABAJO puestoTrabajo)
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                PUESTO_TRABAJO consulta = context.PUESTO_TRABAJO.Find(puestoTrabajo.ID_PUESTO_TRABAJO);
                consulta.CONTROL_ACCESO = puestoTrabajo.CONTROL_ACCESO;
                context.SaveChanges();
                return puestoTrabajo;
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }
        }

        public Entidades.PUESTO_TRABAJO getOne(int idPuestoTrabjo)
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                return context.PUESTO_TRABAJO.Find(idPuestoTrabjo);
            }
            catch (Exception ex)
            {

               // AxError.setError(ex);
                return null;
            }
            

        }
        /// <summary>
        /// Permite obtener el puesto de trabajo segun el nombre de la maquina
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public Entidades.PUESTO_TRABAJO getOne(string nombre)
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                return context.PUESTO_TRABAJO.Where(o => o.NOMBRE == nombre).FirstOrDefault();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }

          

        }

        public List<Entidades.PUESTO_TRABAJO> getAll()
        {

            try
            {
                AxControlORAEntities context = new AxControlORAEntities();
                return context.PUESTO_TRABAJO.ToList();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }
           

        }



    }
}
