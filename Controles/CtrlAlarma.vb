﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Impinj.OctaneSdk
Public Class CtrlAlarma


#Region "PROPIEDADES"

    Private hMonitoreo As Thread
    Private hEfecto As Thread

    Private oControl As New Entidades.Control_Acceso
    Private oControlesN As New Negocio.ControlAccesoN
    Private WithEvents _ControlAcceso As CtrlAutomationObjectsBase
    Public Property ControlAcceso() As CtrlAutomationObjectsBase
        Get
            Return _ControlAcceso
        End Get
        Set(ByVal value As CtrlAutomationObjectsBase)
            _ControlAcceso = value
        End Set
    End Property

    Private _objeto As String
    Public Property objeto() As String
        Get
            Return _objeto
        End Get
        Set(ByVal value As String)
            _objeto = value
        End Set
    End Property

    Private frmContenedor As frmContenedor
    Public Event mostrarControl(control As CtrlAutomationObjectsBase)
#End Region


#Region "CONSTRUCTOR"

    Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub


#End Region

#Region "EVENTOS FORM"

#End Region


#Region "METODOS"




    Public Sub InicializarCtrl(ByRef control As CtrlAutomationObjectsBase)

        ControlAcceso = control
        oControl = oControlesN.GetOne(control.idControlAcceso)
        oControl = oControl
        Select Case oControl.tipoControlAcceso
            Case Entidades.Constante.TIPO_CONTROL_ACCESO.PORTERIA_INGRESO
                Negocio.setVisiblePic(Me, True, picIngreso)
            Case Entidades.Constante.TIPO_CONTROL_ACCESO.BALANZA
                Negocio.setVisiblePic(Me, True, picBalanza)
            Case Entidades.Constante.TIPO_CONTROL_ACCESO.PORTERIA_EGRESO
                Negocio.setVisiblePic(Me, True, picIngreso)
            Case Entidades.Constante.TIPO_CONTROL_ACCESO.PORTERIA_EGRESO_INVENGO
                Negocio.setVisiblePic(Me, True, picIngreso)

        End Select


        frmContenedor = New frmContenedor(ControlAcceso)

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Name = "[SubProcesoAlarma] " + oControl.nombre
        hMonitoreo.Start()

    End Sub
#End Region

#Region "SUBPROCESOS"
    Public Sub Monitoreo()
        While True
            Dim nAlarma As New Negocio.AlarmaN
            Dim ctrlAlarma As New CtrlAlarma
            Select Case nAlarma.EstadoAlarma(oControl.ID_CONTROL_ACCESO, oControl.ID_CONTROL_ACCESO, 0, objeto)


                ''Alarma Aparecida
                Case 0

                    If IsNothing(hEfecto) Then
                        IniciarEfecto()
                    End If
                    If Not hEfecto.IsAlive Then 'Si no esta en ejecucion el efecto
                        'Lo pongo en ejecucion
                        IniciarEfecto()
                        hEfecto.Start()

                    End If
                ''Alarma Aparecida Reconocida
                Case 1
                    If Not IsNothing(hEfecto) Then
                        If hEfecto.IsAlive Then hEfecto.Abort()

                    End If
                    Me.BackColor = Color.Red

                ''Alarma Desaparecida
                Case 2
                    If Not IsNothing(hEfecto) Then
                        If hEfecto.IsAlive Then hEfecto.Abort()

                    End If
                    Me.BackColor = Color.LimeGreen


            End Select

            Thread.Sleep(1000)
        End While
    End Sub

    Private Sub IniciarEfecto()
        hEfecto = New Thread(AddressOf Efecto)
        hEfecto.IsBackground = True
        hEfecto.Name = "Hilo " & Me.Name
    End Sub

    Private Sub Efecto()

        Try
            While Me.BackColor.A > 0 ' Si la propiedad alpha rgb es > 0

                Me.BackColor = Color.FromArgb(Me.BackColor.A - 2, Color.Red)

                Dim Alpha As Integer = Me.BackColor.A - 15
                modDelegado.SetBackColorCtrl(Me, Me, Color.FromArgb(Alpha, Color.Red))

                modDelegado.SetRefreshCtrl(Me, Me)

                Thread.Sleep(10)
                'Beep()
            End While
            '132; 162; 186

            Me.BackColor = Color.FromArgb(255, 255, 255)
        Catch ex As Exception

        End Try


    End Sub

    Public Function EstaEnAlarma(ByVal idSector As Integer, ByVal idControlAcceso As Integer, ByVal objeto As String, ByVal detalle As String) As Integer

        Dim oAlarmaN As New Negocio.AlarmaN
        Return oAlarmaN.ExisteAlarma(idSector, idControlAcceso, objeto, detalle)

    End Function




    Private Sub picDescarga_Click(sender As Object, e As EventArgs) Handles picDescarga.Click, picCalado.Click, picBalanza.Click, picIngreso.Click, picTicket.Click

        'modDelegado.showForm(Me, frmContenedor)
        'frm.ShowDialog()
        RaiseEvent mostrarControl(ControlAcceso)
    End Sub





#End Region

End Class
