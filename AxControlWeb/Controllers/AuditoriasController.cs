﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;

namespace AxControlWeb.Controllers
{
    public class AuditoriasController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();

        // GET: Auditorias
        public ActionResult Index(string Descripcion, int? Sector=null, DateTime? desFec=null, DateTime? hastFec=null)
        {


            
            Dictionary< int, string > Lista= new Dictionary<int, string>();

            var ListSector = from s in db.SECTOR
                                 select new
                                 {
                                     s.ID_SECTOR,
                                     s.NOMBRE
                                 };

            foreach (var item in ListSector)
            {
                var NOMBRE = item.ID_SECTOR + "-" + item.NOMBRE;
                Lista.Add(item.ID_SECTOR, NOMBRE);
            }

            

            ViewData["ListSector"] = new SelectList(Lista, "Key", "Value");
            var aUDITORIA = db.AUDITORIA.Include(a => a.SECTOR);


            if(Descripcion==null && Sector==null && desFec==null && hastFec==null)
            {
                string mes = DateTime.Now.Month.ToString();
                if(mes.Length==1)
                {
                    mes = "0" + mes;
                }
                string fecha = DateTime.Now.Year.ToString() +"-"+ mes + "-" + DateTime.Now.Day.ToString();

                ViewData["fechaDesde"] = fecha;
                ViewData["fechaHasta"] = fecha;
                DateTime hoy = DateTime.Now.Date;
                aUDITORIA = aUDITORIA.Where(A => A.FECHA > hoy);
            }
            else
            {
                if(Descripcion.Length >0)
                {
                    aUDITORIA = aUDITORIA.Where(A => A.DESCRIPCION.Contains(Descripcion));
                }
                if(desFec !=null)
                {
                    aUDITORIA = aUDITORIA.Where(A => A.FECHA >= desFec);
                }
                if (hastFec != null)
                {
                    aUDITORIA = aUDITORIA.Where(A => A.FECHA <= hastFec);
                }
                if(Sector != null)
                {
                    aUDITORIA = aUDITORIA.Where(A => A.ID_SECTOR== Sector);
                }
            }

            

            return View(aUDITORIA.ToList());
        }

     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
