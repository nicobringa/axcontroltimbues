﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoPorteria
    Inherits System.Windows.Forms.UserControl

    'UserControl1 reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()

            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.picBarreraArriba_Ingreso = New System.Windows.Forms.PictureBox()
        Me.picBarreraAbajo_Ingreso = New System.Windows.Forms.PictureBox()
        Me.picBarreraAbajo_Egreso = New System.Windows.Forms.PictureBox()
        Me.picBarreraArriba_Egreso = New System.Windows.Forms.PictureBox()
        Me.lblSalida = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.picSensor_EgresoCortando = New System.Windows.Forms.PictureBox()
        Me.picSensor_IngresoNoCortando = New System.Windows.Forms.PictureBox()
        Me.picSensor_EgresoNoCortando = New System.Windows.Forms.PictureBox()
        Me.picSensor_IngresoCortando = New System.Windows.Forms.PictureBox()
        Me.ToolTipPorteria = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lblFallaBEgreso = New System.Windows.Forms.Label()
        Me.lblFallaBIngreso = New System.Windows.Forms.Label()
        CType(Me.picBarreraArriba_Ingreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarreraAbajo_Ingreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarreraAbajo_Egreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarreraArriba_Egreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_EgresoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_IngresoNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_EgresoNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_IngresoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picBarreraArriba_Ingreso
        '
        Me.picBarreraArriba_Ingreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Entrada_Despacho_arriba
        Me.picBarreraArriba_Ingreso.Location = New System.Drawing.Point(350, 101)
        Me.picBarreraArriba_Ingreso.Name = "picBarreraArriba_Ingreso"
        Me.picBarreraArriba_Ingreso.Size = New System.Drawing.Size(58, 294)
        Me.picBarreraArriba_Ingreso.TabIndex = 0
        Me.picBarreraArriba_Ingreso.TabStop = False
        '
        'picBarreraAbajo_Ingreso
        '
        Me.picBarreraAbajo_Ingreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Entrada_Despacho_abajo
        Me.picBarreraAbajo_Ingreso.Location = New System.Drawing.Point(351, 313)
        Me.picBarreraAbajo_Ingreso.Name = "picBarreraAbajo_Ingreso"
        Me.picBarreraAbajo_Ingreso.Size = New System.Drawing.Size(170, 83)
        Me.picBarreraAbajo_Ingreso.TabIndex = 1
        Me.picBarreraAbajo_Ingreso.TabStop = False
        '
        'picBarreraAbajo_Egreso
        '
        Me.picBarreraAbajo_Egreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Salida_Despacho_abajo
        Me.picBarreraAbajo_Egreso.Location = New System.Drawing.Point(0, 322)
        Me.picBarreraAbajo_Egreso.Name = "picBarreraAbajo_Egreso"
        Me.picBarreraAbajo_Egreso.Size = New System.Drawing.Size(320, 95)
        Me.picBarreraAbajo_Egreso.TabIndex = 2
        Me.picBarreraAbajo_Egreso.TabStop = False
        '
        'picBarreraArriba_Egreso
        '
        Me.picBarreraArriba_Egreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Salida_Despacho_arriba
        Me.picBarreraArriba_Egreso.Location = New System.Drawing.Point(263, 70)
        Me.picBarreraArriba_Egreso.Name = "picBarreraArriba_Egreso"
        Me.picBarreraArriba_Egreso.Size = New System.Drawing.Size(61, 335)
        Me.picBarreraArriba_Egreso.TabIndex = 3
        Me.picBarreraArriba_Egreso.TabStop = False
        '
        'lblSalida
        '
        Me.lblSalida.AutoSize = True
        Me.lblSalida.BackColor = System.Drawing.Color.Transparent
        Me.lblSalida.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalida.ForeColor = System.Drawing.Color.White
        Me.lblSalida.Location = New System.Drawing.Point(16, 262)
        Me.lblSalida.Name = "lblSalida"
        Me.lblSalida.Size = New System.Drawing.Size(136, 31)
        Me.lblSalida.TabIndex = 4
        Me.lblSalida.Text = "EGRESO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(414, 262)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(147, 31)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "INGRESO"
        '
        'picSensor_EgresoCortando
        '
        Me.picSensor_EgresoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Salida_Despacho_cortandoCS
        Me.picSensor_EgresoCortando.Location = New System.Drawing.Point(68, 351)
        Me.picSensor_EgresoCortando.Name = "picSensor_EgresoCortando"
        Me.picSensor_EgresoCortando.Size = New System.Drawing.Size(154, 79)
        Me.picSensor_EgresoCortando.TabIndex = 11
        Me.picSensor_EgresoCortando.TabStop = False
        '
        'picSensor_IngresoNoCortando
        '
        Me.picSensor_IngresoNoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Entrada_Despacho_nocortando
        Me.picSensor_IngresoNoCortando.Location = New System.Drawing.Point(506, 340)
        Me.picSensor_IngresoNoCortando.Name = "picSensor_IngresoNoCortando"
        Me.picSensor_IngresoNoCortando.Size = New System.Drawing.Size(31, 43)
        Me.picSensor_IngresoNoCortando.TabIndex = 12
        Me.picSensor_IngresoNoCortando.TabStop = False
        '
        'picSensor_EgresoNoCortando
        '
        Me.picSensor_EgresoNoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Salida_Despacho_nocortandoCS
        Me.picSensor_EgresoNoCortando.Location = New System.Drawing.Point(69, 351)
        Me.picSensor_EgresoNoCortando.Name = "picSensor_EgresoNoCortando"
        Me.picSensor_EgresoNoCortando.Size = New System.Drawing.Size(154, 75)
        Me.picSensor_EgresoNoCortando.TabIndex = 13
        Me.picSensor_EgresoNoCortando.TabStop = False
        '
        'picSensor_IngresoCortando
        '
        Me.picSensor_IngresoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Entrada_Despacho_cortando
        Me.picSensor_IngresoCortando.Location = New System.Drawing.Point(506, 339)
        Me.picSensor_IngresoCortando.Name = "picSensor_IngresoCortando"
        Me.picSensor_IngresoCortando.Size = New System.Drawing.Size(31, 43)
        Me.picSensor_IngresoCortando.TabIndex = 14
        Me.picSensor_IngresoCortando.TabStop = False
        '
        'ToolTipPorteria
        '
        Me.ToolTipPorteria.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ToolTipPorteria.IsBalloon = True
        Me.ToolTipPorteria.ShowAlways = True
        Me.ToolTipPorteria.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTipPorteria.ToolTipTitle = "Porteria"
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(588, 39)
        Me.lblNombre.TabIndex = 24
        Me.lblNombre.Text = "Portería Interna"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFallaBEgreso
        '
        Me.lblFallaBEgreso.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaBEgreso.AutoSize = True
        Me.lblFallaBEgreso.BackColor = System.Drawing.Color.Red
        Me.lblFallaBEgreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaBEgreso.ForeColor = System.Drawing.Color.White
        Me.lblFallaBEgreso.Location = New System.Drawing.Point(272, 339)
        Me.lblFallaBEgreso.Name = "lblFallaBEgreso"
        Me.lblFallaBEgreso.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaBEgreso.TabIndex = 96
        Me.lblFallaBEgreso.Text = "Falla"
        Me.lblFallaBEgreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFallaBIngreso
        '
        Me.lblFallaBIngreso.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaBIngreso.AutoSize = True
        Me.lblFallaBIngreso.BackColor = System.Drawing.Color.Red
        Me.lblFallaBIngreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaBIngreso.ForeColor = System.Drawing.Color.White
        Me.lblFallaBIngreso.Location = New System.Drawing.Point(360, 339)
        Me.lblFallaBIngreso.Name = "lblFallaBIngreso"
        Me.lblFallaBIngreso.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaBIngreso.TabIndex = 97
        Me.lblFallaBIngreso.Text = "Falla"
        Me.lblFallaBIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlDinamismoPorteria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.Despacho_base_800x600
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.lblFallaBIngreso)
        Me.Controls.Add(Me.lblFallaBEgreso)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblSalida)
        Me.Controls.Add(Me.picBarreraArriba_Ingreso)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.picBarreraArriba_Egreso)
        Me.Controls.Add(Me.picSensor_EgresoNoCortando)
        Me.Controls.Add(Me.picSensor_EgresoCortando)
        Me.Controls.Add(Me.picBarreraAbajo_Egreso)
        Me.Controls.Add(Me.picSensor_IngresoCortando)
        Me.Controls.Add(Me.picBarreraAbajo_Ingreso)
        Me.Controls.Add(Me.picSensor_IngresoNoCortando)
        Me.Name = "CtrlDinamismoPorteria"
        Me.Size = New System.Drawing.Size(588, 601)
        CType(Me.picBarreraArriba_Ingreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarreraAbajo_Ingreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarreraAbajo_Egreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarreraArriba_Egreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_EgresoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_IngresoNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_EgresoNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_IngresoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picBarreraArriba_Ingreso As System.Windows.Forms.PictureBox
    Friend WithEvents picBarreraAbajo_Ingreso As System.Windows.Forms.PictureBox
    Friend WithEvents picBarreraAbajo_Egreso As System.Windows.Forms.PictureBox
    Friend WithEvents picBarreraArriba_Egreso As System.Windows.Forms.PictureBox
    Friend WithEvents lblSalida As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents picSensor_EgresoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents picSensor_IngresoNoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents picSensor_EgresoNoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents picSensor_IngresoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTipPorteria As System.Windows.Forms.ToolTip
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents lblFallaBEgreso As Label
    Friend WithEvents lblFallaBIngreso As Label
End Class
