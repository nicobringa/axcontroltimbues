﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
namespace AccesoDatos
{
    public  class UsuarioAD
    {

        public Entidades.USUARIO add(Entidades.USUARIO oUsuario)
        {

            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {
                    oContext.USUARIO.Add(oUsuario);
                    oContext.SaveChanges();
                    return oUsuario;
                }
            }
            catch (Exception ex)
            {


                return null;
            }
        }



        public Boolean getNombreCuenta(string StrNombreCuenta)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {
                    // Obtengo el usuario que cumpla con la condicion
                    var oResultadoConsulta = (from TablaUsuario in oContext.USUARIO
                                              where TablaUsuario.NOMBRE == StrNombreCuenta
                                              select TablaUsuario).Count();

                    // Si el resultado no es nulo
                    if (oResultadoConsulta > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                //Entidades.ErrorBD.setMsjError(ex.Message);
                return false;
            }
        }

        // Metodo que permite obtener un usuario con los parametros especificados
        public Entidades.USUARIO getOne(string StrNombreCuenta, string StrContrasenaCuenta)
        {
            //Declaro una variable de tipo usuario, creo un objeto y lo uno a la variable de referencia
            Entidades.USUARIO oUsuario = new Entidades.USUARIO();

            using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
            {
                try
                {
                    // Obtengo el usuario que cumpla con la condicion
                    var oResultadoConsulta = (from TablaUsuario in oContext.USUARIO
                                              where TablaUsuario.NOMBRE == StrNombreCuenta && TablaUsuario.CONTRASENIA == StrContrasenaCuenta
                                              select TablaUsuario).SingleOrDefault();

                    // Si encontro el objeto
                    if (oResultadoConsulta != null)
                    {
                        oUsuario.ID_USUARIO = oResultadoConsulta.ID_USUARIO;
                        oUsuario.NOMBRE = oResultadoConsulta.NOMBRE;
                        oUsuario.CONTRASENIA = oResultadoConsulta.CONTRASENIA;
                        oUsuario.ID_CATEGORIA_USUARIO = oResultadoConsulta.ID_CATEGORIA_USUARIO;
                        oUsuario.CATEGORIA_USUARIO = oResultadoConsulta.CATEGORIA_USUARIO;
                        //oUsuario.Tag = oResultadoConsulta.Tag;
                    }
                    else
                    {
                        oUsuario = null;
                    }
                    return oUsuario;
                }
                catch (Exception ex)
                {
                   // axError.setMsjError(ex, "getOneUsuario");
                    //Entidades.ErrorBD.setMsjError(ex.Message);
                    return null;
                }
            }
        }

    }
}
