﻿Imports Newtonsoft
Public Class ConfiguracionN
    Private oDB As AccesoDatos.ConfiguracionAD

    Public Sub New()
        oDB = New AccesoDatos.ConfiguracionAD()
    End Sub

    Public Function GetOne() As Entidades.Configuracion
        Return Me.oDB.getOne()
    End Function

    Public Function Guardar(ByVal oConfiguracion As Entidades.Configuracion) As Boolean
        Return oDB.Guardar(oConfiguracion)
    End Function

    '''' <summary>
    '''' Permite obtener el tiempo de espera
    '''' </summary>
    '''' <param name="Planta"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Function GetTiempoEspera() As Integer

    '    Dim Conf As Entidades.Configuracion = oDB.getOne

    '    Return If(Planta = Entidades.Constante.PLantaErro.ACOPIO, Conf.tiempoBalanzaAcopio, Conf.tiempoBalanzaCelda)

    'End Function

    'Public Sub SetTiempoEspera(ByVal Planta As Entidades.Constante.PLantaErro, ByVal Tiempo As Integer)

    '    oDB.setTiempoEsperaBalanza(Planta, Tiempo)
    'End Sub

    'Public Function setCAMION_PESANDO(ByVal numBalanza As Integer, ByVal CAMION_PESANDO As Entidades.CAMION_PESANDO)
    '    Dim Configuracion As Entidades.Configuracion = oDB.getOne

    '    Dim jsonCAMION_PESANDO As String = If(IsNothing(CAMION_PESANDO), "", Json.JsonConvert.SerializeObject(CAMION_PESANDO))


    '    Return oDB.setCAMION_PESANDO(numBalanza, jsonCAMION_PESANDO)

    'End Function

    'Public Function getCAMION_PESANDO(ByVal numBalanza As Integer) As Entidades.CAMION_PESANDO
    '    Dim configuracion As Entidades.Configuracion = oDB.getOne()

    '    Dim jsonCAMION_PESANDO As String

    '    'If Planta = Entidades.Constante.PLantaErro.ACOPIO Then
    '    jsonCAMION_PESANDO = If(numBalanza = 1, configuracion.CAMION_PESANDOBal1, configuracion.CAMION_PESANDOBal2)

    '    If IsNothing(jsonCAMION_PESANDO) Then Return Nothing

    '    Dim CAMION_PESANDO As Entidades.CAMION_PESANDO = Json.JsonConvert.DeserializeObject(Of Entidades.CAMION_PESANDO)(jsonCAMION_PESANDO)

    '    Return CAMION_PESANDO

    'End Function


End Class
