﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entidades
{
    /// <summary>
    /// CLASE QUE CONTIENE LAS CLASES QUE REPRESENTAN LOS DATOS ENVIADOS POR BIT 
    /// EN FORMATO JSON
    /// </summary>
    public class WsJsonInputAumax
    {

        #region WS SILOS
        /// <summary>
        /// Clase que viene en formato JSON  en el parametro "input" del wbServive AltaModSilo
        /// </summary>
        public class AltaModSilo
        {
            public static string _NOMBRE = "AltaModSilo";

          


            public string nombre = "AltaModSilo";

            /// <summary>
            /// Id generado por BIT. Unívoco por cada operación realizada.
            /// </summary>
            public Int64 idComando { get; set; }
            /// <summary>
            /// 1= Modificación. No se usará el Alta, ya que previamente los Silos existiránen el PCS7. 
            /// </summary>
            public Int16 comando { get; set; }
            /// <summary>
            /// Identificador único por cada silo de la planta
            /// </summary>
            public Int16 idSilo { get; set; }
            /// <summary>
            /// Nombre del Silo
            /// </summary>
            public string silo { get; set; }
            /// <summary>
            /// Identificador único del producto contenido en el silo
            /// </summary>
            public Int16 idProducto { get; set; }
            /// <summary>
            /// Nombre del Producto
            /// </summary>
            public string producto { get; set; }
            /// <summary>
            /// Capacidad total en Kg del Silo
            /// </summary>
            public Int32 capacidad { get; set; }
            /// <summary>
            /// Existencia total en Kg del producto en el Silo
            /// </summary>
            public Int32 existenTotal { get; set; }


            /// <summary>
            /// Enumerador de comando para wl wsAltaModSilo
            /// 1= Modificación. No se usará el Alta, ya que previamente los Silos existiránen el PCS7. 
            /// </summary>
            enum Comando
            {
                MODDIFICACION = 1

            }

        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public class AlarmaSilo{
        //    public int idSilo { get; set; }
        //    public int idAlarma { get; set; }
        //    public string mensaje { get; set; }
        //    public double valor { get; set; }
        //}
        #endregion

        #region DESCARGA
        /// <summary>
        /// Clase que representa los datos enviado por BIT en formato json
        /// Estos datos permite el Alta de una OT en DESCARGA
        /// </summary>
        public class AltaOTDesc
        {
           

            public static string _NOMBRE = "AltaOTDesc";

            public string nombre =  _NOMBRE;
            /// <summary>
            /// 1= Activación de OT desde BIT. 
            /// </summary>
            public int comando { get; set; }
            /// <summary>
            /// Número de OT generado por BIT.
            /// </summary>
            public int numOT { get; set; }
            /// <summary>
            /// Identificador único de la Volcadora de Origen 1(usar Id de Sector). Esta no puede ser 0
            /// </summary>
            public int idOrigen1 { get; set; }
            /// <summary>
            /// Identificador único de la Volcadora de Origen 2(usar Id de Sector). Si se usa una sóla volcadora en la OT, debe ser 0 y los IdOrigen_3…_6 también
            /// </summary>
            public int idOrigen2 { get; set; }
            /// <summary>
            /// Identificador único de la Volcadora de Origen 3(usar Id de Sector). Si se usan dos volcadoras en la OT, debe ser 0 y los IdOrigen_4…_6 también
            /// </summary>
            public int idOrigen3 { get; set; }
            /// <summary>
            /// Identificador único de la Volcadora de Origen 4(usar Id de Sector). Si se usan tres volcadoras en la OT, debe ser 0 y los IdOrigen_5…_6 también
            /// </summary>
            public int idOrigen4 { get; set; }
            /// <summary>
            /// Identificador único del Silo destino.
            /// </summary>
            public int idDestino { get; set; }
            /// <summary>
            /// Identificador único del producto a descargar.
            /// </summary>
            public int idProducto { get; set; }
            /// <summary>
            /// Fecha planificada desde BIT para el inicio de la OT. Sólo para visualización en PCS7
            /// </summary>
            public DateTime fechaHoraPlanificada { get; set; }
            /// <summary>
            /// Usuario responsable de la OT. Sólo para visualización en PCS7
            /// </summary>
            public string usuarioResponsable { get; set; }
            /// <summary>
            /// 1…4 para Descarga. Posición de Cola de OTs
            /// </summary>
            public int numPosicionCola { get; set; }


        }
        /// <summary>
        /// Clase que representa los datos enviados por BIT 
        /// Hace referencia a una evento que quiere notificar al automatismo
        /// WebService EventoOTDescToElectroluz(BIT>Aumax>Electroluz)
        /// </summary>
        public class EventoOTDescToElectroluz
        {

            public static string _NOMBRE = "EventoOTDescToElectroluz";

            public string nombre = _NOMBRE;
            /// <summary>
            /// Número de OT
            /// </summary>
            public int numOTEvento { get; set; }
            /// <summary>
            /// 1= Existencia máxima en Silo, 2= Riesgo de Mezcla….
            /// </summary>
            public int idEvento { get; set; }
            /// <summary>
            /// Mensaje para describir el evento
            /// </summary>
            public string mensajeEvento { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Para IdEvento=1 se debe indicar el % de llenado del Silo.  
            /// Para IdEvento=2 se debe indicar el IdOrigen donde se puede producir mezcla.
            /// </summary>
            public double valorEvento { get; set; }
        }
        #endregion

        #region CARGA
        /// <summary>
        /// Clase que representa los datos enviado en formato JSON por bit
        /// estos datos damos de alta una OT en el sector de CARGA
        /// </summary>
        public class AltaOTCarga
        {


            public string nombre = "AltaOTCarga";
            /// <summary>
            /// 1= Activación de OT desde BIT. 
            /// </summary>
            public int comando { get; set; }
            /// <summary>
            /// Número de OT generado por BIT.
            /// </summary>
            public int numOT { get; set; }
            /// <summary>
            /// Identificador único del Destino
            /// </summary>
            public int idDestino { get; set; }
            /// <summary>
            /// Identificador único del Silo origen. Este no puede ser 0.
            /// </summary>
            public int idOrigen1 { get; set; }
            /// <summary>
            /// Porcentaje de salida de mercadería desde el Silo. 
            /// En caso de ser el único silo, el valor debe ser 100.00
            /// </summary>
            public double valorOrigen1 { get; set; }

            /// <summary>
            /// Identificador único del Silo origen. Este no puede ser 0.
            /// </summary>
            public int idOrigen2 { get; set; }
            /// <summary>
            /// Porcentaje de salida de mercadería desde el Silo. 
            /// En caso de ser el único silo, el valor debe ser 100.00
            /// </summary>
            public double valorOrigen2 { get; set; }

            /// <summary>
            /// Identificador único del Silo origen. Este no puede ser 0.
            /// </summary>
            public int idOrigen3 { get; set; }
            /// <summary>
            /// Porcentaje de salida de mercadería desde el Silo. 
            /// En caso de ser el único silo, el valor debe ser 100.00
            /// </summary>
            public double valorOrigen3 { get; set; }

            /// <summary>
            /// Identificador único del Silo origen. Este no puede ser 0.
            /// </summary>
            public int idOrigen4 { get; set; }
            /// <summary>
            /// Porcentaje de salida de mercadería desde el Silo. 
            /// En caso de ser el único silo, el valor debe ser 100.00
            /// </summary>
            public double valorOrigen4 { get; set; }
            /// <summary>
            /// Identificador único del producto a cargar.
            /// </summary>
            public int idProducto { get; set; }
            /// <summary>
            /// Peso total a Cargar
            /// </summary>
            public int totalCarga { get; set; }
            /// <summary>
            /// Fecha planificada desde BIT para el inicio de la OT. Sólo para visualización en PCS7
            /// </summary>
            public DateTime fechaHoraPlanificada { get; set; }
            /// <summary>
            /// Usuario responsable de la OT. Sólo para visualización en PCS7
            /// </summary>
            public string usuarioResponsable { get; set; }
            /// <summary>
            /// 1=Carga de Camión 2=Carga de Buque 3=Trasile
            /// </summary>
            public int tipoOperacion { get; set; }
            /// <summary>
            /// 1…4 para Carga de Camiones. Posición de Cola de OTs
            /// </summary>
            public int numPosicionCola { get; set; }

        }

        public class EventoOTCargaToElectroluz
        {

            public static string _NOMBRE = "EventoOTCargaToElectroluz";


            public string nombre { get { return _NOMBRE; } }
            /// <summary>
            /// Número de OT
            /// </summary>
            public int numOTEvento { get; set; }
            /// <summary>
            /// 1= Existencia máxima en Silo, 2= Riesgo de Mezcla….
            /// </summary>
            public int idEvento { get; set; }
            /// <summary>
            /// Mensaje para describir el evento
            /// </summary>
            public string mensajeEvento { get; set; }
            /// <summary>
            /// Valor de la variable relacionada al evento. 
            /// Para IdEvento=1 se debe indicar el % de llenado del Silo.  
            /// Para IdEvento=2 se debe indicar el IdOrigen donde se puede 
            /// producir mezcla.
            /// </summary>
            public double valorEvento { get; set; }
        }


        #endregion



    }
}