﻿Imports Entidades
Public Class FrmControlAcceso


#Region "PROPIEDADES"
    Private oError As New Entidades.AX_ERROR
    Private oErrorN As New Negocio.ErrorN
    Private oPuesto As New Entidades.PUESTO_TRABAJO
    Private oPuestoN As New Negocio.Puesto_TrabajoN
#End Region


#Region "CONSTRUCTOR"
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().


        Me.Text = "AxControl " & Entidades.Constante.VERSION

    End Sub


#End Region

#Region "EVENTOS FORM"
    Private Sub FrmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Me.CtrlPorteriaPlantarRFID1.Inicializar()

        'CargarControlesAccesos()
        CtrlMenu1.inicializar()
        Ini()

    End Sub
#End Region


#Region "METODOS"

    Private Sub Ini()
        Dim appConfig As New Negocio.ArchivoAppConfig
        Try
            Dim idControlAcceso As Integer = appConfig.GetControlAccesoIdAppConfig
            Dim nControlAcceso As New Negocio.ControlAccesoN

            Dim oControlAcceso = nControlAcceso.GetOne(idControlAcceso)

            If Not IsNothing(oControlAcceso) Then
                Dim oCtrl As Controles.CtrlAutomationObjectsBase = GetCtrlAcceso(oControlAcceso.ID_CONTROL_ACCESO, oControlAcceso.ID_PLC)
                flpPrincipal.Controls.Add(oCtrl)
            Else

                MsgBox(String.Format("No existe id de control de acceso {0}. Verificar appconfig.", idControlAcceso))
            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            MessageBox.Show(ex.Message)
        End Try



    End Sub

    Function GetCtrlAcceso(idCtrl As Integer, idPLC As Integer) As Control

        Select Case idCtrl
            Case Convert.ToInt16(Constante.Controles.PORTERIA_Ingreso_Egreso)
                Dim oCtrl As New Controles.T1CtrlControlAccesoPorteriaIngresoEgreso(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.ROTONDA_INGRESO)
                Dim oCtrl As New Controles.T2CtrlControlAccesoRotondaIngreso(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.CALADO)
                Dim oCtrl As New Controles.T3CtrlControlAccesoCalado(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.BALANZAS_INGRESO_EGRESO)
                Dim oCtrl As New Controles.T4CtrlControlAccesoBalanzaIngresoEgreso(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.DESCARGA)
                Dim oCtrl As New Controles.T5CtrlControlAccesoDescarga(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.CALLE_INGRESO_EGRESO_CARGA)
                Dim oCtrl As New Controles.T6CtrlControlAccesoCalleIngresoEgresoCarga(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.CARGA_DE_CAMIONES)
                Dim oCtrl As New Controles.T7CtrlControlAccesoPorteriaSoloLectura(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.PORTERIA_PROVEEDORES_y_CALLE_COSTERA)
                Dim oCtrl As New Controles.T8CtrlControlAccesoPorteriaProveedores(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.PORTERIA_LABORATORIO_y_CALLE_INTERNA)
                Dim oCtrl As New Controles.T9CtrlCalleControlLaboratorio(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.MANTENIMIENTO)
                Dim oCtrl As New Controles.T12CtrlControlMantenimiento(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Convert.ToInt16(Constante.Controles.INTERFACE)
                Dim oCtrl As New Controles.T20CtrlControlInterface(idCtrl, idPLC)
                oCtrl.Inicializar()
                Return oCtrl
            Case Else
                Return Nothing
        End Select



    End Function

    ''' <summary>
    ''' Permite cargar los controles de acceso asignado al puesto de trabajo
    ''' </summary>
    Private Sub CargarControlesAccesos()
        Try
            'Obtengo los controles de acceso asignado al puesto de trabajo
            Dim listControlAcceso As List(Of Entidades.CONTROL_ACCESO) = Negocio.ModSesion.PUESTO_TRABAJO.CONTROL_ACCESO.ToList()
            'Recorro los controles de acceso
            For Each ControlAcceso As Entidades.CONTROL_ACCESO In listControlAcceso


                flpPrincipal.Controls.Add(GetCtrlControlAcceso(ControlAcceso))
                'Creo un tab con el controle de acceso
                'NuevoTabPage(ControlAcceso)

            Next
        Catch ex As Exception
            oError.LINEA = Mid(ex.StackTrace, ex.StackTrace.Length - 2, 3)

            oErrorN.Guardar(System.DateTime.Now, Environment.MachineName, ex.TargetSite.DeclaringType.Name, ex.TargetSite.Name, 1, ex.Message)
        End Try


    End Sub




    ''' <summary>
    ''' Permite obtener el control de acceso correspondiente al tipo de control de acceso
    ''' </summary>
    ''' <returns></returns>
    Private Function GetCtrlControlAcceso(ControlAcceso As Entidades.CONTROL_ACCESO) As Control

        Dim ctrlControlAcceso As Control = Nothing
        Select Case ControlAcceso.NOMBRE


            Case Entidades.Constante.TIPO_CONTROL_ACCESO.DESCARGA
                Dim ctrlDescarga As New Controles.CtrlControlAccesoDescarga1
                ctrlDescarga.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrlDescarga.Inicializar()
                ctrlControlAcceso = ctrlDescarga


            Case Entidades.Constante.TIPO_CONTROL_ACCESO.CALADO2
                Dim ctrCalado As New Controles.CtrlControlAccesoCalado2Calles
                ctrCalado.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrCalado.Inicializar()
                ctrlControlAcceso = ctrCalado

            Case Entidades.Constante.TIPO_CONTROL_ACCESO.CALADO4
                Dim ctrCalado As New Controles.CtrlControlAccesoCalado4Calles
                ctrCalado.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrCalado.Inicializar()
                ctrlControlAcceso = ctrCalado

            Case Entidades.Constante.TIPO_CONTROL_ACCESO.INGRESO_BARRERA_DOBLE
                Dim ctrCalado As New Controles.CtrlControlAccesoDobleBarrera
                ctrCalado.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrCalado.Inicializar()
                ctrlControlAcceso = ctrCalado


            Case Entidades.Constante.TIPO_CONTROL_ACCESO.INGRESO_CALADO
                Dim ctrCalado As New Controles.CtrlControlAccesoIngresoCalado
                ctrCalado.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrCalado.Inicializar()
                ctrlControlAcceso = ctrCalado

            Case Entidades.Constante.TIPO_CONTROL_ACCESO.INGRESO_CALADO
                Dim ctrCalado As New Controles.CtrlControlAccesoIngresoCalado
                ctrCalado.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrCalado.Inicializar()
                ctrlControlAcceso = ctrCalado

            Case Entidades.Constante.TIPO_CONTROL_ACCESO.BALANZA1
                Dim ctrBalanza1 As New Controles.CtrlControlAccesoBalanza1
                ctrBalanza1.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrBalanza1.Inicializar()
                ctrlControlAcceso = ctrBalanza1




            Case Else
                ctrlControlAcceso = Nothing
        End Select

        Return ctrlControlAcceso
    End Function

    ''' <summary>
    ''' Permite crear un nuevo tab con el control de acceso correspondiente
    ''' </summary>
    ''' <param name="ControlAcceso"></param>
    Private Sub NuevoTabPage(ControlAcceso As Entidades.CONTROL_ACCESO)

        Dim nvoTabPage As New TabPage
        nvoTabPage.Name = ControlAcceso.NOMBRE
        nvoTabPage.Text = ControlAcceso.NOMBRE
        nvoTabPage.UseVisualStyleBackColor = True

        'Creo el control
        Dim ctrlControlAcceso As Control = Nothing
        Select Case ControlAcceso.NOMBRE
            Case Entidades.Constante.TIPO_CONTROL_ACCESO.PORTERIA
                Dim ctrlPorteria As New Controles.CtrlControlAccesoPorteria
                ctrlPorteria.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO ' Le asigno el id  de control de acceso
                ctrlPorteria.Inicializar()
                ctrlControlAcceso = ctrlPorteria

            Case Entidades.Constante.TIPO_CONTROL_ACCESO.BALANZA
                Dim ctrlBalanza2 As New Controles.CtrlControlAccesoBalanza
                ctrlBalanza2.ID_CONTROL_ACCESO = ControlAcceso.ID_CONTROL_ACCESO
                ctrlBalanza2.Inicializado = ControlAcceso.ID_CONTROL_ACCESO
                ctrlControlAcceso = ctrlBalanza2


        End Select

        If Not IsNothing(ctrlControlAcceso) Then
            nvoTabPage.Controls.Add(ctrlControlAcceso)
            'tabControlesAcceso.Controls.Add(nvoTabPage)
        Else
            MessageBox.Show(String.Format("El control de acceso {0} no tiene asignado un tipo de control de acceso", ControlAcceso.NOMBRE),
                            "NuevoTabPage", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


    End Sub



    Private Sub CtrlMenu1_ClickMenu() Handles CtrlMenu1.ClickMenu
        FrmMenu.Show()
    End Sub

    Private Sub FrmControlAcceso_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Environment.Exit(Environment.ExitCode)
    End Sub
#End Region

#Region "SUBPROCESOS"

#End Region





End Class