﻿Imports Entidades
Imports Entidades.Constante

Public Class FrmConfiguracion
    Public oConfig As New Entidades.Configuracion
    Public oConfigN As New Negocio.ConfiguracionN

#Region "PROPIEDADES"

#End Region


#Region "CONSTRUCTOR"
    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        ''Selecciono el unico registro donde tengo la configuración
        oConfig = oConfigN.GetOne()
        ''si existe cargo los valores en los campos del formulario
        If Not IsNothing(oConfig) Then
            txtBuffer.Text = oConfig.TIEMPO_BUFFER
            txtEsperaBalanza.Text = oConfig.TIEMPO_ESPERA
            txtEsperaEnviarPeso.Text = oConfig.TIEMPO_ENVIAR_PESO
            txtFiltro.Text = oConfig.FILTRO_EPC
        End If
    End Sub
#End Region

#Region "EVENTOS FORM"

    ''' <summary>
    ''' Al guardar, pido autenticacion y registro una auditoria al respecto
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim oAutenticacion = New Controles.FrmAutenticarUsuario(CategoriaUsuario.Administrador)

        If oAutenticacion.ShowDialog = DialogResult.OK Then
            If camposCompletos() Then
                Dim descripAuditoria As String
                oConfig.TIEMPO_BUFFER = txtBuffer.Text
                oConfig.TIEMPO_ESPERA = txtEsperaBalanza.Text
                oConfig.TIEMPO_ENVIAR_PESO = txtEsperaEnviarPeso.Text
                oConfig.FILTRO_EPC = txtFiltro.Text
                oConfigN.Guardar(oConfig)
                descripAuditoria = "Modifico la configuración de tiempos TB: " + txtBuffer.Text + " TE: " + txtEsperaBalanza.Text
                Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionTiempos, 0)
            End If
        Else
            Me.Close()
        End If
        Me.Close()
    End Sub
#End Region


#Region "METODOS"
    ''' <summary>
    ''' Controlo que los campos obligatorios esten cargados
    ''' </summary>
    ''' <returns></returns>
    Public Function camposCompletos() As Boolean
        Dim BlnCamposCorrectos As Boolean = True

        Me.EP.SetError(Me.txtBuffer, Nothing)
        'Si no se especifico el tiempo de buffer
        If Len(Trim(Me.txtBuffer.Text)) = 0 Then
            BlnCamposCorrectos = False
            Me.EP.SetError(Me.txtBuffer, "No ha ingresado el tiempo de Buffer")
        End If

        Me.EP.SetError(Me.txtEsperaBalanza, Nothing)
        'Si no se especifico el tiempo de espera
        If Len(Trim(Me.txtEsperaBalanza.Text)) = 0 Then
            BlnCamposCorrectos = False
            Me.EP.SetError(Me.txtEsperaBalanza, "No ha ingresado el tiempo de Espera")
        End If

        Return BlnCamposCorrectos
    End Function
#End Region

#Region "SUBPROCESOS"

#End Region



End Class