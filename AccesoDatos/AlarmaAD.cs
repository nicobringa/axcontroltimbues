﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;


namespace AccesoDatos
{
    public class AlarmaAD
    {
        public Alarma add(Alarma oAlarma)
        {
            try
            {
                ax_ControlEntities oContext = new ax_ControlEntities();
                oContext.Alarmas.Add(oAlarma);
                oContext.SaveChanges();
                return oAlarma;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public Alarma update(Alarma oAlarma)
        {
            try
            {
                ax_ControlEntities oContext = new ax_ControlEntities();
                oContext.Alarmas.Attach(oAlarma);
                oContext.Entry(oAlarma).State = System.Data.Entity.EntityState.Modified;
                oContext.SaveChanges();

                return oAlarma;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Alarma getOne(int id)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.Find(id);

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public Int32 BuscarId(int idSector, int idControlAcceso, int idFalla, string tag)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.Where(o => o.idSector == idSector && o.idControlAcceso == idControlAcceso
                    && o.tag == tag && o.fechaDesaparicion == null).FirstOrDefault();

                    return oConsulta.idAlarma;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int BuscarAlarma(int idSector)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.Where(o => o.idSector == idSector && o.activa==true).Count();
                return oConsulta;
                    
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Alarma getAlarmaExistente(int idSector , int idControlAcceso , string tag , string detalle )
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.Where(o => o.idSector == idSector && o.idControlAcceso == idControlAcceso
                    && o.tag == tag &&o.detalle== detalle && o.fechaDesaparicion == null).FirstOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Int32 getEstadoAlarma(int idSector, int idControlAcceso,int tipoFalla, string tag)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.Where(o => o.idSector == idSector && o.idControlAcceso == idControlAcceso
                     && o.activa==true).FirstOrDefault();

                    if (oConsulta == null)
                    {
                        //si no me devuelve ningún valor la consulata, significa que tiene fecha de desaparición.
                        return 2;
                    }
                    else
                    {
                        int oConsulta2 = oContext.Alarmas.Where(o => o.idSector == idSector && o.idControlAcceso == idControlAcceso
                         && o.activa == true && o.reconocida== false).Count();

                        if (oConsulta2 > 0)
                        {
                            //no tiene fecha de desaparición y no está reconocida
                            return 0;
                        }
                        else
                        {

                            //si no tiene fecha de desaparición, pero está reconocida.
                            return 1;
                        }
                    }


                   
                }
            }
            catch (Exception ex)
            {
               
                return 0;
            }
        }


        public List<Alarma> getAll()
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.OrderByDescending(a => a.fechaAparicion).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Alarma> getAllActiva(int idSector)
        {
            try
            {
                using (ax_ControlEntities oContext = new ax_ControlEntities())
                {
                    var oConsulta = oContext.Alarmas.Where(a=>a.activa==true).OrderByDescending(a => a.fechaAparicion).ToList();

                    if (idSector > 0)
                    {
                        oConsulta= oContext.Alarmas.Where(a => a.activa == true && a.idSector== idSector).OrderByDescending(a => a.fechaAparicion).ToList();
                    }

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
