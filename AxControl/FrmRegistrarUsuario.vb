﻿Public Class FrmRegistrarUsuario
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim oUsuarioN As New Negocio.UsuarioN
        If CamposCorrectos() Then

            If Negocio.UsuarioN.getPorNombreCuenta(Me.txtUsuario.Text) = False Then
                MsgBox("El Usuario ingresado ya existe!")
                ''siempre que exista error salgo del procedimiento
                Exit Sub
            Else
                Negocio.UsuarioN.AddUsuario(txtUsuario.Text, txtContrasenia.Text, cmbCategoria.SelectedValue)
            End If
        Else
            Exit Sub
        End If
        LimpiarCampos()
        MsgBox("Usuario registrado con exito!")
        Me.Close()
    End Sub

    Private Sub FrmRegistrarUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarCombos()
        cmbCategoria.SelectedIndex = -1
    End Sub

    Private Sub CargarCombos()
        Dim oCategoriaUsuarioN As New Negocio.Categoria_UsuarioN

        oCategoriaUsuarioN.CargarCombo(Me.cmbCategoria)
    End Sub


    Public Function CamposCorrectos() As Boolean
        Try
            Dim BlnCamposCorrectos As Boolean = True


            Me.err.SetError(Me.txtUsuario, Nothing)
            'Si no ha ingresado cuenta
            If Len(Trim(Me.txtUsuario.Text)) = 0 Then
                BlnCamposCorrectos = False
                Me.err.SetError(Me.txtUsuario, "No ha ingresado Nombre de Usuario")
            End If

            Me.err.SetError(Me.txtContrasenia, Nothing)
            'Si no ha ingresado contraseña
            If Len(Trim(Me.txtContrasenia.Text)) = 0 Then
                BlnCamposCorrectos = False
                Me.err.SetError(Me.txtContrasenia, "No ha ingresado Contraseña")
            End If


            'Si no ha seleccionado categoria
            Me.err.SetError(Me.cmbCategoria, Nothing)
            If Me.cmbCategoria.SelectedValue = 0 Then
                BlnCamposCorrectos = False
                Me.err.SetError(Me.cmbCategoria, "No ha seleccionado Categoría")
            End If

            Return BlnCamposCorrectos
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Sub LimpiarCampos()
        txtUsuario.Text = ""
        txtContrasenia.Text = ""
        cmbCategoria.SelectedIndex = -1
    End Sub

End Class