﻿Imports System.Text
Imports System.Threading
Imports Entidades
Imports Entidades.Constante
Public Class CtrlSectorInterfaceDescarga


    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()




    End Sub

    Private _oSector As SECTOR
    Public Property oSector() As SECTOR
        Get
            Return _oSector
        End Get
        Set(ByVal value As SECTOR)
            _oSector = value
        End Set
    End Property

    Private _nDatoWord As New Negocio.DatoWordIntN
    Private _nDatoDWord As New Negocio.DatoDWordDIntN
    Private _nDatoFloat As New Negocio.DatoFloatN
    Private _nDatoByte As New Negocio.DatoByteN

    Private hMonitoreo As Thread
    ''' <summary>
    ''' Lista de db con que trabaja este interface
    ''' </summary>
    Private _listDB As List(Of DbDescarga)

    ''' <summary>
    ''' Unico Contructor
    ''' </summary>
    ''' <param name="oSector">Objeto que represetan el sector</param>
    ''' <param name="cantDb">Cant de db que trabaja este interface</param>
    ''' <param name="startDB">Nro de inicio de debe</param>
    Public Sub New(oSector As SECTOR, cantDb As Integer, startDB As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.oSector = oSector
        Me._listDB = New List(Of DbDescarga)()
        CrearDb(cantDb, startDB)
        Inicializar()

    End Sub



#Region "METODOS"
    Public Overrides Sub Inicializar()
        MyBase.Inicializar()


        InicializarCtrl()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()

    End Sub

    Private Sub InicializarCtrl()

        CargarPropiedadesDatoWord()
        CargarPropiedadesDatoDWord()
        CargarPropiedadesDatoFloat()
        CargarPropiedadesDatoByte()

    End Sub

    ''' <summary>
    ''' Permite cargar las propiedades datoWord
    ''' </summary>
    Private Sub CargarPropiedadesDatoWord()

        'Recorro los db del plc pata este interface
        For Each plcDb In _listDB
            'Busco los datos word para este sector
            Dim listDato As List(Of Entidades.DATO_WORDINT) = _nDatoWord.GetAllPorSector(Me.oSector.ID_SECTOR, plcDb.NroDB)
            'Recorro los datos word y se los asgino a la propiedad correspondiente
            For Each itemTag As Entidades.DATO_WORDINT In listDato

                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

                If tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.COMANDO) Then
                    plcDb.Comando = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_DESTINO) Then
                    plcDb.idDestino = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_ORIGEN1) Then
                    plcDb.idOrigen1 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_ORIGEN2) Then
                    plcDb.idOrigen2 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_ORIGEN3) Then
                    plcDb.idOrigen3 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_ORIGEN4) Then
                    plcDb.idOrigen4 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_PRODUCTO) Then
                    plcDb.IdProducto = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.TIPO_OPERACION) Then
                    plcDb.TipoOperacion = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_EVENTO) Then
                    plcDb.IdEvento = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.ID_EVENTO_TO_ELECTROLUZ) Then
                    plcDb.IdEventoToElectroLuz = itemTag
                End If
            Next
        Next





    End Sub

    ''' <summary>
    ''' Permite cargar los datos dword
    ''' </summary>
    Private Sub CargarPropiedadesDatoDWord()

        'Recorro los db del plc pata este interface
        For Each plcDb In _listDB
            'Busco los datos word para este sector
            Dim listDato As List(Of Entidades.DATO_DWORDDINT) = _nDatoDWord.GetAllPorSector(Me.oSector.ID_SECTOR, plcDb.NroDB)
            'Recorro los datos word y se los asgino a la propiedad correspondiente
            For Each itemTag As Entidades.DATO_DWORDDINT In listDato

                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

                If tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.NUM_OT) Then
                    plcDb.NumOT = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.NUM_OT_EVENTO) Then
                    plcDb.NumOTEvento = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.NUM_OT_EVENTO_TO_ELECTROLUZ) Then
                    plcDb.NumOTEventoToElectroLuz = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.TOTAL_A_CARGAR) Then
                    plcDb.TotalACargar = itemTag
                End If
            Next
        Next



    End Sub
    ''' <summary>
    ''' Permite cargar los datos float()
    ''' </summary>
    Private Sub CargarPropiedadesDatoFloat()


        'Recorro los db del plc pata este interface
        For Each plcDb In _listDB
            'Busco los datos word para este sector
            Dim listDato As List(Of Entidades.DATO_FLOAT) = _nDatoFloat.GetAllPorSector(Me.oSector.ID_SECTOR, plcDb.NroDB)
            'Recorro los datos FLOAT y se los asgino a la propiedad correspondiente
            For Each itemTag As Entidades.DATO_FLOAT In listDato

                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

                If tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_ORIGEN1) Then
                    plcDb.ValorOrigen1 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_ORIGEN2) Then
                    plcDb.ValorOrigen2 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_ORIGEN3) Then
                    plcDb.ValorOrigen3 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_ORIGEN4) Then
                    plcDb.ValorOrigen4 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_EVENTO1) Then
                    plcDb.ValorEvento1 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_EVENTO2) Then
                    plcDb.ValorEvento2 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_EVENTO3) Then
                    plcDb.ValorEvento3 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_EVENTO4) Then
                    plcDb.ValorEvento4 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_EVENTO5) Then
                    plcDb.ValorEvento5 = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.VALOR_EVENTO_TO_ELECTROLUZ) Then
                    plcDb.ValorEventoToElectroLuz = itemTag
                End If
            Next

        Next


    End Sub

    ''' <summary>
    ''' Permite cargar las propiedades de tipo byte
    ''' </summary>
    Private Sub CargarPropiedadesDatoByte()

        'Recorro los db del plc pata este interface
        For Each plcDb In _listDB
            'Busco los datos word para este sector
            Dim listDato As List(Of Entidades.DATO_BYTE) = _nDatoByte.GetAllPorSector(Me.oSector.ID_SECTOR, plcDb.NroDB)
            'Recorro los datos word y se los asgino a la propiedad correspondiente
            For Each itemTag As Entidades.DATO_BYTE In listDato

                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

                If tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.FECHA_HORA_PLANIFICADO) Then
                    plcDb.FechaHoraPlanificada = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.USUARIO_RESPONSABLE) Then
                    plcDb.UsuarioResponsable = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.MENSAJE_EVENTO_TO_ELECTROLUZ) Then
                    plcDb.MensajeEventoToElectroLuz = itemTag
                ElseIf tag.Equals(PROPIEDADES_INTERFACE_DESCARGA.MENSAJE_EVENTO) Then
                    plcDb.MensajeEvento = itemTag
                End If
            Next
        Next



    End Sub

    Private Sub ProcesarWS()
        Dim nInterface = New Negocio.InterfaceN

        'Busco los ws a procesar AltaOTDesc
        Dim listWS = nInterface.GetAll(WsJsonInputAumax.AltaOTDesc._NOMBRE, Entidades.Constante.ESTADO_REGISTRO.SIN_PROCESAR)

        If listWS.Count > 0 Then nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "WS A PROCESAR  AltaOTDesc= " & listWS.Count, Nothing)

        For Each ws In listWS 'Recorro los ws a procesar
            RegistrarDatosPLc_WsAltaOTDesc(ws)
        Next

        'Busco los ws a procesar EventoOTDescToElectroluz
        listWS = nInterface.GetAll(WsJsonInputAumax.EventoOTDescToElectroluz._NOMBRE, Entidades.Constante.ESTADO_REGISTRO.SIN_PROCESAR)

        If listWS.Count > 0 Then nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "WS A PROCESAR  EventoOTDescToElectroluz= " & listWS.Count, Nothing)

        For Each ws In listWS 'Recorro los ws a procesar
            RegistrarDatosPLc_WsAltaOTDescToElectroluz(ws)
        Next

    End Sub

    ''' <summary>
    ''' Permite setear las propiedades para que se escriban las variables del plc
    ''' para el ws AltaOTDesc
    ''' </summary>
    ''' <param name="oInterfaceWS"></param>
    Private Sub RegistrarDatosPLc_WsAltaOTDesc(oInterfaceWS As INTERFACE_WS)

        Dim nInterface As New Negocio.InterfaceN
        'Serializo el wsAltaOTDesc
        Dim altaOTDesc = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.AltaOTDesc)(oInterfaceWS.JSON_INPUT)
        Dim plcDb As DbDescarga
        'Obtengo el db segun la posición de cola enviada en el ws
        Select Case altaOTDesc.numPosicionCola
            Case 1 ' Posicion en cola
                plcDb = _listDB.Where(Function(o) o.NroDB = CType(DB_POSICION_COLA_DESCARGA.POS_COLA_1, Integer)).FirstOrDefault()
            Case 2
                plcDb = _listDB.Where(Function(o) o.NroDB = CType(DB_POSICION_COLA_DESCARGA.POS_COLA_2, Integer)).FirstOrDefault()
            Case 3
                plcDb = _listDB.Where(Function(o) o.NroDB = CType(DB_POSICION_COLA_DESCARGA.POS_COLA_3, Integer)).FirstOrDefault()
            Case 4
                plcDb = _listDB.Where(Function(o) o.NroDB = CType(DB_POSICION_COLA_DESCARGA.POS_COLA_4, Integer)).FirstOrDefault()
            Case Else
                plcDb = Nothing
        End Select

        If IsNothing(plcDb) Then '  si no se encontrol el db para el plc
            'En el ws se envió un numero de cola que no existe
            nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "[RegistrarDatosPLc] = " &
                                 String.Format("El numero de Posición cola {0} no existe DB en el plc para esta posición", altaOTDesc.numPosicionCola), oInterfaceWS.ID_REGISTRO)
            'Paso el ws enviado a finalizado
            oInterfaceWS.ID_ESTADO = Entidades.Constante.ESTADO_REGISTRO.ENVIO_ERRO
            nInterface.Update(oInterfaceWS)
            Return 'Finalizo el proceso
        End If

        'Cargo las variables del plc para escribirlas en la plc
        plcDb.Comando.VALOR = altaOTDesc.comando
        plcDb.NumOT.VALOR = altaOTDesc.numOT
        plcDb.idOrigen1.VALOR = altaOTDesc.idOrigen1
        plcDb.idOrigen2.VALOR = altaOTDesc.idOrigen2
        plcDb.idOrigen3.VALOR = altaOTDesc.idOrigen3
        plcDb.idOrigen4.VALOR = altaOTDesc.idOrigen4
        plcDb.idDestino.VALOR = altaOTDesc.idDestino
        plcDb.IdProducto.VALOR = altaOTDesc.idProducto
        plcDb.FechaHoraPlanificada.VALOR = Encoding.ASCII.GetBytes(altaOTDesc.fechaHoraPlanificada.ToString("dd/MM/yyyy hh:mm:ss"))
        If Not IsNothing(altaOTDesc.usuarioResponsable) Then
            plcDb.UsuarioResponsable.VALOR = Encoding.ASCII.GetBytes(altaOTDesc.usuarioResponsable)
        Else
            plcDb.UsuarioResponsable.VALOR = Encoding.ASCII.GetBytes("Sin definición")
        End If


        'Escribo en axDriver para que luego se registre en el plc
        _nDatoWord.Escribir(plcDb.Comando)
        _nDatoDWord.Escribir(plcDb.NumOT)
        _nDatoWord.Escribir(plcDb.idOrigen1)
        _nDatoWord.Escribir(plcDb.idOrigen2)
        _nDatoWord.Escribir(plcDb.idOrigen3)
        _nDatoWord.Escribir(plcDb.idOrigen4)
        _nDatoWord.Escribir(plcDb.idDestino)
        _nDatoWord.Escribir(plcDb.IdProducto)
        _nDatoByte.Escribir(plcDb.FechaHoraPlanificada)
        _nDatoByte.Escribir(plcDb.UsuarioResponsable)
        'Actualizo el estado a registrado en el plc
        oInterfaceWS.ID_ESTADO = Entidades.Constante.ESTADO_REGISTRO.REGISTRADO_PLC

        nInterface.Update(oInterfaceWS)

        'Agrego el vento
        nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "[RegistrarDatosPLc] = " & "WS REGISTRADO EN PLC ID_REGISTRO " & oInterfaceWS.ID_REGISTRO, Nothing)

    End Sub

    ''' <summary>
    ''' Permite setear las propiedades para que se escriban las variables del plc
    ''' para el ws EventoOTDescToElectroluz
    ''' </summary>
    ''' <param name="oInterfaceWS"></param>
    Private Sub RegistrarDatosPLc_WsAltaOTDescToElectroluz(oInterfaceWS As INTERFACE_WS)

        Dim nInterface As New Negocio.InterfaceN
        'Serializo el wsAltaOTDesc
        Dim eventoOTDesc = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.EventoOTDescToElectroluz)(oInterfaceWS.JSON_INPUT)
        'Uso el primer db del plc para registrar los eventos enviado por BIT
        Dim plcDb As DbDescarga = _listDB(0)

        'Cargo las variables del plc para escribirlas en la plc
        plcDb.NumOTEventoToElectroLuz.VALOR = eventoOTDesc.numOTEvento
        plcDb.IdEventoToElectroLuz.VALOR = eventoOTDesc.idEvento
        plcDb.MensajeEventoToElectroLuz.VALOR = Encoding.ASCII.GetBytes(eventoOTDesc.mensajeEvento)
        plcDb.ValorEventoToElectroLuz.VALOR = eventoOTDesc.valorEvento
        'Escribo en el plc
        _nDatoDWord.Escribir(plcDb.NumOTEventoToElectroLuz)
        _nDatoWord.Escribir(plcDb.IdEventoToElectroLuz)
        _nDatoByte.Escribir(plcDb.MensajeEventoToElectroLuz)
        _nDatoFloat.Escribir(plcDb.ValorEventoToElectroLuz)
        'Actualizo el estado a registrado en el plc
        oInterfaceWS.ID_ESTADO = Entidades.Constante.ESTADO_REGISTRO.REGISTRADO_PLC

        nInterface.Update(oInterfaceWS)

        'Agrego el vento
        nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "[RegistrarDatosPLc] = " & "WS REGISTRADO EN PLC ID_REGISTRO " & oInterfaceWS.ID_REGISTRO, Nothing)

    End Sub

    ''' <summary>
    ''' Verifica si tiene una respuesta del plc de algun comando enviado
    ''' </summary>
    Private Sub LeerRtaPLC()
        Dim ninterface As New Negocio.InterfaceN
        'Recorro los db del plc pata este interface
        For Each plcDb In _listDB

            If plcDb.IdEvento.VALOR > 0 Then '  El plc genero un evento
                Dim eventoOT As New Entidades.WsJsonInputBit.EventoOTDesc
                'Armo la repuesta
                eventoOT.nrOTEvento = plcDb.NumOTEvento.VALOR
                eventoOT.idEvento = plcDb.IdEvento.VALOR
                If Not IsNothing(plcDb.MensajeEvento.VALOR) Then
                    eventoOT.mensajeEvento = System.Text.Encoding.Default.GetString(plcDb.MensajeEvento.VALOR, 2, plcDb.MensajeEvento.CANT_BYTE - 2)
                    eventoOT.mensajeEvento = Constante.BorrarVbNull(eventoOT.mensajeEvento)
                End If
                eventoOT.valorEvento1 = plcDb.ValorEvento1.VALOR
                eventoOT.valorEvento2 = plcDb.ValorEvento2.VALOR
                eventoOT.valorEvento3 = plcDb.ValorEvento3.VALOR
                eventoOT.valorEvento4 = plcDb.ValorEvento4.VALOR
                eventoOT.valorEvento5 = plcDb.ValorEvento5.VALOR
                eventoOT.idComando = ninterface.GetIdComando()

                'Registro el ws en la base de datos
                Dim oInterfaceWS As New Entidades.INTERFACE_WS
                oInterfaceWS.FECHA = DateTime.Now
                oInterfaceWS.ID_ESTADO = CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_ERRO, Int16) ' Pongo el estado como error porque todavia no le envie a BIT
                oInterfaceWS.WEB_SERVICE = Entidades.WsJsonInputBit.EventoOTDesc._NOMBRE
                oInterfaceWS = ninterface.Add(oInterfaceWS)

                If Not IsNothing(oInterfaceWS) Then 'Si se creo el es en la base de datos
                    'Asigno el idRegistro creado en la base de datos a la Alarma de silo
                    eventoOT.idRegistroWS = oInterfaceWS.ID_REGISTRO
                    'Pongo el idAlarma en cero en el PLC
                    _nDatoWord.Escribir(plcDb.IdEvento)
                    'Creo un hilo para enviar la alarma por ws
                    Dim hEnviaEvento = New Thread(AddressOf EnviarEventoOT)
                    hEnviaEvento.IsBackground = True
                    hEnviaEvento.Start(eventoOT)

                End If

            End If



        Next

    End Sub

    ''' <summary>
    ''' Actualiza las propiedades que son de lectura
    ''' Busca en la base de datos de axDriver los nuevos datos de estas propiedades
    ''' </summary>
    Private Sub ActualizarPropiedadesLectura()

        'Recorro los db del plc pata este interface
        For Each plcDb In _listDB
            'DATOS WORD
            plcDb.Comando = _nDatoWord.GetOne(plcDb.Comando.ID)
            plcDb.idDestino = _nDatoWord.GetOne(plcDb.idDestino.ID)
            plcDb.idOrigen1 = _nDatoWord.GetOne(plcDb.idOrigen1.ID)
            plcDb.idOrigen2 = _nDatoWord.GetOne(plcDb.idOrigen2.ID)
            plcDb.idOrigen3 = _nDatoWord.GetOne(plcDb.idOrigen3.ID)
            plcDb.idOrigen4 = _nDatoWord.GetOne(plcDb.idOrigen4.ID)
            plcDb.IdProducto = _nDatoWord.GetOne(plcDb.IdProducto.ID)
            plcDb.TipoOperacion = _nDatoWord.GetOne(plcDb.TipoOperacion.ID)
            plcDb.IdEvento = _nDatoWord.GetOne(plcDb.IdEvento.ID)
            plcDb.IdEventoToElectroLuz = _nDatoWord.GetOne(plcDb.IdEventoToElectroLuz.ID)
            'DATOS DWORD
            plcDb.NumOT = _nDatoDWord.GetOne(plcDb.NumOT.ID)
            plcDb.NumOTEvento = _nDatoDWord.GetOne(plcDb.NumOTEvento.ID)
            plcDb.NumOTEventoToElectroLuz = _nDatoDWord.GetOne(plcDb.NumOTEventoToElectroLuz.ID)
            plcDb.TotalACargar = _nDatoDWord.GetOne(plcDb.TotalACargar.ID)
            'DATO BYTE
            plcDb.FechaHoraPlanificada = _nDatoByte.GetOne(plcDb.FechaHoraPlanificada.ID)
            plcDb.UsuarioResponsable = _nDatoByte.GetOne(plcDb.UsuarioResponsable.ID)
            plcDb.MensajeEventoToElectroLuz = _nDatoByte.GetOne(plcDb.MensajeEventoToElectroLuz.ID)
            plcDb.MensajeEvento = _nDatoByte.GetOne(plcDb.MensajeEvento.ID)
            'DATO FLOAT
            plcDb.ValorEvento1 = _nDatoFloat.GetOne(plcDb.ValorEvento1.ID)
            plcDb.ValorEvento2 = _nDatoFloat.GetOne(plcDb.ValorEvento2.ID)
            plcDb.ValorEvento3 = _nDatoFloat.GetOne(plcDb.ValorEvento3.ID)
            plcDb.ValorEvento4 = _nDatoFloat.GetOne(plcDb.ValorEvento4.ID)
            plcDb.ValorEvento5 = _nDatoFloat.GetOne(plcDb.ValorEvento5.ID)
            plcDb.ValorOrigen1 = _nDatoFloat.GetOne(plcDb.ValorOrigen1.ID)
            plcDb.ValorOrigen2 = _nDatoFloat.GetOne(plcDb.ValorOrigen2.ID)
            plcDb.ValorOrigen3 = _nDatoFloat.GetOne(plcDb.ValorOrigen3.ID)
            plcDb.ValorOrigen4 = _nDatoFloat.GetOne(plcDb.ValorOrigen4.ID)
            plcDb.ValorEventoToElectroLuz = _nDatoFloat.GetOne(plcDb.ValorEventoToElectroLuz.ID)
        Next



    End Sub
    ''' <summary>
    ''' Permite crear los db del plc con el cual va trabajar la interface
    ''' </summary>
    ''' <param name="cantDB"></param>
    ''' <param name="startDB"></param>
    Private Sub CrearDb(cantDB As Integer, startDB As Integer)

        For nroDB = startDB To (startDB - 1) + cantDB ' Creo la cantidad segun la cantidad de db
            Console.WriteLine("Db " & nroDB)
            _listDB.Add(New DbDescarga(nroDB))
        Next

    End Sub

#End Region

#Region "SUBPROCESO"

    Private Sub Monitoreo()

        While True
            Try
                ActualizarPropiedadesLectura()
                ProcesarWS()
                ActualizarPropiedadesLectura()
                LeerRtaPLC()
            Catch ex As Exception
                Dim nInterface As New Negocio.InterfaceN
                nInterface.AddEvento(Constante.ID_EVENTOS.ERROR, "[Monitoreo] = " & ex.Message & " " & ex.StackTrace, Nothing)
            End Try




            Thread.Sleep(500)
        End While
    End Sub

    ''' <summary>
    ''' Permite enviar el evento generado por electroluz a bit
    ''' </summary>
    Private Sub EnviarEventoOT(wsEvento As Entidades.WsJsonInputBit.EventoOTDesc)
        Dim nInterface = New Negocio.InterfaceN
        Try
            Dim jsonEventoOTDesc = Newtonsoft.Json.JsonConvert.SerializeObject(wsEvento)
            'Envia ws a bit
            Dim rta = Negocio.WebServiceBitN.EventoOTDesc(jsonEventoOTDesc)

            'Busco el ws en la base de datos, para guardar su respuesta
            Dim oInterfaceWs = nInterface.GetOne(wsEvento.idRegistroWS)

            If Not IsNothing(oInterfaceWs) Then
                oInterfaceWs.JSON_RPTA_PLC = jsonEventoOTDesc
                oInterfaceWs.ID_ESTADO = IIf(rta = "OK", CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_EXITOSO, Integer), CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_ERRO, Integer))

                nInterface.Update(oInterfaceWs)
            Else
                nInterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                    String.Format("[EnviarEventoOT] Nos encontro el ws AlarmaSilo en la base de datos con ID_REGISTRO  = {0}",
                                                  wsEvento.idRegistroWS),
                                    Nothing)
            End If



        Catch ex As Exception
            'Agregar evento
            nInterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                   String.Format("[EnviarEventoOT]  {0}  - {1} ",
                                                 ex.Message, ex.StackTrace),
                                   Nothing)
        End Try


    End Sub




#End Region

#Region "CLASE HIJA"

    ''' <summary>
    ''' Clase que representa el db de un plc con sus variables del plc como propiedades
    ''' </summary>
    Public Class DbDescarga


        Public Sub New(nroDB As Integer)
            Me.NroDB = nroDB

        End Sub

        Private _NroDB As Integer
        Public Property NroDB() As Integer
            Get
                Return _NroDB
            End Get
            Set(ByVal value As Integer)
                _NroDB = value
            End Set
        End Property

        ''' <summary>
        ''' 1= Activación de OT desde BIT. TRIGGER(debe ser monitoreado por Electroluz y una vez procesado se debe poner en 0)
        ''' </summary>
        Private _Comando As DATO_WORDINT
        Public Property Comando() As DATO_WORDINT
            Get
                Return _Comando
            End Get
            Set(ByVal value As DATO_WORDINT)
                _Comando = value
            End Set
        End Property

        ''' <summary>
        ''' Número de OT generado por BIT.
        ''' </summary>
        Private _NumOT As DATO_DWORDDINT
        Public Property NumOT() As DATO_DWORDDINT
            Get
                Return _NumOT
            End Get
            Set(ByVal value As DATO_DWORDDINT)
                _NumOT = value
            End Set
        End Property

        ''' <summary>
        ''' Identificador único del Silo destino.
        ''' </summary>
        Private _IdDestino As DATO_WORDINT
        Public Property idDestino() As DATO_WORDINT
            Get
                Return _IdDestino
            End Get
            Set(ByVal value As DATO_WORDINT)
                _IdDestino = value
            End Set
        End Property

        ''' <summary>
        ''' Identificador único de la Volcadora de Origen 1(usar Id de Sector). Esta no puede ser 0
        ''' </summary>
        Private _idOrigen1 As DATO_WORDINT
        Public Property idOrigen1() As DATO_WORDINT
            Get
                Return _idOrigen1
            End Get
            Set(ByVal value As DATO_WORDINT)
                _idOrigen1 = value
            End Set
        End Property
        ''' <summary>
        ''' Porcentaje de salida de mercadería desde la PV. Valor ajustado por el operador de calidad  [0% - 100%]
        ''' </summary>
        Private _ValorOrigen1 As DATO_FLOAT
        Public Property ValorOrigen1() As DATO_FLOAT
            Get
                Return _ValorOrigen1
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorOrigen1 = value
            End Set
        End Property

        ''' <summary>
        ''' Identificador único de la Volcadora de Origen 1(usar Id de Sector). Esta no puede ser 0
        ''' </summary>
        Private _idOrigen2 As DATO_WORDINT
        Public Property idOrigen2() As DATO_WORDINT
            Get
                Return _idOrigen2
            End Get
            Set(ByVal value As DATO_WORDINT)
                _idOrigen2 = value
            End Set
        End Property
        ''' <summary>
        ''' Porcentaje de salida de mercadería desde la PV. Valor ajustado por el operador de calidad  [0% - 100%]
        ''' </summary>
        Private _ValorOrigen2 As DATO_FLOAT
        Public Property ValorOrigen2() As DATO_FLOAT
            Get
                Return _ValorOrigen2
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorOrigen2 = value
            End Set
        End Property

        ''' <summary>
        ''' Identificador único de la Volcadora de Origen 1(usar Id de Sector). Esta no puede ser 0
        ''' </summary>
        Private _idOrigen3 As DATO_WORDINT
        Public Property idOrigen3() As DATO_WORDINT
            Get
                Return _idOrigen3
            End Get
            Set(ByVal value As DATO_WORDINT)
                _idOrigen3 = value
            End Set
        End Property
        ''' <summary>
        ''' Porcentaje de salida de mercadería desde la PV. Valor ajustado por el operador de calidad  [0% - 100%]
        ''' </summary>
        Private _ValorOrigen3 As DATO_FLOAT
        Public Property ValorOrigen3() As DATO_FLOAT
            Get
                Return _ValorOrigen3
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorOrigen3 = value
            End Set
        End Property


        ''' <summary>
        ''' Identificador único de la Volcadora de Origen 1(usar Id de Sector). Esta no puede ser 0
        ''' </summary>
        Private _idOrigen4 As DATO_WORDINT
        Public Property idOrigen4() As DATO_WORDINT
            Get
                Return _idOrigen4
            End Get
            Set(ByVal value As DATO_WORDINT)
                _idOrigen4 = value
            End Set
        End Property
        ''' <summary>
        ''' Porcentaje de salida de mercadería desde la PV. Valor ajustado por el operador de calidad  [0% - 100%]
        ''' </summary>
        Private _ValorOrigen4 As DATO_FLOAT
        Public Property ValorOrigen4() As DATO_FLOAT
            Get
                Return _ValorOrigen3
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorOrigen4 = value
            End Set
        End Property

        ''' <summary>
        ''' Identificador único del producto a cargar.
        ''' </summary>
        Private _IdProducto As DATO_WORDINT
        Public Property IdProducto() As DATO_WORDINT
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As DATO_WORDINT)
                _IdProducto = value
            End Set
        End Property
        Private _TotalACargar As DATO_DWORDDINT
        Public Property TotalACargar() As DATO_DWORDDINT
            Get
                Return _TotalACargar
            End Get
            Set(ByVal value As DATO_DWORDDINT)
                _TotalACargar = value
            End Set
        End Property
        ''' <summary>
        ''' Fecha planificada desde BIT para el inicio de la OT.  (ver si esta info solo la presenta BIT)
        ''' </summary>
        Private _FechaHoraPlanificada As DATO_BYTE
        Public Property FechaHoraPlanificada() As DATO_BYTE
            Get
                Return _FechaHoraPlanificada
            End Get
            Set(ByVal value As DATO_BYTE)
                _FechaHoraPlanificada = value
            End Set
        End Property
        ''' <summary>
        ''' Usuario responsable de la OT.  (ver si esta info solo la presenta BIT)
        ''' </summary>
        Private _UsuarioResponsable As DATO_BYTE
        Public Property UsuarioResponsable() As DATO_BYTE
            Get
                Return _UsuarioResponsable
            End Get
            Set(ByVal value As DATO_BYTE)
                _UsuarioResponsable = value
            End Set
        End Property
        ''' <summary>
        ''' 1= Descarga de silos / 2=Trasile / 3=Acondicionado / 4 : Carga de camiones / 5: Carga de Buques
        ''' </summary>
        Private _TipoOperacion As DATO_WORDINT
        Public Property TipoOperacion() As DATO_WORDINT
            Get
                Return _TipoOperacion
            End Get
            Set(ByVal value As DATO_WORDINT)
                _TipoOperacion = value
            End Set
        End Property
        ''' <summary>
        ''' Número de OT
        ''' </summary>
        Private _NumOTEvento As DATO_DWORDDINT
        Public Property NumOTEvento() As DATO_DWORDDINT
            Get
                Return _NumOTEvento
            End Get
            Set(ByVal value As DATO_DWORDDINT)
                _NumOTEvento = value
            End Set
        End Property
        ''' <summary>
        ''' Mensaje para describir el evento
        ''' </summary>
        Private _IdEvento As DATO_WORDINT
        Public Property IdEvento() As DATO_WORDINT
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As DATO_WORDINT)
                _IdEvento = value
            End Set
        End Property

        ''' <summary>
        ''' Valor de la variable relacionada al evento. Si es 3= Inicio de OT, este valor corresponde al numero de BALANZA (1 o 2).Si es Fin de Batch(6), este valor corresponde al Peso del Batch
        ''' </summary>
        Private _ValorEvento1 As DATO_FLOAT
        Public Property ValorEvento1() As DATO_FLOAT
            Get
                Return _ValorEvento1
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorEvento1 = value
            End Set
        End Property
        ''' <summary>
        ''' Valor de la variable relacionada al evento. 
        ''' </summary>
        Private _ValorEvento2 As DATO_FLOAT
        Public Property ValorEvento2() As DATO_FLOAT
            Get
                Return _ValorEvento2
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorEvento2 = value
            End Set
        End Property
        ''' <summary>
        ''' Valor de la variable relacionada al evento. 
        ''' </summary>
        Private _ValorEvento3 As DATO_FLOAT
        Public Property ValorEvento3() As DATO_FLOAT
            Get
                Return _ValorEvento3
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorEvento3 = value
            End Set
        End Property
        ''' <summary>
        ''' Valor de la variable relacionada al evento. 
        ''' </summary>
        Private _ValorEvento4 As DATO_FLOAT
        Public Property ValorEvento4() As DATO_FLOAT
            Get
                Return _ValorEvento4
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorEvento4 = value
            End Set
        End Property
        ''' <summary>
        ''' Valor de la variable relacionada al evento. 
        ''' </summary>
        Private _ValorEvento5 As DATO_FLOAT
        Public Property ValorEvento5() As DATO_FLOAT
            Get
                Return _ValorEvento5
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorEvento5 = value
            End Set
        End Property
        ''' <summary>
        ''' Número de OT
        ''' </summary>
        Private _NumOTEventoToElectroluz As DATO_DWORDDINT
        Public Property NumOTEventoToElectroLuz() As DATO_DWORDDINT
            Get
                Return _NumOTEventoToElectroluz
            End Get
            Set(ByVal value As DATO_DWORDDINT)
                _NumOTEventoToElectroluz = value
            End Set
        End Property
        ''' <summary>
        ''' 1= Existencia máxima en Silo, 2= Riesgo de Mezcla….….. TRIGGER(debe ser monitoreado por Electroluz y una vez procesado se debe poner en 0 y una vez procesado se debe poner en 0
        ''' </summary>
        Private _IdEventoToElectroLuz As DATO_WORDINT
        Public Property IdEventoToElectroLuz() As DATO_WORDINT
            Get
                Return _IdEventoToElectroLuz
            End Get
            Set(ByVal value As DATO_WORDINT)
                _IdEventoToElectroLuz = value
            End Set
        End Property
        ''' <summary>
        ''' Mensaje para describir el evento
        ''' </summary>
        Private _MensajeEventoToElectroLuz As DATO_BYTE
        Public Property MensajeEventoToElectroLuz() As DATO_BYTE
            Get
                Return _MensajeEventoToElectroLuz
            End Get
            Set(ByVal value As DATO_BYTE)
                _MensajeEventoToElectroLuz = value
            End Set
        End Property
        ''' <summary>
        ''' Valor de la variable relacionada al evento. Para IdEvento=1 se debe indicar el % de llenado del Silo.  Para IdEvento=2 se debe indicar el IdOrigen donde se puede producir mezcla.
        ''' </summary>
        Private _ValorEventoToElectroLuz As DATO_FLOAT
        Public Property ValorEventoToElectroLuz() As DATO_FLOAT
            Get
                Return _ValorEventoToElectroLuz
            End Get
            Set(ByVal value As DATO_FLOAT)
                _ValorEventoToElectroLuz = value
            End Set
        End Property

        Private _MensajeEvento As DATO_BYTE
        Public Property MensajeEvento() As DATO_BYTE
            Get
                Return _MensajeEvento
            End Get
            Set(ByVal value As DATO_BYTE)
                _MensajeEvento = value
            End Set
        End Property


    End Class

#End Region


End Class
