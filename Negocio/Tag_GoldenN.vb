﻿Public Class Tag_GoldenN

    Private oBD As New AccesoDatos.Tag_GoldenAD

    Public Sub Guardar(TagTid As String)
        Dim oTagGolden As New Entidades.Tag_golden
        oTagGolden.fecha = DateTime.Now
        oTagGolden.tag_tid = TagTid
        oBD.add(oTagGolden)
    End Sub

    Public Function GetOne(TagTid As String) As Entidades.Tag_golden
        Return oBD.getOne(TagTid)
    End Function


End Class
