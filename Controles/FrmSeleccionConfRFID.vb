﻿Public Class FrmSeleccionConfRFID

    Public _ConfSeleccionada As Entidades.Config_Lector_RFID




    Public Sub New(ByVal listConf As List(Of Entidades.Config_Lector_RFID))

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        cbEntidad.DataSource = listConf
        cbEntidad.DisplayMember = "nombre"

    End Sub

    Private Sub btnIniciarLectura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIniciarLectura.Click
        If cbEntidad.SelectedIndex > -1 Then

            _ConfSeleccionada = CType(cbEntidad.SelectedItem, Entidades.Config_Lector_RFID)
            Me.DialogResult = DialogResult.OK

        End If

    End Sub

End Class