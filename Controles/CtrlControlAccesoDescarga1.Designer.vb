﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlControlAccesoDescarga1
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.btnLecturaManualDes1 = New System.Windows.Forms.Button()
        Me.btnLecturaManualDes2 = New System.Windows.Forms.Button()
        Me.btnLecturaManualDes3 = New System.Windows.Forms.Button()
        Me.CAM_DES3 = New Controles.CtrlCamara()
        Me.CAM_DES2 = New Controles.CtrlCamara()
        Me.CAM_DES1 = New Controles.CtrlCamara()
        Me.CartelLED_DES3 = New Controles.CtrlCartelMultiLED()
        Me.CartelLED_DES2 = New Controles.CtrlCartelMultiLED()
        Me.CartelLED_DES1 = New Controles.CtrlCartelMultiLED()
        Me.DES3_BAR1 = New Controles.CtrlBarrera()
        Me.DES3_BAR2 = New Controles.CtrlBarrera()
        Me.DES2_BAR1 = New Controles.CtrlBarrera()
        Me.DES2_BAR2 = New Controles.CtrlBarrera()
        Me.DES1_BAR1 = New Controles.CtrlBarrera()
        Me.DES1_BAR2 = New Controles.CtrlBarrera()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlDinamismoDescarga3 = New Controles.CtrlDinamismoDescarga()
        Me.CtrlDinamismoDescarga2 = New Controles.CtrlDinamismoDescarga()
        Me.CtrlDinamismoDescarga1 = New Controles.CtrlDinamismoDescarga()
        Me.S_DES1_BAR2 = New Controles.CtrlSensor()
        Me.S_DES1_POS = New Controles.CtrlSensor()
        Me.S_DES1_BAR1 = New Controles.CtrlSensor()
        Me.S_LECTURA_DES1 = New Controles.CtrlSensor()
        Me.S_DES2_BAR2 = New Controles.CtrlSensor()
        Me.S_DES2_POS = New Controles.CtrlSensor()
        Me.S_DES2_BAR1 = New Controles.CtrlSensor()
        Me.S_DES3_BAR2 = New Controles.CtrlSensor()
        Me.S_LECTURA_DES2 = New Controles.CtrlSensor()
        Me.S_DES3_POS = New Controles.CtrlSensor()
        Me.S_DES3_BAR1 = New Controles.CtrlSensor()
        Me.S_LECTURA_DES3 = New Controles.CtrlSensor()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.S_DES1_PLAT = New Controles.CtrlSensor()
        Me.S_DES2_PLAT = New Controles.CtrlSensor()
        Me.S_DES3_PLAT = New Controles.CtrlSensor()
        Me.CtrlAntenaLeyendoDes1 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoDes2 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoDes3 = New Controles.CtrlAntenaLeyendo()
        Me.lblSeparador = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.DESCARGA1 = New Controles.CtrlCabezalBalanza()
        Me.CtrlInfomacionDescarga1 = New Controles.ctrlInfomacionBalanza()
        Me.CtrlInfomacionDescarga2 = New Controles.ctrlInfomacionBalanza()
        Me.DESCARGA2 = New Controles.CtrlCabezalBalanza()
        Me.DESCARGA3 = New Controles.CtrlCabezalBalanza()
        Me.CtrlInfomacionDescarga3 = New Controles.ctrlInfomacionBalanza()
        Me.CtrlTiempoEsperaDesc1 = New Controles.CtrlTiempoEspera()
        Me.CtrlTiempoEsperaDesc2 = New Controles.CtrlTiempoEspera()
        Me.CtrlTiempoEsperaDesc3 = New Controles.CtrlTiempoEspera()
        Me.CtrlTiempoEspera1 = New Controles.CtrlTiempoEspera()
        Me.CtrlTiempoEspera2 = New Controles.CtrlTiempoEspera()
        Me.SuspendLayout()
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(150, -5)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(1264, 50)
        Me.lblNombre.TabIndex = 159
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(1316, -2)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(49, 44)
        Me.btnAyuda.TabIndex = 158
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(1364, -1)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 157
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, 0)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 156
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'btnLecturaManualDes1
        '
        Me.btnLecturaManualDes1.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaManualDes1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualDes1.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualDes1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualDes1.Location = New System.Drawing.Point(31, 183)
        Me.btnLecturaManualDes1.Name = "btnLecturaManualDes1"
        Me.btnLecturaManualDes1.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualDes1.TabIndex = 194
        Me.btnLecturaManualDes1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualDes1.UseVisualStyleBackColor = False
        '
        'btnLecturaManualDes2
        '
        Me.btnLecturaManualDes2.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaManualDes2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualDes2.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualDes2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualDes2.Location = New System.Drawing.Point(410, 183)
        Me.btnLecturaManualDes2.Name = "btnLecturaManualDes2"
        Me.btnLecturaManualDes2.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualDes2.TabIndex = 197
        Me.btnLecturaManualDes2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualDes2.UseVisualStyleBackColor = False
        '
        'btnLecturaManualDes3
        '
        Me.btnLecturaManualDes3.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaManualDes3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualDes3.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualDes3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualDes3.Location = New System.Drawing.Point(833, 181)
        Me.btnLecturaManualDes3.Name = "btnLecturaManualDes3"
        Me.btnLecturaManualDes3.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualDes3.TabIndex = 199
        Me.btnLecturaManualDes3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualDes3.UseVisualStyleBackColor = False
        '
        'CAM_DES3
        '
        Me.CAM_DES3.AutoSize = True
        Me.CAM_DES3.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_DES3.frmCamara = Nothing
        Me.CAM_DES3.GPIO = False
        Me.CAM_DES3.ID_CONTROL_ACCESO = 0
        Me.CAM_DES3.ID_PLC = 0
        Me.CAM_DES3.ID_SECTOR = 0
        Me.CAM_DES3.Inicializado = False
        Me.CAM_DES3.Location = New System.Drawing.Point(1088, 192)
        Me.CAM_DES3.Name = "CAM_DES3"
        Me.CAM_DES3.PLC = Nothing
        Me.CAM_DES3.Size = New System.Drawing.Size(65, 60)
        Me.CAM_DES3.TabIndex = 207
        '
        'CAM_DES2
        '
        Me.CAM_DES2.AutoSize = True
        Me.CAM_DES2.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_DES2.frmCamara = Nothing
        Me.CAM_DES2.GPIO = False
        Me.CAM_DES2.ID_CONTROL_ACCESO = 0
        Me.CAM_DES2.ID_PLC = 0
        Me.CAM_DES2.ID_SECTOR = 0
        Me.CAM_DES2.Inicializado = False
        Me.CAM_DES2.Location = New System.Drawing.Point(680, 204)
        Me.CAM_DES2.Name = "CAM_DES2"
        Me.CAM_DES2.PLC = Nothing
        Me.CAM_DES2.Size = New System.Drawing.Size(67, 60)
        Me.CAM_DES2.TabIndex = 206
        '
        'CAM_DES1
        '
        Me.CAM_DES1.AutoSize = True
        Me.CAM_DES1.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_DES1.frmCamara = Nothing
        Me.CAM_DES1.GPIO = False
        Me.CAM_DES1.ID_CONTROL_ACCESO = 0
        Me.CAM_DES1.ID_PLC = 0
        Me.CAM_DES1.ID_SECTOR = 0
        Me.CAM_DES1.Inicializado = False
        Me.CAM_DES1.Location = New System.Drawing.Point(307, 209)
        Me.CAM_DES1.Name = "CAM_DES1"
        Me.CAM_DES1.PLC = Nothing
        Me.CAM_DES1.Size = New System.Drawing.Size(67, 60)
        Me.CAM_DES1.TabIndex = 205
        '
        'CartelLED_DES3
        '
        Me.CartelLED_DES3.Conectado = False
        Me.CartelLED_DES3.GPIO = False
        Me.CartelLED_DES3.ID_CONTROL_ACCESO = 0
        Me.CartelLED_DES3.ID_PLC = 0
        Me.CartelLED_DES3.ID_SECTOR = 0
        Me.CartelLED_DES3.Inicializado = False
        Me.CartelLED_DES3.Location = New System.Drawing.Point(1017, 286)
        Me.CartelLED_DES3.Name = "CartelLED_DES3"
        Me.CartelLED_DES3.PLC = Nothing
        Me.CartelLED_DES3.Size = New System.Drawing.Size(136, 116)
        Me.CartelLED_DES3.TabIndex = 204
        '
        'CartelLED_DES2
        '
        Me.CartelLED_DES2.Conectado = False
        Me.CartelLED_DES2.GPIO = False
        Me.CartelLED_DES2.ID_CONTROL_ACCESO = 0
        Me.CartelLED_DES2.ID_PLC = 0
        Me.CartelLED_DES2.ID_SECTOR = 0
        Me.CartelLED_DES2.Inicializado = False
        Me.CartelLED_DES2.Location = New System.Drawing.Point(611, 283)
        Me.CartelLED_DES2.Name = "CartelLED_DES2"
        Me.CartelLED_DES2.PLC = Nothing
        Me.CartelLED_DES2.Size = New System.Drawing.Size(136, 116)
        Me.CartelLED_DES2.TabIndex = 201
        '
        'CartelLED_DES1
        '
        Me.CartelLED_DES1.Conectado = False
        Me.CartelLED_DES1.GPIO = False
        Me.CartelLED_DES1.ID_CONTROL_ACCESO = 0
        Me.CartelLED_DES1.ID_PLC = 0
        Me.CartelLED_DES1.ID_SECTOR = 0
        Me.CartelLED_DES1.Inicializado = False
        Me.CartelLED_DES1.Location = New System.Drawing.Point(238, 288)
        Me.CartelLED_DES1.Name = "CartelLED_DES1"
        Me.CartelLED_DES1.PLC = Nothing
        Me.CartelLED_DES1.Size = New System.Drawing.Size(136, 111)
        Me.CartelLED_DES1.TabIndex = 200
        '
        'DES3_BAR1
        '
        Me.DES3_BAR1.BackColor = System.Drawing.Color.Transparent
        Me.DES3_BAR1.Estado = False
        Me.DES3_BAR1.GPIO = False
        Me.DES3_BAR1.ID_CONTROL_ACCESO = 0
        Me.DES3_BAR1.ID_PLC = 0
        Me.DES3_BAR1.ID_SECTOR = 0
        Me.DES3_BAR1.Inicializado = False
        Me.DES3_BAR1.Location = New System.Drawing.Point(818, 221)
        Me.DES3_BAR1.Name = "DES3_BAR1"
        Me.DES3_BAR1.PLC = Nothing
        Me.DES3_BAR1.Size = New System.Drawing.Size(64, 50)
        Me.DES3_BAR1.TabIndex = 193
        '
        'DES3_BAR2
        '
        Me.DES3_BAR2.BackColor = System.Drawing.Color.Transparent
        Me.DES3_BAR2.Estado = False
        Me.DES3_BAR2.GPIO = False
        Me.DES3_BAR2.ID_CONTROL_ACCESO = 0
        Me.DES3_BAR2.ID_PLC = 0
        Me.DES3_BAR2.ID_SECTOR = 0
        Me.DES3_BAR2.Inicializado = False
        Me.DES3_BAR2.Location = New System.Drawing.Point(810, 46)
        Me.DES3_BAR2.Name = "DES3_BAR2"
        Me.DES3_BAR2.PLC = Nothing
        Me.DES3_BAR2.Size = New System.Drawing.Size(64, 50)
        Me.DES3_BAR2.TabIndex = 192
        '
        'DES2_BAR1
        '
        Me.DES2_BAR1.BackColor = System.Drawing.Color.Transparent
        Me.DES2_BAR1.Estado = False
        Me.DES2_BAR1.GPIO = False
        Me.DES2_BAR1.ID_CONTROL_ACCESO = 0
        Me.DES2_BAR1.ID_PLC = 0
        Me.DES2_BAR1.ID_SECTOR = 0
        Me.DES2_BAR1.Inicializado = False
        Me.DES2_BAR1.Location = New System.Drawing.Point(402, 227)
        Me.DES2_BAR1.Name = "DES2_BAR1"
        Me.DES2_BAR1.PLC = Nothing
        Me.DES2_BAR1.Size = New System.Drawing.Size(64, 50)
        Me.DES2_BAR1.TabIndex = 189
        '
        'DES2_BAR2
        '
        Me.DES2_BAR2.BackColor = System.Drawing.Color.Transparent
        Me.DES2_BAR2.Estado = False
        Me.DES2_BAR2.GPIO = False
        Me.DES2_BAR2.ID_CONTROL_ACCESO = 0
        Me.DES2_BAR2.ID_PLC = 0
        Me.DES2_BAR2.ID_SECTOR = 0
        Me.DES2_BAR2.Inicializado = False
        Me.DES2_BAR2.Location = New System.Drawing.Point(403, 47)
        Me.DES2_BAR2.Name = "DES2_BAR2"
        Me.DES2_BAR2.PLC = Nothing
        Me.DES2_BAR2.Size = New System.Drawing.Size(64, 50)
        Me.DES2_BAR2.TabIndex = 188
        '
        'DES1_BAR1
        '
        Me.DES1_BAR1.BackColor = System.Drawing.Color.Transparent
        Me.DES1_BAR1.Estado = False
        Me.DES1_BAR1.GPIO = False
        Me.DES1_BAR1.ID_CONTROL_ACCESO = 0
        Me.DES1_BAR1.ID_PLC = 0
        Me.DES1_BAR1.ID_SECTOR = 0
        Me.DES1_BAR1.Inicializado = False
        Me.DES1_BAR1.Location = New System.Drawing.Point(24, 227)
        Me.DES1_BAR1.Name = "DES1_BAR1"
        Me.DES1_BAR1.PLC = Nothing
        Me.DES1_BAR1.Size = New System.Drawing.Size(64, 50)
        Me.DES1_BAR1.TabIndex = 185
        '
        'DES1_BAR2
        '
        Me.DES1_BAR2.BackColor = System.Drawing.Color.Transparent
        Me.DES1_BAR2.Estado = False
        Me.DES1_BAR2.GPIO = False
        Me.DES1_BAR2.ID_CONTROL_ACCESO = 0
        Me.DES1_BAR2.ID_PLC = 0
        Me.DES1_BAR2.ID_SECTOR = 0
        Me.DES1_BAR2.Inicializado = False
        Me.DES1_BAR2.Location = New System.Drawing.Point(24, 45)
        Me.DES1_BAR2.Name = "DES1_BAR2"
        Me.DES1_BAR2.PLC = Nothing
        Me.DES1_BAR2.Size = New System.Drawing.Size(64, 50)
        Me.DES1_BAR2.TabIndex = 184
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 505)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(1161, 66)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 161
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(1160, 42)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(254, 529)
        Me.CtrlPanelNotificaciones1.TabIndex = 160
        '
        'CtrlDinamismoDescarga3
        '
        Me.CtrlDinamismoDescarga3.Location = New System.Drawing.Point(785, 43)
        Me.CtrlDinamismoDescarga3.Name = "CtrlDinamismoDescarga3"
        Me.CtrlDinamismoDescarga3.Size = New System.Drawing.Size(379, 392)
        Me.CtrlDinamismoDescarga3.TabIndex = 2
        '
        'CtrlDinamismoDescarga2
        '
        Me.CtrlDinamismoDescarga2.Location = New System.Drawing.Point(394, 44)
        Me.CtrlDinamismoDescarga2.Name = "CtrlDinamismoDescarga2"
        Me.CtrlDinamismoDescarga2.Size = New System.Drawing.Size(375, 392)
        Me.CtrlDinamismoDescarga2.TabIndex = 1
        '
        'CtrlDinamismoDescarga1
        '
        Me.CtrlDinamismoDescarga1.Location = New System.Drawing.Point(0, 43)
        Me.CtrlDinamismoDescarga1.Name = "CtrlDinamismoDescarga1"
        Me.CtrlDinamismoDescarga1.Size = New System.Drawing.Size(374, 392)
        Me.CtrlDinamismoDescarga1.TabIndex = 0
        '
        'S_DES1_BAR2
        '
        Me.S_DES1_BAR2.Estado = False
        Me.S_DES1_BAR2.GPIO = False
        Me.S_DES1_BAR2.ID_CONTROL_ACCESO = 0
        Me.S_DES1_BAR2.ID_PLC = 0
        Me.S_DES1_BAR2.ID_SECTOR = 0
        Me.S_DES1_BAR2.Inicializado = False
        Me.S_DES1_BAR2.Location = New System.Drawing.Point(131, 44)
        Me.S_DES1_BAR2.Name = "S_DES1_BAR2"
        Me.S_DES1_BAR2.PLC = Nothing
        Me.S_DES1_BAR2.Size = New System.Drawing.Size(76, 45)
        Me.S_DES1_BAR2.TabIndex = 208
        '
        'S_DES1_POS
        '
        Me.S_DES1_POS.Estado = False
        Me.S_DES1_POS.GPIO = False
        Me.S_DES1_POS.ID_CONTROL_ACCESO = 0
        Me.S_DES1_POS.ID_PLC = 0
        Me.S_DES1_POS.ID_SECTOR = 0
        Me.S_DES1_POS.Inicializado = False
        Me.S_DES1_POS.Location = New System.Drawing.Point(203, 102)
        Me.S_DES1_POS.Name = "S_DES1_POS"
        Me.S_DES1_POS.PLC = Nothing
        Me.S_DES1_POS.Size = New System.Drawing.Size(76, 45)
        Me.S_DES1_POS.TabIndex = 209
        '
        'S_DES1_BAR1
        '
        Me.S_DES1_BAR1.Estado = False
        Me.S_DES1_BAR1.GPIO = False
        Me.S_DES1_BAR1.ID_CONTROL_ACCESO = 0
        Me.S_DES1_BAR1.ID_PLC = 0
        Me.S_DES1_BAR1.ID_SECTOR = 0
        Me.S_DES1_BAR1.Inicializado = False
        Me.S_DES1_BAR1.Location = New System.Drawing.Point(206, 153)
        Me.S_DES1_BAR1.Name = "S_DES1_BAR1"
        Me.S_DES1_BAR1.PLC = Nothing
        Me.S_DES1_BAR1.Size = New System.Drawing.Size(76, 45)
        Me.S_DES1_BAR1.TabIndex = 210
        '
        'S_LECTURA_DES1
        '
        Me.S_LECTURA_DES1.Estado = False
        Me.S_LECTURA_DES1.GPIO = False
        Me.S_LECTURA_DES1.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_DES1.ID_PLC = 0
        Me.S_LECTURA_DES1.ID_SECTOR = 0
        Me.S_LECTURA_DES1.Inicializado = False
        Me.S_LECTURA_DES1.Location = New System.Drawing.Point(43, 377)
        Me.S_LECTURA_DES1.Name = "S_LECTURA_DES1"
        Me.S_LECTURA_DES1.PLC = Nothing
        Me.S_LECTURA_DES1.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_DES1.TabIndex = 211
        '
        'S_DES2_BAR2
        '
        Me.S_DES2_BAR2.Estado = False
        Me.S_DES2_BAR2.GPIO = False
        Me.S_DES2_BAR2.ID_CONTROL_ACCESO = 0
        Me.S_DES2_BAR2.ID_PLC = 0
        Me.S_DES2_BAR2.ID_SECTOR = 0
        Me.S_DES2_BAR2.Inicializado = False
        Me.S_DES2_BAR2.Location = New System.Drawing.Point(504, 47)
        Me.S_DES2_BAR2.Name = "S_DES2_BAR2"
        Me.S_DES2_BAR2.PLC = Nothing
        Me.S_DES2_BAR2.Size = New System.Drawing.Size(76, 45)
        Me.S_DES2_BAR2.TabIndex = 212
        '
        'S_DES2_POS
        '
        Me.S_DES2_POS.Estado = False
        Me.S_DES2_POS.GPIO = False
        Me.S_DES2_POS.ID_CONTROL_ACCESO = 0
        Me.S_DES2_POS.ID_PLC = 0
        Me.S_DES2_POS.ID_SECTOR = 0
        Me.S_DES2_POS.Inicializado = False
        Me.S_DES2_POS.Location = New System.Drawing.Point(583, 102)
        Me.S_DES2_POS.Name = "S_DES2_POS"
        Me.S_DES2_POS.PLC = Nothing
        Me.S_DES2_POS.Size = New System.Drawing.Size(76, 45)
        Me.S_DES2_POS.TabIndex = 213
        '
        'S_DES2_BAR1
        '
        Me.S_DES2_BAR1.Estado = False
        Me.S_DES2_BAR1.GPIO = False
        Me.S_DES2_BAR1.ID_CONTROL_ACCESO = 0
        Me.S_DES2_BAR1.ID_PLC = 0
        Me.S_DES2_BAR1.ID_SECTOR = 0
        Me.S_DES2_BAR1.Inicializado = False
        Me.S_DES2_BAR1.Location = New System.Drawing.Point(583, 153)
        Me.S_DES2_BAR1.Name = "S_DES2_BAR1"
        Me.S_DES2_BAR1.PLC = Nothing
        Me.S_DES2_BAR1.Size = New System.Drawing.Size(76, 45)
        Me.S_DES2_BAR1.TabIndex = 214
        '
        'S_DES3_BAR2
        '
        Me.S_DES3_BAR2.Estado = False
        Me.S_DES3_BAR2.GPIO = False
        Me.S_DES3_BAR2.ID_CONTROL_ACCESO = 0
        Me.S_DES3_BAR2.ID_PLC = 0
        Me.S_DES3_BAR2.ID_SECTOR = 0
        Me.S_DES3_BAR2.Inicializado = False
        Me.S_DES3_BAR2.Location = New System.Drawing.Point(928, 45)
        Me.S_DES3_BAR2.Name = "S_DES3_BAR2"
        Me.S_DES3_BAR2.PLC = Nothing
        Me.S_DES3_BAR2.Size = New System.Drawing.Size(76, 45)
        Me.S_DES3_BAR2.TabIndex = 215
        '
        'S_LECTURA_DES2
        '
        Me.S_LECTURA_DES2.Estado = False
        Me.S_LECTURA_DES2.GPIO = False
        Me.S_LECTURA_DES2.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_DES2.ID_PLC = 0
        Me.S_LECTURA_DES2.ID_SECTOR = 0
        Me.S_LECTURA_DES2.Inicializado = False
        Me.S_LECTURA_DES2.Location = New System.Drawing.Point(422, 377)
        Me.S_LECTURA_DES2.Name = "S_LECTURA_DES2"
        Me.S_LECTURA_DES2.PLC = Nothing
        Me.S_LECTURA_DES2.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_DES2.TabIndex = 216
        '
        'S_DES3_POS
        '
        Me.S_DES3_POS.Estado = False
        Me.S_DES3_POS.GPIO = False
        Me.S_DES3_POS.ID_CONTROL_ACCESO = 0
        Me.S_DES3_POS.ID_PLC = 0
        Me.S_DES3_POS.ID_SECTOR = 0
        Me.S_DES3_POS.Inicializado = False
        Me.S_DES3_POS.Location = New System.Drawing.Point(994, 100)
        Me.S_DES3_POS.Name = "S_DES3_POS"
        Me.S_DES3_POS.PLC = Nothing
        Me.S_DES3_POS.Size = New System.Drawing.Size(76, 45)
        Me.S_DES3_POS.TabIndex = 217
        '
        'S_DES3_BAR1
        '
        Me.S_DES3_BAR1.Estado = False
        Me.S_DES3_BAR1.GPIO = False
        Me.S_DES3_BAR1.ID_CONTROL_ACCESO = 0
        Me.S_DES3_BAR1.ID_PLC = 0
        Me.S_DES3_BAR1.ID_SECTOR = 0
        Me.S_DES3_BAR1.Inicializado = False
        Me.S_DES3_BAR1.Location = New System.Drawing.Point(994, 151)
        Me.S_DES3_BAR1.Name = "S_DES3_BAR1"
        Me.S_DES3_BAR1.PLC = Nothing
        Me.S_DES3_BAR1.Size = New System.Drawing.Size(76, 45)
        Me.S_DES3_BAR1.TabIndex = 218
        '
        'S_LECTURA_DES3
        '
        Me.S_LECTURA_DES3.Estado = False
        Me.S_LECTURA_DES3.GPIO = False
        Me.S_LECTURA_DES3.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_DES3.ID_PLC = 0
        Me.S_LECTURA_DES3.ID_SECTOR = 0
        Me.S_LECTURA_DES3.Inicializado = False
        Me.S_LECTURA_DES3.Location = New System.Drawing.Point(818, 375)
        Me.S_LECTURA_DES3.Name = "S_LECTURA_DES3"
        Me.S_LECTURA_DES3.PLC = Nothing
        Me.S_LECTURA_DES3.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_DES3.TabIndex = 219
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(246, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(123, 36)
        Me.Label3.TabIndex = 222
        Me.Label3.Text = "Descarga 1"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(624, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 36)
        Me.Label4.TabIndex = 223
        Me.Label4.Text = "Descarga 2"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(1030, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(123, 36)
        Me.Label5.TabIndex = 224
        Me.Label5.Text = "Descarga 3"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'S_DES1_PLAT
        '
        Me.S_DES1_PLAT.Estado = False
        Me.S_DES1_PLAT.GPIO = False
        Me.S_DES1_PLAT.ID_CONTROL_ACCESO = 0
        Me.S_DES1_PLAT.ID_PLC = 0
        Me.S_DES1_PLAT.ID_SECTOR = 0
        Me.S_DES1_PLAT.Inicializado = False
        Me.S_DES1_PLAT.Location = New System.Drawing.Point(218, 209)
        Me.S_DES1_PLAT.Name = "S_DES1_PLAT"
        Me.S_DES1_PLAT.PLC = Nothing
        Me.S_DES1_PLAT.Size = New System.Drawing.Size(64, 45)
        Me.S_DES1_PLAT.TabIndex = 225
        '
        'S_DES2_PLAT
        '
        Me.S_DES2_PLAT.Estado = False
        Me.S_DES2_PLAT.GPIO = False
        Me.S_DES2_PLAT.ID_CONTROL_ACCESO = 0
        Me.S_DES2_PLAT.ID_PLC = 0
        Me.S_DES2_PLAT.ID_SECTOR = 0
        Me.S_DES2_PLAT.Inicializado = False
        Me.S_DES2_PLAT.Location = New System.Drawing.Point(596, 209)
        Me.S_DES2_PLAT.Name = "S_DES2_PLAT"
        Me.S_DES2_PLAT.PLC = Nothing
        Me.S_DES2_PLAT.Size = New System.Drawing.Size(63, 45)
        Me.S_DES2_PLAT.TabIndex = 226
        '
        'S_DES3_PLAT
        '
        Me.S_DES3_PLAT.Estado = False
        Me.S_DES3_PLAT.GPIO = False
        Me.S_DES3_PLAT.ID_CONTROL_ACCESO = 0
        Me.S_DES3_PLAT.ID_PLC = 0
        Me.S_DES3_PLAT.ID_SECTOR = 0
        Me.S_DES3_PLAT.Inicializado = False
        Me.S_DES3_PLAT.Location = New System.Drawing.Point(1019, 207)
        Me.S_DES3_PLAT.Name = "S_DES3_PLAT"
        Me.S_DES3_PLAT.PLC = Nothing
        Me.S_DES3_PLAT.Size = New System.Drawing.Size(63, 45)
        Me.S_DES3_PLAT.TabIndex = 227
        '
        'CtrlAntenaLeyendoDes1
        '
        Me.CtrlAntenaLeyendoDes1.Location = New System.Drawing.Point(288, 102)
        Me.CtrlAntenaLeyendoDes1.Name = "CtrlAntenaLeyendoDes1"
        Me.CtrlAntenaLeyendoDes1.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAntenaLeyendoDes1.TabIndex = 228
        Me.CtrlAntenaLeyendoDes1.UltTag = Nothing
        '
        'CtrlAntenaLeyendoDes2
        '
        Me.CtrlAntenaLeyendoDes2.Location = New System.Drawing.Point(665, 102)
        Me.CtrlAntenaLeyendoDes2.Name = "CtrlAntenaLeyendoDes2"
        Me.CtrlAntenaLeyendoDes2.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAntenaLeyendoDes2.TabIndex = 229
        Me.CtrlAntenaLeyendoDes2.UltTag = Nothing
        '
        'CtrlAntenaLeyendoDes3
        '
        Me.CtrlAntenaLeyendoDes3.Location = New System.Drawing.Point(1073, 99)
        Me.CtrlAntenaLeyendoDes3.Name = "CtrlAntenaLeyendoDes3"
        Me.CtrlAntenaLeyendoDes3.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAntenaLeyendoDes3.TabIndex = 230
        Me.CtrlAntenaLeyendoDes3.UltTag = Nothing
        '
        'lblSeparador
        '
        Me.lblSeparador.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblSeparador.Location = New System.Drawing.Point(769, 44)
        Me.lblSeparador.Name = "lblSeparador"
        Me.lblSeparador.Size = New System.Drawing.Size(19, 461)
        Me.lblSeparador.TabIndex = 231
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(375, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 461)
        Me.Label1.TabIndex = 232
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(414, 314)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 233
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'DESCARGA1
        '
        Me.DESCARGA1.BackColor = System.Drawing.Color.Gainsboro
        Me.DESCARGA1.GPIO = False
        Me.DESCARGA1.ID_CONTROL_ACCESO = 0
        Me.DESCARGA1.ID_PLC = 0
        Me.DESCARGA1.ID_SECTOR = 0
        Me.DESCARGA1.Inicializado = False
        Me.DESCARGA1.Location = New System.Drawing.Point(0, 428)
        Me.DESCARGA1.Name = "DESCARGA1"
        Me.DESCARGA1.numeroBalanza = 0
        Me.DESCARGA1.PLC = Nothing
        Me.DESCARGA1.Size = New System.Drawing.Size(374, 77)
        Me.DESCARGA1.TabIndex = 234
        '
        'CtrlInfomacionDescarga1
        '
        Me.CtrlInfomacionDescarga1.BackColor = System.Drawing.Color.White
        Me.CtrlInfomacionDescarga1.Location = New System.Drawing.Point(245, 441)
        Me.CtrlInfomacionDescarga1.Name = "CtrlInfomacionDescarga1"
        Me.CtrlInfomacionDescarga1.Size = New System.Drawing.Size(129, 36)
        Me.CtrlInfomacionDescarga1.TabIndex = 235
        '
        'CtrlInfomacionDescarga2
        '
        Me.CtrlInfomacionDescarga2.BackColor = System.Drawing.Color.White
        Me.CtrlInfomacionDescarga2.Location = New System.Drawing.Point(638, 441)
        Me.CtrlInfomacionDescarga2.Name = "CtrlInfomacionDescarga2"
        Me.CtrlInfomacionDescarga2.Size = New System.Drawing.Size(129, 36)
        Me.CtrlInfomacionDescarga2.TabIndex = 237
        '
        'DESCARGA2
        '
        Me.DESCARGA2.BackColor = System.Drawing.Color.Gainsboro
        Me.DESCARGA2.GPIO = False
        Me.DESCARGA2.ID_CONTROL_ACCESO = 0
        Me.DESCARGA2.ID_PLC = 0
        Me.DESCARGA2.ID_SECTOR = 0
        Me.DESCARGA2.Inicializado = False
        Me.DESCARGA2.Location = New System.Drawing.Point(393, 428)
        Me.DESCARGA2.Name = "DESCARGA2"
        Me.DESCARGA2.numeroBalanza = 0
        Me.DESCARGA2.PLC = Nothing
        Me.DESCARGA2.Size = New System.Drawing.Size(374, 77)
        Me.DESCARGA2.TabIndex = 236
        '
        'DESCARGA3
        '
        Me.DESCARGA3.BackColor = System.Drawing.Color.Gainsboro
        Me.DESCARGA3.GPIO = False
        Me.DESCARGA3.ID_CONTROL_ACCESO = 0
        Me.DESCARGA3.ID_PLC = 0
        Me.DESCARGA3.ID_SECTOR = 0
        Me.DESCARGA3.Inicializado = False
        Me.DESCARGA3.Location = New System.Drawing.Point(787, 428)
        Me.DESCARGA3.Name = "DESCARGA3"
        Me.DESCARGA3.numeroBalanza = 0
        Me.DESCARGA3.PLC = Nothing
        Me.DESCARGA3.Size = New System.Drawing.Size(374, 77)
        Me.DESCARGA3.TabIndex = 238
        '
        'CtrlInfomacionDescarga3
        '
        Me.CtrlInfomacionDescarga3.BackColor = System.Drawing.Color.White
        Me.CtrlInfomacionDescarga3.Location = New System.Drawing.Point(1032, 441)
        Me.CtrlInfomacionDescarga3.Name = "CtrlInfomacionDescarga3"
        Me.CtrlInfomacionDescarga3.Size = New System.Drawing.Size(129, 36)
        Me.CtrlInfomacionDescarga3.TabIndex = 239
        '
        'CtrlTiempoEsperaDesc1
        '
        Me.CtrlTiempoEsperaDesc1.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEsperaDesc1.Location = New System.Drawing.Point(131, 357)
        Me.CtrlTiempoEsperaDesc1.Name = "CtrlTiempoEsperaDesc1"
        Me.CtrlTiempoEsperaDesc1.SetVisible = False
        Me.CtrlTiempoEsperaDesc1.Size = New System.Drawing.Size(100, 63)
        Me.CtrlTiempoEsperaDesc1.TabIndex = 240
        Me.CtrlTiempoEsperaDesc1.Tiempo = Nothing
        '
        'CtrlTiempoEsperaDesc2
        '
        Me.CtrlTiempoEsperaDesc2.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEsperaDesc2.Location = New System.Drawing.Point(504, 367)
        Me.CtrlTiempoEsperaDesc2.Name = "CtrlTiempoEsperaDesc2"
        Me.CtrlTiempoEsperaDesc2.SetVisible = False
        Me.CtrlTiempoEsperaDesc2.Size = New System.Drawing.Size(101, 53)
        Me.CtrlTiempoEsperaDesc2.TabIndex = 241
        Me.CtrlTiempoEsperaDesc2.Tiempo = Nothing
        '
        'CtrlTiempoEsperaDesc3
        '
        Me.CtrlTiempoEsperaDesc3.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEsperaDesc3.Location = New System.Drawing.Point(903, 365)
        Me.CtrlTiempoEsperaDesc3.Name = "CtrlTiempoEsperaDesc3"
        Me.CtrlTiempoEsperaDesc3.SetVisible = False
        Me.CtrlTiempoEsperaDesc3.Size = New System.Drawing.Size(101, 55)
        Me.CtrlTiempoEsperaDesc3.TabIndex = 242
        Me.CtrlTiempoEsperaDesc3.Tiempo = Nothing
        '
        'CtrlTiempoEspera1
        '
        Me.CtrlTiempoEspera1.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEspera1.Location = New System.Drawing.Point(125, 367)
        Me.CtrlTiempoEspera1.Name = "CtrlTiempoEspera1"
        Me.CtrlTiempoEspera1.SetVisible = False
        Me.CtrlTiempoEspera1.Size = New System.Drawing.Size(101, 53)
        Me.CtrlTiempoEspera1.TabIndex = 243
        Me.CtrlTiempoEspera1.Tiempo = Nothing
        '
        'CtrlTiempoEspera2
        '
        Me.CtrlTiempoEspera2.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEspera2.Location = New System.Drawing.Point(504, 356)
        Me.CtrlTiempoEspera2.Name = "CtrlTiempoEspera2"
        Me.CtrlTiempoEspera2.SetVisible = False
        Me.CtrlTiempoEspera2.Size = New System.Drawing.Size(101, 66)
        Me.CtrlTiempoEspera2.TabIndex = 244
        Me.CtrlTiempoEspera2.Tiempo = Nothing
        '
        'CtrlControlAccesoDescarga1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.CtrlTiempoEsperaDesc3)
        Me.Controls.Add(Me.CtrlTiempoEsperaDesc2)
        Me.Controls.Add(Me.CtrlTiempoEspera1)
        Me.Controls.Add(Me.CtrlTiempoEspera2)
        Me.Controls.Add(Me.CtrlTiempoEsperaDesc1)
        Me.Controls.Add(Me.CtrlInfomacionDescarga3)
        Me.Controls.Add(Me.DESCARGA3)
        Me.Controls.Add(Me.CtrlInfomacionDescarga2)
        Me.Controls.Add(Me.DESCARGA2)
        Me.Controls.Add(Me.CtrlInfomacionDescarga1)
        Me.Controls.Add(Me.DESCARGA1)
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblSeparador)
        Me.Controls.Add(Me.CtrlAntenaLeyendoDes3)
        Me.Controls.Add(Me.CtrlAntenaLeyendoDes2)
        Me.Controls.Add(Me.CtrlAntenaLeyendoDes1)
        Me.Controls.Add(Me.S_DES3_PLAT)
        Me.Controls.Add(Me.S_DES2_PLAT)
        Me.Controls.Add(Me.S_DES1_PLAT)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.S_LECTURA_DES3)
        Me.Controls.Add(Me.S_DES3_BAR1)
        Me.Controls.Add(Me.S_DES3_POS)
        Me.Controls.Add(Me.S_LECTURA_DES2)
        Me.Controls.Add(Me.S_DES3_BAR2)
        Me.Controls.Add(Me.S_DES2_BAR1)
        Me.Controls.Add(Me.S_DES2_POS)
        Me.Controls.Add(Me.S_DES2_BAR2)
        Me.Controls.Add(Me.S_LECTURA_DES1)
        Me.Controls.Add(Me.S_DES1_BAR1)
        Me.Controls.Add(Me.S_DES1_POS)
        Me.Controls.Add(Me.S_DES1_BAR2)
        Me.Controls.Add(Me.CAM_DES3)
        Me.Controls.Add(Me.CAM_DES2)
        Me.Controls.Add(Me.CAM_DES1)
        Me.Controls.Add(Me.CartelLED_DES3)
        Me.Controls.Add(Me.CartelLED_DES2)
        Me.Controls.Add(Me.CartelLED_DES1)
        Me.Controls.Add(Me.btnLecturaManualDes3)
        Me.Controls.Add(Me.btnLecturaManualDes2)
        Me.Controls.Add(Me.btnLecturaManualDes1)
        Me.Controls.Add(Me.DES3_BAR1)
        Me.Controls.Add(Me.DES3_BAR2)
        Me.Controls.Add(Me.DES2_BAR1)
        Me.Controls.Add(Me.DES2_BAR2)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.DES1_BAR1)
        Me.Controls.Add(Me.DES1_BAR2)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.CtrlDinamismoDescarga3)
        Me.Controls.Add(Me.CtrlDinamismoDescarga2)
        Me.Controls.Add(Me.CtrlDinamismoDescarga1)
        Me.Name = "CtrlControlAccesoDescarga1"
        Me.Size = New System.Drawing.Size(1414, 571)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlDinamismoDescarga1 As CtrlDinamismoDescarga
    Friend WithEvents CtrlDinamismoDescarga2 As CtrlDinamismoDescarga
    Friend WithEvents CtrlDinamismoDescarga3 As CtrlDinamismoDescarga
    Friend WithEvents lblNombre As Label
    Friend WithEvents btnAyuda As Button
    Friend WithEvents btnReset As Button
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Public WithEvents DES1_BAR2 As CtrlBarrera
    Public WithEvents DES1_BAR1 As CtrlBarrera
    Public WithEvents DES2_BAR2 As CtrlBarrera
    Public WithEvents DES2_BAR1 As CtrlBarrera
    Public WithEvents DES3_BAR2 As CtrlBarrera
    Public WithEvents DES3_BAR1 As CtrlBarrera
    Friend WithEvents btnLecturaManualDes1 As Button
    Friend WithEvents btnLecturaManualDes2 As Button
    Friend WithEvents btnLecturaManualDes3 As Button
    Friend WithEvents CartelLED_DES1 As CtrlCartelMultiLED
    Friend WithEvents CartelLED_DES2 As CtrlCartelMultiLED
    Friend WithEvents CartelLED_DES3 As CtrlCartelMultiLED
    Friend WithEvents CAM_DES1 As CtrlCamara
    Friend WithEvents CAM_DES2 As CtrlCamara
    Friend WithEvents CAM_DES3 As CtrlCamara
    Friend WithEvents S_DES1_BAR2 As CtrlSensor
    Friend WithEvents S_DES1_POS As CtrlSensor
    Friend WithEvents S_DES1_BAR1 As CtrlSensor
    Friend WithEvents S_LECTURA_DES1 As CtrlSensor
    Friend WithEvents S_DES2_BAR2 As CtrlSensor
    Friend WithEvents S_DES2_POS As CtrlSensor
    Friend WithEvents S_DES2_BAR1 As CtrlSensor
    Friend WithEvents S_DES3_BAR2 As CtrlSensor
    Friend WithEvents S_LECTURA_DES2 As CtrlSensor
    Friend WithEvents S_DES3_POS As CtrlSensor
    Friend WithEvents S_DES3_BAR1 As CtrlSensor
    Friend WithEvents S_LECTURA_DES3 As CtrlSensor
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents S_DES1_PLAT As CtrlSensor
    Friend WithEvents S_DES2_PLAT As CtrlSensor
    Friend WithEvents S_DES3_PLAT As CtrlSensor
    Friend WithEvents CtrlAntenaLeyendoDes1 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoDes2 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoDes3 As CtrlAntenaLeyendo
    Friend WithEvents lblSeparador As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnAlarma As Button
    Friend WithEvents DESCARGA1 As CtrlCabezalBalanza
    Friend WithEvents CtrlInfomacionDescarga1 As ctrlInfomacionBalanza
    Friend WithEvents CtrlInfomacionDescarga2 As ctrlInfomacionBalanza
    Friend WithEvents DESCARGA2 As CtrlCabezalBalanza
    Friend WithEvents DESCARGA3 As CtrlCabezalBalanza
    Friend WithEvents CtrlInfomacionDescarga3 As ctrlInfomacionBalanza
    Friend WithEvents CtrlTiempoEsperaDesc1 As CtrlTiempoEspera
    Friend WithEvents CtrlTiempoEsperaDesc2 As CtrlTiempoEspera
    Friend WithEvents CtrlTiempoEsperaDesc3 As CtrlTiempoEspera
    Friend WithEvents CtrlTiempoEspera1 As CtrlTiempoEspera
    Friend WithEvents CtrlTiempoEspera2 As CtrlTiempoEspera
End Class
