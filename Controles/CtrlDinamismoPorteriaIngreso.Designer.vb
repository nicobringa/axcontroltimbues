﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlDinamismoPorteriaIngreso
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picBarreraArriba_Ingreso = New System.Windows.Forms.PictureBox()
        Me.picBarreraAbajo_Ingreso = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblFallaBIngreso = New System.Windows.Forms.Label()
        Me.picSensor_IngresoNoCortando = New System.Windows.Forms.PictureBox()
        Me.picSensor_IngresoCortando = New System.Windows.Forms.PictureBox()
        CType(Me.picBarreraArriba_Ingreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarreraAbajo_Ingreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_IngresoNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_IngresoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picBarreraArriba_Ingreso
        '
        Me.picBarreraArriba_Ingreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Entrada_Despacho_arriba
        Me.picBarreraArriba_Ingreso.Location = New System.Drawing.Point(65, 101)
        Me.picBarreraArriba_Ingreso.Name = "picBarreraArriba_Ingreso"
        Me.picBarreraArriba_Ingreso.Size = New System.Drawing.Size(58, 294)
        Me.picBarreraArriba_Ingreso.TabIndex = 1
        Me.picBarreraArriba_Ingreso.TabStop = False
        '
        'picBarreraAbajo_Ingreso
        '
        Me.picBarreraAbajo_Ingreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Entrada_Despacho_abajo
        Me.picBarreraAbajo_Ingreso.Location = New System.Drawing.Point(65, 313)
        Me.picBarreraAbajo_Ingreso.Name = "picBarreraAbajo_Ingreso"
        Me.picBarreraAbajo_Ingreso.Size = New System.Drawing.Size(170, 83)
        Me.picBarreraAbajo_Ingreso.TabIndex = 2
        Me.picBarreraAbajo_Ingreso.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(103, 265)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(147, 31)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "INGRESO"
        '
        'lblFallaBIngreso
        '
        Me.lblFallaBIngreso.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaBIngreso.AutoSize = True
        Me.lblFallaBIngreso.BackColor = System.Drawing.Color.Red
        Me.lblFallaBIngreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaBIngreso.ForeColor = System.Drawing.Color.White
        Me.lblFallaBIngreso.Location = New System.Drawing.Point(67, 350)
        Me.lblFallaBIngreso.Name = "lblFallaBIngreso"
        Me.lblFallaBIngreso.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaBIngreso.TabIndex = 98
        Me.lblFallaBIngreso.Text = "Falla"
        Me.lblFallaBIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picSensor_IngresoNoCortando
        '
        Me.picSensor_IngresoNoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Entrada_Despacho_nocortando
        Me.picSensor_IngresoNoCortando.Location = New System.Drawing.Point(219, 340)
        Me.picSensor_IngresoNoCortando.Name = "picSensor_IngresoNoCortando"
        Me.picSensor_IngresoNoCortando.Size = New System.Drawing.Size(31, 43)
        Me.picSensor_IngresoNoCortando.TabIndex = 99
        Me.picSensor_IngresoNoCortando.TabStop = False
        '
        'picSensor_IngresoCortando
        '
        Me.picSensor_IngresoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Entrada_Despacho_cortando
        Me.picSensor_IngresoCortando.Location = New System.Drawing.Point(219, 339)
        Me.picSensor_IngresoCortando.Name = "picSensor_IngresoCortando"
        Me.picSensor_IngresoCortando.Size = New System.Drawing.Size(31, 43)
        Me.picSensor_IngresoCortando.TabIndex = 100
        Me.picSensor_IngresoCortando.TabStop = False
        '
        'CtrlDinamismoPorteriaIngreso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.Despacho_Ingreso
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.picSensor_IngresoCortando)
        Me.Controls.Add(Me.picSensor_IngresoNoCortando)
        Me.Controls.Add(Me.lblFallaBIngreso)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.picBarreraAbajo_Ingreso)
        Me.Controls.Add(Me.picBarreraArriba_Ingreso)
        Me.Name = "CtrlDinamismoPorteriaIngreso"
        Me.Size = New System.Drawing.Size(298, 547)
        CType(Me.picBarreraArriba_Ingreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarreraAbajo_Ingreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_IngresoNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_IngresoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picBarreraArriba_Ingreso As PictureBox
    Friend WithEvents picBarreraAbajo_Ingreso As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblFallaBIngreso As Label
    Friend WithEvents picSensor_IngresoNoCortando As PictureBox
    Friend WithEvents picSensor_IngresoCortando As PictureBox
End Class
