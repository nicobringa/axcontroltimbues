﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlDinamismoDobleBarrera
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFallaB2 = New System.Windows.Forms.Label()
        Me.lblFallaB1 = New System.Windows.Forms.Label()
        Me.picBarrera1_Abajo = New System.Windows.Forms.PictureBox()
        Me.picBarrera2_Abajo = New System.Windows.Forms.PictureBox()
        Me.picBarrera2_Arriba = New System.Windows.Forms.PictureBox()
        Me.picBarrera1_Arriba = New System.Windows.Forms.PictureBox()
        Me.picSensorLecturaCortando = New System.Windows.Forms.PictureBox()
        Me.picSensor_IngresoCortando = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.picBarrera1_Abajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera2_Abajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera2_Arriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera1_Arriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensorLecturaCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_IngresoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFallaB2
        '
        Me.lblFallaB2.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaB2.AutoSize = True
        Me.lblFallaB2.BackColor = System.Drawing.Color.Red
        Me.lblFallaB2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaB2.ForeColor = System.Drawing.Color.White
        Me.lblFallaB2.Location = New System.Drawing.Point(429, 309)
        Me.lblFallaB2.Name = "lblFallaB2"
        Me.lblFallaB2.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaB2.TabIndex = 98
        Me.lblFallaB2.Text = "Falla"
        Me.lblFallaB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFallaB1
        '
        Me.lblFallaB1.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaB1.AutoSize = True
        Me.lblFallaB1.BackColor = System.Drawing.Color.Red
        Me.lblFallaB1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaB1.ForeColor = System.Drawing.Color.White
        Me.lblFallaB1.Location = New System.Drawing.Point(48, 318)
        Me.lblFallaB1.Name = "lblFallaB1"
        Me.lblFallaB1.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaB1.TabIndex = 99
        Me.lblFallaB1.Text = "Falla"
        Me.lblFallaB1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picBarrera1_Abajo
        '
        Me.picBarrera1_Abajo.Image = Global.Controles.My.Resources.Resources.barrera1abajo
        Me.picBarrera1_Abajo.Location = New System.Drawing.Point(34, 287)
        Me.picBarrera1_Abajo.Name = "picBarrera1_Abajo"
        Me.picBarrera1_Abajo.Size = New System.Drawing.Size(221, 18)
        Me.picBarrera1_Abajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera1_Abajo.TabIndex = 105
        Me.picBarrera1_Abajo.TabStop = False
        '
        'picBarrera2_Abajo
        '
        Me.picBarrera2_Abajo.Image = Global.Controles.My.Resources.Resources.barrera2abajo1
        Me.picBarrera2_Abajo.Location = New System.Drawing.Point(270, 284)
        Me.picBarrera2_Abajo.Name = "picBarrera2_Abajo"
        Me.picBarrera2_Abajo.Size = New System.Drawing.Size(189, 18)
        Me.picBarrera2_Abajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera2_Abajo.TabIndex = 104
        Me.picBarrera2_Abajo.TabStop = False
        '
        'picBarrera2_Arriba
        '
        Me.picBarrera2_Arriba.Image = Global.Controles.My.Resources.Resources.B2ARRIB
        Me.picBarrera2_Arriba.Location = New System.Drawing.Point(411, 146)
        Me.picBarrera2_Arriba.Name = "picBarrera2_Arriba"
        Me.picBarrera2_Arriba.Size = New System.Drawing.Size(56, 155)
        Me.picBarrera2_Arriba.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera2_Arriba.TabIndex = 100
        Me.picBarrera2_Arriba.TabStop = False
        '
        'picBarrera1_Arriba
        '
        Me.picBarrera1_Arriba.Image = Global.Controles.My.Resources.Resources.B1ARRIB
        Me.picBarrera1_Arriba.Location = New System.Drawing.Point(47, 118)
        Me.picBarrera1_Arriba.Name = "picBarrera1_Arriba"
        Me.picBarrera1_Arriba.Size = New System.Drawing.Size(75, 199)
        Me.picBarrera1_Arriba.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera1_Arriba.TabIndex = 101
        Me.picBarrera1_Arriba.TabStop = False
        '
        'picSensorLecturaCortando
        '
        Me.picSensorLecturaCortando.Image = Global.Controles.My.Resources.Resources.sensorLecturaCortando
        Me.picSensorLecturaCortando.Location = New System.Drawing.Point(70, 339)
        Me.picSensorLecturaCortando.Name = "picSensorLecturaCortando"
        Me.picSensorLecturaCortando.Size = New System.Drawing.Size(379, 23)
        Me.picSensorLecturaCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensorLecturaCortando.TabIndex = 103
        Me.picSensorLecturaCortando.TabStop = False
        '
        'picSensor_IngresoCortando
        '
        Me.picSensor_IngresoCortando.Image = Global.Controles.My.Resources.Resources.sensorBarreraCortando
        Me.picSensor_IngresoCortando.Location = New System.Drawing.Point(128, 309)
        Me.picSensor_IngresoCortando.Name = "picSensor_IngresoCortando"
        Me.picSensor_IngresoCortando.Size = New System.Drawing.Size(275, 20)
        Me.picSensor_IngresoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensor_IngresoCortando.TabIndex = 102
        Me.picSensor_IngresoCortando.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Controles.My.Resources.Resources.sensorSinCortar
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(601, 400)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'CtrlDinamismoDobleBarrera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.picBarrera1_Abajo)
        Me.Controls.Add(Me.picBarrera2_Abajo)
        Me.Controls.Add(Me.picBarrera2_Arriba)
        Me.Controls.Add(Me.picBarrera1_Arriba)
        Me.Controls.Add(Me.picSensorLecturaCortando)
        Me.Controls.Add(Me.picSensor_IngresoCortando)
        Me.Controls.Add(Me.lblFallaB1)
        Me.Controls.Add(Me.lblFallaB2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "CtrlDinamismoDobleBarrera"
        Me.Size = New System.Drawing.Size(594, 401)
        CType(Me.picBarrera1_Abajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera2_Abajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera2_Arriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera1_Arriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensorLecturaCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_IngresoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents lblFallaB2 As Label
    Friend WithEvents lblFallaB1 As Label
    Friend WithEvents picBarrera2_Arriba As PictureBox
    Friend WithEvents picBarrera1_Arriba As PictureBox
    Friend WithEvents picSensor_IngresoCortando As PictureBox
    Friend WithEvents picSensorLecturaCortando As PictureBox
    Friend WithEvents picBarrera2_Abajo As PictureBox
    Friend WithEvents picBarrera1_Abajo As PictureBox
End Class
