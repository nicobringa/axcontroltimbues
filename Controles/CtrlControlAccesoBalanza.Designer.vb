﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlControlAccesoBalanza
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoBalanza))
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.BAL2_BAR2 = New Controles.CtrlBarrera()
        Me.BAL1_BAR2 = New Controles.CtrlBarrera()
        Me.BAL1_BAR1 = New Controles.CtrlBarrera()
        Me.BAL2_BAR1 = New Controles.CtrlBarrera()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlDinamismoBalanza1 = New Controles.CtrlDinamismoBalanza()
        Me.BALANZA1 = New Controles.CtrlCabezalBalanza()
        Me.BALANZA2 = New Controles.CtrlCabezalBalanza()
        Me.S_LECTURA_BAL1_BAR1 = New Controles.CtrlSensor()
        Me.S_LECTURA_BAL2_BAR1 = New Controles.CtrlSensor()
        Me.S_LECTURA_BAL1_BAR2 = New Controles.CtrlSensor()
        Me.S_LECTURA_BAL2_BAR2 = New Controles.CtrlSensor()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.CtrlAnt_Leyendo_BAL2_BAR1 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAnt_Leyendo_BAL1_BAR1 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAnt_Leyendo_BAL1_BAR2 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAnt_Leyendo_BAL2_BAR2 = New Controles.CtrlAntenaLeyendo()
        Me.CartelLED_BALANZA1 = New Controles.CtrlCartelMultiLED()
        Me.CartelLED_BALANZA2 = New Controles.CtrlCartelMultiLED()
        Me.CAM_BAL1 = New Controles.CtrlCamara()
        Me.CAM_BAL2 = New Controles.CtrlCamara()
        Me.CtrlInfomacionBalanza1 = New Controles.ctrlInfomacionBalanza()
        Me.CtrlInfomacionBalanza2 = New Controles.ctrlInfomacionBalanza()
        Me.btnLecturaBal2Bar1 = New System.Windows.Forms.Button()
        Me.btnLecturaBal2Bar2 = New System.Windows.Forms.Button()
        Me.btnLecturaBal1Bar2 = New System.Windows.Forms.Button()
        Me.btnLecturaBal1Bar1 = New System.Windows.Forms.Button()
        Me.btnBajarBal2Bar1 = New System.Windows.Forms.Button()
        Me.btnSubirBal2Bar1 = New System.Windows.Forms.Button()
        Me.btnBajarBal2Bar2 = New System.Windows.Forms.Button()
        Me.btnSubirBal2Bar2 = New System.Windows.Forms.Button()
        Me.BtnBajarBal1Bar2 = New System.Windows.Forms.Button()
        Me.btnSubirBal1Bar2 = New System.Windows.Forms.Button()
        Me.btnBajarBal1Bar1 = New System.Windows.Forms.Button()
        Me.btnSubirBal1Bar1 = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.CtrlTiempoEsperaBal1 = New Controles.CtrlTiempoEspera()
        Me.CtrlTiempoEsperaBal2 = New Controles.CtrlTiempoEspera()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.BAL2_S1 = New Controles.CtrlSensor()
        Me.BAL1_S1 = New Controles.CtrlSensor()
        Me.BAL2_S2 = New Controles.CtrlSensor()
        Me.BAL1_S2 = New Controles.CtrlSensor()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 652)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(798, 57)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 105
        '
        'BAL2_BAR2
        '
        Me.BAL2_BAR2.BackColor = System.Drawing.Color.Transparent
        Me.BAL2_BAR2.Estado = False
        Me.BAL2_BAR2.GPIO = False
        Me.BAL2_BAR2.ID_CONTROL_ACCESO = 0
        Me.BAL2_BAR2.ID_PLC = 0
        Me.BAL2_BAR2.ID_SECTOR = 0
        Me.BAL2_BAR2.Inicializado = False
        Me.BAL2_BAR2.Location = New System.Drawing.Point(539, 288)
        Me.BAL2_BAR2.Name = "BAL2_BAR2"
        Me.BAL2_BAR2.PLC = Nothing
        Me.BAL2_BAR2.Size = New System.Drawing.Size(64, 50)
        Me.BAL2_BAR2.TabIndex = 98
        '
        'BAL1_BAR2
        '
        Me.BAL1_BAR2.BackColor = System.Drawing.Color.Transparent
        Me.BAL1_BAR2.Estado = False
        Me.BAL1_BAR2.GPIO = False
        Me.BAL1_BAR2.ID_CONTROL_ACCESO = 0
        Me.BAL1_BAR2.ID_PLC = 0
        Me.BAL1_BAR2.ID_SECTOR = 0
        Me.BAL1_BAR2.Inicializado = False
        Me.BAL1_BAR2.Location = New System.Drawing.Point(166, 297)
        Me.BAL1_BAR2.Name = "BAL1_BAR2"
        Me.BAL1_BAR2.PLC = Nothing
        Me.BAL1_BAR2.Size = New System.Drawing.Size(64, 50)
        Me.BAL1_BAR2.TabIndex = 97
        '
        'BAL1_BAR1
        '
        Me.BAL1_BAR1.BackColor = System.Drawing.Color.Transparent
        Me.BAL1_BAR1.Estado = False
        Me.BAL1_BAR1.GPIO = False
        Me.BAL1_BAR1.ID_CONTROL_ACCESO = 0
        Me.BAL1_BAR1.ID_PLC = 0
        Me.BAL1_BAR1.ID_SECTOR = 0
        Me.BAL1_BAR1.Inicializado = False
        Me.BAL1_BAR1.Location = New System.Drawing.Point(90, 386)
        Me.BAL1_BAR1.Name = "BAL1_BAR1"
        Me.BAL1_BAR1.PLC = Nothing
        Me.BAL1_BAR1.Size = New System.Drawing.Size(64, 50)
        Me.BAL1_BAR1.TabIndex = 96
        '
        'BAL2_BAR1
        '
        Me.BAL2_BAR1.BackColor = System.Drawing.Color.Transparent
        Me.BAL2_BAR1.Estado = False
        Me.BAL2_BAR1.GPIO = False
        Me.BAL2_BAR1.ID_CONTROL_ACCESO = 0
        Me.BAL2_BAR1.ID_PLC = 0
        Me.BAL2_BAR1.ID_SECTOR = 0
        Me.BAL2_BAR1.Inicializado = False
        Me.BAL2_BAR1.Location = New System.Drawing.Point(734, 386)
        Me.BAL2_BAR1.Name = "BAL2_BAR1"
        Me.BAL2_BAR1.PLC = Nothing
        Me.BAL2_BAR1.Size = New System.Drawing.Size(64, 50)
        Me.BAL2_BAR1.TabIndex = 95
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(801, -3)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(250, 712)
        Me.CtrlPanelNotificaciones1.TabIndex = 2
        '
        'CtrlDinamismoBalanza1
        '
        Me.CtrlDinamismoBalanza1.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(225, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.CtrlDinamismoBalanza1.BackgroundImage = CType(resources.GetObject("CtrlDinamismoBalanza1.BackgroundImage"), System.Drawing.Image)
        Me.CtrlDinamismoBalanza1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlDinamismoBalanza1.Location = New System.Drawing.Point(0, 0)
        Me.CtrlDinamismoBalanza1.Name = "CtrlDinamismoBalanza1"
        Me.CtrlDinamismoBalanza1.Size = New System.Drawing.Size(801, 601)
        Me.CtrlDinamismoBalanza1.TabIndex = 0
        '
        'BALANZA1
        '
        Me.BALANZA1.BackColor = System.Drawing.Color.Gainsboro
        Me.BALANZA1.GPIO = False
        Me.BALANZA1.ID_CONTROL_ACCESO = 0
        Me.BALANZA1.ID_PLC = 0
        Me.BALANZA1.ID_SECTOR = 0
        Me.BALANZA1.Inicializado = False
        Me.BALANZA1.Location = New System.Drawing.Point(3, 577)
        Me.BALANZA1.Name = "BALANZA1"
        Me.BALANZA1.numeroBalanza = 1
        Me.BALANZA1.PLC = Nothing
        Me.BALANZA1.Size = New System.Drawing.Size(267, 75)
        Me.BALANZA1.TabIndex = 106
        '
        'BALANZA2
        '
        Me.BALANZA2.BackColor = System.Drawing.Color.Gainsboro
        Me.BALANZA2.GPIO = False
        Me.BALANZA2.ID_CONTROL_ACCESO = 0
        Me.BALANZA2.ID_PLC = 0
        Me.BALANZA2.ID_SECTOR = 0
        Me.BALANZA2.Inicializado = False
        Me.BALANZA2.Location = New System.Drawing.Point(532, 577)
        Me.BALANZA2.Name = "BALANZA2"
        Me.BALANZA2.numeroBalanza = 2
        Me.BALANZA2.PLC = Nothing
        Me.BALANZA2.Size = New System.Drawing.Size(266, 75)
        Me.BALANZA2.TabIndex = 107
        '
        'S_LECTURA_BAL1_BAR1
        '
        Me.S_LECTURA_BAL1_BAR1.Estado = False
        Me.S_LECTURA_BAL1_BAR1.GPIO = False
        Me.S_LECTURA_BAL1_BAR1.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_BAL1_BAR1.ID_PLC = 0
        Me.S_LECTURA_BAL1_BAR1.ID_SECTOR = 0
        Me.S_LECTURA_BAL1_BAR1.Inicializado = False
        Me.S_LECTURA_BAL1_BAR1.Location = New System.Drawing.Point(294, 540)
        Me.S_LECTURA_BAL1_BAR1.Name = "S_LECTURA_BAL1_BAR1"
        Me.S_LECTURA_BAL1_BAR1.PLC = Nothing
        Me.S_LECTURA_BAL1_BAR1.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_BAL1_BAR1.TabIndex = 108
        '
        'S_LECTURA_BAL2_BAR1
        '
        Me.S_LECTURA_BAL2_BAR1.Estado = False
        Me.S_LECTURA_BAL2_BAR1.GPIO = False
        Me.S_LECTURA_BAL2_BAR1.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_BAL2_BAR1.ID_PLC = 0
        Me.S_LECTURA_BAL2_BAR1.ID_SECTOR = 0
        Me.S_LECTURA_BAL2_BAR1.Inicializado = False
        Me.S_LECTURA_BAL2_BAR1.Location = New System.Drawing.Point(400, 540)
        Me.S_LECTURA_BAL2_BAR1.Name = "S_LECTURA_BAL2_BAR1"
        Me.S_LECTURA_BAL2_BAR1.PLC = Nothing
        Me.S_LECTURA_BAL2_BAR1.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_BAL2_BAR1.TabIndex = 109
        '
        'S_LECTURA_BAL1_BAR2
        '
        Me.S_LECTURA_BAL1_BAR2.Estado = False
        Me.S_LECTURA_BAL1_BAR2.GPIO = False
        Me.S_LECTURA_BAL1_BAR2.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_BAL1_BAR2.ID_PLC = 0
        Me.S_LECTURA_BAL1_BAR2.ID_SECTOR = 0
        Me.S_LECTURA_BAL1_BAR2.Inicializado = False
        Me.S_LECTURA_BAL1_BAR2.Location = New System.Drawing.Point(320, 180)
        Me.S_LECTURA_BAL1_BAR2.Name = "S_LECTURA_BAL1_BAR2"
        Me.S_LECTURA_BAL1_BAR2.PLC = Nothing
        Me.S_LECTURA_BAL1_BAR2.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_BAL1_BAR2.TabIndex = 110
        '
        'S_LECTURA_BAL2_BAR2
        '
        Me.S_LECTURA_BAL2_BAR2.Estado = False
        Me.S_LECTURA_BAL2_BAR2.GPIO = False
        Me.S_LECTURA_BAL2_BAR2.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_BAL2_BAR2.ID_PLC = 0
        Me.S_LECTURA_BAL2_BAR2.ID_SECTOR = 0
        Me.S_LECTURA_BAL2_BAR2.Inicializado = False
        Me.S_LECTURA_BAL2_BAR2.Location = New System.Drawing.Point(400, 180)
        Me.S_LECTURA_BAL2_BAR2.Name = "S_LECTURA_BAL2_BAR2"
        Me.S_LECTURA_BAL2_BAR2.PLC = Nothing
        Me.S_LECTURA_BAL2_BAR2.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_BAL2_BAR2.TabIndex = 111
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(3, 3)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 112
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'CtrlAnt_Leyendo_BAL2_BAR1
        '
        Me.CtrlAnt_Leyendo_BAL2_BAR1.AutoSize = True
        Me.CtrlAnt_Leyendo_BAL2_BAR1.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAnt_Leyendo_BAL2_BAR1.Location = New System.Drawing.Point(492, 477)
        Me.CtrlAnt_Leyendo_BAL2_BAR1.Name = "CtrlAnt_Leyendo_BAL2_BAR1"
        Me.CtrlAnt_Leyendo_BAL2_BAR1.Size = New System.Drawing.Size(111, 99)
        Me.CtrlAnt_Leyendo_BAL2_BAR1.TabIndex = 113
        Me.CtrlAnt_Leyendo_BAL2_BAR1.UltTag = Nothing
        '
        'CtrlAnt_Leyendo_BAL1_BAR1
        '
        Me.CtrlAnt_Leyendo_BAL1_BAR1.AutoSize = True
        Me.CtrlAnt_Leyendo_BAL1_BAR1.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAnt_Leyendo_BAL1_BAR1.Location = New System.Drawing.Point(0, 472)
        Me.CtrlAnt_Leyendo_BAL1_BAR1.Name = "CtrlAnt_Leyendo_BAL1_BAR1"
        Me.CtrlAnt_Leyendo_BAL1_BAR1.Size = New System.Drawing.Size(111, 99)
        Me.CtrlAnt_Leyendo_BAL1_BAR1.TabIndex = 114
        Me.CtrlAnt_Leyendo_BAL1_BAR1.UltTag = Nothing
        '
        'CtrlAnt_Leyendo_BAL1_BAR2
        '
        Me.CtrlAnt_Leyendo_BAL1_BAR2.AutoSize = True
        Me.CtrlAnt_Leyendo_BAL1_BAR2.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAnt_Leyendo_BAL1_BAR2.Location = New System.Drawing.Point(90, 185)
        Me.CtrlAnt_Leyendo_BAL1_BAR2.Name = "CtrlAnt_Leyendo_BAL1_BAR2"
        Me.CtrlAnt_Leyendo_BAL1_BAR2.Size = New System.Drawing.Size(111, 99)
        Me.CtrlAnt_Leyendo_BAL1_BAR2.TabIndex = 115
        Me.CtrlAnt_Leyendo_BAL1_BAR2.UltTag = Nothing
        '
        'CtrlAnt_Leyendo_BAL2_BAR2
        '
        Me.CtrlAnt_Leyendo_BAL2_BAR2.AutoSize = True
        Me.CtrlAnt_Leyendo_BAL2_BAR2.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAnt_Leyendo_BAL2_BAR2.Location = New System.Drawing.Point(579, 185)
        Me.CtrlAnt_Leyendo_BAL2_BAR2.Name = "CtrlAnt_Leyendo_BAL2_BAR2"
        Me.CtrlAnt_Leyendo_BAL2_BAR2.Size = New System.Drawing.Size(111, 99)
        Me.CtrlAnt_Leyendo_BAL2_BAR2.TabIndex = 116
        Me.CtrlAnt_Leyendo_BAL2_BAR2.UltTag = Nothing
        '
        'CartelLED_BALANZA1
        '
        Me.CartelLED_BALANZA1.Conectado = False
        Me.CartelLED_BALANZA1.GPIO = False
        Me.CartelLED_BALANZA1.ID_CONTROL_ACCESO = 0
        Me.CartelLED_BALANZA1.ID_PLC = 0
        Me.CartelLED_BALANZA1.ID_SECTOR = 0
        Me.CartelLED_BALANZA1.Inicializado = False
        Me.CartelLED_BALANZA1.Location = New System.Drawing.Point(117, 472)
        Me.CartelLED_BALANZA1.Name = "CartelLED_BALANZA1"
        Me.CartelLED_BALANZA1.PLC = Nothing
        Me.CartelLED_BALANZA1.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_BALANZA1.TabIndex = 106
        '
        'CartelLED_BALANZA2
        '
        Me.CartelLED_BALANZA2.Conectado = False
        Me.CartelLED_BALANZA2.GPIO = False
        Me.CartelLED_BALANZA2.ID_CONTROL_ACCESO = 0
        Me.CartelLED_BALANZA2.ID_PLC = 0
        Me.CartelLED_BALANZA2.ID_SECTOR = 0
        Me.CartelLED_BALANZA2.Inicializado = False
        Me.CartelLED_BALANZA2.Location = New System.Drawing.Point(609, 472)
        Me.CartelLED_BALANZA2.Name = "CartelLED_BALANZA2"
        Me.CartelLED_BALANZA2.PLC = Nothing
        Me.CartelLED_BALANZA2.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_BALANZA2.TabIndex = 117
        '
        'CAM_BAL1
        '
        Me.CAM_BAL1.AutoSize = True
        Me.CAM_BAL1.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_BAL1.frmCamara = Nothing
        Me.CAM_BAL1.GPIO = False
        Me.CAM_BAL1.ID_CONTROL_ACCESO = 0
        Me.CAM_BAL1.ID_PLC = 0
        Me.CAM_BAL1.ID_SECTOR = 0
        Me.CAM_BAL1.Inicializado = False
        Me.CAM_BAL1.Location = New System.Drawing.Point(245, 49)
        Me.CAM_BAL1.Name = "CAM_BAL1"
        Me.CAM_BAL1.PLC = Nothing
        Me.CAM_BAL1.Size = New System.Drawing.Size(150, 60)
        Me.CAM_BAL1.TabIndex = 118
        '
        'CAM_BAL2
        '
        Me.CAM_BAL2.AutoSize = True
        Me.CAM_BAL2.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_BAL2.frmCamara = Nothing
        Me.CAM_BAL2.GPIO = False
        Me.CAM_BAL2.ID_CONTROL_ACCESO = 0
        Me.CAM_BAL2.ID_PLC = 0
        Me.CAM_BAL2.ID_SECTOR = 0
        Me.CAM_BAL2.Inicializado = False
        Me.CAM_BAL2.Location = New System.Drawing.Point(402, 49)
        Me.CAM_BAL2.Name = "CAM_BAL2"
        Me.CAM_BAL2.PLC = Nothing
        Me.CAM_BAL2.Size = New System.Drawing.Size(150, 60)
        Me.CAM_BAL2.TabIndex = 119
        '
        'CtrlInfomacionBalanza1
        '
        Me.CtrlInfomacionBalanza1.BackColor = System.Drawing.Color.White
        Me.CtrlInfomacionBalanza1.Location = New System.Drawing.Point(269, 607)
        Me.CtrlInfomacionBalanza1.Name = "CtrlInfomacionBalanza1"
        Me.CtrlInfomacionBalanza1.Size = New System.Drawing.Size(127, 36)
        Me.CtrlInfomacionBalanza1.TabIndex = 121
        '
        'CtrlInfomacionBalanza2
        '
        Me.CtrlInfomacionBalanza2.BackColor = System.Drawing.Color.White
        Me.CtrlInfomacionBalanza2.Location = New System.Drawing.Point(402, 607)
        Me.CtrlInfomacionBalanza2.Name = "CtrlInfomacionBalanza2"
        Me.CtrlInfomacionBalanza2.Size = New System.Drawing.Size(129, 36)
        Me.CtrlInfomacionBalanza2.TabIndex = 122
        '
        'btnLecturaBal2Bar1
        '
        Me.btnLecturaBal2Bar1.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaBal2Bar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaBal2Bar1.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaBal2Bar1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaBal2Bar1.Location = New System.Drawing.Point(641, 328)
        Me.btnLecturaBal2Bar1.Name = "btnLecturaBal2Bar1"
        Me.btnLecturaBal2Bar1.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaBal2Bar1.TabIndex = 138
        Me.btnLecturaBal2Bar1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaBal2Bar1.UseVisualStyleBackColor = False
        '
        'btnLecturaBal2Bar2
        '
        Me.btnLecturaBal2Bar2.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaBal2Bar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaBal2Bar2.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaBal2Bar2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaBal2Bar2.Location = New System.Drawing.Point(438, 257)
        Me.btnLecturaBal2Bar2.Name = "btnLecturaBal2Bar2"
        Me.btnLecturaBal2Bar2.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaBal2Bar2.TabIndex = 137
        Me.btnLecturaBal2Bar2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaBal2Bar2.UseVisualStyleBackColor = False
        '
        'btnLecturaBal1Bar2
        '
        Me.btnLecturaBal1Bar2.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaBal1Bar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaBal1Bar2.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaBal1Bar2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaBal1Bar2.Location = New System.Drawing.Point(267, 257)
        Me.btnLecturaBal1Bar2.Name = "btnLecturaBal1Bar2"
        Me.btnLecturaBal1Bar2.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaBal1Bar2.TabIndex = 136
        Me.btnLecturaBal1Bar2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaBal1Bar2.UseVisualStyleBackColor = False
        '
        'btnLecturaBal1Bar1
        '
        Me.btnLecturaBal1Bar1.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaBal1Bar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaBal1Bar1.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaBal1Bar1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaBal1Bar1.Location = New System.Drawing.Point(90, 328)
        Me.btnLecturaBal1Bar1.Name = "btnLecturaBal1Bar1"
        Me.btnLecturaBal1Bar1.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaBal1Bar1.TabIndex = 135
        Me.btnLecturaBal1Bar1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaBal1Bar1.UseVisualStyleBackColor = False
        '
        'btnBajarBal2Bar1
        '
        Me.btnBajarBal2Bar1.BackColor = System.Drawing.Color.Transparent
        Me.btnBajarBal2Bar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBajarBal2Bar1.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.btnBajarBal2Bar1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBajarBal2Bar1.Location = New System.Drawing.Point(716, 125)
        Me.btnBajarBal2Bar1.Name = "btnBajarBal2Bar1"
        Me.btnBajarBal2Bar1.Size = New System.Drawing.Size(40, 54)
        Me.btnBajarBal2Bar1.TabIndex = 146
        Me.btnBajarBal2Bar1.UseVisualStyleBackColor = False
        '
        'btnSubirBal2Bar1
        '
        Me.btnSubirBal2Bar1.BackColor = System.Drawing.Color.Transparent
        Me.btnSubirBal2Bar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubirBal2Bar1.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubirBal2Bar1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubirBal2Bar1.Location = New System.Drawing.Point(678, 125)
        Me.btnSubirBal2Bar1.Name = "btnSubirBal2Bar1"
        Me.btnSubirBal2Bar1.Size = New System.Drawing.Size(40, 54)
        Me.btnSubirBal2Bar1.TabIndex = 145
        Me.btnSubirBal2Bar1.UseVisualStyleBackColor = False
        '
        'btnBajarBal2Bar2
        '
        Me.btnBajarBal2Bar2.BackColor = System.Drawing.Color.Transparent
        Me.btnBajarBal2Bar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBajarBal2Bar2.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.btnBajarBal2Bar2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBajarBal2Bar2.Location = New System.Drawing.Point(521, 171)
        Me.btnBajarBal2Bar2.Name = "btnBajarBal2Bar2"
        Me.btnBajarBal2Bar2.Size = New System.Drawing.Size(40, 54)
        Me.btnBajarBal2Bar2.TabIndex = 144
        Me.btnBajarBal2Bar2.UseVisualStyleBackColor = False
        '
        'btnSubirBal2Bar2
        '
        Me.btnSubirBal2Bar2.BackColor = System.Drawing.Color.Transparent
        Me.btnSubirBal2Bar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubirBal2Bar2.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubirBal2Bar2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubirBal2Bar2.Location = New System.Drawing.Point(482, 171)
        Me.btnSubirBal2Bar2.Name = "btnSubirBal2Bar2"
        Me.btnSubirBal2Bar2.Size = New System.Drawing.Size(40, 54)
        Me.btnSubirBal2Bar2.TabIndex = 143
        Me.btnSubirBal2Bar2.UseVisualStyleBackColor = False
        '
        'BtnBajarBal1Bar2
        '
        Me.BtnBajarBal1Bar2.BackColor = System.Drawing.Color.Transparent
        Me.BtnBajarBal1Bar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBajarBal1Bar2.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.BtnBajarBal1Bar2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnBajarBal1Bar2.Location = New System.Drawing.Point(245, 171)
        Me.BtnBajarBal1Bar2.Name = "BtnBajarBal1Bar2"
        Me.BtnBajarBal1Bar2.Size = New System.Drawing.Size(40, 54)
        Me.BtnBajarBal1Bar2.TabIndex = 142
        Me.BtnBajarBal1Bar2.UseVisualStyleBackColor = False
        '
        'btnSubirBal1Bar2
        '
        Me.btnSubirBal1Bar2.BackColor = System.Drawing.Color.Transparent
        Me.btnSubirBal1Bar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubirBal1Bar2.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubirBal1Bar2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubirBal1Bar2.Location = New System.Drawing.Point(207, 171)
        Me.btnSubirBal1Bar2.Name = "btnSubirBal1Bar2"
        Me.btnSubirBal1Bar2.Size = New System.Drawing.Size(40, 54)
        Me.btnSubirBal1Bar2.TabIndex = 141
        Me.btnSubirBal1Bar2.UseVisualStyleBackColor = False
        '
        'btnBajarBal1Bar1
        '
        Me.btnBajarBal1Bar1.BackColor = System.Drawing.Color.Transparent
        Me.btnBajarBal1Bar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBajarBal1Bar1.Image = Global.Controles.My.Resources.Resources.Bajar
        Me.btnBajarBal1Bar1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBajarBal1Bar1.Location = New System.Drawing.Point(62, 124)
        Me.btnBajarBal1Bar1.Name = "btnBajarBal1Bar1"
        Me.btnBajarBal1Bar1.Size = New System.Drawing.Size(40, 54)
        Me.btnBajarBal1Bar1.TabIndex = 140
        Me.btnBajarBal1Bar1.UseVisualStyleBackColor = False
        '
        'btnSubirBal1Bar1
        '
        Me.btnSubirBal1Bar1.BackColor = System.Drawing.Color.Transparent
        Me.btnSubirBal1Bar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubirBal1Bar1.Image = Global.Controles.My.Resources.Resources.Subir
        Me.btnSubirBal1Bar1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSubirBal1Bar1.Location = New System.Drawing.Point(24, 124)
        Me.btnSubirBal1Bar1.Name = "btnSubirBal1Bar1"
        Me.btnSubirBal1Bar1.Size = New System.Drawing.Size(40, 54)
        Me.btnSubirBal1Bar1.TabIndex = 139
        Me.btnSubirBal1Bar1.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(755, 3)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 147
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'CtrlTiempoEsperaBal1
        '
        Me.CtrlTiempoEsperaBal1.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEsperaBal1.Location = New System.Drawing.Point(12, 53)
        Me.CtrlTiempoEsperaBal1.Name = "CtrlTiempoEsperaBal1"
        Me.CtrlTiempoEsperaBal1.SetVisible = False
        Me.CtrlTiempoEsperaBal1.Size = New System.Drawing.Size(99, 60)
        Me.CtrlTiempoEsperaBal1.TabIndex = 148
        Me.CtrlTiempoEsperaBal1.Tiempo = Nothing
        '
        'CtrlTiempoEsperaBal2
        '
        Me.CtrlTiempoEsperaBal2.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEsperaBal2.Location = New System.Drawing.Point(681, 52)
        Me.CtrlTiempoEsperaBal2.Name = "CtrlTiempoEsperaBal2"
        Me.CtrlTiempoEsperaBal2.SetVisible = False
        Me.CtrlTiempoEsperaBal2.Size = New System.Drawing.Size(99, 60)
        Me.CtrlTiempoEsperaBal2.TabIndex = 149
        Me.CtrlTiempoEsperaBal2.Tiempo = Nothing
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(160, 3)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(49, 44)
        Me.btnAyuda.TabIndex = 150
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.Black
        Me.lblNombre.Location = New System.Drawing.Point(215, 4)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(534, 42)
        Me.lblNombre.TabIndex = 155
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BAL2_S1
        '
        Me.BAL2_S1.Estado = False
        Me.BAL2_S1.GPIO = False
        Me.BAL2_S1.ID_CONTROL_ACCESO = 0
        Me.BAL2_S1.ID_PLC = 0
        Me.BAL2_S1.ID_SECTOR = 0
        Me.BAL2_S1.Inicializado = False
        Me.BAL2_S1.Location = New System.Drawing.Point(378, 461)
        Me.BAL2_S1.Name = "BAL2_S1"
        Me.BAL2_S1.PLC = Nothing
        Me.BAL2_S1.Size = New System.Drawing.Size(76, 45)
        Me.BAL2_S1.TabIndex = 158
        '
        'BAL1_S1
        '
        Me.BAL1_S1.Estado = False
        Me.BAL1_S1.GPIO = False
        Me.BAL1_S1.ID_CONTROL_ACCESO = 0
        Me.BAL1_S1.ID_PLC = 0
        Me.BAL1_S1.ID_SECTOR = 0
        Me.BAL1_S1.Inicializado = False
        Me.BAL1_S1.Location = New System.Drawing.Point(296, 461)
        Me.BAL1_S1.Name = "BAL1_S1"
        Me.BAL1_S1.PLC = Nothing
        Me.BAL1_S1.Size = New System.Drawing.Size(76, 45)
        Me.BAL1_S1.TabIndex = 159
        '
        'BAL2_S2
        '
        Me.BAL2_S2.Estado = False
        Me.BAL2_S2.GPIO = False
        Me.BAL2_S2.ID_CONTROL_ACCESO = 0
        Me.BAL2_S2.ID_PLC = 0
        Me.BAL2_S2.ID_SECTOR = 0
        Me.BAL2_S2.Inicializado = False
        Me.BAL2_S2.Location = New System.Drawing.Point(378, 360)
        Me.BAL2_S2.Name = "BAL2_S2"
        Me.BAL2_S2.PLC = Nothing
        Me.BAL2_S2.Size = New System.Drawing.Size(76, 45)
        Me.BAL2_S2.TabIndex = 160
        '
        'BAL1_S2
        '
        Me.BAL1_S2.Estado = False
        Me.BAL1_S2.GPIO = False
        Me.BAL1_S2.ID_CONTROL_ACCESO = 0
        Me.BAL1_S2.ID_PLC = 0
        Me.BAL1_S2.ID_SECTOR = 0
        Me.BAL1_S2.Inicializado = False
        Me.BAL1_S2.Location = New System.Drawing.Point(296, 360)
        Me.BAL1_S2.Name = "BAL1_S2"
        Me.BAL1_S2.PLC = Nothing
        Me.BAL1_S2.Size = New System.Drawing.Size(76, 45)
        Me.BAL1_S2.TabIndex = 161
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(678, 64)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 194
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CtrlControlAccesoBalanza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.BAL1_S2)
        Me.Controls.Add(Me.BAL2_S2)
        Me.Controls.Add(Me.BAL1_S1)
        Me.Controls.Add(Me.BAL2_S1)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.CtrlTiempoEsperaBal2)
        Me.Controls.Add(Me.CtrlTiempoEsperaBal1)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnBajarBal2Bar1)
        Me.Controls.Add(Me.btnSubirBal2Bar1)
        Me.Controls.Add(Me.btnBajarBal2Bar2)
        Me.Controls.Add(Me.btnSubirBal2Bar2)
        Me.Controls.Add(Me.BtnBajarBal1Bar2)
        Me.Controls.Add(Me.btnSubirBal1Bar2)
        Me.Controls.Add(Me.btnBajarBal1Bar1)
        Me.Controls.Add(Me.btnSubirBal1Bar1)
        Me.Controls.Add(Me.btnLecturaBal2Bar1)
        Me.Controls.Add(Me.btnLecturaBal2Bar2)
        Me.Controls.Add(Me.btnLecturaBal1Bar2)
        Me.Controls.Add(Me.btnLecturaBal1Bar1)
        Me.Controls.Add(Me.CtrlInfomacionBalanza2)
        Me.Controls.Add(Me.CtrlInfomacionBalanza1)
        Me.Controls.Add(Me.CAM_BAL2)
        Me.Controls.Add(Me.CAM_BAL1)
        Me.Controls.Add(Me.CartelLED_BALANZA2)
        Me.Controls.Add(Me.CartelLED_BALANZA1)
        Me.Controls.Add(Me.CtrlAnt_Leyendo_BAL2_BAR2)
        Me.Controls.Add(Me.CtrlAnt_Leyendo_BAL1_BAR2)
        Me.Controls.Add(Me.CtrlAnt_Leyendo_BAL1_BAR1)
        Me.Controls.Add(Me.CtrlAnt_Leyendo_BAL2_BAR1)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.S_LECTURA_BAL2_BAR2)
        Me.Controls.Add(Me.S_LECTURA_BAL1_BAR2)
        Me.Controls.Add(Me.S_LECTURA_BAL2_BAR1)
        Me.Controls.Add(Me.S_LECTURA_BAL1_BAR1)
        Me.Controls.Add(Me.BALANZA2)
        Me.Controls.Add(Me.BALANZA1)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.BAL2_BAR2)
        Me.Controls.Add(Me.BAL1_BAR2)
        Me.Controls.Add(Me.BAL1_BAR1)
        Me.Controls.Add(Me.BAL2_BAR1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.CtrlDinamismoBalanza1)
        Me.Name = "CtrlControlAccesoBalanza"
        Me.Size = New System.Drawing.Size(1051, 712)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlDinamismoBalanza1 As CtrlDinamismoBalanza
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents S_LECTURA_BAL1_BAR1 As CtrlSensor
    Friend WithEvents S_LECTURA_BAL2_BAR1 As CtrlSensor
    Friend WithEvents S_LECTURA_BAL1_BAR2 As CtrlSensor
    Friend WithEvents S_LECTURA_BAL2_BAR2 As CtrlSensor
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents CtrlAnt_Leyendo_BAL2_BAR1 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAnt_Leyendo_BAL1_BAR1 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAnt_Leyendo_BAL1_BAR2 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAnt_Leyendo_BAL2_BAR2 As CtrlAntenaLeyendo
    Friend WithEvents CartelLED_BALANZA1 As CtrlCartelMultiLED
    Friend WithEvents CartelLED_BALANZA2 As CtrlCartelMultiLED
    Friend WithEvents CAM_BAL1 As CtrlCamara
    Friend WithEvents CAM_BAL2 As CtrlCamara
    Friend WithEvents CtrlInfomacionBalanza1 As ctrlInfomacionBalanza
    Friend WithEvents CtrlInfomacionBalanza2 As ctrlInfomacionBalanza
    Friend WithEvents btnLecturaBal2Bar1 As Button
    Friend WithEvents btnLecturaBal2Bar2 As Button
    Friend WithEvents btnLecturaBal1Bar2 As Button
    Friend WithEvents btnLecturaBal1Bar1 As Button
    Friend WithEvents btnBajarBal2Bar1 As Button
    Friend WithEvents btnSubirBal2Bar1 As Button
    Friend WithEvents btnBajarBal2Bar2 As Button
    Friend WithEvents btnSubirBal2Bar2 As Button
    Friend WithEvents BtnBajarBal1Bar2 As Button
    Friend WithEvents btnSubirBal1Bar2 As Button
    Friend WithEvents btnBajarBal1Bar1 As Button
    Friend WithEvents btnSubirBal1Bar1 As Button
    Public WithEvents BAL2_BAR1 As CtrlBarrera
    Public WithEvents BAL1_BAR1 As CtrlBarrera
    Public WithEvents BAL1_BAR2 As CtrlBarrera
    Public WithEvents BAL2_BAR2 As CtrlBarrera
    Public WithEvents BALANZA1 As CtrlCabezalBalanza
    Public WithEvents BALANZA2 As CtrlCabezalBalanza
    Friend WithEvents btnReset As Button
    Friend WithEvents CtrlTiempoEsperaBal1 As CtrlTiempoEspera
    Friend WithEvents CtrlTiempoEsperaBal2 As CtrlTiempoEspera
    Friend WithEvents btnAyuda As Button
    Friend WithEvents lblNombre As Label
    Friend WithEvents BAL2_S1 As CtrlSensor
    Friend WithEvents BAL1_S1 As CtrlSensor
    Friend WithEvents BAL2_S2 As CtrlSensor
    Friend WithEvents BAL1_S2 As CtrlSensor
    Friend WithEvents btnAlarma As Button
End Class
