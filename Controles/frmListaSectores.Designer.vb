﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListaSectores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgSector = New System.Windows.Forms.DataGridView()
        Me.colControl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroSector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgSector, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgSector
        '
        Me.dgSector.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgSector.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colControl, Me.colNroSector, Me.colSector})
        Me.dgSector.Location = New System.Drawing.Point(-2, -1)
        Me.dgSector.Name = "dgSector"
        Me.dgSector.Size = New System.Drawing.Size(746, 214)
        Me.dgSector.TabIndex = 0
        '
        'colControl
        '
        Me.colControl.HeaderText = "Control Acceso"
        Me.colControl.Name = "colControl"
        Me.colControl.Width = 300
        '
        'colNroSector
        '
        Me.colNroSector.HeaderText = "Nro. Sector"
        Me.colNroSector.Name = "colNroSector"
        '
        'colSector
        '
        Me.colSector.HeaderText = "Sector"
        Me.colSector.Name = "colSector"
        Me.colSector.Width = 300
        '
        'frmListaSectores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(745, 209)
        Me.Controls.Add(Me.dgSector)
        Me.Name = "frmListaSectores"
        Me.Text = "frmListaSectores"
        CType(Me.dgSector, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgSector As DataGridView
    Friend WithEvents colControl As DataGridViewTextBoxColumn
    Friend WithEvents colNroSector As DataGridViewTextBoxColumn
    Friend WithEvents colSector As DataGridViewTextBoxColumn
End Class
