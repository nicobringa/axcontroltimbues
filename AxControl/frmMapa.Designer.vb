﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMapa
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapa))
        Me.lblContenedor = New System.Windows.Forms.Label()
        Me.lblTexto = New System.Windows.Forms.Label()
        Me.lblColor = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()

        Me.Label5 = New System.Windows.Forms.Label()
        Me.CtrlMenu1 = New Controles.CtrlMenu()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PanelControlAcceso = New System.Windows.Forms.Panel()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblContenedor
        '
        Me.lblContenedor.BackColor = System.Drawing.SystemColors.Desktop
        Me.lblContenedor.Location = New System.Drawing.Point(20, 822)
        Me.lblContenedor.Name = "lblContenedor"
        Me.lblContenedor.Size = New System.Drawing.Size(231, 136)
        Me.lblContenedor.TabIndex = 13
        '
        'lblTexto
        '
        Me.lblTexto.BackColor = System.Drawing.SystemColors.Desktop
        Me.lblTexto.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblTexto.Location = New System.Drawing.Point(60, 862)
        Me.lblTexto.Name = "lblTexto"
        Me.lblTexto.Size = New System.Drawing.Size(187, 21)
        Me.lblTexto.TabIndex = 14
        Me.lblTexto.Text = "APARECIDA  (Color parpadeando)"
        '
        'lblColor
        '
        Me.lblColor.BackColor = System.Drawing.Color.Red
        Me.lblColor.Location = New System.Drawing.Point(25, 858)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(29, 25)
        Me.lblColor.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(25, 892)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 25)
        Me.Label1.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Lime
        Me.Label2.Location = New System.Drawing.Point(25, 926)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 25)
        Me.Label2.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Desktop
        Me.Label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label3.Location = New System.Drawing.Point(60, 896)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(191, 21)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "APARECIDA RECONOCIDA (Color fijo)"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Desktop
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(60, 929)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(115, 21)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "DESAPARECIDA"
        '
        'CtrlAlarmero1
        '
        Me.CtrlAlarmero1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CtrlAlarmero1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.CtrlAlarmero1.idSector = 0
        Me.CtrlAlarmero1.Location = New System.Drawing.Point(0, 51)
        Me.CtrlAlarmero1.Name = "CtrlAlarmero1"
        Me.CtrlAlarmero1.Size = New System.Drawing.Size(1829, 100)
        Me.CtrlAlarmero1.TabIndex = 12
        '
        'CtrlAlarmaPorteriaEgreso
        '
        Me.CtrlAlarmaPorteriaEgreso.BackColor = System.Drawing.Color.Lime
        Me.CtrlAlarmaPorteriaEgreso.BitMonitoreo = CType(0, Short)
        Me.CtrlAlarmaPorteriaEgreso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CtrlAlarmaPorteriaEgreso.ControlAcceso = Nothing
        Me.CtrlAlarmaPorteriaEgreso.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CtrlAlarmaPorteriaEgreso.GPIO = False
        Me.CtrlAlarmaPorteriaEgreso.idControlAcceso = 40
        Me.CtrlAlarmaPorteriaEgreso.idPLC = 0
        Me.CtrlAlarmaPorteriaEgreso.idSector = 40
        Me.CtrlAlarmaPorteriaEgreso.Inicializado = False
        Me.CtrlAlarmaPorteriaEgreso.Location = New System.Drawing.Point(447, 628)
        Me.CtrlAlarmaPorteriaEgreso.Name = "CtrlAlarmaPorteriaEgreso"
        Me.CtrlAlarmaPorteriaEgreso.objeto = "CALADO 4 CALLES"
        Me.CtrlAlarmaPorteriaEgreso.PLC = Nothing
        Me.CtrlAlarmaPorteriaEgreso.Size = New System.Drawing.Size(74, 61)
        Me.CtrlAlarmaPorteriaEgreso.TabIndex = 4
        '
        'CtrlAlarmaBalanzaIngreso
        '
        Me.CtrlAlarmaBalanzaIngreso.BackColor = System.Drawing.Color.Lime
        Me.CtrlAlarmaBalanzaIngreso.BitMonitoreo = CType(0, Short)
        Me.CtrlAlarmaBalanzaIngreso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CtrlAlarmaBalanzaIngreso.ControlAcceso = Nothing
        Me.CtrlAlarmaBalanzaIngreso.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CtrlAlarmaBalanzaIngreso.GPIO = False
        Me.CtrlAlarmaBalanzaIngreso.idControlAcceso = 30
        Me.CtrlAlarmaBalanzaIngreso.idPLC = 0
        Me.CtrlAlarmaBalanzaIngreso.idSector = 30
        Me.CtrlAlarmaBalanzaIngreso.Inicializado = False
        Me.CtrlAlarmaBalanzaIngreso.Location = New System.Drawing.Point(791, 449)
        Me.CtrlAlarmaBalanzaIngreso.Name = "CtrlAlarmaBalanzaIngreso"
        Me.CtrlAlarmaBalanzaIngreso.objeto = "INGRESO CALADO"
        Me.CtrlAlarmaBalanzaIngreso.PLC = Nothing
        Me.CtrlAlarmaBalanzaIngreso.Size = New System.Drawing.Size(74, 61)
        Me.CtrlAlarmaBalanzaIngreso.TabIndex = 3
        '
        'CtrlAlarmaBalanzaEgreso
        '
        Me.CtrlAlarmaBalanzaEgreso.BackColor = System.Drawing.Color.Lime
        Me.CtrlAlarmaBalanzaEgreso.BitMonitoreo = CType(0, Short)
        Me.CtrlAlarmaBalanzaEgreso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CtrlAlarmaBalanzaEgreso.ControlAcceso = Nothing
        Me.CtrlAlarmaBalanzaEgreso.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CtrlAlarmaBalanzaEgreso.GPIO = False
        Me.CtrlAlarmaBalanzaEgreso.idControlAcceso = 20
        Me.CtrlAlarmaBalanzaEgreso.idPLC = 0
        Me.CtrlAlarmaBalanzaEgreso.idSector = 20
        Me.CtrlAlarmaBalanzaEgreso.Inicializado = False
        Me.CtrlAlarmaBalanzaEgreso.Location = New System.Drawing.Point(494, 449)
        Me.CtrlAlarmaBalanzaEgreso.Name = "CtrlAlarmaBalanzaEgreso"
        Me.CtrlAlarmaBalanzaEgreso.objeto = "CALADO 2 CALLES"
        Me.CtrlAlarmaBalanzaEgreso.PLC = Nothing
        Me.CtrlAlarmaBalanzaEgreso.Size = New System.Drawing.Size(74, 61)
        Me.CtrlAlarmaBalanzaEgreso.TabIndex = 2
        '
        'ctrlAlarmaIngresoPorteria
        '
        Me.ctrlAlarmaIngresoPorteria.BackColor = System.Drawing.Color.Lime
        Me.ctrlAlarmaIngresoPorteria.BitMonitoreo = CType(0, Short)
        Me.ctrlAlarmaIngresoPorteria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ctrlAlarmaIngresoPorteria.ControlAcceso = Nothing
        Me.ctrlAlarmaIngresoPorteria.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ctrlAlarmaIngresoPorteria.GPIO = False
        Me.ctrlAlarmaIngresoPorteria.idControlAcceso = 10
        Me.ctrlAlarmaIngresoPorteria.idPLC = 0
        Me.ctrlAlarmaIngresoPorteria.idSector = 10
        Me.ctrlAlarmaIngresoPorteria.Inicializado = False
        Me.ctrlAlarmaIngresoPorteria.Location = New System.Drawing.Point(718, 589)
        Me.ctrlAlarmaIngresoPorteria.Name = "ctrlAlarmaIngresoPorteria"
        Me.ctrlAlarmaIngresoPorteria.objeto = "PORTERIA INGRESO"
        Me.ctrlAlarmaIngresoPorteria.PLC = Nothing
        Me.ctrlAlarmaIngresoPorteria.Size = New System.Drawing.Size(74, 61)
        Me.ctrlAlarmaIngresoPorteria.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Desktop
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(25, 832)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(187, 21)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "ESTADOS DE ALARMA:"
        '
        'CtrlMenu1
        '
        Me.CtrlMenu1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CtrlMenu1.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.CtrlMenu1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CtrlMenu1.HabilitarScroll = False
        Me.CtrlMenu1.Location = New System.Drawing.Point(0, -1)
        Me.CtrlMenu1.Name = "CtrlMenu1"
        Me.CtrlMenu1.Size = New System.Drawing.Size(4788, 53)
        Me.CtrlMenu1.TabIndex = 21
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.AxControl.My.Resources.Resources.Fondo
        Me.PictureBox1.Location = New System.Drawing.Point(-106, 97)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1915, 896)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PanelControlAcceso
        '
        Me.PanelControlAcceso.BackColor = System.Drawing.Color.White
        Me.PanelControlAcceso.Location = New System.Drawing.Point(1001, 151)
        Me.PanelControlAcceso.Name = "PanelControlAcceso"
        Me.PanelControlAcceso.Size = New System.Drawing.Size(902, 842)
        Me.PanelControlAcceso.TabIndex = 23
        '
        'frmMapa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1904, 770)
        Me.Controls.Add(Me.PanelControlAcceso)
        Me.Controls.Add(Me.CtrlMenu1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblColor)
        Me.Controls.Add(Me.lblTexto)
        Me.Controls.Add(Me.lblContenedor)
        Me.Controls.Add(Me.CtrlAlarmero1)
        Me.Controls.Add(Me.CtrlAlarmaPorteriaEgreso)
        Me.Controls.Add(Me.CtrlAlarmaBalanzaIngreso)
        Me.Controls.Add(Me.CtrlAlarmaBalanzaEgreso)
        Me.Controls.Add(Me.ctrlAlarmaIngresoPorteria)
        Me.Controls.Add(Me.PictureBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMapa"
        Me.Text = " Controles de Acceso"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As PictureBox

    Friend WithEvents lblContenedor As Label
    Friend WithEvents lblTexto As Label
    Friend WithEvents lblColor As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents CtrlMenu1 As Controles.CtrlMenu
    Friend WithEvents PanelControlAcceso As Panel
End Class
