﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;
using System.Web.UI.WebControls;

namespace AxControlWeb.Controllers
{
    public class CONFIG_LECTOR_RFIDController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();





        // GET: CONFIG_LECTOR_RFID
        public ActionResult Index()
        {
            var cONFIG_LECTOR_RFID = db.CONFIG_LECTOR_RFID.Include(c => c.LECTOR_RFID);
            return View(cONFIG_LECTOR_RFID.ToList());
        }

        // GET: CONFIG_LECTOR_RFID/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONFIG_LECTOR_RFID cONFIG_LECTOR_RFID = db.CONFIG_LECTOR_RFID.Find(id);
            if (cONFIG_LECTOR_RFID == null)
            {
                return HttpNotFound();
            }
            return View(cONFIG_LECTOR_RFID);
        }

        // GET: CONFIG_LECTOR_RFID/Create
        public ActionResult Create()
        {
            ViewBag.ID_LECTOR_RFID = new SelectList(db.LECTOR_RFID, "ID_LECTOR_RFID", "NOMBRE");
            return View();
        }

        // POST: CONFIG_LECTOR_RFID/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_CONF_LECTOR_RFID,ID_LECTOR_RFID,NOMBRE,SELECT_READER_MODE,SELECT_SEARCH,SELECT_SESSION,SELECT_REPORT_MODE,GPIO_PUERTO1,GPIO_PUERTO2,GPIO_PUERTO3,GPIO_PUERTO4,GPIO_HABILITADO,RSSI,SELECCIONADO")] CONFIG_LECTOR_RFID cONFIG_LECTOR_RFID)
        {
            if (ModelState.IsValid)
            {
                db.CONFIG_LECTOR_RFID.Add(cONFIG_LECTOR_RFID);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_LECTOR_RFID = new SelectList(db.LECTOR_RFID, "ID_LECTOR_RFID", "NOMBRE", cONFIG_LECTOR_RFID.ID_LECTOR_RFID);
            return View(cONFIG_LECTOR_RFID);
        }

        // GET: CONFIG_LECTOR_RFID/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONFIG_LECTOR_RFID cONFIG_LECTOR_RFID = db.CONFIG_LECTOR_RFID.Find(id);
            if (cONFIG_LECTOR_RFID == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_LECTOR_RFID = new SelectList(db.LECTOR_RFID, "ID_LECTOR_RFID", "NOMBRE", cONFIG_LECTOR_RFID.ID_LECTOR_RFID);

            List<ListItem> items = new List<ListItem>();
            items.Add(new ListItem("Auto Set Dense Reader", "1000"));
            items.Add(new ListItem("Auto Set Single Reader", "1001"));
            items.Add(new ListItem("Max Throughput", "0"));
            items.Add(new ListItem("Hybrid", "1"));
            items.Add(new ListItem("Dense Reader (M=4)", "2"));
            items.Add(new ListItem("Dense Reader (M=8)", "3"));
            items.Add(new ListItem("MaxMiller", "4"));         
            ViewBag.READER_MODE = new SelectList(items, "value", "text", cONFIG_LECTOR_RFID.SELECT_READER_MODE.ToString());
            return View(cONFIG_LECTOR_RFID);
        }

        // POST: CONFIG_LECTOR_RFID/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_CONF_LECTOR_RFID,ID_LECTOR_RFID,NOMBRE,SELECT_READER_MODE,SELECT_SEARCH,SELECT_SESSION,SELECT_REPORT_MODE,GPIO_PUERTO1,GPIO_PUERTO2,GPIO_PUERTO3,GPIO_PUERTO4,GPIO_HABILITADO,RSSI")]CONFIG_LECTOR_RFID cONFIG_LECTOR_RFID, string READER_MODE)
        {
            if (ModelState.IsValid)
            {
                cONFIG_LECTOR_RFID.SELECT_READER_MODE = Int32.Parse(READER_MODE);
                db.Entry(cONFIG_LECTOR_RFID).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Editar","LECTOR_RFID", new { id= cONFIG_LECTOR_RFID.ID_CONF_LECTOR_RFID});
            }
            ViewBag.ID_LECTOR_RFID = new SelectList(db.LECTOR_RFID, "ID_LECTOR_RFID", "NOMBRE", cONFIG_LECTOR_RFID.ID_LECTOR_RFID);
            return View(cONFIG_LECTOR_RFID);
        }

        // GET: CONFIG_LECTOR_RFID/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONFIG_LECTOR_RFID cONFIG_LECTOR_RFID = db.CONFIG_LECTOR_RFID.Find(id);
            if (cONFIG_LECTOR_RFID == null)
            {
                return HttpNotFound();
            }
            return View(cONFIG_LECTOR_RFID);
        }

        // POST: CONFIG_LECTOR_RFID/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CONFIG_LECTOR_RFID cONFIG_LECTOR_RFID = db.CONFIG_LECTOR_RFID.Find(id);
            db.CONFIG_LECTOR_RFID.Remove(cONFIG_LECTOR_RFID);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}


public class READER_MODE
{
    public int id { get; set; }
    public string  nombre { get; set; }

}