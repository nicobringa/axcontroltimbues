﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class DatoDWordDIntAD
    {
        public DATO_DWORDDINT add(DATO_DWORDDINT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    oContext.DATO_DWORDDINT.Add(oDato);
                    oContext.SaveChanges();
                    return oDato;
                }
            }
            catch (Exception ex)
            {
                return new DATO_DWORDDINT();
            }
        }

        public bool delete(DATO_DWORDDINT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.ID == oDato.ID).FirstOrDefault();
                    if (oConsulta != null)
                    {
                        oContext.DATO_DWORDDINT.Remove(oConsulta);
                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DATO_DWORDDINT update(DATO_DWORDDINT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_DWORDDINT
                                     where tabla.ID == oDato.ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FK_PLC = oDato.FK_PLC;
                        oConsulta.FLAG_RW = oDato.FLAG_RW;
                        oConsulta.NUM_WORD = oDato.NUM_WORD;
                        oConsulta.NUM_DB = oDato.NUM_DB;
                        oConsulta.QC = oDato.QC;
                        oConsulta.TAG = oDato.TAG;
                        oConsulta.VALOR = oDato.VALOR;

                        oContext.SaveChanges();
                        return oDato;
                    }
                    else
                    {
                        return new DATO_DWORDDINT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_DWORDDINT();
            }
        }

        public DATO_DWORDDINT getOne(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.ID == id).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_DWORDDINT();
            }
        }

        public DATO_DWORDDINT getOne(int idplc, string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.TAG == tag && a.FK_PLC == idplc).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_DWORDDINT();
            }
        }

        public List<DATO_DWORDDINT> getAll()
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_DWORDDINT>();
            }
        }

        public List<DATO_DWORDDINT> getAll(int idplc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.FK_PLC == idplc).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_DWORDDINT>();
            }
        }

        public List<DATO_DWORDDINT> getAllPorSector(int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_DWORDDINT>();
            }
        }

        public List<DATO_DWORDDINT> getAllPorSector(int idSector, int nroDB)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.ID_SECTOR == idSector && a.NUM_DB == nroDB).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_DWORDDINT>();
            }
        }

        public DATO_DWORDDINT leer(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.ID == id).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        if (oConsulta.FLAG_RW == true)
                        {
                            return oConsulta;
                        }
                        else
                        {
                            return new DATO_DWORDDINT();
                        }
                    }
                    else
                    {
                        return new DATO_DWORDDINT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_DWORDDINT();
            }
        }

        public DATO_DWORDDINT escribir(int id, int valor)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_DWORDDINT
                                     where tabla.ID == id
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FLAG_RW = true;
                         oConsulta.VALOR = valor;

                         oContext.SaveChanges();
                         return oConsulta;

                    }
                    else
                    {
                        return new DATO_DWORDDINT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_DWORDDINT();
            }
        }

        public List<DATO_DWORDDINT> getAllTags(int idplc, string nombreTag, int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_DWORDDINT.Where(a => a.FK_PLC == idplc && a.TAG.Contains(nombreTag) && a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_DWORDDINT>();
            }
        }
    }
}
