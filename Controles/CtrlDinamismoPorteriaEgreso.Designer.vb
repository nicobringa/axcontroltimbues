﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoPorteriaEgreso
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.picBarreraArriba_Egreso = New System.Windows.Forms.PictureBox()
        Me.picBarreraAbajo_Egreso = New System.Windows.Forms.PictureBox()
        Me.picSensor_EgresoNoCortando = New System.Windows.Forms.PictureBox()
        Me.picSensor_EgresoCortando = New System.Windows.Forms.PictureBox()
        Me.lblFallaBEgreso = New System.Windows.Forms.Label()
        CType(Me.picBarreraArriba_Egreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarreraAbajo_Egreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_EgresoNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensor_EgresoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picBarreraArriba_Egreso
        '
        Me.picBarreraArriba_Egreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Salida_Despacho_arriba
        Me.picBarreraArriba_Egreso.Location = New System.Drawing.Point(263, 70)
        Me.picBarreraArriba_Egreso.Name = "picBarreraArriba_Egreso"
        Me.picBarreraArriba_Egreso.Size = New System.Drawing.Size(61, 335)
        Me.picBarreraArriba_Egreso.TabIndex = 4
        Me.picBarreraArriba_Egreso.TabStop = False
        '
        'picBarreraAbajo_Egreso
        '
        Me.picBarreraAbajo_Egreso.BackgroundImage = Global.Controles.My.Resources.Resources.Barrera_Salida_Despacho_abajo
        Me.picBarreraAbajo_Egreso.Location = New System.Drawing.Point(0, 322)
        Me.picBarreraAbajo_Egreso.Name = "picBarreraAbajo_Egreso"
        Me.picBarreraAbajo_Egreso.Size = New System.Drawing.Size(320, 95)
        Me.picBarreraAbajo_Egreso.TabIndex = 5
        Me.picBarreraAbajo_Egreso.TabStop = False
        '
        'picSensor_EgresoNoCortando
        '
        Me.picSensor_EgresoNoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Salida_Despacho_nocortandoCS
        Me.picSensor_EgresoNoCortando.Location = New System.Drawing.Point(68, 351)
        Me.picSensor_EgresoNoCortando.Name = "picSensor_EgresoNoCortando"
        Me.picSensor_EgresoNoCortando.Size = New System.Drawing.Size(154, 75)
        Me.picSensor_EgresoNoCortando.TabIndex = 14
        Me.picSensor_EgresoNoCortando.TabStop = False
        '
        'picSensor_EgresoCortando
        '
        Me.picSensor_EgresoCortando.BackgroundImage = Global.Controles.My.Resources.Resources.Sensor_Salida_Despacho_cortandoCS
        Me.picSensor_EgresoCortando.Location = New System.Drawing.Point(67, 351)
        Me.picSensor_EgresoCortando.Name = "picSensor_EgresoCortando"
        Me.picSensor_EgresoCortando.Size = New System.Drawing.Size(154, 79)
        Me.picSensor_EgresoCortando.TabIndex = 15
        Me.picSensor_EgresoCortando.TabStop = False
        '
        'lblFallaBEgreso
        '
        Me.lblFallaBEgreso.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaBEgreso.AutoSize = True
        Me.lblFallaBEgreso.BackColor = System.Drawing.Color.Red
        Me.lblFallaBEgreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaBEgreso.ForeColor = System.Drawing.Color.White
        Me.lblFallaBEgreso.Location = New System.Drawing.Point(268, 385)
        Me.lblFallaBEgreso.Name = "lblFallaBEgreso"
        Me.lblFallaBEgreso.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaBEgreso.TabIndex = 99
        Me.lblFallaBEgreso.Text = "Falla"
        Me.lblFallaBEgreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlDinamismoPorteriaEgreso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.Despacho_Egreso
        Me.Controls.Add(Me.lblFallaBEgreso)
        Me.Controls.Add(Me.picSensor_EgresoCortando)
        Me.Controls.Add(Me.picSensor_EgresoNoCortando)
        Me.Controls.Add(Me.picBarreraAbajo_Egreso)
        Me.Controls.Add(Me.picBarreraArriba_Egreso)
        Me.Name = "CtrlDinamismoPorteriaEgreso"
        Me.Size = New System.Drawing.Size(319, 503)
        CType(Me.picBarreraArriba_Egreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarreraAbajo_Egreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_EgresoNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensor_EgresoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picBarreraArriba_Egreso As PictureBox
    Friend WithEvents picBarreraAbajo_Egreso As PictureBox
    Friend WithEvents picSensor_EgresoNoCortando As PictureBox
    Friend WithEvents picSensor_EgresoCortando As PictureBox
    Friend WithEvents lblFallaBEgreso As Label
End Class
