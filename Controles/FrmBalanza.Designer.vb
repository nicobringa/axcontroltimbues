﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBalanza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTiempoEstabilidad = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDifEstabilidad = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPesoMaximo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnResetComando = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pnlPesoMenorDifEstabilidad = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.pnlPesoMayor500kg = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.pnlErrorBalanza = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pnlPesoMaximo = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.pnlPesoEstable = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.pnlFueraEquilibrio = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.pnlFueraRango = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.pnlPesoNegativo = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.pnlPesoNeto = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnDeshabilitarComando = New System.Windows.Forms.Button()
        Me.btnHabilitarComando = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(364, 156)
        Me.Label8.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 16)
        Me.Label8.TabIndex = 95
        Me.Label8.Text = "(Seg)"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTiempoEstabilidad
        '
        Me.txtTiempoEstabilidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTiempoEstabilidad.Location = New System.Drawing.Point(281, 156)
        Me.txtTiempoEstabilidad.Name = "txtTiempoEstabilidad"
        Me.txtTiempoEstabilidad.Size = New System.Drawing.Size(77, 20)
        Me.txtTiempoEstabilidad.TabIndex = 87
        Me.txtTiempoEstabilidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(16, 153)
        Me.Label3.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(242, 20)
        Me.Label3.TabIndex = 86
        Me.Label3.Text = "Tiempo de espera de estabilidad:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDifEstabilidad
        '
        Me.txtDifEstabilidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDifEstabilidad.Location = New System.Drawing.Point(281, 130)
        Me.txtDifEstabilidad.Name = "txtDifEstabilidad"
        Me.txtDifEstabilidad.Size = New System.Drawing.Size(77, 20)
        Me.txtDifEstabilidad.TabIndex = 85
        Me.txtDifEstabilidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(16, 127)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 20)
        Me.Label2.TabIndex = 84
        Me.Label2.Text = "Diferencia de estabilidad:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtPesoMaximo
        '
        Me.txtPesoMaximo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPesoMaximo.Location = New System.Drawing.Point(281, 104)
        Me.txtPesoMaximo.Name = "txtPesoMaximo"
        Me.txtPesoMaximo.Size = New System.Drawing.Size(77, 20)
        Me.txtPesoMaximo.TabIndex = 83
        Me.txtPesoMaximo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(16, 104)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 20)
        Me.Label1.TabIndex = 82
        Me.Label1.Text = "Peso maximo: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(1, 0)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(425, 39)
        Me.lblTitulo.TabIndex = 96
        Me.lblTitulo.Text = "Balanza EgresoIngreso"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnHabilitarComando)
        Me.GroupBox1.Controls.Add(Me.btnDeshabilitarComando)
        Me.GroupBox1.Controls.Add(Me.btnResetComando)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 47)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(403, 56)
        Me.GroupBox1.TabIndex = 130
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Comandos"
        '
        'btnResetComando
        '
        Me.btnResetComando.BackColor = System.Drawing.Color.Gray
        Me.btnResetComando.FlatAppearance.BorderSize = 0
        Me.btnResetComando.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnResetComando.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnResetComando.ForeColor = System.Drawing.Color.White
        Me.btnResetComando.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnResetComando.Location = New System.Drawing.Point(269, 19)
        Me.btnResetComando.Name = "btnResetComando"
        Me.btnResetComando.Size = New System.Drawing.Size(84, 29)
        Me.btnResetComando.TabIndex = 157
        Me.btnResetComando.Text = "&Reset"
        Me.btnResetComando.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnAceptar.FlatAppearance.BorderSize = 0
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnAceptar.ForeColor = System.Drawing.Color.White
        Me.btnAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAceptar.Location = New System.Drawing.Point(132, 188)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(159, 44)
        Me.btnAceptar.TabIndex = 129
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(367, 106)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 16)
        Me.Label6.TabIndex = 92
        Me.Label6.Text = "(Tn)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(367, 130)
        Me.Label4.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 16)
        Me.Label4.TabIndex = 131
        Me.Label4.Text = "(kg)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.Add(Me.pnlPesoMenorDifEstabilidad)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.pnlPesoMayor500kg)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.pnlErrorBalanza)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.pnlPesoMaximo)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.pnlPesoEstable)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.pnlFueraEquilibrio)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.pnlFueraRango)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.pnlPesoNegativo)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.pnlPesoNeto)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Location = New System.Drawing.Point(12, 238)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(403, 67)
        Me.Panel1.TabIndex = 154
        '
        'pnlPesoMenorDifEstabilidad
        '
        Me.pnlPesoMenorDifEstabilidad.BackColor = System.Drawing.Color.Gray
        Me.pnlPesoMenorDifEstabilidad.Location = New System.Drawing.Point(248, 48)
        Me.pnlPesoMenorDifEstabilidad.Name = "pnlPesoMenorDifEstabilidad"
        Me.pnlPesoMenorDifEstabilidad.Size = New System.Drawing.Size(17, 12)
        Me.pnlPesoMenorDifEstabilidad.TabIndex = 164
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(5, 45)
        Me.Label16.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(237, 15)
        Me.Label16.TabIndex = 163
        Me.Label16.Text = "Peso menor a la diferencia de estabilidad:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlPesoMayor500kg
        '
        Me.pnlPesoMayor500kg.BackColor = System.Drawing.Color.Gray
        Me.pnlPesoMayor500kg.Location = New System.Drawing.Point(383, 48)
        Me.pnlPesoMayor500kg.Name = "pnlPesoMayor500kg"
        Me.pnlPesoMayor500kg.Size = New System.Drawing.Size(17, 12)
        Me.pnlPesoMayor500kg.TabIndex = 162
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(266, 45)
        Me.Label15.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(115, 15)
        Me.Label15.TabIndex = 161
        Me.Label15.Text = "Peso mayor 500 kg:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlErrorBalanza
        '
        Me.pnlErrorBalanza.BackColor = System.Drawing.Color.Gray
        Me.pnlErrorBalanza.Location = New System.Drawing.Point(44, 12)
        Me.pnlErrorBalanza.Name = "pnlErrorBalanza"
        Me.pnlErrorBalanza.Size = New System.Drawing.Size(17, 12)
        Me.pnlErrorBalanza.TabIndex = 152
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(5, 9)
        Me.Label10.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 15)
        Me.Label10.TabIndex = 160
        Me.Label10.Text = "Error:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlPesoMaximo
        '
        Me.pnlPesoMaximo.BackColor = System.Drawing.Color.Gray
        Me.pnlPesoMaximo.Location = New System.Drawing.Point(318, 30)
        Me.pnlPesoMaximo.Name = "pnlPesoMaximo"
        Me.pnlPesoMaximo.Size = New System.Drawing.Size(17, 12)
        Me.pnlPesoMaximo.TabIndex = 157
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(230, 27)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 15)
        Me.Label5.TabIndex = 156
        Me.Label5.Text = "Peso maximo:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlPesoEstable
        '
        Me.pnlPesoEstable.BackColor = System.Drawing.Color.Gray
        Me.pnlPesoEstable.Location = New System.Drawing.Point(372, 12)
        Me.pnlPesoEstable.Name = "pnlPesoEstable"
        Me.pnlPesoEstable.Size = New System.Drawing.Size(17, 12)
        Me.pnlPesoEstable.TabIndex = 155
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(292, 9)
        Me.Label7.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(81, 15)
        Me.Label7.TabIndex = 154
        Me.Label7.Text = "Peso estable:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlFueraEquilibrio
        '
        Me.pnlFueraEquilibrio.BackColor = System.Drawing.Color.Gray
        Me.pnlFueraEquilibrio.Location = New System.Drawing.Point(206, 30)
        Me.pnlFueraEquilibrio.Name = "pnlFueraEquilibrio"
        Me.pnlFueraEquilibrio.Size = New System.Drawing.Size(17, 12)
        Me.pnlFueraEquilibrio.TabIndex = 153
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(95, 27)
        Me.Label11.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(113, 15)
        Me.Label11.TabIndex = 152
        Me.Label11.Text = "Fuera de equilibrio:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlFueraRango
        '
        Me.pnlFueraRango.BackColor = System.Drawing.Color.Gray
        Me.pnlFueraRango.Location = New System.Drawing.Point(269, 12)
        Me.pnlFueraRango.Name = "pnlFueraRango"
        Me.pnlFueraRango.Size = New System.Drawing.Size(17, 12)
        Me.pnlFueraRango.TabIndex = 151
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(176, 9)
        Me.Label12.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(94, 15)
        Me.Label12.TabIndex = 150
        Me.Label12.Text = "Fuera de rango:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlPesoNegativo
        '
        Me.pnlPesoNegativo.BackColor = System.Drawing.Color.Gray
        Me.pnlPesoNegativo.Location = New System.Drawing.Point(153, 12)
        Me.pnlPesoNegativo.Name = "pnlPesoNegativo"
        Me.pnlPesoNegativo.Size = New System.Drawing.Size(17, 12)
        Me.pnlPesoNegativo.TabIndex = 149
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(65, 9)
        Me.Label13.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 15)
        Me.Label13.TabIndex = 148
        Me.Label13.Text = "Peso negativo:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlPesoNeto
        '
        Me.pnlPesoNeto.BackColor = System.Drawing.Color.Gray
        Me.pnlPesoNeto.Location = New System.Drawing.Point(72, 30)
        Me.pnlPesoNeto.Name = "pnlPesoNeto"
        Me.pnlPesoNeto.Size = New System.Drawing.Size(17, 12)
        Me.pnlPesoNeto.TabIndex = 147
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(5, 27)
        Me.Label14.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 15)
        Me.Label14.TabIndex = 146
        Me.Label14.Text = "Peso neto:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnDeshabilitarComando
        '
        Me.btnDeshabilitarComando.BackColor = System.Drawing.Color.Red
        Me.btnDeshabilitarComando.FlatAppearance.BorderSize = 0
        Me.btnDeshabilitarComando.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeshabilitarComando.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnDeshabilitarComando.ForeColor = System.Drawing.Color.White
        Me.btnDeshabilitarComando.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeshabilitarComando.Location = New System.Drawing.Point(44, 19)
        Me.btnDeshabilitarComando.Name = "btnDeshabilitarComando"
        Me.btnDeshabilitarComando.Size = New System.Drawing.Size(102, 30)
        Me.btnDeshabilitarComando.TabIndex = 158
        Me.btnDeshabilitarComando.Text = "&Deshabilitar"
        Me.btnDeshabilitarComando.UseVisualStyleBackColor = False
        '
        'btnHabilitarComando
        '
        Me.btnHabilitarComando.BackColor = System.Drawing.Color.Green
        Me.btnHabilitarComando.FlatAppearance.BorderSize = 0
        Me.btnHabilitarComando.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHabilitarComando.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnHabilitarComando.ForeColor = System.Drawing.Color.White
        Me.btnHabilitarComando.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnHabilitarComando.Location = New System.Drawing.Point(153, 19)
        Me.btnHabilitarComando.Name = "btnHabilitarComando"
        Me.btnHabilitarComando.Size = New System.Drawing.Size(102, 30)
        Me.btnHabilitarComando.TabIndex = 159
        Me.btnHabilitarComando.Text = "&Habilitar"
        Me.btnHabilitarComando.UseVisualStyleBackColor = False
        '
        'FrmBalanza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(427, 312)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtTiempoEstabilidad)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDifEstabilidad)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPesoMaximo)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "FrmBalanza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmBalanza"
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label8 As Label
    Friend WithEvents txtTiempoEstabilidad As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtDifEstabilidad As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPesoMaximo As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblTitulo As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnAceptar As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents pnlErrorBalanza As Panel
    Friend WithEvents Label10 As Label
    Friend WithEvents pnlPesoMaximo As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents pnlPesoEstable As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents pnlFueraEquilibrio As Panel
    Friend WithEvents Label11 As Label
    Friend WithEvents pnlFueraRango As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents pnlPesoNegativo As Panel
    Friend WithEvents Label13 As Label
    Friend WithEvents pnlPesoNeto As Panel
    Friend WithEvents Label14 As Label
    Friend WithEvents pnlPesoMayor500kg As Panel
    Friend WithEvents Label15 As Label
    Friend WithEvents pnlPesoMenorDifEstabilidad As Panel
    Friend WithEvents Label16 As Label
    Friend WithEvents btnResetComando As Button
    Friend WithEvents btnDeshabilitarComando As Button
    Friend WithEvents btnHabilitarComando As Button
End Class
