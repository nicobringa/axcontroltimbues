﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
namespace AccesoDatos
{
  public  class ErrorAD
    {

        public Boolean add(DateTime fecha,string maquina, string formulario, string procedimiento, int linea, string descripcion)
        {
            try
            {
                AxControlORAEntities context = new AxControlORAEntities();

                AX_ERROR errorActual= new AX_ERROR();
                errorActual.FECHA_HORA = fecha;
                errorActual.PUESTO_TRABAJO = maquina;
                errorActual.FORMULARIO = formulario;
                errorActual.PROCEDIMIENTO = procedimiento;
                errorActual.LINEA =Convert.ToInt16(linea);
                errorActual.DESCRIPCION = descripcion;
           
                context.AX_ERROR.Add(errorActual);
                context.SaveChanges();
                return true;
            }
            catch (Exception )
            {

   
                return false;
            }
        }

    }
}
