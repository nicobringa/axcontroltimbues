﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDescarga
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblTAG = New System.Windows.Forms.Label()
        Me.lblCalle = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtHab_LecturaRFID_Salida = New System.Windows.Forms.TextBox()
        Me.txtCmd = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.EP = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblHabLectura = New System.Windows.Forms.Label()
        CType(Me.EP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTAG
        '
        Me.lblTAG.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTAG.Location = New System.Drawing.Point(0, 0)
        Me.lblTAG.Name = "lblTAG"
        Me.lblTAG.Size = New System.Drawing.Size(382, 33)
        Me.lblTAG.TabIndex = 0
        Me.lblTAG.Text = "DESCARGA"
        Me.lblTAG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCalle
        '
        Me.lblCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalle.Location = New System.Drawing.Point(0, 33)
        Me.lblCalle.Name = "lblCalle"
        Me.lblCalle.Size = New System.Drawing.Size(382, 33)
        Me.lblCalle.TabIndex = 1
        Me.lblCalle.Text = "CALLE"
        Me.lblCalle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(211, 33)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Hab_LecturaRFID_Salida:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtHab_LecturaRFID_Salida
        '
        Me.txtHab_LecturaRFID_Salida.Location = New System.Drawing.Point(220, 74)
        Me.txtHab_LecturaRFID_Salida.Name = "txtHab_LecturaRFID_Salida"
        Me.txtHab_LecturaRFID_Salida.Size = New System.Drawing.Size(100, 20)
        Me.txtHab_LecturaRFID_Salida.TabIndex = 3
        '
        'txtCmd
        '
        Me.txtCmd.Location = New System.Drawing.Point(220, 107)
        Me.txtCmd.Name = "txtCmd"
        Me.txtCmd.Size = New System.Drawing.Size(100, 20)
        Me.txtCmd.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 99)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(211, 33)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "NuevoCamion"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(326, 105)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Enviar cmd"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'EP
        '
        Me.EP.ContainerControl = Me
        '
        'lblHabLectura
        '
        Me.lblHabLectura.AutoSize = True
        Me.lblHabLectura.Location = New System.Drawing.Point(326, 74)
        Me.lblHabLectura.Name = "lblHabLectura"
        Me.lblHabLectura.Size = New System.Drawing.Size(39, 13)
        Me.lblHabLectura.TabIndex = 7
        Me.lblHabLectura.Text = "Label3"
        '
        'CtrlDescarga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblHabLectura)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtCmd)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtHab_LecturaRFID_Salida)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCalle)
        Me.Controls.Add(Me.lblTAG)
        Me.Name = "CtrlDescarga"
        Me.Size = New System.Drawing.Size(429, 160)
        CType(Me.EP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTAG As Label
    Friend WithEvents lblCalle As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtHab_LecturaRFID_Salida As TextBox
    Friend WithEvents txtCmd As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents EP As ErrorProvider
    Friend WithEvents lblHabLectura As Label
End Class
