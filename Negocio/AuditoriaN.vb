﻿Public Class AuditoriaN
    Public Shared oBD As New AccesoDatos.AuditoriaAD

    ''' <summary>
    ''' Permite agregar una aditoria
    ''' </summary>
    ''' <param name="Descripcion">descripcion o suceso de la auditoria</param>
    ''' <param name="_Sector">Donde srugio el suceso</param>
    ''' <param name="idTrasaccion">id Trasaccion que genero el suceso puede ser null(nothing)</param>
    ''' <remarks></remarks>
    Public Shared Function AddAuditoria(ByVal Descripcion As String, ByVal TagRFID As String, ByVal _Sector As Integer, ByVal idTrasaccion As Long?,
                                        ByVal usuario As String, ByVal accion As Entidades.Constante.acciones, Optional ByVal idAuditoriaWS As Integer = 0) As Entidades.Auditoria
        Dim oAuditoria As New Entidades.Auditoria

        oAuditoria.descripcion = Descripcion
        oAuditoria.ID_SECTOR = _Sector
        oAuditoria.fecha = DateTime.Now
        oAuditoria.ID_TRANSACCION = idTrasaccion
        oAuditoria.usuario = usuario
        oAuditoria.accion = accion
        oAuditoria.TAG_RFID = TagRFID
        oAuditoria.ID_AUDITORIA_WS = idAuditoriaWS

        Return oBD.add(oAuditoria)
    End Function


    Public Function GetAll(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, ByVal accion As Entidades.Constante.acciones) As List(Of Entidades.Auditoria)

        Return If(accion = Entidades.Constante.acciones.TODOS, oBD.getAll(FechaDesde, FechaHasta), oBD.getAll(FechaDesde, FechaHasta, accion))


    End Function


    Public Function GetAllGeneral() As List(Of Entidades.Auditoria)
        Return oBD.getAllGeneral()
    End Function


    Public Function GetFiltro(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, ByVal tagRFID As String, ByVal sector As Int32, ByVal transaccion As Int32, ByVal accion As Int32, ByVal descripcion As String) As List(Of Entidades.Auditoria)
        Return oBD.getFiltro(FechaDesde, FechaHasta, tagRFID, sector, transaccion, accion, descripcion)
    End Function

    Public Function GetAllTraccion(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, ByVal Trasaccion As Integer) As List(Of Entidades.Auditoria)

        Return oBD.getAllTraccion(FechaDesde, FechaHasta, Trasaccion)


    End Function

    Public Function GetAllUsuario(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, ByVal Usuario As String) As List(Of Entidades.Auditoria)

        Return oBD.getAllUsuario(FechaDesde, FechaHasta, Usuario)


    End Function

    Public Function GetAllTagRFID(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, ByVal TagRFID As String) As List(Of Entidades.Auditoria)

        Return oBD.getAllTagRFID(FechaDesde, FechaHasta, TagRFID)


    End Function


    Public Function GetAllDescripcion(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, ByVal descripcion As String) As List(Of Entidades.Auditoria)

        Return oBD.getAllDescripcion(FechaDesde, FechaHasta, descripcion)



    End Function

    Public Function GetAllPorSector(ByVal idSector As String) As List(Of Entidades.Auditoria)
        Return oBD.getAllPorSector(idSector)
    End Function

End Class
