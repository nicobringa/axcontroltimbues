﻿Public Class CtrlAutomationObjectsPorteria

    ''' <summary>
    ''' Permite ocultar el dinamismo de la barrera de ingreso y sus controles
    ''' </summary>
    Private _EnableBarreraIngreso As Boolean
    Public Property EnableBarreraIngreo() As Boolean
        Get
            Return _EnableBarreraIngreso
        End Get
        Set(ByVal value As Boolean)
            _EnableBarreraIngreso = value
        End Set
    End Property

    ''' <summary>
    ''' Permite ocultar el dinamismo de la barrera de egreso y sus controles
    ''' </summary>
    Private _EnableBarreraEgreso As Boolean
    Public Property EnableBarreraEgreso() As Boolean
        Get
            Return _EnableBarreraEgreso
        End Get
        Set(ByVal value As Boolean)
            _EnableBarreraEgreso = value
        End Set
    End Property

End Class
