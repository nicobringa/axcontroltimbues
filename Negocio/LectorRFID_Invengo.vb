﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades
Imports System.Drawing
Imports System.Threading

Imports Core = Invengo.NetAPI.Core
Imports IRP1 = Invengo.NetAPI.Protocol.IRP1

Public Class LectorRFID_Invengo

#Region "Propiedades"

    Dim DireccionArchivo As String = My.Application.Info.DirectoryPath 'Entidades.Constante.CARPETA_CONFIGURACION

    Public Shared COLOR_DESCONECTADO As Color = Color.Red
    Public Shared COLOR_CONECTADO As Color = Color.LimeGreen
    Public Shared COLOR_SIN_CONFIGURACION As Color = Color.LightYellow


    Public Shared COLOR_START As Color = Color.Blue
    Public Shared COLOR_STOP As Color = Color.Red

    Public Shared TEXT_CONECTADO As String = "Conectado"
    Public Shared TEXT_DESCONECTADO As String = "Desconectado"
    Public Shared TEXT_SIN_CONFIGURACION As String = "Sin Configuración"
    Public Shared TEXT_START As String = "Start"
    Public Shared TEXT_STOP As String = "Stop"

    Private _Nombre As String

    Public readerInvengo As IRP1.Reader
    Private scanMsgInvengo As New IRP1.ReadTag(IRP1.ReadTag.ReadMemoryBank.EPC_TID_UserData_6C)
    Dim byteArr() As Byte = New Byte((8) - 1) {}

    Public list() As Double
    Public ValoresPW As New List(Of String)()
    Public PowerValue As String

    Public EPC_Invengo As String = Nothing
    Public TID_Invengo As String = Nothing
    Public RSSI_Invengo As String = Nothing

    Public isReading As Boolean = False

    Private _tagEPC As String

    Private hiloControlEstadoInvengo As Thread

    Public Property tagEPC() As String
        Get
            Return _tagEPC
        End Get
        Set(ByVal value As String)
            _tagEPC = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Private _Ip As String
    Public Property Ip() As String
        Get
            Return _Ip
        End Get
        Set(ByVal value As String)
            _Ip = value
        End Set
    End Property

    Private _LectorRFID As Entidades.Lector_RFID
    Public Property LectorRFID() As Entidades.Lector_RFID
        Get
            Return _LectorRFID
        End Get
        Set(ByVal value As Entidades.Lector_RFID)
            _LectorRFID = value
        End Set
    End Property

    Private _AutoConectarse As Boolean

    Public Property AUTO_CONECTARSE() As Boolean
        Get
            Return _AutoConectarse
        End Get
        Set(ByVal value As Boolean)
            _AutoConectarse = value
        End Set
    End Property

    Private _AutoStart As Boolean

    Public Property AutoStart() As Boolean
        Get
            Return _AutoStart
        End Get
        Set(ByVal value As Boolean)
            _AutoStart = value
        End Set
    End Property


    Private _LectorConfigurado As Boolean

    Public Property LectorConfigurado() As Boolean
        Get
            Return _LectorConfigurado
        End Get
        Set(ByVal value As Boolean)
            _LectorConfigurado = value
        End Set
    End Property

    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

#End Region

#Region "Enum"

    ''' <summary>
    ''' Los tipos de estado de conexion que tiene el lector RFID
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum EstadoConexion

        Conectado = 1
        Desconectado = 2
        SinConfiguracion = 3

    End Enum

    ''' <summary>
    ''' Estado de lectura del lector RFID
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum EstadoLectura

        Start = 1
        xStop = 2

    End Enum

#End Region

#Region "Mis Eventos"

    ''' <summary>
    ''' Evento que se ejecuta cuando la antena del lector leyo un TAG
    ''' </summary>
    ''' <param name="EPC">Numer de epc</param>
    ''' <param name="TID">Numer de tid</param>
    ''' <remarks></remarks>
    Public Event TagLeido(ByVal EPC As String, ByVal TID As String, ByVal LectorRFID As Entidades.Lector_RFID, Manual As Boolean)

    Public Event ErrorRFID(Msj As String, Invengo As LectorRFID_Invengo)

    Public Event InfoRFID(Msj As String, Invengo As LectorRFID_Invengo)

    Public Event MsjEstadoConexion(ByVal Msj As String, ByVal LectorRFID As Entidades.Lector_RFID)

#End Region

#Region "Contructor"

    Public Sub New(ByVal oLectorRFID As Entidades.Lector_RFID)

        If Not IsNothing(oLectorRFID) Then ' si encontro una configuración

            'Paso lo datos al objeto

            Me.Ip = oLectorRFID.ip + ":7086"
            Me.Nombre = oLectorRFID.nombre
            Me.LectorRFID = oLectorRFID
            LectorConfigurado = True

        Else 'No tiene un configuración establecida el lector

            LectorConfigurado = False

        End If

    End Sub

#End Region

#Region "Metodos"

    'Public Overridable Sub GenerarAlarma(detalle As String, idControlAcceso As Integer, idSector As Integer, objeto As String)
    '    Dim nAlarmas As New Negocio.AlarmaN

    '    'Si existe la alarma no la genera de vuelta
    '    If nAlarmas.ExisteAlarma(idSector, idControlAcceso, objeto, detalle) Then Return
    '    Dim oAlarma As New Entidades.Alarma

    '    oAlarma.activa = True
    '    oAlarma.reconocida = False
    '    oAlarma.fechaAparicion = DateTime.Now
    '    oAlarma.tag = "LECTOR INVENGO"
    '    oAlarma.idSector = idSector
    '    oAlarma.idControlAcceso = idControlAcceso
    '    oAlarma.detalle = detalle

    '    nAlarmas.GenerarAlarma(detalle, idControlAcceso, idSector, objeto)
    'End Sub
    ''' <summary>
    ''' Permite modificar el estado de una alarma
    ''' </summary>
    ''' <param name="idControlAcceso"></param>
    ''' <param name="tipoFalla"></param>
    ''' <param name="idSector"></param>
    ''' <param name="objeto"></param>
    'Public Overridable Sub ModificarAlarma(idControlAcceso As Integer, tipoFalla As Integer, idSector As Integer, objeto As String)
    '    Dim nAlarmas As New Negocio.AlarmaN
    '    Dim oAlarma As New Entidades.Alarma
    '    Dim idAlarma As Int32
    '    idAlarma = nAlarmas.BuscarIdAlarma(idSector, idControlAcceso, tipoFalla, objeto)
    '    If idAlarma > 0 Then
    '        oAlarma = nAlarmas.GetOne(idAlarma)

    '        oAlarma.activa = False
    '        oAlarma.fechaDesaparicion = DateTime.Now

    '        nAlarmas.Guardar(oAlarma)
    '    End If
    'End Sub


    Public Sub ConsultarEstadoInvengo()
        hiloControlEstadoInvengo = New Thread(AddressOf Monitoreo)
        hiloControlEstadoInvengo.IsBackground = True
        hiloControlEstadoInvengo.Start()
    End Sub

    Public Sub Monitoreo()
        'While True
        '    If Not isConnected() Then
        '        Try
        '            RaiseEvent MsjEstadoConexion("Conectando...", Me._LectorRFID)
        '            Conectar()

        '            If isConnected() Then
        '                RaiseEvent MsjEstadoConexion("Conectado.", Me._LectorRFID)
        '                StartRead()
        '                RaiseEvent MsjEstadoConexion("Leyendo.", Me._LectorRFID)

        '            End If
        '        Catch ex As Exception
        '            RaiseEvent MsjEstadoConexion(ex.ToString(), Me._LectorRFID)
        '        End Try
        '    Else
        '        RaiseEvent MsjEstadoConexion("Conectado.", Me._LectorRFID)
        '    End If
        '    Thread.Sleep(1000)
        'End While
    End Sub


    Public Function GuardarConf() As Boolean

        Me.LectorRFID.nombre = Me.Nombre
        Dim posIP As Integer = Me.Ip.IndexOf(":")
        Me.LectorRFID.ip = Mid(Me.Ip, 1, posIP)
        Me.LectorRFID.descripcion = Me.Descripcion
        Dim nLectorRFID As New Negocio.LectorRFID_N
        Return nLectorRFID.Update(LectorRFID)

    End Function


    ''' <summary>
    ''' Consulta el estado de conexión del lector
    ''' </summary>
    ''' <returns></returns>
    Public Function isConnected() As Boolean
        If Not IsNothing(readerInvengo) Then
            If readerInvengo.IsConnected = False Then
                'GenerarAlarma("El lector no se encuentra conectado", LectorRFID.idControlAcceso, LectorRFID.idControlAcceso, LectorRFID.control_acceso.nombre)
            End If
            Return readerInvengo.IsConnected
        Else
            Return False
            ' GenerarAlarma("El lector no se encuentra conectado", LectorRFID.idControlAcceso, LectorRFID.idControlAcceso, LectorRFID.control_acceso.nombre)
        End If
    End Function


    ''' <summary>
    ''' Obtiene error del lector
    ''' </summary>
    ''' <param name="e"></param>
    Public Sub Reader_OnApiException(ByVal e As Core.ErrInfo)

        MsgBox(e.Ei.ErrCode)
        MsgBox(e.Ei.ErrMsg)

    End Sub

    ''' <summary>
    ''' Permite que el lector se conecte con la PC
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Conectar()

        Try



            readerInvengo = New IRP1.Reader("Reader", "TCPIP_Client", Ip)
            If Not readerInvengo.IsConnected Then
                readerInvengo.Connect()
                isReading = False



            End If
        Catch ex As Exception

            RaiseEvent MsjEstadoConexion(ex.ToString(), Me._LectorRFID)

        End Try

    End Sub


    ''' <summary>
    ''' Permite que el lector comience a leer
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StartRead()
        If ((Not (readerInvengo) Is Nothing) _
            AndAlso readerInvengo.IsConnected AndAlso isReading = False) Then
            Dim a As Byte = &H80
            a += &H1
            scanMsgInvengo.Antenna = a
            If readerInvengo.Send(scanMsgInvengo) Then
                AddHandler readerInvengo.OnMessageNotificationReceived, AddressOf reader_OnMessageNotificationReceived



            End If
            isReading = True
        End If

    End Sub


    ''' <summary>
    ''' Evento que se dispara cuando se reciben los datos de la lectura
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub reader_OnMessageNotificationReceived(ByVal reader As Invengo.NetAPI.Core.BaseReader, ByVal msg As Invengo.NetAPI.Core.IMessageNotification)
        Dim msgType As String = msg.GetMessageType
        msgType = msgType.Substring((msgType.LastIndexOf(ChrW(46)) + 1))
        Select Case (msgType)
            Case "RXD_TagData"
                'Try
                Dim m As IRP1.RXD_TagData = CType(msg, IRP1.RXD_TagData)
                    Dim tagType As String = m.ReceivedMessage.TagType
                    EPC_Invengo = Core.Util.ConvertByteArrayToHexString(m.ReceivedMessage.EPC)
                    TID_Invengo = Core.Util.ConvertByteArrayToHexString(m.ReceivedMessage.TID)
                    RSSI_Invengo = Core.Util.ConvertByteArrayToHexString(m.ReceivedMessage.RSSI)
                    tagEPC = TID_Invengo
                    RaiseEvent TagLeido(EPC_Invengo, TID_Invengo, Me.LectorRFID, False)
                'Catch ex As Exception

                'End Try
        End Select
    End Sub



    ''' <summary>
    ''' Permite que el lector detenga la lectura
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StopRead()
        If readerInvengo.IsConnected() Then
            readerInvengo.Send(New IRP1.PowerOff())
            'Me.IsStar = False
            isReading = False
        End If
    End Sub

    ''' <summary>
    ''' Desconecta el lector (necesita volver a conectar)
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Disconnect()

        Try
            If readerInvengo.IsConnected() Then
                readerInvengo.Disconnect()
            End If
        Catch ex As Exception

        End Try

    End Sub


    ''' <summary>
    ''' Permite configurar el tiempo de lectura de ID repetidas
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CambiarTiempoLectura(intSegundos As Integer)

        If ((Not (readerInvengo) Is Nothing)) Then
            byteArr(1) = intSegundos * 10
            readerInvengo.Send(New IRP1.FilterByTime(0, byteArr))
        End If

    End Sub

    Public Sub DetectarConfig()

        Try
            Dim order As IRP1.SysQuery_800 = New IRP1.SysQuery_800(104, 0)
            readerInvengo.Send(order)
            list = New Double((order.ReceivedMessage.QueryData.Length) - 1) {}
            Dim i As Integer = 0
            Dim order1 As IRP1.SysQuery_800 = New IRP1.SysQuery_800(101, 0)
            Do While (i < list.Length)
                list(i) = CType(i, Double)
                'power value list
                If ((order.ReceivedMessage.QueryData(i) <> 0) _
                            AndAlso (i < list.Length)) Then
                    ValoresPW.Add(list(i).ToString)
                End If
                i = (i + 1)
            Loop
            readerInvengo.Send(order1)
            PowerValue = list(order1.ReceivedMessage.QueryData(0)).ToString
            ''End If
        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Guarda la config en la base de datos y la aplica al lector
    ''' </summary>
    Public Sub AplicarConfig(PoderAntena As String, segundos As Integer)

        Try
            ''Potencia de Antena
            'Dim aData() As Byte = New Byte((2) - 1) {}
            'aData(0) = 0
            'aData(1) = CType(PoderAntena, Byte)
            'Dim order As IRP1.SysConfig_800 = New IRP1.SysConfig_800(101, aData)
            'readerInvengo.Send(order)

            ''Tiempo de lectura del mismo tag
            CambiarTiempoLectura(segundos)

            ''Modo de lectura


        Catch ex As Exception

        End Try

    End Sub

#End Region

End Class
