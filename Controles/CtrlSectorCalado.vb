﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading

Public Class CtrlSectorCalado

#Region "PROPIEDADES"


    Private _idSector As Integer = 0
    Public Property ID_SECTOR() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Private Sector As SECTOR
    Public WithEvents Buffer As Entidades.BufferTag
    Private UltIdNotificacion As Integer

    ''' <summary>
    ''' Evento que se genera cuando se realiza una lectura manual RFID
    ''' </summary>
    Public Event EventTagLeidoManual(ByVal oTagLeidoManual As TagLeidoManual)

    Public boEscrituraVariableCartel As Boolean = False
    Public dtEstampaTiempoEscrituraCartel As New DateTime
    Public inLargoMensaje As Integer = 0

#End Region

#Region "CONSTRUCTOR"

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Inicializar()
    End Sub

    Public Sub New(oSector As SECTOR)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.Sector = oSector
        _idSector = Me.Sector.ID_SECTOR
        Inicializar()
    End Sub

#End Region

#Region "Eventos FORM"

#End Region

#Region "METODOS"
    ''' <summary>
    ''' Metodo para incializar todas las variables de estado del objeto
    ''' </summary>
    Public Sub Inicializar()
        ' InitializeComponent()
        If Me.ID_SECTOR = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_SECTOR, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID un control de acceso")

            Return
        End If



        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN

        If IsNothing(Me.Sector) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_SECTOR, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el sector")
            Return

        End If
        Dim nombreSector As String = String.Format("{0} - ({1})", Me.Sector.NOMBRE, Me.Sector.ID_SECTOR)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreSector)

        inicializarCtrl()
        SetBuffer()
        SetVisualizar()

    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        'De cada sector traigo los TAG del plc que tiene el control de acceso
        Dim nDatoWord As New Negocio.DatoWordIntN
        Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(Me.Sector.ID_SECTOR)

        For Each itemDato As Entidades.DATO_WORDINT In listDatoWordInt

            Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

            itemDato.TAG = Constante.getNombreTag(itemDato.TAG)
            'Si el tag contiene el nombre del control
            If itemDato.TAG.Equals(S_LECTURA.Name) Then
                ctrlAutomatizacion = Me.S_LECTURA
            Else
                ctrlAutomatizacion = Nothing
            End If
            If Not IsNothing(ctrlAutomatizacion) Then
                'Si todavia no esta inicializado
                If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(itemDato.FK_PLC, Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, Me.Sector.ID_SECTOR) ' BARRERA DE INGRESO
            End If
        Next

        CARTEL_CALADO.Inicializar(Me.Sector.ID_SECTOR)
        'Escribo el msj por defecto al cartel apenas lo inicio
        CARTEL_CALADO.fnEscribirCartel(Constante.PasosCartelMultiled.TextoFijoPorDefecto, "")
    End Sub

    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <returns></returns>
    Public Function getBufferAntena() As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Me.Buffer
        Return Buffer
    End Function

    Private Sub SetBuffer()
        'Inicializo los buffer
        Me.Buffer = New Entidades.BufferTag()

        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.Buffer.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER()
    End Sub

    Private Sub SetVisualizar()
        'Negocio.modDelegado.setVisibleCtrl(Me, S_LECTURA_INGRESO, Me.ControlAcceso.VISIBLE_BAR1)
        'Negocio.modDelegado.setVisibleCtrl(Me, S_INGRESO, Me.ControlAcceso.VISIBLE_BAR1)
        'Negocio.modDelegado.setVisibleCtrl(Me, B_INGRESO, Me.ControlAcceso.VISIBLE_BAR1)

        'Negocio.modDelegado.setVisibleCtrl(Me, CartelLED_INGRESO, Me.ControlAcceso.VISIBLE_CARTEL)
        'Negocio.modDelegado.setVisibleCtrl(Me, CAM_INGRESO, Me.ControlAcceso.VISIBLE_CAM)
        'CtrlDinamismoPorteriaIngreso1.SetVisibleBarrera(Me.ControlAcceso.VISIBLE_BAR1)

    End Sub

    ''' <summary>
    ''' Procedimeinto que consulta al WS EstaHabilitado si el tag leido esta habilitado a ingresar o salir
    ''' dependiendo del sector
    ''' Y procesa el axRespuesta para realizar las operaciones necesarias
    ''' </summary>
    ''' <param name="TagRFID">Tag leido</param>
    Public Sub EstaHabilitado(TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            Dim tmp As String '= String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            'CtrlPanelNotificaciones1.MostrarNotificacion(Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp, Me.Sector.ID_SECTOR)
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)
            Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
            'Preginto si esta habilitado
            If axRespuesta.habilitado Then
                'Si esta habilitado
                'Muestro el mensaje en el cartel
                tmp = "[ " + DateTime.Now + " ] CAMIÓN HABILITADO: ID TRANSACCIÓN: " + axRespuesta.idTransaccion.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp, Me.Sector.ID_SECTOR)

                'Ejecuto el comando permitir ingresar en la barrera
                'If Not IsNothing(CtrlCamara) Then CtrlCamara.SacarFoto(axRespuesta.idTransaccion, ID_SECTOR)
                ' If Not IsNothing(Barrera) Then Barrera.EjectComando(BarreraComando.PermitirIngresar)
                nUltTagLeido.Delete(ID_SECTOR)
            Else
                tmp = "[ " + DateTime.Now + " ] CAMIÓN NO HABILITADO: " + axRespuesta.mensaje.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, TipoNotificacion.xErrorUser, tmp, Me.Sector.ID_SECTOR)

                'Guardo el ultimo tag leido
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)
            End If


        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp, Me.Sector.ID_SECTOR)
        End Try

    End Sub

    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub

    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        RaiseEvent EventTagLeidoManual(oTagLeidoManual)
        'TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub

    Public Function fnComprobarComandoCartel() As Boolean
        Try
            Dim nDatoWordInt As New DatoWordIntN
            Dim ComandoCartel = nDatoWordInt.GetOneTagIdSector("CARTEL.Comando", Me.Sector.ID_SECTOR)
            Dim EstadoCartel = nDatoWordInt.GetOneTagIdSector("CARTEL.Estado", Me.Sector.ID_SECTOR)
            If ComandoCartel.VALOR <> EstadoCartel.VALOR Then
                CARTEL_CALADO.fnEscribirCartel(ComandoCartel.VALOR, "")
                EstadoCartel.VALOR = ComandoCartel.VALOR
                EstadoCartel.FLAG_RW = True
                nDatoWordInt.Update(EstadoCartel)
                Return True
            End If
            Return False
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

#End Region

#Region "Eventos"

    Public Sub ClearBufferIngreso() Handles Buffer.ClearBuffer
        Me.CtrlAtenaLeyendo.BorrarUltimosTagLeido()
    End Sub

    Public Sub tiempoBufferIngreso(ByVal Tiempo As Integer) Handles Buffer.ShowTiempoBuffer
        Me.CtrlAtenaLeyendo.setTiempoBuffer(Tiempo)
    End Sub




#End Region

End Class
