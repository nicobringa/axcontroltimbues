﻿Imports System
Imports System.Configuration
Imports System.Windows.Forms

Public Class ArchivoAppConfig

#Region "Metodos"

    Public Function SetNombreServidorCadenaConexion(ByVal StrNombreSeccion As String, ByVal StrNombreServidor As String) As String
        Dim oArchivoConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location.ToString())
        Dim oSeccionCadenaConexion As ConnectionStringsSection = DirectCast(oArchivoConfiguration.GetSection("connectionStrings"), ConnectionStringsSection)
        Dim StrCadenaConexion As String = oSeccionCadenaConexion.ConnectionStrings(StrNombreSeccion).ConnectionString 'Ej:"HorasMarchaEntidades"

        Dim StrCadenaConexionParte1 As String = StrCadenaConexion.Substring(0, InStr(StrCadenaConexion, "server=", CompareMethod.Text) + 6)
        Dim StrCadenaConexionParte2 As String = ";" & StrCadenaConexion.Substring(InStr(StrCadenaConexion, ";user id=", CompareMethod.Text), Len(StrCadenaConexion) - InStr(StrCadenaConexion, ";user id=", CompareMethod.Text))

        'Especifico el nuevo valor de la cadena de conexion
        oSeccionCadenaConexion.ConnectionStrings(StrNombreSeccion).ConnectionString = StrCadenaConexionParte1 & StrNombreServidor & StrCadenaConexionParte2

        'Guardo los cambios en el archivo
        oArchivoConfiguration.Save()

        Return Me.GetNombreServidorCadenaConexion(StrNombreSeccion)
    End Function

    Public Function GetNombreServidorCadenaConexion(ByVal StrNombreSeccion As String) As String
        Dim oArchivoConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location.ToString())
        Dim oSeccionCadenaConexion As ConnectionStringsSection = DirectCast(oArchivoConfiguration.GetSection("connectionStrings"), ConnectionStringsSection)
        Dim StrCadenaConexion As String = oSeccionCadenaConexion.ConnectionStrings(StrNombreSeccion).ConnectionString 'Ej:"HorasMarchaEntidades"

        Dim StrNombreServidor As String = StrCadenaConexion.Substring(InStr(StrCadenaConexion, "server=", CompareMethod.Text) + 6, InStr(StrCadenaConexion, ";user id=", CompareMethod.Text) - (InStr(StrCadenaConexion, "server=", CompareMethod.Text) + 7))

        Return StrNombreServidor
    End Function

    Public Function GetClave(ByVal StrNombreClave As String) As String
        Dim oArchivoAppConfig As Configuration = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location.ToString()) 'Dim oConfiguracion As ConfigurationSettings = ConfigurationManager.OpenExeConfiguration(Application.StartupPath & "\WindowsApplication1.exe")

        ' Obtenemos el objeto AppSettingsSectiion
        Dim oAppSetting As AppSettingsSection = oArchivoAppConfig.AppSettings

        ' Leemos el valor del elemento Area
        Return oAppSetting.Settings.Item(StrNombreClave).Value
    End Function

    Public Function SetClave(ByVal StrNombreClave As String, ByVal StrValorClave As String) As String
        Dim oArchivoAppConfig As Configuration = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location.ToString()) 'Dim oConfiguracion As ConfigurationSettings = ConfigurationManager.OpenExeConfiguration(Application.StartupPath & "\WindowsApplication1.exe")

        ' Obtengo el objeto AppSettingsSectiion
        Dim oAppSetting As AppSettingsSection = oArchivoAppConfig.AppSettings

        'Establezco el nuevo valor
        oAppSetting.Settings.Item(StrNombreClave).Value = StrValorClave

        ' Guardo los valores del objeto Configuration en el archivo de configuración XML actual.
        oArchivoAppConfig.Save(ConfigurationSaveMode.Modified)

        'Retorno el nuevo valor
        Return Me.GetClave(StrNombreClave)
    End Function

    Public Sub SetCadenaConexion(ByVal StrNombreSeccion As String, ByVal CadenaConexion As String)
        Dim oArchivoConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath)
        Dim oSeccionCadenaConexion As ConnectionStringsSection = DirectCast(oArchivoConfiguration.GetSection("connectionStrings"), ConnectionStringsSection)
        Dim StrCadenaConexion As String = oSeccionCadenaConexion.ConnectionStrings(StrNombreSeccion).ConnectionString 'Ej:"ProduccionEntidades"

        Dim StrCadenaConexionParte1 As String = StrCadenaConexion.Substring(0, InStr(StrCadenaConexion, "connection string=", CompareMethod.Text) + "connection string=".Length)

        oSeccionCadenaConexion.ConnectionStrings(StrNombreSeccion).ConnectionString = StrCadenaConexionParte1 & CadenaConexion & """"

        oArchivoConfiguration.Save()
    End Sub

    Public Function GetCadenaConexion(ByVal StrNombreSeccion As String) As String
        Dim oArchivoConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath)
        Dim oSeccionCadenaConexion As ConnectionStringsSection = DirectCast(oArchivoConfiguration.GetSection("connectionStrings"), ConnectionStringsSection)
        Dim StrCadenaConexion As String = oSeccionCadenaConexion.ConnectionStrings(StrNombreSeccion).ConnectionString 'Ej:"ProduccionEntidades"

        StrCadenaConexion = StrCadenaConexion.Substring(InStr(StrCadenaConexion, "connection string=", CompareMethod.Text) + "connection string=".Length)
        StrCadenaConexion = StrCadenaConexion.Substring(0, StrCadenaConexion.Length - 1)

        Return StrCadenaConexion

    End Function

    ''' <summary>
    ''' Permite obtener el id del control de acceso configurador 
    ''' </summary>
    ''' <returns></returns>
    Public Function GetControlAccesoIdAppConfig() As Integer
        ' Return GetClave("Puesto_Trabajo")

        Dim idControlAcceso = System.Configuration.ConfigurationManager.AppSettings("ID_CONTROL_ACCESO")
        Dim int_idControlAcceso As Integer
        If Integer.TryParse(idControlAcceso, int_idControlAcceso) Then
            Return int_idControlAcceso
        Else
            Throw New ArgumentException("El idControlAcceso no es un NUMERO.")
        End If


    End Function
#End Region

End Class
