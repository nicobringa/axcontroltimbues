﻿Imports AccesoDatos
Imports Entidades

Public Class PlcN
    Public Function Add(ByVal oPLC As Plc) As Plc
        Dim oDatoAD As New PlcAD
        Return oDatoAD.add(oPLC)
    End Function

    Public Function Delete(ByVal oPLC As Plc) As Boolean
        Dim oDatoAD As New PlcAD
        Return oDatoAD.delete(oPLC)
    End Function

    Public Function Update(ByVal oPLC As Plc) As Plc
        Dim oDatoAD As New PlcAD
        Return oDatoAD.update(oPLC)
    End Function

    Public Function GetOne(ByVal id As Integer) As Plc
        Dim oDatoAD As New PlcAD
        Return oDatoAD.getOne(id)
    End Function

    Public Function GetOneIP(ByVal ip As String) As Plc
        Dim oDatoAD As New PlcAD
        Return oDatoAD.getOneIP(ip)
    End Function

    Public Function GetOne(ByVal nombre As String) As Plc
        Dim oDatoAD As New PlcAD
        Return oDatoAD.getOne(nombre)
    End Function

    Public Function GetAll() As List(Of Plc)
        Dim oDatoAD As New PlcAD
        Return oDatoAD.getAll
    End Function
End Class
