﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class T3CtrlControlAccesoCalado
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.flpSectores = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.SuspendLayout()
        '
        'flpSectores
        '
        Me.flpSectores.AutoScroll = True
        Me.flpSectores.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.flpSectores.Location = New System.Drawing.Point(0, 32)
        Me.flpSectores.Name = "flpSectores"
        Me.flpSectores.Size = New System.Drawing.Size(898, 538)
        Me.flpSectores.TabIndex = 184
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(857, -3)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(41, 35)
        Me.btnReset.TabIndex = 183
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(898, -3)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 573)
        Me.CtrlPanelNotificaciones1.TabIndex = 182
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(4, 0)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(152, 29)
        Me.btnLectorRfid.TabIndex = 181
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(898, 32)
        Me.lblNombre.TabIndex = 180
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 571)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(898, 110)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 179
        '
        'T3CtrlControlAccesoCalado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.flpSectores)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Name = "T3CtrlControlAccesoCalado"
        Me.Size = New System.Drawing.Size(1110, 688)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents flpSectores As FlowLayoutPanel
    Friend WithEvents btnReset As Button
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents btnLectorRfid As Button
    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
End Class
