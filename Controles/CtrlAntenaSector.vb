﻿Public Class CtrlAntenaSector

#Region "PROPIEDADES"
    Private oModeloLectorN As New Negocio.Modelo_LectorRfidN
#End Region
    Dim i, cantAntenas As Integer

    Dim lblNombreAntena(99) As Label
    Dim txtNombreSector(99) As TextBox
    Dim btnAsignar(99) As Button
    Dim modeloActual As Integer

#Region "CONSTRUCTOR"

#End Region

#Region "EVENTOS FORM"





#End Region


#Region "METODOS"



    Private Sub CargarControles()
        Try



            cantAntenas = oModeloLectorN.GetCantAnetnas(2)

            For i = 1 To cantAntenas
                ''Agrego un label, para mostrar el nombre de la antena a asignar
                lblNombreAntena(i) = New Label
                lblNombreAntena(i).Width = 77
                lblNombreAntena(i).Height = 17
                lblNombreAntena(i).AutoSize = False
                lblNombreAntena(i).BorderStyle = BorderStyle.None
                lblNombreAntena(i).Font = New Font("Arial", 10, FontStyle.Bold)
                lblNombreAntena(i).Location = New Point(7, i * 32)
                lblNombreAntena(i).Text = "Antena " & i
                Me.pnlAntenas.Controls.Add(lblNombreAntena(i))

                ''Muestro el nombre del sector, que se le está asignado a la antena
                txtNombreSector(i) = New TextBox
                txtNombreSector(i).Width = 176
                txtNombreSector(i).Height = 20
                txtNombreSector(i).AutoSize = False
                txtNombreSector(i).ReadOnly = True
                txtNombreSector(i).BorderStyle = BorderStyle.FixedSingle
                txtNombreSector(i).Font = New Font("Arial", 10, FontStyle.Bold)
                txtNombreSector(i).Location = New Point(85, i * 32)
                Me.pnlAntenas.Controls.Add(txtNombreSector(i))


                ''Agrego un botón, para realizar la asignación
                btnAsignar(i) = New Button
                btnAsignar(i).Width = 70
                btnAsignar(i).Height = 23
                btnAsignar(i).AutoSize = False
                btnAsignar(i).Font = New Font("Arial", 10, FontStyle.Bold)
                btnAsignar(i).Location = New Point(270, i * 32)
                btnAsignar(i).Text = "Asignar"

                AddHandler btnAsignar(i).Click, AddressOf btnAsignar_Click
                Me.pnlAntenas.Controls.Add(btnAsignar(i))



            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub btnAsignar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("Hiciste Click en el botón" & modeloActual)

    End Sub

#End Region

#Region "SUBPROCESOS"

#End Region





End Class
