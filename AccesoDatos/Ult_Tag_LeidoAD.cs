﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class Ult_Tag_LeidoAD
    {
        public ULTIMO_TAG_LEIDO add(Entidades.ULTIMO_TAG_LEIDO oUltTagLeido )
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    oContext.ULTIMO_TAG_LEIDO.Add(oUltTagLeido);
                    oContext.SaveChanges();
                    return oUltTagLeido;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Boolean delete(int idSector)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.ULTIMO_TAG_LEIDO.Where(u => u.ID_SECTOR == idSector).ToList();

                    oContext.ULTIMO_TAG_LEIDO.RemoveRange(oConsulta);
                    oContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Permite obtener el ultimo tag leido de un sector
        /// </summary>
        /// <param name="idSector"></param>
        /// <returns></returns>
        public ULTIMO_TAG_LEIDO getLastTag(int idSector)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.ULTIMO_TAG_LEIDO.Where(u => u.ID_SECTOR == idSector).
                        OrderByDescending(u => u.FECHA).FirstOrDefault();

                    return oConsulta;
                    
                  
                }
            }
            catch (Exception ex)
            {
                return null; 
            }
        }

        public string ultimoTag()
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.ULTIMO_TAG_LEIDO.
                        OrderByDescending(u => u.FECHA).FirstOrDefault();

                    return oConsulta.TAG_RFID;


                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
