﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Impinj.OctaneSdk
Public Class CtrlControlAccesoTicket2Calles

#Region "PROPIEDADES"
    Private ControlAcceso As Entidades.Control_Acceso
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private hMonitoreo As Thread
    Private WithEvents BufferCall1 As Entidades.BufferTag
    Private WithEvents BufferCall2 As Entidades.BufferTag
    Private oLector As New Entidades.Lector_RFID
    Private nLector As New Negocio.LectorRFID_N
#End Region

#Region "CONSTRUCTOR"
    Public Sub New(ID_CONTROL_ACCESO As Integer, ID_PLC As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_PLC = ID_PLC

    End Sub
    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

    End Sub

#End Region

#Region "EVENTOS FORM"
    Private Sub btnLectorRfid_Click(sender As Object, e As EventArgs) Handles btnLectorRfid.Click

        Dim frm As New FrmConfLectoresRFID(Me.ListRFID_SpeedWay)
        frm.ShowDialog()


    End Sub
#End Region

#Region "METODOS"
    Public Overrides Sub Inicializar()

        If Me.ID_CONTROL_ACCESO = 0 Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
                Me.Name + ": Se debe asignar un ID de puesto de trabajo")

            Return
        End If

        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)



        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If

        Dim nombreControlAcceso As String = String.Format("{0} - ({1})", Me.ControlAcceso.NOMBRE, Me.ControlAcceso.ID_CONTROL_ACCESO)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreControlAcceso)
        'Si el puesto de trabajo es servidor
        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            'Inicializo como servidor
            IniServidor()
        Else
            IniCliente()
        End If

        'Ejecuto el hilo de monitoreo
        If Not IsNothing(hMonitoreo) Then
            If hMonitoreo.IsAlive Then hMonitoreo.Abort()
            hMonitoreo = Nothing
        End If

        inicializarCtrl()
        SetBuffer()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()


    End Sub

    ''' <summary>
    ''' Inicializar el control de acceso como servidor
    ''' </summary>
    Public Sub IniServidor()
        'Cargo los lectores RFID
        CargarLectoresRFID()

    End Sub

    Public Sub IniCliente()
        CargarLectoresRFID()
        modDelegado.setVisibleCtrl(Me, btnLectorRfid, False)
    End Sub



    Private Sub CargarLectoresRFID()
        'Busco los lectores
        Dim nLectorEFID As New Negocio.LectorRFID_N

        Dim listLectorRFID As List(Of Entidades.LECTOR_RFID) = nLectorEFID.GetAll(Me.ID_CONTROL_ACCESO)
        'listLectorRFID.Clear()

        If IsNothing(listLectorRFID) Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
                "No tiene lectores RFID asginados")

            Return 'FIN DE PROCEDIMIENTO
        ElseIf listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
                "No tiene lectores RFID asginados")
            Return 'FIN DE PROCEDIMIENTO
        End If

        If listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
                "No tiene lectores RFID asginados")

            Return 'FIN 
        End If

        Me.ListRFID_SpeedWay = Nothing
        'Recorro los lectores agregados para el puesto de trabajo
        For Each LectorRFID As Entidades.LECTOR_RFID In listLectorRFID
            'Creo el lector RFID SpeedWay para su utilización
            Dim RFID_SpeedWay As New Negocio.LectorRFID_SpeedWay(LectorRFID)

            If (IsNothing(Me.ListRFID_SpeedWay)) Then Me.ListRFID_SpeedWay = New List(Of Negocio.LectorRFID_SpeedWay)
            'Agrego el LectorRFID a la lista
            Me.ListRFID_SpeedWay.Add(RFID_SpeedWay)
            'Agrego los escuchadores para los evento del lector
            AddHandler RFID_SpeedWay.TagLeido, AddressOf Me.TagLeido
            AddHandler RFID_SpeedWay.ErrorRFID, AddressOf Me.ErrorRFID
            AddHandler RFID_SpeedWay.InfoRFID, AddressOf Me.InfoRFID


        Next

        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            CtrlEstadoLectoresRFID1.CargarLectores(Me.ListRFID_SpeedWay)
        Else
            CtrlEstadoLectoresRFID1.CargarLectoresCliente(listLectorRFID)
        End If
    End Sub

    Private Sub ConectarLectoresRFID()

        'Recorro los lectores RFID Configurado
        For Each RFID_SpeedWay As LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            'Si el lector no esta conectado
            If Not RFID_SpeedWay.IsConnected Then

                Try

                    oLector = nLector.GetOne(RFID_SpeedWay.Ip)
                    oLector.CONECTADO = RFID_SpeedWay.Conectarse()
                    nLector.Update(oLector)

                Catch ex As Exception
                    'Surgio un error en la conexion
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, ex.Message)
                End Try
            Else
                'El lector ya se encuentra conectado
                Dim tmp As String = String.Format("El lector {0} se encuentra conectado", RFID_SpeedWay.Nombre)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Permite regrescar el dinamismo de todo el control
    ''' </summary>
    Private Sub RefrescarDinamismo()

        If S1.Inicializado And Me.S1.GPIO Then
            S1.SetEstadoPLC()
            CtrlDinamismoTicket2Calles1.SetSensorEstado(1, S1.oEstado.valor)
        ElseIf Me.S1.GPIO = True Then ' Trabaja con GPIO
            Dim oSpeedWay As LectorRFID_SpeedWay
            oSpeedWay = GetLectorRFID_SpeedWay()
            If Not IsNothing(oSpeedWay) Then
                S1.SetEstadoGpio(oSpeedWay, TipoConexion.NormalCerrado)
                CtrlDinamismoTicket2Calles1.SetSensorEstado(1, Me.S1.oEstado.valor)
            End If
        End If

        If S2.Inicializado And Me.S2.GPIO Then
            S2.SetEstadoPLC()
            CtrlDinamismoTicket2Calles1.SetSensorEstado(2, S2.oEstado.valor)
        ElseIf Me.S2.GPIO = True Then ' Trabaja con GPIO
            Dim oSpeedWay As LectorRFID_SpeedWay
            oSpeedWay = GetLectorRFID_SpeedWay()
            If Not IsNothing(oSpeedWay) Then
                S2.SetEstadoGpio(oSpeedWay, TipoConexion.NormalCerrado)
                CtrlDinamismoTicket2Calles1.SetSensorEstado(2, Me.S2.oEstado.valor)
            End If
        End If

        '  Dim nAlarma As New Negocio.Alar
        modDelegado.setVisibleCtrl(Me, btnAlarma, nAlarma.mostrarAlarma(Me.ID_CONTROL_ACCESO))
    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR
            'De cada sector traigo los TAG del plc que tiene el control de acceso
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWordInt As List(Of Entidades.DatoWordInt) = nDatoWord.GetAllPorSector(itemSector.ID_SECTOR)

            For Each itemDato As Entidades.DatoWordInt In listDatoWordInt

                Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

                'Obtengo el nombre del tag quitandole la propiedad
                itemDato.tag = Constante.getNombreTag(itemDato.tag)
                'Si el tag contiene el nombre del control
                If itemDato.tag.Equals(S1.Name) Then

                    ctrlAutomatizacion = Me.S1

                ElseIf itemDato.tag.Equals(S2.Name) Then

                    ctrlAutomatizacion = Me.S2
                Else
                    ctrlAutomatizacion = Nothing
                End If

                If Not IsNothing(ctrlAutomatizacion) Then
                    'Si todavia no esta inicializado
                    If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(Me.ControlAcceso.ID_PLC, Me.ControlAcceso.ID_CONTROL_ACCESO, itemSector.ID_SECTOR) ' BARRERA DE INGRESO
                End If
            Next
        Next

        IMPRESORA_1.Inicializar()
        IMPRESORA_2.Inicializar()
    End Sub

    ''' <summary>
    ''' Permite obtener un lectorRFID  de la lista de lectores RFID del control de acceso
    ''' No aplica actualmente para mas de una lector
    ''' </summary>
    ''' <returns></returns>
    Private Function GetLectorRFID_SpeedWay()
        'Si no tiene lectores cargados termino el procedimiento
        If IsNothing(ListRFID_SpeedWay) Then Return Nothing

        If ListRFID_SpeedWay.Count = 1 Then
            'Devuelvo el primer lector
            Return ListRFID_SpeedWay(0)
        Else
            'No aplica actualmente para mas de un lector
            Return Nothing
        End If
    End Function
    Private Sub SetBuffer()

        'Inicializo los buffer
        Me.BufferCall1 = New Entidades.BufferTag()
        Me.BufferCall2 = New Entidades.BufferTag()


        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.BufferCall1.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferCall2.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER



    End Sub

    Private Sub VerEstadoLecturaRFID()
        If Not IsNothing(Me.ControlAcceso.ID_PLC) Then


            Dim nDatoBool As New Negocio.DatoBoolN
            Dim nombreTag As String = String.Format("{0}.{1}", TIPO_CONTROL_ACCESO.BALANZA, PROPIEDADES_TAG_PLC.LECTURA_RFID)
            Dim oDatoLecturaRFID As DatoBool = nDatoBool.GetOne(Me.ControlAcceso.ID_PLC, nombreTag)

            If Not IsNothing(oDatoLecturaRFID) Then
                'Si no tiene lector RFID cargado termino el proceso
                If IsNothing(Me.ListRFID_SpeedWay) Then Return
                'Recorro todos los lectores que tiene el control de acceso 
                For Each itemLector As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
                    If itemLector.IsConnected Then 'Si esta conectado
                        'Pregunto si tendria que estar leyendo
                        If oDatoLecturaRFID.valor = True Then
                            If Not itemLector.isRunning Then 'Si no esta leyendo 
                                itemLector.StartRead() 'Comienza a leer
                            End If

                        Else ' No tendria que estar leyendo
                            If itemLector.isRunning Then 'Si  esta leyendo 
                                itemLector.StopRead() ' Paro la lectura
                            End If

                        End If
                    End If
                Next
            Else
                Dim msj As String = String.Format("No se encontro el tipo de dato {0}", nombreTag)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, msj)
            End If
        Else ' 'De lo contrario busca el estado de lectura por el GPIO
            Dim oLectorRFID As LectorRFID_SpeedWay = GetLectorRFID_SpeedWay()
            oLectorRFID.ConsultarSensoresLectura(TipoConexion.NormalCerrado)
        End If
    End Sub

    Private Sub EstaHabilitado(ID_SECTOR As Integer, TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            Dim tmp As String = String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)

            Dim ctrlImpresora As Controles.CtrlImpresora = Nothing
            'Ejecuto el comando dependiendo de la barrera 
            'Para saber si es de ingreso o de egreso

            Select Case ID_SECTOR
                Case S1.ID_SECTOR
                    ctrlImpresora = IMPRESORA_1
                Case S2.ID_SECTOR
                    ctrlImpresora = IMPRESORA_2
            End Select


            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)
            tmp = String.Format("axRespuesta: {0}", axRespuesta.ToString())
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)
            'Pregunto si esta habilitado
            If axRespuesta.habilitado Then
                ctrlImpresora.Imprimir()

            Else

                'Guardo el ultimo tag leido
                Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)

            End If

        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, tmp)
        End Try

    End Sub
    ''' <summary>
    ''' Permite saber si la antena esta habilitada a leer
    ''' verifica si el sensor del sector esta cortando
    ''' </summary>
    ''' <param name="ID_SECTOR"> Numero de sector para verficiar si el sensor esta cortando</param>
    ''' <returns>FALSE = No esta habilitado a leer TRUE = Habilitado a leer </returns>
    Private Function AntenaHabilitada(ID_SECTOR As Integer) As Boolean

        Dim ctrlSensorLectura As Controles.CtrlSensor = Nothing

        Select Case ID_SECTOR
            Case S1.ID_SECTOR
                ctrlSensorLectura = S1

            Case S2.ID_SECTOR
                ctrlSensorLectura = S2

            Case Else
                Return False
        End Select
        'La antena va estar habilitada si el sensor va estar abierto
        Return ctrlSensorLectura.oEstado.valor = SensorInfrarojoEstado.ConPresencia

    End Function

    Private Function getAntenaLeyendo(ID_SECTOR As Integer) As Controles.CtrlAntenaLeyendo
        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = Nothing

        Select Case ID_SECTOR
            Case S1.ID_SECTOR
                ctrlAntLeyendo = CtrlAtenaLeyendoCalle1
            Case S2.ID_SECTOR
                ctrlAntLeyendo = CtrlAtenaLeyendoCalle2

        End Select



        Return ctrlAntLeyendo
    End Function


    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <param name="ID_SECTOR"></param>
    ''' <returns></returns>
    Private Function getBufferAntena(ID_SECTOR As Integer) As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Nothing

        'Le pregunto el sector a las barreras 
        'Para saber si es de ingreso o de egreso
        If S1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferCall1
        ElseIf S2.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferCall2

        End If

        Return Buffer
    End Function

#End Region

#Region "LECTOR RFID"

    ''' <summary>
    ''' Evento que se ejecuta cuando se leyo un tag 
    ''' </summary>
    ''' <param name="TagRFID"></param>
    ''' <param name="NumAntena"></param>
    ''' <remarks></remarks>
    Public Sub TagLeido(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim SUB_TAG As String = "[TagLeido]"
        Console.WriteLine("TAG RFID LEIDO: " & TagRFID)

        Dim nSector As New Negocio.SectorN
        Dim ID_SECTOR As Integer = 0

        'Busco el sector al que corresponde la antena
        Dim nAntena As New Negocio.Antena_RfidN
        Dim oAntena As Entidades.ANTENAS_RFID = nAntena.GetOne(NumAntena, LectorRFID.CONFIG_LECTOR_RFID(0).idConfiguracion)

        If IsNothing(oAntena) Then
            Dim tmp As String = String.Format("{0} La antena {1} no esta habilitada", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, tmp)
            Return
        Else
            If Not IsNothing(oAntena.ID_SECTOR) Then ID_SECTOR = oAntena.ID_SECTOR

        End If


        If ID_SECTOR = 0 Then 'No tiene un sector asignado la antena
            Dim tmp As String = String.Format("{0} La antena {1} no tiene ningun sector asignado", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, tmp)
            Return
        End If

        If Not manual Then ' Si no es manual (Manual = False) verifica si la antena esta habilitada para leer
            'Si la antena no esta habilitada
            If Not AntenaHabilitada(ID_SECTOR) Then Return
        End If


        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = getAntenaLeyendo(ID_SECTOR)
        If Not IsNothing(ctrlAntLeyendo) Then
            ctrlAntLeyendo.TagLeyendo(TagRFID)
        Else
            Dim tmp As String = String.Format("{0} No se encontro el CtrlAntenaLeyendo", SUB_TAG)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, tmp)
        End If

        'Obtengo el buffer
        Dim Buffer As Entidades.BufferTag = getBufferAntena(ID_SECTOR)

        'Agrego al buffer al tag leido y  obtengo el dialog cuando agregue al buffer el tagrfid
        Dim _BufferDIalog As Entidades.BufferTag.BufferDialog = Buffer.AddTagLeido(TagRFID)

        If _BufferDIalog = BufferTag.BufferDialog.TagLeido Then Return ' Termino el procedimiento

        'EJECUTO EL PROCEDIMINETO "EstaHabilitado" PARA SABER SI EL TAG LEIDO ESTA HABILITADO A INGRESAR O SALIR DE LA PORTERIA
        EstaHabilitado(ID_SECTOR, TagRFID)
    End Sub


    ''' <summary>
    ''' Evento que se ejecuta para  informa un error en el Lector RFID
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub ErrorRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, Msj)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando el lector envia un mensaje de información
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub InfoRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, Msj)
    End Sub



    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub


    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub



#End Region

#Region "SUBPROCESOS"
    Public Sub Monitoreo()
        'Apenas comienza el hilo conecto los lectores
        If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            If Not IsNothing(Me.ListRFID_SpeedWay) Then ConectarLectoresRFID()
        End If


        While True

            Try
                RefrescarDinamismo()


                If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFID()
                    VerEstadoLecturaRFID()
                Else
                    Dim oLectores As New List(Of Entidades.LECTOR_RFID)
                    oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFIDCliente(oLectores)
                End If


            Catch ex As Exception
                Dim msj As String = String.Format("{0} - {1}", "[Monitoreo]", ex.Message)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xError, msj)
            End Try
            Thread.Sleep(Me.ControlAcceso.MONITOREO)
        End While

    End Sub

    Private Sub btnAlarma_Click(sender As Object, e As EventArgs) Handles btnAlarma.Click
        Dim frm As New frmAlarmero(ID_CONTROL_ACCESO)
        frm.ShowDialog()
    End Sub
#End Region

#Region "EVENTOS BUFFER"

    Public Sub tiempoBufferCalle1(ByVal Tiempo As Integer) Handles BufferCall1.ShowTiempoBuffer
        Me.CtrlAtenaLeyendoCalle1.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferCalle2(ByVal Tiempo As Integer) Handles BufferCall2.ShowTiempoBuffer
        Me.CtrlAtenaLeyendoCalle2.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub ClearBufferCalle1() Handles BufferCall1.ClearBuffer
        Me.CtrlAtenaLeyendoCalle1.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferCalado2() Handles BufferCall2.ClearBuffer
        Me.CtrlAtenaLeyendoCalle2.BorrarUltimosTagLeido()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Negocio.AuditoriaN.AddAuditoria("Reseteo Control de acceso", "", Me.ID_CONTROL_ACCESO, 0, WS_ERRO_USUARIO, Constante.acciones.ResetControlAcceso, 0)
        If Not IsNothing(ListRFID_SpeedWay) Then
            For Each lector As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
                lector.Disconnect()
            Next
            ListRFID_SpeedWay.Clear()
        End If
        Inicializar()
    End Sub

    Private Sub btnLecturaManual1_Click(sender As Object, e As EventArgs) Handles btnLecturaManual1.Click, btnLecturaManual2.Click
        Dim btnLecturaManual As Button = sender
        Dim ctrlSensor As Controles.CtrlSensor = If(btnLecturaManual.Name = btnLecturaManual1.Name, S1, S2)

        Dim frm As New FrmLecturaManual(ctrlSensor.ID_SECTOR)
        AddHandler frm.TagLeidoManual, AddressOf TagLeidoManual
        frm.ShowDialog()
        frm.Dispose()
    End Sub

#End Region


End Class
