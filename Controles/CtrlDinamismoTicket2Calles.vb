﻿'POSICION LOCATION BARRERAS Y SENSORES
'picBIzqBaja = 118; 143
'picBDerBaja = 417; 139
'picBIzqAlta = 306; 0
'picBDerAlta = 412; 0

'picSDerRojo = 495; 207
'picSIzqRojo = 150; 207
'picSIzqRojo= 153; 208

'picSDerVerde=495; 207

Imports Negocio
Imports Entidades.BarrreraFalsa
Imports Entidades.Constante

Public Class CtrlDinamismoTicket2Calles

    Public Sub SetSensorEstado(ByVal NumSensor As Integer, ByVal _SensorEstado As Integer)
        Dim picCortando As PictureBox = Nothing
        Dim picNoCortando As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing


        If NumSensor = 1 Then
            picCortando = picS1Cortando
            picNoCortando = picS1NoCortando


        ElseIf NumSensor = 2 Then
            picCortando = picS2Cortando
            picNoCortando = picS2NoCortando


        End If

        If NumSensor = 1 Then
            picCortando = picS1Cortando
            picNoCortando = picS1NoCortando


        ElseIf NumSensor = 2 Then
            picCortando = picS2Cortando
            picNoCortando = picS2NoCortando

        End If

        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picNoCortando
            picNoVisible = picCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picCortando
            picNoVisible = picNoCortando


        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        Else
            'Oculto las dos barreras
            picVisible = picCortando
            picNoVisible = picNoCortando

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub

End Class
