﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoTicket2Calles
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.picS1Cortando = New System.Windows.Forms.PictureBox()
        Me.picS1NoCortando = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.picS2Cortando = New System.Windows.Forms.PictureBox()
        Me.picS2NoCortando = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTipDeshabilitado = New System.Windows.Forms.ToolTip(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.picS1Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS1NoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS2Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS2NoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picS1Cortando
        '
        Me.picS1Cortando.Image = Global.Controles.My.Resources.Resources.BarreraFalsaSensoresRojoIzq8000x400
        Me.picS1Cortando.Location = New System.Drawing.Point(150, 207)
        Me.picS1Cortando.Name = "picS1Cortando"
        Me.picS1Cortando.Size = New System.Drawing.Size(147, 172)
        Me.picS1Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS1Cortando.TabIndex = 5
        Me.picS1Cortando.TabStop = False
        '
        'picS1NoCortando
        '
        Me.picS1NoCortando.Image = Global.Controles.My.Resources.Resources.BarreraFalsaSensoresVerdeIzq8000x400
        Me.picS1NoCortando.Location = New System.Drawing.Point(153, 208)
        Me.picS1NoCortando.Name = "picS1NoCortando"
        Me.picS1NoCortando.Size = New System.Drawing.Size(148, 172)
        Me.picS1NoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS1NoCortando.TabIndex = 7
        Me.picS1NoCortando.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(323, 267)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 37)
        Me.Label1.TabIndex = 65
        Me.Label1.Text = "1"
        '
        'picS2Cortando
        '
        Me.picS2Cortando.Image = Global.Controles.My.Resources.Resources.BarreraFalsaSensoresRojoDer8000x400
        Me.picS2Cortando.Location = New System.Drawing.Point(495, 207)
        Me.picS2Cortando.Name = "picS2Cortando"
        Me.picS2Cortando.Size = New System.Drawing.Size(139, 177)
        Me.picS2Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS2Cortando.TabIndex = 4
        Me.picS2Cortando.TabStop = False
        '
        'picS2NoCortando
        '
        Me.picS2NoCortando.Image = Global.Controles.My.Resources.Resources.BarreraFalsaSensoresVerdeDer8000x400
        Me.picS2NoCortando.Location = New System.Drawing.Point(495, 207)
        Me.picS2NoCortando.Name = "picS2NoCortando"
        Me.picS2NoCortando.Size = New System.Drawing.Size(130, 173)
        Me.picS2NoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS2NoCortando.TabIndex = 6
        Me.picS2NoCortando.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(430, 267)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 37)
        Me.Label2.TabIndex = 66
        Me.Label2.Text = "2"
        '
        'ToolTipDeshabilitado
        '
        Me.ToolTipDeshabilitado.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ToolTipDeshabilitado.IsBalloon = True
        Me.ToolTipDeshabilitado.ShowAlways = True
        Me.ToolTipDeshabilitado.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning
        Me.ToolTipDeshabilitado.ToolTipTitle = "Balanza"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Controles.My.Resources.Resources.CasillaDer8000x400
        Me.PictureBox1.Location = New System.Drawing.Point(414, 149)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(70, 112)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 99
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Controles.My.Resources.Resources.CasillaIzq8000x400
        Me.PictureBox2.Location = New System.Drawing.Point(306, 150)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(75, 114)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 100
        Me.PictureBox2.TabStop = False
        '
        'CtrlDinamismoTicket2Calles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.BarreraFalsaVacio8000x400
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.picS2Cortando)
        Me.Controls.Add(Me.picS1Cortando)
        Me.Controls.Add(Me.picS1NoCortando)
        Me.Controls.Add(Me.picS2NoCortando)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "CtrlDinamismoTicket2Calles"
        Me.Size = New System.Drawing.Size(793, 397)
        CType(Me.picS1Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS1NoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS2Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS2NoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picS1Cortando As System.Windows.Forms.PictureBox
    Friend WithEvents picS1NoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents picS2Cortando As System.Windows.Forms.PictureBox
    Friend WithEvents picS2NoCortando As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ToolTipDeshabilitado As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
End Class
