﻿Public Class SectorN
    Public Shared oBD As New AccesoDatos.SectorAD
    Public Function GetAllSector() As List(Of Entidades.Sector)
        Return oBD.getAll()
    End Function

    Public Function GetAllSectorControl(idControl As Integer) As List(Of Entidades.Sector)
        Return oBD.getAllControl(idControl)
    End Function

    Public Function GetOne(idSector As Integer) As Entidades.Sector
        Return oBD.getOne(idSector)
    End Function
End Class
