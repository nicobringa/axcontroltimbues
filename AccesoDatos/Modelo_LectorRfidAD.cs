﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
namespace AccesoDatos
{
   public  class Modelo_LectorRfidAD
    {


        public int getCantAntenas(int idModeloLector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta =( from tabla in oContext.MODELO_LECTOR_RFID
                                where tabla.ID_MODELO_LECTOR_RFID == idModeloLector
                                select tabla).FirstOrDefault();
                return Convert.ToInt32( oConsulta.CANT_ANTENAS);
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return 0;

            }
        }


        public List<Entidades.MODELO_LECTOR_RFID> getAll()
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.MODELO_LECTOR_RFID
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;
            }
        }
    }
}
