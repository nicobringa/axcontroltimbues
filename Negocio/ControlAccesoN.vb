﻿Public Class ControlAccesoN

    Public Function GetOne(idCartel As Integer) As Entidades.CONTROL_ACCESO
        Dim oBD As New AccesoDatos.ControlAccesoAD
        Return oBD.getOne(idCartel)
    End Function

    Public Function GetAll() As List(Of Entidades.Control_Acceso)
        Dim oBD As New AccesoDatos.ControlAccesoAD
        Return oBD.getAll()
    End Function

    Public Sub AumentarBitVida(idControlAcceso As Integer)
        Dim oBD As New AccesoDatos.ControlAccesoAD
        oBD.SumarBitVida(idControlAcceso)
    End Sub

    Public Function GetIdControlConIdSector(idSector As Integer) As Integer
        Dim oBD As New AccesoDatos.ControlAccesoAD
        Return oBD.GetIDControlConIdSector(idSector)
    End Function

End Class
