﻿
Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Impinj.OctaneSdk
Public Class CtrlControlAccesoCalado2Calles

#Region "PROPIEDADES"


    Private _RFID As Boolean
    Public Property RFID() As Boolean
        Get
            Return _RFID
        End Get
        Set(ByVal value As Boolean)
            _RFID = value
        End Set
    End Property

    Private ControlAcceso As Entidades.Control_Acceso
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private hMonitoreo As Thread
    Private WithEvents BufferCalado1 As Entidades.BufferTag
    Private WithEvents BufferCalado2 As Entidades.BufferTag
    Private oLector As New Entidades.Lector_RFID
    Private nLector As New Negocio.LectorRFID_N
#End Region

#Region "EVENTOS FORM"
    Private Sub btnLectorRfid_Click(sender As Object, e As EventArgs) Handles btnLectorRfid.Click

        Dim frm As New FrmConfLectoresRFID(Me.ListRFID_SpeedWay)
        frm.ShowDialog()


    End Sub

#End Region

#Region "METODOS"
    Public Overrides Sub Inicializar()

        If Me.ID_CONTROL_ACCESO = 0 Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO:" + Me.Name + ": Se debe asignar un ID de puesto de trabajo")

            Return
        End If

        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)

        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO:" + Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If

        Dim nombreControlAcceso As String = String.Format("{0} - ({1})", Me.ControlAcceso.NOMBRE, Me.ControlAcceso.ID_CONTROL_ACCESO)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreControlAcceso)
        'Si el puesto de trabajo es servidor
        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            'Inicializo como servidor
            IniServidor()
        Else
            'De los contrario como cliente
            IniCliente()
        End If

        'Ejecuto el hilo de monitoreo
        If Not IsNothing(hMonitoreo) Then
            If hMonitoreo.IsAlive Then hMonitoreo.Abort()
            hMonitoreo = Nothing
        End If

        inicializarCtrl()
        SetBuffer()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()


    End Sub

    Public Sub InicializarWS()
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)
        inicializarCtrl()
    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR
            'De cada sector traigo los TAG del plc que tiene el control de acceso
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(itemSector.ID_SECTOR)

            For Each itemDato As Entidades.DATO_WORDINT In listDatoWordInt

                Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

                'Obtengo el nombre del tag quitandole la propiedad
                itemDato.TAG = Constante.getNombreTag(itemDato.TAG)
                'Si el tag contiene el nombre del control
                If itemDato.TAG.Equals(S_LECTURA_CALADO1.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_CALADO1

                ElseIf itemDato.TAG.Equals(S_LECTURA_CALADO2.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_CALADO2

                Else
                    ctrlAutomatizacion = Nothing
                End If

                If Not IsNothing(ctrlAutomatizacion) Then
                    'Si todavia no esta inicializado
                    If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(Me.ControlAcceso.ID_PLC, Me.ControlAcceso.ID_CONTROL_ACCESO, itemSector.ID_SECTOR) ' BARRERA DE INGRESO
                End If
            Next

        Next



        CAM_CUPO_CALADO1.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CAM_CUPO_CALADO2.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CartelLED_CUPO_CALADO1.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CartelLED_CUPO_CALADO2.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)

    End Sub

    Public Sub IniServidor()
        'Cargo los lectores RFID
        CargarLectoresRFID()

    End Sub

    Public Sub IniCliente()
        CargarLectoresRFID()
        modDelegado.setVisibleCtrl(Me, btnLectorRfid, False)
    End Sub

    Private Sub CargarLectorRFIDCliente()

        Dim oLectores As New List(Of Entidades.LECTOR_RFID)
        oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
        CtrlEstadoLectoresRFID1.CargarLectoresCliente(oLectores)
    End Sub

    Private Sub CargarLectoresRFID()
        'Busco los lectores
        Dim nLectorEFID As New Negocio.LectorRFID_N

        Dim listLectorRFID As List(Of Entidades.LECTOR_RFID) = nLectorEFID.GetAll(Me.ID_CONTROL_ACCESO)
        'listLectorRFID.Clear()

        If IsNothing(listLectorRFID) Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO:" + "No tiene lectores RFID asginados")

            Return 'FIN DE PROCEDIMIENTO
        End If

        If listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO:" + "No tiene lectores RFID asginados")

            Return 'FIN 
        End If

        'Recorro los lectores agregados para el puesto de trabajo
        For Each LectorRFID As Entidades.LECTOR_RFID In listLectorRFID
            'Creo el lector RFID SpeedWay para su utilización
            Dim RFID_SpeedWay As New Negocio.LectorRFID_SpeedWay(LectorRFID)

            If (IsNothing(Me.ListRFID_SpeedWay)) Then Me.ListRFID_SpeedWay = New List(Of Negocio.LectorRFID_SpeedWay)
            'Agrego el LectorRFID a la lista
            Me.ListRFID_SpeedWay.Add(RFID_SpeedWay)
            'Agrego los escuchadores para los evento del lector
            AddHandler RFID_SpeedWay.TagLeido, AddressOf Me.TagLeido
            AddHandler RFID_SpeedWay.ErrorRFID, AddressOf Me.ErrorRFID
            AddHandler RFID_SpeedWay.InfoRFID, AddressOf Me.InfoRFID
            ' AddHandler RFID_SpeedWay.EventGPIO, AddressOf Me.EventoGPIO

        Next

        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            CtrlEstadoLectoresRFID1.CargarLectores(Me.ListRFID_SpeedWay)
        Else
            CtrlEstadoLectoresRFID1.CargarLectoresCliente(listLectorRFID)
        End If




    End Sub

    Private Function getAntenaLeyendo(ID_SECTOR As Integer) As Controles.CtrlAntenaLeyendo
        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = Nothing

        Select Case ID_SECTOR
            Case S_LECTURA_CALADO1.ID_SECTOR
                ctrlAntLeyendo = CtrlAntenaLeyendoCalado1
            Case S_LECTURA_CALADO2.ID_SECTOR
                ctrlAntLeyendo = CtrlAntenaLeyendoCalado2

        End Select



        Return ctrlAntLeyendo
    End Function

    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <param name="ID_SECTOR"></param>
    ''' <returns></returns>
    Private Function getBufferAntena(ID_SECTOR As Integer) As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Nothing

        'Le pregunto el sector a las barreras 
        'Para saber si es de ingreso o de egreso
        If S_LECTURA_CALADO1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferCalado1
        ElseIf S_LECTURA_CALADO2.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferCalado2
        End If

        Return Buffer
    End Function

    Private Sub EstaHabilitado(ID_SECTOR As Integer, TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            Dim tmp As String '= String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            'CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)

            Dim CtrlCartel As Controles.CtrlCartelMultiLED = Nothing

            'Ejecuto el comando dependiendo de la barrera 
            'Para saber si es de ingreso o de egreso

            Select Case ID_SECTOR
                Case S_LECTURA_CALADO1.ID_SECTOR
                    CtrlCartel = CartelLED_CUPO_CALADO1

                Case S_LECTURA_CALADO2.ID_SECTOR
                    CtrlCartel = CartelLED_CUPO_CALADO2


            End Select

            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)




            'Pregunto si esta habilitado
            If axRespuesta.habilitado Then
                'Si esta habilitado
                tmp = "[ " + DateTime.Now + " ] CAMIÓN HABILITADO: ID TRANSACCIÓN: " + axRespuesta.idTransaccion.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)
                'Muestro el mensaje en el cartel
                CtrlCartel.EscribirCartel(axRespuesta.mensaje)
            Else
                tmp = "[ " + DateTime.Now + " ] CAMIÓN NO HABILITADO: " + axRespuesta.mensaje.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorUser, tmp)
                CtrlCartel.EscribirCartel("Camión no habilitado")
                'Guardo el ultimo tag leido
                Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)

            End If


            If Not IsNothing(CtrlCartel) Then CtrlCartel.EscribirCartel(axRespuesta.mensaje)
        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End Try

    End Sub

    Private Sub RefrescarDinamismo()


        'SENSORES
        If Me.S_LECTURA_CALADO1.Inicializado And Me.S_LECTURA_CALADO1.GPIO = False Then ' Trabaja con PLC
            Me.S_LECTURA_CALADO1.SetEstadoPLC()
            CtrlCalado1.SetSensorEstado(Me.S_LECTURA_CALADO1.oEstado.valor)

        ElseIf Me.S_LECTURA_CALADO1.GPIO = True Then ' Trabaja con GPIO
            Dim oSpeedWay As LectorRFID_SpeedWay
            oSpeedWay = GetLectorRFID_SpeedWay()
            If Not IsNothing(oSpeedWay) Then
                S_LECTURA_CALADO1.SetEstadoGpio(oSpeedWay, TipoConexion.NormalCerrado)
                CtrlCalado1.SetSensorEstado(Me.S_LECTURA_CALADO1.oEstado.valor)
            End If

        End If
        If Me.S_LECTURA_CALADO2.Inicializado And Me.S_LECTURA_CALADO2.GPIO = False Then
            Me.S_LECTURA_CALADO2.SetEstadoPLC()
            CtrlCalado2.SetSensorEstado(Me.S_LECTURA_CALADO2.oEstado.valor)

        ElseIf Me.S_LECTURA_CALADO2.GPIO = True Then
            Dim oSpeedWay As LectorRFID_SpeedWay
            oSpeedWay = GetLectorRFID_SpeedWay()
            If Not IsNothing(oSpeedWay) Then
                S_LECTURA_CALADO2.SetEstadoGpio(oSpeedWay, TipoConexion.NormalCerrado)
                CtrlCalado2.SetSensorEstado(Me.S_LECTURA_CALADO2.oEstado.valor)
            End If

        End If

        '  Dim nAlarma As New Negocio.AlarmaN
        '    modDelegado.setVisibleCtrl(Me, btnAlarma, nAlarma.mostrarAlarma(Me.ID_CONTROL_ACCESO))

    End Sub

    Private Sub ConectarLectoresRFID()

        'Recorro los lectores RFID Configurado
        For Each RFID_SpeedWay As LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            'Si el lector no esta conectado
            If Not RFID_SpeedWay.IsConnected Then

                Try
                    oLector = nLector.GetOne(RFID_SpeedWay.Ip)
                    oLector.CONECTADO = RFID_SpeedWay.Conectarse()
                    nLector.Update(oLector)
                Catch ex As Exception
                    'Surgio un error en la conexion
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, ex.Message)
                End Try
            Else
                'El lector ya se encuentra conectado
                Dim tmp As String = "[ " + DateTime.Now + " ] INFO: " + String.Format("El lector {0} se encuentra conectado", RFID_SpeedWay.Nombre)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Le consulto al PLC si tengo que comenzar la lectura RFID
    ''' </summary>
    Private Sub VerEstadoLecturaRFID()
        If Not IsNothing(Me.ControlAcceso.ID_PLC) Then
            Dim nDatoBool As New Negocio.DatoBoolN
            Dim nombreTag As String = String.Format("{0}.{1}", TIPO_CONTROL_ACCESO.BALANZA, PROPIEDADES_TAG_PLC.LECTURA_RFID)
            Dim oDatoLecturaRFID As DATO_BOOL = nDatoBool.GetOne(Me.ControlAcceso.ID_PLC, nombreTag)
            'Pregunto si tiene dato de lectura RFID cargado en el PLC 
            If Not IsNothing(oDatoLecturaRFID) Then
                'Si no tiene lector RFID cargado termino el proceso
                If IsNothing(Me.ListRFID_SpeedWay) Then Return
                'Recorro todos los lectores que tiene el control de acceso 
                For Each itemLector As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
                    If itemLector.IsConnected Then 'Si esta conectado
                        'Pregunto si tendria que estar leyendo
                        If oDatoLecturaRFID.valor = True Then
                            If Not itemLector.isRunning Then 'Si no esta leyendo 
                                itemLector.StartRead() 'Comienza a leer
                            End If

                        Else ' No tendria que estar leyendo
                            If itemLector.isRunning Then 'Si  esta leyendo 
                                itemLector.StopRead() ' Paro la lectura
                            End If

                        End If
                    End If
                Next
            Else
                Dim msj As String = String.Format("No se encontro el tipo de dato {0}", nombreTag)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
            End If
        Else ' 'De lo contrario busca el estado de lectura por el GPIO
            Dim oLectorRFID As LectorRFID_SpeedWay = GetLectorRFID_SpeedWay()
            oLectorRFID.ConsultarSensoresLectura(TipoConexion.NormalCerrado)
        End If
    End Sub

    Private Sub SetBuffer()

        'Inicializo los buffer
        Me.BufferCalado1 = New Entidades.BufferTag()
        Me.BufferCalado2 = New Entidades.BufferTag()

        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.BufferCalado1.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferCalado2.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER


    End Sub


    ''' <summary>
    ''' Permite saber si la antena esta habilitada a leer
    ''' verifica si el sensor del sector esta cortando
    ''' </summary>
    ''' <param name="ID_SECTOR"> Numero de sector para verficiar si el sensor esta cortando</param>
    ''' <returns>FALSE = No esta habilitado a leer TRUE = Habilitado a leer </returns>
    Private Function AntenaHabilitada(ID_SECTOR As Integer) As Boolean

        Dim ctrlSensorLectura As Controles.CtrlSensor = Nothing

        Select Case ID_SECTOR
            Case S_LECTURA_CALADO1.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_CALADO1

            Case S_LECTURA_CALADO2.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_CALADO2

            Case Else
                Return False
        End Select
        'La antena va estar habilitada si el sensor va estar abierto
        Return ctrlSensorLectura.oEstado.valor = SensorInfrarojoEstado.ConPresencia

    End Function

    ''' <summary>
    ''' Permite obtener un lectorRFID  de la lista de lectores RFID del control de acceso
    ''' No aplica actualmente para mas de una lector
    ''' </summary>
    ''' <returns></returns>
    Private Function GetLectorRFID_SpeedWay() As LectorRFID_SpeedWay
        'Si no tiene lectores cargados termino el procedimiento
        If IsNothing(ListRFID_SpeedWay) Then Return Nothing

        If ListRFID_SpeedWay.Count = 1 Then
            'Devuelvo el primer lector
            Return ListRFID_SpeedWay(0)
        Else
            'No aplica actualmente para mas de un lector
            Return Nothing
        End If
    End Function

#End Region

#Region "LECTOR RFID"

    ''' <summary>
    ''' Evento que se ejecuta cuando se leyo un tag 
    ''' </summary>
    ''' <param name="TagRFID"></param>
    ''' <param name="NumAntena"></param>
    ''' <remarks></remarks>
    Public Sub TagLeido(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim SUB_TAG As String = "[TagLeido]"
        Console.WriteLine("TAG RFID LEIDO: " & TagRFID)

        Dim nSector As New Negocio.SectorN
        Dim ID_SECTOR As Integer = 0

        'Busco el sector al que corresponde la antena
        Dim nAntena As New Negocio.Antena_RfidN
        Dim oAntena As Entidades.ANTENAS_RFID = nAntena.GetOne(NumAntena, LectorRFID.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)

        If IsNothing(oAntena) Then
            Dim tmp As String = String.Format("{0} La antena {1} no esta habilitada", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        Else
            If Not IsNothing(oAntena.ID_SECTOR) Then ID_SECTOR = oAntena.ID_SECTOR

        End If


        If ID_SECTOR = 0 Then 'No tiene un sector asignado la antena
            Dim tmp As String = String.Format("{0} La antena {1} no tiene ningun sector asignado", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        End If

        If Not manual Then ' Si no es manual (Manual = False) verifica si la antena esta habilitada para leer
            'Si la antena no esta habilitada
            If Not AntenaHabilitada(ID_SECTOR) Then Return
        End If


        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = getAntenaLeyendo(ID_SECTOR)
        If Not IsNothing(ctrlAntLeyendo) Then
            ctrlAntLeyendo.TagLeyendo(TagRFID)
        Else
            Dim tmp As String = String.Format("{0} No se encontro el CtrlAntenaLeyendo", SUB_TAG)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End If

        'Obtengo el buffer
        Dim Buffer As Entidades.BufferTag = getBufferAntena(ID_SECTOR)

        'Agrego al buffer al tag leido y  obtengo el dialog cuando agregue al buffer el tagrfid
        Dim _BufferDIalog As Entidades.BufferTag.BufferDialog = Buffer.AddTagLeido(TagRFID)

        If _BufferDIalog = BufferTag.BufferDialog.TagLeido Then Return ' Termino el procedimiento

        'EJECUTO EL PROCEDIMINETO "EstaHabilitado" PARA SABER SI EL TAG LEIDO ESTA HABILITADO A INGRESAR O SALIR DE LA PORTERIA
        EstaHabilitado(ID_SECTOR, TagRFID)
    End Sub


    ''' <summary>
    ''' Evento que se ejecuta para  informa un error en el Lector RFID
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub ErrorRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, Msj)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando el lector envia un mensaje de información
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub InfoRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, Msj, idSector)
    End Sub

    Public Sub EventoGPIO(Puerto As Integer, Estado As Boolean, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay)
        Dim TipoConex As TipoConexion = TipoConexion.NormalCerrado
        Dim EstadoNormal As Boolean = If(TipoConex = TipoConexion.NormalAbierto, False, True)
        If Estado <> EstadoNormal Then 'Si el estado es distinto a su estado normal

            If Not LectorRFID_SpeedWay.IsStar Then 'Y no esta leyendo

                LectorRFID_SpeedWay.StartRead() ' Comenzar a leer

            End If

        Else ' Si el estado es = a su estado normal consultado el estado de los demas puertos para saber si inicio o dentengo la lectura
            LectorRFID_SpeedWay.ConsultarSensoresLectura(TipoConexion.NormalCerrado)

        End If


    End Sub


    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub


    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Negocio.AuditoriaN.AddAuditoria("Reseteo Control de acceso", "", Me.ID_CONTROL_ACCESO, 0, WS_ERRO_USUARIO, Constante.acciones.ResetControlAcceso, 0)
        If Not IsNothing(ListRFID_SpeedWay) Then
            For Each lector As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
                lector.Disconnect()
            Next
            ListRFID_SpeedWay.Clear()
        End If
        Inicializar()
    End Sub



#End Region

#Region "EVENTOS BUFFER"

    Public Sub tiempoBufferCalado1(ByVal Tiempo As Integer) Handles BufferCalado1.ShowTiempoBuffer
        Me.CtrlAntenaLeyendoCalado1.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferCalado2(ByVal Tiempo As Integer) Handles BufferCalado2.ShowTiempoBuffer
        Me.CtrlAntenaLeyendoCalado2.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub ClearBufferCalado1() Handles BufferCalado1.ClearBuffer
        Me.CtrlAntenaLeyendoCalado1.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferCalado2() Handles BufferCalado2.ClearBuffer
        Me.CtrlAntenaLeyendoCalado2.BorrarUltimosTagLeido()
    End Sub

#End Region

#Region "SUBPROCESOS"

    Public Sub Monitoreo()
        'Apenas comienza el hilo conecto los lectores
        If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            If Not IsNothing(Me.ListRFID_SpeedWay) Then ConectarLectoresRFID()
        End If

        While True

            Try
                RefrescarDinamismo()
                If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFID()
                    VerEstadoLecturaRFID()
                Else
                    Dim oLectores As New List(Of Entidades.LECTOR_RFID)
                    oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFIDCliente(oLectores)

                End If

            Catch ex As Exception
                Dim msj As String = String.Format("{0} - {1}", "[Monitoreo]", ex.Message)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
            End Try
            Thread.Sleep(Me.ControlAcceso.MONITOREO)
        End While

    End Sub

    Private Sub btnAlarma_Click(sender As Object, e As EventArgs) Handles btnAlarma.Click
        ' Dim frm As New frmAlarmero(ID_CONTROL_ACCESO)
        ' frm.ShowDialog()
    End Sub

    Private Sub btnLecturaManualCalado2_Click(sender As Object, e As EventArgs) Handles btnLecturaManualCalado2.Click
        If S_LECTURA_CALADO2.Inicializado Then
            Dim frm As New FrmLecturaManual(S_LECTURA_CALADO2.ID_SECTOR)
            AddHandler frm.TagLeidoManual, AddressOf TagLeidoManual
            frm.ShowDialog()
            frm.Dispose()
        End If
    End Sub

    Private Sub btnLecturaManualCalado1_Click(sender As Object, e As EventArgs) Handles btnLecturaManualCalado1.Click
        If S_LECTURA_CALADO1.Inicializado Then
            Dim frm As New FrmLecturaManual(S_LECTURA_CALADO1.ID_SECTOR)
            AddHandler frm.TagLeidoManual, AddressOf TagLeidoManual
            frm.ShowDialog()
            frm.Dispose()
        End If
    End Sub

#End Region

End Class
