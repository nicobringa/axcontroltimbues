﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading

Public Class CtrlSectorDobleBarreraFalsa
#Region "PROPIEDADES"


    Private _idSector As Integer = 0
    Public Property ID_SECTOR() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Private Sector As SECTOR
    Public WithEvents Buffer As Entidades.BufferTag
    Private UltIdNotificacion As Integer

    ''' <summary>
    ''' Evento que se genera cuando se realiza una lectura manual RFID
    ''' </summary>
    Public Event EventTagLeidoManual(ByVal oTagLeidoManual As TagLeidoManual)

#End Region

#Region "CONSTRUCTOR"

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Inicializar()
    End Sub

    Public Sub New(oSector As SECTOR)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.Sector = oSector
        _idSector = Me.Sector.ID_SECTOR
        Inicializar()
    End Sub

#End Region

#Region "Eventos FORM"

    Public Event ClickBarreraManual(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _BarreraComando As Entidades.Constante.BarreraComando)

    'Private Sub btnSubirBarIngreso_Click(sender As Object, e As EventArgs)
    '    If BARRERA.Inicializado Then
    '        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Operador)

    '        If oAutenticacion.ShowDialog = DialogResult.OK Then
    '            Dim descripAuditoria As String = "Subo Barrera Ingreso"
    '            BARRERA.EjectComando(BarreraComando.SubirManual)
    '            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", BARRERA.ID_SECTOR, 0, WS_ERRO_USUARIO, Constante.acciones.suboBarreraIngreso, 0)
    '        End If
    '    End If
    'End Sub

    'Private Sub btnBajarBarIngreso_Click(sender As Object, e As EventArgs)
    '    If BARRERA.Inicializado Then
    '        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Operador)

    '        If oAutenticacion.ShowDialog = DialogResult.OK Then
    '            Dim descripAuditoria As String = "bajo Barrera Ingreso"
    '            BARRERA.EjectComando(BarreraComando.BajarManual)
    '            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", BARRERA.ID_SECTOR, 0, WS_ERRO_USUARIO, Constante.acciones.bajoBarreraIngreso, 0)
    '        End If
    '    End If
    'End Sub

#End Region

#Region "METODOS"
    ''' <summary>
    ''' Metodo para incializar todas las variables de estado del objeto
    ''' </summary>
    Public Sub Inicializar()
        ' InitializeComponent()
        If Me.ID_SECTOR = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_SECTOR, Entidades.Constante.TipoNotificacion.Informacion,
                "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID un control de acceso")

            Return
        End If



        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN

        If IsNothing(Me.Sector) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_SECTOR, Entidades.Constante.TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el sector")
            Return

        End If
        Dim nombreSector As String = String.Format("{0} - ({1})", Me.Sector.NOMBRE, Me.Sector.ID_SECTOR)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreSector)

        inicializarCtrl()
    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        'De cada sector traigo los TAG del plc que tiene el control de acceso
        Dim nDatoWord As New Negocio.DatoWordIntN
        Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(Me.Sector.ID_SECTOR)

        For Each itemDato As Entidades.DATO_WORDINT In listDatoWordInt

            Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

            itemDato.TAG = Constante.getNombreTag(itemDato.TAG)
            'Si el tag contiene el nombre del control
            If itemDato.TAG.Equals(BARRERA1.Name) Then
                ctrlAutomatizacion = Me.BARRERA1
            ElseIf itemDato.TAG.Equals(BARRERA2.Name) Then
                ctrlAutomatizacion = Me.BARRERA2
            Else
                ctrlAutomatizacion = Nothing
            End If
            If Not IsNothing(ctrlAutomatizacion) Then
                'Si todavia no esta inicializado
                If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(itemDato.FK_PLC, Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, Me.Sector.ID_SECTOR) ' BARRERA DE INGRESO
            End If
        Next

    End Sub

#End Region

#Region "Eventos"

#End Region

End Class