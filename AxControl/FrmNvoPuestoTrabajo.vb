﻿Public Class FrmNvoPuestoTrabajo

#Region "PROPIEDADES"
    Private _NombrePuestoTrabajo As String
    Private Property NombrePuestoTrabajo() As String
        Get
            Return _NombrePuestoTrabajo
        End Get
        Set(ByVal value As String)
            _NombrePuestoTrabajo = value
            txtPuestoTrabajo.Text = value
        End Set
    End Property

    Private _PuestoTrabajo As Entidades.Puesto_Trabajo
    Public Property PuestoTrabajo() As Entidades.Puesto_Trabajo
        Get
            Return _PuestoTrabajo
        End Get
        Set(ByVal value As Entidades.Puesto_Trabajo)
            _PuestoTrabajo = value
        End Set
    End Property
#End Region

#Region "CONSTRUCTOR"
    Public Sub New(NombreMaquina As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.NombrePuestoTrabajo = NombreMaquina


    End Sub

    Public Sub New(PuestoTrabajo As Entidades.Puesto_Trabajo)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.PuestoTrabajo = PuestoTrabajo


    End Sub

#End Region

#Region "EVENTOS FORM"

    Private Sub FrmNvoPuestoTrabajo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RefrescarGrilla()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim listControlAcceso As List(Of Entidades.Control_Acceso) = GetControlAccesoChecked()

        If Not IsNothing(listControlAcceso) Then
            'Guardo los controles de acceso para el puesto de trabajo

            If IsNothing(Me.PuestoTrabajo) Then
                Me.PuestoTrabajo = New Entidades.Puesto_Trabajo
                Me.PuestoTrabajo.nombre = Me.NombrePuestoTrabajo
            End If

            PuestoTrabajo.control_acceso = listControlAcceso
            Dim nPuestoTrabajo As New Negocio.Puesto_TrabajoN
            Me.PuestoTrabajo = nPuestoTrabajo.Guardar(Me.PuestoTrabajo)

            If Me.PuestoTrabajo.ID_PUESTO_TRABAJO > 0 Then
                Negocio.ModSesion.PUESTO_TRABAJO = Me.PuestoTrabajo
                Me.DialogResult = DialogResult.OK

            End If


        Else
            MessageBox.Show("Seleccione al menos 1 control de acceso", "Aceptar", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

        End If


    End Sub
#End Region

#Region "METODOS"


    ''' <summary>
    ''' Permite refrescar la grilla de control de acceso
    ''' </summary>
    Private Sub RefrescarGrilla()

        dgControlAcceso.Rows.Clear()
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Dim listControlAcceso As List(Of Entidades.Control_Acceso) = nControlAcceso.GetAll()

        If Not IsNothing(listControlAcceso) Then

            For Each ControlAccceso As Entidades.Control_Acceso In listControlAcceso

                dgControlAcceso.Rows.Add(ControlAccceso.ID_CONTROL_ACCESO, ControlAccceso.NOMBRE)
                Dim UltIndex As Integer = dgControlAcceso.RowCount - 1
                dgControlAcceso.Rows(UltIndex).Tag = ControlAccceso
            Next

        Else


        End If

    End Sub

    ''' <summary>
    ''' Devuelve la cantidad de filas seleccionada en la celda Check
    ''' </summary>
    ''' <returns></returns>
    Private Function CountCheck() As Integer

        Dim count As Integer = 0
        For Each Row As DataGridViewRow In dgControlAcceso.Rows

            Dim RowChecked As Boolean = Row.Cells(colSeleccion.Index).Value
            If RowChecked Then count += 1

        Next

        Return count
    End Function

    ''' <summary>
    ''' Devuelve los controles de acceso seleccionado para el puesto de trabajo
    ''' </summary>
    ''' <returns></returns>
    Private Function GetControlAccesoChecked() As List(Of Entidades.Control_Acceso)
        Dim ListControl As List(Of Entidades.Control_Acceso) = Nothing
        For Each row As DataGridViewRow In dgControlAcceso.Rows

            If row.Cells(colSeleccion.Index).Value Then

                If IsNothing(ListControl) Then ListControl = New List(Of Entidades.Control_Acceso)
                Dim ControlAcceso As New Entidades.CONTROL_ACCESO
                ControlAcceso.ID_CONTROL_ACCESO = CType(row.Tag, Entidades.CONTROL_ACCESO).ID_CONTROL_ACCESO
                ListControl.Add(ControlAcceso)


            End If

        Next
       Return ListControl
    End Function
#End Region

End Class