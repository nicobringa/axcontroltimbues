﻿Imports Entidades
Imports Entidades.Constante
Public Class FrmBalanza

#Region "PROPIEDADES"


    Private _CtrlCabezalBalanza As Controles.CtrlCabezalBalanza
    Public Property CtrlCabezalBalanza() As Controles.CtrlCabezalBalanza
        Get
            Return _CtrlCabezalBalanza
        End Get
        Set(ByVal value As Controles.CtrlCabezalBalanza)
            _CtrlCabezalBalanza = value




        End Set
    End Property

    Private _oComando As DATO_WORDINT
    Public Property oComando() As DATO_WORDINT
        Get
            Return _oComando
        End Get
        Set(ByVal value As DATO_WORDINT)
            _oComando = value
        End Set
    End Property

#End Region


#Region "CONSTRUCTOR"
    Public Sub New(ByVal CtrlCabezalBalanza As Controles.CtrlCabezalBalanza)
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.CtrlCabezalBalanza = CtrlCabezalBalanza
        Me.oComando = CtrlCabezalBalanza.oComando
        Consultar()
    End Sub


#End Region

#Region "EVENTOS FORM"
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            If CamposCompletos() Then

                Dim descripAuditoria As String
                Me.CtrlCabezalBalanza.oPesoMaximo.valor = txtPesoMaximo.Text
                Me.CtrlCabezalBalanza.oDiferenciaEstabilidad.valor = txtDifEstabilidad.Text
                Me.CtrlCabezalBalanza.oTiempoEstabilidad.valor = txtTiempoEstabilidad.Text

                Dim nDatoWordInt As New Negocio.DatoWordIntN
                nDatoWordInt.Escribir(Me.CtrlCabezalBalanza.oPesoMaximo.id, Me.CtrlCabezalBalanza.oPesoMaximo.valor)
                nDatoWordInt.Escribir(Me.CtrlCabezalBalanza.oDiferenciaEstabilidad.id, Me.CtrlCabezalBalanza.oDiferenciaEstabilidad.valor)
                nDatoWordInt.Escribir(Me.CtrlCabezalBalanza.oTiempoEstabilidad.id, Me.CtrlCabezalBalanza.oTiempoEstabilidad.valor)

                descripAuditoria = "Configuro Balanza: "
                Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionBalanza, 0)
            End If
            Me.Close()
        Else
            Me.Close()
        End If

    End Sub

#End Region


#Region "METODOS"

    Public Sub Consultar()

        txtDifEstabilidad.Text = Me.CtrlCabezalBalanza.oDiferenciaEstabilidad.valor
        txtPesoMaximo.Text = Me.CtrlCabezalBalanza.oPesoMaximo.valor
        txtTiempoEstabilidad.Text = Me.CtrlCabezalBalanza.oTiempoEstabilidad.valor

        Dim ColorDinamismo As Color = Nothing

        'PESO NETO
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoNeto.valor = 0, Color.Red, Color.Green)
        pnlPesoNeto.BackColor = ColorDinamismo
        'PESO NEGATIVO
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoNegativo.valor = 0, Color.Gray, Color.Red)
        pnlPesoNegativo.BackColor = ColorDinamismo
        'FUERA RANGO
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oFueraRango.valor = 0, Color.Gray, Color.Red)
        pnlFueraRango.BackColor = ColorDinamismo
        'FUERA EQUILIBRIO
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oFueraEquilibrio.valor = 0, Color.Gray, Color.Red)
        pnlFueraEquilibrio.BackColor = ColorDinamismo
        'FUERA PESO MAYOR 500
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoMayor500.valor = 0, Color.Gray, Color.Green)
        pnlPesoMayor500kg.BackColor = ColorDinamismo
        'PESO ESTABLE
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoEstable.valor = 0, Color.Gray, Color.Green)
        pnlPesoEstable.BackColor = ColorDinamismo
        'PESO MENOR A DIFERENCIA DE ESTABILIDAD
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoMenorDifEstabilidad.valor = 0, Color.Gray, Color.Green)
        pnlPesoMenorDifEstabilidad.BackColor = ColorDinamismo
        'PESO MAXIMO
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoMaximoBal.valor = 0, Color.Gray, Color.Red)
        pnlPesoMaximo.BackColor = ColorDinamismo
        'ERROR
        ColorDinamismo = If(Me.CtrlCabezalBalanza.oPesoMaximoBal.valor = 0, Color.Gray, Color.Red)
        pnlErrorBalanza.BackColor = ColorDinamismo

    End Sub

    Private Function CamposCompletos() As Boolean

        Return True
    End Function

    Private Sub btnDeshabilitar_Click(sender As Object, e As EventArgs) Handles btnDeshabilitarComando.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            Dim descripAuditoria As String
            Dim comando As Integer = Constante.BalanzaComando.Deshabilitar
            Dim nDatoWordInt As New Negocio.DatoWordIntN
            nDatoWordInt.Escribir(oComando.id, comando)
            descripAuditoria = "Configuro Balanza: deshabilitar "
            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionBalanza, 0)
        End If
    End Sub

    Private Sub btnHabilitar_Click(sender As Object, e As EventArgs) Handles btnHabilitarComando.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            Dim descripAuditoria As String
            Dim comando As Integer = Constante.BalanzaComando.Habilitar
            Dim nDatoWordInt As New Negocio.DatoWordIntN
            nDatoWordInt.Escribir(oComando.id, comando)
            descripAuditoria = "Configuro Balanza: habilitar"
            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionBalanza, 0)
        End If
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnResetComando.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            Dim descripAuditoria As String
            Dim comando As Integer = Constante.BalanzaComando.Reset
            Dim nDatoWordInt As New Negocio.DatoWordIntN
            nDatoWordInt.Escribir(oComando.id, comando)
            descripAuditoria = "Configuro Balanza: Reset "
            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionBalanza, 0)
        End If
    End Sub

#End Region

#Region "SUBPROCESOS"

#End Region










End Class