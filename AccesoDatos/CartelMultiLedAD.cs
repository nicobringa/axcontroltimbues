﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class CartelMultiLedAD
    {
        public CARTEL_MULTILED update_config(CARTEL_MULTILED cartel)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {

                    var oConsulta = (from tabla in oContext.CARTEL_MULTILED
                                     where tabla.ID_CARTEL == cartel.ID_CARTEL
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.TAG = cartel.TAG;
                        oConsulta.IP = cartel.IP;
                        oConsulta.PORT = cartel.PORT;
                        oConsulta.CONECTADO = cartel.CONECTADO;
                        oConsulta.PASO_ACTUAL = cartel.PASO_ACTUAL;
                        oConsulta.MENSAJE_ACTUAL = cartel.MENSAJE_ACTUAL;

                        oContext.SaveChanges();
                        return cartel;
                    }
                    else
                    {
                        return new CARTEL_MULTILED();
                    }

                }
            }
            catch (Exception ex)
            {
                return new CARTEL_MULTILED();
            }
        }

        public bool update_mensaje(string tag, string mensaje, Boolean estado)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {

                    var oConsulta = (from tabla in oContext.CARTEL_MULTILED
                                     where tabla.TAG == tag
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        //oConsulta.mensaje = mensaje;
                        //oConsulta.estado = estado;

                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public CARTEL_MULTILED getOne(string ip)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CARTEL_MULTILED.Where(a => a.IP == ip).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new CARTEL_MULTILED();
            }
        }

        public CARTEL_MULTILED getOneTag(string tag, Int32 idControlAcceso)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CARTEL_MULTILED.Where(a => a.TAG == tag && a.ID_CARTEL == idControlAcceso ).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new CARTEL_MULTILED();
            }
        }

        public List<CARTEL_MULTILED> getAll()
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CARTEL_MULTILED.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<CARTEL_MULTILED>();
            }
        }


        public List<CARTEL_MULTILED> getAllSector(Int32 idControlAcceso)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CARTEL_MULTILED.Where(a=>a.ID_CONTROL_ACCESO == idControlAcceso).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<CARTEL_MULTILED>();
            }
        }


        public List<CARTEL_MULTILED> getAllTag(string tag)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CARTEL_MULTILED.Where(a=>a.TAG== tag).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<CARTEL_MULTILED>();
            }
        }
    }
}
