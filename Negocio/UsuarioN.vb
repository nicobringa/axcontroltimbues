﻿Public Class UsuarioN
    Public Shared oBD As New AccesoDatos.UsuarioAD
    ''' <summary>
    ''' Permite agregar un usuario
    ''' </summary>
    ''' <param name="nombre">nombre de usuario</param>
    ''' <param name="contrasenia">Contraseña del usuario</param>
    ''' <param name="idCategoria">Indice de la categoria a la que pertenece</param>
    ''' <remarks></remarks>
    Public Shared Function AddUsuario(ByVal nombre As String, ByVal contrasenia As String, idCategoria As Integer) As Entidades.usuario
        Dim oUsuario As New Entidades.USUARIO

        oUsuario.NOMBRE = nombre
        oUsuario.CONTRASENIA = contrasenia
        oUsuario.ID_CATEGORIA_USUARIO = idCategoria

        Return oBD.add(oUsuario)
    End Function

    Public Shared Function getPorNombreCuenta(ByVal StrNombreCuenta As String) As Boolean
        Dim oUsuarioAD As New AccesoDatos.UsuarioAD
        Return oUsuarioAD.getNombreCuenta(StrNombreCuenta)
    End Function


    Public Function getOne(ByVal StrNombreCuenta As String, ByVal StrContrasena As String) As Entidades.usuario
        Dim oUsuarioAD As New AccesoDatos.UsuarioAD
        Return oUsuarioAD.getOne(StrNombreCuenta, StrContrasena)
    End Function
End Class
