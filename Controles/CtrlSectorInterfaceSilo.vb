﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Text
Imports System.Threading
Public Class CtrlSectorInterfaceSilo


#Region "PROPIEDADES"

    ''' <summary>
    ''' Id generado por BIT. Unívoco por cada operación realizada.
    ''' </summary>
    Private _IdComando As Entidades.DATO_DWORDDINT
    Public Property IdComando As DATO_DWORDDINT
        Get
            Return _IdComando
        End Get
        Set(ByVal value As Entidades.DATO_DWORDDINT)
            _IdComando = value
        End Set
    End Property
    ''' <summary>
    ''' 1= Modificación. No se usará el Alta, ya que previamente los Silos existirán en el PCS7. TRIGGER(debe ser monitoreado por Electroluz)
    ''' </summary>
    Private _Comando As DATO_WORDINT
    Public Property Comando As DATO_WORDINT
        Get
            Return _Comando
        End Get
        Set(ByVal value As DATO_WORDINT)
            _Comando = value
        End Set
    End Property
    ''' <summary>
    ''' Identificador único por cada silo de la planta
    ''' </summary>
    Private _IdSilo As DATO_WORDINT
    Public Property IdSilo As DATO_WORDINT
        Get
            Return _IdSilo
        End Get
        Set(ByVal value As DATO_WORDINT)
            _IdSilo = value
        End Set
    End Property
    ''' <summary>
    ''' Nombre del Silo
    ''' </summary>
    Private _Silo As DATO_BYTE
    Public Property Silo() As DATO_BYTE
        Get
            Return _Silo
        End Get
        Set(ByVal value As DATO_BYTE)
            _Silo = value
        End Set
    End Property
    ''' <summary>
    ''' Identificador único del producto contenido en el silo
    ''' </summary>
    Private _IdProducto As DATO_WORDINT
    Public Property IdProducto As DATO_WORDINT
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As DATO_WORDINT)
            _IdProducto = value
        End Set
    End Property
    ''' <summary>
    '''Nombre del Producto
    ''' </summary>
    Private _Producto As DATO_BYTE
    Public Property Producto As DATO_BYTE
        Get
            Return _Producto
        End Get
        Set(ByVal value As DATO_BYTE)
            _Producto = value
        End Set
    End Property
    ''' <summary>
    ''' Capacidad total en Kg del Silo
    ''' </summary>
    Private _Capacidad As DATO_DWORDDINT
    Public Property Capacidad As DATO_DWORDDINT
        Get
            Return _Capacidad
        End Get
        Set(ByVal value As DATO_DWORDDINT)
            _Capacidad = value
        End Set
    End Property
    ''' <summary>
    ''' Existencia total en Kg del producto en el Silo
    ''' </summary>
    Private _ExistenTotal As DATO_DWORDDINT
    Public Property ExistenTotal As DATO_DWORDDINT
        Get
            Return _ExistenTotal
        End Get
        Set(ByVal value As DATO_DWORDDINT)
            _ExistenTotal = value
        End Set
    End Property
    ''' <summary>
    ''' Id generado por BIT. Utilizado por axControl para retornar la respuesta haciendo referencia al Id de comando generado por BIT
    ''' </summary>
    Private _IdComandoRta As DATO_DWORDDINT
    Public Property IdComandoRta As DATO_DWORDDINT
        Get
            Return _IdComandoRta
        End Get
        Set(ByVal value As DATO_DWORDDINT)
            _IdComandoRta = value
        End Set
    End Property
    ''' <summary>
    ''' 1= Procesado, 2=Error en axControl, 3=Error en PLC S71200, 4=Error en PCS7, 5=TimeOut. TRIGGER(debe ser monitoreado por AuMax)
    ''' </summary>
    Private _EstadoRta As DATO_WORDINT
    Public Property EstadoRta As DATO_WORDINT
        Get
            Return _EstadoRta
        End Get
        Set(ByVal value As DATO_WORDINT)
            _EstadoRta = value
        End Set
    End Property
    ''' <summary>
    '''  Mensaje para describir el estado de la respuesta
    ''' </summary>
    Private _MensajeRta As DATO_BYTE
    Public Property MensajeRta As DATO_BYTE
        Get
            Return _MensajeRta
        End Get
        Set(ByVal value As DATO_BYTE)
            _MensajeRta = value
        End Set
    End Property
    ''' <summary>
    ''' Identificador único por cada silo de la planta
    ''' </summary>
    Private _IdSiloAlarma As DATO_WORDINT
    Public Property IdSiloAlarma As DATO_WORDINT
        Get
            Return _IdSiloAlarma
        End Get
        Set(ByVal value As DATO_WORDINT)
            _IdSiloAlarma = value
        End Set
    End Property
    ''' <summary>
    ''' 1= Nivel Bajo en Silo, 2= Nivel Alto en Silo, ….. (a definir entre BIT y Electroluz)
    ''' </summary>
    Private _IdAlarma As DATO_WORDINT
    Public Property IdAlarma As DATO_WORDINT
        Get
            Return _IdAlarma
        End Get
        Set(ByVal value As DATO_WORDINT)
            _IdAlarma = value
        End Set
    End Property
    ''' <summary>
    ''' Mensaje de la Alarma
    ''' </summary>
    Private _MensjaeAlarma As DATO_BYTE
    Public Property MensajeAlarma As DATO_BYTE
        Get
            Return _MensjaeAlarma
        End Get
        Set(ByVal value As DATO_BYTE)
            _MensjaeAlarma = value
        End Set
    End Property

    ''' <summary>
    ''' Valor de la variable que genera la alarma
    ''' </summary>
    Private _ValorAlarma As DATO_FLOAT
    Public Property ValorAlarma As DATO_FLOAT
        Get
            Return _ValorAlarma
        End Get
        Set(ByVal value As DATO_FLOAT)
            _ValorAlarma = value
        End Set
    End Property


    Private _oSector As SECTOR
    Public Property oSector() As SECTOR
        Get
            Return _oSector
        End Get
        Set(ByVal value As SECTOR)
            _oSector = value
        End Set
    End Property

    Private _nDatoWord As New Negocio.DatoWordIntN
    Private _nDatoDWord As New Negocio.DatoDWordDIntN
    Private _nDatoFloat As New Negocio.DatoFloatN
    Private _nDatoByte As New Negocio.DatoByteN

    Private hMonitoreo As Thread
#End Region


    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()




    End Sub


    Public Sub New(oSector As SECTOR)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.oSector = oSector

        Inicializar()
    End Sub


#Region "METODOS"

    Public Overrides Sub Inicializar()
        MyBase.Inicializar()


        InicializarCtrl()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()

    End Sub

    Private Sub InicializarCtrl()



        CargarPropiedadesDatoWord()
        CargarPropiedadesDatoDWord()
        CargarPropiedadesDatoFloat()
        CargarPropiedadesDatoByte()

    End Sub

    ''' <summary>
    ''' Permite cargar las propiedades datoWord
    ''' </summary>
    Private Sub CargarPropiedadesDatoWord()

        'Busco los datos word para este sector
        Dim listDato As List(Of Entidades.DATO_WORDINT) = _nDatoWord.GetAllPorSector(Me.oSector.ID_SECTOR)
        'Recorro los datos word y se los asgino a la propiedad correspondiente
        For Each itemTag As Entidades.DATO_WORDINT In listDato

            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

            If tag.Equals(PROPIEDADES_INTERFACE_SILOS.COMANDO) Then
                Comando = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.ID_SILO) Then
                IdSilo = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.ID_PRODUCTO) Then
                IdProducto = itemTag

            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.ESTADO_RTA) Then
                EstadoRta = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.ID_SILO_ALARMA) Then
                IdSiloAlarma = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.ID_ALARMA) Then
                IdAlarma = itemTag
            End If
        Next



    End Sub
    ''' <summary>
    ''' Permite cargar los datos dword
    ''' </summary>
    Private Sub CargarPropiedadesDatoDWord()


        'Busco los datos word para este sector
        Dim listDato As List(Of Entidades.DATO_DWORDDINT) = _nDatoDWord.GetAllPorSector(Me.oSector.ID_SECTOR)
        'Recorro los datos word y se los asgino a la propiedad correspondiente
        For Each itemTag As Entidades.DATO_DWORDDINT In listDato

            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

            If tag.Equals(PROPIEDADES_INTERFACE_SILOS.ID_COMANDO) Then
                IdComando = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.CAPACIDAD) Then
                Capacidad = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.EXISTEN_TOTAL) Then
                ExistenTotal = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.ID_COMANDO_RTA) Then
                IdComandoRta = itemTag

            End If
        Next

    End Sub

    ''' <summary>
    ''' Permite cargar los datos dword
    ''' </summary>
    Private Sub CargarPropiedadesDatoFloat()


        'Busco los datos word para este sector
        Dim listDato As List(Of Entidades.DATO_FLOAT) = _nDatoFloat.GetAllPorSector(Me.oSector.ID_SECTOR)
        'Recorro los datos word y se los asgino a la propiedad correspondiente
        For Each itemTag As Entidades.DATO_FLOAT In listDato

            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

            If tag.Equals(PROPIEDADES_INTERFACE_SILOS.VALOR_ALARMA) Then
                ValorAlarma = itemTag


            End If
        Next

    End Sub

    ''' <summary>
    ''' Permite cargar las propiedades de tipo byte
    ''' </summary>
    Private Sub CargarPropiedadesDatoByte()


        'Busco los datos word para este sector
        Dim listDato As List(Of Entidades.DATO_BYTE) = _nDatoByte.GetAllPorSector(Me.oSector.ID_SECTOR)
        'Recorro los datos word y se los asgino a la propiedad correspondiente
        For Each itemTag As Entidades.DATO_BYTE In listDato

            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

            If tag.Equals(PROPIEDADES_INTERFACE_SILOS.SILO) Then
                Silo = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.PRODUCTO) Then
                Producto = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.MENSAJE_RTA) Then
                MensajeRta = itemTag
            ElseIf tag.Equals(PROPIEDADES_INTERFACE_SILOS.MENSAJE_ALARMA) Then
                MensajeAlarma = itemTag
            End If
        Next

    End Sub

    ''' <summary>
    ''' Busca los ws a procesar 
    ''' </summary>
    Private Sub ProcesarWS()
        Dim nInterface = New Negocio.InterfaceN

        'Busco los ws a procesar
        Dim listWS = nInterface.GetAll(WsJsonInputAumax.AltaModSilo._NOMBRE, Entidades.Constante.ESTADO_REGISTRO.SIN_PROCESAR)

        If listWS.Count > 0 Then nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "WS A PROCESAR = " & listWS.Count, Nothing)

        For Each ws In listWS 'Recorro los ws a procesar
            RegistrarDatosPLc(ws)
        Next


    End Sub
    ''' <summary>
    ''' Permite setear las propiedades para que se escriban en el plc
    ''' </summary>
    ''' <param name="oInterfaceWS"></param>
    Private Sub RegistrarDatosPLc(oInterfaceWS As INTERFACE_WS)

        Dim altaModSilo = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Entidades.WsJsonInputAumax.AltaModSilo)(oInterfaceWS.JSON_INPUT)


        Me.IdComando.VALOR = altaModSilo.idComando
        Me.Comando.VALOR = altaModSilo.comando
        Me.IdSilo.VALOR = altaModSilo.idSilo
        Me.Silo.VALOR = Encoding.ASCII.GetBytes(altaModSilo.silo)
        Me.IdProducto.VALOR = altaModSilo.idProducto
        Me.Producto.VALOR = Encoding.ASCII.GetBytes(altaModSilo.producto)
        Me.Capacidad.VALOR = altaModSilo.capacidad
        Me.ExistenTotal.VALOR = altaModSilo.existenTotal

        _nDatoDWord.Escribir(Me.IdComando)
        _nDatoWord.Escribir(Me.Comando)
        _nDatoWord.Escribir(Me.IdSilo)
        _nDatoByte.Escribir(Me.Silo)
        _nDatoWord.Escribir(Me.IdProducto)
        _nDatoByte.Escribir(Me.Producto)
        _nDatoDWord.Escribir(Me.Capacidad)
        _nDatoDWord.Escribir(Me.ExistenTotal)
        ' _nDatoWord.Escribir(Me.EstadoRta.ID, 0)
        '_nDatoWord.Escribir(Me.IdSiloAlarma)
        '_nDatoWord.Escribir(Me.IdAlarma)

        'Actualizo el estado a registrado ene l plc
        oInterfaceWS.ID_ESTADO = Entidades.Constante.ESTADO_REGISTRO.REGISTRADO_PLC
        Dim nInterface As New Negocio.InterfaceN
        nInterface.Update(oInterfaceWS)

        'Agrego el vento
        nInterface.AddEvento(Constante.ID_EVENTOS.INFORMACION, "[RegistrarDatosPLc] = " & "WS REGISTRADO EN PLC ID_REGISTRO " & oInterfaceWS.ID_REGISTRO, Nothing)

    End Sub

    ''' <summary>
    ''' Verifica si tiene una respuesta del plc de algun comando enviado
    ''' </summary>
    Private Sub LeerRtaPLC()
        Dim ninterface As New Negocio.InterfaceN
        If Me.EstadoRta.VALOR > 0 And Me.IdComandoRta.VALOR > 0 Then ' Verifica el idComandoRta para ver si el plc retorna la repuesta de alguna comando enviado
            Me.ActualizarPropiedadesLectura()
            'Obtengo los  datos de la repuesta y armo la respuesta
            Dim RtaAltaModSilo As New Entidades.WsJsonInputBit.RtaAltaModSilo
            RtaAltaModSilo.idComandoRta = Me.IdComandoRta.VALOR
            RtaAltaModSilo.idEstadoRta = Me.EstadoRta.VALOR
            If Not IsNothing(Me.MensajeRta.VALOR) Then
                RtaAltaModSilo.mensajeRta = System.Text.Encoding.Default.GetString(Me.MensajeRta.VALOR, 2, Me.MensajeRta.CANT_BYTE - 2)

                RtaAltaModSilo.mensajeRta = BorrarVbNull(RtaAltaModSilo.mensajeRta)

            End If

            'Una vez que obtengo los datos del plc pongo el idComando = 0
            _nDatoWord.Escribir(Me.EstadoRta.ID, 0)
            _nDatoDWord.Escribir(Me.IdComandoRta.ID, 0)
            Dim arraycero(50) As Byte
            _nDatoByte.Escribir(Me.MensajeRta.ID, arraycero)


            Dim hEnviarWSRtaAltaModSilo = New Thread(AddressOf EnviarWSRtaAltaModSilo)
            hEnviarWSRtaAltaModSilo.IsBackground = True
            hEnviarWSRtaAltaModSilo.Start(RtaAltaModSilo)




        End If

        If Me.IdAlarma.VALOR > 0 Then
            Me.ActualizarPropiedadesLectura()
            Dim AlarmaSilo As New Entidades.WsJsonInputBit.AlarmaSilo
            AlarmaSilo.idAlarma = Me.IdAlarma.VALOR
            AlarmaSilo.idSilo = Me.IdSiloAlarma.VALOR
            If Not IsNothing(Me.MensajeAlarma.VALOR) Then
                AlarmaSilo.mensaje = System.Text.Encoding.Default.GetString(Me.MensajeAlarma.VALOR, 2, Me.MensajeAlarma.CANT_BYTE - 2)
                AlarmaSilo.mensaje = BorrarVbNull(AlarmaSilo.mensaje)
            End If
            AlarmaSilo.valor = Me.ValorAlarma.VALOR

            'Creo el ws en la base de datos
            Dim oInterfaceWS As New Entidades.INTERFACE_WS
            oInterfaceWS.FECHA = DateTime.Now
            oInterfaceWS.ID_ESTADO = CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_ERRO, Int16) ' Pongo el estado como error porque todavia no le envie a BIT
            oInterfaceWS.WEB_SERVICE = Entidades.WsJsonInputBit.AlarmaSilo._NOMBRE
            oInterfaceWS = ninterface.Add(oInterfaceWS)

            If Not IsNothing(oInterfaceWS) Then ' Si se creo el es en la base de datos
                'Asigno el idRegistro creado en la base de datos a la Alarma de silo
                AlarmaSilo.idRegistroWS = oInterfaceWS.ID_REGISTRO
                'Pongo el idAlarma en cero en el PLC
                _nDatoWord.Escribir(Me.IdAlarma)
                'Creo un hilo para enviar la alarma por ws
                Dim hEnviarWSAlarmaSilo = New Thread(AddressOf EnviarWSAlarmaSilo)
                hEnviarWSAlarmaSilo.IsBackground = True
                hEnviarWSAlarmaSilo.Start(AlarmaSilo)
            Else
                'Si no se pudo crear el ws en al base de datos genero un evento
                ninterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                   String.Format("[LeerRtaPLC]  No se pudo crear en la Base de datos la AlarmaSilo = {0} ",
                                                AlarmaSilo.idAlarma),
                                   Nothing)
            End If




            'Enviar AlarmaSilo

        End If

    End Sub

    ''' <summary>
    ''' Actualiza las propiedades que son de lectura
    ''' Busca en la base de datos de axDriver los nuevos datos de estas propiedades
    ''' </summary>
    Private Sub ActualizarPropiedadesLectura()


        Me.IdComando = _nDatoDWord.GetOne(Me.IdComando.ID)
        Me.EstadoRta = _nDatoWord.GetOne(Me.EstadoRta.ID)
        Me.MensajeRta = _nDatoByte.GetOne(Me.MensajeRta.ID)
        Me.IdSilo = _nDatoWord.GetOne(Me.IdSilo.ID)
        Me.IdSiloAlarma = _nDatoWord.GetOne(Me.IdSiloAlarma.ID)
        Me.IdAlarma = _nDatoWord.GetOne(Me.IdAlarma.ID)
        Me.MensajeAlarma = _nDatoByte.GetOne(Me.MensajeAlarma.ID)
        Me.ValorAlarma = _nDatoFloat.GetOne(Me.ValorAlarma.ID)
        Me.IdComandoRta = _nDatoDWord.GetOne(Me.IdComandoRta.ID)

    End Sub




#End Region


#Region "SUBPROCESO"

    Private Sub Monitoreo()

        While True
            Try
                ActualizarPropiedadesLectura()

                ProcesarWS()

                LeerRtaPLC()
            Catch ex As Exception
                Dim nInterface As New Negocio.InterfaceN
                nInterface.AddEvento(Constante.ID_EVENTOS.ERROR, "[Monitoreo] = " & ex.Message & " " & ex.StackTrace, Nothing)
            End Try




            Thread.Sleep(500)
        End While
    End Sub

    ''' <summary>
    ''' Permite eviar la repuesta por ws y guardar su respuesta
    ''' </summary>
    Private Sub EnviarWSRtaAltaModSilo(wsRta As Entidades.WsJsonInputBit.RtaAltaModSilo)
        Dim nInterface = New Negocio.InterfaceN
        Try


            Dim jsonRtaAltaModSilo = Newtonsoft.Json.JsonConvert.SerializeObject(wsRta)
            Dim rta = Negocio.WebServiceBitN.RtaAltaModSilo(jsonRtaAltaModSilo)

            'Busco el ws en la base de datos, para guardar su respuesta
            Dim oInterfaceWs = nInterface.GetOne(Entidades.WsJsonInputAumax.AltaModSilo._NOMBRE, ESTADO_REGISTRO.REGISTRADO_PLC, wsRta.idComandoRta)

            If Not IsNothing(oInterfaceWs) Then
                oInterfaceWs.JSON_RPTA_PLC = jsonRtaAltaModSilo
                oInterfaceWs.ID_ESTADO = IIf(rta <> "0", CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_EXITOSO, Integer), CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_ERRO, Integer))

                nInterface.Update(oInterfaceWs)
            Else
                nInterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                     String.Format("[EnviarWSRtaAltaModSilo]  No se encontro el ws AltaModSilo con el idComando = {0} para guardar la respuesta ",
                                                   wsRta.idComandoRta),
                                     Nothing)
            End If



        Catch ex As Exception
            'Agregar evento
            nInterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                   String.Format("[EnviarWSRtaAltaModSilo]  {0}  - {1} ",
                                                 ex.Message, ex.StackTrace),
                                   Nothing)
        End Try


    End Sub

    Private Sub EnviarWSAlarmaSilo(wsAlrma As Entidades.WsJsonInputBit.AlarmaSilo)
        Dim nInterface = New Negocio.InterfaceN
        Try
            'Paso a json la alarma
            Dim jsonAlarmaSilo = Newtonsoft.Json.JsonConvert.SerializeObject(wsAlrma)
            'Lo envio por ws a bit
            Dim rta = Negocio.WebServiceBitN.AlarmaSilo(jsonAlarmaSilo)
            'Busco el ws en la base de datos, para guardar su respuesta
            Dim oInterfaceWs = nInterface.GetOne(wsAlrma.idRegistroWS)
            oInterfaceWs.JSON_INPUT = jsonAlarmaSilo

            If Not IsNothing(oInterfaceWs) Then ' Si se pudo guardar correctamente 
                If rta = "OK" Then
                    oInterfaceWs.ID_ESTADO = CType(Entidades.Constante.ESTADO_REGISTRO.ENVIO_EXITOSO, Int16)
                End If



            Else
                nInterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                     String.Format("[EnviarWSAlarmaSilo] Nos encontro el ws AlarmaSilo en la base de datos con ID_REGISTRO  = {0}",
                                                   wsAlrma.idRegistroWS),
                                     Nothing)
            End If
            nInterface.Update(oInterfaceWs)

        Catch ex As Exception
            'Agregar evento
            nInterface.AddEvento(Constante.ID_EVENTOS.ERROR,
                                   String.Format("[EnviarWSRtaAltaModSilo]  {0}  - {1} ",
                                                 ex.Message, ex.StackTrace),
                                   Nothing)
        End Try


    End Sub

    Private Sub button1_Click(sender As Object, e As EventArgs)
        Dim inputBit As New Entidades.WsJsonInputAumax.AltaModSilo

        inputBit.idComando = 2
        inputBit.comando = 1
        inputBit.idSilo = 1
        inputBit.silo = "Nombre Silo"
        inputBit.idProducto = 50
        inputBit.producto = "Soja"
        inputBit.capacidad = 100
        inputBit.existenTotal = 50


        Dim ointerfaceWs As New Entidades.INTERFACE_WS


        ointerfaceWs.FECHA = Date.Now
        ointerfaceWs.ID_COMANDO = inputBit.idComando
        ointerfaceWs.WEB_SERVICE = Entidades.WsJsonInputAumax.AltaModSilo._NOMBRE
        ointerfaceWs.ID_ESTADO = Entidades.Constante.ESTADO_REGISTRO.SIN_PROCESAR
        ointerfaceWs.JSON_INPUT = Newtonsoft.Json.JsonConvert.SerializeObject(inputBit)

        Dim nInterface As New Negocio.InterfaceN
        nInterface.Add(ointerfaceWs)


    End Sub
#End Region


End Class
