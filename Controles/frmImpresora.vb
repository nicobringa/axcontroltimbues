﻿Imports Negocio
Imports Entidades
Imports System.IO.Ports
Imports System.Windows.Forms
Imports Entidades.Constante

Public Class frmImpresora
#Region "Variables de Instancia"
    Dim oImpresora As Entidades.Impresora
    Dim nImpresora As Negocio.ImpresoraN
    Dim oldIp As String
#End Region
#Region "Eventos"

    Public Event Update_(ByVal impresora As Entidades.Impresora)
#End Region

    Public Sub New(ByVal impresora As Entidades.Impresora)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()


        nImpresora = New ImpresoraN
        Me.oldIp = impresora.ip
        oImpresora = impresora
        Consultar()
        AddHandler ImpresoraSTAR.MsjError, AddressOf MsjErrorImpresora
    End Sub


    Private Sub Consultar()

        txtIP.Text = Me.oImpresora.ip
        txtPuerto.Text = Me.oImpresora.port

    End Sub

    Public Sub SeleccionarItemCombo(ByVal CmbCombo As ComboBox, ByVal Id As Integer)
        For I As Integer = 0 To CmbCombo.Items.Count - 1
            CmbCombo.SelectedIndex = I
            If CType(CmbCombo.SelectedItem, ItemCombo).Id = Id Then
                I = CmbCombo.Items.Count - 1
            End If
        Next
    End Sub



    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            GuardarDatos()
            RaiseEvent Update_(oImpresora)
        Else
            Me.Close()
        End If
    End Sub

    Private Function GuardarDatos() As Boolean
        Try

            oImpresora.ip = txtIP.Text
            oImpresora.port = CInt(txtPuerto.Text)
            Dim descripAuditoria As String
            descripAuditoria = "Configuro Impresora: " + oImpresora.ip
            Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionImpresora, 0)
            oImpresora = nImpresora.Update_Config(oImpresora)
            If oImpresora.ip <> "" Then
                MsgBox("Configuración de la impresora actualizada correctamente", MsgBoxStyle.Information, "axControl")
                Return True
            Else
                MsgBox("La Configuración de la impresora no se pudo actualizar. Intente nuevamente o solicite asistencia técnica", MsgBoxStyle.Exclamation, "axControl")
                Return False
            End If
        Catch ex As Exception
            MsgBox("Los valores de Configuración de la impresora no corresponden. Revise e intente nuevamente.", MsgBoxStyle.Exclamation, "axControl")
            Return False
        Finally
            Me.Close()
        End Try

    End Function

    Private Sub btnEscribirCartel_Click(sender As Object, e As EventArgs) Handles btnEscribirCartel.Click
        ImpresoraSTAR.RecibirDatos("PRUEBA", "PRUEBA, PRUEBA, PRUEBA", "645342")
        ImpresoraSTAR.Imprimir()
    End Sub


    Private Sub MsjErrorImpresora(tag As String, msj As String)

        MessageBox.Show(msj, tag, MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub
End Class