﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class PlcAD
    {
        public PLC add(PLC oPlc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    oContext.PLC.Add(oPlc);
                    oContext.SaveChanges();
                    return oPlc;
                }
            }
            catch (Exception ex)
            {
                return new PLC();
            }
        }

        public bool delete(PLC oPlc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.PLC.Where(a => a.ID == oPlc.ID).FirstOrDefault();
                    if (oConsulta != null)
                    {
                        oContext.PLC.Remove(oConsulta);
                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public PLC update(PLC oPlc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.PLC
                                     where tabla.ID == oPlc.ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.AUTO_CONECTAR = oPlc.AUTO_CONECTAR;
                        oConsulta.DESCRIPCION = oPlc.DESCRIPCION;
                        oConsulta.ESTADO = oPlc.ESTADO;
                        oConsulta.IP = oPlc.IP;
                        oConsulta.NOMBRE = oPlc.NOMBRE;
                        oConsulta.PUERTO = oPlc.PUERTO;
                        oConsulta.RACK = oPlc.RACK;
                        oConsulta.SLOT = oPlc.SLOT;
                        oConsulta.TIEMPO_RECONECT = oPlc.TIEMPO_RECONECT;
                        oConsulta.TIEMPO_SCAN = oPlc.TIEMPO_SCAN;
                        oContext.SaveChanges();
                        return oPlc;
                    }
                    else
                    {
                        return new PLC();
                    }

                }
            }
            catch (Exception ex)
            {
                return new PLC();
            }
        }

        public PLC getOne(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.PLC.Where(a => a.ID == id).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new PLC();
            }
        }

        public PLC getOneIP(string ip)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.PLC.Where(a => a.IP == ip).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new PLC();
            }
        }

        public PLC getOne(string nombre)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.PLC.Where(a => a.NOMBRE == nombre).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new PLC();
            }
        }

        public List<PLC> getAll()
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.PLC.OrderByDescending(a => a.NOMBRE).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<PLC>();
            }
        }
    }
}
