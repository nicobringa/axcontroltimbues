﻿Imports Entidades
Imports AccesoDatos
Imports System.Net

Public Class CartelMultiLedN
    Public Function Update_Config(ByVal ocartel As CARTEL_MULTILED) As CARTEL_MULTILED
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.update_config(ocartel)
    End Function

    Public Function Update_Mensaje(ByVal tag As String, ByVal mensaje As String, ByVal estado As Boolean) As Boolean
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.update_mensaje(tag, mensaje, estado)
    End Function

    Public Function GetOne(ByVal id As Integer) As CARTEL_MULTILED
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.getOne(id)
    End Function

    Public Function GetOne(ByVal ip As String) As CARTEL_MULTILED
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.getOne(ip)
    End Function

    Public Function GetOneTag(ByVal tag As String, ByVal idSector As Integer) As CARTEL_MULTILED
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.getOneTag(tag, idSector)
    End Function


    Public Function GetAll() As List(Of CARTEL_MULTILED)
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.getAll
    End Function

    Public Function GetAllSector(ByVal idSector As Int32) As List(Of CARTEL_MULTILED)
        Dim oCartelAD As New CartelMultiLedAD
        Return oCartelAD.getAllSector(idSector)
    End Function

    Public Enum ComandosCartel
        InicioTrama1 = 2
        InicioTrama2 = 16
        FinTrama1 = 16
        FinTrama2 = 3
        CambiarPrograma = 48 '0x30 : Cambiar programa actual.
        EscribirVariable = 49 '0x31 : Escribir variable.
        CambioPaso = 51 '0x33 : Cambio de Step
        ConfigWatchDogTimer = 56 '0x38 : Config el Watchdog timer
    End Enum

    ''' <summary>
    ''' Permite agregar una byte a la trama
    ''' </summary>
    Private Sub AgregarByteTrama(ByRef Trama() As Byte, numAscii As Decimal)
        Array.Resize(Trama, Trama.Length + 1)
        Trama(Trama.Length - 1) = numAscii
    End Sub

    Public Function CalcularChesckSum(ByRef Trama() As Byte) As Integer

        Dim LRC As Integer = 0

        For index As Integer = 2 To Trama.Length - 1
            LRC = (LRC + Trama(index))

        Next
        LRC = LRC And 255
        LRC = (((LRC Xor 255) + 1) And 255)

        Return LRC
    End Function

    Public Sub CheckSumTo2Byte(checkSum As Integer, ByRef asciiCheck1 As Integer, ByRef asciiCheck2 As Integer)

        Dim strHex As String = Hex(checkSum)

        Dim check1 As String = strHex.Substring(0, 1)
        Dim check2 As String
        If strHex.Length = 1 Then
            check2 = "0"
        Else
            check2 = strHex.Substring(1, 1)
        End If

        asciiCheck1 = Asc(check1)
        asciiCheck2 = Asc(check2)

    End Sub

    ''' <summary>
    ''' Función para escribir variable en cartel
    ''' </summary>
    ''' <param name="mensaje"></param>
    ''' <param name="nroVariable"></param>
    ''' <param name="Trama0"></param>
    ''' <param name="Trama1"></param>
    ''' <param name="IP"></param>
    ''' <param name="PORT"></param>
    ''' <returns></returns>
    Public Function escribirVar(mensaje As String, nroVariable As Integer, Trama0 As Decimal, Trama1 As Decimal, IP As String, PORT As String) As Boolean
        Try
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {Convert.ToDecimal(ComandosCartel.InicioTrama1), Convert.ToDecimal(ComandosCartel.InicioTrama2), Convert.ToDecimal(ComandosCartel.EscribirVariable)}
            'Variable
            Dim var0 As Decimal = 48 '0
            Dim var1 As Decimal
            Select Case nroVariable
                Case 1
                    var1 = 49
                Case 2
                    var1 = 50
                Case 3
                    var1 = 51
                Case Else
            End Select
            'Agrego la variable 01, porque el cartel necesita 2 bytes para la variable
            AgregarByteTrama(sendBytes, var0)
            AgregarByteTrama(sendBytes, var1)
            'Recorro el mensaje y voy agregando caracter por caracter
            While mensaje.Length > 0
                Dim letraMsj As Decimal = Asc(mensaje.Substring(0, 1).ToString())
                AgregarByteTrama(sendBytes, letraMsj)
                mensaje = mensaje.Substring(1, mensaje.Length - 1)
            End While
            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)
            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama1))
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama2))
            Dim udpClient As New Net.Sockets.UdpClient()
            udpClient.Connect(IP, PORT)
            udpClient.Send(sendBytes, sendBytes.Length)
            Dim groupEP As New IPEndPoint(IPAddress.Parse(IP), PORT)
            udpClient.Client.ReceiveTimeout = 1000
            Try
                Dim bytes As Byte() = udpClient.Receive(groupEP)
                If bytes(2) = 54 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Función para cambiar el paso del cartel
    ''' </summary>
    ''' <param name="nroPaso"></param>
    ''' <param name="Trama0"></param>
    ''' <param name="Trama1"></param>
    ''' <param name="IP"></param>
    ''' <param name="PORT"></param>
    ''' <returns></returns>
    Public Function CambiarStep(nroPaso As Integer, Trama0 As Decimal, Trama1 As Decimal, IP As String, PORT As String) As Boolean
        Try
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {Convert.ToDecimal(ComandosCartel.InicioTrama1), Convert.ToDecimal(ComandosCartel.InicioTrama2), Convert.ToDecimal(ComandosCartel.CambioPaso)}

            Dim NroStep0 As Decimal = 48 '0
            Dim NroStep1 As Decimal
            Select Case nroPaso
                Case 0 'Fondo Negro Mensaje SISTEMA DETENIDO
                    NroStep1 = 48
                Case 1 'Marco Blanco Mensaje ACA TIMBUES
                    NroStep1 = 49
                Case 2 'Marco Verde Mensaje AVANCE
                    NroStep1 = 50
                Case 3 'Marco Rojo Mensaje RETROCEDA
                    NroStep1 = 51
                Case 4 'Marco Amarillo Mensaje ESPERE
                    NroStep1 = 52
                Case 5 'Marco Blanco Mensaje Pasante Variable
                    NroStep1 = 53
                Case 6 'TODO VERDE
                    NroStep1 = 54
                Case 7 'TODO ROJO
                    NroStep1 = 55
                Case 8 'FUERA DE SERVICIO
                    NroStep1 = 56
            End Select
            'Agrego la variable 01, porque el cartel necesita 2 bytes para la variable
            AgregarByteTrama(sendBytes, NroStep0)
            AgregarByteTrama(sendBytes, NroStep1)
            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)
            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)
            'Agrego el fin de trama
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama1))
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama2))
            Dim udpClient As New Net.Sockets.UdpClient()
            udpClient.Connect(IP, PORT)
            udpClient.Send(sendBytes, sendBytes.Length)
            Dim groupEP As New IPEndPoint(IPAddress.Parse(IP), PORT)
            udpClient.Client.ReceiveTimeout = 1000
            Try
                Dim bytes As Byte() = udpClient.Receive(groupEP)
                If bytes(2) = 54 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Establece el tiempo del WatchdogDimer.
    ''' </summary>
    ''' <param name="Minutos"></param>
    ''' <param name="Trama0"></param>
    ''' <param name="Trama1"></param>
    ''' <param name="IP"></param>
    ''' <param name="PORT"></param>
    ''' <returns></returns>
    Public Function EstablecerWatchDogTimer(Minutos As Integer, Trama0 As Decimal, Trama1 As Decimal, IP As String, PORT As String)
        Try
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {Convert.ToDecimal(ComandosCartel.InicioTrama1), Convert.ToDecimal(ComandosCartel.InicioTrama2), Convert.ToDecimal(ComandosCartel.ConfigWatchDogTimer)}
            'Creo el decimal que ACTIVA el Watchdog Timer
            Dim Activado As Decimal = 49
            'Agrego el decimal de activación del Watchdog timer a la trama
            AgregarByteTrama(sendBytes, Activado)
            'Creo los minutos (dos bytes) para establecer tiempo de Watchdog timer
            Dim Min0 As Decimal = 48 '0x30 en HEX, es igual a 0
            Dim Min1 As Decimal = 49 '0x31 en HEX, es igual a 1
            Select Case Minutos
                Case 1
                    Min1 = 49 '0x31 en HEX, es igual a 1
                Case 2
                    Min1 = 50 '0x32 en HEX, es igual a 2
                Case 3
                    Min1 = 51 '0x33 en HEX, es igual a 3
                Case Else
                    Min1 = 51 '0x33 en HEX, es igual a 3
            End Select
            'Agrego los minutos a la trama
            AgregarByteTrama(sendBytes, Min0)
            AgregarByteTrama(sendBytes, Min1)
            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)
            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)
            'Agrego el fin de la trama
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama1))
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama2))

            Dim udpClient As New Net.Sockets.UdpClient()
            udpClient.Connect(IP, PORT)
            udpClient.Send(sendBytes, sendBytes.Length)
            Dim groupEP As New IPEndPoint(IPAddress.Parse(IP), PORT)
            udpClient.Client.ReceiveTimeout = 1000
            Try
                Dim bytes As Byte() = udpClient.Receive(groupEP)
                If bytes(2) = 54 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        Catch ex As Exception
            Return False
        End Try
    End Function


    ''' <summary>
    ''' Función para cambiar el programa del cartel
    ''' </summary>
    ''' <param name="NroPrograma"></param>
    ''' <param name="Trama0"></param>
    ''' <param name="Trama1"></param>
    ''' <param name="IP"></param>
    ''' <param name="PORT"></param>
    ''' <returns></returns>
    Public Function CambioPrograma(NroPrograma As Integer, Trama0 As Decimal, Trama1 As Decimal, IP As String, PORT As String)
        Try
            'Creo el vector de bytes con los parametros para el cambio de programa
            Dim sendBytes() As Byte = {Convert.ToDecimal(ComandosCartel.InicioTrama1), Convert.ToDecimal(ComandosCartel.InicioTrama2), Convert.ToDecimal(ComandosCartel.CambiarPrograma)}

            'Agrego los bytes del programa correspondiente
            Dim NroPrograma0 As Decimal = 48
            Dim NroPrograma1 As Decimal
            Select Case NroPrograma
                Case 0
                    NroPrograma1 = 48
                Case 1
                    NroPrograma1 = 49
                Case Else
                    NroPrograma1 = 48
            End Select
            'Agrego los nro de programa al vector
            AgregarByteTrama(sendBytes, NroPrograma0)
            AgregarByteTrama(sendBytes, NroPrograma1)
            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)
            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama1))
            AgregarByteTrama(sendBytes, Convert.ToDecimal(ComandosCartel.FinTrama2))
            Dim udpClient As New Net.Sockets.UdpClient()
            udpClient.Connect(IP, PORT)
            udpClient.Send(sendBytes, sendBytes.Length)
            Dim groupEP As New IPEndPoint(IPAddress.Parse(IP), PORT)
            udpClient.Client.ReceiveTimeout = 1000
            Try
                Dim bytes As Byte() = udpClient.Receive(groupEP)
                If bytes(2) = 54 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        Catch ex As Exception
            Return False
        End Try
    End Function


End Class
