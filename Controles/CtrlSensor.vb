﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Public Class CtrlSensor
    Inherits CtrlAutomationObjectsBase


#Region "Variables de Instancia"
    Dim oPLC As Plc
    Dim oPLCN As PlcN
    Public oEstado As DATO_WORDINT
    Public oHabilitacion As DATO_WORDINT
    Dim oTiempoRet As DATO_WORDINT
    Dim oDatoIntN As DatoWordIntN
    Private WithEvents ofrmBarrera As frmBarrera
#End Region

#Region "Propiedades"
    Private EstadoBarrera_ As Boolean
    Public Property Estado() As Boolean
        Get
            Return EstadoBarrera_
        End Get
        Set(ByVal value As Boolean)
            EstadoBarrera_ = value
        End Set
    End Property



#End Region

#Region "Eventos"
    Public Event UsoManual()
#End Region

#Region "Métodos"
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oPLC = New PLC
        oPLCN = New PlcN

        'oEstado = New DATO_WORDINT
        'oComando = New DATO_WORDINT
        'oInfrarrojo = New DATO_WORDINT
        oDatoIntN = New DatoWordIntN
        ofrmBarrera = New frmBarrera

    End Sub

    Public Overrides Sub Inicializar(ID_PLC As Integer?, ID_CONTROL_ACCESO As Integer, ID_SECTOR As Integer)

        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_SECTOR = ID_SECTOR

        'Si el control de acceso no tiene plc asignado significa es que trabaja con el GPIO del lector RFID
        If IsNothing(ID_PLC) Then
            Me.GPIO = True
            Me.Inicializado = True
            Return
        End If
        Me.ID_PLC = ID_PLC
        oPLC = oPLCN.GetOne(Me.ID_PLC)
        If oPLC Is Nothing Then
            Me.EP.SetError(Me.lbTAG, "No existe el PLC " & PLC & ". Revise la configuración en axDriverS7.")
        Else
            Me.lbTAG.Text = Me.Name

            Dim oTags As New List(Of DATO_WORDINT)

            Dim oTagN As New Negocio.DatoWordIntN


            oTags = oTagN.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)


            For Each itemTag As Entidades.DATO_WORDINT In oTags
                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

                If tag.Equals(PROPIEDADES_TAG_PLC.ESTADO) Then
                    oEstado = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC.HABILITACION) Then
                    oHabilitacion = itemTag

                ElseIf tag.Equals(PROPIEDADES_TAG_PLC.TIEMPO_RET) Then
                    oTiempoRet = itemTag
                End If
            Next

            Dim tmp As String
            If IsNothing(oEstado) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7", Me.Tag, PROPIEDADES_TAG_PLC.ESTADO, Me.PLC)
                Me.EP.SetError(Me.lbTAG, tmp)
                Negocio.modDelegado.SetEnable(Me, btnConfig, False)
                Return
            End If

            If IsNothing(oHabilitacion) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7", Me.Tag, PROPIEDADES_TAG_PLC.HABILITACION, Me.PLC)
                Me.EP.SetError(Me.lbTAG, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If

            If IsNothing(oTiempoRet) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7", Me.Tag, PROPIEDADES_TAG_PLC.TIEMPO_RET, Me.PLC)
                Me.EP.SetError(Me.lbTAG, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If

        End If

        Me.Inicializado = True
    End Sub

    'Public Sub SetEstado()

    '    If Not Me.Gpio Then 'No esta trabajando con el GPIO
    '        SetEstadoPLC()
    '    Else 'Trabaja con el GPIO
    '        SetEstadoGpio()
    '    End If

    'End Sub

    Public Sub Refrescar()
        oEstado = oDatoIntN.GetOne(ID_SECTOR, oEstado.TAG)
    End Sub

    ''' <summary>
    ''' Permite obter
    ''' </summary>
    Public Sub SetEstadoPLC()

        If IsNothing(oEstado) Then
            Inicializar(Me.ID_PLC, Me.ID_CONTROL_ACCESO, Me.ID_SECTOR)
            Return
        End If

        oEstado = oDatoIntN.GetOne(ID_SECTOR, oEstado.TAG)
        If oEstado Is Nothing Then
            Me.EP.SetError(Me.lbTAG, "No existe el tag " & Me.Tag & ".Estado en el PLC " & Me.PLC & ". Revise la configuración en axDriverS7.")
        Else
            If oEstado.QC <> QC.QC_OK Then
                ' Me.EP.SetError(Me.lbTAG, "La variable " & oEstado.tag & " del PLC " & Me.PLC & " no puede ser leida correctamente( QC = " & oEstado.qc.ToString & "). Revise la configuración en axDriverS7.")
            Else

                'Me.EP.SetError(Me.lbTAG, Nothing)

                Select Case oEstado.VALOR

                    Case SensorInfrarojoEstado.Sinprecencia
                        modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.Green)
                        modDelegado.SetPictureVisible(Me, pbSensorDeshab, False)
                        modDelegado.SetBackColorCtrl(Me, Me, Color.Green)

                    Case SensorInfrarojoEstado.ConPresencia
                        modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.Red)
                        modDelegado.SetPictureVisible(Me, pbSensorDeshab, False)
                        modDelegado.SetBackColorCtrl(Me, Me, Color.Red)

                    Case SensorInfrarojoEstado.Deshabilitado
                        modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.OrangeRed)
                        modDelegado.SetPictureVisible(Me, pbSensorDeshab, True)
                        modDelegado.SetBackColorCtrl(Me, Me, Color.Transparent)

                End Select
            End If

        End If
    End Sub

    Public Sub SetEstadoGpio(oSpeedWay As Negocio.LectorRFID_SpeedWay, TipoConex As Entidades.Constante.TipoConexion)
        If Not Me.GPIO Then Return
        'obtengo el puerto asignado para el sector del control
        Dim nPuerto As New Negocio.Puerto_RfidN
        If IsNothing(oSpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0)) Then Throw New Exception("[CtrlSensor] No tiene configuración el lector RFID " & oSpeedWay.Nombre)
        Dim oPuerto As Entidades.PUERTO_RFID = nPuerto.GetOneSector(oSpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID, Me.Name)
        Me.ID_SECTOR = oPuerto.ID_SECTOR
        'El sector no tiene puerto asignado
        If IsNothing(oPuerto) Then Return
        Dim ValorPuerto As Boolean
        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then 'Es servidor
            'Obtengo el estado del puerto del lector RFID
            ValorPuerto = oSpeedWay.GetOneEtadoSensor(oPuerto.NUM_PUERTO)

        Else 'Cliente
            ValorPuerto = oPuerto.ESTADO
        End If


        If IsNothing(oEstado) Then oEstado = New DATO_WORDINT()
        Dim EstadoNormal As Boolean = If(TipoConex = TipoConexion.NormalAbierto, False, True)

        oPuerto.estado = ValorPuerto
        If ValorPuerto = EstadoNormal Then
            oEstado.valor = SensorInfrarojoEstado.Sinprecencia
            modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.Green)
            modDelegado.SetPictureVisible(Me, pbSensorDeshab, False)
            modDelegado.SetBackColorCtrl(Me, Me, Color.Green)

        Else 'Cortando
            oEstado.valor = SensorInfrarojoEstado.ConPresencia
            modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.Red)
            modDelegado.SetPictureVisible(Me, pbSensorDeshab, False)
            modDelegado.SetBackColorCtrl(Me, Me, Color.Red)

        End If

        If ModSesion.PUESTO_TRABAJO.servidor Then nPuerto.Update(oPuerto)

    End Sub

    Private Sub btnConfig_Click(sender As System.Object, e As System.EventArgs) Handles btnConfig.Click

        If Not IsNothing(oEstado) Then

            Dim frm As New frmSensor(oEstado, oTiempoRet, oHabilitacion, Me.Name)
            frm.ShowDialog()

        Else
            MsgBox("No se pudo obtener el estado de la barrera , intente nuevamente", MsgBoxStyle.Exclamation, "axControl")
        End If

    End Sub

    Private Sub UsoManual_() Handles ofrmBarrera.UsoManual
        RaiseEvent UsoManual()
    End Sub
#Region "Delegados"
    Delegate Sub SetFrmEstadoCallback(ByVal [frm] As System.Windows.Forms.Form, ByVal [estado] As Integer)


#End Region

#End Region

End Class
