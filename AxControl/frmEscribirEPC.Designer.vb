﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEscribirEPC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEscribirEPC))
        Me.txtEpcActual = New System.Windows.Forms.TextBox()
        Me.btnLeer = New System.Windows.Forms.Button()
        Me.txtEscribirEPC = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnEscribir = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtIpLector = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtEpcActual
        '
        Me.txtEpcActual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEpcActual.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEpcActual.Location = New System.Drawing.Point(151, 26)
        Me.txtEpcActual.MaxLength = 24
        Me.txtEpcActual.Name = "txtEpcActual"
        Me.txtEpcActual.Size = New System.Drawing.Size(298, 26)
        Me.txtEpcActual.TabIndex = 30
        '
        'btnLeer
        '
        Me.btnLeer.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnLeer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLeer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeer.ForeColor = System.Drawing.Color.White
        Me.btnLeer.Location = New System.Drawing.Point(485, 21)
        Me.btnLeer.Name = "btnLeer"
        Me.btnLeer.Size = New System.Drawing.Size(99, 31)
        Me.btnLeer.TabIndex = 29
        Me.btnLeer.Text = "Leer"
        Me.btnLeer.UseVisualStyleBackColor = False
        '
        'txtEscribirEPC
        '
        Me.txtEscribirEPC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEscribirEPC.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEscribirEPC.Location = New System.Drawing.Point(151, 114)
        Me.txtEscribirEPC.MaxLength = 24
        Me.txtEscribirEPC.Name = "txtEscribirEPC"
        Me.txtEscribirEPC.Size = New System.Drawing.Size(298, 26)
        Me.txtEscribirEPC.TabIndex = 32
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(32, 28)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(110, 24)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "EPC Actual:"
        '
        'btnEscribir
        '
        Me.btnEscribir.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnEscribir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEscribir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEscribir.ForeColor = System.Drawing.Color.White
        Me.btnEscribir.Location = New System.Drawing.Point(485, 112)
        Me.btnEscribir.Name = "btnEscribir"
        Me.btnEscribir.Size = New System.Drawing.Size(99, 31)
        Me.btnEscribir.TabIndex = 34
        Me.btnEscribir.Text = "Escribir"
        Me.btnEscribir.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(17, 116)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 24)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Escribir EPC :"
        '
        'txtIpLector
        '
        Me.txtIpLector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIpLector.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIpLector.Location = New System.Drawing.Point(151, 73)
        Me.txtIpLector.MaxLength = 24
        Me.txtIpLector.Name = "txtIpLector"
        Me.txtIpLector.Size = New System.Drawing.Size(298, 26)
        Me.txtIpLector.TabIndex = 36
        Me.txtIpLector.Text = "192.168.226.150"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(55, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 24)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "Ip Lector:"
        '
        'frmEscribirEPC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 174)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtIpLector)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnEscribir)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtEscribirEPC)
        Me.Controls.Add(Me.txtEpcActual)
        Me.Controls.Add(Me.btnLeer)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEscribirEPC"
        Me.Text = "Escribir EPC"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtEpcActual As TextBox
    Friend WithEvents btnLeer As Button
    Friend WithEvents txtEscribirEPC As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents btnEscribir As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtIpLector As TextBox
    Friend WithEvents Label2 As Label
End Class
