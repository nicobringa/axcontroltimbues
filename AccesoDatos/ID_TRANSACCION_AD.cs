﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using System.Data.SqlClient;
using System.Data;

namespace AccesoDatos
{
   public class ID_TRANSACCION_AD
    {

        SqlConnection cnn;
        SqlCommand sqlCmd;

        public void ConectarBD()
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings["axControl_InterfaceEntities"].ConnectionString;
            cnn = new SqlConnection(connection);

        }

        public Int64 getId_Transaccion()
        {
            try
            {
                if (cnn == null)
                {
                    ConectarBD();
                }

                cnn.Open();
                sqlCmd = new SqlCommand();
                sqlCmd.Connection = cnn;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "SECUENCIA_ID_TRANSACCION";

                Int64 idTransaccion = (Int64)sqlCmd.ExecuteScalar();

                return idTransaccion;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}
