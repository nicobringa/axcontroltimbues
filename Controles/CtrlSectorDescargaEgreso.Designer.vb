﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorDescargaEgreso
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlSectorDescargaEgreso))
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnLecturaManual = New System.Windows.Forms.Button()
        Me.DESCARGA = New Controles.CtrlDescarga()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlAtenaLeyendo = New Controles.CtrlAntenaLeyendo()
        Me.SuspendLayout()
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(-4, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(691, 43)
        Me.lblNombre.TabIndex = 177
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnLecturaManual
        '
        Me.btnLecturaManual.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual.Image = CType(resources.GetObject("btnLecturaManual.Image"), System.Drawing.Image)
        Me.btnLecturaManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual.Location = New System.Drawing.Point(95, 80)
        Me.btnLecturaManual.Name = "btnLecturaManual"
        Me.btnLecturaManual.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual.TabIndex = 181
        Me.btnLecturaManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual.UseVisualStyleBackColor = False
        '
        'DESCARGA
        '
        Me.DESCARGA.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DESCARGA.BitMonitoreo = CType(0, Short)
        Me.DESCARGA.GPIO = False
        Me.DESCARGA.ID_CONTROL_ACCESO = 0
        Me.DESCARGA.ID_PLC = 0
        Me.DESCARGA.ID_SECTOR = 0
        Me.DESCARGA.Inicializado = False
        Me.DESCARGA.Location = New System.Drawing.Point(3, 124)
        Me.DESCARGA.Name = "DESCARGA"
        Me.DESCARGA.patenteLeida = Nothing
        Me.DESCARGA.PLC = Nothing
        Me.DESCARGA.Size = New System.Drawing.Size(429, 160)
        Me.DESCARGA.TabIndex = 183
        Me.DESCARGA.tiempoPatenteLeidas = 0
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(438, 43)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(249, 345)
        Me.CtrlPanelNotificaciones1.TabIndex = 182
        '
        'CtrlAtenaLeyendo
        '
        Me.CtrlAtenaLeyendo.Location = New System.Drawing.Point(3, 46)
        Me.CtrlAtenaLeyendo.Name = "CtrlAtenaLeyendo"
        Me.CtrlAtenaLeyendo.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendo.TabIndex = 178
        Me.CtrlAtenaLeyendo.UltTag = Nothing
        '
        'CtrlSectorDescargaEgreso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.DESCARGA)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.btnLecturaManual)
        Me.Controls.Add(Me.CtrlAtenaLeyendo)
        Me.Controls.Add(Me.lblNombre)
        Me.Name = "CtrlSectorDescargaEgreso"
        Me.Size = New System.Drawing.Size(693, 392)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlAtenaLeyendo As CtrlAntenaLeyendo
    Friend WithEvents btnLecturaManual As Button
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents DESCARGA As CtrlDescarga
End Class
