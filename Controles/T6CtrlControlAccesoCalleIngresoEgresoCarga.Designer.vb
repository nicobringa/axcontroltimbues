﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class T6CtrlControlAccesoCalleIngresoEgresoCarga
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.flpSectores = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.SuspendLayout()
        '
        'flpSectores
        '
        Me.flpSectores.AutoScroll = True
        Me.flpSectores.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.flpSectores.Location = New System.Drawing.Point(0, 44)
        Me.flpSectores.Name = "flpSectores"
        Me.flpSectores.Size = New System.Drawing.Size(449, 452)
        Me.flpSectores.TabIndex = 179
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(404, 3)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 40)
        Me.btnReset.TabIndex = 178
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(449, -3)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 573)
        Me.CtrlPanelNotificaciones1.TabIndex = 177
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, 0)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(152, 44)
        Me.btnLectorRfid.TabIndex = 176
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(449, 44)
        Me.lblNombre.TabIndex = 175
        Me.lblNombre.Text = "Nombre Rotonda"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 497)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(449, 75)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 174
        '
        'T6CtrlControlAccesoCalleIngresoEgresoCarga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.flpSectores)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Name = "T6CtrlControlAccesoCalleIngresoEgresoCarga"
        Me.Size = New System.Drawing.Size(649, 575)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents flpSectores As FlowLayoutPanel
    Friend WithEvents btnReset As Button
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents btnLectorRfid As Button
    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
End Class
