﻿
Imports Negocio
Imports Entidades
Imports Entidades.Constante
Public Class CtrlDinamismoBalanza


    Public Sub SetBarreraEstado(NumBalanza As Integer, ByVal NumBarrera As Integer, ByVal _BarreraEstado As BarreraEstado)

        Dim picBarreraArriba As PictureBox = Nothing
        Dim picBarreraAbajo As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing

        If NumBalanza = 1 Then

            If NumBarrera = 1 Then
                picBarreraArriba = picBAL1_BAR1_ARRIBA
                picBarreraAbajo = picBAL1_BAR1_ABAJO
                lblFalla = lblFalla_BAL1_BAR1
            ElseIf NumBarrera = 2 Then

                picBarreraArriba = picBAL1_BAR2_ARRIBA
                picBarreraAbajo = picBAL1_BAR2_ABAJO
                lblFalla = lblFalla_BAL1_BAR2
            End If


        ElseIf NumBalanza = 2 Then

            If NumBarrera = 1 Then
                picBarreraArriba = picBAL2_BAR1_ARRIBA
                picBarreraAbajo = picBAL2_BAR1_ABAJO
                lblFalla = lblFalla_BAL2_BAR1
            ElseIf NumBarrera = 2 Then

                picBarreraArriba = picBAL2_BAR2_ARRIBA
                picBarreraAbajo = picBAL2_BAR2_ABAJO
                lblFalla = lblFalla_BAL2_BAR2
            End If


        End If

        If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then
            picVisible = picBarreraArriba
            picNoVisible = picBarreraAbajo
            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
            picVisible = picBarreraAbajo
            picNoVisible = picBarreraArriba
            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.Falla Then
            picNoVisible = Nothing
            picVisible = Nothing
            modDelegado.setVisibleCtrl(Me, lblFalla, True)

        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub

    Public Sub SetSensorEstado(NumBalanza As Integer, ByVal NumSensor As Integer, ByVal _SensorEstado As Integer)
        Dim picCortando As PictureBox = Nothing
        Dim picNoCortando As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing

        If NumBalanza = 1 Then

            If NumSensor = 1 Then
                picCortando = picBAL1_S1Cortando
                picNoCortando = picBAL1_S1NoCortando


            ElseIf NumSensor = 2 Then
                picCortando = picBAL1_S2Cortando
                picNoCortando = picBAL1_S2NoCortando


            End If

        ElseIf NumBalanza = 2 Then

            If NumSensor = 1 Then
                picCortando = picBAL2_S1Cortando
                picNoCortando = picBAL2_S1NoCortando


            ElseIf NumSensor = 2 Then
                picCortando = picBAL2_S2Cortando
                picNoCortando = picBAL2_S2NoCortando


            End If

        End If


        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picNoCortando
            picNoVisible = picCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picCortando
            picNoVisible = picNoCortando


        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        Else
            'Oculto las dos barreras
            picVisible = picCortando
            picNoVisible = picNoCortando

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub












End Class
