﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
   public class BarrreraFalsa
    {

       public static string CMD_BARRERA1 = "Comando BF1";
       public static string CMD_BARRERA2 = "Comando BF2";
       public static string CMD_HABILITACION_BARRERAS = "Habilitacion BF";
       public static string ESTADO_SENSORES_CRUCE = "Estado Sensor Cruce BF";
       public static string  TIEMPO_INFRARROJO = "Tiempo Infrarrojo";
       public static string ESTADO_BF1 = "Estado BF1";
       public static string ESTADO_BF2 = "Estado BF2";



      public enum Barrera
       {
           /// <summary>
           /// La barrera 1 es la barrera que esta mas serca de la balanza
           /// </summary>
           Barrera1 = 1,

           /// <summary>
           /// La barrera 2
           /// </summary>
           Barrera2 = 2
       }

   

       public enum ComandoBarrera
       {
           CerrarAutomatico = 0,
           AbrirAutomatico = 1,
           AbrirManual = 2,
           CerrarManual = 3
       }
       /// <summary>
       /// Bit8 = Estado Sensor cruce BI, 
       /// Bit9 = Estado Sensor cruce BE (0= Cortando; 1=NO Cortando), 
       /// Bit10 = NO SE USA, Bit 11 Aviso revisar Sensor cruce BI,  
       /// Bit 12 Aviso revisar sensor cruce BE(1= Visualizar Falla 0= Normal)
       /// </summary>
       public enum BitEstadoSensorCruce
       {
           EstadoSensorBF1 = 8,
           EstadoSensorBF2 = 9,
           AvisoRevisarSensorBF1 =11,
           AvisoRevisarSensorBF2 = 12,
           EstadoHabilitacionBF1 = 13,
           EstadoHabilitacionBF2 = 14


       }

       public enum BitHabilitacionBarreras
       {
           HabilitaBF1 = 0,
           HabilitaBF2 = 1
       }
    }
}
