﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfBD
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfBD))
        Me.EP = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.lblCamposObligatorios = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.TabControlConfiguracion = New System.Windows.Forms.TabControl()
        Me.TabPageBDLocal = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBaseDatosLocal = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblContraseniaLocal = New System.Windows.Forms.Label()
        Me.txtContraseniaLocal = New System.Windows.Forms.TextBox()
        Me.lbl = New System.Windows.Forms.Label()
        Me.txtUsuarioLocal = New System.Windows.Forms.TextBox()
        Me.lblNombreServidor = New System.Windows.Forms.Label()
        Me.txtServidorLocal = New System.Windows.Forms.TextBox()
        Me.TabPageDBDriver = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBaseDriver = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtContraseniaDriver = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtUsuarioDriver = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtServidorDriver = New System.Windows.Forms.TextBox()
        CType(Me.EP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlConfiguracion.SuspendLayout()
        Me.TabPageBDLocal.SuspendLayout()
        Me.TabPageDBDriver.SuspendLayout()
        Me.SuspendLayout()
        '
        'EP
        '
        Me.EP.ContainerControl = Me
        '
        'lblTitulo
        '
        Me.lblTitulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(1, -5)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(528, 44)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Text = "Configuración Base de Datos"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCamposObligatorios
        '
        Me.lblCamposObligatorios.AutoSize = True
        Me.lblCamposObligatorios.BackColor = System.Drawing.Color.Transparent
        Me.lblCamposObligatorios.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCamposObligatorios.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.lblCamposObligatorios.Location = New System.Drawing.Point(181, 282)
        Me.lblCamposObligatorios.Name = "lblCamposObligatorios"
        Me.lblCamposObligatorios.Size = New System.Drawing.Size(176, 20)
        Me.lblCamposObligatorios.TabIndex = 0
        Me.lblCamposObligatorios.Text = "Campos Obligatorios (*)"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(37, Byte), Integer))
        Me.btnGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.White
        Me.btnGuardar.Location = New System.Drawing.Point(160, 305)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(209, 50)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'TabControlConfiguracion
        '
        Me.TabControlConfiguracion.Controls.Add(Me.TabPageBDLocal)
        Me.TabControlConfiguracion.Controls.Add(Me.TabPageDBDriver)
        Me.TabControlConfiguracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlConfiguracion.Location = New System.Drawing.Point(6, 45)
        Me.TabControlConfiguracion.Name = "TabControlConfiguracion"
        Me.TabControlConfiguracion.SelectedIndex = 0
        Me.TabControlConfiguracion.Size = New System.Drawing.Size(523, 223)
        Me.TabControlConfiguracion.TabIndex = 33
        '
        'TabPageBDLocal
        '
        Me.TabPageBDLocal.Controls.Add(Me.Label1)
        Me.TabPageBDLocal.Controls.Add(Me.txtBaseDatosLocal)
        Me.TabPageBDLocal.Controls.Add(Me.Label2)
        Me.TabPageBDLocal.Controls.Add(Me.lblContraseniaLocal)
        Me.TabPageBDLocal.Controls.Add(Me.txtContraseniaLocal)
        Me.TabPageBDLocal.Controls.Add(Me.lbl)
        Me.TabPageBDLocal.Controls.Add(Me.txtUsuarioLocal)
        Me.TabPageBDLocal.Controls.Add(Me.lblNombreServidor)
        Me.TabPageBDLocal.Controls.Add(Me.txtServidorLocal)
        Me.TabPageBDLocal.Location = New System.Drawing.Point(4, 25)
        Me.TabPageBDLocal.Name = "TabPageBDLocal"
        Me.TabPageBDLocal.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageBDLocal.Size = New System.Drawing.Size(515, 194)
        Me.TabPageBDLocal.TabIndex = 0
        Me.TabPageBDLocal.Text = "BD Local"
        Me.TabPageBDLocal.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(67, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 24)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "* Base de datos:"
        '
        'txtBaseDatosLocal
        '
        Me.txtBaseDatosLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBaseDatosLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBaseDatosLocal.Location = New System.Drawing.Point(219, 76)
        Me.txtBaseDatosLocal.MaxLength = 45
        Me.txtBaseDatosLocal.Name = "txtBaseDatosLocal"
        Me.txtBaseDatosLocal.Size = New System.Drawing.Size(277, 26)
        Me.txtBaseDatosLocal.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(4, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(181, 24)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Base de Datos Local"
        '
        'lblContraseniaLocal
        '
        Me.lblContraseniaLocal.AutoSize = True
        Me.lblContraseniaLocal.BackColor = System.Drawing.Color.Transparent
        Me.lblContraseniaLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContraseniaLocal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.lblContraseniaLocal.Location = New System.Drawing.Point(90, 143)
        Me.lblContraseniaLocal.Name = "lblContraseniaLocal"
        Me.lblContraseniaLocal.Size = New System.Drawing.Size(123, 24)
        Me.lblContraseniaLocal.TabIndex = 4
        Me.lblContraseniaLocal.Text = "* Contraseña:"
        '
        'txtContraseniaLocal
        '
        Me.txtContraseniaLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContraseniaLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContraseniaLocal.Location = New System.Drawing.Point(219, 142)
        Me.txtContraseniaLocal.MaxLength = 45
        Me.txtContraseniaLocal.Name = "txtContraseniaLocal"
        Me.txtContraseniaLocal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtContraseniaLocal.Size = New System.Drawing.Size(277, 26)
        Me.txtContraseniaLocal.TabIndex = 5
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.BackColor = System.Drawing.Color.Transparent
        Me.lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.lbl.Location = New System.Drawing.Point(122, 111)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(91, 24)
        Me.lbl.TabIndex = 2
        Me.lbl.Text = "* Usuario:"
        '
        'txtUsuarioLocal
        '
        Me.txtUsuarioLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUsuarioLocal.Location = New System.Drawing.Point(219, 110)
        Me.txtUsuarioLocal.MaxLength = 45
        Me.txtUsuarioLocal.Name = "txtUsuarioLocal"
        Me.txtUsuarioLocal.Size = New System.Drawing.Size(277, 26)
        Me.txtUsuarioLocal.TabIndex = 3
        '
        'lblNombreServidor
        '
        Me.lblNombreServidor.AutoSize = True
        Me.lblNombreServidor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreServidor.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreServidor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.lblNombreServidor.Location = New System.Drawing.Point(18, 44)
        Me.lblNombreServidor.Name = "lblNombreServidor"
        Me.lblNombreServidor.Size = New System.Drawing.Size(195, 24)
        Me.lblNombreServidor.TabIndex = 0
        Me.lblNombreServidor.Text = "* Nombre de servidor:"
        '
        'txtServidorLocal
        '
        Me.txtServidorLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtServidorLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServidorLocal.Location = New System.Drawing.Point(219, 43)
        Me.txtServidorLocal.MaxLength = 45
        Me.txtServidorLocal.Name = "txtServidorLocal"
        Me.txtServidorLocal.Size = New System.Drawing.Size(277, 26)
        Me.txtServidorLocal.TabIndex = 1
        '
        'TabPageDBDriver
        '
        Me.TabPageDBDriver.Controls.Add(Me.Label3)
        Me.TabPageDBDriver.Controls.Add(Me.txtBaseDriver)
        Me.TabPageDBDriver.Controls.Add(Me.Label4)
        Me.TabPageDBDriver.Controls.Add(Me.Label5)
        Me.TabPageDBDriver.Controls.Add(Me.txtContraseniaDriver)
        Me.TabPageDBDriver.Controls.Add(Me.Label6)
        Me.TabPageDBDriver.Controls.Add(Me.txtUsuarioDriver)
        Me.TabPageDBDriver.Controls.Add(Me.Label7)
        Me.TabPageDBDriver.Controls.Add(Me.txtServidorDriver)
        Me.TabPageDBDriver.Location = New System.Drawing.Point(4, 25)
        Me.TabPageDBDriver.Name = "TabPageDBDriver"
        Me.TabPageDBDriver.Size = New System.Drawing.Size(515, 194)
        Me.TabPageDBDriver.TabIndex = 1
        Me.TabPageDBDriver.Text = "BD Driver"
        Me.TabPageDBDriver.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(67, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(146, 24)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "* Base de datos:"
        '
        'txtBaseDriver
        '
        Me.txtBaseDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBaseDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBaseDriver.Location = New System.Drawing.Point(219, 76)
        Me.txtBaseDriver.MaxLength = 45
        Me.txtBaseDriver.Name = "txtBaseDriver"
        Me.txtBaseDriver.Size = New System.Drawing.Size(277, 26)
        Me.txtBaseDriver.TabIndex = 17
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(4, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(185, 24)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Base de Datos Driver"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(90, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(123, 24)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "* Contraseña:"
        '
        'txtContraseniaDriver
        '
        Me.txtContraseniaDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContraseniaDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContraseniaDriver.Location = New System.Drawing.Point(219, 142)
        Me.txtContraseniaDriver.MaxLength = 45
        Me.txtContraseniaDriver.Name = "txtContraseniaDriver"
        Me.txtContraseniaDriver.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtContraseniaDriver.Size = New System.Drawing.Size(277, 26)
        Me.txtContraseniaDriver.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(122, 111)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 24)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "* Usuario:"
        '
        'txtUsuarioDriver
        '
        Me.txtUsuarioDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUsuarioDriver.Location = New System.Drawing.Point(219, 110)
        Me.txtUsuarioDriver.MaxLength = 45
        Me.txtUsuarioDriver.Name = "txtUsuarioDriver"
        Me.txtUsuarioDriver.Size = New System.Drawing.Size(277, 26)
        Me.txtUsuarioDriver.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(18, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(195, 24)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "* Nombre de servidor:"
        '
        'txtServidorDriver
        '
        Me.txtServidorDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtServidorDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServidorDriver.Location = New System.Drawing.Point(219, 43)
        Me.txtServidorDriver.MaxLength = 45
        Me.txtServidorDriver.Name = "txtServidorDriver"
        Me.txtServidorDriver.Size = New System.Drawing.Size(277, 26)
        Me.txtServidorDriver.TabIndex = 10
        '
        'FrmConfBD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(528, 370)
        Me.Controls.Add(Me.TabControlConfiguracion)
        Me.Controls.Add(Me.lblCamposObligatorios)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmConfBD"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuración Base de Datos"
        CType(Me.EP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlConfiguracion.ResumeLayout(False)
        Me.TabPageBDLocal.ResumeLayout(False)
        Me.TabPageBDLocal.PerformLayout()
        Me.TabPageDBDriver.ResumeLayout(False)
        Me.TabPageDBDriver.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EP As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents lblCamposObligatorios As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents TabControlConfiguracion As System.Windows.Forms.TabControl
    Friend WithEvents TabPageBDLocal As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblContraseniaLocal As System.Windows.Forms.Label
    Friend WithEvents txtContraseniaLocal As System.Windows.Forms.TextBox
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioLocal As System.Windows.Forms.TextBox
    Friend WithEvents lblNombreServidor As System.Windows.Forms.Label
    Friend WithEvents txtServidorLocal As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtBaseDatosLocal As System.Windows.Forms.TextBox
    Friend WithEvents TabPageDBDriver As TabPage
    Friend WithEvents Label3 As Label
    Friend WithEvents txtBaseDriver As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtContraseniaDriver As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtUsuarioDriver As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtServidorDriver As TextBox
End Class
