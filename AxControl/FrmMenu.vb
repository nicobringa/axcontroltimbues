﻿Imports Controles
Public Class FrmMenu
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim frm As New FrmConfLectoresRFID(Me.ListRFID_SpeedWay)
        'frm.ShowDialog()
        Me.Close()
        Dim frm As New FrmConfiguracion
        frm.ShowDialog()

    End Sub

    Private Sub btnSimulacionBalanza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim frm As New FrmSimulacionBalanza
        'frm.Show()

    End Sub

    Private Sub btnSimulacionWS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimulacionWS.Click
        Dim frm As New FrmSimulacionWS
        frm.Show()
    End Sub

    Private Sub btnUsuario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuario.Click
        Dim frm As New FrmRegistrarUsuario()
        ' Dim frm As New frmEscribirEPC
        frm.ShowDialog()

    End Sub

    Private Sub btnAuditoria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuditoria.Click
        Dim frm As New FrmAuditoria
        frm.Show()
    End Sub

    Private Sub btnCerrarAplicacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrarAplicacion.Click

        If MessageBox.Show("¿Desea cerrar la aplicación?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Application.ExitThread() ' Cierra la aplicacion y todos procesos
        End If

    End Sub

    Private Sub btnConfBD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfBD.Click
        Dim frmBD As New FrmConfBD
        frmBD.Show()
    End Sub

    Private Sub btn_ConfigurarRutaWS_Click(sender As Object, e As EventArgs) Handles btn_ConfigurarRutaWS.Click
        Dim frm As New FrmConfiguracionWS
        frm.Show()
    End Sub


End Class