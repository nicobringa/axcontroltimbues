﻿Imports Negocio
Imports Entidades
Imports Entidades.Constante

Public Class CtrlDinamismoBalanza1
    Public Sub SetBarreraEstado(ByVal NumBarrera As Integer, ByVal _BarreraEstado As BarreraEstado)

        Dim picBarreraArriba As PictureBox = Nothing
        Dim picBarreraAbajo As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing



        If NumBarrera = 1 Then
            picBarreraArriba = picBAR1_ARRIBA
            picBarreraAbajo = picBAR1_ABAJO
            lblFalla = lblFalla_BAR1
        ElseIf NumBarrera = 2 Then

            picBarreraArriba = picBAR2_ARRIBA
            picBarreraAbajo = picBAR2_ABAJO
            lblFalla = lblFalla_BAR2
        End If


        If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then
            picVisible = picBarreraArriba
            picNoVisible = picBarreraAbajo
            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
            picVisible = picBarreraAbajo
            picNoVisible = picBarreraArriba
            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.Falla Then
            picNoVisible = Nothing
            picVisible = Nothing
            modDelegado.setVisibleCtrl(Me, lblFalla, True)

        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub

    Public Sub SetSensorEstado(TipoSensor As Entidades.Constante.tipoSensor, ByVal NumSensor As Integer, ByVal _SensorEstado As Integer)
        Dim picCortando As PictureBox = Nothing
        Dim picNoCortando As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing


        ''Sensor de lectura
        If TipoSensor = tipoSensor.Lectura Then
            picCortando = picS1LecturaCortando

            picNoCortando = picS1LecturaNoCortando

        End If

        ''Sensor de barrera
        If TipoSensor = tipoSensor.Barrera Then
            If NumSensor = 1 Then
                picCortando = picS1PosicionCortando

                picNoCortando = picS1PosicionNoCortando
            ElseIf NumSensor = 2 Then
                picCortando = picS2PosicionCortando

                picNoCortando = picS2PosicionNoCortando
            End If
        End If

        ''sensor de posicion
        If TipoSensor = tipoSensor.Posicion Then
            picCortando = picSensorPosicionCortando
            picNoCortando = picSensorPosicionSinCortar
        End If



        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picNoCortando
            picNoVisible = picCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picCortando
            picNoVisible = picNoCortando


        End If

        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
            modDelegado.setVisiblePic(Me, False, picS2LecturaNoCortando)
            modDelegado.setVisiblePic(Me, False, picS2LecturaCortando)
        Else
            'Oculto las dos barreras
            picVisible = picCortando
            picNoVisible = picNoCortando

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
            modDelegado.setVisiblePic(Me, False, picS2LecturaNoCortando)
            modDelegado.setVisiblePic(Me, False, picS2LecturaCortando)
        End If


    End Sub

    Public Sub sinSensorPosicion()
        modDelegado.setVisiblePic(Me, False, picSensorPosicionSinCortar)
        modDelegado.setVisiblePic(Me, False, picSensorPosicionCortando)
        modDelegado.setVisiblePic(Me, False, picS2LecturaNoCortando)
        modDelegado.setVisiblePic(Me, False, picS2LecturaCortando)
    End Sub

End Class
