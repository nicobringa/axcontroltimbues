﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades
Imports System.Drawing
Imports StarMicronics.StarIOExtension
Imports StarMicronics.StarIO
Imports System.Text

Public Class ImpresoraSTAR

#Region "Propiedades"

    Public Shared builder As ICommandBuilder = StarIoExt.CreateCommandBuilder(Emulation.StarLine)
    Public Shared portSettings As String = ";l"
    Public Shared portName As String = "TCP:192.168.226.124"
    Public Shared port As IPort
    Public Shared ComandosFinales() As Byte

    Public Shared strTitulo As String
    Public Shared strEncabezado As String
    Public Shared strCodDeBarra As String

    Public Shared Event MsjError(tag As String, msj As String)


#End Region

#Region "Enum"

#End Region

#Region "Mis eventos"

#End Region

#Region "Constructor"





#End Region

#Region "Métodos"


    Public Shared Sub RecibirDatos(Titulo As String, Encabezado As String, CodBarra As String)
        strTitulo = Titulo
        strEncabezado = Encabezado
        strCodDeBarra = CodBarra
    End Sub

    Public Shared Sub Imprimir()

        byArray(builder)
        EnviarComandos(ComandosFinales)
        ComandosFinales = Nothing
        builder.Clear()

    End Sub

    ''' <summary>
    ''' Crea un objeto builder con todo el texto del ticket.
    ''' </summary>
    ''' <param name="builder"></param>
    ''' <returns></returns>
    Public Shared Function fnConstructorTicket(ByVal builder As ICommandBuilder) As ICommandBuilder

        Try
            builder.AppendCodePage(CodePageType.CP1252)
            builder.AppendInternational(InternationalType.Spain)
            builder.AppendCharacterSpace(0)

            builder.AppendAlignment(AlignmentPosition.Center)
            builder.AppendMultiple(Encoding.GetEncoding("Windows-1252").GetBytes(strTitulo & vbLf), 2, 2)
            builder.Append(Encoding.GetEncoding("Windows-1252").GetBytes(strEncabezado & vbLf))

            builder.AppendAlignment(AlignmentPosition.Left)
            builder.Append(Encoding.GetEncoding("Windows-1252").GetBytes("------------------------------------------------" & vbLf))

            builder.AppendAlignment(AlignmentPosition.Center)
            builder.Append(Encoding.GetEncoding("Windows-1252").GetBytes("Este es un documento de prueba" & vbLf))
            builder.Append(Encoding.GetEncoding("Windows-1252").GetBytes("Fecha: " + (DateTime.Today.Day.ToString() + "/" + DateTime.Today.Month.ToString() + "/" + DateTime.Today.Year.ToString()) & vbLf))

            builder.AppendAlignment(AlignmentPosition.Left)
            builder.Append(Encoding.GetEncoding("Windows-1252").GetBytes("------------------------------------------------" & vbLf))

            builder.AppendAlignment(AlignmentPosition.Center)
            builder.AppendBarcode(Encoding.GetEncoding("ASCII").GetBytes("{B" + strCodDeBarra), BarcodeSymbology.Code128, BarcodeWidth.Mode2, 40, True)
            ''builder.AppendQrCode(Encoding.UTF8.GetBytes("AuMax S.R.L."), QrCodeModel.No2, QrCodeLevel.L, 2)

            Return builder
        Catch ex As Exception
            RaiseEvent MsjError("fnConstructorTicket", ex.Message)
            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' Convierte el ticket en array de bytes.
    ''' </summary>
    ''' <param name="builder"></param>
    Public Shared Sub byArray(ByVal builder As ICommandBuilder)

        Try
            fnConstructorTicket(builder)
            builder.AppendCutPaper(CutPaperAction.PartialCutWithFeed)
            builder.EndDocument()
            ComandosFinales = builder.Commands
        Catch ex As Exception
            RaiseEvent MsjError("byArray", ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Retorna el estado de la impresora. TRUE Conectada, FALSE Desconectada
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function CheckStarConnection() As Boolean

        Try
            port = Factory.I.GetPort(portName, portSettings, 1000) 'Si la impresora está desconectada, sale por el Catch
            Return True
        Catch ex As Exception
            RaiseEvent MsjError("CheckStarConnection", ex.Message)

            Return False
        End Try

    End Function


    ''' <summary>
    ''' Envía el comando de impresión 
    ''' </summary>
    ''' <param name="commands"></param>
    Public Shared Sub EnviarComandos(ByVal commands() As Byte)

        Try

            If CheckStarConnection() Then

                Dim status As StarPrinterStatus
                status = port.BeginCheckedBlock

                If status.Offline Then
                    If status.ReceiptPaperEmpty Then
                        MsgBox("Sin papel")
                    Else
                        MsgBox("Error en la impresora")
                    End If
                Else
                    Dim commandsLength As UInteger = CType(commands.Length, UInteger)
                    Dim writtenLength As UInteger = port.WritePort(commands, 0, commandsLength)
                    commands = Nothing

                End If
            Else
                MsgBox("La impresora está desconectada")
            End If

        Catch ex As Exception
            RaiseEvent MsjError("EnviarComandos", ex.Message)
            MsgBox("Error al imprimir")
        End Try

    End Sub


#End Region

End Class
