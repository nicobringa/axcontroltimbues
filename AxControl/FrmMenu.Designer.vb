﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMenu))
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.btnSimulacionWS = New System.Windows.Forms.Button()
        Me.btnConfBD = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnUsuario = New System.Windows.Forms.Button()
        Me.btnAuditoria = New System.Windows.Forms.Button()
        Me.btnCerrarAplicacion = New System.Windows.Forms.Button()
        Me.btn_ConfigurarRutaWS = New System.Windows.Forms.Button()
        Me.btnSimulador = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblTitulo
        '
        Me.lblTitulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(0, 0)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(335, 39)
        Me.lblTitulo.TabIndex = 61
        Me.lblTitulo.Text = "Menu"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSimulacionWS
        '
        Me.btnSimulacionWS.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSimulacionWS.BackColor = System.Drawing.Color.White
        Me.btnSimulacionWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSimulacionWS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSimulacionWS.Location = New System.Drawing.Point(1, 275)
        Me.btnSimulacionWS.Name = "btnSimulacionWS"
        Me.btnSimulacionWS.Size = New System.Drawing.Size(334, 62)
        Me.btnSimulacionWS.TabIndex = 62
        Me.btnSimulacionWS.Text = "Simulacion WS"
        Me.btnSimulacionWS.UseVisualStyleBackColor = False
        '
        'btnConfBD
        '
        Me.btnConfBD.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConfBD.BackColor = System.Drawing.Color.White
        Me.btnConfBD.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConfBD.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConfBD.Location = New System.Drawing.Point(1, 100)
        Me.btnConfBD.Name = "btnConfBD"
        Me.btnConfBD.Size = New System.Drawing.Size(334, 62)
        Me.btnConfBD.TabIndex = 63
        Me.btnConfBD.Text = "Base de Datos"
        Me.btnConfBD.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(1, 161)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(334, 62)
        Me.Button1.TabIndex = 65
        Me.Button1.Text = "Configuración de tiempos"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnUsuario
        '
        Me.btnUsuario.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUsuario.BackColor = System.Drawing.Color.White
        Me.btnUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUsuario.Location = New System.Drawing.Point(1, 39)
        Me.btnUsuario.Name = "btnUsuario"
        Me.btnUsuario.Size = New System.Drawing.Size(334, 62)
        Me.btnUsuario.TabIndex = 66
        Me.btnUsuario.Text = "Usuarios"
        Me.btnUsuario.UseVisualStyleBackColor = False
        '
        'btnAuditoria
        '
        Me.btnAuditoria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAuditoria.BackColor = System.Drawing.Color.White
        Me.btnAuditoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAuditoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAuditoria.Location = New System.Drawing.Point(1, 336)
        Me.btnAuditoria.Name = "btnAuditoria"
        Me.btnAuditoria.Size = New System.Drawing.Size(334, 62)
        Me.btnAuditoria.TabIndex = 67
        Me.btnAuditoria.Text = "Auditoria"
        Me.btnAuditoria.UseVisualStyleBackColor = False
        '
        'btnCerrarAplicacion
        '
        Me.btnCerrarAplicacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrarAplicacion.BackColor = System.Drawing.Color.White
        Me.btnCerrarAplicacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrarAplicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrarAplicacion.Location = New System.Drawing.Point(1, 454)
        Me.btnCerrarAplicacion.Name = "btnCerrarAplicacion"
        Me.btnCerrarAplicacion.Size = New System.Drawing.Size(334, 62)
        Me.btnCerrarAplicacion.TabIndex = 69
        Me.btnCerrarAplicacion.Text = "Cerrar Aplicación"
        Me.btnCerrarAplicacion.UseVisualStyleBackColor = False
        '
        'btn_ConfigurarRutaWS
        '
        Me.btn_ConfigurarRutaWS.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_ConfigurarRutaWS.BackColor = System.Drawing.Color.White
        Me.btn_ConfigurarRutaWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_ConfigurarRutaWS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ConfigurarRutaWS.Location = New System.Drawing.Point(1, 214)
        Me.btn_ConfigurarRutaWS.Name = "btn_ConfigurarRutaWS"
        Me.btn_ConfigurarRutaWS.Size = New System.Drawing.Size(334, 62)
        Me.btn_ConfigurarRutaWS.TabIndex = 70
        Me.btn_ConfigurarRutaWS.Text = "Configurar Ruta WS"
        Me.btn_ConfigurarRutaWS.UseVisualStyleBackColor = False
        '
        'btnSimulador
        '
        Me.btnSimulador.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSimulador.BackColor = System.Drawing.Color.White
        Me.btnSimulador.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSimulador.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSimulador.Location = New System.Drawing.Point(1, 393)
        Me.btnSimulador.Name = "btnSimulador"
        Me.btnSimulador.Size = New System.Drawing.Size(334, 62)
        Me.btnSimulador.TabIndex = 71
        Me.btnSimulador.Text = "Simulador"
        Me.btnSimulador.UseVisualStyleBackColor = False
        '
        'FrmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(335, 517)
        Me.Controls.Add(Me.btnSimulador)
        Me.Controls.Add(Me.btn_ConfigurarRutaWS)
        Me.Controls.Add(Me.btnCerrarAplicacion)
        Me.Controls.Add(Me.btnAuditoria)
        Me.Controls.Add(Me.btnUsuario)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnConfBD)
        Me.Controls.Add(Me.btnSimulacionWS)
        Me.Controls.Add(Me.lblTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents btnSimulacionWS As System.Windows.Forms.Button
    Friend WithEvents btnConfBD As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnUsuario As System.Windows.Forms.Button
    Friend WithEvents btnAuditoria As System.Windows.Forms.Button
    Friend WithEvents btnCerrarAplicacion As System.Windows.Forms.Button
    Friend WithEvents btn_ConfigurarRutaWS As Button
    Friend WithEvents btnSimulador As Button
End Class
