﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Constante
    {


        public static string CARPETA_CONFIGURACION = "C:\\Aumax\\AxConfControl";
        public static string VERSION = "1.0.8";
        public static string WS_ERRO_PATH = "/home/pruebas";
        public static string WS_ERRO_EMPRESA = "0000";
        public static string WS_ERRO_USUARIO = "AUMAX";
        public static string WS_ERRO_HASH = "1";
        public static string WS_ERRO_TERMINAL = "1";
        //-----------------------------------------------------/
        public static string NOMBRE_EXPLANDA = "Explanada";
        public static string NOMBRE_ACOPIO = "Acopio";
        public static string NOMBRE_CELDA1Y2 = "Celda1y2";

        public static string NOMBRE_PORTERIA = "Porteria";
        public static string NOMBRE_CALADO = "Calado";
        public static string NOMBRE_BALANZA = "Balanza";
        public static string NOMBRE_BARRERA_FALSA = "BarreraFalsa";

        public static string USUARIO_AUMAX = "Aumax";


        public static string NOMBRE_SECTOR_INGRESO_PORTERIA_EXPLANADA = "IngresoPorteriaExplanada";


        public static int CantidadFijaMaxima = 65;

        public static string getNombrePLC(PuestoTrabajo idPuestoTrabajo)
        {

            string puestoTrabajo;




            switch (idPuestoTrabajo)
            {
                case PuestoTrabajo.Porteria:
                    puestoTrabajo = NOMBRE_PORTERIA;
                    break;
                case PuestoTrabajo.Calador:
                    puestoTrabajo = NOMBRE_CALADO;
                    break;
                case PuestoTrabajo.Balanzas:
                    puestoTrabajo = NOMBRE_BALANZA;
                    break;
                case PuestoTrabajo.BarreraFalse:
                    puestoTrabajo = NOMBRE_BARRERA_FALSA;
                    break;
                default:
                    puestoTrabajo = "prueba";
                    break;
            }


            return "PLC-" + puestoTrabajo;
        }


        public class TIPO_CONTROL_ACCESO
        {
            public static string BALANZA = "BALANZA";
            public static string PORTERIA = "PORTERIA";
            public static string PORTERIA_INGRESO = "PORTERIA INGRESO";
            public static string PORTERIA_EGRESO = "PORTERIA EGRESO";
            public static string PORTERIA_EGRESO_INVENGO = "PORTERIA EGRESO INVENGO";
            public static string DESCARGA = "DESCARGA";
            public static string CALADO2 = "CALADO 2 CALLES";
            public static string CALADO4 = "CALADO 4 CALLES";
            public static string INGRESO_BARRERA_DOBLE = "INGRESO BARRERA DOBLE";
            public static string INGRESO_CALADO = "INGRESO CALADO";
            public static string BALANZA1 = "BALANZA1";
            public static string TICKET = "TICKET";
        }

        public class PROPIEDADES_TAG_PLC
        {
            public static String ESTADO = "Estado";
            public static String ESTADO_OLD = "EstadoOld";
            public static String COMANDO = "Comando";
            public static String TIEMPO_RET = "TiempoRet";
            public static String PESO_MAX = "PesoMax";
            public static String DIFERENCIA_ESTABILIDAD = "DiferenciaEstabilidad";
            public static String TIEMPO_ESTABILIDAD = "TiempoEstabilidad";
            public static String PESO = "Peso";
            public static String LECTURA_RFID = "LecturaRFID";
            public static String FALLA = "Falla";
            public static String HABILITACION = "Habilitacion";
            public static String HABILITACION_SENSOR_POSICION = "HabSenPosicion";
            public static String RESET = "Reset";
            public static String PESO_NETO = "PesoNeto";
            public static String PESO_NEGATIVO = "PesoNegativo";
            public static String FUERA_RANGO = "FueraRango";
            public static String FUERA_EQUILIBRIO = "FueraEquilibrio";
            public static String PESO_MAYOR_500 = "PesoMayor500";
            public static String PESO_ESTABLE = "PesoEstable";
            public static String PESO_MENOR_DIF_ESTABILIDAD = "PesoMenorDifEstabilidad";
            public static String PESO_MAXIMO_BAL = "PesoMaximoBal";
            public static String ERROR_BAL = "ErrorBal";
            public static String BitVida = "BitVida";
            /// <summary>
            /// 1= Habilita la Lectura del TAG a la salida de la Descarga. 
            /// AxControl lo lee y lo pone a 0
            /// </summary>
            public static String HAB_LecturaRFID_SALIDA = "Hab_LecturaRFID_Salida";
            public static String NVO_CAMION = "NuevoCamion";
        }

        public class PROPIEDADES_TAG_PLC_DATOS_DESCARGA
        {
            public static String PATENTE_CHASIS = "patenteChasis";
            public static String PATENTE_ACOPLADO = "patenteAcoplado";
            public static String TURNO = "turno";
            public static String SECTOR = "sector";
            public static String PRODUCTO = "producto";
            public static String MOTIVO = "motivo";
            public static String HABILITADO = "habilitado";
            public static String calidad = "calidad";
        }

        public class PROPIEDADES_INTERFACE_SILOS
        {
            public static String COMANDO = "Comando";
            public static String ID_SILO = "IdSilo";
            public static String ID_PRODUCTO = "IdProducto";
            public static String ESTADO_RTA = "EstadoRta";
            public static String ID_SILO_ALARMA = "IdSiloAlarma";
            public static String ID_ALARMA = "IdAlarma";
            public static String ID_COMANDO = "IdComando";
            public static String CAPACIDAD = "Capacidad";
            public static String EXISTEN_TOTAL = "ExistenTotal";
            public static String ID_COMANDO_RTA = "IdComandoRta";
            public static String VALOR_ALARMA = "ValorAlarma";
            public static String SILO = "Silo";
            public static String PRODUCTO = "Producto";
            public static String MENSAJE_RTA = "MensajeRta";
            public static String MENSAJE_ALARMA = "MensajeAlarma";

        }

        public class PROPIEDADES_INTERFACE_DESCARGA
        {
            public static String COMANDO = "Comando";
            public static String ID_DESTINO = "IdDestino";
            public static String ID_ORIGEN1 = "IdOrigen1";
            public static String ID_ORIGEN2 = "IdOrigen2";
            public static String ID_ORIGEN3 = "IdOrigen3";
            public static String ID_ORIGEN4 = "IdOrigen4";
            public static String ID_PRODUCTO = "IdProducto";
            public static String TIPO_OPERACION = "TipoOperacion";
            public static String ID_EVENTO = "IdEvento";
            public static String ID_EVENTO_TO_ELECTROLUZ = "IdEventoToElectroluz";
            public static String VALOR_ORIGEN1 = "ValorOrigen1";
            public static String VALOR_ORIGEN2 = "ValorOrigen2";
            public static String VALOR_ORIGEN3 = "ValorOrigen3";
            public static String VALOR_ORIGEN4 = "ValorOrigen4";
            public static String VALOR_EVENTO1 = "ValorEvento1";
            public static String VALOR_EVENTO2 = "ValorEvento2";
            public static String VALOR_EVENTO3 = "ValorEvento3";
            public static String VALOR_EVENTO4 = "ValorEvento4";
            public static String VALOR_EVENTO5 = "ValorEvento5";
            public static String VALOR_EVENTO_TO_ELECTROLUZ = "ValorEventoToElectroluz";
            public static String FECHA_HORA_PLANIFICADO = "FechaHoraPlanificada";
            public static String USUARIO_RESPONSABLE = "UsuarioResponsable";
            public static String MENSAJE_EVENTO_TO_ELECTROLUZ = "MensajeEventoToElectroluz";
            public static String MENSAJE_EVENTO = "MensajeEvento";
            public static String NUM_OT = "NumOT";
            public static String NUM_OT_EVENTO = "NumOTEvento";
            public static String NUM_OT_EVENTO_TO_ELECTROLUZ = "NumOTEventoToElectroluz";
            public static String TOTAL_A_CARGAR = "TotalACargar";


        }



        /// <summary>
        /// Nombre de los comando a hacer por el sistema axControl
        /// </summary>
        public class COMANDOS_AX_TO_SG_TO_DO
        {
            public static String TAG_LEIDO = "TAG_LEIDO";
            public static String PESO_LEIDO = "PESO_LEIDO";
            public static String BARRERA_STS = "BARRERA_STS";
            public static String PERMITIR_SALIR_MAN_STS = "PERM_SALIR_MAN_STS";
            public static String SET_PESO_BALALANZA = "SET_PESO_BAL";


        }

        /// <summary>
        /// Nombre de los comando que envia 
        /// </summary>
        public class COMANDOS_SG_TO_AX_TO_DO
        {
            public static String TAG_HABILITADO = "TAG_HAB";
            public static String PESO_HABILITADO = "PESO_HAB";
            public static String BARRERA_MANUAL = "BARRERA_MAN";
            public static String PERMITIR_SALIR_MANUAL = "PERM_SALIR_MAN";
            public static String GET_PESO_BALANZA = "GET_PESO_BAL";

        }


        public class CAMARA_ANPR
        {
            public static String CAMARA_INGRESO = "CAM-INGRESO";
            public static String CAMARA_EGRESO = "CAM-EGRESO";
        }


        #region "COMANDOS"

        /// <summary>
        /// Comando Barrera1: 
        ///Comando Barrera1: 1=Deshabilitar; 2=Automático; 3=Subir Manual;4=Bajar Manual
        /// </summary>
        public enum BarreraComando
        {
            Deshabilitar = 1,
            Automatico = 2,
            SubirManual = 3,
            BajarManual = 4,
            PermitirIngresar = 5
        }

        /// <summary>
        /// Habilitación Sensor de posición de Barrera2: 
        /// 1=Habilitado; 
        /// 2=Deshabilitado
        /// </summary>
        public enum SensorComando
        {
            Deshabilitado = 2,
            Habilitado = 1
        }

        /// <summary>
        /// Comando Balanza: 
        ///Comando Balanza: 1=Habilita;2=Deshabilita;3=Permitir Salir;4=Permitir Ingreso B1;5=Permitir Ingreso B2;6=Permitir Salir Manual; 7= Reset
        /// </summary>
        public enum BalanzaComando
        {
            Habilitar = 1,
            Deshabilitar = 2,
            PermitirSalir = 3,
            PermitirIngresoBarrera1 = 4,
            PermitirIngresoBarrera2 = 5,
            PermitirSalirManual = 6,
            Reset = 7
        }

        /// <summary>
        /// Comando que se escribe en la variable NuevoCamion del plc
        /// </summary>
        public enum DescargaNuevoCamionComando
        {
            //1= Ingreso de Camión OK(conforme para descargar) 2=Ingreso de Camión NO OK(no conforme para descargar). 
            //Está variable es escrita por axControl, luego el PLC la procesa y la pone en 0
            /// <summary>
            /// 1= Ingreso de Camión OK(conforme para descargar)
            /// </summary>
            INGRESO_OK = 1,
            /// <summary>
            /// 2=Ingreso de Camión NO OK(no conforme para descargar). 
            /// </summary>
            INGRESO_NO_OK = 2

        }
        /// <summary>
        /// valores del dato motivo del axRespuesta
        /// </summary>
        public enum AxRespuestaMotivo
        {
            /// <summary>
            /// El camión se encuentra dado de alta en el circuito  pero esta mal posicionado (calle o sector)
            /// </summary>
            MAL_POSICIONADO = 1,
            /// <summary>
            /// EL tag del camión no esta dado de alta en el circuito.
            /// </summary>
            SIN_ALTA = 2
        }

        #endregion

        #region "ESTADOS"

        /// <summary>
        /// Estado Barrera1: 
        ///Estado Barrera2: 1=Arriba Auto;2=Abajo Auto;3=Arriba Manual;4=Abajo Manual,5=Falla
        /// </summary>
        public enum BarreraEstado
        {

            ArribaAuto = 1,
            AbajoAuto = 2,
            ArribaManual = 3,
            AbajoManual = 4,
            Falla = 5,
            Deshabilitado = 6
        }



        public enum SensorInfrarojoEstado
        {
            // Estado Sensor de Cruce Antena1: 1=Deshabilitado;2=Sensor con presencia;3=Sensor sin presencia;4= Posible Falla
            Deshabilitado = 1,
            ConPresencia = 2,
            Sinprecencia = 3,
            falla = 4
        }

        /// <summary>
        /// Estado de la lógica de pasos de la Balanza. 
        /// 0=Reposo; 
        ///* 10= Apertura Barrera de Entrada (Sentido ingresando a planta) ; 
        ///* 20= Cierre Barrera de Entrada (Sentido ingresando a planta); 
        ///* 30= Posicionando Camión. Muestra Cartel Avance (Sentido ingresando a planta); 
        ///* 40= Posicionando Camión. Muestra Cartel Retroceda (Sentido ingresando a planta); 
        ///* 50= Camión Posicionado. Esperando estabilidad (Sentido ingresando a planta); 
        ///* 60= Peso Máximo alcanzado (Sentido ingresando a planta); 
        ///* 70= Aviso tomar peso (Sentido ingresando a planta); 
        /// *80= Apertura barrera de Salida (Sentido ingresando a planta); 
        /// 90= Cierre barrera de Salida (Sentido ingresando a planta); 
        ///* 110= Apertura Barrera de Entrada (Sentido egresando de planta); 
        ///* 120= Cierre Barrera de Entrada (Sentido egresando de planta); 
        ///* 130= Posicionando Camión. Muestra Cartel Avance  (Sentido egresando de planta); 
        ///* 140= Posicionando Camión. Muestra Cartel Retroceda  (Sentido egresando de planta); 
        ///* 150= Camión Posicionado. Esperando estabilidad  (Sentido egresando de planta); 
        ///* 160= Peso Máximo alcanzado (Sentido egresando de planta); 
        ///* 170= Aviso tomar peso  (Sentido egresando de planta); 
        /// *180= Apertura barrera de Salida (Sentido egresando de planta); 
        /// 190= Cierre barrera de Salida (Sentido egresando de planta); 
        ///* 1000= En Manual (alguna barrera se encuentra accionada manualmente)
        /// </summary>
        public enum BalanzaEstado
        {
            BarrerasCerrada = 0,

            HabilitadaIngresarBarrera1 = 10,
            HabilitadaIngresarBarrera2 = 110,

            CierreBarreraEntradaBar1 = 20,
            CierreBarreraEntradaBar2 = 120,

            AvanzarCamionBarrera1 = 30,
            AvanzarCamionBarrera2 = 130,

            RetrocedaCamionBarrera1 = 40,
            RetrocedaCamionBarrera2 = 140,

            EstabilizandoPesoBarrera1 = 50,
            EstabilizandoPesoBarrera2 = 150,

            PesoMaximoBarrera1 = 60,
            PesoMaximoBarrera2 = 160,

            TomarPesoBarrera1 = 70,
            TomarPesoBarrera2 = 170,

            ErrorBalanza1 = 80,
            ErrorBalanza2 = 180,

            PermitirSalirBarrera1 = 90,
            PermitirSalirBarrera2 = 190,

            BajaBarrera1Salida = 100,
            BajaBarrera2Salida = 200,

            Manual = 1000,
            Deshabilitada = 1002

        }

        public enum LecturaRFID_ESTADO
        {
            LecturaDeshabilitada = 0,
            Antena1Habilitada = 1,
            Antena2Habilitada = 2

        }

        #endregion

        public enum tipoFalla
        {
            fallaConexíon = 1,
            fallaBarrera = 2
        }

        public enum EstadoAlarma
        {
            Aparecida = 0,
            AparacidaReconocida = 1,
            Desaparecida = 2
        }

        public enum DeshabHabSensorPosicionBarrera
        {
            Habilitar = 1,
            Deshabilitar = 2
        }


        /// <summary>
        /// 
        /// </summary>
        public enum BalanzaHabilitacion
        {
            Deshabilitada = 0,
            Habilitada = 1
        }

        public enum SensorHabilitacion
        {
            Habilitar = 1,
            Deshabilitar = 2
        }

        public enum HabLecturaRfid_Salida
        {
            DESHABILITAR_LECTURA = 0,
            HABILITAR_LECTURA = 1

        }

        public enum tipoSensor
        {
            Posicion = 1,
            Barrera = 2,
            Lectura = 3,
            Plataforma = 4
        }


        /// <summary>
        /// Un Puesto de trabajo es un sector o parte de la planta que opera con rfid con una logica establecida
        /// </summary>
        public enum PuestoTrabajo
        {
            Porteria = 1,

            Calador = 2,
            /// <summary>
            /// En el puesto de balanza existen dos balanzas
            /// </summary>
            Balanzas = 3,
            BarreraFalse = 4

        }

        /// <summary>
        /// El tiempo que va dorir el subproceso ejecucion segun el sector
        /// </summary>
        public enum SleepEjecucion
        {
            Porteria = 1000,
            Balanza = 1000,
            BarreraFalsa = 1000

        }


        public enum CategoriaUsuario
        {
            Administrador = 1,
            Supervisor = 2,
            Operador = 3
        }

        public enum CartelesPorteria
        {
            Ingreso = 0,
            Egreso = 1
        }

        public enum OperacionBarrera
        {
            SubirBarrera = 0,
            BajarBarrera = 1
        }

        public enum EstadoHabilitacion
        {
            Deshabilitado = 0,
            Habilitado = 1

        }

        public enum EstadoLector
        {
            desconectado = 0,
            conectadoSinLectura = 1,
            conectadoLeyendo = 2,
            sinConfiguracion = 3,
        }
        public enum IdEstadoLector
        {
            sinLectura = 0,
            desconectado = 1,
            leyendo = 2,
            lecturaTAG = 3,
            noExisteLector = 4
        }

        public enum EstadoLectura
        {
            SinLectura = 0,
            Leyendo = 1
        }

        //public enum SensorEstado
        //{
        //    Deshabilitado = 0,
        //    Cerrado = 0,
        //    Abierto = 1
        //}

        public class SENSOR_CRUCE_ESTADO
        {
            public static bool CON_PRESENCIA = true;
            public static bool SIN_PRESENCIA = false;

        }



        public class BARRERA_HABILITACION
        {
            public static bool HABILITAR = true;
            public static bool DESHABILITAR = false;
        }



        public enum SensorEstadoFalla
        {
            EnFalla = 1,
            Normal = 0
        }


        public enum TiposEquipos
        {
            Barrera = 0,
            Sensor = 1
        }



        public enum IngresoEgreso
        {
            Ingreso = 0,
            Egreso = 1
        }

        public enum TipoNotificacion
        {
            xErrorDeve = 0,
            Informacion = 1,
            LecturaTag = 2,
            Suceso = 3,
            xErrorUser = 4,
            InformacionDeve = 5,
        }

        public enum BarrerasBalanza
        {
            barreraIngresoBalanza1 = 20,
            barreraEgresoBalanza1 = 21,
            barreraIngresoBalanza2 = 22,
            barreraEgresoBalanza2 = 23
        }


        public enum BarreraDoble
        {
            barrera1 = 1,
            barrera2 = 2
        }

        public enum numeroBalanza
        {
            Balanza1 = 1,
            Balanza2 = 2
        }

        public enum QC
        {
            QC_BAD_ERROR_CONF = 4,
            QC_BAD_NO_CONECT = 8,
            QC_BAD_NO_COM = 24,
            QC_UNCERTINE = 120,
            QC_OK = 128
        }

        public enum acciones
        {
            BarreraManual = 1,
            WS_EstaHabilitado = 2,
            WS_HabilitadoManual = 3,
            WS_OperBarreraManual = 4,
            WS_PesoObtenido = 5,
            Ejec_Comando = 6,
            WS_GetPesoBalanza = 7,
            WS_SalirBalanza = 8,
            Estado_Balanza = 9,
            Reset_Balanza = 10,
            SinConexionRFID = 11,
            EscrituraPLC = 12,
            ErrorEscrituraPLC = 13,
            ErrorLectura = 14,
            TODOS = 100,
            habilitacionBarrera = 15,
            habilitacionSensor = 16,
            configuracionBalanza = 17,
            configuracionCamara = 18,
            configuracionCartel = 19,
            suboBarreraEgreso = 20,
            bajoBarreraEgreso = 21,
            suboBarreraIngreso = 22,
            bajoBarreraIngreso = 23,
            configuracionTiempos = 24,
            configuracionBaseDatos = 25,
            cambioEstadoBalaza = 26,
            ResetControlAcceso = 27,
            configuracionImpresora = 28,
            Tag_Habilitado = 29,
            Habilitar_todo = 30,
            Comando_Interface = 31,
            DesconexionPerifericos = 32,
            Peso_Balanza = 33


        }


        //      Public Enum TipoConexion
        //    NormalAbierto = 1
        //    NormalCerrado = 2

        //End Enum

        public enum TipoConexion
        {
            NormalAbierto = 1,
            NormalCerrado = 2
        }


        public static EstadoHabilitacion getEstadoHabilitacion(Int16 dato, Int16 bitPosicion)
        {
            int resultado;

            resultado = (dato & (int)Math.Pow(2, bitPosicion));

            //Si el resultado es mayor que 0 es porque el bit esta en 1 , y si es 0  el bit esta en 0
            // y si el bit esta en 1 el sensor esta de
            return resultado > 0 ? EstadoHabilitacion.Habilitado : EstadoHabilitacion.Deshabilitado;
        }

        public static SensorEstadoFalla getEstadoSensorFalla(Int16 datoSensor, Int16 bitPosicion)
        {
            int resultado;

            resultado = (datoSensor & (int)Math.Pow(2, bitPosicion));

            //Si el resultado es mayor que 0 es porque el bit esta en 1 , y si es 0  el bit esta en 0
            // y si el bit esta en 1 el sensor esta de
            return resultado > 0 ? SensorEstadoFalla.EnFalla : SensorEstadoFalla.Normal;


        }

        public static double BinarioADecimal(string BinStr)
        {
            double mult = 0;
            double DecNum = 0;
            mult = 1;
            DecNum = 0;

            int i = 0;
            for (i = BinStr.Length - 1; i >= 0; i += -1)
            {
                if (BinStr.Substring(i, 1) == "1")
                {
                    DecNum = DecNum + mult;
                }
                mult = mult * 2;
            }


            return DecNum;
        }

        /// <summary>
        /// FORMATO DEL NOMBRE DE TAG PLC
        /// [NombreCtrl]_CA[idControlAcceso]_SEC[idSector]
        /// </summary>
        /// <param name="name"></param>
        /// <param name="idControlAcceso"></param>
        /// <param name="idSector"></param>
        /// <returns></returns>
        public static string GetNombreTag(string name, int idControlAcceso, int idSector)
        {
            return string.Format("{0}_CA{1}_SEC{2}", name, idControlAcceso, idSector);
        }
        /// <summary>
        /// Permite devolver el nombre del tag quitandole la propiedad
        /// la propiedad es lo que sigue despues del "."
        /// Ejemplo: Balanza.Estado
        /// Devuelve: Balanza
        /// </summary>
        /// <param name="tag">Nombre del tag con la propiedad</param>
        /// <returns>El nombre del tag sin la propiedad</returns>
        public static string getNombreTag(string tag)
        {
            int punto = tag.IndexOf(".");

            return tag.Substring(0, punto);
        }

        /// <summary>
        /// Permite devolver el nombre de la propiedad del tag
        /// Ej: Balanza1.Comando return Comando
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static string getNombrePropiedad(string tag)
        {
            int punto = tag.IndexOf(".");

            return tag.Substring(punto + 1);
        }




        public void GenerarEvento(string mensaje, int id, int tipo)
        {
            System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog();

            // Check if the event source exists. If not create it.
            if (!System.Diagnostics.EventLog.SourceExists("axConrol"))
            {
                System.Diagnostics.EventLog.CreateEventSource("axConrol", "Application");
            }

            eventLog.Source = "axControl";

            var tEvento = System.Diagnostics.EventLogEntryType.Information;
            switch (tipo)
            {
                case 1:
                    tEvento = System.Diagnostics.EventLogEntryType.Error;
                    break;
                case 2:
                    tEvento = System.Diagnostics.EventLogEntryType.Warning;
                    break;
                case 4:
                    tEvento = System.Diagnostics.EventLogEntryType.Information;
                    break;
                case 8:
                    tEvento = System.Diagnostics.EventLogEntryType.SuccessAudit;
                    break;
                case 16:
                    tEvento = System.Diagnostics.EventLogEntryType.FailureAudit;
                    break;
            }


            // Create an event ID to add to the event log
            //int eventID = id;
            //string mensaje = string.Format("PATENTE: {1}{0}BARRERA: {2}{0}PESAJE: {3}{0}", Environment.NewLine, patente, BarreraActual, pesajeActual);
            // Write an entry to the event log.
            eventLog.WriteEntry(mensaje,
                                tEvento,
                                id);

            // Close the Event Log
            eventLog.Close();

        }


        public enum TipoEventoLog
        {
            Error = 1,
            FailureAudit = 16,
            Information = 4,
            SuccessAudit = 8,
            Warning = 2
        }

        public enum Controles
        {
            PORTERIA_Ingreso_Egreso = 100,
            ROTONDA_INGRESO = 200,
            CALADO = 300,
            BALANZAS_INGRESO_EGRESO = 400,
            DESCARGA = 500,
            CALLE_INGRESO_EGRESO_CARGA = 600,
            CARGA_DE_CAMIONES = 700,
            PORTERIA_PROVEEDORES_y_CALLE_COSTERA = 800,
            PORTERIA_LABORATORIO_y_CALLE_INTERNA = 900,
            MANTENIMIENTO = 1200,
            INTERFACE = 2000
        }


        public enum EstadoBarrera
        {
            ArribaAutomatica = 1,
            AbajoAutomatica = 2,
            ArribaManual = 3,
            AbajoManual = 4,
            Falla = 5
        }

        public enum EstadoSensor
        {
            Deshabilitado = 0,
            SensorConPresencia = 1,
            SensorSinPresencia = 2,
            Falla = 3

        }

        public enum EstadoSemaforo
        {
            Verde = 1,
            Rojo = 2,
            Apagado = 3
        }

        public enum TipoLectura
        {
            Constante = 1,
            Corte_Espira = 2
        }
        /// <summary>
        /// Id del plc de aumax , que se comunica con los plc de electrolux
        /// </summary>
        public enum ID_PLC
        {
            /// <summary>
            /// PLC que gestiona la comunicación en el sector de DESCARGA Y SILOS (T5)
            /// </summary>
            SILOS_DESCARGA = 1,
            /// <summary>
            /// PLC que gestiona la counicación en el sector de CARGA_TRASILE_ACONDICIONADORA (T6)
            /// </summary>
            CARGA_TRASILE_ACONDICIONADORA = 2,
            /// <summary>
            /// PLC que gestiona la comunicación en la CARGA DE BUQUE 
            /// </summary>
            CARGA_BUQUE = 3

        }

        public enum DB_PLC
        {
            SILOS_DESCARGA = 1000,
            CARGA_TRASILE_ACONDICIONADORA = 2,
            CARGA_BUQUE = 3
        }

        /// <summary>
        /// Enumerador del id de evento
        /// </summary>
        public enum ID_EVENTOS
        {
            EXITO = 1,
            INFORMACION = 2,
            ERROR = 3
        }

        /// <summary>
        /// DIRECCIONAMIENTO WORD DEL DB DE SILOS
        /// TAG	DATA TYPE	OFFSET
        ///IdComando DInt	0.0
        ///Comando Int	4.0
        ///IdSilo Int	6.0
        ///Silo String[20]	8.0
        ///Id Producto Int	30.0
        ///Producto String[20]	32.0
        ///Capacidad DInt	54.0
        ///ExistenTotal DInt	58.0
        ///IdComandoRta DInt	62.0
        ///EstadoRta Int	66.0
        ///MensajeRta String[50]	68.0
        ///IdSiloAlarma Int	120.0
        ///IdAlarma Int	122.0
        ///MensajeAlarma String[50]	124.0
        ///ValorAlarma Real	176.0

        /// </summary>
        public enum DIR_WORD_PLC_SILO
        {
            // AUMAX => ELECTROLUZ

            /// <summary>
            /// Id generado por BIT. Unívoco por cada operación realizada.
            /// </summary>
            ID_COMANDO = 0,
            /// <summary>
            /// 1= Modificación. No se usará el Alta, ya que previamente los Silos existirán en el PCS7. TRIGGER(debe ser monitoreado por Electroluz)
            /// </summary>
            COMANDO = 4,
            /// <summary>
            /// Identificador único por cada silo de la planta
            /// </summary>
            ID_SILO = 6,
            /// <summary>
            /// Nombre del Silo
            /// </summary>
            SILO = 8,
            /// <summary>
            /// Identificador único del producto contenido en el silo
            /// </summary>
            ID_PRODUCTO = 30,
            /// <summary>
            /// Nombre del Producto
            /// </summary>
            PRODUCTO = 32,
            /// <summary>
            /// Capacidad total en Kg del Silo
            /// </summary>
            CAPACIDAD = 54,
            /// <summary>
            /// Existencia total en Kg del producto en el Silo
            /// </summary>
            EXISTEN_TOTAL = 58,

            //ELECTROLUX => AUMAX
            /// <summary>
            /// Id generado por BIT. Utilizado por axControl para retornar la respuesta haciendo referencia al Id de comando generado por BIT
            /// </summary>
            ID_COMANDO_RTA = 62,
            /// <summary>
            /// 1= Procesado, 2=Error en axControl, 3=Error en PLC S71200, 4=Error en PCS7, 5=TimeOut. TRIGGER(debe ser monitoreado por AuMax)
            /// </summary>
            ESTADO_RTA = 66,
            /// <summary>
            /// Mensaje para describir el estado de la respuesta
            /// </summary>
            MENSAJE_RTA = 68,
            /// <summary>
            /// Identificador único por cada silo de la planta
            /// </summary>
            ID_SILO_ALARMA = 120,
            /// <summary>
            /// 1= Nivel Bajo en Silo, 2= Nivel Alto en Silo, ….. (a definir entre BIT y Electroluz)
            /// </summary>
            ID_ALARMA = 122,
            /// <summary>
            /// Mensaje de la Alarma
            /// </summary>
            MENSAJE_ALARMA = 124,
            /// <summary>
            /// Valor de la variable que genera la alarma
            /// </summary>
            VALOR_ALARMA = 176

        }

        public enum ESTADO_REGISTRO
        {
            /// <summary>
            /// "EL WS FUE REGISTRADO Y ESTA PENDIENTE DE SER PROCESADO POR EL SERVICIO AXINTERFACE
            /// </summary>
            SIN_PROCESAR = 1,
            /// <summary>
            /// "NO SE PUDO REGISTRAR EL WS EN EL PLC, QUEDA PENDIENTE A REGISTRARSE EN EL PLC"
            /// </summary>
            ERROR_REGISTRO_PLC = 2,
            /// <summary>
            /// EL WS FUE REGISTRADO EN EL PLC, Y QUEDA A LA ESPERA DE RESPUESTA 
            /// </summary>
            REGISTRADO_PLC = 3,
            /// <summary>
            /// SE RECIBIO RESPUESTA DEL PLC Y SE ENVIO MEDIANTE WS A BIT
            /// </summary>
            ENVIO_EXITOSO = 4,
            /// <summary>
            /// NO SE PUDO ENVIAR LA RESPUESTA DEL PLC A BIT, QUEDA PENDIENTE EL ENVIO
            /// </summary>
            ENVIO_ERRO = 5

        }

        public enum RTA_ALTA_MOD_SILO_ESTADO
        {
            PROCESADO = 1,
            ERROR_AXCONTROL = 2,
            ERROR_PLC_S71200 = 3,
            ERROR_PLC_PCS7 = 4,
            TIME_OUT = 5
        }

        public enum PasosCartelMultiled
        {
            SistemaDetenido = 0,
            TextoFijoPorDefecto = 1,
            TextoFijoAVANCE = 2,
            TextoFijoRETROCEDA = 3,
            TextoFijoESPERE = 4,
            TextoVariable = 5,
            FondoVerde = 6,
            FondoRojo = 7,
            TextoFijoFUERADESERVICIO = 8
        }

        /// <summary>
        /// Permite obtener el db del PLC segun la posición de cola
        /// </summary>
        public enum DB_POSICION_COLA_DESCARGA
        {
            POS_COLA_1 = 1001,
            POS_COLA_2 = 1002,
            POS_COLA_3 = 1003,
            POS_COLA_4 = 1004
        }
        /// <summary>
        /// Permite obtener el db del PLC segun la posición de cola
        /// </summary>
        public enum ID_EVENTO_OT_DESCARGA
        {
            RECEPCION = 1,
            ACTIVACION = 2,
            INICIO = 3,
            PAUSA = 4,
            FIN = 5,
            FIN_BATCH = 6,
            ERROR_TIME_OUT = 7,

        }
        /// <summary>
        /// Enumerador que permite saber la cantidad de db que va utilizar
        /// las interface del plc segun el sector (Silo,Descarga,Carga camiones, Carga Buqye)
        /// </summary>
        public enum CANT_DB_PLC_INTERFACE
        {
            SILO = 1,
            DESCARGA = 4,
            CARGA_CAMION = 4,
            CARGA_TRASILE = 4,
            CARGA_BUQUE = 7

        }
        /// <summary>
        /// 
        /// </summary>
        public enum START_DB_PLC_INTERFACE
        {
            SILO = 1,
            DESCARGA = 1001,
            CARGA_CAMION = 1001,
            CARGA_TRASILE = 1011,
            CARGA_BUQUE = 1001

        }


        public static string BorrarVbNull(string mensaje)
        {
            int indexNull = 0;
            indexNull = mensaje.IndexOf('\0');
            if (indexNull > -1)
            {
                return mensaje.Remove(indexNull, mensaje.Length - indexNull);
            }
            else
            {
                return mensaje;
            }
        }

        public enum TIPO_COMANDO
        {
            REINICIAR_CONTROL = 1,
            REINICIAR_SECTOR = 2,
            HABILITAR_TODO = 3,
            ESCRIBIR_CARTEL = 4,
            LECTURA_MANUAL = 5
        }
    }
}
