﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Threading;


namespace AxInterface
{


    /// <summary>
    /// Clase que permite gestionar los web servide de interface y la conexión con el plc
    /// </summary>
    public class InterfaceSilo
    {

        #region PROPIEDADES
        /// <summary>
        /// Id generado por BIT. Unívoco por cada operación realizada.
        /// </summary>
        public DATO_DWORDDINT idComando { get; set; }
        /// <summary>
        /// 1= Modificación. No se usará el Alta, ya que previamente los Silos existirán en el PCS7. TRIGGER(debe ser monitoreado por Electroluz)
        /// </summary>
        public DATO_WORDINT comando { get; set; }
        /// <summary>
        /// Identificador único por cada silo de la planta
        /// </summary>
        public DATO_WORDINT idSilo { get; set; }
        /// <summary>
        /// Nombre del Silo
        /// </summary>
        public DATO_BYTE silo { get; set; }
        /// <summary>
        /// Identificador único del producto contenido en el silo
        /// </summary>
        public DATO_WORDINT idProducto { get; set; }
        /// <summary>
        /// Nombre del Producto
        /// </summary>
        public DATO_BYTE producto { get; set; }
        /// <summary>
        /// Capacidad total en Kg del Silo
        /// </summary>
        public DATO_DWORDDINT capacidad { get; set; }
        /// <summary>
        /// Existencia total en Kg del producto en el Silo
        /// </summary>
        public DATO_DWORDDINT existenTotal { get; set; }
        /// <summary>
        /// Id generado por BIT. Utilizado por axControl para retornar la respuesta haciendo referencia al Id de comando generado por BIT
        /// </summary>
        public DATO_DWORDDINT idComandoRta { get; set; }
        /// <summary>
        /// 1= Procesado, 2=Error en axControl, 3=Error en PLC S71200, 4=Error en PCS7, 5=TimeOut. TRIGGER(debe ser monitoreado por AuMax)
        /// </summary>
        public DATO_WORDINT estadoRta { get; set; }
        /// <summary>
        /// Mensaje para describir el estado de la respuesta
        /// </summary>
        public DATO_BYTE mensajeRta { get; set; }
        /// <summary>
        /// Identificador único por cada silo de la planta
        /// </summary>
        public DATO_WORDINT idSiloAlarma { get; set; }
        /// <summary>
        /// 1= Nivel Bajo en Silo, 2= Nivel Alto en Silo, ….. (a definir entre BIT y Electroluz)
        /// </summary>
        public DATO_WORDINT idAlarma { get; set; }
        /// <summary>
        /// Mensaje de la Alarma
        /// </summary>
        public DATO_BYTE mensajeAlarma { get; set; }
        public decimal valorAlarma { get; set; }

        #endregion

        /// <summary>
        /// Permite obtener los ws de silo a procesar
        /// </summary>
        public void ProcesarWS()
        {
            Entidades.AxControlORAEntities db = new AxControlORAEntities();

            //Busco en la tabla INTERFACE_WS si tengo web service a procesar
            List<INTERFACE_WS> listInterfaceSilo = db.INTERFACE_WS.Where(o => o.WEB_SERVICE == WsJsonInputAumax.AltaModSilo._NOMBRE &&
            o.ID_ESTADO == (int)Entidades.Constante.ESTADO_REGISTRO.SIN_PROCESAR).ToList();

            if (listInterfaceSilo.Count > 0) // Si tengo ws a procesar
            {
                addEvento(Constante.ID_EVENTOS.INFORMACION, "WS A PROCESAR = " + listInterfaceSilo.Count, null);
                foreach (var itemInterfaceSilo in listInterfaceSilo) // recorro
                {
                    //Escribo en el plc los datos del ws
                    bool registroExitoso = registrarDatosPLC(itemInterfaceSilo);
                    //Guardo el estado del registro
                    itemInterfaceSilo.ID_ESTADO = registroExitoso ? (int)Entidades.Constante.ESTADO_REGISTRO.REGISTRADO_PLC :
                        (int)Constante.ESTADO_REGISTRO.ERROR_REGISTRO_PLC;
                }
                db.SaveChanges();
            }

        }


        /// <summary>
        /// Hilo que se ejecuta de forma infinita para procesar los web service que estan en la tabla GESTION DE SILOS
        /// Encargado de grabar los datos en el PLC de aumax y de escuchar la respuesta del plc
        /// </summary>
        public void runThread()
        {
            //Creo el objeto plc siemens
          
        }

        /// <summary>
        /// Permite escribir en el plc los datos de ws
        /// </summary>
        /// <param name="getionSilos"></param>
        public bool registrarDatosPLC(Entidades.INTERFACE_WS interfaceSilo)
        {

            try
            {
                addEvento(Constante.ID_EVENTOS.INFORMACION, "[registrarDatosPLC] REGISTRAR WS = " + interfaceSilo.JSON_INPUT, interfaceSilo.ID_REGISTRO);
                //desearilizo
                Entidades.WsJsonInputAumax.AltaModSilo altaModSilo = Newtonsoft.Json.JsonConvert.DeserializeObject<Entidades.WsJsonInputAumax.AltaModSilo>(interfaceSilo.JSON_INPUT);



                // addEvento(Constante.ID_EVENTOS.EXITO, string.Format("[registrarDatosPLC] Los datos del WS con idComando = {0} se escribieron en el plc ip  = {1} de forma exitosa ", altaModSilo.idComando, this.plcSiemens.IP), interfaceSilo.ID_REGISTRO);
                return true;
            }
            catch (Exception ex)
            {

                addEvento(Constante.ID_EVENTOS.ERROR, String.Format("[registrarDatosPLC] ID_REGISTRO = {0} , MENSAJE = {1},  ", interfaceSilo.ID_REGISTRO, ex.Message), interfaceSilo.ID_REGISTRO);

                return false;
            }


        }


        /// <summary>
        /// Consulto al plc si tengo alguna RESPUESTA A PROCESAR
        /// </summary>
        public void leerPLC()
        {
            try
            {
              


            }
            catch (Exception ex)
            {

                addEvento(Constante.ID_EVENTOS.ERROR, String.Format("[leerRTA_PLC] Mensaje = {0} ", ex.Message), null);
            }


        }

        /// <summary>
        /// Permite leer las variables del plc que tienen la rta del comando
        /// </summary>
        /// <param name="idComandoRta"></param>
        private void leerComandoRta(uint idComandoRta)
        {
        

            //// Envio la rtaModAltSilo a bit mediante un hilo
            //Thread threadEnviarRtaComando = new Thread(() => enviarRtaAltaModSilo(rtaAltaModSilo));
            //threadEnviarRtaComando.IsBackground = true;
            //threadEnviarRtaComando.Start();

           
        }

        /// <summary>
        /// permite enviar al respuesta del comando altamodsilo
        /// </summary>
        /// <param name="rta"></param>
        public void enviarRtaAltaModSilo(WsJsonInputBit.RtaAltaModSilo rta)
        {
          


        }


        /// <summary>
        /// Permite las variables del plc que permite obtener los datos del la alarma
        /// </summary>
        /// <param name="idAlarma"></param>
        private void leerAlarma(uint idAlarma)
        {
         
        }

        /// <summary>
        /// Permite enviar el ws de alarma de silo a bit
        /// </summary>
        /// <param name="alarma"></param>
        public void enviarAlarmaSilo(WsJsonInputBit.AlarmaSilo alarma)
        {
          
           

        }

        private void addEvento(Entidades.Constante.ID_EVENTOS idEvento, string mensaje, Int64? idRegistro)
        {
        
        }




    }
}
