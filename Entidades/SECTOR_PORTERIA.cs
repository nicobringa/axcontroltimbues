﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class SECTOR_PORTERIA
    {
        public int ID_SECTOR { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<int> ID_CONTROL_ACCESO { get; set; }
        public string DESCRIPCION { get; set; }

        public int Barrera { get; set; }
        public int Sensor { get; set; }
        public int Semaforo1 { get; set; }
        public int Semaforo2 { get; set; }

        public int Espira { get; set; }
    }
}
