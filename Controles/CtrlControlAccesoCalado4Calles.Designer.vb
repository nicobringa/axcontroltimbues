﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlControlAccesoCalado4Calles
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoCalado4Calles))
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.ctrlCalado4 = New Controles.CtrlDinamismoCalado()
        Me.ctrlCalado3 = New Controles.CtrlDinamismoCalado()
        Me.ctrlCalado2 = New Controles.CtrlDinamismoCalado()
        Me.ctrlCalado1 = New Controles.CtrlDinamismoCalado()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CartelLED_PUERTO_CALADO1 = New Controles.CtrlCartelMultiLED()
        Me.CartelLED_PUERTO_CALADO2 = New Controles.CtrlCartelMultiLED()
        Me.CartelLED_PUERTO_CALADO3 = New Controles.CtrlCartelMultiLED()
        Me.CartelLED_PUERTO_CALADO4 = New Controles.CtrlCartelMultiLED()
        Me.CAM_PUERTO_CALADO1 = New Controles.CtrlCamara()
        Me.CAM_PUERTO_CALADO2 = New Controles.CtrlCamara()
        Me.CAM_PUERTO_CALADO3 = New Controles.CtrlCamara()
        Me.CAM_PUERTO_CALADO4 = New Controles.CtrlCamara()
        Me.CtrlAntenaLeyendoCalado1 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoCalado2 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoCalado3 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoCalado4 = New Controles.CtrlAntenaLeyendo()
        Me.S_LECTURA_CALADO1 = New Controles.CtrlSensor()
        Me.S_LECTURA_CALADO2 = New Controles.CtrlSensor()
        Me.S_LECTURA_CALADO3 = New Controles.CtrlSensor()
        Me.S_LECTURA_CALADO4 = New Controles.CtrlSensor()
        Me.lblSeparador = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, 43)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 113
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'ctrlCalado4
        '
        Me.ctrlCalado4.BackgroundImage = CType(resources.GetObject("ctrlCalado4.BackgroundImage"), System.Drawing.Image)
        Me.ctrlCalado4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ctrlCalado4.GPIO = False
        Me.ctrlCalado4.ID_CONTROL_ACCESO = 0
        Me.ctrlCalado4.ID_PLC = 0
        Me.ctrlCalado4.ID_SECTOR = 0
        Me.ctrlCalado4.Inicializado = False
        Me.ctrlCalado4.Location = New System.Drawing.Point(975, 43)
        Me.ctrlCalado4.Name = "ctrlCalado4"
        Me.ctrlCalado4.PLC = Nothing
        Me.ctrlCalado4.Size = New System.Drawing.Size(303, 399)
        Me.ctrlCalado4.TabIndex = 5
        '
        'ctrlCalado3
        '
        Me.ctrlCalado3.BackgroundImage = CType(resources.GetObject("ctrlCalado3.BackgroundImage"), System.Drawing.Image)
        Me.ctrlCalado3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ctrlCalado3.GPIO = False
        Me.ctrlCalado3.ID_CONTROL_ACCESO = 0
        Me.ctrlCalado3.ID_PLC = 0
        Me.ctrlCalado3.ID_SECTOR = 0
        Me.ctrlCalado3.Inicializado = False
        Me.ctrlCalado3.Location = New System.Drawing.Point(651, 42)
        Me.ctrlCalado3.Name = "ctrlCalado3"
        Me.ctrlCalado3.PLC = Nothing
        Me.ctrlCalado3.Size = New System.Drawing.Size(308, 400)
        Me.ctrlCalado3.TabIndex = 4
        '
        'ctrlCalado2
        '
        Me.ctrlCalado2.BackgroundImage = CType(resources.GetObject("ctrlCalado2.BackgroundImage"), System.Drawing.Image)
        Me.ctrlCalado2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ctrlCalado2.GPIO = False
        Me.ctrlCalado2.ID_CONTROL_ACCESO = 0
        Me.ctrlCalado2.ID_PLC = 0
        Me.ctrlCalado2.ID_SECTOR = 0
        Me.ctrlCalado2.Inicializado = False
        Me.ctrlCalado2.Location = New System.Drawing.Point(325, 43)
        Me.ctrlCalado2.Name = "ctrlCalado2"
        Me.ctrlCalado2.PLC = Nothing
        Me.ctrlCalado2.Size = New System.Drawing.Size(307, 399)
        Me.ctrlCalado2.TabIndex = 3
        '
        'ctrlCalado1
        '
        Me.ctrlCalado1.BackgroundImage = CType(resources.GetObject("ctrlCalado1.BackgroundImage"), System.Drawing.Image)
        Me.ctrlCalado1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ctrlCalado1.GPIO = False
        Me.ctrlCalado1.ID_CONTROL_ACCESO = 0
        Me.ctrlCalado1.ID_PLC = 0
        Me.ctrlCalado1.ID_SECTOR = 0
        Me.ctrlCalado1.Inicializado = False
        Me.ctrlCalado1.Location = New System.Drawing.Point(0, 43)
        Me.ctrlCalado1.Name = "ctrlCalado1"
        Me.ctrlCalado1.PLC = Nothing
        Me.ctrlCalado1.Size = New System.Drawing.Size(309, 399)
        Me.ctrlCalado1.TabIndex = 2
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(1278, 39)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(229, 494)
        Me.CtrlPanelNotificaciones1.TabIndex = 0
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(1507, 43)
        Me.lblNombre.TabIndex = 167
        Me.lblNombre.Text = "Nombre Calado"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CartelLED_PUERTO_CALADO1
        '
        Me.CartelLED_PUERTO_CALADO1.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.CartelLED_PUERTO_CALADO1.Conectado = False
        Me.CartelLED_PUERTO_CALADO1.GPIO = False
        Me.CartelLED_PUERTO_CALADO1.ID_CONTROL_ACCESO = 0
        Me.CartelLED_PUERTO_CALADO1.ID_PLC = 0
        Me.CartelLED_PUERTO_CALADO1.ID_SECTOR = 0
        Me.CartelLED_PUERTO_CALADO1.Inicializado = False
        Me.CartelLED_PUERTO_CALADO1.Location = New System.Drawing.Point(127, 331)
        Me.CartelLED_PUERTO_CALADO1.Name = "CartelLED_PUERTO_CALADO1"
        Me.CartelLED_PUERTO_CALADO1.PLC = Nothing
        Me.CartelLED_PUERTO_CALADO1.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_PUERTO_CALADO1.TabIndex = 181
        '
        'CartelLED_PUERTO_CALADO2
        '
        Me.CartelLED_PUERTO_CALADO2.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.CartelLED_PUERTO_CALADO2.Conectado = False
        Me.CartelLED_PUERTO_CALADO2.GPIO = False
        Me.CartelLED_PUERTO_CALADO2.ID_CONTROL_ACCESO = 0
        Me.CartelLED_PUERTO_CALADO2.ID_PLC = 0
        Me.CartelLED_PUERTO_CALADO2.ID_SECTOR = 0
        Me.CartelLED_PUERTO_CALADO2.Inicializado = False
        Me.CartelLED_PUERTO_CALADO2.Location = New System.Drawing.Point(447, 331)
        Me.CartelLED_PUERTO_CALADO2.Name = "CartelLED_PUERTO_CALADO2"
        Me.CartelLED_PUERTO_CALADO2.PLC = Nothing
        Me.CartelLED_PUERTO_CALADO2.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_PUERTO_CALADO2.TabIndex = 182
        '
        'CartelLED_PUERTO_CALADO3
        '
        Me.CartelLED_PUERTO_CALADO3.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.CartelLED_PUERTO_CALADO3.Conectado = False
        Me.CartelLED_PUERTO_CALADO3.GPIO = False
        Me.CartelLED_PUERTO_CALADO3.ID_CONTROL_ACCESO = 0
        Me.CartelLED_PUERTO_CALADO3.ID_PLC = 0
        Me.CartelLED_PUERTO_CALADO3.ID_SECTOR = 0
        Me.CartelLED_PUERTO_CALADO3.Inicializado = False
        Me.CartelLED_PUERTO_CALADO3.Location = New System.Drawing.Point(779, 331)
        Me.CartelLED_PUERTO_CALADO3.Name = "CartelLED_PUERTO_CALADO3"
        Me.CartelLED_PUERTO_CALADO3.PLC = Nothing
        Me.CartelLED_PUERTO_CALADO3.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_PUERTO_CALADO3.TabIndex = 183
        '
        'CartelLED_PUERTO_CALADO4
        '
        Me.CartelLED_PUERTO_CALADO4.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.CartelLED_PUERTO_CALADO4.Conectado = False
        Me.CartelLED_PUERTO_CALADO4.GPIO = False
        Me.CartelLED_PUERTO_CALADO4.ID_CONTROL_ACCESO = 0
        Me.CartelLED_PUERTO_CALADO4.ID_PLC = 0
        Me.CartelLED_PUERTO_CALADO4.ID_SECTOR = 0
        Me.CartelLED_PUERTO_CALADO4.Inicializado = False
        Me.CartelLED_PUERTO_CALADO4.Location = New System.Drawing.Point(1098, 331)
        Me.CartelLED_PUERTO_CALADO4.Name = "CartelLED_PUERTO_CALADO4"
        Me.CartelLED_PUERTO_CALADO4.PLC = Nothing
        Me.CartelLED_PUERTO_CALADO4.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_PUERTO_CALADO4.TabIndex = 184
        '
        'CAM_PUERTO_CALADO1
        '
        Me.CAM_PUERTO_CALADO1.AutoSize = True
        Me.CAM_PUERTO_CALADO1.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_PUERTO_CALADO1.frmCamara = Nothing
        Me.CAM_PUERTO_CALADO1.GPIO = False
        Me.CAM_PUERTO_CALADO1.ID_CONTROL_ACCESO = 0
        Me.CAM_PUERTO_CALADO1.ID_PLC = 0
        Me.CAM_PUERTO_CALADO1.ID_SECTOR = 0
        Me.CAM_PUERTO_CALADO1.Inicializado = False
        Me.CAM_PUERTO_CALADO1.Location = New System.Drawing.Point(220, 193)
        Me.CAM_PUERTO_CALADO1.Name = "CAM_PUERTO_CALADO1"
        Me.CAM_PUERTO_CALADO1.PLC = Nothing
        Me.CAM_PUERTO_CALADO1.Size = New System.Drawing.Size(78, 60)
        Me.CAM_PUERTO_CALADO1.TabIndex = 187
        '
        'CAM_PUERTO_CALADO2
        '
        Me.CAM_PUERTO_CALADO2.AutoSize = True
        Me.CAM_PUERTO_CALADO2.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_PUERTO_CALADO2.frmCamara = Nothing
        Me.CAM_PUERTO_CALADO2.GPIO = False
        Me.CAM_PUERTO_CALADO2.ID_CONTROL_ACCESO = 0
        Me.CAM_PUERTO_CALADO2.ID_PLC = 0
        Me.CAM_PUERTO_CALADO2.ID_SECTOR = 0
        Me.CAM_PUERTO_CALADO2.Inicializado = False
        Me.CAM_PUERTO_CALADO2.Location = New System.Drawing.Point(549, 193)
        Me.CAM_PUERTO_CALADO2.Name = "CAM_PUERTO_CALADO2"
        Me.CAM_PUERTO_CALADO2.PLC = Nothing
        Me.CAM_PUERTO_CALADO2.Size = New System.Drawing.Size(78, 60)
        Me.CAM_PUERTO_CALADO2.TabIndex = 188
        '
        'CAM_PUERTO_CALADO3
        '
        Me.CAM_PUERTO_CALADO3.AutoSize = True
        Me.CAM_PUERTO_CALADO3.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_PUERTO_CALADO3.frmCamara = Nothing
        Me.CAM_PUERTO_CALADO3.GPIO = False
        Me.CAM_PUERTO_CALADO3.ID_CONTROL_ACCESO = 0
        Me.CAM_PUERTO_CALADO3.ID_PLC = 0
        Me.CAM_PUERTO_CALADO3.ID_SECTOR = 0
        Me.CAM_PUERTO_CALADO3.Inicializado = False
        Me.CAM_PUERTO_CALADO3.Location = New System.Drawing.Point(872, 172)
        Me.CAM_PUERTO_CALADO3.Name = "CAM_PUERTO_CALADO3"
        Me.CAM_PUERTO_CALADO3.PLC = Nothing
        Me.CAM_PUERTO_CALADO3.Size = New System.Drawing.Size(78, 60)
        Me.CAM_PUERTO_CALADO3.TabIndex = 189
        '
        'CAM_PUERTO_CALADO4
        '
        Me.CAM_PUERTO_CALADO4.AutoSize = True
        Me.CAM_PUERTO_CALADO4.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_PUERTO_CALADO4.frmCamara = Nothing
        Me.CAM_PUERTO_CALADO4.GPIO = False
        Me.CAM_PUERTO_CALADO4.ID_CONTROL_ACCESO = 0
        Me.CAM_PUERTO_CALADO4.ID_PLC = 0
        Me.CAM_PUERTO_CALADO4.ID_SECTOR = 0
        Me.CAM_PUERTO_CALADO4.Inicializado = False
        Me.CAM_PUERTO_CALADO4.Location = New System.Drawing.Point(1191, 171)
        Me.CAM_PUERTO_CALADO4.Name = "CAM_PUERTO_CALADO4"
        Me.CAM_PUERTO_CALADO4.PLC = Nothing
        Me.CAM_PUERTO_CALADO4.Size = New System.Drawing.Size(78, 60)
        Me.CAM_PUERTO_CALADO4.TabIndex = 190
        '
        'CtrlAntenaLeyendoCalado1
        '
        Me.CtrlAntenaLeyendoCalado1.AutoSize = True
        Me.CtrlAntenaLeyendoCalado1.BackColor = System.Drawing.Color.White
        Me.CtrlAntenaLeyendoCalado1.Location = New System.Drawing.Point(211, 96)
        Me.CtrlAntenaLeyendoCalado1.Name = "CtrlAntenaLeyendoCalado1"
        Me.CtrlAntenaLeyendoCalado1.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAntenaLeyendoCalado1.TabIndex = 191
        Me.CtrlAntenaLeyendoCalado1.UltTag = Nothing
        '
        'CtrlAntenaLeyendoCalado2
        '
        Me.CtrlAntenaLeyendoCalado2.AutoSize = True
        Me.CtrlAntenaLeyendoCalado2.BackColor = System.Drawing.Color.White
        Me.CtrlAntenaLeyendoCalado2.Location = New System.Drawing.Point(540, 96)
        Me.CtrlAntenaLeyendoCalado2.Name = "CtrlAntenaLeyendoCalado2"
        Me.CtrlAntenaLeyendoCalado2.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAntenaLeyendoCalado2.TabIndex = 192
        Me.CtrlAntenaLeyendoCalado2.UltTag = Nothing
        '
        'CtrlAntenaLeyendoCalado3
        '
        Me.CtrlAntenaLeyendoCalado3.AutoSize = True
        Me.CtrlAntenaLeyendoCalado3.BackColor = System.Drawing.Color.White
        Me.CtrlAntenaLeyendoCalado3.Location = New System.Drawing.Point(863, 96)
        Me.CtrlAntenaLeyendoCalado3.Name = "CtrlAntenaLeyendoCalado3"
        Me.CtrlAntenaLeyendoCalado3.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAntenaLeyendoCalado3.TabIndex = 193
        Me.CtrlAntenaLeyendoCalado3.UltTag = Nothing
        '
        'CtrlAntenaLeyendoCalado4
        '
        Me.CtrlAntenaLeyendoCalado4.AutoSize = True
        Me.CtrlAntenaLeyendoCalado4.BackColor = System.Drawing.Color.White
        Me.CtrlAntenaLeyendoCalado4.Location = New System.Drawing.Point(1182, 96)
        Me.CtrlAntenaLeyendoCalado4.Name = "CtrlAntenaLeyendoCalado4"
        Me.CtrlAntenaLeyendoCalado4.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAntenaLeyendoCalado4.TabIndex = 194
        Me.CtrlAntenaLeyendoCalado4.UltTag = Nothing
        '
        'S_LECTURA_CALADO1
        '
        Me.S_LECTURA_CALADO1.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.S_LECTURA_CALADO1.Estado = False
        Me.S_LECTURA_CALADO1.GPIO = True
        Me.S_LECTURA_CALADO1.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_CALADO1.ID_PLC = 0
        Me.S_LECTURA_CALADO1.ID_SECTOR = 0
        Me.S_LECTURA_CALADO1.Inicializado = False
        Me.S_LECTURA_CALADO1.Location = New System.Drawing.Point(4, 331)
        Me.S_LECTURA_CALADO1.Name = "S_LECTURA_CALADO1"
        Me.S_LECTURA_CALADO1.PLC = Nothing
        Me.S_LECTURA_CALADO1.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_CALADO1.TabIndex = 195
        '
        'S_LECTURA_CALADO2
        '
        Me.S_LECTURA_CALADO2.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.S_LECTURA_CALADO2.Estado = False
        Me.S_LECTURA_CALADO2.GPIO = True
        Me.S_LECTURA_CALADO2.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_CALADO2.ID_PLC = 0
        Me.S_LECTURA_CALADO2.ID_SECTOR = 0
        Me.S_LECTURA_CALADO2.Inicializado = False
        Me.S_LECTURA_CALADO2.Location = New System.Drawing.Point(331, 331)
        Me.S_LECTURA_CALADO2.Name = "S_LECTURA_CALADO2"
        Me.S_LECTURA_CALADO2.PLC = Nothing
        Me.S_LECTURA_CALADO2.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_CALADO2.TabIndex = 196
        '
        'S_LECTURA_CALADO3
        '
        Me.S_LECTURA_CALADO3.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.S_LECTURA_CALADO3.Estado = False
        Me.S_LECTURA_CALADO3.GPIO = True
        Me.S_LECTURA_CALADO3.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_CALADO3.ID_PLC = 0
        Me.S_LECTURA_CALADO3.ID_SECTOR = 0
        Me.S_LECTURA_CALADO3.Inicializado = False
        Me.S_LECTURA_CALADO3.Location = New System.Drawing.Point(657, 331)
        Me.S_LECTURA_CALADO3.Name = "S_LECTURA_CALADO3"
        Me.S_LECTURA_CALADO3.PLC = Nothing
        Me.S_LECTURA_CALADO3.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_CALADO3.TabIndex = 197
        '
        'S_LECTURA_CALADO4
        '
        Me.S_LECTURA_CALADO4.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.S_LECTURA_CALADO4.Estado = False
        Me.S_LECTURA_CALADO4.GPIO = True
        Me.S_LECTURA_CALADO4.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_CALADO4.ID_PLC = 0
        Me.S_LECTURA_CALADO4.ID_SECTOR = 0
        Me.S_LECTURA_CALADO4.Inicializado = False
        Me.S_LECTURA_CALADO4.Location = New System.Drawing.Point(981, 331)
        Me.S_LECTURA_CALADO4.Name = "S_LECTURA_CALADO4"
        Me.S_LECTURA_CALADO4.PLC = Nothing
        Me.S_LECTURA_CALADO4.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_CALADO4.TabIndex = 198
        '
        'lblSeparador
        '
        Me.lblSeparador.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblSeparador.Location = New System.Drawing.Point(306, 43)
        Me.lblSeparador.Name = "lblSeparador"
        Me.lblSeparador.Size = New System.Drawing.Size(19, 399)
        Me.lblSeparador.TabIndex = 199
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(632, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 401)
        Me.Label1.TabIndex = 200
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(956, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 401)
        Me.Label2.TabIndex = 201
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(1191, 43)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 203
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(1234, 43)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(43, 43)
        Me.btnAyuda.TabIndex = 202
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 442)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(1278, 91)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 1
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(1133, 42)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 204
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CtrlControlAccesoCalado4Calles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblSeparador)
        Me.Controls.Add(Me.S_LECTURA_CALADO4)
        Me.Controls.Add(Me.S_LECTURA_CALADO3)
        Me.Controls.Add(Me.S_LECTURA_CALADO2)
        Me.Controls.Add(Me.S_LECTURA_CALADO1)
        Me.Controls.Add(Me.CtrlAntenaLeyendoCalado4)
        Me.Controls.Add(Me.CtrlAntenaLeyendoCalado3)
        Me.Controls.Add(Me.CtrlAntenaLeyendoCalado2)
        Me.Controls.Add(Me.CtrlAntenaLeyendoCalado1)
        Me.Controls.Add(Me.CAM_PUERTO_CALADO4)
        Me.Controls.Add(Me.CAM_PUERTO_CALADO3)
        Me.Controls.Add(Me.CAM_PUERTO_CALADO2)
        Me.Controls.Add(Me.CAM_PUERTO_CALADO1)
        Me.Controls.Add(Me.CartelLED_PUERTO_CALADO4)
        Me.Controls.Add(Me.CartelLED_PUERTO_CALADO3)
        Me.Controls.Add(Me.CartelLED_PUERTO_CALADO2)
        Me.Controls.Add(Me.CartelLED_PUERTO_CALADO1)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.ctrlCalado2)
        Me.Controls.Add(Me.ctrlCalado1)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.ctrlCalado3)
        Me.Controls.Add(Me.ctrlCalado4)
        Me.Name = "CtrlControlAccesoCalado4Calles"
        Me.Size = New System.Drawing.Size(1502, 532)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents ctrlCalado1 As CtrlDinamismoCalado
    Friend WithEvents ctrlCalado2 As CtrlDinamismoCalado
    Friend WithEvents ctrlCalado3 As CtrlDinamismoCalado
    Friend WithEvents ctrlCalado4 As CtrlDinamismoCalado
    Friend WithEvents btnLectorRfid As Button
    Protected WithEvents lblNombre As Label
    Friend WithEvents CartelLED_PUERTO_CALADO1 As CtrlCartelMultiLED
    Friend WithEvents CartelLED_PUERTO_CALADO2 As CtrlCartelMultiLED
    Friend WithEvents CartelLED_PUERTO_CALADO3 As CtrlCartelMultiLED
    Friend WithEvents CartelLED_PUERTO_CALADO4 As CtrlCartelMultiLED
    Friend WithEvents CAM_PUERTO_CALADO1 As CtrlCamara
    Friend WithEvents CAM_PUERTO_CALADO2 As CtrlCamara
    Friend WithEvents CAM_PUERTO_CALADO3 As CtrlCamara
    Friend WithEvents CAM_PUERTO_CALADO4 As CtrlCamara
    Friend WithEvents CtrlAntenaLeyendoCalado1 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoCalado2 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoCalado3 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoCalado4 As CtrlAntenaLeyendo
    Friend WithEvents S_LECTURA_CALADO1 As CtrlSensor
    Friend WithEvents S_LECTURA_CALADO2 As CtrlSensor
    Friend WithEvents S_LECTURA_CALADO3 As CtrlSensor
    Friend WithEvents S_LECTURA_CALADO4 As CtrlSensor
    Friend WithEvents lblSeparador As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnReset As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents btnAlarma As Button
End Class
