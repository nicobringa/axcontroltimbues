﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class Puerto_RfidAD
    {

        public PUERTO_RFID getOne(int numPuerto , int idConf )
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.PUERTO_RFID.Where(a => a.NUM_PUERTO == numPuerto && a.ID_CONF_LECTOR_RFID == idConf).SingleOrDefault();

                return oConsulta;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }

        public PUERTO_RFID getOne( int idConf, string tag)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.PUERTO_RFID.Where(a =>  a.ID_CONF_LECTOR_RFID == idConf & a.TAG == tag).SingleOrDefault();

                return oConsulta;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }

        public PUERTO_RFID add(PUERTO_RFID puerto)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                 oContext.PUERTO_RFID.Add(puerto);
                oContext.SaveChanges();
                return puerto;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }

        public PUERTO_RFID update(PUERTO_RFID puerto)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.PUERTO_RFID.Where(a => a.NUM_PUERTO == puerto.NUM_PUERTO && a.ID_CONF_LECTOR_RFID == puerto.ID_CONF_LECTOR_RFID).SingleOrDefault();
                oConsulta.ID_SECTOR = puerto.ID_SECTOR;
                oConsulta.TAG = puerto.TAG;
                oConsulta.ESTADO = puerto.ESTADO;
                oContext.SaveChanges();
                return oConsulta;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }
    }
}
