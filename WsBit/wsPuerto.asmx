<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/">
      <s:element name="HabilitarTag">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="preqHabilitarTag" type="tns:reqHabilitar" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="reqHabilitar">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="IdSector" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="TAG" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="HabilitarTagResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="HabilitarTagResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RegistrarPesada">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="reqPesada" type="tns:reqPesada" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="reqPesada">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="IdSector" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="IdTransaccion" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Peso" type="s:int" />
        </s:sequence>
      </s:complexType>
      <s:element name="RegistrarPesadaResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="RegistrarPesadaResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="HabilitarTagSoapIn">
    <wsdl:part name="parameters" element="tns:HabilitarTag" />
  </wsdl:message>
  <wsdl:message name="HabilitarTagSoapOut">
    <wsdl:part name="parameters" element="tns:HabilitarTagResponse" />
  </wsdl:message>
  <wsdl:message name="RegistrarPesadaSoapIn">
    <wsdl:part name="parameters" element="tns:RegistrarPesada" />
  </wsdl:message>
  <wsdl:message name="RegistrarPesadaSoapOut">
    <wsdl:part name="parameters" element="tns:RegistrarPesadaResponse" />
  </wsdl:message>
  <wsdl:portType name="Servicio_AutomataSoap">
    <wsdl:operation name="HabilitarTag">
      <wsdl:input message="tns:HabilitarTagSoapIn" />
      <wsdl:output message="tns:HabilitarTagSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="RegistrarPesada">
      <wsdl:input message="tns:RegistrarPesadaSoapIn" />
      <wsdl:output message="tns:RegistrarPesadaSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="Servicio_AutomataSoap" type="tns:Servicio_AutomataSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="HabilitarTag">
      <soap:operation soapAction="http://tempuri.org/HabilitarTag" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RegistrarPesada">
      <soap:operation soapAction="http://tempuri.org/RegistrarPesada" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="Servicio_AutomataSoap12" type="tns:Servicio_AutomataSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="HabilitarTag">
      <soap12:operation soapAction="http://tempuri.org/HabilitarTag" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RegistrarPesada">
      <soap12:operation soapAction="http://tempuri.org/RegistrarPesada" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="Servicio_Automata">
    <wsdl:port name="Servicio_AutomataSoap" binding="tns:Servicio_AutomataSoap">
      <soap:address location="http://10.15.118.90:55703/wsPuerto.asmx" />
    </wsdl:port>
    <wsdl:port name="Servicio_AutomataSoap12" binding="tns:Servicio_AutomataSoap12">
      <soap12:address location="http://10.15.118.90:55703/wsPuerto.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>