﻿Imports Negocio.LectorRFID_SpeedWay
Imports Impinj.OctaneSdk
Imports Negocio.modDelegado
Public Class frmEscribirEPC
    Private WithEvents _LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay
    Private reader As New ImpinjReader


    ''' <summary>
    ''' limpio el texto de tag actual y lo reemplazo por el último leido de todos los sectores
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnLeer_Click(sender As Object, e As EventArgs) Handles btnLeer.Click
        SetText(Me, txtEpcActual, "")

        Dim tag As New Negocio.Ult_Tag_LeidoN
        SetText(Me, txtEpcActual, tag.ultimoTag)
    End Sub


    ''' <summary>
    ''' abro una conexión al ip indicado, y llamo a la funcion de escribir pasandole epcActual y el que quiero escribir
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnEscribir_Click(sender As Object, e As EventArgs) Handles btnEscribir.Click

        Try
            Dim ip As String = txtIpLector.Text
            If txtEpcActual.Text.Length > 0 And txtEscribirEPC.Text.Length > 0 Then

                reader.Connect(ip)
                Dim settings As Settings = reader.QueryDefaultSettings()
                reader.ApplySettings(settings)
                reader.Start()

                ProgramEpc(txtEpcActual.Text, txtEscribirEPC.Text)
            End If
        Catch ex As Exception
            MsgBox("¡Error: !  " & ex.Message)
        End Try


    End Sub

    ''' <summary>
    ''' escribo el nuevo valor sobre el epc del tag, va a funcionar siempre y cuando el tag esté cerca de la antena al llamar esta función.
    ''' </summary>
    ''' <param name="currentEpc"></param>
    ''' <param name="newEpc"></param>
    Public Sub ProgramEpc(ByVal currentEpc As String, ByVal newEpc As String)

        Try
            If (currentEpc.Length Mod 4 <> 0) OrElse (newEpc.Length Mod 4 <> 0) Then Throw New Exception("EPCs debe ser multiplo de 16 bits (4 caracteres hexadecimales)")
            Dim seq As TagOpSequence = New TagOpSequence()
            seq.TargetTag.MemoryBank = MemoryBank.Epc
            seq.TargetTag.BitPointer = BitPointers.Epc
            seq.TargetTag.Data = currentEpc
            Dim writeEpc As TagWriteOp = New TagWriteOp()
            writeEpc.Id = 123
            writeEpc.MemoryBank = MemoryBank.Epc
            writeEpc.Data = TagData.FromHexString(newEpc)
            writeEpc.WordPointer = WordPointers.Epc
            seq.Ops.Add(writeEpc)

            reader.AddOpSequence(seq)

            MsgBox("¡El tag fue escrito correctamente!")
        Catch ex As Exception
            MsgBox("¡Error: ocurrio un problema mientras se intentaba escribir el tag!" & ex.Message)
        End Try

    End Sub


End Class