﻿Public Class FrmConfiguracionWS
#Region "PROPIEDADES"
    Public oConfiguracion As New Entidades.Configuracion
    Public oConfiguracionN As New Negocio.ConfiguracionN
#End Region


#Region "CONSTRUCTOR"
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        cargar()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
#End Region

#Region "EVENTOS FORM"
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtRuta.Text.Length > 0 Then
            Dim oConfig As New Entidades.Configuracion
            oConfig = oConfiguracionN.GetOne
            oConfig.RUTA_WS = txtRuta.Text
            oConfig.SIMULACION_WS = cbxSimularRespuesta.Checked
            oConfig.RESPUES_WS = rbTrue.Checked
            oConfiguracionN.Guardar(oConfig)
            Me.Close()
        End If
    End Sub
#End Region


#Region "METODOS"
    Public Sub Cargar()
        oConfiguracion = oConfiguracionN.GetOne
        If oConfiguracion.RUTA_WS.Length > 0 Then
            txtRuta.Text = oConfiguracion.RUTA_WS
        End If
        cbxSimularRespuesta.Checked = oConfiguracion.RESPUES_WS
        gbRespuesta.Enabled = oConfiguracion.RESPUES_WS
        rbTrue.Checked = oConfiguracion.RESPUES_WS
    End Sub

    Private Sub cbxSimularRespuesta_CheckedChanged(sender As Object, e As EventArgs) Handles cbxSimularRespuesta.CheckedChanged
        gbRespuesta.Enabled = cbxSimularRespuesta.Checked

    End Sub


#End Region

#Region "SUBPROCESOS"

#End Region
End Class