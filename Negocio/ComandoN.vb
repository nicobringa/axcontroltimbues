﻿Imports AccesoDatos
Public Class ComandoN
    Public Function GetAll(ByVal control As Int32) As List(Of Entidades.COMANDO)
        Dim oBD As New AccesoDatos.ComandoAD
        Return oBD.getAll(control)
    End Function

    Public Function ActualizarComando(ByVal control As Int32) As Boolean
        Dim oBD As New AccesoDatos.ComandoAD
        Return oBD.actualizarComando(control)
    End Function

    Public Function AgregarComando(oComando As Entidades.COMANDO) As Boolean
        Dim oBD As New AccesoDatos.ComandoAD
        Return oBD.agregarComando(oComando)
    End Function
End Class
