﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlConfRFID
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.cboReportMode = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboSession = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboSearchMode = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboReaderMode = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtPowerAnt2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbxAntena1 = New System.Windows.Forms.CheckBox()
        Me.txtObserAnt4 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtObserAnt3 = New System.Windows.Forms.TextBox()
        Me.txtPowerAnt1 = New System.Windows.Forms.TextBox()
        Me.txtObserAnt2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtObserAnt1 = New System.Windows.Forms.TextBox()
        Me.txtSensitivityAnt1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxAntena2 = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtSensitivityAnt4 = New System.Windows.Forms.TextBox()
        Me.txtSensitivityAnt2 = New System.Windows.Forms.TextBox()
        Me.txtPowerAnt4 = New System.Windows.Forms.TextBox()
        Me.cbxAntena3 = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbxAntena4 = New System.Windows.Forms.CheckBox()
        Me.txtPowerAnt3 = New System.Windows.Forms.TextBox()
        Me.txtSensitivityAnt3 = New System.Windows.Forms.TextBox()
        Me.gbxGPIO = New System.Windows.Forms.GroupBox()
        Me.cbxGpioPuerto4 = New System.Windows.Forms.CheckBox()
        Me.cbxGpioPuerto3 = New System.Windows.Forms.CheckBox()
        Me.cbxGpioPuerto2 = New System.Windows.Forms.CheckBox()
        Me.cbxGpioPuerto1 = New System.Windows.Forms.CheckBox()
        Me.cbxGpioHabilitado = New System.Windows.Forms.CheckBox()
        Me.txtRssi = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.gbxGPIO.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Nombre"
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(53, 5)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(335, 22)
        Me.txtNombre.TabIndex = 25
        '
        'cboReportMode
        '
        Me.cboReportMode.FormattingEnabled = True
        Me.cboReportMode.Items.AddRange(New Object() {"WaitForQuery", "Individual", "BatchAfterStop"})
        Me.cboReportMode.Location = New System.Drawing.Point(283, 82)
        Me.cboReportMode.Name = "cboReportMode"
        Me.cboReportMode.Size = New System.Drawing.Size(221, 21)
        Me.cboReportMode.TabIndex = 44
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(193, 84)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 13)
        Me.Label12.TabIndex = 43
        Me.Label12.Text = "Report Mode"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 79)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(44, 13)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Session"
        '
        'cboSession
        '
        Me.cboSession.FormattingEnabled = True
        Me.cboSession.Items.AddRange(New Object() {"Session 0", "Session 1", "Session 2", "Session 3"})
        Me.cboSession.Location = New System.Drawing.Point(66, 76)
        Me.cboSession.Name = "cboSession"
        Me.cboSession.Size = New System.Drawing.Size(121, 21)
        Me.cboSession.TabIndex = 41
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(305, 47)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 13)
        Me.Label10.TabIndex = 40
        Me.Label10.Text = "Search Mode"
        '
        'cboSearchMode
        '
        Me.cboSearchMode.FormattingEnabled = True
        Me.cboSearchMode.Items.AddRange(New Object() {"Dual Tarjet", "Single Target Inventory", "Single Target w/Suppression"})
        Me.cboSearchMode.Location = New System.Drawing.Point(382, 44)
        Me.cboSearchMode.Name = "cboSearchMode"
        Me.cboSearchMode.Size = New System.Drawing.Size(218, 21)
        Me.cboSearchMode.TabIndex = 39
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 13)
        Me.Label8.TabIndex = 38
        Me.Label8.Text = "Reader Mode"
        '
        'cboReaderMode
        '
        Me.cboReaderMode.FormattingEnabled = True
        Me.cboReaderMode.Items.AddRange(New Object() {"Auto Set Dense Reader", "Auto Set Single Reader", "Max Throughput", "Hybrid Mode", "Dense Reader (M=4)", "Dense Reader (M=8)", "MaxMiller"})
        Me.cboReaderMode.Location = New System.Drawing.Point(81, 41)
        Me.cboReaderMode.Name = "cboReaderMode"
        Me.cboReaderMode.Size = New System.Drawing.Size(218, 21)
        Me.cboReaderMode.TabIndex = 37
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtPowerAnt2)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.cbxAntena1)
        Me.GroupBox1.Controls.Add(Me.txtObserAnt4)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtObserAnt3)
        Me.GroupBox1.Controls.Add(Me.txtPowerAnt1)
        Me.GroupBox1.Controls.Add(Me.txtObserAnt2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtObserAnt1)
        Me.GroupBox1.Controls.Add(Me.txtSensitivityAnt1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cbxAntena2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtSensitivityAnt4)
        Me.GroupBox1.Controls.Add(Me.txtSensitivityAnt2)
        Me.GroupBox1.Controls.Add(Me.txtPowerAnt4)
        Me.GroupBox1.Controls.Add(Me.cbxAntena3)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cbxAntena4)
        Me.GroupBox1.Controls.Add(Me.txtPowerAnt3)
        Me.GroupBox1.Controls.Add(Me.txtSensitivityAnt3)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 117)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(759, 112)
        Me.GroupBox1.TabIndex = 45
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Configuración  de antenas"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(625, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 13)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "Observación"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(465, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 13)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "TxPower"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(532, 19)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(70, 13)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "Rx Sensitivity"
        '
        'txtPowerAnt2
        '
        Me.txtPowerAnt2.Enabled = False
        Me.txtPowerAnt2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPowerAnt2.Location = New System.Drawing.Point(76, 66)
        Me.txtPowerAnt2.Name = "txtPowerAnt2"
        Me.txtPowerAnt2.Size = New System.Drawing.Size(68, 22)
        Me.txtPowerAnt2.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(243, 21)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 13)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Observación"
        '
        'cbxAntena1
        '
        Me.cbxAntena1.AutoSize = True
        Me.cbxAntena1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAntena1.Location = New System.Drawing.Point(55, 42)
        Me.cbxAntena1.Name = "cbxAntena1"
        Me.cbxAntena1.Size = New System.Drawing.Size(15, 14)
        Me.cbxAntena1.TabIndex = 0
        Me.cbxAntena1.UseVisualStyleBackColor = True
        '
        'txtObserAnt4
        '
        Me.txtObserAnt4.Enabled = False
        Me.txtObserAnt4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObserAnt4.Location = New System.Drawing.Point(603, 66)
        Me.txtObserAnt4.Name = "txtObserAnt4"
        Me.txtObserAnt4.Size = New System.Drawing.Size(138, 22)
        Me.txtObserAnt4.TabIndex = 26
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Ant1"
        '
        'txtObserAnt3
        '
        Me.txtObserAnt3.Enabled = False
        Me.txtObserAnt3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObserAnt3.Location = New System.Drawing.Point(603, 38)
        Me.txtObserAnt3.Name = "txtObserAnt3"
        Me.txtObserAnt3.Size = New System.Drawing.Size(138, 22)
        Me.txtObserAnt3.TabIndex = 25
        '
        'txtPowerAnt1
        '
        Me.txtPowerAnt1.Enabled = False
        Me.txtPowerAnt1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPowerAnt1.Location = New System.Drawing.Point(76, 38)
        Me.txtPowerAnt1.Name = "txtPowerAnt1"
        Me.txtPowerAnt1.Size = New System.Drawing.Size(68, 22)
        Me.txtPowerAnt1.TabIndex = 4
        '
        'txtObserAnt2
        '
        Me.txtObserAnt2.Enabled = False
        Me.txtObserAnt2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObserAnt2.Location = New System.Drawing.Point(222, 66)
        Me.txtObserAnt2.Name = "txtObserAnt2"
        Me.txtObserAnt2.Size = New System.Drawing.Size(138, 22)
        Me.txtObserAnt2.TabIndex = 24
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(83, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "TxPower"
        '
        'txtObserAnt1
        '
        Me.txtObserAnt1.Enabled = False
        Me.txtObserAnt1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObserAnt1.Location = New System.Drawing.Point(222, 38)
        Me.txtObserAnt1.Name = "txtObserAnt1"
        Me.txtObserAnt1.Size = New System.Drawing.Size(138, 22)
        Me.txtObserAnt1.TabIndex = 23
        '
        'txtSensitivityAnt1
        '
        Me.txtSensitivityAnt1.Enabled = False
        Me.txtSensitivityAnt1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSensitivityAnt1.Location = New System.Drawing.Point(150, 38)
        Me.txtSensitivityAnt1.Name = "txtSensitivityAnt1"
        Me.txtSensitivityAnt1.Size = New System.Drawing.Size(66, 22)
        Me.txtSensitivityAnt1.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(150, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Rx Sensitivity"
        '
        'cbxAntena2
        '
        Me.cbxAntena2.AutoSize = True
        Me.cbxAntena2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAntena2.Location = New System.Drawing.Point(55, 70)
        Me.cbxAntena2.Name = "cbxAntena2"
        Me.cbxAntena2.Size = New System.Drawing.Size(15, 14)
        Me.cbxAntena2.TabIndex = 8
        Me.cbxAntena2.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Ant2"
        '
        'txtSensitivityAnt4
        '
        Me.txtSensitivityAnt4.Enabled = False
        Me.txtSensitivityAnt4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSensitivityAnt4.Location = New System.Drawing.Point(531, 66)
        Me.txtSensitivityAnt4.Name = "txtSensitivityAnt4"
        Me.txtSensitivityAnt4.Size = New System.Drawing.Size(66, 22)
        Me.txtSensitivityAnt4.TabIndex = 19
        '
        'txtSensitivityAnt2
        '
        Me.txtSensitivityAnt2.Enabled = False
        Me.txtSensitivityAnt2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSensitivityAnt2.Location = New System.Drawing.Point(150, 66)
        Me.txtSensitivityAnt2.Name = "txtSensitivityAnt2"
        Me.txtSensitivityAnt2.Size = New System.Drawing.Size(66, 22)
        Me.txtSensitivityAnt2.TabIndex = 11
        '
        'txtPowerAnt4
        '
        Me.txtPowerAnt4.Enabled = False
        Me.txtPowerAnt4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPowerAnt4.Location = New System.Drawing.Point(457, 66)
        Me.txtPowerAnt4.Name = "txtPowerAnt4"
        Me.txtPowerAnt4.Size = New System.Drawing.Size(68, 22)
        Me.txtPowerAnt4.TabIndex = 18
        '
        'cbxAntena3
        '
        Me.cbxAntena3.AutoSize = True
        Me.cbxAntena3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAntena3.Location = New System.Drawing.Point(436, 42)
        Me.cbxAntena3.Name = "cbxAntena3"
        Me.cbxAntena3.Size = New System.Drawing.Size(15, 14)
        Me.cbxAntena3.TabIndex = 12
        Me.cbxAntena3.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(396, 71)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 13)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Ant4"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(396, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Ant3"
        '
        'cbxAntena4
        '
        Me.cbxAntena4.AutoSize = True
        Me.cbxAntena4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAntena4.Location = New System.Drawing.Point(436, 70)
        Me.cbxAntena4.Name = "cbxAntena4"
        Me.cbxAntena4.Size = New System.Drawing.Size(15, 14)
        Me.cbxAntena4.TabIndex = 16
        Me.cbxAntena4.UseVisualStyleBackColor = True
        '
        'txtPowerAnt3
        '
        Me.txtPowerAnt3.Enabled = False
        Me.txtPowerAnt3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPowerAnt3.Location = New System.Drawing.Point(457, 38)
        Me.txtPowerAnt3.Name = "txtPowerAnt3"
        Me.txtPowerAnt3.Size = New System.Drawing.Size(68, 22)
        Me.txtPowerAnt3.TabIndex = 14
        '
        'txtSensitivityAnt3
        '
        Me.txtSensitivityAnt3.Enabled = False
        Me.txtSensitivityAnt3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSensitivityAnt3.Location = New System.Drawing.Point(531, 38)
        Me.txtSensitivityAnt3.Name = "txtSensitivityAnt3"
        Me.txtSensitivityAnt3.Size = New System.Drawing.Size(66, 22)
        Me.txtSensitivityAnt3.TabIndex = 15
        '
        'gbxGPIO
        '
        Me.gbxGPIO.Controls.Add(Me.cbxGpioPuerto4)
        Me.gbxGPIO.Controls.Add(Me.cbxGpioPuerto3)
        Me.gbxGPIO.Controls.Add(Me.cbxGpioPuerto2)
        Me.gbxGPIO.Controls.Add(Me.cbxGpioPuerto1)
        Me.gbxGPIO.Location = New System.Drawing.Point(27, 235)
        Me.gbxGPIO.Name = "gbxGPIO"
        Me.gbxGPIO.Size = New System.Drawing.Size(310, 57)
        Me.gbxGPIO.TabIndex = 47
        Me.gbxGPIO.TabStop = False
        Me.gbxGPIO.Text = "GPIO"
        '
        'cbxGpioPuerto4
        '
        Me.cbxGpioPuerto4.AutoSize = True
        Me.cbxGpioPuerto4.Location = New System.Drawing.Point(235, 20)
        Me.cbxGpioPuerto4.Name = "cbxGpioPuerto4"
        Me.cbxGpioPuerto4.Size = New System.Drawing.Size(66, 17)
        Me.cbxGpioPuerto4.TabIndex = 50
        Me.cbxGpioPuerto4.Text = "Puerto 4"
        Me.cbxGpioPuerto4.UseVisualStyleBackColor = True
        '
        'cbxGpioPuerto3
        '
        Me.cbxGpioPuerto3.AutoSize = True
        Me.cbxGpioPuerto3.Location = New System.Drawing.Point(163, 20)
        Me.cbxGpioPuerto3.Name = "cbxGpioPuerto3"
        Me.cbxGpioPuerto3.Size = New System.Drawing.Size(66, 17)
        Me.cbxGpioPuerto3.TabIndex = 49
        Me.cbxGpioPuerto3.Text = "Puerto 3"
        Me.cbxGpioPuerto3.UseVisualStyleBackColor = True
        '
        'cbxGpioPuerto2
        '
        Me.cbxGpioPuerto2.AutoSize = True
        Me.cbxGpioPuerto2.Location = New System.Drawing.Point(87, 20)
        Me.cbxGpioPuerto2.Name = "cbxGpioPuerto2"
        Me.cbxGpioPuerto2.Size = New System.Drawing.Size(66, 17)
        Me.cbxGpioPuerto2.TabIndex = 48
        Me.cbxGpioPuerto2.Text = "Puerto 2"
        Me.cbxGpioPuerto2.UseVisualStyleBackColor = True
        '
        'cbxGpioPuerto1
        '
        Me.cbxGpioPuerto1.AutoSize = True
        Me.cbxGpioPuerto1.Location = New System.Drawing.Point(18, 20)
        Me.cbxGpioPuerto1.Name = "cbxGpioPuerto1"
        Me.cbxGpioPuerto1.Size = New System.Drawing.Size(66, 17)
        Me.cbxGpioPuerto1.TabIndex = 47
        Me.cbxGpioPuerto1.Text = "Puerto 1"
        Me.cbxGpioPuerto1.UseVisualStyleBackColor = True
        '
        'cbxGpioHabilitado
        '
        Me.cbxGpioHabilitado.AutoSize = True
        Me.cbxGpioHabilitado.Location = New System.Drawing.Point(8, 243)
        Me.cbxGpioHabilitado.Name = "cbxGpioHabilitado"
        Me.cbxGpioHabilitado.Size = New System.Drawing.Size(15, 14)
        Me.cbxGpioHabilitado.TabIndex = 48
        Me.cbxGpioHabilitado.UseVisualStyleBackColor = True
        '
        'txtRssi
        '
        Me.txtRssi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRssi.Location = New System.Drawing.Point(541, 82)
        Me.txtRssi.Name = "txtRssi"
        Me.txtRssi.Size = New System.Drawing.Size(68, 22)
        Me.txtRssi.TabIndex = 49
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(510, 86)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(27, 13)
        Me.Label16.TabIndex = 50
        Me.Label16.Text = "Rssi"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(611, 86)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(161, 13)
        Me.Label17.TabIndex = 51
        Me.Label17.Text = "(El valor 0 deshabilita el filtro rssi)"
        '
        'CtrlConfRFID
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtRssi)
        Me.Controls.Add(Me.cbxGpioHabilitado)
        Me.Controls.Add(Me.gbxGPIO)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboReportMode)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cboSession)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cboSearchMode)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboReaderMode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtNombre)
        Me.Name = "CtrlConfRFID"
        Me.Size = New System.Drawing.Size(772, 311)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxGPIO.ResumeLayout(False)
        Me.gbxGPIO.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents cboReportMode As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboSession As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboSearchMode As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboReaderMode As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPowerAnt2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbxAntena1 As System.Windows.Forms.CheckBox
    Friend WithEvents txtObserAnt4 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtObserAnt3 As System.Windows.Forms.TextBox
    Friend WithEvents txtPowerAnt1 As System.Windows.Forms.TextBox
    Friend WithEvents txtObserAnt2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtObserAnt1 As System.Windows.Forms.TextBox
    Friend WithEvents txtSensitivityAnt1 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbxAntena2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSensitivityAnt4 As System.Windows.Forms.TextBox
    Friend WithEvents txtSensitivityAnt2 As System.Windows.Forms.TextBox
    Friend WithEvents txtPowerAnt4 As System.Windows.Forms.TextBox
    Friend WithEvents cbxAntena3 As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbxAntena4 As System.Windows.Forms.CheckBox
    Friend WithEvents txtPowerAnt3 As System.Windows.Forms.TextBox
    Friend WithEvents txtSensitivityAnt3 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents gbxGPIO As GroupBox
    Friend WithEvents cbxGpioPuerto4 As CheckBox
    Friend WithEvents cbxGpioPuerto3 As CheckBox
    Friend WithEvents cbxGpioPuerto2 As CheckBox
    Friend WithEvents cbxGpioPuerto1 As CheckBox
    Friend WithEvents cbxGpioHabilitado As CheckBox
    Friend WithEvents txtRssi As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
End Class
