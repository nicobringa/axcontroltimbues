﻿'BARRERA ARRIBA 263; 70
'BARRERA ABAJO 0; 322
'SENSOR VERDE 68; 351
'SENSOR ROJO 67; 352


Imports System.Threading
Imports Negocio
Imports Entidades.Constante


Public Class CtrlDinamismoPorteriaEgreso



    Public Sub SetBarreraEstado(ByVal _BarreraEstado As BarreraEstado)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing




        If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then

            picVisible = picBarreraArriba_Egreso
            picNoVisible = picBarreraAbajo_Egreso
            modDelegado.setVisibleCtrl(Me, lblFallaBEgreso, False)

        ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
            picVisible = picBarreraAbajo_Egreso
            picNoVisible = picBarreraArriba_Egreso
            modDelegado.setVisibleCtrl(Me, lblFallaBEgreso, False)
        ElseIf _BarreraEstado = BarreraEstado.Falla Then
            picNoVisible = Nothing
            picVisible = Nothing
            modDelegado.setVisibleCtrl(Me, lblFallaBEgreso, True)
        End If



        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If

    End Sub

    Public Sub SetSensorEstado(ByVal _SensorEstado As Integer)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing





        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picSensor_EgresoNoCortando
            picNoVisible = picSensor_EgresoCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picSensor_EgresoCortando
            picNoVisible = picSensor_EgresoNoCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = Nothing
            picNoVisible = Nothing

        End If



        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)

        Else
            modDelegado.setVisiblePic(Me, False, picSensor_EgresoNoCortando)
            modDelegado.setVisiblePic(Me, False, picSensor_EgresoCortando)

        End If


    End Sub

End Class
