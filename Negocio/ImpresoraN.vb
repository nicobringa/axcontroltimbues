﻿Imports Entidades
Imports AccesoDatos
Public Class ImpresoraN
    Dim oImpresoraAD As New ImpresoraAD
    Public Function GetOne(ByVal ip As String) As Impresora

        Return oImpresoraAD.getOne(ip)
    End Function

    Public Function GetOneTag(ByVal tag As String) As Impresora

        Return oImpresoraAD.getOneTag(tag)
    End Function

    Public Function Update_Config(ByVal ocartel As Impresora) As Impresora

        Return oImpresoraAD.update_config(ocartel)
    End Function

End Class
