﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
namespace AccesoDatos
{
    public class ComandoAD
    {
        public List<Entidades.COMANDO> getAll(int control)
        {
            try
            {
              AxControlORAEntities  context = new AxControlORAEntities();
                return context.COMANDO.Where(c=>c.ID_CONTROL== control && c.PROCESADO==0).ToList();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;
            }


        }

        public bool actualizarComando(int idComando)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.COMANDO.Where(a => a.ID_COMANDO == idComando).SingleOrDefault();
                oConsulta.PROCESADO = 1;
                oContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool agregarComando(Entidades.COMANDO oComando)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {
                    oContext.COMANDO.Add(oComando);
                    oContext.SaveChanges();
                    return true;
                }                
            }
            catch (Exception e)
            {
                return false;
                throw;
            }
        }
    }
}
