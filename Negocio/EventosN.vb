﻿Public Class EventosN

    Public Function EscribirEventLog(DescripcionEvento As String, TipoEvento As EventLogEntryType, ControlAcceso As String) As Boolean
        Try
            Dim oEventLog As New EventLog()
            Dim nombreLog As String = "ControlAcceso"
            If Not EventLog.SourceExists(ControlAcceso) Then
                EventLog.CreateEventSource(ControlAcceso, nombreLog)
            End If
            oEventLog.Source = ControlAcceso
            oEventLog.WriteEntry(DescripcionEvento, TipoEvento)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class
