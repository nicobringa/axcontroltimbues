﻿Public Class Puerto_RfidN
    Private oBD As AccesoDatos.Puerto_RfidAD

    Public Sub New()
        oBD = New AccesoDatos.Puerto_RfidAD()
    End Sub

    Public Function GetOne(numPuerto As Integer, idConf As Integer) As Entidades.Puerto_RFID
        Return oBD.getOne(numPuerto, idConf)

    End Function

    Public Function GetOneSector(idConf As Integer, tag As String) As Entidades.Puerto_RFID
        Return oBD.getOne(idConf, tag)


    End Function

    Public Function Add(Puerto As Entidades.Puerto_RFID) As Entidades.Puerto_RFID
        Return oBD.add(Puerto)

    End Function

    Public Function Update(Puerto As Entidades.Puerto_RFID) As Entidades.Puerto_RFID
        Return oBD.update(Puerto)

    End Function



End Class
