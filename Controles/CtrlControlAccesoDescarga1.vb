﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Impinj.OctaneSdk
Public Class CtrlControlAccesoDescarga1

#Region "PROPIEDADES"
    Private _idControlAcceso As Integer = 0
    Public Property ID_CONTROL_ACCESO() As Integer
        Get
            Return _idControlAcceso
        End Get
        Set(ByVal value As Integer)
            _idControlAcceso = value
        End Set
    End Property

    Private _RFID As Boolean
    Public Property RFID() As Boolean
        Get
            Return _RFID
        End Get
        Set(ByVal value As Boolean)
            _RFID = value
        End Set
    End Property

    Private ControlAcceso As Entidades.CONTROL_ACCESO
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private hMonitoreo As Thread
    Private WithEvents BufferBar1Des1 As Entidades.BufferTag
    Private WithEvents BufferBar1Des2 As Entidades.BufferTag
    Private WithEvents BufferBar1Des3 As Entidades.BufferTag
    Private oLector As New Entidades.LECTOR_RFID
    Private nLector As New Negocio.LectorRFID_N
    'Private nAlarma As New Negocio.AlarmaN
#End Region

#Region "Constructor"
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()



    End Sub

    Public Sub New(ID_CONTROL_ACCESO As Integer, ID_PLC As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_PLC = ID_PLC

    End Sub
#End Region

#Region "EVENTOS FORM"
    Private Sub btnLectorRfid_Click(sender As Object, e As EventArgs) Handles btnLectorRfid.Click

        Dim frm As New FrmConfLectoresRFID(Me.ListRFID_SpeedWay)
        frm.ShowDialog()


    End Sub

#End Region

#Region "METODOS"
    Public Overrides Sub Inicializar()

        If Me.ID_CONTROL_ACCESO = 0 Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID de puesto de trabajo")

            Return
        End If

        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)

        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If

        Dim nombreControlAcceso As String = String.Format("{0} - ({1})", Me.ControlAcceso.NOMBRE, Me.ControlAcceso.ID_CONTROL_ACCESO)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreControlAcceso)
        'Si el puesto de trabajo es servidor
        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            'Inicializo como servidor
            IniServidor()
        Else
            'De los contrario como cliente
            IniCliente()
        End If

        'Ejecuto el hilo de monitoreo
        If Not IsNothing(hMonitoreo) Then
            If hMonitoreo.IsAlive Then hMonitoreo.Abort()
            hMonitoreo = Nothing
        End If

        inicializarCtrl()
        SetBuffer()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()


    End Sub

    Public Sub InicializarWS()
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)
        inicializarCtrl()
    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR
            'De cada sector traigo los TAG del plc que tiene el control de acceso
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(itemSector.ID_SECTOR)

            For Each itemDato As Entidades.DATO_WORDINT In listDatoWordInt

                Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

                'Obtengo el nombre del tag quitandole la propiedad
                itemDato.TAG = Constante.getNombreTag(itemDato.TAG)
                'Si el tag contiene el nombre del control
                If itemDato.TAG.Equals(DES1_BAR1.Name) Then
                    ctrlAutomatizacion = Me.DES1_BAR1

                ElseIf itemDato.TAG.Equals(DES1_BAR2.Name) Then
                    ctrlAutomatizacion = Me.DES1_BAR2

                ElseIf itemDato.TAG.Equals(S_DES1_BAR1.Name) Then
                    ctrlAutomatizacion = Me.S_DES1_BAR1

                ElseIf itemDato.TAG.Equals(S_DES1_BAR2.Name) Then
                    ctrlAutomatizacion = Me.S_DES1_BAR2

                ElseIf itemDato.TAG.Equals(S_DES1_POS.Name) Then
                    ctrlAutomatizacion = Me.S_DES1_POS

                ElseIf itemDato.TAG.Equals(S_LECTURA_DES1.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_DES1

                ElseIf itemDato.TAG.Equals(S_DES1_PLAT.Name) Then
                    ctrlAutomatizacion = Me.S_DES1_PLAT

                ElseIf itemDato.TAG.Equals(DES2_BAR1.Name) Then
                    ctrlAutomatizacion = Me.DES2_BAR1

                ElseIf itemDato.TAG.Equals(DES2_BAR2.Name) Then
                    ctrlAutomatizacion = Me.DES2_BAR2

                ElseIf itemDato.TAG.Equals(S_DES2_BAR1.Name) Then
                    ctrlAutomatizacion = Me.S_DES2_BAR1

                ElseIf itemDato.TAG.Equals(S_DES2_BAR2.Name) Then
                    ctrlAutomatizacion = Me.S_DES2_BAR2

                ElseIf itemDato.TAG.Equals(S_DES2_POS.Name) Then
                    ctrlAutomatizacion = Me.S_DES2_POS

                ElseIf itemDato.TAG.Equals(S_LECTURA_DES2.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_DES2

                ElseIf itemDato.TAG.Equals(S_DES2_PLAT.Name) Then
                    ctrlAutomatizacion = Me.S_DES2_PLAT

                ElseIf itemDato.TAG.Equals(DES3_BAR1.Name) Then
                    ctrlAutomatizacion = Me.DES3_BAR1

                ElseIf itemDato.TAG.Equals(DES3_BAR2.Name) Then
                    ctrlAutomatizacion = Me.DES3_BAR2

                ElseIf itemDato.TAG.Equals(S_DES3_BAR1.Name) Then
                    ctrlAutomatizacion = Me.S_DES3_BAR1

                ElseIf itemDato.TAG.Equals(S_DES3_BAR2.Name) Then
                    ctrlAutomatizacion = Me.S_DES3_BAR2

                ElseIf itemDato.TAG.Equals(S_DES3_POS.Name) Then
                    ctrlAutomatizacion = Me.S_DES3_POS

                ElseIf itemDato.TAG.Equals(S_LECTURA_DES3.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_DES3

                ElseIf itemDato.TAG.Equals(S_DES3_PLAT.Name) Then
                    ctrlAutomatizacion = Me.S_DES3_PLAT

                ElseIf itemDato.TAG.Equals(DESCARGA1.Name) Then
                    ctrlAutomatizacion = Me.DESCARGA1

                ElseIf itemDato.TAG.Equals(DESCARGA2.Name) Then
                    ctrlAutomatizacion = Me.DESCARGA2

                ElseIf itemDato.TAG.Equals(DESCARGA3.Name) Then
                    ctrlAutomatizacion = Me.DESCARGA3

                Else
                    ctrlAutomatizacion = Nothing
                End If

                If Not IsNothing(ctrlAutomatizacion) Then
                    'Si todavia no esta inicializado
                    If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(Me.ControlAcceso.ID_PLC, Me.ControlAcceso.ID_CONTROL_ACCESO, itemSector.ID_SECTOR) ' BARRERA DE INGRESO
                End If


            Next





        Next

        CartelLED_DES1.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CartelLED_DES2.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CartelLED_DES3.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CAM_DES1.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CAM_DES2.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CAM_DES3.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
    End Sub

    Public Sub IniServidor()
        'Cargo los lectores RFID
        CargarLectoresRFID()

    End Sub

    Public Sub IniCliente()
        CargarLectorRFIDCliente()
    End Sub

    Private Sub CargarLectorRFIDCliente()

        Dim oLectores As New List(Of Entidades.LECTOR_RFID)
        oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
        CtrlEstadoLectoresRFID1.CargarLectoresCliente(oLectores)
    End Sub

    Private Sub CargarLectoresRFID()
        'Busco los lectores
        Dim nLectorEFID As New Negocio.LectorRFID_N

        Dim listLectorRFID As List(Of Entidades.LECTOR_RFID) = nLectorEFID.GetAll(Me.ID_CONTROL_ACCESO)
        'listLectorRFID.Clear()

        If IsNothing(listLectorRFID) Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN DE PROCEDIMIENTO
        End If

        If listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN 
        End If

        'Recorro los lectores agregados para el puesto de trabajo
        For Each LectorRFID As Entidades.LECTOR_RFID In listLectorRFID
            'Creo el lector RFID SpeedWay para su utilización
            Dim RFID_SpeedWay As New Negocio.LectorRFID_SpeedWay(LectorRFID)

            If (IsNothing(Me.ListRFID_SpeedWay)) Then Me.ListRFID_SpeedWay = New List(Of Negocio.LectorRFID_SpeedWay)
            'Agrego el LectorRFID a la lista
            Me.ListRFID_SpeedWay.Add(RFID_SpeedWay)
            'Agrego los escuchadores para los evento del lector
            AddHandler RFID_SpeedWay.TagLeido, AddressOf Me.TagLeido
            AddHandler RFID_SpeedWay.ErrorRFID, AddressOf Me.ErrorRFID
            AddHandler RFID_SpeedWay.InfoRFID, AddressOf Me.InfoRFID


        Next

        CtrlEstadoLectoresRFID1.CargarLectores(Me.ListRFID_SpeedWay)
    End Sub

    Private Function getAntenaLeyendo(ID_SECTOR As Integer) As Controles.CtrlAntenaLeyendo
        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = Nothing

        Select Case ID_SECTOR
            Case DES1_BAR1.ID_SECTOR
                ctrlAntLeyendo = CtrlAntenaLeyendoDes1
            Case DES2_BAR1.ID_SECTOR
                ctrlAntLeyendo = CtrlAntenaLeyendoDes2
            Case DES3_BAR1.ID_SECTOR
                ctrlAntLeyendo = CtrlAntenaLeyendoDes3
        End Select



        Return ctrlAntLeyendo
    End Function

    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <param name="ID_SECTOR"></param>
    ''' <returns></returns>
    Private Function getBufferAntena(ID_SECTOR As Integer) As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Nothing

        'Le pregunto el sector a las barreras 
        'Para saber si es de ingreso o de egreso
        If DES1_BAR1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBar1Des1
        ElseIf DES2_BAR1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBar1Des2
        ElseIf DES3_BAR1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBar1Des3
        End If

        Return Buffer
    End Function

    Private Sub EstaHabilitado(ID_SECTOR As Integer, TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            Dim tmp As String '= String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            ' CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)

            Dim CtrlCartel As Controles.CtrlCartelMultiLED = Nothing
            Dim ctrlCabezalBalanza As Controles.CtrlCabezalBalanza = Nothing
            Dim Comando As Entidades.Constante.BalanzaComando
            Dim CtrlInfoBalanza As ctrlInfomacionBalanza = Nothing
            Dim numBalanza As Integer
            'Ejecuto el comando dependiendo de la barrera 
            'Para saber si es de ingreso o de egreso

            Select Case ID_SECTOR
                Case DES1_BAR1.ID_SECTOR
                    ' ctrlCabezalBalanza = BALANZA1
                    CtrlCartel = CartelLED_DES1
                    'CtrlInfoBalanza = ctrlInfomacionBalanza
                    numBalanza = 1
                Case DES2_BAR1.ID_SECTOR
                    ' ctrlCabezalBalanza = BALANZA1
                    CtrlCartel = CartelLED_DES2
                    'CtrlInfoBalanza = ctrlInfomacionBalanza
                    numBalanza = 2
                Case DES3_BAR1.ID_SECTOR
                    ' ctrlCabezalBalanza = BALANZA1
                    CtrlCartel = CartelLED_DES3
                    'CtrlInfoBalanza = ctrlInfomacionBalanza
                    numBalanza = 3
            End Select

            'Si la balanza esta en otro estado que no sea la barrera cerrada significa que la balanza no esta habilitada o esta ocupada
            'If ctrlCabezalBalanza.oEstado.valor <> Constante.BalanzaEstado.BarrerasCerrada Then
            '    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
            '                                                    String.Format("La balanza {0} no se encuentra habilitada o esta ocupada", numBalanza))
            '    Return
            'End If




            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)




            'Pregunto si esta habilitado
            If axRespuesta.habilitado Then
                'Si esta habilitado
                'Muestro el mensaje en el cartel
                tmp = "[ " + DateTime.Now + " ] CAMIÓN HABILITADO: ID TRANSACCIÓN: " + axRespuesta.idTransaccion.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)
                Select Case ID_SECTOR
                    Case DES1_BAR1.ID_SECTOR
                        Comando = BalanzaComando.PermitirIngresoBarrera1
                    Case DES2_BAR1.ID_SECTOR
                        Comando = BalanzaComando.PermitirIngresoBarrera1
                    Case DES3_BAR1.ID_SECTOR
                        Comando = BalanzaComando.PermitirIngresoBarrera1
                End Select

                'Ejecuto el comando permitir ingresar en la barrera
                If Not IsNothing(ctrlCabezalBalanza) Then
                    Dim nConfiguracion As New Negocio.ConfiguracionN
                    'Dim CAMION_PESANDO As New CAMION_PESANDO(TagRFID, axRespuesta, ID_SECTOR, numBalanza)
                    'nConfiguracion.setCAMION_PESANDO(numBalanza, CAMION_PESANDO)

                    'If Not IsNothing(CtrlInfoBalanza) Then CtrlInfoBalanza.CAMION_PESANDO(True, CAMION_PESANDO)
                    ctrlCabezalBalanza.EjectComando(Comando)
                Else
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve,
                                                                 "No hay cabezal de balanza inicializado " & ID_SECTOR)
                End If

            Else
                tmp = "[ " + DateTime.Now + " ] CAMIÓN NO HABILITADO: " + axRespuesta.mensaje.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorUser, tmp)
                'Guardo el ultimo tag leido
                Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)

            End If


            If Not IsNothing(CtrlCartel) Then CtrlCartel.EscribirCartel(axRespuesta.mensaje)
        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End Try

    End Sub

    Private Sub RefrescarDinamismo()
        'BARRERAS
        If Me.DES1_BAR1.Inicializado Then
            Me.DES1_BAR1.SetEstado()
            CtrlDinamismoDescarga1.SetBarreraEstado(1, Me.DES1_BAR1.oEstado.valor)

        End If
        If Me.DES1_BAR2.Inicializado Then
            Me.DES1_BAR2.SetEstado()
            CtrlDinamismoDescarga1.SetBarreraEstado(2, Me.DES1_BAR2.oEstado.valor)
        End If

        If Me.DES2_BAR1.Inicializado Then
            Me.DES2_BAR1.SetEstado()
            CtrlDinamismoDescarga2.SetBarreraEstado(1, Me.DES2_BAR1.oEstado.valor)

        End If
        If Me.DES2_BAR2.Inicializado Then
            Me.DES2_BAR2.SetEstado()
            CtrlDinamismoDescarga2.SetBarreraEstado(2, Me.DES2_BAR2.oEstado.valor)
        End If

        If Me.DES3_BAR1.Inicializado Then
            Me.DES3_BAR1.SetEstado()
            CtrlDinamismoDescarga3.SetBarreraEstado(1, Me.DES3_BAR1.oEstado.valor)

        End If
        If Me.DES3_BAR2.Inicializado Then
            Me.DES3_BAR2.SetEstado()
            CtrlDinamismoDescarga3.SetBarreraEstado(2, Me.DES3_BAR2.oEstado.valor)
        End If

        'SENSORES
        If Me.S_LECTURA_DES1.Inicializado Then
            Me.S_LECTURA_DES1.SetEstadoPLC()
            CtrlDinamismoDescarga1.SetSensorEstado(tipoSensor.Lectura, 1, Me.S_LECTURA_DES1.oEstado.valor)
        End If

        If Me.S_LECTURA_DES2.Inicializado Then
            Me.S_LECTURA_DES2.SetEstadoPLC()
            CtrlDinamismoDescarga2.SetSensorEstado(tipoSensor.Lectura, 1, Me.S_LECTURA_DES2.oEstado.valor)
        End If

        If Me.S_LECTURA_DES3.Inicializado Then
            Me.S_LECTURA_DES3.SetEstadoPLC()
            CtrlDinamismoDescarga3.SetSensorEstado(tipoSensor.Lectura, 1, Me.S_LECTURA_DES3.oEstado.valor)
        End If

        'BALANZA 
        If Me.DESCARGA1.Inicializado Then
            Me.DESCARGA1.Refrescar()
        End If

        If Me.DESCARGA2.Inicializado Then
            Me.DESCARGA2.Refrescar()
        End If

        If Me.DESCARGA3.Inicializado Then
            Me.DESCARGA3.Refrescar()
        End If


        If Me.S_DES1_BAR1.Inicializado Then
            Me.S_DES1_BAR1.SetEstadoPLC()
            CtrlDinamismoDescarga1.SetSensorEstado(tipoSensor.Posicion, 1, Me.S_DES1_BAR1.oEstado.valor)
        End If

        If Me.S_DES1_BAR2.Inicializado Then
            Me.S_DES1_BAR2.SetEstadoPLC()
            CtrlDinamismoDescarga1.SetSensorEstado(tipoSensor.Posicion, 2, Me.S_DES1_BAR2.oEstado.valor)
        End If

        If Me.S_DES2_BAR1.Inicializado Then
            Me.S_DES2_BAR1.SetEstadoPLC()
            CtrlDinamismoDescarga2.SetSensorEstado(tipoSensor.Posicion, 1, Me.S_DES2_BAR1.oEstado.valor)
        End If

        If Me.S_DES2_BAR2.Inicializado Then
            Me.S_DES2_BAR2.SetEstadoPLC()
            CtrlDinamismoDescarga2.SetSensorEstado(tipoSensor.Posicion, 2, Me.S_DES2_BAR2.oEstado.valor)
        End If

        If Me.S_DES3_BAR1.Inicializado Then
            Me.S_DES3_BAR1.SetEstadoPLC()
            CtrlDinamismoDescarga3.SetSensorEstado(tipoSensor.Posicion, 1, Me.S_DES3_BAR1.oEstado.valor)
        End If

        If Me.S_DES3_BAR2.Inicializado Then
            Me.S_DES3_BAR2.SetEstadoPLC()
            CtrlDinamismoDescarga3.SetSensorEstado(tipoSensor.Posicion, 2, Me.S_DES3_BAR2.oEstado.valor)
        End If


        'Plataforma

        If Me.S_DES1_PLAT.Inicializado() Then
            Me.S_DES1_PLAT.SetEstadoPLC()
            CtrlDinamismoDescarga1.setPlataformaDescarga(tipoSensor.Plataforma, 1, Me.S_DES1_PLAT.oEstado.valor)
        End If

        If Me.S_DES2_PLAT.Inicializado() Then
            Me.S_DES2_PLAT.SetEstadoPLC()
            CtrlDinamismoDescarga2.setPlataformaDescarga(tipoSensor.Plataforma, 2, Me.S_DES2_PLAT.oEstado.valor)
        End If
        If Me.S_DES3_PLAT.Inicializado() Then
            Me.S_DES3_PLAT.SetEstadoPLC()
            CtrlDinamismoDescarga3.setPlataformaDescarga(tipoSensor.Plataforma, 3, Me.S_DES3_PLAT.oEstado.valor)
        End If

        'BALANZAS
        If Me.DESCARGA1.Inicializado Then
            Me.DESCARGA1.Refrescar()
        End If
        If Me.DESCARGA2.Inicializado Then
            Me.DESCARGA2.Refrescar()
        End If
        If Me.DESCARGA3.Inicializado Then
            Me.DESCARGA3.Refrescar()
        End If

        ' Dim nAlarma As New Negocio.AlarmaN
        '   modDelegado.setVisibleCtrl(Me, btnAlarma, nAlarma.mostrarAlarma(Me.ID_CONTROL_ACCESO))




    End Sub

    Private Sub ConectarLectoresRFID()

        'Recorro los lectores RFID Configurado
        For Each RFID_SpeedWay As LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            'Si el lector no esta conectado
            If Not RFID_SpeedWay.IsConnected Then

                Try

                    oLector = nLector.GetOne(RFID_SpeedWay.Ip)
                    oLector.CONECTADO = RFID_SpeedWay.Conectarse()
                    nLector.Update(oLector)

                Catch ex As Exception
                    'Surgio un error en la conexion
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, ex.Message)
                End Try
            Else
                'El lector ya se encuentra conectado
                Dim tmp As String = "[ " + DateTime.Now + " ] INFO: " + String.Format("El lector {0} se encuentra conectado", RFID_SpeedWay.Nombre)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Le consulto al PLC si tengo que comenzar la lectura RFID
    ''' </summary>
    Private Sub VerEstadoLecturaRFID()

        Dim nDatoBool As New Negocio.DatoBoolN
        Dim nombreTag As String = String.Format("{0}.{1}", TIPO_CONTROL_ACCESO.DESCARGA, PROPIEDADES_TAG_PLC.LECTURA_RFID)
        Dim oDatoLecturaRFID As DATO_BOOL = nDatoBool.GetOne(Me.ControlAcceso.ID_PLC, nombreTag)

        If Not IsNothing(oDatoLecturaRFID) Then
            'Si no tiene lector RFID cargado termino el proceso
            If IsNothing(Me.ListRFID_SpeedWay) Then Return
            'Recorro todos los lectores que tiene el control de acceso 
            For Each itemLector As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
                If itemLector.IsConnected Then 'Si esta conectado
                    'Pregunto si tendria que estar leyendo
                    If oDatoLecturaRFID.valor = True Then
                        If Not itemLector.isRunning Then 'Si no esta leyendo 
                            itemLector.StartRead() 'Comienza a leer
                        End If

                    Else ' No tendria que estar leyendo
                        If itemLector.isRunning Then 'Si  esta leyendo 
                            itemLector.StopRead() ' Paro la lectura
                        End If

                    End If
                End If
            Next
        Else
            Dim msj As String = String.Format("No se encontro el tipo de dato {0}", nombreTag)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
        End If

    End Sub

    Private Sub SetBuffer()

        'Inicializo los buffer
        Me.BufferBar1Des1 = New Entidades.BufferTag()
        Me.BufferBar1Des2 = New Entidades.BufferTag()
        Me.BufferBar1Des3 = New Entidades.BufferTag()

        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.BufferBar1Des1.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferBar1Des2.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferBar1Des3.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER



    End Sub
    Private hResetBal1 As Thread
    Private hResetBal2 As Thread
    Dim UltEstadoBalanza2 As ConstanteBalanza.EstadoBalanza = ConstanteBalanza.EstadoBalanza.BajaBarrera2Salida
    Private hEnviarPesoDes1 As Thread
    Private hEnviarPesoDes2 As Thread
    Private hEnviarPesoDes3 As Thread
    Private _CAMION_PESANDODes1 As Entidades.CAMION_PESANDO
    Private _CAMION_PESANDODes2 As Entidades.CAMION_PESANDO
    Private _CAMION_PESANDODes3 As Entidades.CAMION_PESANDO
    Private WithEvents bfBalanza1 As Entidades.BufferTag
    Private WithEvents bfBalanza2 As Entidades.BufferTag
    Dim UltEstadoBalanza1 As ConstanteBalanza.EstadoBalanza = ConstanteBalanza.EstadoBalanza.BajaBarrera1Salida
    Private _TiempoEsperaBal1 As Integer
    Private _TiempoEsperaBal2 As Integer
    Private Sub VerEstadoDescarga(ByVal nroDescarga As Integer)
        Dim SUB_TAG As String = "VerEstadoDescarga"
        Try
            'SELECCIONO LA BALANZA
            'Dim _Balanza As Entidades.ConstanteBalanza.Balanzas
            Dim controlBalanza As New CtrlCabezalBalanza

            Dim nBalanza As New Negocio.BalanzaN
            Dim ctrlCartelLED As New CtrlCartelMultiLED

            Dim ctrlInformacionBalanza As ctrlInfomacionBalanza
            Dim ctrlTiempoEspera As CtrlTiempoEspera

            Dim CAMION_PESANDO As CAMION_PESANDO
            Dim hEnviarPeso As Thread

            If nroDescarga = 1 Then
                controlBalanza = DESCARGA1
                ctrlCartelLED = CartelLED_DES1
                ctrlInformacionBalanza = CtrlInfomacionDescarga1
                hEnviarPeso = hEnviarPesoDes1
                CAMION_PESANDO = _CAMION_PESANDODes1
                ctrlTiempoEspera = CtrlTiempoEsperaDesc1
            ElseIf nroDescarga = 2 Then
                controlBalanza = DESCARGA2
                ctrlCartelLED = CartelLED_DES2
                ctrlInformacionBalanza = CtrlInfomacionDescarga2
                hEnviarPeso = hEnviarPesoDes2
                CAMION_PESANDO = _CAMION_PESANDODes2
                ctrlTiempoEspera = CtrlTiempoEsperaDesc2
            Else
                controlBalanza = DESCARGA3
                ctrlCartelLED = CartelLED_DES3
                ctrlInformacionBalanza = CtrlInfomacionDescarga3
                hEnviarPeso = hEnviarPesoDes3
                CAMION_PESANDO = _CAMION_PESANDODes3
                ctrlTiempoEspera = CtrlTiempoEsperaDesc3
            End If


            'Bandera para saber si el sistema tiene que escribir en el 
            'cartel por defector siempre escribo en el cartel
            Dim EscribirCartel As Boolean = True
            'Mensaje que debo escribir en el cartel
            Dim MensajeCartel As String = "AcaBio" 'DateTime.Now.ToString("HH:mm")
            Dim MensajePorDefector As String = DateTime.Now.ToString("HH:mm")
            Dim _EstadoBalanza As ConstanteBalanza.BarreraEstado = controlBalanza.oEstado.valor

            Dim nombreEstado As String = ""
            'Obtengo el ultimo estado de la balanza
            Dim UltEstadoBalanza As Constante.BalanzaEstado = Me.UltEstadoBalanza1

            UltEstadoBalanza1 = _EstadoBalanza

            Dim oConfig As New Entidades.CONFIGURACION
            Dim oConfigN As New Negocio.ConfiguracionN
            oConfig = oConfigN.GetOne

            If controlBalanza.oEstado.valor = Constante.BalanzaEstado.BarrerasCerrada Then '<---- BARRERA CERRADA
                nombreEstado = "Barrera Cerrada"
                EscribirCartel = True
                MensajeCartel = "Aca"
                CAMION_PESANDO = Nothing

                'Si quedo el hilo de Enviar peso vivo lo aborto
                If Not IsNothing(hEnviarPeso) And Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
                    If hEnviarPeso.IsAlive Then hEnviarPeso.Abort()
                    hEnviarPeso = Nothing

                End If

                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                ctrlTiempoEspera.Tiempo = oConfig.TIEMPO_ESPERA
                ctrlTiempoEspera.SetVisible = False

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.HabilitadaIngresarBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.HabilitadaIngresarBarrera2 Then '<--- HABILITADA INGRESAR
                'SI EL ESTADO DE LA BALANZA ES HABILITADA INGRESAR

                nombreEstado = "Habilitada Ingresar Barrera"
                'ESCRIBO AVANCE EN LOS CARTELES

                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_AVANCE
                'Habilito el camión pesando
                Dim nConfiguracion As New Negocio.ConfiguracionN
                'Obtengo el camion que esta pesando


                ctrlInformacionBalanza.CAMION_PESANDO(True, CAMION_PESANDO)
                Dim tiempoEspera As Integer = _TiempoEsperaBal1

                If (ctrlTiempoEspera.Tiempo > 0) Then
                    ctrlTiempoEspera.SetVisible = True
                    '_TiempoEsperaBal1 += 1
                    ctrlTiempoEspera.Tiempo = ctrlTiempoEspera.Tiempo - 1

                Else ' Igual

                    Dim DescripcionAudit As String = "Reset de Balanza por tiempo agotado :  "
                    Negocio.AuditoriaN.AddAuditoria(DescripcionAudit, "", ID_SECTOR, 0, "Aumax", Constante.acciones.Reset_Balanza)
                    'Aqui realizar el reset de balanza
                    ctrlTiempoEspera.SetVisible = False

                    'Si la computadora es servidor aplica el reset
                    If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then controlBalanza.Reset()

                End If

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.ErrorBalanza1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.ErrorBalanza2 Then '<--- BALANZA EN CERO O NO CONECTADA
                'SI EL ESTADO ES BALANZA CERO
                'Selecciono el cartel que debo escribir,siempre escribo al cartel opuesto 
                'de la barrera por la cual accede el camion
                nombreEstado = "Balanza Cero NoConectada Barrera"
                'ESCRIBO EN EL CARTEL LLAMAR
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_LLAMAR

                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                ctrlTiempoEspera.Tiempo = oConfig.TIEMPO_ESPERA
            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.AvanzarCamionBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.AvanzarCamionBarrera2 Then '<--- AVANZA CAMION
                'SI EL ESTADO ES AVANCE CAMION
                nombreEstado = "Avanzar Camion Barrera"
                'ESCRIBO EN EL CARTEL AVANCE
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_AVANCE
                ' ctrlTiempoEspera.SetVisible = False
                ''CtrlBalanza1.SetTiempoEspera(_Balanza, 0)

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.RetrocedaCamionBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.RetrocedaCamionBarrera2 Then '<--- RETROCEDA CAMION
                'SI EL ESTADO ES RETROCEDA CAMION
                nombreEstado = "RetrocedaCamionBarrera"
                'ESCRIBO EN EL CARTEL RETROCEDA
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_RETROCEDA


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.EstabilizandoPesoBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.EstabilizandoPesoBarrera2 Then '<--- ESTABILIZANDO PESO
                nombreEstado = "Estabilizando Peso Barrera"
                'ESCRIBO EN EL CARTEL LA HORA ACTUAL
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_ESPERE
                ctrlTiempoEspera.SetVisible = False

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.PesoMaximoBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.PesoMaximoBarrera2 Then '<---PESO MAXIMO

                nombreEstado = "Peso Maximo Barrera"
                'ESCRIBO EN EL CARTEL LA HORA ACTUAL
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_PESO_MAXIMO


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.TomarPesoBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.TomarPesoBarrera2 Then ' <--- TOMAR PESO


                Dim nConfiguracion As New Negocio.ConfiguracionN

                ' _CAMION_PESANDOBal1 = CAMION_PESANDO

                'Si ya envio el peso espero hasta que el plc cambie de estado
                nombreEstado = "Tomar Peso Barrera"
                If CAMION_PESANDO.PESO_ENVIADO Then Return

                EscribirCartel = False
                ' si el hilo no esta en ejecucion  es porque se todavia no se envio el peso 
                'si la maquina es el servio envia el peso tomado por WS
                If IsNothing(hEnviarPeso) And Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then '
                    EscribirCartel = True
                    MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_ESPERE

                    hEnviarPeso = New Thread(AddressOf EnviarPeso)
                    hEnviarPeso.IsBackground = True
                    'Envio el hilo y espero la respuesta
                    'hEnviarPesoBal1 = hEnviarPeso
                    hEnviarPeso.Start(1)
                End If


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.PermitirSalirBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.PermitirSalirBarrera2 Then '<-- PERMITIR SALIR
                nombreEstado = "Permitir Salir Barrera"
                EscribirCartel = False


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.BajaBarrera1Salida Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.BajaBarrera2Salida Then '<--- BAJAR BARRERA
                EscribirCartel = False
                CAMION_PESANDO = Nothing
                EscribirCartel = False
                nombreEstado = "Baja Barrera Salida"
                'Informo a los cliente a los clientes que que el camión dejo de pesar
                Dim nConfiguracion As New Negocio.ConfiguracionN


                ' ctrlInfomacionBalanza.CAMION_PESANDO(False, Nothing)


            End If



            If UltEstadoBalanza <> _EstadoBalanza And Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then  'Si cambio de estado y es servidor


                Dim idTrasaccion As Long = 0
                'Si tengo un camion pesando
                If Not IsNothing(CAMION_PESANDO) Then
                    'Coloco el idTrasaccion para la auditoria
                    'If Not IsNothing(CAMION_PESANDO.axRespuesta) Then idTrasaccion = CAMION_PESANDO.axRespuesta.idTransaccion
                End If

                Dim DescripcionAuditoria As String = "Cambio de estado en la balanza : a estado : " & nombreEstado
                Negocio.AuditoriaN.AddAuditoria(DescripcionAuditoria, "", ID_SECTOR, idTrasaccion, "Aumax", Constante.acciones.cambioEstadoBalaza)
                If EscribirCartel Then

                    ctrlCartelLED.EscribirCartel(MensajeCartel)

                End If


            End If

        Catch ex As Exception

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
        End Try

    End Sub

    Private Sub EnviarPeso(ByVal numDescarga As Integer)
        Dim nBalanza As New Negocio.BalanzaN
        Dim SUB_TAG As String = "[EnviarPeso]"
        Try
            Dim CabezalBalanza As Controles.CtrlCabezalBalanza
            Dim ctrlInfo As Controles.ctrlInfomacionBalanza
            Dim Cartel As Controles.CtrlCartelMultiLED
            Dim oCamara As Controles.CtrlCamara
            Dim CAMION_PESANDO As CAMION_PESANDO
            If numDescarga = 1 Then
                CabezalBalanza = DESCARGA1
                ctrlInfo = CtrlInfomacionDescarga1
                Cartel = CartelLED_DES1
                oCamara = Me.CAM_DES1
                CAMION_PESANDO = _CAMION_PESANDODes1
            ElseIf numDescarga = 2 Then
                CabezalBalanza = DESCARGA2
                ctrlInfo = CtrlInfomacionDescarga2
                Cartel = CartelLED_DES2
                oCamara = Me.CAM_DES2
                CAMION_PESANDO = _CAMION_PESANDODes2
            Else
                CabezalBalanza = DESCARGA3
                ctrlInfo = CtrlInfomacionDescarga3
                Cartel = CartelLED_DES3
                oCamara = Me.CAM_DES3
                CAMION_PESANDO = _CAMION_PESANDODes3
            End If

            'Tomar el peso de la balanza
            Dim PesoBalanza As Integer = CabezalBalanza.oPeso.valor

            'CtrlBalanza1.SetEsperaWS(_Balanza, True)

            Dim Habilitado As Boolean = False
            Dim AxRespuesta As Entidades.AxRespuesta = Nothing
            Dim nConf As New Negocio.ConfiguracionN
            Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()
            Dim cant As Integer = 0

            While Habilitado = False

                'ctrlInfomacionBalanza.Espera(True)
                Try
                    PesoBalanza = CabezalBalanza.oPeso.valor
                    'AxRespuesta = Negocio.WebServiceBitN.RegistrarPesada(CAMION_PESANDO.axRespuesta.ID_SECTOR, CAMION_PESANDO.tagRFID, PesoBalanza, CAMION_PESANDO.axRespuesta.idTransaccion)
                Catch ex As Exception
                    CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
                End Try


                If Not IsNothing(AxRespuesta) Then
                    Dim tmp = "[ " + DateTime.Now + " ] PESO ENVIADO: " + String.Format("{0}", AxRespuesta.ToString())
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)

                    CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.Informacion, "[ " + DateTime.Now + " ] PESO OBTENIDO: " + AxRespuesta.ToString())

                    CAMION_PESANDO.PESO_ENVIADO = True
                    'CAMION_PESANDO.axRespuesta = AxRespuesta
                    'ctrlInfomacionBalanza.CAMION_PESANDO(True, CAMION_PESANDO)
                    Habilitado = AxRespuesta.habilitado
                    If AxRespuesta.habilitado Then
                        CabezalBalanza.EjectComando(BalanzaComando.PermitirSalir)
                        Exit While
                    End If
                End If

                Thread.Sleep(oConf.TIEMPO_ENVIAR_PESO * 1000)
                cant += 1
                ' ctrlInfomacionBalanza.setCant(cant)
            End While
            'Si sale del bucle es porque la respuesta del ws es TRUE
            oCamara.SacarFoto(AxRespuesta.idTransaccion, AxRespuesta.idSector)
            ctrlInfo.Espera(False)

            If Not IsNothing(AxRespuesta) Then Cartel.EscribirCartel(AxRespuesta.mensaje)

            Thread.Sleep(2000) ' Duemo 2 segundo asi toma el cambio de estado el plc  
        Catch ex As Exception

            'axLog.AxLog.e(Tag, SUB_TAG & ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
            If numDescarga = 1 Then
                hEnviarPesoDes1 = Nothing
            ElseIf numDescarga = 2 Then
                hEnviarPesoDes2 = Nothing
            Else
                hEnviarPesoDes3 = Nothing
            End If

        End Try



    End Sub

    Private Sub ResetBalanza(ByVal Balanza As ConstanteBalanza.Balanzas)

        Try
            CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.Informacion, "[ " + DateTime.Now + " ] INFO: " + "RESET BALANZA : " & Balanza.ToString)

            'AUDITO EL RESET DE BALANZA 2016-11-16 NFB
            Dim CAMION_PESANDO As CAMION_PESANDO
            If Balanza = 1 Then
                CAMION_PESANDO = _CAMION_PESANDODes1
            ElseIf Balanza = 2 Then
                CAMION_PESANDO = _CAMION_PESANDODes2
            Else
                CAMION_PESANDO = _CAMION_PESANDODes3
            End If
            Dim TagRFID As String = If(IsNothing(CAMION_PESANDO), "", CAMION_PESANDO.TAG_RFID)
            'Dim idTrasaccion As Integer = If(IsNothing(CAMION_PESANDO), 0, CAMION_PESANDO.axRespuesta.idTransaccion)
            'Dim ID_SECTOR As Integer = If(IsNothing(CAMION_PESANDO), 0, CAMION_PESANDO.axRespuesta.ID_SECTOR)
            Negocio.AuditoriaN.AddAuditoria("Reset Balanza : " & Balanza.ToString, TagRFID, ID_SECTOR, 0, Entidades.Constante.USUARIO_AUMAX, acciones.Reset_Balanza)


            ' Dim nTag As New Negocio.TagPlcN
            Dim NombrePlc As String = Entidades.Constante.getNombrePLC(Negocio.ModSesion.PUESTO_TRABAJO.NOMBRE)
            Dim NombreTagHabilitacion As String = If(Balanza = ConstanteBalanza.Balanzas.Uno, ConstanteBalanza.CMD_BALANZA_1,
                                         ConstanteBalanza.CMD_BALANZA_2)
            'deshabilito la balanza
            'Dim xRet As Boolean = nTag.SetValorTag(NombrePlc, NombreTagHabilitacion, ConstanteBalanza.ComandoBalanza.DeshabilitarBalanza)

            Thread.Sleep(2000) ' 3 segundos
            'Espero hasta que se deshabilite la balanza
            While True
                ' Entidades.Balanza.ESTADO_HABILITACION_BAL1 Or oTAG.nombre = Entidades.Balanza.ESTADO_HABILITACION_BAL2
                Dim NombreTagEstadoHabilitacion As String = If(Balanza = ConstanteBalanza.Balanzas.Uno, ConstanteBalanza.ESTADO_HABILITACION_BAL1, ConstanteBalanza.ESTADO_HABILITACION_BAL2)

                'Dim valor As Decimal = nTag.GetValorTag(NombrePlc, NombreTagEstadoHabilitacion)
                'Una vez que se deshabilito la vuelvo habilitar
                'If valor = ConstanteBalanza.EstadoHabilitacionBalanza.Deshabilitado Then
                '    'nTag.SetValorTag(NombrePlc, NombreTagHabilitacion, ConstanteBalanza.ComandoBalanza.HabilitarBalanza)
                '    Exit While
                'End If
                Thread.Sleep(1000) '1 seGundo
            End While



        Catch ex As Exception
            CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, "ResetBalanza: " & ex.Message)


        End Try




    End Sub

    ''' <summary>
    ''' Permite saber si la antena esta habilitada a leer
    ''' verifica si el sensor del sector esta cortando
    ''' </summary>
    ''' <param name="ID_SECTOR"> Numero de sector para verficiar si el sensor esta cortando</param>
    ''' <returns>FALSE = No esta habilitado a leer TRUE = Habilitado a leer </returns>
    Private Function AntenaHabilitada(ID_SECTOR As Integer) As Boolean

        Dim ctrlSensorLectura As Controles.CtrlSensor = Nothing

        Select Case ID_SECTOR
            Case S_LECTURA_DES1.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_DES1

            Case S_LECTURA_DES2.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_DES2

            Case S_LECTURA_DES3.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_DES3

            Case Else
                Return False
        End Select
        'La antena va estar habilitada si el sensor va estar abierto
        Return ctrlSensorLectura.oEstado.valor = SensorInfrarojoEstado.ConPresencia

    End Function

#End Region

#Region "SUBPROCESOS"

    Public Sub Monitoreo()
        'Apenas comienza el hilo conecto los lectores
        If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            If Not IsNothing(Me.ListRFID_SpeedWay) Then ConectarLectoresRFID()
        End If


        While True

            Try
                RefrescarDinamismo()
                'nAlarma.estadoPLC(ID_CONTROL_ACCESO)

                If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFID()
                    VerEstadoLecturaRFID()
                Else
                    Dim oLectores As New List(Of Entidades.LECTOR_RFID)
                    oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFIDCliente(oLectores)
                End If

                VerEstadoDescarga(1) ' Veo el estado de la balanza 1
                VerEstadoDescarga(2)
                VerEstadoDescarga(3)

            Catch ex As Exception
                Dim msj As String = String.Format("{0} - {1}", "[Monitoreo]", ex.Message)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
            End Try
            Thread.Sleep(Me.ControlAcceso.MONITOREO)
        End While

    End Sub

#End Region

#Region "LECTOR RFID"

    ''' <summary>
    ''' Evento que se ejecuta cuando se leyo un tag 
    ''' </summary>
    ''' <param name="TagRFID"></param>
    ''' <param name="NumAntena"></param>
    ''' <remarks></remarks>
    Public Sub TagLeido(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim SUB_TAG As String = "[TagLeido]"
        Console.WriteLine("TAG RFID LEIDO: " & TagRFID)

        Dim nSector As New Negocio.SectorN
        Dim ID_SECTOR As Integer = 0

        'Busco el sector al que corresponde la antena
        Dim nAntena As New Negocio.Antena_RfidN
        Dim oAntena As Entidades.ANTENAS_RFID = nAntena.GetOne(NumAntena, LectorRFID.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)

        If IsNothing(oAntena) Then
            Dim tmp As String = String.Format("{0} La antena {1} no esta habilitada", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        Else
            If Not IsNothing(oAntena.ID_SECTOR) Then ID_SECTOR = oAntena.ID_SECTOR

        End If


        If ID_SECTOR = 0 Then 'No tiene un sector asignado la antena
            Dim tmp As String = String.Format("{0} La antena {1} no tiene ningun sector asignado", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        End If

        If Not manual Then ' Si no es manual (Manual = False) verifica si la antena esta habilitada para leer
            'Si la antena no esta habilitada
            If Not AntenaHabilitada(ID_SECTOR) Then Return
        End If


        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = getAntenaLeyendo(ID_SECTOR)
        If Not IsNothing(ctrlAntLeyendo) Then
            ctrlAntLeyendo.TagLeyendo(TagRFID)
        Else
            Dim tmp As String = String.Format("{0} No se encontro el CtrlAntenaLeyendo", SUB_TAG)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End If

        'Obtengo el buffer
        Dim Buffer As Entidades.BufferTag = getBufferAntena(ID_SECTOR)

        'Agrego al buffer al tag leido y  obtengo el dialog cuando agregue al buffer el tagrfid
        Dim _BufferDIalog As Entidades.BufferTag.BufferDialog = Buffer.AddTagLeido(TagRFID)

        If _BufferDIalog = BufferTag.BufferDialog.TagLeido Then Return ' Termino el procedimiento

        'EJECUTO EL PROCEDIMINETO "EstaHabilitado" PARA SABER SI EL TAG LEIDO ESTA HABILITADO A INGRESAR O SALIR DE LA PORTERIA
        EstaHabilitado(ID_SECTOR, TagRFID)
    End Sub


    ''' <summary>
    ''' Evento que se ejecuta para  informa un error en el Lector RFID
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub ErrorRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, Msj)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando el lector envia un mensaje de información
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub InfoRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, Msj, idSector)
    End Sub


    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub


    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub

    Private Sub DES2_BAR2_Load(sender As Object, e As EventArgs) Handles DES2_BAR2.Load

    End Sub

    Private Sub CtrlControlAccesoDescarga1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnAlarma_Click(sender As Object, e As EventArgs) Handles btnAlarma.Click
        ' Dim frm As New frmAlarmero(ID_CONTROL_ACCESO)
        '  frm.ShowDialog()
    End Sub

#End Region

#Region "EVENTOS BUFFER"

    Public Sub tiempoBufferDescarga1(ByVal Tiempo As Integer) Handles BufferBar1Des1.ShowTiempoBuffer
        Me.CtrlAntenaLeyendoDes1.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferDescarga2(ByVal Tiempo As Integer) Handles BufferBar1Des2.ShowTiempoBuffer
        Me.CtrlAntenaLeyendoDes2.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferDescarga3(ByVal Tiempo As Integer) Handles BufferBar1Des3.ShowTiempoBuffer
        Me.CtrlAntenaLeyendoDes3.setTiempoBuffer(Tiempo)
    End Sub


    Public Sub ClearBufferDescarga1() Handles BufferBar1Des1.ClearBuffer
        Me.CtrlAntenaLeyendoDes1.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferDescarga2() Handles BufferBar1Des2.ClearBuffer
        Me.CtrlAntenaLeyendoDes2.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferDescarga3() Handles BufferBar1Des3.ClearBuffer
        Me.CtrlAntenaLeyendoDes3.BorrarUltimosTagLeido()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Negocio.AuditoriaN.AddAuditoria("Reseteo Control de acceso", "", Me.ID_CONTROL_ACCESO, 0, WS_ERRO_USUARIO, Constante.acciones.ResetControlAcceso, 0)
        If Not IsNothing(ListRFID_SpeedWay) Then
            For Each lector As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
                lector.Disconnect()
            Next
            ListRFID_SpeedWay.Clear()
        End If
        Inicializar()
    End Sub



#End Region
End Class
