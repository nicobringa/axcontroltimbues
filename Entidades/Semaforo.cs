﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Semaforo
    {
        public DATO_WORDINT comando { get; set; }
        public DATO_WORDINT estado { get; set; }
    }
}
