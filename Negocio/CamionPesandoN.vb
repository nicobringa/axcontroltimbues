﻿Public Class CAMION_PESANDON
    Dim oBD As New AccesoDatos.CAMION_PESANDOAD


    ''' <summary>
    ''' Permite guardar en la base de datos el camion que esta pesando
    ''' </summary>
    ''' <param name="CAMION_PESANDO"></param>
    ''' <returns></returns>
    Public Function SetCAMION_PESANDO(CAMION_PESANDO As Entidades.CAMION_PESANDO, idSector2 As Integer) As Entidades.CAMION_PESANDO


        Dim EliminarCamion As Entidades.CAMION_PESANDO = oBD.getOne(Convert.ToInt32(CAMION_PESANDO.ID_SECTOR), idSector2)
        If Not IsNothing(EliminarCamion) Then 'Existe un camion basura
            'Lo elimino
            oBD.delete(EliminarCamion)
        End If
        Return oBD.add(CAMION_PESANDO)

    End Function

    Public Function SetCamionPesando(CamionPesando As Entidades.CAMION_PESANDO) As Entidades.CAMION_PESANDO
        Dim EliminarCamion As Entidades.CAMION_PESANDO = oBD.getOne(CamionPesando.ID_SECTOR)
        If Not IsNothing(EliminarCamion) Then
            oBD.delete(EliminarCamion)
        End If
        Return oBD.add(CamionPesando)
    End Function


    Public Function Delete(CAMION_PESANDO As Entidades.CAMION_PESANDO) As Boolean
        Return oBD.delete(CAMION_PESANDO)
    End Function


    Public Function Update(CAMION_PESANDO As Entidades.CAMION_PESANDO) As Entidades.CAMION_PESANDO
        Return oBD.update(CAMION_PESANDO)
    End Function

    Public Function GetOne(TagRfid As String, idSector As Integer) As Entidades.CAMION_PESANDO
        Return oBD.getOne(TagRfid, idSector)
    End Function

    Public Function GetOne(idSector As Integer) As Entidades.CAMION_PESANDO
        Return oBD.getOne(idSector)
    End Function

    Public Function GetOne(idSector1 As Integer, idSecto2 As Integer) As Entidades.CAMION_PESANDO
        Return oBD.getOne(idSector1, idSecto2)
    End Function


End Class
