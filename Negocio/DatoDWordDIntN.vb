﻿Imports AccesoDatos
Imports Entidades

Public Class DatoDWordDIntN

    Public Function Add(ByVal oDato As DATO_DWORDDINT) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.add(oDato)
    End Function

    Public Function Delete(ByVal oDato As DATO_DWORDDINT) As Boolean
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.delete(oDato)
    End Function

    Public Function Update(ByVal oDato As DATO_DWORDDINT) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.update(oDato)
    End Function

    Public Function GetOne(ByVal id As Integer) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getOne(id)
    End Function

    Public Function GetOne(ByVal idplc As Integer, ByVal tag As String) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getOne(idplc, tag)
    End Function

    Public Function GetAll() As List(Of DATO_DWORDDINT)
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getAll
    End Function

    Public Function GetAll(ByVal idPLC As Integer) As List(Of DATO_DWORDDINT)
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getAll(idPLC)
    End Function

    Public Function GetAllPorSector(idSector As Integer)
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getAllPorSector(idSector)
    End Function
    Public Function GetAllPorSector(idSector As Integer, nroDB As Integer)
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getAllPorSector(idSector, nroDB)
    End Function

    Public Function Leer(ByVal idTAG As Integer) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.leer(idTAG)
    End Function

    Public Function Escribir(ByVal idTAG As Integer, ByVal valor As Integer) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.escribir(idTAG, valor)
    End Function

    Public Function Escribir(oDato As DATO_DWORDDINT) As DATO_DWORDDINT
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.escribir(oDato.ID, oDato.VALOR)
    End Function

    Public Function GetAllTags(ByVal idPLC As Integer, ByVal nombreTag As String, ByVal idSector As Integer) As List(Of DATO_DWORDDINT)
        Dim oDatoAD As New DatoDWordDIntAD
        Return oDatoAD.getAllTags(idPLC, nombreTag, idSector)
    End Function

End Class
