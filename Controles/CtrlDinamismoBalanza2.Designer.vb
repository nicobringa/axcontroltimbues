﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoBalanza2
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pic_s1_barrera_rojo = New System.Windows.Forms.PictureBox()
        Me.pic_s1_barrera_verde = New System.Windows.Forms.PictureBox()
        Me.pic_s1_lectura_rojo = New System.Windows.Forms.PictureBox()
        Me.pic_s1_lectura_verde = New System.Windows.Forms.PictureBox()
        Me.pic_s2_lectura_verde = New System.Windows.Forms.PictureBox()
        Me.pic_s2_lectura_rojo = New System.Windows.Forms.PictureBox()
        Me.pic_s2_barrera_rojo = New System.Windows.Forms.PictureBox()
        Me.pic_s2_barrera_verde = New System.Windows.Forms.PictureBox()
        Me.pic_b2_abajo = New System.Windows.Forms.PictureBox()
        Me.pic_b2_arriba = New System.Windows.Forms.PictureBox()
        Me.pic_s2_posicion_verde = New System.Windows.Forms.PictureBox()
        Me.pic_s2_posicion_rojo = New System.Windows.Forms.PictureBox()
        Me.pic_s1_posicion_verde = New System.Windows.Forms.PictureBox()
        Me.pic_s1_posicion_rojo = New System.Windows.Forms.PictureBox()
        Me.pic_b1_arriba = New System.Windows.Forms.PictureBox()
        Me.pic_b1_abajo = New System.Windows.Forms.PictureBox()
        Me.lblFalla_BAR1 = New System.Windows.Forms.Label()
        Me.lblFalla_BAR2 = New System.Windows.Forms.Label()
        CType(Me.pic_s1_barrera_rojo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s1_barrera_verde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s1_lectura_rojo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s1_lectura_verde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s2_lectura_verde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s2_lectura_rojo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s2_barrera_rojo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s2_barrera_verde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_b2_abajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_b2_arriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s2_posicion_verde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s2_posicion_rojo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s1_posicion_verde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_s1_posicion_rojo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_b1_arriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_b1_abajo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pic_s1_barrera_rojo
        '
        Me.pic_s1_barrera_rojo.Image = Global.Controles.My.Resources.Resources.S1_Barrera_ROJO
        Me.pic_s1_barrera_rojo.Location = New System.Drawing.Point(247, 401)
        Me.pic_s1_barrera_rojo.Name = "pic_s1_barrera_rojo"
        Me.pic_s1_barrera_rojo.Size = New System.Drawing.Size(203, 42)
        Me.pic_s1_barrera_rojo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s1_barrera_rojo.TabIndex = 0
        Me.pic_s1_barrera_rojo.TabStop = False
        '
        'pic_s1_barrera_verde
        '
        Me.pic_s1_barrera_verde.Image = Global.Controles.My.Resources.Resources.S1_Barrera_VERDE
        Me.pic_s1_barrera_verde.Location = New System.Drawing.Point(244, 401)
        Me.pic_s1_barrera_verde.Name = "pic_s1_barrera_verde"
        Me.pic_s1_barrera_verde.Size = New System.Drawing.Size(207, 41)
        Me.pic_s1_barrera_verde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s1_barrera_verde.TabIndex = 1
        Me.pic_s1_barrera_verde.TabStop = False
        '
        'pic_s1_lectura_rojo
        '
        Me.pic_s1_lectura_rojo.Image = Global.Controles.My.Resources.Resources.S1_Lectura_ROJO
        Me.pic_s1_lectura_rojo.Location = New System.Drawing.Point(220, 486)
        Me.pic_s1_lectura_rojo.Name = "pic_s1_lectura_rojo"
        Me.pic_s1_lectura_rojo.Size = New System.Drawing.Size(251, 40)
        Me.pic_s1_lectura_rojo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s1_lectura_rojo.TabIndex = 2
        Me.pic_s1_lectura_rojo.TabStop = False
        '
        'pic_s1_lectura_verde
        '
        Me.pic_s1_lectura_verde.Image = Global.Controles.My.Resources.Resources.S1_Lectura_VERDE
        Me.pic_s1_lectura_verde.Location = New System.Drawing.Point(223, 488)
        Me.pic_s1_lectura_verde.Name = "pic_s1_lectura_verde"
        Me.pic_s1_lectura_verde.Size = New System.Drawing.Size(246, 38)
        Me.pic_s1_lectura_verde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s1_lectura_verde.TabIndex = 3
        Me.pic_s1_lectura_verde.TabStop = False
        '
        'pic_s2_lectura_verde
        '
        Me.pic_s2_lectura_verde.Image = Global.Controles.My.Resources.Resources.S2_LECTURA_VERDE
        Me.pic_s2_lectura_verde.Location = New System.Drawing.Point(302, 68)
        Me.pic_s2_lectura_verde.Name = "pic_s2_lectura_verde"
        Me.pic_s2_lectura_verde.Size = New System.Drawing.Size(81, 12)
        Me.pic_s2_lectura_verde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s2_lectura_verde.TabIndex = 4
        Me.pic_s2_lectura_verde.TabStop = False
        '
        'pic_s2_lectura_rojo
        '
        Me.pic_s2_lectura_rojo.Image = Global.Controles.My.Resources.Resources.S2_LECTURA_ROJO
        Me.pic_s2_lectura_rojo.Location = New System.Drawing.Point(302, 68)
        Me.pic_s2_lectura_rojo.Name = "pic_s2_lectura_rojo"
        Me.pic_s2_lectura_rojo.Size = New System.Drawing.Size(82, 12)
        Me.pic_s2_lectura_rojo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s2_lectura_rojo.TabIndex = 5
        Me.pic_s2_lectura_rojo.TabStop = False
        '
        'pic_s2_barrera_rojo
        '
        Me.pic_s2_barrera_rojo.Image = Global.Controles.My.Resources.Resources.S2_Barrera_ROJO1
        Me.pic_s2_barrera_rojo.Location = New System.Drawing.Point(300, 81)
        Me.pic_s2_barrera_rojo.Name = "pic_s2_barrera_rojo"
        Me.pic_s2_barrera_rojo.Size = New System.Drawing.Size(85, 18)
        Me.pic_s2_barrera_rojo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s2_barrera_rojo.TabIndex = 6
        Me.pic_s2_barrera_rojo.TabStop = False
        '
        'pic_s2_barrera_verde
        '
        Me.pic_s2_barrera_verde.Image = Global.Controles.My.Resources.Resources.S2_BARRERA_VERDE
        Me.pic_s2_barrera_verde.Location = New System.Drawing.Point(300, 81)
        Me.pic_s2_barrera_verde.Name = "pic_s2_barrera_verde"
        Me.pic_s2_barrera_verde.Size = New System.Drawing.Size(85, 16)
        Me.pic_s2_barrera_verde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s2_barrera_verde.TabIndex = 7
        Me.pic_s2_barrera_verde.TabStop = False
        '
        'pic_b2_abajo
        '
        Me.pic_b2_abajo.Image = Global.Controles.My.Resources.Resources.B2_ABAJO1
        Me.pic_b2_abajo.Location = New System.Drawing.Point(284, 58)
        Me.pic_b2_abajo.Name = "pic_b2_abajo"
        Me.pic_b2_abajo.Size = New System.Drawing.Size(103, 41)
        Me.pic_b2_abajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_b2_abajo.TabIndex = 8
        Me.pic_b2_abajo.TabStop = False
        '
        'pic_b2_arriba
        '
        Me.pic_b2_arriba.Image = Global.Controles.My.Resources.Resources.B2_ARRIBA1
        Me.pic_b2_arriba.Location = New System.Drawing.Point(284, 3)
        Me.pic_b2_arriba.Name = "pic_b2_arriba"
        Me.pic_b2_arriba.Size = New System.Drawing.Size(21, 102)
        Me.pic_b2_arriba.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_b2_arriba.TabIndex = 9
        Me.pic_b2_arriba.TabStop = False
        '
        'pic_s2_posicion_verde
        '
        Me.pic_s2_posicion_verde.Image = Global.Controles.My.Resources.Resources.S2_POSICION_VERDE
        Me.pic_s2_posicion_verde.Location = New System.Drawing.Point(295, 105)
        Me.pic_s2_posicion_verde.Name = "pic_s2_posicion_verde"
        Me.pic_s2_posicion_verde.Size = New System.Drawing.Size(95, 25)
        Me.pic_s2_posicion_verde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s2_posicion_verde.TabIndex = 10
        Me.pic_s2_posicion_verde.TabStop = False
        '
        'pic_s2_posicion_rojo
        '
        Me.pic_s2_posicion_rojo.Image = Global.Controles.My.Resources.Resources.S2_POSICION_ROJO
        Me.pic_s2_posicion_rojo.Location = New System.Drawing.Point(296, 106)
        Me.pic_s2_posicion_rojo.Name = "pic_s2_posicion_rojo"
        Me.pic_s2_posicion_rojo.Size = New System.Drawing.Size(93, 25)
        Me.pic_s2_posicion_rojo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s2_posicion_rojo.TabIndex = 11
        Me.pic_s2_posicion_rojo.TabStop = False
        '
        'pic_s1_posicion_verde
        '
        Me.pic_s1_posicion_verde.Image = Global.Controles.My.Resources.Resources.S1_POSICION_VERDE
        Me.pic_s1_posicion_verde.Location = New System.Drawing.Point(258, 300)
        Me.pic_s1_posicion_verde.Name = "pic_s1_posicion_verde"
        Me.pic_s1_posicion_verde.Size = New System.Drawing.Size(170, 35)
        Me.pic_s1_posicion_verde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s1_posicion_verde.TabIndex = 12
        Me.pic_s1_posicion_verde.TabStop = False
        '
        'pic_s1_posicion_rojo
        '
        Me.pic_s1_posicion_rojo.Image = Global.Controles.My.Resources.Resources.S1_POSICION_ROJO
        Me.pic_s1_posicion_rojo.Location = New System.Drawing.Point(258, 300)
        Me.pic_s1_posicion_rojo.Name = "pic_s1_posicion_rojo"
        Me.pic_s1_posicion_rojo.Size = New System.Drawing.Size(169, 33)
        Me.pic_s1_posicion_rojo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_s1_posicion_rojo.TabIndex = 13
        Me.pic_s1_posicion_rojo.TabStop = False
        '
        'pic_b1_arriba
        '
        Me.pic_b1_arriba.Image = Global.Controles.My.Resources.Resources.B1_ARRIBA1
        Me.pic_b1_arriba.Location = New System.Drawing.Point(197, 229)
        Me.pic_b1_arriba.Name = "pic_b1_arriba"
        Me.pic_b1_arriba.Size = New System.Drawing.Size(53, 229)
        Me.pic_b1_arriba.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_b1_arriba.TabIndex = 14
        Me.pic_b1_arriba.TabStop = False
        '
        'pic_b1_abajo
        '
        Me.pic_b1_abajo.Image = Global.Controles.My.Resources.Resources.B1_ABAJO1
        Me.pic_b1_abajo.Location = New System.Drawing.Point(201, 360)
        Me.pic_b1_abajo.Name = "pic_b1_abajo"
        Me.pic_b1_abajo.Size = New System.Drawing.Size(187, 97)
        Me.pic_b1_abajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pic_b1_abajo.TabIndex = 15
        Me.pic_b1_abajo.TabStop = False
        '
        'lblFalla_BAR1
        '
        Me.lblFalla_BAR1.AccessibleDescription = "Balanza 1 Barrera 1"
        Me.lblFalla_BAR1.AutoSize = True
        Me.lblFalla_BAR1.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAR1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAR1.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAR1.Location = New System.Drawing.Point(197, 401)
        Me.lblFalla_BAR1.Name = "lblFalla_BAR1"
        Me.lblFalla_BAR1.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAR1.TabIndex = 95
        Me.lblFalla_BAR1.Text = "Falla"
        Me.lblFalla_BAR1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFalla_BAR2
        '
        Me.lblFalla_BAR2.AccessibleDescription = "Balanza 1 Barrera 1"
        Me.lblFalla_BAR2.AutoSize = True
        Me.lblFalla_BAR2.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAR2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAR2.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAR2.Location = New System.Drawing.Point(269, 77)
        Me.lblFalla_BAR2.Name = "lblFalla_BAR2"
        Me.lblFalla_BAR2.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAR2.TabIndex = 96
        Me.lblFalla_BAR2.Text = "Falla"
        Me.lblFalla_BAR2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlDinamismoBalanza2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.Balanza_Base3
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.lblFalla_BAR2)
        Me.Controls.Add(Me.lblFalla_BAR1)
        Me.Controls.Add(Me.pic_s1_barrera_verde)
        Me.Controls.Add(Me.pic_s2_lectura_verde)
        Me.Controls.Add(Me.pic_s2_barrera_rojo)
        Me.Controls.Add(Me.pic_s1_lectura_verde)
        Me.Controls.Add(Me.pic_s1_lectura_rojo)
        Me.Controls.Add(Me.pic_s1_barrera_rojo)
        Me.Controls.Add(Me.pic_s2_barrera_verde)
        Me.Controls.Add(Me.pic_s2_lectura_rojo)
        Me.Controls.Add(Me.pic_b2_abajo)
        Me.Controls.Add(Me.pic_b2_arriba)
        Me.Controls.Add(Me.pic_s2_posicion_rojo)
        Me.Controls.Add(Me.pic_s1_posicion_rojo)
        Me.Controls.Add(Me.pic_s1_posicion_verde)
        Me.Controls.Add(Me.pic_b1_abajo)
        Me.Controls.Add(Me.pic_b1_arriba)
        Me.Controls.Add(Me.pic_s2_posicion_verde)
        Me.Name = "CtrlDinamismoBalanza2"
        Me.Size = New System.Drawing.Size(700, 525)
        CType(Me.pic_s1_barrera_rojo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s1_barrera_verde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s1_lectura_rojo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s1_lectura_verde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s2_lectura_verde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s2_lectura_rojo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s2_barrera_rojo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s2_barrera_verde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_b2_abajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_b2_arriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s2_posicion_verde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s2_posicion_rojo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s1_posicion_verde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_s1_posicion_rojo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_b1_arriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_b1_abajo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pic_s1_barrera_rojo As PictureBox
    Friend WithEvents pic_s1_barrera_verde As PictureBox
    Friend WithEvents pic_s1_lectura_rojo As PictureBox
    Friend WithEvents pic_s1_lectura_verde As PictureBox
    Friend WithEvents pic_s2_lectura_verde As PictureBox
    Friend WithEvents pic_s2_lectura_rojo As PictureBox
    Friend WithEvents pic_s2_barrera_rojo As PictureBox
    Friend WithEvents pic_s2_barrera_verde As PictureBox
    Friend WithEvents pic_b2_abajo As PictureBox
    Friend WithEvents pic_b2_arriba As PictureBox
    Friend WithEvents pic_s2_posicion_verde As PictureBox
    Friend WithEvents pic_s2_posicion_rojo As PictureBox
    Friend WithEvents pic_s1_posicion_verde As PictureBox
    Friend WithEvents pic_s1_posicion_rojo As PictureBox
    Friend WithEvents pic_b1_arriba As PictureBox
    Friend WithEvents pic_b1_abajo As PictureBox
    Friend WithEvents lblFalla_BAR1 As Label
    Friend WithEvents lblFalla_BAR2 As Label
End Class
