﻿Imports Entidades
Imports Entidades.Constante
Public Class AlarmaN
    Private oBD As New AccesoDatos.AlarmaAD

    Public Function Guardar(oAlarma As Entidades.Alarma) As Entidades.Alarma
        If oAlarma.idAlarma = 0 Then ' Add
            Return oBD.add(oAlarma)
        Else ' Update
            Return oBD.update(oAlarma)
        End If
    End Function

    Public Function GetAll()
        Return oBD.getAll()
    End Function

    Public Function estadoPLC(ByVal idControlAcceso As Int32) As Boolean

        Dim oControl As New Entidades.CONTROL_ACCESO
        Dim nControl As New Negocio.ControlAccesoN
        oControl = nControl.GetOne(idControlAcceso)

        Dim oPlc As New Entidades.Plc
        Dim nPlc As New Negocio.PlcN
        oPlc = nPlc.GetOne(Convert.ToInt32(oControl.idPLC))

        If Not IsNothing(oPlc) Then
            If Not My.Computer.Network.Ping(oPlc.ip) Then
                GenerarAlarma("El PLC no se encuentra en Red", oControl.idControlAcceso, oControl.idControlAcceso, oControl.tipoControlAcceso)
                Return False
            Else
                ModificarAlarma(oControl.idControlAcceso, Constante.tipoFalla.fallaConexíon, oControl.idControlAcceso, oControl.tipoControlAcceso)
                Return True
            End If

        End If
        Return True

    End Function

    ''' <summary>
    ''' Permite crear una alarma 
    ''' </summary>
    Public Overridable Sub GenerarAlarma(detalle As String, idControlAcceso As Integer, idSector As Integer, objeto As String)
        Dim nAlarmas As New Negocio.AlarmaN

        'Si existe la alarma no la genera de vuelta
        If nAlarmas.ExisteAlarma(idSector, idControlAcceso, objeto, detalle) Then Return
        Dim oAlarma As New Entidades.Alarma

        oAlarma.activa = True
        oAlarma.reconocida = False
        oAlarma.fechaAparicion = DateTime.Now
        oAlarma.tag = objeto
        oAlarma.idSector = idSector
        oAlarma.idControlAcceso = idControlAcceso
        oAlarma.detalle = detalle

        nAlarmas.Guardar(oAlarma)
    End Sub
    ''' <summary>
    ''' Permite modificar el estado de una alarma
    ''' </summary>
    ''' <param name="idControlAcceso"></param>
    ''' <param name="tipoFalla"></param>
    ''' <param name="idSector"></param>
    ''' <param name="objeto"></param>
    Public Overridable Sub ModificarAlarma(idControlAcceso As Integer, tipoFalla As Integer, idSector As Integer, objeto As String)
        Dim nAlarmas As New Negocio.AlarmaN
        Dim oAlarma As New Entidades.Alarma
        Dim idAlarma As Int32
        idAlarma = nAlarmas.BuscarIdAlarma(idSector, idControlAcceso, tipoFalla, objeto)
        If idAlarma > 0 Then
            oAlarma = nAlarmas.GetOne(idAlarma)

            oAlarma.activa = False
            oAlarma.fechaDesaparicion = DateTime.Now

            nAlarmas.Guardar(oAlarma)
        End If
    End Sub

    Public Function GetActivas(idSector As Integer)
        Return oBD.getAllActiva(idSector)
    End Function
    Public Function GetOne(idAlarma As Integer) As Entidades.Alarma
        Return oBD.getOne(idAlarma)
    End Function

    ''' <summary>
    ''' Permite saber si ya existe una alarma activa 
    ''' para el objeto 
    ''' </summary>
    ''' <param name="idSector"></param>
    ''' <param name="idControlAcceso"></param>
    ''' <param name="objeto"></param>
    ''' <returns></returns>
    Public Function ExisteAlarma(idSector As Integer, idControlAcceso As Integer, objeto As String, detalle As String) As Boolean
        Dim oAlarma As Entidades.Alarma
        oAlarma = oBD.getAlarmaExistente(idSector, idControlAcceso, objeto, detalle)
        'Si no esta vacio es que ya existe una alarma creada
        Return Not IsNothing(oAlarma)
    End Function

    ''' <summary>
    ''' Devuelve el estado de la alarma
    ''' </summary>
    ''' <param name="idsector"></param>
    ''' <param name="idcontrolAcceso"></param>
    ''' <param name="idFalla"></param>
    ''' <param name="objeto"></param>
    ''' <returns></returns>
    Public Function EstadoAlarma(idsector As Integer, idcontrolAcceso As Integer, idFalla As Integer, objeto As String) As Integer
        Dim oAlarma As Int32
        oAlarma = oBD.getEstadoAlarma(idsector, idcontrolAcceso, idFalla, objeto)
        Return oAlarma
    End Function


    Public Function BuscarIdAlarma(idsector As Integer, idcontrolAcceso As Integer, idFalla As Integer, objeto As String) As Integer
        Dim oAlarma As Int32
        oAlarma = oBD.BuscarId(idsector, idcontrolAcceso, idFalla, objeto)
        Return oAlarma
    End Function


    Public Function mostrarAlarma(idsector As Integer) As Boolean
        If (oBD.BuscarAlarma(idsector)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
