﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class DatoFloatAD
    {
        public DATO_FLOAT add(DATO_FLOAT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    oContext.DATO_FLOAT.Add(oDato);
                    oContext.SaveChanges();
                    return oDato;
                }
            }
            catch (Exception ex)
            {
                return new DATO_FLOAT();
            }
        }

        public bool delete(DATO_FLOAT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.ID == oDato.ID).FirstOrDefault();
                    if (oConsulta != null)
                    {
                        oContext.DATO_FLOAT.Remove(oConsulta);
                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DATO_FLOAT update(DATO_FLOAT oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_FLOAT
                                     where tabla.ID == oDato.ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FK_PLC = oDato.FK_PLC;
                        oConsulta.FLAG_RW = oDato.FLAG_RW;
                        oConsulta.NUM_WORD = oDato.NUM_WORD;
                        oConsulta.NUM_DB = oDato.NUM_DB;
                        oConsulta.QC = oDato.QC;
                        oConsulta.TAG = oDato.TAG;
                        oConsulta.VALOR = oDato.VALOR;

                        oContext.SaveChanges();
                        return oDato;
                    }
                    else
                    {
                        return new DATO_FLOAT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_FLOAT();
            }
        }

        public DATO_FLOAT getOne(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.ID == id).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_FLOAT();
            }
        }

        public DATO_FLOAT getOne(int idplc, string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.TAG == tag && a.FK_PLC == idplc).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_FLOAT();
            }
        }

        public List<DATO_FLOAT> getAll()
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_FLOAT>();
            }
        }

        public List<DATO_FLOAT> getAll(int idplc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.FK_PLC == idplc).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_FLOAT>();
            }
        }

        public List<DATO_FLOAT> getAllPorSector(int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_FLOAT>();
            }
        }

        public List<DATO_FLOAT> getAllPorSector(int idSector, int nroDB)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.ID_SECTOR == idSector && a.NUM_DB == nroDB).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_FLOAT>();
            }
        }

        public DATO_FLOAT leer(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.ID == id).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        if (oConsulta.FLAG_RW == true)
                        {
                            return oConsulta;
                        }
                        else
                        {
                            return new DATO_FLOAT();
                        }
                    }
                    else
                    {
                        return new DATO_FLOAT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_FLOAT();
            }
        }

        public DATO_FLOAT escribir(int id, decimal valor)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_FLOAT
                                     where tabla.ID == id
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FLAG_RW = true;
                        oConsulta.VALOR = valor;

                        oContext.SaveChanges();
                        return oConsulta;
                    }
                    else
                    {
                        return new DATO_FLOAT();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_FLOAT();
            }


        }

        public List<DATO_FLOAT> getAllTags(int idplc, string nombreTag, int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_FLOAT.Where(a => a.FK_PLC == idplc && a.TAG.Contains(nombreTag) && a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_FLOAT>();
            }
        }
    }
}
