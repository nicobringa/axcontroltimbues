﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Impinj.OctaneSdk
Public Class CtrlControlAccesoDescarga

#Region "PROPIEDADES"
    Private ControlAcceso As Entidades.Control_Acceso
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private WithEvents BufferDescarga1 As Entidades.BufferTag
    Private BufferDescarga2 As Entidades.BufferTag
    Private BufferDescarga3 As Entidades.BufferTag
    Private hMonitoreo As Thread
    Private oLector As New Entidades.Lector_RFID
    Private nLector As New Negocio.LectorRFID_N
#End Region


#Region "CONSTRUCTOR"

#End Region

#Region "EVENTOS FORM"

#End Region


#Region "METODOS"

    Public Overrides Sub Inicializar()

        If Me.ID_CONTROL_ACCESO = 0 Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID de puesto de trabajo")

            Return
        End If


        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)

        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
             "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, Me.ControlAcceso.NOMBRE)
        'Si el puesto de trabajo es servidor
        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            'Inicializo como servidor
            IniServidor()
        Else
            'De los contrario como cliente
            IniCliente()
        End If

        'Ejecuto el hilo de monitoreo
        If Not IsNothing(hMonitoreo) Then
            If hMonitoreo.IsAlive Then hMonitoreo.Abort()
            hMonitoreo = Nothing
        End If

        inicializarCtrl()
        SetBuffer()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()
    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR
            'De cada sector traigo los TAG del plc que tiene el control de acceso
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(itemSector.ID_SECTOR)

            ' Me.S_LECTURA_DESCARGA1.Inicializar(Nothing, Me.ControlAcceso.ID_CONTROL_ACCESO, itemSector.ID_SECTOR)


        Next

        CartelLED_DESCARGA.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)

    End Sub

    ''' <summary>
    ''' Inicializar el control de acceso como servidor
    ''' </summary>
    Public Sub IniServidor()
        'Cargo los lectores RFID
        CargarLectoresRFID()

    End Sub

    ''' <summary>
    ''' Inicializar el control de acceso como cliente
    ''' </summary>
    Public Sub IniCliente()

    End Sub

    ''' <summary>
    ''' Permite buscar los lectores RFID cargados en la base de datos
    ''' y cargarlos al sistema
    ''' </summary>
    Private Sub CargarLectoresRFID()
        'Busco los lectores
        Dim nLectorEFID As New Negocio.LectorRFID_N

        Dim listLectorRFID As List(Of Entidades.LECTOR_RFID) = nLectorEFID.GetAll(Me.ID_CONTROL_ACCESO)
        'listLectorRFID.Clear()

        If IsNothing(listLectorRFID) Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, 0, Entidades.Constante.TipoNotificacion.Informacion,
                "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN DE PROCEDIMIENTO
        End If

        If listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN 
        End If

        'Recorro los lectores agregados para el puesto de trabajo
        For Each LectorRFID As Entidades.LECTOR_RFID In listLectorRFID
            'Creo el lector RFID SpeedWay para su utilización
            Dim RFID_SpeedWay As New Negocio.LectorRFID_SpeedWay(LectorRFID)

            If (IsNothing(Me.ListRFID_SpeedWay)) Then Me.ListRFID_SpeedWay = New List(Of Negocio.LectorRFID_SpeedWay)
            'Agrego el LectorRFID a la lista
            Me.ListRFID_SpeedWay.Add(RFID_SpeedWay)
            'Agrego los escuchadores para los evento del lector
            AddHandler RFID_SpeedWay.TagLeido, AddressOf Me.TagLeido
            AddHandler RFID_SpeedWay.ErrorRFID, AddressOf Me.ErrorRFID
            AddHandler RFID_SpeedWay.InfoRFID, AddressOf Me.InfoRFID


        Next

        CtrlEstadoLectoresRFID1.CargarLectores(Me.ListRFID_SpeedWay)
    End Sub

    ''' <summary>
    ''' Permite conectar el lector si esta desconectado
    ''' </summary>
    Private Sub ConectarLectoresRFID()

        'Recorro los lectores RFID Configurado
        For Each RFID_SpeedWay As LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            'Si el lector no esta conectado
            If Not RFID_SpeedWay.IsConnected Then

                Try

                    oLector = nLector.GetOne(RFID_SpeedWay.Ip)
                    oLector.CONECTADO = RFID_SpeedWay.Conectarse()
                    nLector.Update(oLector)

                Catch ex As Exception
                    'Surgio un error en la conexion
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, ex.Message)
                End Try
            Else
                'El lector ya se encuentra conectado
                Dim tmp As String = "[ " + DateTime.Now + " ] INFO: " + String.Format("El lector {0} se encuentra conectado", RFID_SpeedWay.Nombre)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <param name="ID_SECTOR"></param>
    ''' <returns></returns>
    Private Function getBufferAntena(ID_SECTOR As Integer) As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Nothing
        'Le pregunto el sector a las barreras 
        'Para saber si es de ingreso o de egreso

        Buffer = BufferDescarga1




        Return Buffer
    End Function

    ''' <summary>
    ''' Permite obtener el ctrlAntenaLeyendo segun el sector
    ''' </summary>
    ''' <param name="ID_SECTOR"> sector del cual se quiere obtener el control</param>
    ''' <returns></returns>
    Private Function getAntenaLeyendo(ID_SECTOR As Integer) As Controles.CtrlAntenaLeyendo
        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = Nothing

        'Le pregunto el sector a las barreras 
        'Para saber si es de ingreso o de egreso

        ctrlAntLeyendo = CtrlAntenaDescarga1





        Return ctrlAntLeyendo
    End Function


    Private Sub VerEstadoLecturaRFID()
        Dim nDatoBool As New Negocio.DatoBoolN
        Dim nombreTag As String = String.Format("{0}.{1}", TIPO_CONTROL_ACCESO.PORTERIA, PROPIEDADES_TAG_PLC.LECTURA_RFID)
        Dim oDatoLecturaRFID As DATO_BOOL = nDatoBool.GetOne(Me.ControlAcceso.ID_PLC, nombreTag)

        If Not IsNothing(oDatoLecturaRFID) Then
            'Recorro todos los lectores que tiene el control de acceso 
            For Each itemLector As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
                If itemLector.IsConnected Then 'Si esta conectado
                    'Pregunto si tendria que estar leyendo
                    If oDatoLecturaRFID.valor = True Then
                        If Not itemLector.isRunning Then 'Si no esta leyendo 
                            itemLector.StartRead() 'Comienza a leer
                        End If

                    Else ' No tendria que estar leyendo
                        If itemLector.isRunning Then 'Si  esta leyendo 
                            itemLector.StopRead() ' Paro la lectura
                        End If

                    End If
                End If
            Next
        Else
            Dim msj As String = String.Format("No se encontro el tipo de dato {0}", nombreTag)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
        End If

    End Sub

    Private Sub RefrescarDinamismo()
        'Voy a recorrer todos los lectores RFID  y Consultar los puertos de los sensores

        For Each itemSpeedWay As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            If itemSpeedWay.IsConnected Then


            End If

        Next



    End Sub

    ''' <summary>
    ''' Permite saber si la antena esta habilitada a leer
    ''' verifica si el sensor del sector esta cortando
    ''' </summary>
    ''' <param name="ID_SECTOR"> Numero de sector para verficiar si el sensor esta cortando</param>
    ''' <returns>FALSE = No esta habilitado a leer TRUE = Habilitado a leer </returns>
    Private Function AntenaHabilitada(ID_SECTOR As Integer) As Boolean

        Return True

        Dim ctrlSensorLectura As Controles.CtrlSensor = Nothing

        'Select Case ID_SECTOR
        '    Case S_LECTURA_DESCARGA1.ID_SECTOR
        '        ctrlSensorLectura = S_LECTURA_DESCARGA1

        '    Case Else
        '        Return False
        'End Select
        'If Not IsNothing(ctrlSensorLectura.oEstado) Then
        '    'La antena va estar habilitada si el sensor va estar abierto
        '    Return ctrlSensorLectura.oEstado.valor = SensorInfrarojoEstado.ConLectura
        'Else
        '    Return False
        'End If


    End Function

    Private Sub SetBuffer()

        'Inicializo los buffer
        Me.BufferDescarga1 = New Entidades.BufferTag()


        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.BufferDescarga1.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER



    End Sub

#End Region

#Region "SUBPROCESOS"
    Public Sub Monitoreo()
        While True

            Try

                RefrescarDinamismo()
                CtrlEstadoLectoresRFID1.RefrescarEstadosRFID()
                VerEstadoLecturaRFID()

            Catch ex As Exception

            End Try
            Thread.Sleep(Me.ControlAcceso.MONITOREO)
        End While

    End Sub
#End Region

#Region "LECTOR RFID"

    ''' <summary>
    ''' Evento que se ejecuta cuando se leyo un tag 
    ''' </summary>
    ''' <param name="TagRFID"></param>
    ''' <param name="NumAntena"></param>
    ''' <remarks></remarks>
    Public Sub TagLeido(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim SUB_TAG As String = "[TagLeido]"
        Console.WriteLine("TAG RFID LEIDO: " & TagRFID)

        Dim nSector As New Negocio.SectorN
        Dim ID_SECTOR As Integer = 0

        'Busco el sector al que corresponde la antena
        Dim nAntena As New Negocio.Antena_RfidN
        Dim oAntena As Entidades.ANTENAS_RFID = nAntena.GetOne(NumAntena, LectorRFID.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)

        If IsNothing(oAntena) Then
            Dim tmp As String = String.Format("{0} La antena {1} no esta habilitada", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        Else
            If Not IsNothing(oAntena.ID_SECTOR) Then ID_SECTOR = oAntena.ID_SECTOR

        End If
        If ID_SECTOR = 0 Then 'No tiene un sector asignado la antena
            Dim tmp As String = String.Format("{0} La antena {1} no tiene ningun sector asignado", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        End If

        If Not manual Then ' Si no es manual (Manual = False) verifica si la antena esta habilitada para leer
            'Si la antena no esta habilitada
            If Not AntenaHabilitada(ID_SECTOR) Then Return
        End If


        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = getAntenaLeyendo(ID_SECTOR)
        If Not IsNothing(ctrlAntLeyendo) Then
            ctrlAntLeyendo.TagLeyendo(TagRFID)
        Else
            Dim tmp As String = String.Format("{0} No se encontro el CtrlAntenaLeyendo", SUB_TAG)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End If

        'Obtengo el buffer
        Dim Buffer As Entidades.BufferTag = getBufferAntena(ID_SECTOR)

        'Agrego al buffer al tag leido y  obtengo el dialog cuando agregue al buffer el tagrfid
        Dim _BufferDIalog As Entidades.BufferTag.BufferDialog = Buffer.AddTagLeido(TagRFID)

        If _BufferDIalog = BufferTag.BufferDialog.TagLeido Then Return ' Termino el procedimiento
        'Guardo el ultimo tag leido
        Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
        nUltTagLeido.Guardar(TagRFID, ID_SECTOR)
        'EJECUTO EL PROCEDIMINETO "EstaHabilitado" PARA SABER SI EL TAG LEIDO ESTA HABILITADO A INGRESAR O SALIR DE LA PORTERIA
        EstaHabilitado(ID_SECTOR, TagRFID)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta para  informa un error en el Lector RFID
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub ErrorRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, Msj)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando el lector envia un mensaje de información
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub InfoRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, Msj, idSector)
    End Sub

    Public Sub EventoGPO(Puerto As Integer, Estado As Boolean, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay)

        If Estado = False Then 'Si el estado del sensor es TRUE (Cortando)

            If Not LectorRFID_SpeedWay.IsStar Then 'Y no esta leyendo

                LectorRFID_SpeedWay.StartRead() ' Comenzar a leer

            End If

        Else ' Estado = False (No cortando)
            LectorRFID_SpeedWay.ConsultarSensoresLectura(TipoConexion.NormalAbierto)

        End If


    End Sub


    ''' <summary>
    ''' Procedimeinto que consulta al WS EstaHabilitado si el tag leido esta habilitado a ingresar o salir
    ''' dependiendo del sector
    ''' Y procesa el axRespuesta para realizar las operaciones necesarias
    ''' </summary>
    ''' <param name="ID_SECTOR"> Donde se leyo el tag</param>
    ''' <param name="TagRFID">Tag leido</param>
    Private Sub EstaHabilitado(ID_SECTOR As Integer, TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            Dim tmp As String '= String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            ' CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)

            'Preginto si no esta habilitado
            If Not axRespuesta.habilitado Then
                tmp = "[ " + DateTime.Now + " ] CAMIÓN NO HABILITADO: " + axRespuesta.mensaje.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorUser, tmp)
                'Guardo el ultimo tag leido
                Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)
            End If

            If Not IsNothing(axRespuesta) Then CartelLED_DESCARGA.EscribirCartel(axRespuesta.mensaje)
        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End Try

    End Sub

    Private Sub btnLectorRfid_Click(sender As Object, e As EventArgs) Handles btnLectorRfid.Click
        Dim frm As New FrmConfLectoresRFID(Me.ListRFID_SpeedWay)
        frm.ShowDialog()
    End Sub

    Private Sub btnLecturaDescarga1_Click(sender As Object, e As EventArgs) Handles btnLecturaDescarga1.Click

        Dim frm As New FrmLecturaManual(30)
        AddHandler frm.TagLeidoManual, AddressOf TagLeidoManual
        frm.ShowDialog()
        frm.Dispose()


    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Negocio.AuditoriaN.AddAuditoria("Reseteo Control de acceso Balanza", "", Me.ID_CONTROL_ACCESO, 0, WS_ERRO_USUARIO, Constante.acciones.ResetControlAcceso, 0)
        If Not IsNothing(ListRFID_SpeedWay) Then
            For Each lector As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
                lector.Disconnect()
            Next
            ListRFID_SpeedWay.Clear()
        End If
        Inicializar()
    End Sub

    Private Sub btnAyuda_Click(sender As Object, e As EventArgs) Handles btnAyuda.Click
        Help.ShowHelp(Me, Application.StartupPath & "\AyudaCA.chm", HelpNavigator.KeywordIndex, "PantallaDescarga")
    End Sub

    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.Lector_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub


    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub

#End Region

    Public Sub tiempoBufferIngreso(ByVal Tiempo As Integer) Handles BufferDescarga1.ShowTiempoBuffer
        Me.CtrlAntenaDescarga1.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub ClearBufferIngreso() Handles BufferDescarga1.ClearBuffer
        Me.CtrlAntenaDescarga1.BorrarUltimosTagLeido()
    End Sub

End Class
