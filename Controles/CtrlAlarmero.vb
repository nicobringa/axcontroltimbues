﻿Imports System.Threading
Imports Entidades
Public Class CtrlAlarmero

#Region "PROPIEDADES"
    Private _idSector As Integer
    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property


    Private hiloAlarmas As Thread
#End Region


#Region "CONSTRUCTOR"
    Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().


    End Sub


#End Region

#Region "EVENTOS FORM"
    Private Sub dataGridView1_CellClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgAlarmero.CellClick
        ''Elijo la columna que tiene los botones
        If e.ColumnIndex = 8 Then

            Dim oAutenticacion = New FrmAutenticarUsuario(Constante.CategoriaUsuario.Administrador)

            If oAutenticacion.ShowDialog = DialogResult.OK Then
                Dim intID As Integer = 0
                Dim nAlarma As New Negocio.AlarmaN
                Dim oAlarma As Entidades.Alarma

                intID = dgAlarmero.Item(0, e.RowIndex).Value()
                oAlarma = nAlarma.GetOne(intID)

                oAlarma.fechaReconocimieto = DateTime.Now
                oAlarma.reconocida = True
                nAlarma.Guardar(oAlarma)


                refrescarGrilla(Me.idSector)

            End If
        End If

    End Sub
#End Region


#Region "METODOS"

    Public Sub InicializarCtrl(ByVal sector As Integer)
        idSector = sector
        hiloAlarmas = New Thread(AddressOf actualizarEstado)
        hiloAlarmas.IsBackground = True
        hiloAlarmas.Start()
    End Sub

    Public Sub cerrar()
        If Not IsNothing(hiloAlarmas) Then
            If hiloAlarmas.IsAlive Then hiloAlarmas.Abort()
        End If
    End Sub

    Public Sub actualizarEstado()
        While True

            refrescarGrilla(Me.idSector)
            Thread.Sleep(30000)
        End While
    End Sub

    Public Sub refrescarGrilla(Optional ByVal idSector = 0)

        Negocio.modDelegado.ClearRows(Me, dgAlarmero)
        Dim nAlarma As New Negocio.AlarmaN
        Dim oAlarmas As New List(Of Entidades.Alarma)

        oAlarmas = nAlarma.GetActivas(idSector)

        If IsNothing(oAlarmas) Then Return 'NFB

        For Each oAlarma As Entidades.Alarma In oAlarmas
            '' dgAlarmero.Rows.Add(oAlarma.idSector, oAlarma.idControlAcceso, oAlarma.objeto)
            Dim strfecApar As String = oAlarma.fechaAparicion
            Dim strFecRecon As String
            If Not IsNothing(oAlarma.fechaReconocimieto) Then
                strFecRecon = oAlarma.fechaReconocimieto
            Else
                strFecRecon = ""
            End If
            Dim reconocido As String
            If oAlarma.reconocida = False Then
                reconocido = "RECONOCER"
            Else
                reconocido = "-"
            End If

            Negocio.modDelegado.AddRows(Me, dgAlarmero, oAlarma.idAlarma, oAlarma.idSector, oAlarma.idControlAcceso, oAlarma.tag, oAlarma.detalle, strfecApar, strFecRecon, oAlarma.idTipoFalla, reconocido)

        Next

    End Sub

#End Region

#Region "SUBPROCESOS"

#End Region
End Class
