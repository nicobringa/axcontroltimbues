﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlLectorRFID
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnConectarDesconectar = New System.Windows.Forms.Button()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtIP = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnStar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblLectorLectura = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblLectorConexion = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAgrConfiguracion = New System.Windows.Forms.Button()
        Me.Guardar = New System.Windows.Forms.Button()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpConf = New System.Windows.Forms.TabPage()
        Me.flpConf = New System.Windows.Forms.FlowLayoutPanel()
        Me.tpEstado = New System.Windows.Forms.TabPage()
        Me.lblTemperatura = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena4 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena3 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena2 = New System.Windows.Forms.Label()
        Me.lblEstadoAntena1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.cmbModelo = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpConf.SuspendLayout()
        Me.tpEstado.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnConectarDesconectar
        '
        Me.btnConectarDesconectar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnConectarDesconectar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConectarDesconectar.Location = New System.Drawing.Point(568, 49)
        Me.btnConectarDesconectar.Name = "btnConectarDesconectar"
        Me.btnConectarDesconectar.Size = New System.Drawing.Size(154, 34)
        Me.btnConectarDesconectar.TabIndex = 1
        Me.btnConectarDesconectar.Text = "Conectar"
        Me.btnConectarDesconectar.UseVisualStyleBackColor = False
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.Enabled = False
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(3, 11)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(440, 31)
        Me.txtNombre.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 20)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "IP:"
        '
        'txtIP
        '
        Me.txtIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIP.Location = New System.Drawing.Point(40, 53)
        Me.txtIP.Name = "txtIP"
        Me.txtIP.Size = New System.Drawing.Size(215, 26)
        Me.txtIP.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnStar)
        Me.GroupBox1.Location = New System.Drawing.Point(775, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(109, 81)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Estado de Lectura"
        '
        'btnStar
        '
        Me.btnStar.BackColor = System.Drawing.Color.Blue
        Me.btnStar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStar.ForeColor = System.Drawing.Color.White
        Me.btnStar.Location = New System.Drawing.Point(6, 19)
        Me.btnStar.Name = "btnStar"
        Me.btnStar.Size = New System.Drawing.Size(101, 54)
        Me.btnStar.TabIndex = 57
        Me.btnStar.Text = "Start"
        Me.btnStar.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(37, Byte), Integer))
        Me.Panel1.Controls.Add(Me.lblLectorLectura)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lblLectorConexion)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(0, 491)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(888, 26)
        Me.Panel1.TabIndex = 56
        '
        'lblLectorLectura
        '
        Me.lblLectorLectura.AutoSize = True
        Me.lblLectorLectura.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLectorLectura.ForeColor = System.Drawing.Color.Aqua
        Me.lblLectorLectura.Location = New System.Drawing.Point(310, 3)
        Me.lblLectorLectura.Name = "lblLectorLectura"
        Me.lblLectorLectura.Size = New System.Drawing.Size(14, 20)
        Me.lblLectorLectura.TabIndex = 3
        Me.lblLectorLectura.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(203, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Lectura RFID:"
        '
        'lblLectorConexion
        '
        Me.lblLectorConexion.AutoSize = True
        Me.lblLectorConexion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLectorConexion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLectorConexion.Location = New System.Drawing.Point(110, 3)
        Me.lblLectorConexion.Name = "lblLectorConexion"
        Me.lblLectorConexion.Size = New System.Drawing.Size(14, 20)
        Me.lblLectorConexion.TabIndex = 1
        Me.lblLectorConexion.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(101, 20)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Lector RFID:"
        '
        'btnAgrConfiguracion
        '
        Me.btnAgrConfiguracion.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAgrConfiguracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgrConfiguracion.ForeColor = System.Drawing.Color.Black
        Me.btnAgrConfiguracion.Location = New System.Drawing.Point(449, 3)
        Me.btnAgrConfiguracion.Name = "btnAgrConfiguracion"
        Me.btnAgrConfiguracion.Size = New System.Drawing.Size(177, 39)
        Me.btnAgrConfiguracion.TabIndex = 57
        Me.btnAgrConfiguracion.Text = "Agregar Configuración"
        Me.btnAgrConfiguracion.UseVisualStyleBackColor = False
        '
        'Guardar
        '
        Me.Guardar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Guardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guardar.Location = New System.Drawing.Point(632, 3)
        Me.Guardar.Name = "Guardar"
        Me.Guardar.Size = New System.Drawing.Size(137, 39)
        Me.Guardar.TabIndex = 58
        Me.Guardar.Text = "Guardar"
        Me.Guardar.UseVisualStyleBackColor = False
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(3, 113)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(879, 52)
        Me.txtDescripcion.TabIndex = 61
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 20)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "Descripción:"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tpConf)
        Me.TabControl1.Controls.Add(Me.tpEstado)
        Me.TabControl1.Location = New System.Drawing.Point(3, 169)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(879, 319)
        Me.TabControl1.TabIndex = 63
        '
        'tpConf
        '
        Me.tpConf.Controls.Add(Me.flpConf)
        Me.tpConf.Location = New System.Drawing.Point(4, 22)
        Me.tpConf.Name = "tpConf"
        Me.tpConf.Padding = New System.Windows.Forms.Padding(3)
        Me.tpConf.Size = New System.Drawing.Size(871, 293)
        Me.tpConf.TabIndex = 0
        Me.tpConf.Text = "Configuración"
        Me.tpConf.UseVisualStyleBackColor = True
        '
        'flpConf
        '
        Me.flpConf.AutoScroll = True
        Me.flpConf.BackColor = System.Drawing.Color.Aqua
        Me.flpConf.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpConf.Location = New System.Drawing.Point(3, 3)
        Me.flpConf.Name = "flpConf"
        Me.flpConf.Size = New System.Drawing.Size(865, 287)
        Me.flpConf.TabIndex = 11
        '
        'tpEstado
        '
        Me.tpEstado.Controls.Add(Me.lblTemperatura)
        Me.tpEstado.Controls.Add(Me.Label9)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena4)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena3)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena2)
        Me.tpEstado.Controls.Add(Me.lblEstadoAntena1)
        Me.tpEstado.Controls.Add(Me.Label8)
        Me.tpEstado.Controls.Add(Me.PictureBox4)
        Me.tpEstado.Controls.Add(Me.Label7)
        Me.tpEstado.Controls.Add(Me.PictureBox3)
        Me.tpEstado.Controls.Add(Me.Label6)
        Me.tpEstado.Controls.Add(Me.PictureBox2)
        Me.tpEstado.Controls.Add(Me.Label5)
        Me.tpEstado.Controls.Add(Me.PictureBox1)
        Me.tpEstado.Location = New System.Drawing.Point(4, 22)
        Me.tpEstado.Name = "tpEstado"
        Me.tpEstado.Padding = New System.Windows.Forms.Padding(3)
        Me.tpEstado.Size = New System.Drawing.Size(871, 312)
        Me.tpEstado.TabIndex = 1
        Me.tpEstado.Text = "Estado"
        Me.tpEstado.UseVisualStyleBackColor = True
        '
        'lblTemperatura
        '
        Me.lblTemperatura.AutoSize = True
        Me.lblTemperatura.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTemperatura.Location = New System.Drawing.Point(618, 84)
        Me.lblTemperatura.Name = "lblTemperatura"
        Me.lblTemperatura.Size = New System.Drawing.Size(67, 37)
        Me.lblTemperatura.TabIndex = 75
        Me.lblTemperatura.Text = "50°"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(540, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(222, 37)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "Temperatura:"
        '
        'lblEstadoAntena4
        '
        Me.lblEstadoAntena4.AutoSize = True
        Me.lblEstadoAntena4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena4.Location = New System.Drawing.Point(138, 149)
        Me.lblEstadoAntena4.Name = "lblEstadoAntena4"
        Me.lblEstadoAntena4.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena4.TabIndex = 73
        Me.lblEstadoAntena4.Text = "-"
        '
        'lblEstadoAntena3
        '
        Me.lblEstadoAntena3.AutoSize = True
        Me.lblEstadoAntena3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena3.Location = New System.Drawing.Point(138, 111)
        Me.lblEstadoAntena3.Name = "lblEstadoAntena3"
        Me.lblEstadoAntena3.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena3.TabIndex = 72
        Me.lblEstadoAntena3.Text = "-"
        '
        'lblEstadoAntena2
        '
        Me.lblEstadoAntena2.AutoSize = True
        Me.lblEstadoAntena2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena2.Location = New System.Drawing.Point(138, 73)
        Me.lblEstadoAntena2.Name = "lblEstadoAntena2"
        Me.lblEstadoAntena2.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena2.TabIndex = 71
        Me.lblEstadoAntena2.Text = "-"
        '
        'lblEstadoAntena1
        '
        Me.lblEstadoAntena1.AutoSize = True
        Me.lblEstadoAntena1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoAntena1.Location = New System.Drawing.Point(138, 35)
        Me.lblEstadoAntena1.Name = "lblEstadoAntena1"
        Me.lblEstadoAntena1.Size = New System.Drawing.Size(15, 20)
        Me.lblEstadoAntena1.TabIndex = 70
        Me.lblEstadoAntena1.Text = "-"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(45, 149)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(87, 20)
        Me.Label8.TabIndex = 69
        Me.Label8.Text = "Antena 4:"
        '
        'PictureBox4
        '
        Me.PictureBox4.Location = New System.Drawing.Point(6, 143)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox4.TabIndex = 68
        Me.PictureBox4.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(45, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 20)
        Me.Label7.TabIndex = 67
        Me.Label7.Text = "Antena 3:"
        '
        'PictureBox3
        '
        Me.PictureBox3.Location = New System.Drawing.Point(6, 105)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox3.TabIndex = 66
        Me.PictureBox3.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(45, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 20)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "Antena 2:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(6, 67)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 64
        Me.PictureBox2.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(45, 35)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 20)
        Me.Label5.TabIndex = 63
        Me.Label5.Text = "Antena 1:"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(6, 29)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'cmbModelo
        '
        Me.cmbModelo.FormattingEnabled = True
        Me.cmbModelo.Location = New System.Drawing.Point(337, 58)
        Me.cmbModelo.Name = "cmbModelo"
        Me.cmbModelo.Size = New System.Drawing.Size(215, 21)
        Me.cmbModelo.TabIndex = 64
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(269, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 20)
        Me.Label10.TabIndex = 65
        Me.Label10.Text = "Modelo:"
        '
        'CtrlLectorRFID
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmbModelo)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Guardar)
        Me.Controls.Add(Me.btnAgrConfiguracion)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtIP)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.btnConectarDesconectar)
        Me.Name = "CtrlLectorRFID"
        Me.Size = New System.Drawing.Size(888, 517)
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tpConf.ResumeLayout(False)
        Me.tpEstado.ResumeLayout(False)
        Me.tpEstado.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnConectarDesconectar As System.Windows.Forms.Button
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtIP As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnStar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblLectorLectura As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblLectorConexion As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnAgrConfiguracion As System.Windows.Forms.Button
    Friend WithEvents Guardar As System.Windows.Forms.Button
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpConf As System.Windows.Forms.TabPage
    Friend WithEvents flpConf As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tpEstado As System.Windows.Forms.TabPage
    Friend WithEvents lblTemperatura As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblEstadoAntena4 As System.Windows.Forms.Label
    Friend WithEvents lblEstadoAntena3 As System.Windows.Forms.Label
    Friend WithEvents lblEstadoAntena2 As System.Windows.Forms.Label
    Friend WithEvents lblEstadoAntena1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbModelo As ComboBox
    Friend WithEvents Label10 As Label
End Class
