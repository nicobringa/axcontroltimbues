﻿Public Class CtrlProgressBalanza



    Private _Value As Int16
    Public Property Value() As Int16
        Get
            Return _Value
        End Get
        Set(ByVal value As Int16)
            _Value = value

            If value = 0 Then
                Me.picBarra.Width = 0
            Else
                Me.picBarra.Width = (Me.Size.Width / (Me.Size.Width / value)) + 3
            End If

        End Set
    End Property


    Private _ColorBarra As Color
    Public Property ColorBarra() As Color
        Get
            Return _ColorBarra
        End Get
        Set(ByVal value As Color)
            _ColorBarra = value
            Me.picBarra.BackColor = value
        End Set
    End Property



    Public Sub SetProgressPeso(ByVal PorcentajeProgress As Integer, ByVal ColoProgess As Color)
        Negocio.modDelegado.SetWithCtrl(Me, Me.picBarra, PorcentajeProgress)
        Negocio.modDelegado.SetBackColorCtrl(Me, Me.picBarra, ColoProgess)

    End Sub




End Class
