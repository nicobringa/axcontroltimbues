﻿function EjecutarComando(sector, comando, descripcion) {
    $.ajax({
        type: "POST",
        url: "/Comandos/GenerarComando?idSector=" + sector + "&idComando=" + comando + "&tag=" + descripcion, // the URL of the controller action method

        data: null, // optional data
        success: function (result) {
            // do something with result
        },
        error: function (req, status, error) {
            // do something with error   
        }
    });
}

function fnGuardarNotificacion(idSector, idControlAcceso, Notificacion, tipoNotif) {
    $.ajax({
        type: "POST",
        url: "/Notificaciones/GuardarNotificacion?idSector=" + idSector + "&idControlAcceso=" + idControlAcceso + "&Notificacion=" + Notificacion + "&tipoNotif=" + tipoNotif, // the URL of the controller action method

        data: null, // optional data
        success: function (result) {
            // do something with result
        },
        error: function (req, status, error) {
            // do something with error   
        }
    });
}

function tagLeidoManual(sector) {
    var nroTag = prompt("Ingrese el nro. de tag:", "");
    if (nroTag == null || nroTag == "") {
    } else {
        $.ajax({
            type: "POST",
            url: "/Comandos/TagLeidoManual?idSector=" + sector + "&tag=" + nroTag, // the URL of the controller action method

            data: null, // optional data
            success: function (result) {
                // do something with result
            },
            error: function (req, status, error) {
                // do something with error   
            }
        });
        //var url = "@Html.Raw(Url.Action('TagLeidoManual', 'Comandos', new { idSector = '-2', tag = '-1' }))";  
        //url = url.replace("-2", sector);
        //url = url.replace("-1", nroTag);

        //$.get(url, function (data) {
        //});
    }
}

function GenerarComandoControl(idSector, tag, idTipoComando) {
    $.ajax({
        type: "POST",
        url: "/Comandos/ComandoControl?idSector=" + idSector + "&tag=" + tag + "&idTipoComando=" + idTipoComando, // the URL of the controller action method

        data: null, // optional data
        success: function (result) {
            // do something with result
        },
        error: function (req, status, error) {
            // do something with error   
        }
    });
}