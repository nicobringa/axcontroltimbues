//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class SECTOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SECTOR()
        {
            this.ANTENAS_RFID = new HashSet<ANTENAS_RFID>();
            this.PUERTO_RFID = new HashSet<PUERTO_RFID>();
            this.AUDITORIA = new HashSet<AUDITORIA>();
            this.NOTIFICACION = new HashSet<NOTIFICACION>();
        }
    
        public int ID_SECTOR { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<int> ID_CONTROL_ACCESO { get; set; }
        public string DESCRIPCION { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANTENAS_RFID> ANTENAS_RFID { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PUERTO_RFID> PUERTO_RFID { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AUDITORIA> AUDITORIA { get; set; }
        public virtual CONTROL_ACCESO CONTROL_ACCESO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NOTIFICACION> NOTIFICACION { get; set; }
    }
}
