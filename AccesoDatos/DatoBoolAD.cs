﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class DatoBoolAD
    {
        public DATO_BOOL add(DATO_BOOL oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    oContext.DATO_BOOL.Add(oDato);
                    oContext.SaveChanges();
                    return oDato;
                }
            }
            catch (Exception ex)
            {
                return new DATO_BOOL();
            }
        }

        public bool delete(DATO_BOOL oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.Where(a => a.ID == oDato.ID).FirstOrDefault();
                    if (oConsulta != null)
                    {
                        oContext.DATO_BOOL.Remove(oConsulta);
                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DATO_BOOL update(DATO_BOOL oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_BOOL
                                     where tabla.ID == oDato.ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FK_PLC = oDato.FK_PLC;
                        oConsulta.FLAG_RW = oDato.FLAG_RW;
                        oConsulta.NUM_BIT = oDato.NUM_BIT;
                        oConsulta.NUM_BYTE = oDato.NUM_BYTE;
                        oConsulta.NUM_DB = oDato.NUM_DB;
                        oConsulta.QC = oDato.QC;
                        oConsulta.TAG = oDato.TAG;
                        oConsulta.VALOR = oDato.VALOR;

                        oContext.SaveChanges();
                        return oDato;
                    }
                    else
                    {
                        return new DATO_BOOL();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_BOOL();
            }
        }

        public DATO_BOOL getOne(int ID)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.Where(a => a.ID == ID).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_BOOL();
            }
        }

        public DATO_BOOL getOne(int idplc, string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.Where(a => a.TAG == tag && a.FK_PLC == idplc).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_BOOL();
            }
        }

        public List<DATO_BOOL> getAll()
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BOOL>();
            }
        }

        public List<DATO_BOOL> getAll(int idplc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.Where(a => a.FK_PLC == idplc).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BOOL>();
            }
        }

        public DATO_BOOL leer(int ID)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.Where(a => a.ID == ID).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        if (oConsulta.FLAG_RW == true)
                        {
                            return oConsulta;
                        }
                        else
                        {
                            return new DATO_BOOL();
                        }
                    }
                    else
                    {
                        return new DATO_BOOL();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_BOOL();
            }
        }

        public DATO_BOOL escribir(int ID, bool valor)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_BOOL
                                     where tabla.ID == ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {

                        oConsulta.FLAG_RW = true;
                        oConsulta.VALOR = valor;

                        oContext.SaveChanges();
                        return oConsulta;

                    }
                    else
                    {
                        return new DATO_BOOL();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_BOOL();
            }


        }

        public List<DATO_BOOL> getAllTags(int idplc, string nombreTag, int ID_SECTOR)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BOOL.Where(a => a.FK_PLC == idplc && a.TAG.Contains(nombreTag) && a.ID_SECTOR == ID_SECTOR).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BOOL>();
            }
        }

    }
}
