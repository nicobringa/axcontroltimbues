﻿Imports System.Configuration
Imports Entidades
Imports Entidades.Constante
Imports Negocio

Public Class FrmConfBD

#Region "Variables de Instancia"

    Private oArchivoAppConfig As ArchivoAppConfig
    Private oArchivoConfiguration As Configuration
    Private oSeccionCadenaConexion As ConnectionStringsSection
    Private StrCadenaConexion As String
    Private StrCadenaConexionParte1 As String
    Private StrNombreServidor As String
    Private StrCadenaConexionParte2 As String


    Private SessionLocal As String = "ax_ControlEntities"
    Private SessionDriver As String = "ax_Drivers7Entities"
    'Private SessionAxLaundryMaster As String = "ax_laundry_master"

    'server=192.168.226.66;user id=aumax;password=codasaxis;persistsecurityinfo=True;database=ax_laundry
    'PROPIEDADES DEL STRING DE CONEXION
    Private PROP_STR_CONEX_SERVIDOR As String = "server="
    Private PROP_STR_CONEX_USUARIO As String = "user id="
    Private PROP_STR_CONEX_CONTRASENIA As String = "password="
    Private PROP_STR_CONEX_BASEDATOS As String = "database="




#End Region

#Region "Eventos"
    Public Event CambiosConfiguracion(ByVal BlnHabilitar As Boolean)
#End Region

#Region "Conctructor"
    Public Sub New()
        InitializeComponent()

        Me.oArchivoAppConfig = New ArchivoAppConfig


        Consultar()
    End Sub
#End Region

#Region "Metodos"

    Private Function CamposCorrectos() As Boolean
        Dim BlnCamposCorrectos As Boolean = True

        Me.EP.SetError(Me.txtServidorLocal, Nothing)
        'Si no se especifico cadena conexion
        If Len(Trim(Me.txtServidorLocal.Text)) = 0 Then
            BlnCamposCorrectos = False
            Me.EP.SetError(Me.txtServidorLocal, "No ha ingresado Cadena de Conexión")
        End If

        Return BlnCamposCorrectos
    End Function

    Private Function ArmarStrConex(ByVal Servidor As String, ByVal Usuario As String, ByVal contrasenia As String, ByVal BaseDatos As String) As String
        'server=192.168.226.66;user id=aumax;password=codasaxis;persistsecurityinfo=True;database=ax_laundry
        Dim strConex As String

        strConex = "server=" & Servidor & ";"
        strConex += "user id=" & Usuario & ";"
        strConex += "password=" & contrasenia & ";"
        strConex += "persistsecurityinfo=True;"
        strConex += "database=" & BaseDatos

        Return strConex
    End Function

    Private Function getNombrePropiedadConex(ByVal strConex As String, ByVal PropiedadConex As String) As String
        Try
            'server=192.168.226.66;user id=aumax;password=codasaxis;persistsecurityinfo=True;database=ax_laundry
            Dim PosNombre As Integer = InStr(strConex, PropiedadConex)

            Dim PosCharPuntoComa As Integer = InStr(PosNombre, strConex, ";")

            Dim PosCharIgual As Integer = InStr(PosNombre, strConex, "=")
            Dim LenghtNombrePropiedad As String

            If (PosCharPuntoComa > 0) Then
                LenghtNombrePropiedad = (PosCharPuntoComa - PosCharIgual) - 1
            Else
                PosCharPuntoComa = strConex.Length
                LenghtNombrePropiedad = (PosCharPuntoComa - PosCharIgual)
            End If

            Dim NombrePropiedad As String = strConex.Substring(PosCharIgual, LenghtNombrePropiedad)

            Return NombrePropiedad
        Catch ex As Exception

            MessageBox.Show(ex.Message, "GerNombrePropiedadConex", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ""
        End Try

    End Function






    Private Sub BtnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'server=192.168.226.66;user id=aumax;password=codasaxis;persistsecurityinfo=True;database=ax_laundry
        Try
            'Si los campos son correctos
            If Me.CamposCorrectos() Then
                Me.oArchivoAppConfig = New ArchivoAppConfig
                Dim oAutenticacion = New Controles.FrmAutenticarUsuario(CategoriaUsuario.Administrador)
                Dim descripAuditoria As String
                If oAutenticacion.ShowDialog = DialogResult.OK Then
                    'Si cambio el nombre del servidor
                    If Me.txtServidorLocal.Text <> Me.StrNombreServidor Then
                        If MsgBox("¿Está seguro que desea editar los datos del servidor?", vbYesNo, "Mensaje") = vbYes Then
                            'Especifico el nuevo valor de la cadena de conexion

                            If TabControlConfiguracion.SelectedIndex = 0 Then
                                descripAuditoria = "Modifico la configuración de base de datos Local "
                                Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionBaseDatos, 0)
                                Me.oArchivoAppConfig.SetCadenaConexion(SessionLocal, ArmarStrConex(txtServidorLocal.Text, txtUsuarioLocal.Text, txtContraseniaLocal.Text, txtBaseDatosLocal.Text))

                            Else
                                descripAuditoria = "Modifico la configuración de base de datos Servidor "
                                Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionBaseDatos, 0)
                                Me.oArchivoAppConfig.SetCadenaConexion(SessionDriver, ArmarStrConex(txtServidorDriver.Text, txtUsuarioDriver.Text, txtContraseniaDriver.Text, txtBaseDriver.Text))

                            End If


                            MsgBox("Reinicie la aplicación para que tome los cambios de la Cadena de conexión.", vbInformation, "Mensaje")

                            RaiseEvent CambiosConfiguracion(False)
                        End If
                    End If
                Else
                    Me.Close()
                End If

                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("No se ha podido guardar los cambios en el Archivo de Configuración, asegúrese que la aplicación tenga permisos de Administrador.", vbCritical, "Mensaje")
        End Try
    End Sub

    Private Sub Consultar()
        Dim StrConexLocal As String = Me.oArchivoAppConfig.GetCadenaConexion(SessionLocal)

        Dim StrConexDriver As String = Me.oArchivoAppConfig.GetCadenaConexion(SessionDriver)
        'Dim StrConexMaster As String = Me.oArchivoAppConfig.GetCadenaConexion(SessionAxLaundryMaster)

        txtServidorLocal.Text = getNombrePropiedadConex(StrConexLocal, PROP_STR_CONEX_SERVIDOR)
        txtUsuarioLocal.Text = getNombrePropiedadConex(StrConexLocal, PROP_STR_CONEX_USUARIO)
        txtContraseniaLocal.Text = getNombrePropiedadConex(StrConexLocal, PROP_STR_CONEX_CONTRASENIA)
        txtBaseDatosLocal.Text = getNombrePropiedadConex(StrConexLocal, PROP_STR_CONEX_BASEDATOS)

        txtServidorDriver.Text = getNombrePropiedadConex(StrConexDriver, PROP_STR_CONEX_SERVIDOR)
        txtUsuarioDriver.Text = getNombrePropiedadConex(StrConexDriver, PROP_STR_CONEX_USUARIO)
        txtContraseniaDriver.Text = getNombrePropiedadConex(StrConexDriver, PROP_STR_CONEX_CONTRASENIA)
        txtBaseDriver.Text = getNombrePropiedadConex(StrConexDriver, PROP_STR_CONEX_BASEDATOS)
    End Sub
#End Region

    Private Sub TabPageBDLocal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPageBDLocal.Click

    End Sub
End Class