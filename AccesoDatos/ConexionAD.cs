﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

using System.Data.EntityClient;

namespace AccesoDatos
{
    public class ConexionAD
    {
        public Boolean ConexionAxCotrol()
        {
             string TAG  = "[ConexionAxDriver]";
            try
            {
                EntityConnection conn = new EntityConnection("name=ax_ControlEntities");

                conn.Open();
                return true;
            }
            catch (Exception ex)
            {

                Entidades.ErrorBD.setMsjError(ex.Message);
                return false;
            }
        }

        public Boolean ConexionAxDriver()
        {
            string TAG = "[ConexionAxDriver]";
            try
            {
                EntityConnection conn = new EntityConnection("name=ax_Drivers7Entities");

                conn.Open();
                return true;
            }
            catch (Exception ex)
            {

               Entidades.ErrorBD.setMsjError(ex.Message);
                return false;
            }
        }
    }
}
