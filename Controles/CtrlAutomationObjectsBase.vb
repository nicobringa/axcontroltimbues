﻿Imports System.Windows.Forms
Public Class CtrlAutomationObjectsBase

    Private _Inicializado As Boolean = False
    Public Property Inicializado() As Boolean
        Get
            Return _Inicializado
        End Get
        Set(ByVal value As Boolean)
            _Inicializado = value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad que permite saber si el sensor verifica su estado con el lector RFID o el PLC
    ''' =TRUE Verifica el estado con el lector | =False Verifica el estado con el PLC
    ''' </summary>
    Private _GPIO As Boolean
    Public Property GPIO() As Boolean
        Get
            Return _GPIO
        End Get
        Set(ByVal value As Boolean)
            _GPIO = value
        End Set
    End Property


    Private newPLC As String
    Public Property PLC() As String
        Get
            Return newPLC
        End Get
        Set(ByVal value As String)
            newPLC = value
        End Set
    End Property

    Private _idPLC As Integer
    Public Property ID_PLC() As Integer
        Get
            Return _idPLC
        End Get
        Set(ByVal value As Integer)
            _idPLC = value
        End Set
    End Property

    Private _idControlAcceso As Integer
    Public Property ID_CONTROL_ACCESO() As Integer
        Get
            Return _idControlAcceso
        End Get
        Set(ByVal value As Integer)
            _idControlAcceso = value
        End Set
    End Property

    Private _idSector As Integer
    Public Property ID_SECTOR() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Private _BitMonitoreo As Int16
    Public Property BitMonitoreo() As Int16
        Get
            Return _BitMonitoreo
        End Get
        Set(ByVal value As Int16)
            _BitMonitoreo = value
        End Set
    End Property

    Public Event Alarma()

    Private _patenteLeida As String
    Public Property patenteLeida() As String
        Get
            Return _patenteLeida
        End Get
        Set(ByVal value As String)
            'Si se borro la ultima patente leida o se actualizo la patente 
            If value = "" Or value <> _patenteLeida Then
                'Vuelvo el tiempo a 0
                tiempoPatenteLeidas = 0
            End If
            _patenteLeida = value
        End Set
    End Property

    Private _tiempoPatenteLeida As Integer
    Public Property tiempoPatenteLeidas() As Integer
        Get
            Return _tiempoPatenteLeida
        End Get
        Set(ByVal value As Integer)
            _tiempoPatenteLeida = value
        End Set
    End Property

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()


        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Public Overridable Sub Inicializar()

    End Sub

    Public Overridable Sub Inicializar(ID_PLC As Integer?, ID_CONTROL_ACCESO As Integer, ID_SECTOR As Integer)

    End Sub




    Public Overridable Sub LecturaBaseDatos()

    End Sub

    Public Overridable Sub SetHabControl(ByVal hab As Boolean)

    End Sub

    ''' <summary>
    ''' Permite crear una alarma 
    ''' </summary>
    'Public Overridable Sub GenerarAlarma(detalle As String, ID_CONTROL_ACCESO As Integer, ID_SECTOR As Integer, objeto As String)
    '    Dim nAlarmas As New Negocio.AlarmaN

    '    'Si existe la alarma no la genera de vuelta
    '    If nAlarmas.ExisteAlarma(ID_SECTOR, ID_CONTROL_ACCESO, objeto, detalle) Then Return
    '    Dim oAlarma As New Entidades.Alarma

    '    oAlarma.activa = True
    '    oAlarma.reconocida = False
    '    oAlarma.fechaAparicion = DateTime.Now
    '    oAlarma.tag = objeto
    '    oAlarma.ID_SECTOR = ID_SECTOR
    '    oAlarma.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
    '    oAlarma.detalle = detalle

    '    nAlarmas.Guardar(oAlarma)
    'End Sub
    ''' <summary>
    ''' Permite modificar el estado de una alarma
    ''' </summary>
    ''' <param name="ID_CONTROL_ACCESO"></param>
    ''' <param name="tipoFalla"></param>
    ''' <param name="ID_SECTOR"></param>
    ''' <param name="objeto"></param>
    'Public Overridable Sub ModificarAlarma(ID_CONTROL_ACCESO As Integer, tipoFalla As Integer, ID_SECTOR As Integer, objeto As String)
    '    Dim nAlarmas As New Negocio.AlarmaN
    '    Dim oAlarma As New Entidades.Alarma
    '    Dim idAlarma As Int32
    '    idAlarma = nAlarmas.BuscarIdAlarma(ID_SECTOR, ID_CONTROL_ACCESO, tipoFalla, objeto)
    '    If idAlarma > 0 Then
    '        oAlarma = nAlarmas.GetOne(idAlarma)

    '        oAlarma.activa = False
    '        oAlarma.fechaDesaparicion = DateTime.Now

    '        nAlarmas.Guardar(oAlarma)
    '    End If
    'End Sub



End Class
