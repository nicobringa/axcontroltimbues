﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Newtonsoft.Json;

namespace AxControlWeb.Controllers
{
    [Authorize]
    public class ControlAccesoController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();

        public string GetControlesAcceso()
        {
            List<CONTROL_ACCESO> ListaControlAcceso = db.CONTROL_ACCESO.OrderBy(a => a.ID_CONTROL_ACCESO).ToList();
            return JsonConvert.SerializeObject(ListaControlAcceso, Formatting.Indented,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);    
        }


        public ActionResult Control(int idControl)
        {
            var controlActual = (from ca in db.CONTROL_ACCESO where ca.ID_CONTROL_ACCESO == idControl select ca).FirstOrDefault();
            return View(controlActual);
        }



        public ActionResult Control_(int idControl)
        {
            var controlActual = (from ca in db.CONTROL_ACCESO where ca.ID_CONTROL_ACCESO == idControl select ca).FirstOrDefault();
            return PartialView(controlActual);
        }

        public ActionResult Semaforo(int idSector)
        {
            AxDriverORAEntities dbX = new AxDriverORAEntities();
            var Semaforo1 = (from s in dbX.DATO_WORDINT where s.ID_SECTOR == idSector && s.TAG.Contains("SEMAFORO1") select s).FirstOrDefault();
            var Semaforo2 = (from s in dbX.DATO_WORDINT where s.ID_SECTOR == idSector && s.TAG.Contains("SEMAFORO2") select s).FirstOrDefault();

            if (Semaforo1 == null && Semaforo2 == null)
            {
                var Semaforo = (from s in dbX.DATO_WORDINT where s.ID_SECTOR == idSector && s.TAG.Contains("SEMAFORO") && s.TAG.Contains("Estado") select s).FirstOrDefault();
                ViewBag.estadoSemaforo = Semaforo.VALOR.ToString();
                return View(Semaforo);
            }

            if (Semaforo1 != null)
            {
                ViewBag.estadoSemaforo1 = Semaforo1.VALOR.ToString();
            }
            else
            {
                ViewBag.estadoSemaforo1 = "NULL";
            }

            if (Semaforo2 != null)
            {
                ViewBag.estadoSemaforo2 = Semaforo2.VALOR.ToString();
                return View(Semaforo2);
            }
            else
            {
                ViewBag.estadoSemaforo2 = "NULL";
                return View(Semaforo1);
            }

        }

        public ActionResult Barrera(int idSector)
        {
            AxDriverORAEntities dbX = new AxDriverORAEntities();
            var Barrera1 = (from s in dbX.DATO_WORDINT where s.ID_SECTOR == idSector && s.TAG.Contains("BARRERA") select s).FirstOrDefault();
            var SensorBarrera1 = (from s in dbX.DATO_WORDINT where s.ID_SECTOR == idSector && s.TAG.Contains("S_BARRERA.TiempoRet") select s).FirstOrDefault();

            if (Barrera1 != null)
            {
                ViewBag.Barrera1 = Barrera1.VALOR.ToString();
            }
            else
            {
                ViewBag.Barrera1 = "NULL";
            }
            ViewBag.Sensor = SensorBarrera1.VALOR;
            return View(Barrera1);
        }

        public ActionResult Sensor(int idSector)
        {
            AxDriverORAEntities dbX = new AxDriverORAEntities();
            var Sensor1 = (from s in dbX.DATO_WORDINT where s.ID_SECTOR == idSector && s.TAG.Contains("S_LECTURA") select s).FirstOrDefault();
            if (Sensor1 != null)
            {
                ViewBag.estadoSensor1 = Sensor1.VALOR.ToString();
            }
            else
            {
                ViewBag.estadoSensor1 = "NULL";
            }
            return View(Sensor1);
        }

        public ActionResult ComandosSensor(int idSector, string tag)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            Entidades.Sensor oSensor = new Entidades.Sensor();
            oSensor.Descripcion = tag;
            oSensor.Estado = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG.Contains(tag + ".Estado")).FirstOrDefault();
            oSensor.Habilitacion = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG.Contains(tag + ".Habilitacion")).FirstOrDefault();
            oSensor.TiempoRet = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG.Contains(tag + ".TiempoRet")).FirstOrDefault();
            ViewBag.oSensor = oSensor;
            SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSector = oSector;
            return View();
        }

        public ActionResult ComandosBarrera(int idSector, string tag)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            Entidades.Barrera oBarrera = new Entidades.Barrera();
            oBarrera.Descripcion = tag;
            oBarrera.Estado = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG.Contains(tag + ".Estado")).FirstOrDefault();
            oBarrera.Comando = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG.Contains(tag + ".Comando")).FirstOrDefault();
            oBarrera.HabSenPosicion = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG.Contains(tag + ".HabSenPosicion")).FirstOrDefault();
            ViewBag.oBarrera = oBarrera;
            SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSector = oSector;
            return View();
        }

        /// <summary>
        /// Devuelvo como JSON el estado que tiene el dispositivo
        /// </summary>
        /// <param name="idSector"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        public string GetJsonEstadoDispositivo(int idSector, string tag)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            int? inEstadoDispositivo = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == tag).Select(a => a.VALOR).FirstOrDefault();
            return JsonConvert.SerializeObject(Convert.ToInt16(inEstadoDispositivo), Formatting.Indented,
              new JsonSerializerSettings
              {
                  ReferenceLoopHandling = ReferenceLoopHandling.Ignore
              });            
        }

        public ActionResult ComandosSemaforo(int idSector, string tag)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            DATO_WORDINT oSemaforoEstado = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG==(tag+".Estado")).FirstOrDefault();
            DATO_WORDINT oSemaforoComando = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == (tag + ".Comando")).FirstOrDefault();
            ViewBag.oSemaforoEstado = oSemaforoEstado;
            ViewBag.oSemaforoComando = oSemaforoComando;
            SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSector = oSector;
            ViewBag.Tag = tag;
            return View();
        }

        public ActionResult Ayuda(int idSector)
        {
            return View();
        }

        public PartialViewResult DinamismoPorteria_(int idSector)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            SECTOR oSector = db.SECTOR.Where(a => a.ID_CONTROL_ACCESO == idSector).FirstOrDefault();

            if (oSector == null)
            {
                oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            }

            SECTOR_PORTERIA newSectPorteria = new SECTOR_PORTERIA();
            newSectPorteria.NOMBRE = oSector.NOMBRE;
            newSectPorteria.ID_SECTOR = oSector.ID_SECTOR;
            newSectPorteria.ID_CONTROL_ACCESO = oSector.ID_CONTROL_ACCESO;
            newSectPorteria.DESCRIPCION = oSector.DESCRIPCION;

            var Barrera = (from o in dbD.DATO_WORDINT where o.ID_SECTOR == oSector.ID_SECTOR && o.TAG == "BARRERA.Estado" select o).FirstOrDefault();
            if (Barrera != null)
            {
                newSectPorteria.Barrera = (int)Barrera.VALOR;
            }
            else
            {
                newSectPorteria.Barrera = 0;
            }

            var Sensor = (from o in dbD.DATO_WORDINT where o.ID_SECTOR == oSector.ID_SECTOR && o.TAG == "S_BARRERA.Estado" select o).FirstOrDefault();
            if (Sensor != null)
            {
                newSectPorteria.Sensor = (int)Sensor.VALOR;
            }
            else
            {
                newSectPorteria.Sensor = 0;
            }

            var EstadoSem1 = (from a in dbD.DATO_WORDINT where a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "SEMAFORO1.Estado" select a).FirstOrDefault();
            if (EstadoSem1 != null)
            {
                newSectPorteria.Semaforo1 = (int)EstadoSem1.VALOR;
            }
            else
            {
                newSectPorteria.Semaforo1 = 0;
            }
            var EstadoSem2 = (from a in dbD.DATO_WORDINT where a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "SEMAFORO2.Estado" select a).FirstOrDefault();
            if (EstadoSem2 != null)
            {
                newSectPorteria.Semaforo2 = (int)EstadoSem2.VALOR;
            }
            else
            {
                newSectPorteria.Semaforo2 = 0;
            }
            var Espira = (from o in dbD.DATO_WORDINT where o.ID_SECTOR == oSector.ID_SECTOR && o.TAG == "S_LECTURA.Estado" select o).FirstOrDefault();
            if (Espira != null)
            {
                newSectPorteria.Espira = (int)Espira.VALOR;
            }
            else
            {
                newSectPorteria.Espira = 0;
            }

            ViewBag.inEstadoLector = GetEstadoLectorRFID(idSector);
            ViewBag.oSectorPorteria = newSectPorteria;
            return PartialView();
        }

        public PartialViewResult DinamismoBalanzaT4_(int idSector)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            Entidades.SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();

            SECTOR_BALANZA_UNIDIRECCIONAL newSectBalanza = GetBalanza(oSector);

            Sensor oSensorLectura = new Sensor();
            oSensorLectura.Descripcion = "S_LECTURA";
            oSensorLectura.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensorLectura.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensorLectura.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSensorLectura = oSensorLectura;

            Sensor oSensor1Barrera = new Sensor();
            oSensor1Barrera.Descripcion = "S1_BARRERA";
            oSensor1Barrera.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_BARRERA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor1Barrera.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_BARRERA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor1Barrera.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_BARRERA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSensor1Barrera = oSensor1Barrera;

            Sensor oSensor1Posicion = new Sensor();
            oSensor1Posicion.Descripcion = "S1_POSICION";
            oSensor1Posicion.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_POSICION.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor1Posicion.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_POSICION.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor1Posicion.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_POSICION.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSensor1Posicion = oSensor1Posicion;

            Sensor oSensor2Barrera = new Sensor();
            oSensor2Barrera.Descripcion = "S2_BARRERA";
            oSensor2Barrera.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_BARRERA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor2Barrera.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_BARRERA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor2Barrera.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_BARRERA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSensor2Barrera = oSensor2Barrera;

            Sensor oSensor2Posicion = new Sensor();
            oSensor2Posicion.Descripcion = "S2_POSICION";
            oSensor2Posicion.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_POSICION.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor2Posicion.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_POSICION.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensor2Posicion.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_POSICION.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSensor2Posicion = oSensor2Posicion;

            Barrera oBarrera1 = new Barrera();
            oBarrera1.Descripcion = "BARRERA1";
            oBarrera1.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA1.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oBarrera1.Comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA1.Comando") && a.ID_SECTOR == idSector).FirstOrDefault();
            oBarrera1.HabSenPosicion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA1.HabSenPosicion") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oBarrera1 = oBarrera1;

            Barrera oBarrera2 = new Barrera();
            oBarrera2.Descripcion = "BARRERA2";
            oBarrera2.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA2.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oBarrera2.Comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA2.Comando") && a.ID_SECTOR == idSector).FirstOrDefault();
            oBarrera2.HabSenPosicion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA2.HabSenPosicion") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oBarrera2 = oBarrera2;


            ViewBag.newSectBalanza = newSectBalanza;

            CARTEL_MULTILED oCartel = db.CARTEL_MULTILED.Where(a => a.ID_CARTEL == idSector).FirstOrDefault();
            ViewBag.oCartel = oCartel;

        



            ViewBag.inEstadoLector = GetEstadoLectorRFID(idSector);

            return PartialView();
        }

        public PartialViewResult DinamismoBarreraFalsaT4_(int idSector)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            Entidades.SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            switch (idSector)
            {
                case 430: //Doble barrera
                    Barrera oBarrera1 = new Barrera();
                    oBarrera1.Descripcion = "BARRERA1";
                    oBarrera1.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA1.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oBarrera1.Comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA1.Comando") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oBarrera1.HabSenPosicion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA1.HabSenPosicion") && a.ID_SECTOR == idSector).FirstOrDefault();
                    ViewBag.oBarrera1 = oBarrera1;

                    Sensor oSensor1Barrera = new Sensor();
                    oSensor1Barrera.Descripcion = "S1_BARRERA";
                    oSensor1Barrera.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_BARRERA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oSensor1Barrera.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_BARRERA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oSensor1Barrera.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S1_BARRERA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
                    ViewBag.oSensor1Barrera = oSensor1Barrera;

                    Barrera oBarrera2 = new Barrera();
                    oBarrera2.Descripcion = "BARRERA1";
                    oBarrera2.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA2.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oBarrera2.Comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA2.Comando") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oBarrera2.HabSenPosicion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA2.HabSenPosicion") && a.ID_SECTOR == idSector).FirstOrDefault();
                    ViewBag.oBarrera2 = oBarrera2;

                    Sensor oSensor2Barrera = new Sensor();
                    oSensor2Barrera.Descripcion = "S2_BARRERA";
                    oSensor2Barrera.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_BARRERA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oSensor2Barrera.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_BARRERA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oSensor2Barrera.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S2_BARRERA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
                    ViewBag.oSensor2Barrera = oSensor2Barrera;

                    break;

                case 460: //Barrera simple
                    Barrera oBarrera = new Barrera();
                    oBarrera.Descripcion = "BARRERA";
                    oBarrera.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oBarrera.Comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA.Comando") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oBarrera.HabSenPosicion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("BARRERA.HabSenPosicion") && a.ID_SECTOR == idSector).FirstOrDefault();
                    ViewBag.oBarrera = oBarrera;

                    Sensor oSensorBarrera = new Sensor();
                    oSensorBarrera.Descripcion = "S_BARRERA";
                    oSensorBarrera.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_BARRERA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oSensorBarrera.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_BARRERA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
                    oSensorBarrera.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_BARRERA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
                    ViewBag.oSensorBarrera = oSensorBarrera;

                    break;
            }


            ViewBag.oSector = oSector;


            return PartialView();
        }

        public ActionResult CabezalBalanza(int idSector)
        {
            Entidades.SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            Entidades.SECTOR_BALANZA_UNIDIRECCIONAL oBalanza = GetBalanza(oSector);
            ViewBag.oBalanza = oBalanza;
            ViewBag.oSector = oSector;
            return View();
        }

        public PartialViewResult DinamismoPorteriaSoloLectura_(int idSector)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();

            SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSector = oSector;

            Sensor oSensorLectura = new Sensor();
            oSensorLectura.Descripcion = "S_LECTURA_RFID";
            oSensorLectura.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA_RFID.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensorLectura.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA_RFID.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensorLectura.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA_RFID.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSensorLectura = oSensorLectura;

            return PartialView();
        }

        Entidades.SECTOR_BALANZA_UNIDIRECCIONAL GetBalanza(Entidades.SECTOR oSector)
        {
            Entidades.AxDriverORAEntities dbD = new AxDriverORAEntities();
            Entidades.SECTOR_BALANZA_UNIDIRECCIONAL oBalanza = new SECTOR_BALANZA_UNIDIRECCIONAL();
            oBalanza.NOMBRE = oSector.NOMBRE;
            oBalanza.ID_SECTOR = oSector.ID_SECTOR;
            oBalanza.ID_CONTROL_ACCESO = oSector.ID_CONTROL_ACCESO;
            oBalanza.Descripcion = oSector.DESCRIPCION;

            oBalanza.Estado = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.Estado").FirstOrDefault();
            oBalanza.Comando = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.Comando").FirstOrDefault();
            oBalanza.Peso = dbD.DATO_DWORDDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.Peso").FirstOrDefault();
            oBalanza.PesoMax = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoMax").FirstOrDefault();
            oBalanza.TiempoEstabilidad = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.TiempoEstabilidad").FirstOrDefault();
            oBalanza.DiferenciaEstabilidad = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.DiferenciaEstabilidad").FirstOrDefault();
            oBalanza.LecturaRFID = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.LecturaRFID").FirstOrDefault();
            oBalanza.EstadoOld = dbD.DATO_WORDINT.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.EstadoOld").FirstOrDefault();
            oBalanza.dbPesoNeto = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoNeto").FirstOrDefault();
            oBalanza.dbPesoNegativo = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoNegativo").FirstOrDefault();
            oBalanza.dbFueraRango = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.FueraRango").FirstOrDefault();
            oBalanza.dbFueraEquilibrio = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.FueraEquilibrio").FirstOrDefault();
            oBalanza.dbPesoMayor500 = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoMayor500").FirstOrDefault();
            oBalanza.dbPesoEstable = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoEstable").FirstOrDefault();
            oBalanza.dbPesoMenorDifEstabilidad = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoMenorDifEstabilidad").FirstOrDefault();
            oBalanza.dbPesoMaximoBal = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.PesoMaximoBal").FirstOrDefault();
            oBalanza.dbErrorBal = dbD.DATO_BOOL.Where(a => a.ID_SECTOR == oSector.ID_SECTOR && a.TAG == "BALANZA.ErrorBal").FirstOrDefault();
            return oBalanza;
        }

        public PartialViewResult OpcionesControlBalanza(int idSector)
        {
            Entidades.SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            SECTOR_BALANZA_UNIDIRECCIONAL newSectBalanza = GetBalanza(oSector);
            ViewBag.oSector = oSector;

            return PartialView();
        }

        public PartialViewResult OpcionesControlPorteria(int idSector)
        {
            Entidades.SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            ViewBag.oSector = oSector;

            return PartialView();
        }

        public PartialViewResult BitVidaControl(int idControlAcceso, int BitVida)
        {
            CONTROL_ACCESO oControl = db.CONTROL_ACCESO.Where(a => a.ID_CONTROL_ACCESO == idControlAcceso).FirstOrDefault();
            bool EstadoControl;
            if (BitVida != oControl.BIT_VIDA)
            {
                EstadoControl = true;
            }
            else
            {
                EstadoControl = false;
            }
            ViewBag.EstadoControl = EstadoControl;
            ViewBag.BitVidaActual = BitVida;
            return PartialView();
        }

        public string GetBitVidaJson(int idControl)
        {
            decimal? Bit = db.CONTROL_ACCESO.Where(a => a.ID_CONTROL_ACCESO == idControl).Select(a => a.BIT_VIDA).FirstOrDefault();
            return JsonConvert.SerializeObject(Convert.ToInt16(Bit), Formatting.Indented,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }

        /// <summary>
        /// Accion que devuelve la vista del control de acceso Calado
        /// </summary>
        /// <returns></returns>
        public ActionResult ControlCaladoT3(int idControlAcceso)
        {
            Entidades.CONTROL_ACCESO controlAcceso = db.CONTROL_ACCESO.Find(idControlAcceso);

            return View(controlAcceso);
        }
        /// <summary>
        /// Acción que devuelve la lista de control de acceso de calado
        /// </summary>
        /// <param name="idControlAcceso"></param>
        /// <returns></returns>
        public ActionResult ControlDescargaT5(int idControlAcceso)
        {
            Entidades.CONTROL_ACCESO controlAcceso = db.CONTROL_ACCESO.Find(idControlAcceso);

            return View(controlAcceso);
        }


        /// <summary>
        /// Acción que devuelve el dinamismo del calado
        /// </summary>
        /// <returns></returns>
        public ActionResult DinamismoCaladoT3_(int idSector)
        {

            AxDriverORAEntities dbD = new AxDriverORAEntities();
            ViewBag.oSector = db.SECTOR.Find(idSector);

            Sensor oSensorLectura = new Sensor();
            oSensorLectura.Descripcion = "S_LECTURA";
            oSensorLectura.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensorLectura.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.Habilitacion") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSensorLectura.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.TiempoRet") && a.ID_SECTOR == idSector).FirstOrDefault();

            Semaforo oSemaforo = new Semaforo();
            oSemaforo.comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("SEMAFORO.Comando") && a.ID_SECTOR == idSector).FirstOrDefault();
            oSemaforo.estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("SEMAFORO.Estado") && a.ID_SECTOR == idSector).FirstOrDefault();

            ViewBag.inEstadoLector = GetEstadoLectorRFID(idSector);
            ViewBag.oSensorLectura = oSensorLectura;
            ViewBag.oSemaforo = oSemaforo;

            return View();
        }

        /// <summary>
        /// Acción que devuelve el dinamismo del calado
        /// </summary>
        /// <returns></returns>
        public ActionResult DinamismoDescargaT5(int idSectorIngreso, int idSectorEgreso)
        {

            AxDriverORAEntities dbD = new AxDriverORAEntities();
            ViewBag.oSector = db.SECTOR.Find(idSectorIngreso);

            Sensor oSensorLectura = new Sensor();
            oSensorLectura.Descripcion = "S_LECTURA";
            oSensorLectura.Estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.Estado") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oSensorLectura.Habilitacion = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.Habilitacion") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oSensorLectura.TiempoRet = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("S_LECTURA.TiempoRet") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();

            Semaforo oSemaforo = new Semaforo();
            oSemaforo.comando = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("SEMAFORO.Comando") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oSemaforo.estado = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("SEMAFORO.Estado") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();

            ViewBag.oSemaforo = oSemaforo;

            Descarga oDescarga = new Descarga();

            oDescarga.pesoChofer = dbD.DATO_DWORDDINT.Where(a => a.TAG.Contains("DESCARGA.Peso_Chofer") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oDescarga.estadoPlataforma = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("DESCARGA.EstadoPlataforma") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oDescarga.estadoMotorPrincipal = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("DESCARGA.EstadoMotorPrincipal") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oDescarga.paso = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("DESCARGA.Paso") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();
            oDescarga.pasoOld = dbD.DATO_WORDINT.Where(a => a.TAG.Contains("DESCARGA.PasoOld") && a.ID_SECTOR == idSectorIngreso).FirstOrDefault();

            ViewBag.inEstadoLectorIngreso = GetEstadoLectorRFID(idSectorIngreso);
            ViewBag.inEstadoLectorEgreso = GetEstadoLectorRFID(idSectorEgreso);
            ViewBag.oSensorLectura = oSensorLectura;
            ViewBag.oDescarga = oDescarga;

            return View();
        }

        public ActionResult DinamismoDescargaEgresoT5(int idSector)
        {

            AxDriverORAEntities dbD = new AxDriverORAEntities();
            ViewBag.oSector = db.SECTOR.Find(idSector);

       
            ViewBag.inEstadoLector = GetEstadoLectorRFID(idSector);
          

            return View();
        }

        public ActionResult ConfiguracionDescarga(int idSector)
        {
            AxDriverORAEntities dbD = new AxDriverORAEntities();
            Entidades.SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            Descarga oDescarga = new Descarga();
            oDescarga.stMinPesoChofer = dbD.DATO_DWORDDINT.Where(a => a.TAG.Contains("DESCARGA.SpminPesoChofer") && a.ID_SECTOR == idSector).FirstOrDefault();
            oDescarga.stMaxPesoChofer = dbD.DATO_DWORDDINT.Where(a => a.TAG.Contains("DESCARGA.SpmaxPesoChofer") && a.ID_SECTOR == idSector).FirstOrDefault();

            ViewBag.inEstadoLector = GetEstadoLectorRFID(idSector);
            ViewBag.oDescarga = oDescarga;
            ViewBag.oSector = oSector;
            return View(oDescarga);
        }

        [HttpPost]
        public ActionResult ConfiguracionDescarga(int idSector, Descarga descarga)
        {

            ComandosController cmdController = new ComandosController();
            cmdController.GenerarComandoDword(idSector, (int)descarga.stMinPesoChofer.VALOR, descarga.stMinPesoChofer.TAG);
            cmdController.GenerarComandoDword(idSector, (int)descarga.stMaxPesoChofer.VALOR, descarga.stMaxPesoChofer.TAG);


            return RedirectToAction("Control", new { idControl = 500 });
        }

        /// <summary>
        /// Permite obtener el ID del estado del lector
        /// </summary>
        /// <param name="idSector"></param>
        /// <returns></returns>
        private int GetEstadoLectorRFID(Int32 idSector)
        {
            var oLector = (from a in db.LECTOR_RFID where a.ID_LECTOR_RFID == idSector select a).FirstOrDefault();
            int inEstadoLector = 0;
            if (oLector != null)
            {
                if (oLector.CONECTADO == 0) { inEstadoLector = (int)Constante.IdEstadoLector.desconectado; } //Desconectado
                else if (oLector.CONECTADO == 1 && oLector.LEYENDO == 0)
                {
                    //Busco la notificacion del ult tag leido, si pasó menos de 5 segundos asigno el estado LEYENDO
                    var dtUltimoTagLeido = db.NOTIFICACION.Where(a => a.TIPO_NOTIFICACION == "2")
                                                          .Where(a => a.ID_SECTOR == idSector)
                                                          .Select(a => a.FECHA).OrderByDescending(a => a)
                                                          .FirstOrDefault();
                    if (dtUltimoTagLeido != null)
                    {
                        inEstadoLector = (dtUltimoTagLeido < (DateTime.Now.AddSeconds(5))) ? (int)Constante.IdEstadoLector.sinLectura : (int)Constante.IdEstadoLector.lecturaTAG;
                    }
                    else
                    {
                        inEstadoLector = (int)Constante.IdEstadoLector.sinLectura;
                    }
                }
                else if (oLector.CONECTADO == 1 && oLector.LEYENDO == 1)
                {
                    //Busco la notificacion del ult tag leido, si pasó menos de 5 segundos asigno el estado LEYENDO
                    var dtUltimoTagLeido = db.NOTIFICACION.Where(a => a.TIPO_NOTIFICACION == "2")
                                                         .Where(a => a.ID_SECTOR == idSector)
                                                         .Select(a => a.FECHA).OrderByDescending(a => a)
                                                         .FirstOrDefault();
                    if (dtUltimoTagLeido != null)
                    {
                        inEstadoLector = (dtUltimoTagLeido < (DateTime.Now.AddSeconds(5))) ? (int)Constante.IdEstadoLector.leyendo : (int)Constante.IdEstadoLector.lecturaTAG;
                    }
                    else
                    {
                        inEstadoLector = (int)Constante.IdEstadoLector.leyendo;
                    }

                }
            }
            else
            {
                inEstadoLector = (int)Constante.IdEstadoLector.noExisteLector; //No encuentra el lector en Base de datos
            }

            return inEstadoLector;
        }
    }
}

