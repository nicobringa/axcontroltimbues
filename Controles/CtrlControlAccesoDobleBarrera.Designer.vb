﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlControlAccesoDobleBarrera
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoDobleBarrera))
        Me.CtrlDinamismoDobleBarrera1 = New Controles.CtrlDinamismoDobleBarrera()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnLecturaManualIngreso = New System.Windows.Forms.Button()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.S_BARRERA = New Controles.CtrlSensor()
        Me.S_LECTURA = New Controles.CtrlSensor()
        Me.B_2 = New Controles.CtrlBarrera()
        Me.B_1 = New Controles.CtrlBarrera()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.CAM_EGRESO = New Controles.CtrlCamara()
        Me.CartelLED_EGRESO = New Controles.CtrlCartelMultiLED()
        Me.CtrlDinamismoPorteriaIngreso1 = New Controles.CtrlDinamismoPorteriaIngreso()
        Me.CAM_INGRESO = New Controles.CtrlCamara()
        Me.btnLecturaManualIngreso2 = New System.Windows.Forms.Button()
        Me.S_INGRESO = New Controles.CtrlSensor()
        Me.S_LECTURA_INGRESO = New Controles.CtrlSensor()
        Me.B_INGRESO = New Controles.CtrlBarrera()
        Me.CtrlAntenaLeyendoEgreso = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoIngreso = New Controles.CtrlAntenaLeyendo()
        Me.lblSalida = New System.Windows.Forms.Label()
        Me.lblSeparador = New System.Windows.Forms.Label()
        Me.btnAlarma = New System.Windows.Forms.Button()

        Me.CartelLED_INGRESO = New Controles.CtrlCartelMultiLED()
        Me.btnLectorRfid2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CtrlDinamismoDobleBarrera1
        '
        Me.CtrlDinamismoDobleBarrera1.BackColor = System.Drawing.Color.LightGray
        Me.CtrlDinamismoDobleBarrera1.Location = New System.Drawing.Point(318, 44)
        Me.CtrlDinamismoDobleBarrera1.Name = "CtrlDinamismoDobleBarrera1"
        Me.CtrlDinamismoDobleBarrera1.Size = New System.Drawing.Size(591, 406)
        Me.CtrlDinamismoDobleBarrera1.TabIndex = 0
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, -2)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(1166, 47)
        Me.lblNombre.TabIndex = 164
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(1121, -2)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(45, 45)
        Me.btnAyuda.TabIndex = 163
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(1077, -2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(45, 45)
        Me.btnReset.TabIndex = 162
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnLecturaManualIngreso
        '
        Me.btnLecturaManualIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(164, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.btnLecturaManualIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualIngreso.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualIngreso.Location = New System.Drawing.Point(578, 210)
        Me.btnLecturaManualIngreso.Name = "btnLecturaManualIngreso"
        Me.btnLecturaManualIngreso.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualIngreso.TabIndex = 161
        Me.btnLecturaManualIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualIngreso.UseVisualStyleBackColor = False
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, -2)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 47)
        Me.btnLectorRfid.TabIndex = 155
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'S_BARRERA
        '
        Me.S_BARRERA.Estado = False
        Me.S_BARRERA.GPIO = False
        Me.S_BARRERA.ID_CONTROL_ACCESO = 0
        Me.S_BARRERA.ID_PLC = 0
        Me.S_BARRERA.ID_SECTOR = 0
        Me.S_BARRERA.Inicializado = False
        Me.S_BARRERA.Location = New System.Drawing.Point(807, 372)
        Me.S_BARRERA.Name = "S_BARRERA"
        Me.S_BARRERA.PLC = Nothing
        Me.S_BARRERA.Size = New System.Drawing.Size(81, 45)
        Me.S_BARRERA.TabIndex = 168
        '
        'S_LECTURA
        '
        Me.S_LECTURA.Estado = False
        Me.S_LECTURA.GPIO = False
        Me.S_LECTURA.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA.ID_PLC = 0
        Me.S_LECTURA.ID_SECTOR = 0
        Me.S_LECTURA.Inicializado = False
        Me.S_LECTURA.Location = New System.Drawing.Point(807, 304)
        Me.S_LECTURA.Name = "S_LECTURA"
        Me.S_LECTURA.PLC = Nothing
        Me.S_LECTURA.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA.TabIndex = 167
        '
        'B_2
        '
        Me.B_2.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_2.Estado = False
        Me.B_2.GPIO = False
        Me.B_2.ID_CONTROL_ACCESO = 0
        Me.B_2.ID_PLC = 0
        Me.B_2.ID_SECTOR = 0
        Me.B_2.Inicializado = False
        Me.B_2.Location = New System.Drawing.Point(322, 272)
        Me.B_2.Name = "B_2"
        Me.B_2.PLC = Nothing
        Me.B_2.Size = New System.Drawing.Size(62, 52)
        Me.B_2.TabIndex = 166
        '
        'B_1
        '
        Me.B_1.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_1.Estado = False
        Me.B_1.GPIO = False
        Me.B_1.ID_CONTROL_ACCESO = 0
        Me.B_1.ID_PLC = 0
        Me.B_1.ID_SECTOR = 0
        Me.B_1.Inicializado = False
        Me.B_1.Location = New System.Drawing.Point(777, 246)
        Me.B_1.Name = "B_1"
        Me.B_1.PLC = Nothing
        Me.B_1.Size = New System.Drawing.Size(62, 52)
        Me.B_1.TabIndex = 165
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(910, 42)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(256, 465)
        Me.CtrlPanelNotificaciones1.TabIndex = 169
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 444)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(912, 64)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 170
        '
        'CAM_EGRESO
        '
        Me.CAM_EGRESO.AutoSize = True
        Me.CAM_EGRESO.frmCamara = Nothing
        Me.CAM_EGRESO.GPIO = False
        Me.CAM_EGRESO.ID_CONTROL_ACCESO = 0
        Me.CAM_EGRESO.ID_PLC = 0
        Me.CAM_EGRESO.ID_SECTOR = 0
        Me.CAM_EGRESO.Inicializado = False
        Me.CAM_EGRESO.Location = New System.Drawing.Point(391, 75)
        Me.CAM_EGRESO.Name = "CAM_EGRESO"
        Me.CAM_EGRESO.PLC = Nothing
        Me.CAM_EGRESO.Size = New System.Drawing.Size(83, 60)
        Me.CAM_EGRESO.TabIndex = 171
        '
        'CartelLED_EGRESO
        '
        Me.CartelLED_EGRESO.Conectado = False
        Me.CartelLED_EGRESO.GPIO = False
        Me.CartelLED_EGRESO.ID_CONTROL_ACCESO = 0
        Me.CartelLED_EGRESO.ID_PLC = 0
        Me.CartelLED_EGRESO.ID_SECTOR = 0
        Me.CartelLED_EGRESO.Inicializado = False
        Me.CartelLED_EGRESO.Location = New System.Drawing.Point(541, 74)
        Me.CartelLED_EGRESO.Name = "CartelLED_EGRESO"
        Me.CartelLED_EGRESO.PLC = Nothing
        Me.CartelLED_EGRESO.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_EGRESO.TabIndex = 172
        '
        'CtrlDinamismoPorteriaIngreso1
        '
        Me.CtrlDinamismoPorteriaIngreso1.BackgroundImage = CType(resources.GetObject("CtrlDinamismoPorteriaIngreso1.BackgroundImage"), System.Drawing.Image)
        Me.CtrlDinamismoPorteriaIngreso1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlDinamismoPorteriaIngreso1.GPIO = False
        Me.CtrlDinamismoPorteriaIngreso1.ID_CONTROL_ACCESO = 0
        Me.CtrlDinamismoPorteriaIngreso1.ID_PLC = 0
        Me.CtrlDinamismoPorteriaIngreso1.ID_SECTOR = 0
        Me.CtrlDinamismoPorteriaIngreso1.Inicializado = False
        Me.CtrlDinamismoPorteriaIngreso1.Location = New System.Drawing.Point(0, -45)
        Me.CtrlDinamismoPorteriaIngreso1.Name = "CtrlDinamismoPorteriaIngreso1"
        Me.CtrlDinamismoPorteriaIngreso1.PLC = Nothing
        Me.CtrlDinamismoPorteriaIngreso1.Size = New System.Drawing.Size(299, 490)
        Me.CtrlDinamismoPorteriaIngreso1.TabIndex = 173
        '
        'CAM_INGRESO
        '
        Me.CAM_INGRESO.AutoSize = True
        Me.CAM_INGRESO.frmCamara = Nothing
        Me.CAM_INGRESO.GPIO = False
        Me.CAM_INGRESO.ID_CONTROL_ACCESO = 0
        Me.CAM_INGRESO.ID_PLC = 0
        Me.CAM_INGRESO.ID_SECTOR = 0
        Me.CAM_INGRESO.Inicializado = False
        Me.CAM_INGRESO.Location = New System.Drawing.Point(5, 378)
        Me.CAM_INGRESO.Name = "CAM_INGRESO"
        Me.CAM_INGRESO.PLC = Nothing
        Me.CAM_INGRESO.Size = New System.Drawing.Size(86, 60)
        Me.CAM_INGRESO.TabIndex = 174
        '
        'btnLecturaManualIngreso2
        '
        Me.btnLecturaManualIngreso2.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManualIngreso2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualIngreso2.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualIngreso2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualIngreso2.Location = New System.Drawing.Point(13, 131)
        Me.btnLecturaManualIngreso2.Name = "btnLecturaManualIngreso2"
        Me.btnLecturaManualIngreso2.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualIngreso2.TabIndex = 177
        Me.btnLecturaManualIngreso2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualIngreso2.UseVisualStyleBackColor = False
        '
        'S_INGRESO
        '
        Me.S_INGRESO.Estado = False
        Me.S_INGRESO.GPIO = False
        Me.S_INGRESO.ID_CONTROL_ACCESO = 0
        Me.S_INGRESO.ID_PLC = 0
        Me.S_INGRESO.ID_SECTOR = 0
        Me.S_INGRESO.Inicializado = False
        Me.S_INGRESO.Location = New System.Drawing.Point(212, 347)
        Me.S_INGRESO.Name = "S_INGRESO"
        Me.S_INGRESO.PLC = Nothing
        Me.S_INGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_INGRESO.TabIndex = 181
        '
        'S_LECTURA_INGRESO
        '
        Me.S_LECTURA_INGRESO.Estado = False
        Me.S_LECTURA_INGRESO.GPIO = False
        Me.S_LECTURA_INGRESO.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_INGRESO.ID_PLC = 0
        Me.S_LECTURA_INGRESO.ID_SECTOR = 0
        Me.S_LECTURA_INGRESO.Inicializado = False
        Me.S_LECTURA_INGRESO.Location = New System.Drawing.Point(133, 304)
        Me.S_LECTURA_INGRESO.Name = "S_LECTURA_INGRESO"
        Me.S_LECTURA_INGRESO.PLC = Nothing
        Me.S_LECTURA_INGRESO.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA_INGRESO.TabIndex = 180
        '
        'B_INGRESO
        '
        Me.B_INGRESO.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_INGRESO.Estado = False
        Me.B_INGRESO.GPIO = False
        Me.B_INGRESO.ID_CONTROL_ACCESO = 0
        Me.B_INGRESO.ID_PLC = 0
        Me.B_INGRESO.ID_SECTOR = 0
        Me.B_INGRESO.Inicializado = False
        Me.B_INGRESO.Location = New System.Drawing.Point(0, 246)
        Me.B_INGRESO.Name = "B_INGRESO"
        Me.B_INGRESO.PLC = Nothing
        Me.B_INGRESO.Size = New System.Drawing.Size(62, 52)
        Me.B_INGRESO.TabIndex = 179
        '
        'CtrlAntenaLeyendoEgreso
        '
        Me.CtrlAntenaLeyendoEgreso.Location = New System.Drawing.Point(821, 168)
        Me.CtrlAntenaLeyendoEgreso.Name = "CtrlAntenaLeyendoEgreso"
        Me.CtrlAntenaLeyendoEgreso.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAntenaLeyendoEgreso.TabIndex = 182
        Me.CtrlAntenaLeyendoEgreso.UltTag = Nothing
        '
        'CtrlAntenaLeyendoIngreso
        '
        Me.CtrlAntenaLeyendoIngreso.Location = New System.Drawing.Point(0, 53)
        Me.CtrlAntenaLeyendoIngreso.Name = "CtrlAntenaLeyendoIngreso"
        Me.CtrlAntenaLeyendoIngreso.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAntenaLeyendoIngreso.TabIndex = 183
        Me.CtrlAntenaLeyendoIngreso.UltTag = Nothing
        '
        'lblSalida
        '
        Me.lblSalida.AutoSize = True
        Me.lblSalida.BackColor = System.Drawing.Color.FromArgb(CType(CType(164, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.lblSalida.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalida.ForeColor = System.Drawing.Color.White
        Me.lblSalida.Location = New System.Drawing.Point(535, 176)
        Me.lblSalida.Name = "lblSalida"
        Me.lblSalida.Size = New System.Drawing.Size(136, 31)
        Me.lblSalida.TabIndex = 184
        Me.lblSalida.Text = "EGRESO"
        '
        'lblSeparador
        '
        Me.lblSeparador.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblSeparador.Location = New System.Drawing.Point(299, 44)
        Me.lblSeparador.Name = "lblSeparador"
        Me.lblSeparador.Size = New System.Drawing.Size(19, 401)
        Me.lblSeparador.TabIndex = 200
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(758, 176)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 201
        Me.btnAlarma.UseVisualStyleBackColor = True
        '

        'CartelLED_INGRESO
        '
        Me.CartelLED_INGRESO.Conectado = False
        Me.CartelLED_INGRESO.GPIO = False
        Me.CartelLED_INGRESO.ID_CONTROL_ACCESO = 0
        Me.CartelLED_INGRESO.ID_PLC = 0
        Me.CartelLED_INGRESO.ID_SECTOR = 0
        Me.CartelLED_INGRESO.Inicializado = False
        Me.CartelLED_INGRESO.Location = New System.Drawing.Point(110, 75)
        Me.CartelLED_INGRESO.Name = "CartelLED_INGRESO"
        Me.CartelLED_INGRESO.PLC = Nothing
        Me.CartelLED_INGRESO.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_INGRESO.TabIndex = 203
        '
        'btnLectorRfid2
        '
        Me.btnLectorRfid2.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid2.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid2.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid2.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid2.Location = New System.Drawing.Point(318, -4)
        Me.btnLectorRfid2.Name = "btnLectorRfid2"
        Me.btnLectorRfid2.Size = New System.Drawing.Size(151, 47)
        Me.btnLectorRfid2.TabIndex = 204
        Me.btnLectorRfid2.Text = "Lector RIFID"
        Me.btnLectorRfid2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid2.UseVisualStyleBackColor = False
        '
        'CtrlControlAccesoDobleBarrera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnLectorRfid2)
        Me.Controls.Add(Me.CartelLED_INGRESO)

        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.lblSeparador)
        Me.Controls.Add(Me.lblSalida)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlAntenaLeyendoIngreso)
        Me.Controls.Add(Me.CtrlAntenaLeyendoEgreso)
        Me.Controls.Add(Me.S_INGRESO)
        Me.Controls.Add(Me.S_LECTURA_INGRESO)
        Me.Controls.Add(Me.B_INGRESO)
        Me.Controls.Add(Me.btnLecturaManualIngreso2)
        Me.Controls.Add(Me.CAM_INGRESO)
        Me.Controls.Add(Me.CtrlDinamismoPorteriaIngreso1)
        Me.Controls.Add(Me.CartelLED_EGRESO)
        Me.Controls.Add(Me.CAM_EGRESO)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.S_BARRERA)
        Me.Controls.Add(Me.S_LECTURA)
        Me.Controls.Add(Me.B_2)
        Me.Controls.Add(Me.B_1)
        Me.Controls.Add(Me.btnLecturaManualIngreso)
        Me.Controls.Add(Me.CtrlDinamismoDobleBarrera1)
        Me.Name = "CtrlControlAccesoDobleBarrera"
        Me.Size = New System.Drawing.Size(1165, 508)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlDinamismoDobleBarrera1 As CtrlDinamismoDobleBarrera
    Friend WithEvents lblNombre As Label
    Friend WithEvents btnAyuda As Button
    Friend WithEvents btnReset As Button
    Friend WithEvents btnLecturaManualIngreso As Button
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents S_BARRERA As CtrlSensor
    Friend WithEvents S_LECTURA As CtrlSensor
    Public WithEvents B_2 As CtrlBarrera
    Public WithEvents B_1 As CtrlBarrera
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents CAM_EGRESO As CtrlCamara
    Friend WithEvents CartelLED_EGRESO As CtrlCartelMultiLED
    Friend WithEvents CtrlDinamismoPorteriaIngreso1 As CtrlDinamismoPorteriaIngreso
    Friend WithEvents CAM_INGRESO As CtrlCamara
    Friend WithEvents btnLecturaManualIngreso2 As Button
    Friend WithEvents S_INGRESO As CtrlSensor
    Friend WithEvents S_LECTURA_INGRESO As CtrlSensor
    Public WithEvents B_INGRESO As CtrlBarrera
    Friend WithEvents CtrlAntenaLeyendoEgreso As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoIngreso As CtrlAntenaLeyendo
    Friend WithEvents lblSalida As Label
    Friend WithEvents lblSeparador As Label
    Friend WithEvents btnAlarma As Button

    Friend WithEvents CartelLED_INGRESO As CtrlCartelMultiLED
    Friend WithEvents btnLectorRfid2 As Button
End Class
