﻿
Imports Entidades

''' <summary>
''' Control de impresora 
''' </summary>
Public Class CtrlImpresora


#Region "PROPIEDADES"
    Private _Conectado As Boolean
    Public Property Conectado() As Boolean
        Get
            Return _Conectado
        End Get
        Set(ByVal value As Boolean)
            _Conectado = value
            If Conectado Then
                Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Conectado")
                Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Green)
            Else
                Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Desconectado")
                Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Red)
            End If
        End Set
    End Property

    Private _impresora As Entidades.Impresora

    Public Property oImpresora() As Entidades.Impresora
        Get
            Return _impresora
        End Get
        Set(ByVal value As Entidades.Impresora)
            _impresora = value

        End Set
    End Property

    Private WithEvents oFrmIpresora As frmImpresora
#End Region


#Region "CONSTRUCTOR"
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
#End Region



#Region "METODOS"
    Public Sub Inicializar()
        Try
            Me.lbTAG.Text = Me.Name
            Me.Tag = Me.Name

            Dim nImpresora As New Negocio.ImpresoraN
            oImpresora = nImpresora.GetOneTag(Me.Name)

            If Not IsNothing(oImpresora) Then
                Negocio.ImpresoraSTAR.portName = oImpresora.ip
                Dim udpClient As New Net.Sockets.UdpClient()

                If EstaConectado() Then
                    'udpClient.Connect(oImpresora.ip, oImpresora.port)
                    'udpClient.Close()
                End If
            Else

            End If


        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Metodo que permite saber si hay conexion con la impresora, 
    ''' cuando estamos trabajando con una conexion UDP
    ''' </summary>
    ''' <returns></returns>
    Public function EstaConectado() As Boolean
        'Me.Conectado = My.Computer.Network.Ping(oImpresora.ip)
        'Return Me.Conectado

        If Negocio.ImpresoraSTAR.CheckStarConnection() Then
            Me.Conectado = True
            Return True
        Else
            Me.Conectado = False
            Return False
        End If

    End function

    Public Function Imprimir()

    End Function

    Private Sub btnConf_Click(sender As Object, e As EventArgs) Handles btnConf.Click
        oFrmIpresora = New frmImpresora(oImpresora)
        oFrmIpresora.ShowDialog()
        oFrmIpresora = Nothing
    End Sub
#End Region



#Region "EVENTOS FORM"

#End Region

#Region "SUBPROCESOS"

#End Region


End Class
