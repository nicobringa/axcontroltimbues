//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class NOTIFICACION
    {
        public long ID_NOTIFICACION { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public string MENSAJE { get; set; }
        public string TAG { get; set; }
        public string TIPO_NOTIFICACION { get; set; }
        public Nullable<int> ID_SECTOR { get; set; }
        public Nullable<int> ID_CONTROL_ACCESO { get; set; }
    
        public virtual SECTOR SECTOR { get; set; }
    }
}
