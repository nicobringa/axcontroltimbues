﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlControlAccesoCalado2Calles
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoCalado2Calles))
        Me.CtrlCalado1 = New Controles.CtrlDinamismoCalado()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.S_LECTURA_CALADO2 = New Controles.CtrlSensor()
        Me.S_LECTURA_CALADO1 = New Controles.CtrlSensor()
        Me.CartelLED_CUPO_CALADO1 = New Controles.CtrlCartelMultiLED()
        Me.CtrlAntenaLeyendoCalado2 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAntenaLeyendoCalado1 = New Controles.CtrlAntenaLeyendo()
        Me.CartelLED_CUPO_CALADO2 = New Controles.CtrlCartelMultiLED()
        Me.CAM_CUPO_CALADO1 = New Controles.CtrlCamara()
        Me.CAM_CUPO_CALADO2 = New Controles.CtrlCamara()
        Me.lblSeparador = New System.Windows.Forms.Label()
        Me.CtrlCalado2 = New Controles.CtrlDinamismoCalado()
        Me.btnLecturaManualCalado2 = New System.Windows.Forms.Button()
        Me.btnLecturaManualCalado1 = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CtrlCalado1
        '
        Me.CtrlCalado1.BackgroundImage = CType(resources.GetObject("CtrlCalado1.BackgroundImage"), System.Drawing.Image)
        Me.CtrlCalado1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlCalado1.GPIO = False
        Me.CtrlCalado1.ID_CONTROL_ACCESO = 0
        Me.CtrlCalado1.ID_PLC = 0
        Me.CtrlCalado1.ID_SECTOR = 0
        Me.CtrlCalado1.Inicializado = False
        Me.CtrlCalado1.Location = New System.Drawing.Point(0, 43)
        Me.CtrlCalado1.Name = "CtrlCalado1"
        Me.CtrlCalado1.PLC = Nothing
        Me.CtrlCalado1.Size = New System.Drawing.Size(308, 402)
        Me.CtrlCalado1.TabIndex = 0
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(624, 40)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(226, 465)
        Me.CtrlPanelNotificaciones1.TabIndex = 2
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 443)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(624, 62)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 3
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(151, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(699, 43)
        Me.lblNombre.TabIndex = 166
        Me.lblNombre.Text = "Nombre Calado"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'S_LECTURA_CALADO2
        '
        Me.S_LECTURA_CALADO2.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.S_LECTURA_CALADO2.Estado = False
        Me.S_LECTURA_CALADO2.GPIO = True
        Me.S_LECTURA_CALADO2.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_CALADO2.ID_PLC = 2
        Me.S_LECTURA_CALADO2.ID_SECTOR = 0
        Me.S_LECTURA_CALADO2.Inicializado = False
        Me.S_LECTURA_CALADO2.Location = New System.Drawing.Point(330, 334)
        Me.S_LECTURA_CALADO2.Name = "S_LECTURA_CALADO2"
        Me.S_LECTURA_CALADO2.PLC = Nothing
        Me.S_LECTURA_CALADO2.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_CALADO2.TabIndex = 170
        '
        'S_LECTURA_CALADO1
        '
        Me.S_LECTURA_CALADO1.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.S_LECTURA_CALADO1.Estado = False
        Me.S_LECTURA_CALADO1.GPIO = True
        Me.S_LECTURA_CALADO1.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_CALADO1.ID_PLC = 2
        Me.S_LECTURA_CALADO1.ID_SECTOR = 0
        Me.S_LECTURA_CALADO1.Inicializado = False
        Me.S_LECTURA_CALADO1.Location = New System.Drawing.Point(3, 334)
        Me.S_LECTURA_CALADO1.Name = "S_LECTURA_CALADO1"
        Me.S_LECTURA_CALADO1.PLC = Nothing
        Me.S_LECTURA_CALADO1.Size = New System.Drawing.Size(76, 45)
        Me.S_LECTURA_CALADO1.TabIndex = 171
        '
        'CartelLED_CUPO_CALADO1
        '
        Me.CartelLED_CUPO_CALADO1.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.CartelLED_CUPO_CALADO1.Conectado = False
        Me.CartelLED_CUPO_CALADO1.GPIO = False
        Me.CartelLED_CUPO_CALADO1.ID_CONTROL_ACCESO = 0
        Me.CartelLED_CUPO_CALADO1.ID_PLC = 0
        Me.CartelLED_CUPO_CALADO1.ID_SECTOR = 0
        Me.CartelLED_CUPO_CALADO1.Inicializado = False
        Me.CartelLED_CUPO_CALADO1.Location = New System.Drawing.Point(128, 334)
        Me.CartelLED_CUPO_CALADO1.Name = "CartelLED_CUPO_CALADO1"
        Me.CartelLED_CUPO_CALADO1.PLC = Nothing
        Me.CartelLED_CUPO_CALADO1.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_CUPO_CALADO1.TabIndex = 180
        '
        'CtrlAntenaLeyendoCalado2
        '
        Me.CtrlAntenaLeyendoCalado2.AutoSize = True
        Me.CtrlAntenaLeyendoCalado2.BackColor = System.Drawing.Color.White
        Me.CtrlAntenaLeyendoCalado2.Location = New System.Drawing.Point(539, 93)
        Me.CtrlAntenaLeyendoCalado2.Name = "CtrlAntenaLeyendoCalado2"
        Me.CtrlAntenaLeyendoCalado2.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAntenaLeyendoCalado2.TabIndex = 181
        Me.CtrlAntenaLeyendoCalado2.UltTag = Nothing
        '
        'CtrlAntenaLeyendoCalado1
        '
        Me.CtrlAntenaLeyendoCalado1.AutoSize = True
        Me.CtrlAntenaLeyendoCalado1.BackColor = System.Drawing.Color.White
        Me.CtrlAntenaLeyendoCalado1.Location = New System.Drawing.Point(220, 90)
        Me.CtrlAntenaLeyendoCalado1.Name = "CtrlAntenaLeyendoCalado1"
        Me.CtrlAntenaLeyendoCalado1.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAntenaLeyendoCalado1.TabIndex = 182
        Me.CtrlAntenaLeyendoCalado1.UltTag = Nothing
        '
        'CartelLED_CUPO_CALADO2
        '
        Me.CartelLED_CUPO_CALADO2.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.CartelLED_CUPO_CALADO2.Conectado = False
        Me.CartelLED_CUPO_CALADO2.GPIO = False
        Me.CartelLED_CUPO_CALADO2.ID_CONTROL_ACCESO = 0
        Me.CartelLED_CUPO_CALADO2.ID_PLC = 0
        Me.CartelLED_CUPO_CALADO2.ID_SECTOR = 0
        Me.CartelLED_CUPO_CALADO2.Inicializado = False
        Me.CartelLED_CUPO_CALADO2.Location = New System.Drawing.Point(446, 334)
        Me.CartelLED_CUPO_CALADO2.Name = "CartelLED_CUPO_CALADO2"
        Me.CartelLED_CUPO_CALADO2.PLC = Nothing
        Me.CartelLED_CUPO_CALADO2.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_CUPO_CALADO2.TabIndex = 185
        '
        'CAM_CUPO_CALADO1
        '
        Me.CAM_CUPO_CALADO1.AutoSize = True
        Me.CAM_CUPO_CALADO1.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_CUPO_CALADO1.frmCamara = Nothing
        Me.CAM_CUPO_CALADO1.GPIO = False
        Me.CAM_CUPO_CALADO1.ID_CONTROL_ACCESO = 0
        Me.CAM_CUPO_CALADO1.ID_PLC = 0
        Me.CAM_CUPO_CALADO1.ID_SECTOR = 0
        Me.CAM_CUPO_CALADO1.Inicializado = False
        Me.CAM_CUPO_CALADO1.Location = New System.Drawing.Point(229, 165)
        Me.CAM_CUPO_CALADO1.Name = "CAM_CUPO_CALADO1"
        Me.CAM_CUPO_CALADO1.PLC = Nothing
        Me.CAM_CUPO_CALADO1.Size = New System.Drawing.Size(78, 60)
        Me.CAM_CUPO_CALADO1.TabIndex = 186
        '
        'CAM_CUPO_CALADO2
        '
        Me.CAM_CUPO_CALADO2.AutoSize = True
        Me.CAM_CUPO_CALADO2.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_CUPO_CALADO2.frmCamara = Nothing
        Me.CAM_CUPO_CALADO2.GPIO = False
        Me.CAM_CUPO_CALADO2.ID_CONTROL_ACCESO = 0
        Me.CAM_CUPO_CALADO2.ID_PLC = 0
        Me.CAM_CUPO_CALADO2.ID_SECTOR = 0
        Me.CAM_CUPO_CALADO2.Inicializado = False
        Me.CAM_CUPO_CALADO2.Location = New System.Drawing.Point(539, 168)
        Me.CAM_CUPO_CALADO2.Name = "CAM_CUPO_CALADO2"
        Me.CAM_CUPO_CALADO2.PLC = Nothing
        Me.CAM_CUPO_CALADO2.Size = New System.Drawing.Size(78, 60)
        Me.CAM_CUPO_CALADO2.TabIndex = 187
        '
        'lblSeparador
        '
        Me.lblSeparador.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblSeparador.Location = New System.Drawing.Point(305, 43)
        Me.lblSeparador.Name = "lblSeparador"
        Me.lblSeparador.Size = New System.Drawing.Size(19, 397)
        Me.lblSeparador.TabIndex = 189
        '
        'CtrlCalado2
        '
        Me.CtrlCalado2.BackgroundImage = CType(resources.GetObject("CtrlCalado2.BackgroundImage"), System.Drawing.Image)
        Me.CtrlCalado2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlCalado2.GPIO = False
        Me.CtrlCalado2.ID_CONTROL_ACCESO = 0
        Me.CtrlCalado2.ID_PLC = 0
        Me.CtrlCalado2.ID_SECTOR = 0
        Me.CtrlCalado2.Inicializado = False
        Me.CtrlCalado2.Location = New System.Drawing.Point(319, 43)
        Me.CtrlCalado2.Name = "CtrlCalado2"
        Me.CtrlCalado2.PLC = Nothing
        Me.CtrlCalado2.Size = New System.Drawing.Size(308, 402)
        Me.CtrlCalado2.TabIndex = 190
        '
        'btnLecturaManualCalado2
        '
        Me.btnLecturaManualCalado2.BackColor = System.Drawing.Color.White
        Me.btnLecturaManualCalado2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualCalado2.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualCalado2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualCalado2.Location = New System.Drawing.Point(435, 200)
        Me.btnLecturaManualCalado2.Name = "btnLecturaManualCalado2"
        Me.btnLecturaManualCalado2.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualCalado2.TabIndex = 188
        Me.btnLecturaManualCalado2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualCalado2.UseVisualStyleBackColor = False
        '
        'btnLecturaManualCalado1
        '
        Me.btnLecturaManualCalado1.BackColor = System.Drawing.Color.White
        Me.btnLecturaManualCalado1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualCalado1.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualCalado1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualCalado1.Location = New System.Drawing.Point(105, 190)
        Me.btnLecturaManualCalado1.Name = "btnLecturaManualCalado1"
        Me.btnLecturaManualCalado1.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualCalado1.TabIndex = 184
        Me.btnLecturaManualCalado1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualCalado1.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(536, 42)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 160
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(578, 42)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(43, 43)
        Me.btnAyuda.TabIndex = 158
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, -1)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 157
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(478, 43)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 194
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CtrlControlAccesoCalado2Calles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.lblSeparador)
        Me.Controls.Add(Me.btnLecturaManualCalado2)
        Me.Controls.Add(Me.CAM_CUPO_CALADO2)
        Me.Controls.Add(Me.CAM_CUPO_CALADO1)
        Me.Controls.Add(Me.CartelLED_CUPO_CALADO2)
        Me.Controls.Add(Me.btnLecturaManualCalado1)
        Me.Controls.Add(Me.CtrlAntenaLeyendoCalado1)
        Me.Controls.Add(Me.CtrlAntenaLeyendoCalado2)
        Me.Controls.Add(Me.CartelLED_CUPO_CALADO1)
        Me.Controls.Add(Me.S_LECTURA_CALADO1)
        Me.Controls.Add(Me.S_LECTURA_CALADO2)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.CtrlCalado1)
        Me.Controls.Add(Me.CtrlCalado2)
        Me.Name = "CtrlControlAccesoCalado2Calles"
        Me.Size = New System.Drawing.Size(849, 505)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlCalado1 As CtrlDinamismoCalado
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents btnReset As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents btnLectorRfid As Button
    Protected WithEvents lblNombre As Label
    Friend WithEvents S_LECTURA_CALADO2 As CtrlSensor
    Friend WithEvents S_LECTURA_CALADO1 As CtrlSensor
    Friend WithEvents CartelLED_CUPO_CALADO1 As CtrlCartelMultiLED
    Friend WithEvents CtrlAntenaLeyendoCalado2 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAntenaLeyendoCalado1 As CtrlAntenaLeyendo
    Friend WithEvents btnLecturaManualCalado1 As Button
    Friend WithEvents CartelLED_CUPO_CALADO2 As CtrlCartelMultiLED
    Friend WithEvents CAM_CUPO_CALADO1 As CtrlCamara
    Friend WithEvents CAM_CUPO_CALADO2 As CtrlCamara
    Friend WithEvents btnLecturaManualCalado2 As Button
    Friend WithEvents lblSeparador As Label
    Friend WithEvents CtrlCalado2 As CtrlDinamismoCalado
    Friend WithEvents btnAlarma As Button
End Class
