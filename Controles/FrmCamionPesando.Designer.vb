﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCAMION_PESANDO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCAMION_PESANDO))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtFecha = New System.Windows.Forms.DateTimePicker()
        Me.cbPesoEnviado = New System.Windows.Forms.CheckBox()
        Me.gbAxRespuesta = New System.Windows.Forms.GroupBox()
        Me.txtMensaje = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtIdTransaccion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbHabilitado = New System.Windows.Forms.CheckBox()
        Me.txtIdSector = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTagRFID = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbAxRespuesta.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        ' Me.PictureBox1.Image = Global.Controles.My.Resources.Resources.CAMION_PESANDO32
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 25)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(8, 47)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 20)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "Fecha:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtFecha
        '
        Me.dtFecha.CustomFormat = "dd/MM/yyyy hh:mm:ss"
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFecha.Location = New System.Drawing.Point(68, 47)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(127, 20)
        Me.dtFecha.TabIndex = 73
        '
        'cbPesoEnviado
        '
        Me.cbPesoEnviado.AutoSize = True
        Me.cbPesoEnviado.Enabled = False
        Me.cbPesoEnviado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPesoEnviado.Location = New System.Drawing.Point(12, 73)
        Me.cbPesoEnviado.Name = "cbPesoEnviado"
        Me.cbPesoEnviado.Size = New System.Drawing.Size(112, 20)
        Me.cbPesoEnviado.TabIndex = 77
        Me.cbPesoEnviado.Text = "Peso Enviado"
        Me.cbPesoEnviado.UseVisualStyleBackColor = True
        '
        'gbAxRespuesta
        '
        Me.gbAxRespuesta.Controls.Add(Me.txtMensaje)
        Me.gbAxRespuesta.Controls.Add(Me.Label5)
        Me.gbAxRespuesta.Controls.Add(Me.txtIdTransaccion)
        Me.gbAxRespuesta.Controls.Add(Me.Label2)
        Me.gbAxRespuesta.Controls.Add(Me.cbHabilitado)
        Me.gbAxRespuesta.Controls.Add(Me.txtIdSector)
        Me.gbAxRespuesta.Controls.Add(Me.Label4)
        Me.gbAxRespuesta.Location = New System.Drawing.Point(12, 99)
        Me.gbAxRespuesta.Name = "gbAxRespuesta"
        Me.gbAxRespuesta.Size = New System.Drawing.Size(290, 128)
        Me.gbAxRespuesta.TabIndex = 78
        Me.gbAxRespuesta.TabStop = False
        Me.gbAxRespuesta.Text = "AxRespuesta Esta Habilitado"
        '
        'txtMensaje
        '
        Me.txtMensaje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMensaje.Location = New System.Drawing.Point(85, 77)
        Me.txtMensaje.Multiline = True
        Me.txtMensaje.Name = "txtMensaje"
        Me.txtMensaje.ReadOnly = True
        Me.txtMensaje.Size = New System.Drawing.Size(199, 45)
        Me.txtMensaje.TabIndex = 84
        Me.txtMensaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 74)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 20)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "Mensaje:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtIdTransaccion
        '
        Me.txtIdTransaccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdTransaccion.Location = New System.Drawing.Point(128, 47)
        Me.txtIdTransaccion.Name = "txtIdTransaccion"
        Me.txtIdTransaccion.ReadOnly = True
        Me.txtIdTransaccion.Size = New System.Drawing.Size(156, 20)
        Me.txtIdTransaccion.TabIndex = 82
        Me.txtIdTransaccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(6, 47)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 20)
        Me.Label2.TabIndex = 81
        Me.Label2.Text = "ID Transacción:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbHabilitado
        '
        Me.cbHabilitado.AutoSize = True
        Me.cbHabilitado.Enabled = False
        Me.cbHabilitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHabilitado.Location = New System.Drawing.Point(164, 17)
        Me.cbHabilitado.Name = "cbHabilitado"
        Me.cbHabilitado.Size = New System.Drawing.Size(89, 20)
        Me.cbHabilitado.TabIndex = 80
        Me.cbHabilitado.Text = "Habilitado"
        Me.cbHabilitado.UseVisualStyleBackColor = True
        '
        'txtIdSector
        '
        Me.txtIdSector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdSector.Location = New System.Drawing.Point(89, 17)
        Me.txtIdSector.Name = "txtIdSector"
        Me.txtIdSector.ReadOnly = True
        Me.txtIdSector.Size = New System.Drawing.Size(69, 20)
        Me.txtIdSector.TabIndex = 79
        Me.txtIdSector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(6, 17)
        Me.Label4.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 20)
        Me.Label4.TabIndex = 79
        Me.Label4.Text = "ID Sector:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTagRFID
        '
        Me.txtTagRFID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTagRFID.Location = New System.Drawing.Point(144, 17)
        Me.txtTagRFID.Name = "txtTagRFID"
        Me.txtTagRFID.ReadOnly = True
        Me.txtTagRFID.Size = New System.Drawing.Size(158, 20)
        Me.txtTagRFID.TabIndex = 80
        Me.txtTagRFID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(55, 17)
        Me.Label3.Margin = New System.Windows.Forms.Padding(3, 0, 3, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 20)
        Me.Label3.TabIndex = 79
        Me.Label3.Text = "Tag RFID:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.ForeColor = System.Drawing.Color.White
        Me.btnAceptar.Location = New System.Drawing.Point(48, 233)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(196, 31)
        Me.btnAceptar.TabIndex = 81
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'FrmCAMION_PESANDO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(316, 274)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtTagRFID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.gbAxRespuesta)
        Me.Controls.Add(Me.cbPesoEnviado)
        Me.Controls.Add(Me.dtFecha)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmCAMION_PESANDO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Camión Pesando"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbAxRespuesta.ResumeLayout(False)
        Me.gbAxRespuesta.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbPesoEnviado As System.Windows.Forms.CheckBox
    Friend WithEvents gbAxRespuesta As System.Windows.Forms.GroupBox
    Friend WithEvents txtMensaje As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtIdTransaccion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbHabilitado As System.Windows.Forms.CheckBox
    Friend WithEvents txtIdSector As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTagRFID As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class
