﻿Imports Impinj.OctaneSdk
Public Class CtrlConfRFID

    Private _ConfRFID As Entidades.Config_Lector_RFID


    Public Sub New(ByVal oConf As Entidades.Config_Lector_RFID)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        _ConfRFID = oConf
        RefrescarConf()


    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()



    End Sub







#Region "Eventos"
    Private Sub txtNombre_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.Leave
        _ConfRFID.Nombre = txtNombre.Text
    End Sub

    Private Sub cbxAntena_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxAntena1.CheckedChanged, cbxAntena2.CheckedChanged, cbxAntena3.CheckedChanged, cbxAntena4.CheckedChanged

        Dim cbx As CheckBox = CType(sender, CheckBox)

        Select Case cbx.Name

            Case "cbxAntena1"
                If cbxAntena1.Checked Then
                    txtPowerAnt1.Enabled = True
                    txtSensitivityAnt1.Enabled = True
                    txtObserAnt1.Enabled = True
                Else
                    txtPowerAnt1.Enabled = False
                    txtSensitivityAnt1.Enabled = False
                    txtObserAnt1.Enabled = False
                    DeshabilitarAnt(1)
                End If
            Case "cbxAntena2"
                If cbxAntena2.Checked Then
                    txtPowerAnt2.Enabled = True
                    txtSensitivityAnt2.Enabled = True
                    txtObserAnt2.Enabled = True

                Else
                    txtPowerAnt2.Enabled = False
                    txtSensitivityAnt2.Enabled = False
                    txtObserAnt2.Enabled = False
                    DeshabilitarAnt(2)
                End If
            Case "cbxAntena3"
                If cbxAntena3.Checked Then
                    txtPowerAnt3.Enabled = True
                    txtSensitivityAnt3.Enabled = True
                    txtObserAnt3.Enabled = True
                Else
                    txtPowerAnt3.Enabled = False
                    txtSensitivityAnt3.Enabled = False
                    txtObserAnt3.Enabled = False
                    DeshabilitarAnt(3)
                End If
            Case "cbxAntena4"
                If cbxAntena4.Checked Then
                    txtPowerAnt4.Enabled = True
                    txtSensitivityAnt4.Enabled = True
                    txtObserAnt4.Enabled = True
                Else
                    txtPowerAnt4.Enabled = False
                    txtSensitivityAnt4.Enabled = False
                    txtObserAnt4.Enabled = False
                    DeshabilitarAnt(4)
                End If

        End Select




    End Sub

    Private Sub cbo_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReaderMode.SelectionChangeCommitted, cboSearchMode.SelectionChangeCommitted, cboSession.SelectionChangeCommitted, cboReportMode.SelectionChangeCommitted
        Dim combo As ComboBox = CType(sender, ComboBox)

        Select Case combo.Name
            Case "cboReaderMode"
                Select Case cboReaderMode.SelectedIndex
                    Case 0
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.AutoSetDenseReader
                    Case 1
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.AutoSetSingleReader
                    Case 2
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.MaxThroughput
                    Case 3
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.Hybrid
                    Case 4
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.DenseReaderM4
                    Case 5
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.DenseReaderM8
                    Case 6
                        _ConfRFID.SELECT_READER_MODE = ReaderMode.MaxMiller

                End Select
            Case "cboSearchMode"
                _ConfRFID.SELECT_SEARCH = cboSearchMode.SelectedValue
                Select Case cboSearchMode.SelectedIndex
                    Case 0
                        _ConfRFID.SELECT_SEARCH = SearchMode.DualTarget
                    Case 1
                        _ConfRFID.SELECT_SEARCH = SearchMode.SingleTarget
                    Case 2
                        _ConfRFID.SELECT_SEARCH = SearchMode.TagFocus
                End Select
            Case "cboSession"
                _ConfRFID.SELECT_SESSION = cboSession.SelectedIndex
            Case "cboReportMode"
                Select Case cboReportMode.SelectedIndex
                    Case 0
                        _ConfRFID.SELECT_REPORT_MODE = ReportMode.WaitForQuery
                    Case 1
                        _ConfRFID.SELECT_REPORT_MODE = ReportMode.Individual
                    Case 2
                        _ConfRFID.SELECT_REPORT_MODE = ReportMode.BatchAfterStop
                End Select
        End Select
    End Sub

    Private Sub txtAnt_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPowerAnt1.Leave, txtSensitivityAnt1.Leave, txtObserAnt1.Leave, txtPowerAnt2.Leave, txtSensitivityAnt2.Leave, txtObserAnt2.Leave, txtPowerAnt3.Leave, txtSensitivityAnt3.Leave, txtObserAnt3.Leave, txtPowerAnt4.Leave, txtSensitivityAnt4.Leave, txtObserAnt4.Leave

        Dim txt As TextBox = CType(sender, TextBox)

        If txt.Name = txtPowerAnt1.Name Or txt.Name = txtSensitivityAnt1.Name Or txt.Name = txtObserAnt1.Name Then 'Ant1
            SetConfAntena(1, txtPowerAnt1.Text, txtSensitivityAnt1.Text, txtObserAnt1.Text)

        ElseIf txt.Name = txtPowerAnt2.Name Or txt.Name = txtSensitivityAnt2.Name Or txt.Name = txtObserAnt2.Name Then 'Ant2
            SetConfAntena(2, txtPowerAnt2.Text, txtSensitivityAnt2.Text, txtObserAnt2.Text)
        ElseIf txt.Name = txtPowerAnt3.Name Or txt.Name = txtSensitivityAnt3.Name Or txt.Name = txtObserAnt3.Name Then 'Ant3
            SetConfAntena(3, txtPowerAnt3.Text, txtSensitivityAnt3.Text, txtObserAnt3.Text)
        Else 'Ant4
            SetConfAntena(4, txtPowerAnt4.Text, txtSensitivityAnt4.Text, txtObserAnt4.Text)

        End If

        Select Case txt.Text
            Case txtPowerAnt1.Name

            Case txtPowerAnt2.Name

            Case txtPowerAnt3.Name

            Case txtPowerAnt4.Name

        End Select


    End Sub
    Private Sub txtRssi_Leave(sender As Object, e As EventArgs) Handles txtRssi.Leave
        Dim out As Integer
        If Integer.TryParse(txtRssi.Text, out) Then
            Me._ConfRFID.RSSI = out
        End If

    End Sub


#End Region

#Region "Metodos"

    ''' <summary>
    ''' Crea el objeto antena o lo modifica 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetConfAntena(ByVal NumAnt As Integer, ByVal power As String, ByVal sensivity As String, ByVal observacion As String)
        Dim HabilitarAnt As Boolean = True
        Dim _sensivity As String = If(sensivity = "", 0, sensivity)
        Dim _Power As String = If(power = "", 0, power)

        For Each Ant As Entidades.ANTENAS_RFID In _ConfRFID.ANTENAS_RFID
            If Ant.NUM_ANTENA = NumAnt Then 'Si ya esta creada o Habilitada modifico el objeto
                Ant.TX_POWER = _Power
                Ant.RX_SENSITIVITY = _sensivity
                Ant.OBSERVACION = observacion
                HabilitarAnt = False
            End If

        Next

        If HabilitarAnt Then ' si no lo encuentra habilita la antena
            Dim Antena As New Entidades.ANTENAS_RFID
            Antena.NUM_ANTENA = NumAnt
            Antena.TX_POWER = _Power
            Antena.RX_SENSITIVITY = _sensivity
            Antena.OBSERVACION = observacion

            'Agrega la antena al lector
            _ConfRFID.ANTENAS_RFID.Add(Antena)

        End If
    End Sub

    Private Sub DeshabilitarAnt(ByVal NumAnt As Integer)
        For Each Ant As Entidades.ANTENAS_RFID In _ConfRFID.ANTENAS_RFID
            If Ant.NUM_ANTENA = NumAnt Then
                _ConfRFID.ANTENAS_RFID.Remove(Ant)
                Exit For
            End If
        Next
    End Sub

    Private Sub PosicionarCombos(ByVal cbo As ComboBox, ByVal value As Integer)

        Select Case cbo.Name
            Case cboReaderMode.Name
                Select Case value
                    Case ReaderMode.AutoSetDenseReader
                        cboReaderMode.SelectedIndex = 0
                    Case ReaderMode.AutoSetSingleReader
                        cboReaderMode.SelectedIndex = 1
                    Case ReaderMode.MaxThroughput
                        cboReaderMode.SelectedIndex = 2
                    Case ReaderMode.Hybrid
                        cboReaderMode.SelectedIndex = 3
                    Case ReaderMode.DenseReaderM4
                        cboReaderMode.SelectedIndex = 4
                    Case ReaderMode.DenseReaderM8
                        cboReaderMode.SelectedIndex = 5
                    Case ReaderMode.MaxMiller
                        cboReaderMode.SelectedIndex = 6
                    Case Else
                        'no tiene configuracion por el usuario por defecto se muestra la primera opcion
                        cboReaderMode.SelectedIndex = 0
                End Select
            Case cboReportMode.Name
                Select Case value
                    Case ReportMode.WaitForQuery
                        cboReportMode.SelectedIndex = 0
                    Case ReportMode.Individual
                        cboReportMode.SelectedIndex = 1
                    Case ReportMode.BatchAfterStop
                        cboReportMode.SelectedIndex = 2
                End Select
            Case cboSearchMode.Name
                Select Case value
                    Case SearchMode.DualTarget
                        cboSearchMode.SelectedIndex = 0
                    Case SearchMode.SingleTarget
                        cboSearchMode.SelectedIndex = 1
                    Case SearchMode.TagFocus
                        cboSearchMode.SelectedIndex = 2
                End Select
            Case cboSession.Name
                cboSession.SelectedIndex = value


        End Select


    End Sub

    ''' <summary>
    ''' Muesta las configuracion establecida
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RefrescarConf()
        txtNombre.Text = _ConfRFID.NOMBRE
        PosicionarCombos(cboReaderMode, _ConfRFID.SELECT_READER_MODE)
        PosicionarCombos(cboReportMode, _ConfRFID.SELECT_REPORT_MODE)
        PosicionarCombos(cboSearchMode, _ConfRFID.SELECT_SEARCH)
        PosicionarCombos(cboSession, _ConfRFID.SELECT_SESSION)

        For Each ant As Entidades.ANTENAS_RFID In _ConfRFID.ANTENAS_RFID
            If ant.NUM_ANTENA = 1 Then
                cbxAntena1.Checked = True
                txtPowerAnt1.Text = ant.TX_POWER
                txtSensitivityAnt1.Text = ant.RX_SENSITIVITY
                txtObserAnt1.Text = ant.OBSERVACION
            ElseIf ant.NUM_ANTENA = 2 Then
                cbxAntena2.Checked = True
                txtPowerAnt2.Text = ant.TX_POWER
                txtSensitivityAnt2.Text = ant.RX_SENSITIVITY
                txtObserAnt2.Text = ant.OBSERVACION
            ElseIf ant.NUM_ANTENA = 3 Then
                cbxAntena3.Checked = True
                txtPowerAnt3.Text = ant.TX_POWER
                txtSensitivityAnt3.Text = ant.RX_SENSITIVITY
                txtObserAnt3.Text = ant.OBSERVACION
            ElseIf ant.NUM_ANTENA = 4 Then
                cbxAntena4.Checked = True
                txtPowerAnt4.Text = ant.TX_POWER
                txtSensitivityAnt4.Text = ant.RX_SENSITIVITY
                txtObserAnt4.Text = ant.OBSERVACION
            End If
        Next

        cbxGpioHabilitado.Checked = _ConfRFID.GPIO_HABILITADO
        gbxGPIO.Enabled = _ConfRFID.GPIO_HABILITADO
        cbxGpioPuerto1.Checked = _ConfRFID.GPIO_PUERTO1
        cbxGpioPuerto2.Checked = _ConfRFID.GPIO_PUERTO2
        cbxGpioPuerto3.Checked = _ConfRFID.GPIO_PUERTO3
        cbxGpioPuerto4.Checked = _ConfRFID.GPIO_PUERTO4

        txtRssi.Text = _ConfRFID.RSSI

    End Sub

    ''' <summary>
    ''' Devuelve la lista de antenas habilitadas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ConfigAntenas() As List(Of Entidades.ANTENAS_RFID)
        _ConfRFID.ANTENAS_RFID.Clear()

        Dim ListAntenas As New List(Of Entidades.ANTENAS_RFID)

        If cbxAntena1.Checked Then ' Habilitado la antena 1
            Dim Antena1 As New Entidades.ANTENAS_RFID
            'Lo guardo en el objeto que se va guardar en el xml
            Antena1.NUM_ANTENA = 1
            Antena1.TX_POWER = txtPowerAnt1.Text
            Antena1.RX_SENSITIVITY = txtSensitivityAnt1.Text
            Antena1.OBSERVACION = txtNombre.Text

            'Agrego la antena a la coleccion
            ListAntenas.Add(Antena1)
            'ListAntenas.Add(Antena1)
        End If

        If cbxAntena2.Checked Then ' Habilitado la antena 2
            Dim Antena2 As New Entidades.ANTENAS_RFID
            'Lo guardo en el objeto que se va guardar en el xml
            Antena2.NUM_ANTENA = 2
            Antena2.TX_POWER = txtPowerAnt2.Text
            Antena2.RX_SENSITIVITY = txtSensitivityAnt2.Text
            Antena2.OBSERVACION = txtObserAnt2.Text

            'Agrego la antena a la coleccion
            ListAntenas.Add(Antena2)
        End If

        If cbxAntena3.Checked Then ' Habilitado la antena 3
            Dim Antena3 As New Entidades.ANTENAS_RFID

            Antena3.NUM_ANTENA = 3
            Antena3.TX_POWER = txtPowerAnt3.Text
            Antena3.RX_SENSITIVITY = txtSensitivityAnt3.Text
            Antena3.OBSERVACION = txtObserAnt3.Text

            'Agrego la antena a la coleccion
            ListAntenas.Add(Antena3)

        End If

        If cbxAntena4.Checked Then ' Habilitado la antena 4
            Dim Antena4 As New Entidades.ANTENAS_RFID

            Antena4.NUM_ANTENA = 4
            Antena4.TX_POWER = txtPowerAnt4.Text
            Antena4.RX_SENSITIVITY = txtSensitivityAnt4.Text
            Antena4.OBSERVACION = txtObserAnt4.Text

            'Agrego la antena a la coleccion
            ListAntenas.Add(Antena4)
        End If


        Return ListAntenas
    End Function

    Private Sub CheckBoxGpio_CheckedChanged(sender As Object, e As EventArgs) Handles cbxGpioPuerto1.CheckedChanged, cbxGpioPuerto2.CheckedChanged, cbxGpioPuerto3.CheckedChanged, cbxGpioPuerto4.CheckedChanged, cbxGpioHabilitado.CheckedChanged

        Dim cbx As CheckBox = CType(sender, CheckBox)

        Select Case cbx.Name
            Case cbxGpioPuerto1.Name
                _ConfRFID.GPIO_PUERTO1 = cbxGpioPuerto1.Checked
            Case cbxGpioPuerto2.Name
                _ConfRFID.GPIO_PUERTO2 = cbxGpioPuerto2.Checked
            Case cbxGpioPuerto3.Name
                _ConfRFID.GPIO_PUERTO3 = cbxGpioPuerto3.Checked
            Case cbxGpioPuerto4.Name
                _ConfRFID.GPIO_PUERTO4 = cbxGpioPuerto4.Checked
            Case cbxGpioHabilitado.Name
                gbxGPIO.Enabled = cbxGpioHabilitado.Checked
                _ConfRFID.GPIO_HABILITADO = cbxGpioHabilitado.Checked
        End Select




    End Sub


#End Region

    'Private _SelectReaderMode As Integer
    'Public Property SELECT_READER_MODE() As Integer
    '    Get
    '        Return _SelectReaderMode
    '    End Get
    '    Set(ByVal value As Integer)
    '        _SelectReaderMode = value
    '    End Set
    'End Property
    'Private _SelectSeachMode As Integer
    'Public Property SELECT_SEARCH() As Integer
    '    Get
    '        Return _SelectSeachMode
    '    End Get
    '    Set(ByVal value As Integer)
    '        _SelectSeachMode = value
    '    End Set
    'End Property
    'Private _SelectSession As Integer
    'Public Property SelectSession() As Integer
    '    Get
    '        Return _SelectSession
    '    End Get
    '    Set(ByVal value As Integer)
    '        _SelectSession = value
    '    End Set
    'End Property
    'Private _IsStart As Boolean = False
    'Public Property IsStart() As Boolean
    '    Get
    '        Return _IsStart
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _IsStart = value
    '    End Set
    'End Property

    ' ''' <summary>
    ' ''' Si tiene un configuracion definida por el usuario
    ' ''' True - Configuracion definida por el usuario
    ' ''' False- no tiene configuracion definida por el usuario , tiene la configuracion por defecto
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private _TieneCofigUser As Boolean
    'Public Property TieneConfigUser() As Boolean
    '    Get
    '        Return _TieneCofigUser
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _TieneCofigUser = value
    '    End Set
    'End Property

    ' ''' <summary>
    ' ''' OBbtiene o establece el reporte seleccionado , para luego guardarlo en el archivo XML
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private _SelectReportMode As Integer
    'Public Property SELECT_REPORT_MODE() As Integer
    '    Get
    '        Return _SelectReportMode
    '    End Get
    '    Set(ByVal value As Integer)
    '        _SelectReportMode = value
    '    End Set
    'End Property







End Class
