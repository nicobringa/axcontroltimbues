﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoBalanza1
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.picS1LecturaNoCortando = New System.Windows.Forms.PictureBox()
        Me.picS1LecturaCortando = New System.Windows.Forms.PictureBox()
        Me.picS1PosicionNoCortando = New System.Windows.Forms.PictureBox()
        Me.picS1PosicionCortando = New System.Windows.Forms.PictureBox()
        Me.picS2LecturaNoCortando = New System.Windows.Forms.PictureBox()
        Me.picS2LecturaCortando = New System.Windows.Forms.PictureBox()
        Me.picS2PosicionNoCortando = New System.Windows.Forms.PictureBox()
        Me.picS2PosicionCortando = New System.Windows.Forms.PictureBox()
        Me.picBAR1_ABAJO = New System.Windows.Forms.PictureBox()
        Me.picBAR2_ABAJO = New System.Windows.Forms.PictureBox()
        Me.picBAR2_ARRIBA = New System.Windows.Forms.PictureBox()
        Me.picBAR1_ARRIBA = New System.Windows.Forms.PictureBox()
        Me.lblFalla_BAR1 = New System.Windows.Forms.Label()
        Me.lblFalla_BAR2 = New System.Windows.Forms.Label()
        Me.picSensorPosicionSinCortar = New System.Windows.Forms.PictureBox()
        Me.picSensorPosicionCortando = New System.Windows.Forms.PictureBox()
        CType(Me.picS1LecturaNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS1LecturaCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS1PosicionNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS1PosicionCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS2LecturaNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS2LecturaCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS2PosicionNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picS2PosicionCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAR1_ABAJO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAR2_ABAJO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAR2_ARRIBA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBAR1_ARRIBA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensorPosicionSinCortar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensorPosicionCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picS1LecturaNoCortando
        '
        Me.picS1LecturaNoCortando.Image = Global.Controles.My.Resources.Resources.S1LecturaNoCortando
        Me.picS1LecturaNoCortando.Location = New System.Drawing.Point(124, 304)
        Me.picS1LecturaNoCortando.Name = "picS1LecturaNoCortando"
        Me.picS1LecturaNoCortando.Size = New System.Drawing.Size(257, 60)
        Me.picS1LecturaNoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS1LecturaNoCortando.TabIndex = 0
        Me.picS1LecturaNoCortando.TabStop = False
        '
        'picS1LecturaCortando
        '
        Me.picS1LecturaCortando.Image = Global.Controles.My.Resources.Resources.S1LecturaCortando
        Me.picS1LecturaCortando.Location = New System.Drawing.Point(126, 300)
        Me.picS1LecturaCortando.Name = "picS1LecturaCortando"
        Me.picS1LecturaCortando.Size = New System.Drawing.Size(254, 68)
        Me.picS1LecturaCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS1LecturaCortando.TabIndex = 1
        Me.picS1LecturaCortando.TabStop = False
        '
        'picS1PosicionNoCortando
        '
        Me.picS1PosicionNoCortando.Image = Global.Controles.My.Resources.Resources.S1PosicionNoCortando
        Me.picS1PosicionNoCortando.Location = New System.Drawing.Point(156, 227)
        Me.picS1PosicionNoCortando.Name = "picS1PosicionNoCortando"
        Me.picS1PosicionNoCortando.Size = New System.Drawing.Size(202, 43)
        Me.picS1PosicionNoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS1PosicionNoCortando.TabIndex = 2
        Me.picS1PosicionNoCortando.TabStop = False
        '
        'picS1PosicionCortando
        '
        Me.picS1PosicionCortando.Image = Global.Controles.My.Resources.Resources.S1PosicionCortando
        Me.picS1PosicionCortando.Location = New System.Drawing.Point(156, 227)
        Me.picS1PosicionCortando.Name = "picS1PosicionCortando"
        Me.picS1PosicionCortando.Size = New System.Drawing.Size(198, 49)
        Me.picS1PosicionCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS1PosicionCortando.TabIndex = 3
        Me.picS1PosicionCortando.TabStop = False
        '
        'picS2LecturaNoCortando
        '
        Me.picS2LecturaNoCortando.Image = Global.Controles.My.Resources.Resources.S2LecturaNoCortando
        Me.picS2LecturaNoCortando.Location = New System.Drawing.Point(214, 86)
        Me.picS2LecturaNoCortando.Name = "picS2LecturaNoCortando"
        Me.picS2LecturaNoCortando.Size = New System.Drawing.Size(80, 15)
        Me.picS2LecturaNoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS2LecturaNoCortando.TabIndex = 4
        Me.picS2LecturaNoCortando.TabStop = False
        '
        'picS2LecturaCortando
        '
        Me.picS2LecturaCortando.Image = Global.Controles.My.Resources.Resources.S2LecturaCortando
        Me.picS2LecturaCortando.Location = New System.Drawing.Point(215, 86)
        Me.picS2LecturaCortando.Name = "picS2LecturaCortando"
        Me.picS2LecturaCortando.Size = New System.Drawing.Size(83, 13)
        Me.picS2LecturaCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS2LecturaCortando.TabIndex = 5
        Me.picS2LecturaCortando.TabStop = False
        '
        'picS2PosicionNoCortando
        '
        Me.picS2PosicionNoCortando.Image = Global.Controles.My.Resources.Resources.S2PosicionNoCortando
        Me.picS2PosicionNoCortando.Location = New System.Drawing.Point(210, 100)
        Me.picS2PosicionNoCortando.Name = "picS2PosicionNoCortando"
        Me.picS2PosicionNoCortando.Size = New System.Drawing.Size(91, 23)
        Me.picS2PosicionNoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS2PosicionNoCortando.TabIndex = 6
        Me.picS2PosicionNoCortando.TabStop = False
        '
        'picS2PosicionCortando
        '
        Me.picS2PosicionCortando.Image = Global.Controles.My.Resources.Resources.S2PosicionCortando
        Me.picS2PosicionCortando.Location = New System.Drawing.Point(209, 99)
        Me.picS2PosicionCortando.Name = "picS2PosicionCortando"
        Me.picS2PosicionCortando.Size = New System.Drawing.Size(92, 24)
        Me.picS2PosicionCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picS2PosicionCortando.TabIndex = 7
        Me.picS2PosicionCortando.TabStop = False
        '
        'picBAR1_ABAJO
        '

        Me.picBAR1_ABAJO.Location = New System.Drawing.Point(110, 184)
        Me.picBAR1_ABAJO.Name = "picBAR1_ABAJO"
        Me.picBAR1_ABAJO.Size = New System.Drawing.Size(210, 116)
        Me.picBAR1_ABAJO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAR1_ABAJO.TabIndex = 8
        Me.picBAR1_ABAJO.TabStop = False
        '
        'picBAR2_ABAJO
        '

        Me.picBAR2_ABAJO.Location = New System.Drawing.Point(193, 76)
        Me.picBAR2_ABAJO.Name = "picBAR2_ABAJO"
        Me.picBAR2_ABAJO.Size = New System.Drawing.Size(114, 47)
        Me.picBAR2_ABAJO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAR2_ABAJO.TabIndex = 9
        Me.picBAR2_ABAJO.TabStop = False
        '
        'picBAR2_ARRIBA
        '

        Me.picBAR2_ARRIBA.Location = New System.Drawing.Point(190, 9)
        Me.picBAR2_ARRIBA.Name = "picBAR2_ARRIBA"
        Me.picBAR2_ARRIBA.Size = New System.Drawing.Size(27, 125)
        Me.picBAR2_ARRIBA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAR2_ARRIBA.TabIndex = 10
        Me.picBAR2_ARRIBA.TabStop = False
        '
        'picBAR1_ARRIBA
        '

        Me.picBAR1_ARRIBA.Location = New System.Drawing.Point(101, 26)
        Me.picBAR1_ARRIBA.Name = "picBAR1_ARRIBA"
        Me.picBAR1_ARRIBA.Size = New System.Drawing.Size(79, 274)
        Me.picBAR1_ARRIBA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBAR1_ARRIBA.TabIndex = 11
        Me.picBAR1_ARRIBA.TabStop = False
        '
        'lblFalla_BAR1
        '
        Me.lblFalla_BAR1.AccessibleDescription = "Balanza 1 Barrera 1"
        Me.lblFalla_BAR1.AutoSize = True
        Me.lblFalla_BAR1.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAR1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAR1.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAR1.Location = New System.Drawing.Point(106, 239)
        Me.lblFalla_BAR1.Name = "lblFalla_BAR1"
        Me.lblFalla_BAR1.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAR1.TabIndex = 94
        Me.lblFalla_BAR1.Text = "Falla"
        Me.lblFalla_BAR1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFalla_BAR2
        '
        Me.lblFalla_BAR2.AccessibleDescription = "Balanza 1 Barrera 1"
        Me.lblFalla_BAR2.AutoSize = True
        Me.lblFalla_BAR2.BackColor = System.Drawing.Color.Red
        Me.lblFalla_BAR2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFalla_BAR2.ForeColor = System.Drawing.Color.White
        Me.lblFalla_BAR2.Location = New System.Drawing.Point(169, 99)
        Me.lblFalla_BAR2.Name = "lblFalla_BAR2"
        Me.lblFalla_BAR2.Size = New System.Drawing.Size(48, 20)
        Me.lblFalla_BAR2.TabIndex = 95
        Me.lblFalla_BAR2.Text = "Falla"
        Me.lblFalla_BAR2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picSensorPosicionSinCortar
        '
        Me.picSensorPosicionSinCortar.Image = Global.Controles.My.Resources.Resources.SensorPosicion
        Me.picSensorPosicionSinCortar.Location = New System.Drawing.Point(167, 200)
        Me.picSensorPosicionSinCortar.Name = "picSensorPosicionSinCortar"
        Me.picSensorPosicionSinCortar.Size = New System.Drawing.Size(178, 22)
        Me.picSensorPosicionSinCortar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensorPosicionSinCortar.TabIndex = 96
        Me.picSensorPosicionSinCortar.TabStop = False
        '
        'picSensorPosicionCortando
        '
        Me.picSensorPosicionCortando.Image = Global.Controles.My.Resources.Resources.SensonPosicionCortando
        Me.picSensorPosicionCortando.Location = New System.Drawing.Point(168, 199)
        Me.picSensorPosicionCortando.Name = "picSensorPosicionCortando"
        Me.picSensorPosicionCortando.Size = New System.Drawing.Size(175, 22)
        Me.picSensorPosicionCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensorPosicionCortando.TabIndex = 97
        Me.picSensorPosicionCortando.TabStop = False
        '
        'CtrlDinamismoBalanza1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.Balanza_8S
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.picSensorPosicionSinCortar)
        Me.Controls.Add(Me.picSensorPosicionCortando)
        Me.Controls.Add(Me.lblFalla_BAR2)
        Me.Controls.Add(Me.lblFalla_BAR1)
        Me.Controls.Add(Me.picS1LecturaNoCortando)
        Me.Controls.Add(Me.picS2PosicionCortando)
        Me.Controls.Add(Me.picS2LecturaCortando)
        Me.Controls.Add(Me.picS1LecturaCortando)
        Me.Controls.Add(Me.picS1PosicionNoCortando)
        Me.Controls.Add(Me.picS2PosicionNoCortando)
        Me.Controls.Add(Me.picS2LecturaNoCortando)
        Me.Controls.Add(Me.picS1PosicionCortando)
        Me.Controls.Add(Me.picBAR1_ARRIBA)
        Me.Controls.Add(Me.picBAR1_ABAJO)
        Me.Controls.Add(Me.picBAR2_ARRIBA)
        Me.Controls.Add(Me.picBAR2_ABAJO)
        Me.Name = "CtrlDinamismoBalanza1"
        Me.Size = New System.Drawing.Size(533, 391)
        CType(Me.picS1LecturaNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS1LecturaCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS1PosicionNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS1PosicionCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS2LecturaNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS2LecturaCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS2PosicionNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picS2PosicionCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAR1_ABAJO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAR2_ABAJO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAR2_ARRIBA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBAR1_ARRIBA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensorPosicionSinCortar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensorPosicionCortando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picS1LecturaNoCortando As PictureBox
    Friend WithEvents picS1LecturaCortando As PictureBox
    Friend WithEvents picS1PosicionNoCortando As PictureBox
    Friend WithEvents picS1PosicionCortando As PictureBox
    Friend WithEvents picS2LecturaNoCortando As PictureBox
    Friend WithEvents picS2LecturaCortando As PictureBox
    Friend WithEvents picS2PosicionNoCortando As PictureBox
    Friend WithEvents picS2PosicionCortando As PictureBox
    Friend WithEvents picBAR1_ABAJO As PictureBox
    Friend WithEvents picBAR2_ABAJO As PictureBox
    Friend WithEvents picBAR2_ARRIBA As PictureBox
    Friend WithEvents picBAR1_ARRIBA As PictureBox
    Friend WithEvents lblFalla_BAR1 As Label
    Friend WithEvents lblFalla_BAR2 As Label
    Friend WithEvents picSensorPosicionSinCortar As PictureBox
    Friend WithEvents picSensorPosicionCortando As PictureBox
End Class
