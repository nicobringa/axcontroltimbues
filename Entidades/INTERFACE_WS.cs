//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class INTERFACE_WS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INTERFACE_WS()
        {
            this.INTERFACE_EVENTOS = new HashSet<INTERFACE_EVENTOS>();
        }
    
        public long ID_REGISTRO { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public Nullable<decimal> ID_COMANDO { get; set; }
        public string WEB_SERVICE { get; set; }
        public string JSON_INPUT { get; set; }
        public Nullable<int> ID_ESTADO { get; set; }
        public string JSON_RPTA_PLC { get; set; }
    
        public virtual INTERFACE_ESTADO INTERFACE_ESTADO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INTERFACE_EVENTOS> INTERFACE_EVENTOS { get; set; }
    }
}
