﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;
using axControl.Models;

namespace AxControlWeb.Controllers
{
    public class SectoresController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();
        private AxDriverORAEntities dbDriv = new AxDriverORAEntities();

        
        public PartialViewResult _sector(int idSector)
        {
            var sectorActual = (from sa in db.SECTOR where sa.ID_SECTOR == idSector select sa).FirstOrDefault();
           
            return PartialView(sectorActual);
        }

        public PartialViewResult _DinamismoPorteria(int idSector)
        {
            var Barrera = (from o in dbDriv.DATO_WORDINT where o.ID_SECTOR == idSector && o.TAG == "BARRERA.Estado" select o).FirstOrDefault();
            var Sensor = (from o in dbDriv.DATO_WORDINT where o.ID_SECTOR == idSector && o.TAG == "S_BARRERA.Estado" select o).FirstOrDefault();
            var Semaforo1 = (from o in dbDriv.DATO_WORDINT where o.ID_SECTOR == idSector && o.TAG == "SEMAFORO1.Estado" select o).FirstOrDefault();
            var Semaforo2 = (from o in dbDriv.DATO_WORDINT where o.ID_SECTOR == idSector && o.TAG == "SEMAFORO2.Estado" select o).FirstOrDefault();
            var Espira = (from o in dbDriv.DATO_WORDINT where o.ID_SECTOR == idSector && o.TAG == "S_LECTURA.Estado" select o).FirstOrDefault();



            if (Barrera!=null)
            {
                switch (Barrera.VALOR)
                {
                    case (int)Constante.EstadoBarrera.ArribaAutomatica:
                    case (int)Constante.EstadoBarrera.ArribaManual:
                        ViewBag.estadoBarrera = "barrera levantada";
                        break;
                    case (int)Constante.EstadoBarrera.AbajoAutomatica:
                    case (int)Constante.EstadoBarrera.AbajoManual:
                    case (int)Constante.EstadoBarrera.Falla:
                        ViewBag.estadoBarrera = "barrera baja_";
                        break;
                }
            }
            else
            {
                ViewBag.estadoBarrera = "NULL";
            }

            if(Sensor!=null)
            {
                switch (Sensor.VALOR)
                {
                    case (int)Constante.EstadoSensor.SensorConPresencia:
                        ViewBag.estadoSensor = "Sensor_Encendido";
                        break;
                    case (int)Constante.EstadoSensor.SensorSinPresencia:
                    case (int)Constante.EstadoSensor.Falla:
                    case (int)Constante.EstadoSensor.Deshabilitado:
                        ViewBag.estadoSensor = "Sensor_Apagado";
                        break;

                }
            }
            else
            {
                ViewBag.estadoSensor = "NULL";
            }

            if (Semaforo1!=null)
            {
                switch (Semaforo1.VALOR)
                {
                    case (int)Constante.EstadoSemaforo.Verde:
                        ViewBag.estadoSemaforo1 = "VERDE";
                        break;
                    case (int)Constante.EstadoSemaforo.Rojo:
                        ViewBag.estadoSemaforo1 = "ROJO";
                        break;
                    case (int)Constante.EstadoSemaforo.Apagado:
                        ViewBag.estadoSemaforo1 = "APAGADO";
                        break;
                }
            }
            else
            {
                ViewBag.estadoSemaforo1 = "NULL";
            }

            if (Semaforo2 != null)
            {
                switch (Semaforo2.VALOR)
                {
                    case (int)Constante.EstadoSemaforo.Verde:
                        ViewBag.estadoSemaforo2 = "VERDE";
                        break;
                    case (int)Constante.EstadoSemaforo.Rojo:
                        ViewBag.estadoSemaforo2 = "ROJO";
                        break;
                    case (int)Constante.EstadoSemaforo.Apagado:
                        ViewBag.estadoSemaforo2 = "APAGADO";
                        break;
                }
            }
            else
            {
                ViewBag.estadoSemaforo2 = "NULL";
            }

            if (Espira != null)
            {
                switch (Espira.VALOR)
                {
                    case (int)Constante.EstadoSensor.SensorConPresencia:
                        ViewBag.estadoEspira = "Espira_Con_Lectura";
                        break;
                    case (int)Constante.EstadoSensor.SensorSinPresencia:
                    case (int)Constante.EstadoSensor.Falla:
                    case (int)Constante.EstadoSensor.Deshabilitado:
                        ViewBag.estadoEspira = "Espira_Sin_Lectura";
                        break;
                }
            }
            else
            {
                ViewBag.estadoEspira = "NULL";
            }
            var sectorActual = (from sa in db.SECTOR where sa.ID_SECTOR == idSector select sa).FirstOrDefault();
            return PartialView(sectorActual);
        }

        #region "Balanza"

        public PartialViewResult _DinamismoBalanza(int idSector)
        {
            SECTOR oSector = db.SECTOR.Where(a => a.ID_SECTOR == idSector).FirstOrDefault();
            int? EstadoBar1 = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "BARRERA1.Estado").Select(a => a.VALOR).FirstOrDefault();
            int? EstadoBar2 = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "BARRERA2.Estado").Select(a => a.VALOR).FirstOrDefault();
            int? EstadoS1_Bar = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "S1_BARRERA.Estado").Select(a => a.VALOR).FirstOrDefault();
            int? EstadoS2_Bar = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "S2_BARRERA.Estado").Select(a => a.VALOR).FirstOrDefault();
            int? EstadoS1_Pos = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "S1_POSICION.Estado").Select(a => a.VALOR).FirstOrDefault();
            int? EstadoS2_Pos = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "S2_POSICION.Estado").Select(a => a.VALOR).FirstOrDefault();
            int? EstadoS_Lect = dbDriv.DATO_WORDINT.Where(a => a.ID_SECTOR == idSector && a.TAG == "S_LECTURA.Estado").Select(a => a.VALOR).FirstOrDefault();

            ViewBag.oSector = oSector;
            ViewBag.EstadoBar1 = EstadoBar1 == (int)Constante.EstadoBarrera.ArribaAutomatica || EstadoBar1 == (int)Constante.EstadoBarrera.ArribaAutomatica ? "barrera levantada" : "barrera baja_";
            ViewBag.EstadoBar2 = EstadoBar2 == (int)Constante.EstadoBarrera.ArribaAutomatica || EstadoBar2 == (int)Constante.EstadoBarrera.ArribaAutomatica ? "barrera levantada" : "barrera baja_";
            ViewBag.EstadoS1_Bar = EstadoS1_Bar == (int)Constante.EstadoSensor.SensorConPresencia ? "Sensor_Encendido" : "Sensor_Apagado";
            ViewBag.EstadoS2_Bar = EstadoS2_Bar == (int)Constante.EstadoSensor.SensorConPresencia ? "Sensor_Encendido" : "Sensor_Apagado";
            ViewBag.EstadoS1_Pos = EstadoS1_Pos == (int)Constante.EstadoSensor.SensorConPresencia ? "Sensor_Encendido" : "Sensor_Apagado";
            ViewBag.EstadoS2_Pos = EstadoS2_Pos == (int)Constante.EstadoSensor.SensorConPresencia ? "Sensor_Encendido" : "Sensor_Apagado";
            ViewBag.EstadoS_Lect = EstadoS_Lect == (int)Constante.EstadoSensor.SensorConPresencia ? "Espira_Con_Lectura" : "Espira_Sin_Lectura";

            return PartialView(oSector);
        }

        public PartialViewResult Balanza(int idSector)
        {

            return PartialView();
        }

        public objBalanza GetBalanza(string tag, int idSector)
        {
            List<DATO_WORDINT> ListDatoWordInt = dbDriv.DATO_WORDINT.Where(a => a.TAG.StartsWith(tag) && a.ID_SECTOR == idSector).ToList();
            List<DATO_BOOL> ListDatoBool = dbDriv.DATO_BOOL.Where(a => a.TAG.StartsWith(tag) && a.ID_SECTOR == idSector).ToList();
            List<DATO_DWORDDINT> ListDatoDWordInt = dbDriv.DATO_DWORDDINT.Where(a => a.TAG.StartsWith(tag) && a.ID_SECTOR == idSector).ToList();
            objBalanza oBalanza = new objBalanza();
            oBalanza.Descripcion = tag;
            oBalanza.LecturaRFID = dbDriv.DATO_WORDINT.Where(a => a.TAG == "BALANZA.LecturaRFID" && a.ID_SECTOR == idSector).SingleOrDefault();
            foreach (DATO_WORDINT item in ListDatoWordInt)
            {
                if (item.TAG.EndsWith("Comando"))
                {
                    oBalanza.Comando = item;
                }
                if (item.TAG.EndsWith("DiferenciaEstabilidad"))
                {
                    oBalanza.DiferenciaEstabilidad = item;
                }
                if (item.TAG.EndsWith("Estado"))
                {
                    oBalanza.Estado = item;
                }
                if (item.TAG.EndsWith("EstadoOld"))
                {
                    oBalanza.EstadoOld = item;
                }
                if (item.TAG.EndsWith("PesoMax"))
                {
                    oBalanza.PesoMax = item;
                }
                if (item.TAG.EndsWith("TiempoEstabilidad"))
                {
                    oBalanza.TiempoEstabilidad = item;
                }
            }
            foreach (DATO_BOOL item in ListDatoBool)
            {
                if (item.TAG.EndsWith("ErrorBal"))
                {
                    oBalanza.dbErrorBal = item;
                }
                if (item.TAG.EndsWith("FueraEquilibrio"))
                {
                    oBalanza.dbFueraEquilibrio = item;
                }
                if (item.TAG.EndsWith("FueraRango"))
                {
                    oBalanza.dbFueraRango = item;
                }
                if (item.TAG.EndsWith("PesoEstable"))
                {
                    oBalanza.dbPesoEstable = item;
                }
                if (item.TAG.EndsWith("PesoMaximoBal"))
                {
                    oBalanza.dbPesoMaximoBal = item;
                }
                if (item.TAG.EndsWith("PesoMayor500"))
                {
                    oBalanza.dbPesoMayor500 = item;
                }
                if (item.TAG.EndsWith("PesoMenorDifEstabilidad"))
                {
                    oBalanza.dbPesoMenorDifEstabilidad = item;
                }
                if (item.TAG.EndsWith("PesoNegativo"))
                {
                    oBalanza.dbPesoNegativo = item;
                }
                if (item.TAG.EndsWith("PesoNeto"))
                {
                    oBalanza.dbPesoNeto = item;
                }
            }
            foreach (DATO_DWORDDINT item in ListDatoDWordInt)
            {
                if (item.TAG.EndsWith("Peso"))
                {
                    oBalanza.Peso = item;
                }
            }
            return oBalanza;
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
