﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;

namespace AxControlWeb.Controllers
{
    public class NotificacionesController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();

        public ActionResult Inicio()
        {
            //Las notificaciones de tipo Error Programador "0", no se van a visualizar en la web.
            List<Entidades.NOTIFICACION> ListaNotif = db.NOTIFICACION.Where(a=>a.TIPO_NOTIFICACION != "0" && a.FECHA > DateTime.Now.AddDays(-1)).OrderByDescending(a => a.FECHA).ToList();
            var ListControlesAcceso = db.CONTROL_ACCESO.ToList();
            ViewBag.ListControlesAcceso = ListControlesAcceso;
            return View(ListaNotif);
        }

        // GET: Notificaciones
        public ActionResult Index()
        {
            //Las notificaciones de tipo Error Programador "0", no se van a visualizar en la web.
            var nOTIFICACION = db.NOTIFICACION.Where(a => a.TIPO_NOTIFICACION != "0" && a.FECHA > DateTime.Now.AddDays(-1));
            return View(nOTIFICACION.ToList());
        }

        public void fnEliminarNotificacion(long idNotificacion)
        {
            try
            {
                var reporte = (from d in db.NOTIFICACION
                               where d.ID_NOTIFICACION == idNotificacion
                               select d).Single();
                db.NOTIFICACION.Remove(reporte);
                db.SaveChanges();
            }
            catch (Exception)
            {

            }

        }
        // GET: Notificaciones/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NOTIFICACION nOTIFICACION = db.NOTIFICACION.Find(id);
            if (nOTIFICACION == null)
            {
                return HttpNotFound();
            }
            return View(nOTIFICACION);
        }

        // GET: Notificaciones/Create
        public ActionResult Create()
        {
            ViewBag.ID_SECTOR = new SelectList(db.SECTOR, "ID_SECTOR", "NOMBRE");
            return View();
        }

        // POST: Notificaciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_NOTIFICACION,FECHA,MENSAJE,TAG,TIPO_NOTIFICACION,ID_SECTOR,ID_CONTROL_ACCESO")] NOTIFICACION nOTIFICACION)
        {
            if (ModelState.IsValid)
            {
                db.NOTIFICACION.Add(nOTIFICACION);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_SECTOR = new SelectList(db.SECTOR, "ID_SECTOR", "NOMBRE", nOTIFICACION.ID_SECTOR);
            return View(nOTIFICACION);
        }

        // GET: Notificaciones/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NOTIFICACION nOTIFICACION = db.NOTIFICACION.Find(id);
            if (nOTIFICACION == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_SECTOR = new SelectList(db.SECTOR, "ID_SECTOR", "NOMBRE", nOTIFICACION.ID_SECTOR);
            return View(nOTIFICACION);
        }

        // POST: Notificaciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_NOTIFICACION,FECHA,MENSAJE,TAG,TIPO_NOTIFICACION,ID_SECTOR,ID_CONTROL_ACCESO")] NOTIFICACION nOTIFICACION)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nOTIFICACION).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_SECTOR = new SelectList(db.SECTOR, "ID_SECTOR", "NOMBRE", nOTIFICACION.ID_SECTOR);
            return View(nOTIFICACION);
        }

        // GET: Notificaciones/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NOTIFICACION nOTIFICACION = db.NOTIFICACION.Find(id);
            if (nOTIFICACION == null)
            {
                return HttpNotFound();
            }
            return View(nOTIFICACION);
        }

        // POST: Notificaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            NOTIFICACION nOTIFICACION = db.NOTIFICACION.Find(id);
            db.NOTIFICACION.Remove(nOTIFICACION);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public PartialViewResult _ListaNotificaciones(int idControl, int idSector, int? idSector2)
        { 
            //Las notificaciones de tipo Error Programador "0", no se van a visualizar en la web.
            List<NOTIFICACION> noti = new List<NOTIFICACION>();

            DateTime ayer = DateTime.Now.AddDays(-1);
            if (idSector == 0)
            {
                noti = (from n in db.NOTIFICACION where n.ID_CONTROL_ACCESO == idControl && n.ID_SECTOR == null select n).Where(a => a.TIPO_NOTIFICACION != "0" && a.FECHA > ayer).OrderByDescending(a => a.FECHA).ToList();
            }
            else
            {
                if (idSector2 == null)
                {
                    noti = (from n in db.NOTIFICACION where n.ID_CONTROL_ACCESO == idControl && n.ID_SECTOR == idSector select n).Where(a => a.TIPO_NOTIFICACION != "0" && a.FECHA > ayer).OrderByDescending(a => a.FECHA).ToList();
                }
               else
                {
                    noti = (from n in db.NOTIFICACION where n.ID_CONTROL_ACCESO == idControl && (n.ID_SECTOR == idSector || n.ID_SECTOR == idSector2)  select n).Where(a => a.TIPO_NOTIFICACION != "0" && a.FECHA > ayer).OrderByDescending(a => a.FECHA).ToList();
                }
            }

            ViewBag.ListaNotificacion = noti;
            return PartialView();
        }

        public void GuardarNotificacion(int? idSector, int idControlAcceso, string Notificacion, string tipoNotif)
        {
            try
            {
                BorrarNotificacionesAntiguas();
                AxControlORAEntities dbNoti = new AxControlORAEntities();
                NOTIFICACION oNotificacion = new NOTIFICACION();
                oNotificacion.ID_SECTOR = idSector;
                oNotificacion.ID_CONTROL_ACCESO = idControlAcceso;
                oNotificacion.FECHA = System.DateTime.Now;
                oNotificacion.MENSAJE = Notificacion;
                oNotificacion.TIPO_NOTIFICACION = tipoNotif;

                dbNoti.NOTIFICACION.Add(oNotificacion);
                dbNoti.SaveChanges();

                dbNoti.Dispose();
                dbNoti = null;
            }
            catch (Exception ex)
            {
                string msj = ex.Message;
                throw;
            }

        }

        /// <summary>
        /// Busco y borro las notificaciones cuya fecha sea anterior a la semana pasada.
        /// </summary>
        public void BorrarNotificacionesAntiguas()
        {
            try
            {

                AxControlORAEntities dbNot = new AxControlORAEntities();
                //Tomo cómo parametro la fecha de hace una semana al día de hoy.
                DateTime dtAntiguas = DateTime.Now.AddDays(-7);

                //Listo todas las notificaciones que tengan fecha anterior a la semana pasada
                var listaNotificaciones = (from n in dbNot.NOTIFICACION where n.FECHA < dtAntiguas select n).ToList();

                if(listaNotificaciones.Count>0)
                {
                    //Elimino y guardo el cambio
                    dbNot.NOTIFICACION.RemoveRange(listaNotificaciones);
                    dbNot.SaveChanges();
                }

                //Cierro la conexión y limpio.
                dbNot.Dispose();
                dbNot = null;

            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}
