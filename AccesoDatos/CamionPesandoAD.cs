﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

using System.Data.Entity;

namespace AccesoDatos
{
    public class CAMION_PESANDOAD
    {
        public CAMION_PESANDO add(CAMION_PESANDO CAMION_PESANDO)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                oContext.CAMION_PESANDO.Add(CAMION_PESANDO);
                oContext.SaveChanges();
                return CAMION_PESANDO;
            }
            catch (Exception ex)
            {

                return null;
            }


        }

        public CAMION_PESANDO getOne(string tagRFID, int idSector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();

                return oContext.CAMION_PESANDO.AsNoTracking().Where(o => o.ID_SECTOR == idSector &&
                o.TAG_RFID == tagRFID).SingleOrDefault();

            }
            catch (Exception)
            {

                return null;
            }
        }

        public CAMION_PESANDO getOne(int idSector1, int idSector2)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();

                return oContext.CAMION_PESANDO.AsNoTracking().Where(o => o.ID_SECTOR == idSector1 || o.ID_SECTOR == idSector2).SingleOrDefault();

            }
            catch (Exception)
            {

                return null;
            }
        }

        public CAMION_PESANDO getOne(int idSector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                CAMION_PESANDO oCamion=oContext.CAMION_PESANDO.Where(o => o.ID_SECTOR == idSector).SingleOrDefault();
                return oContext.CAMION_PESANDO.Where(o => o.ID_SECTOR == idSector).SingleOrDefault();

            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public CAMION_PESANDO update(CAMION_PESANDO CAMION_PESANDO)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                oContext.Entry(CAMION_PESANDO).State = EntityState.Modified;
                oContext.SaveChanges();
                return CAMION_PESANDO;
            }
            catch (Exception ex)
            {

                return null;
            }

        }


        public bool delete(CAMION_PESANDO CAMION_PESANDO)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                oContext.CAMION_PESANDO.Attach(CAMION_PESANDO);
                oContext.CAMION_PESANDO.Remove(CAMION_PESANDO);
                oContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }

    }
}
