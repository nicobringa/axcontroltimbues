﻿Public Class FrmCAMION_PESANDO

    Private _EstadoBalanza As Entidades.Constante.BalanzaEstado
    Private _CAMION_PESANDO As Entidades.CAMION_PESANDO


    Public Sub New(ByRef CAMION_PESANDO As Entidades.CAMION_PESANDO)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        _CAMION_PESANDO = CAMION_PESANDO
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Consultar(CAMION_PESANDO)
    End Sub


    Private Sub Consultar(ByRef CAMION_PESANDO As Entidades.CAMION_PESANDO)

        txtTagRFID.Text = CAMION_PESANDO.TAG_RFID
        txtIdSector.Text = CAMION_PESANDO.ID_SECTOR
        '  txtIdTransaccion.Text = CAMION_PESANDO.i
        cbPesoEnviado.Checked = CAMION_PESANDO.PESO_ENVIADO


    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs)
        If _EstadoBalanza = Entidades.Constante.BalanzaEstado.BarrerasCerrada Then

            If MessageBox.Show("¿Estas seguro que quieres eliminar el camión que esta pesando?", "Eliminar camión", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim nCAMION_PESANDO As New Negocio.CAMION_PESANDON
                nCAMION_PESANDO.Delete(_CAMION_PESANDO)
            End If

        Else

            MessageBox.Show("No se puede eliminar el camión porque la balanza no se encuentra en el etado 0", "Eliminar camión pesando",
                            MessageBoxButtons.OK, MessageBoxIcon.Hand)

        End If
    End Sub
End Class