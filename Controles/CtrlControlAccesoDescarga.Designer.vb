﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlControlAccesoDescarga
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnLecturaDescarga1 = New System.Windows.Forms.Button()
        Me.CtrlAntenaDescarga1 = New Controles.CtrlAntenaLeyendo()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CartelLED_DESCARGA = New Controles.CtrlCartelMultiLED()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(3, 3)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 87
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btnLecturaDescarga1)
        Me.Panel1.Controls.Add(Me.CtrlAntenaDescarga1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CartelLED_DESCARGA)
        Me.Panel1.Location = New System.Drawing.Point(2, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(507, 144)
        Me.Panel1.TabIndex = 88
        '
        'btnLecturaDescarga1
        '
        Me.btnLecturaDescarga1.BackColor = System.Drawing.Color.Transparent
        Me.btnLecturaDescarga1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaDescarga1.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaDescarga1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaDescarga1.Location = New System.Drawing.Point(320, 3)
        Me.btnLecturaDescarga1.Name = "btnLecturaDescarga1"
        Me.btnLecturaDescarga1.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaDescarga1.TabIndex = 136
        Me.btnLecturaDescarga1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaDescarga1.UseVisualStyleBackColor = False
        '
        'CtrlAntenaDescarga1
        '
        Me.CtrlAntenaDescarga1.AutoSize = True
        Me.CtrlAntenaDescarga1.Location = New System.Drawing.Point(203, 8)
        Me.CtrlAntenaDescarga1.Name = "CtrlAntenaDescarga1"
        Me.CtrlAntenaDescarga1.Size = New System.Drawing.Size(111, 103)
        Me.CtrlAntenaDescarga1.TabIndex = 92
        Me.CtrlAntenaDescarga1.UltTag = Nothing
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 29)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Descarga 1"
        '
        'CartelLED_DESCARGA
        '
        Me.CartelLED_DESCARGA.Location = New System.Drawing.Point(3, 40)
        Me.CartelLED_DESCARGA.Name = "CartelLED_DESCARGA"
        Me.CartelLED_DESCARGA.Size = New System.Drawing.Size(194, 102)
        Me.CartelLED_DESCARGA.TabIndex = 89
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(513, -3)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(258, 266)
        Me.CtrlPanelNotificaciones1.TabIndex = 92
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(2, 191)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(510, 73)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 94
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(457, 0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 107
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(402, -3)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(49, 44)
        Me.btnAyuda.TabIndex = 152
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'lblNombre
        '
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(160, 3)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(236, 42)
        Me.lblNombre.TabIndex = 153
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlControlAccesoDescarga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Name = "CtrlControlAccesoDescarga"
        Me.Size = New System.Drawing.Size(772, 263)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents CartelLED_DESCARGA As CtrlCartelMultiLED
    Friend WithEvents Label1 As Label
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents CtrlAntenaDescarga1 As CtrlAntenaLeyendo
    Friend WithEvents btnLecturaDescarga1 As Button
    Friend WithEvents btnReset As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents lblNombre As Label
End Class
