﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio

Public Class frmBarrera
#Region "Variables de Instancia"
    Dim oDatoIntN As DatoWordIntN
#End Region

#Region "Eventos"
    Public Event UsoManual()
#End Region


#Region "Propiedades"

    Private _Estado As Integer
    Public Property Estado() As Integer
        Get
            Return _Estado
        End Get
        Set(ByVal value As Integer)
            _Estado = value
            Select Case value
                Case BarreraEstado.ArribaAuto
                    Negocio.modDelegado.SetText(Me, txtEstado, "Barrera Abierta Automatico")
                Case BarreraEstado.ArribaManual
                    Negocio.modDelegado.SetText(Me, txtEstado, "Barrera Abierta Manual")
                Case BarreraEstado.AbajoAuto
                    Negocio.modDelegado.SetText(Me, txtEstado, "Barrera Cerrada Automatico")
                Case BarreraEstado.AbajoManual
                    Negocio.modDelegado.SetText(Me, txtEstado, "Barrera Cerrada Manual")
                Case BarreraEstado.Falla
                    Negocio.modDelegado.SetText(Me, txtEstado, "Barrera en Falla")
            End Select


        End Set
    End Property




    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
            txtDescripcion.Text = value
        End Set
    End Property

    Private _Comando As DATO_WORDINT
    Public Property oComando() As DATO_WORDINT
        Get
            Return _Comando
        End Get
        Set(ByVal value As DATO_WORDINT)
            _Comando = value
        End Set
    End Property

    Private _HabSensPosicion As Entidades.DATO_WORDINT
    Public Property oHabSensorPosicion() As Entidades.DATO_WORDINT
        Get
            Return _HabSensPosicion
        End Get
        Set(ByVal value As Entidades.DATO_WORDINT)
            _HabSensPosicion = value
            Dim tmp As String = If(value.VALOR = Constante.SensorHabilitacion.Deshabilitar, "Deshabilitado", "Habilitado")

            Negocio.modDelegado.SetText(Me, txtEstadoSensorPosicion, tmp)
        End Set
    End Property

#End Region

#Region "Métodos"
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oDatoIntN = New DatoWordIntN

    End Sub


    Public Sub New(oEstado As DATO_WORDINT, oComando As DATO_WORDINT, oHabSensorPosicion As DATO_WORDINT, Nombre As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oDatoIntN = New DatoWordIntN
        Me.Estado = oEstado.VALOR

        Me.Descripcion = Nombre
        Me.oComando = oComando
        Me.oHabSensorPosicion = oHabSensorPosicion
        txtPLC.Text = oEstado.FK_PLC
        txtSector.Text = oEstado.ID_SECTOR

    End Sub
#End Region


    Private Sub btnAceptar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnComandos_Click(sender As Object, e As EventArgs) Handles btnSubir.Click, BtnBajar.Click, btnAutomaticoComando.Click, btnDeshabilitarComando.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog <> DialogResult.OK Then Return

        Dim btn As Button = sender
        Dim oComandoEscribir As Constante.BarreraComando
        Dim nComando As New DatoWordIntN
        Dim strAuditoria As String = ""
        Select Case btn.Name
            Case btnSubir.Name
                strAuditoria = String.Format("Se solicito subir en manual la barrera {0}", Me.Descripcion)
                oComandoEscribir = Constante.BarreraComando.SubirManual

            Case BtnBajar.Name
                strAuditoria = String.Format("Se solicito bajar en manual la barrera {0}", Me.Descripcion)
                oComandoEscribir = Constante.BarreraComando.BajarManual

            Case btnAutomaticoComando.Name
                strAuditoria = String.Format("Se solicito habilitar la barrera {0}", Me.Descripcion)
                oComandoEscribir = Constante.BarreraComando.Automatico

            Case btnDeshabilitarComando.Name
                strAuditoria = String.Format("Se solicito deshabilitar la barrera {0}", Me.Descripcion)
                oComandoEscribir = Constante.BarreraComando.Deshabilitar

        End Select
        Negocio.AuditoriaN.AddAuditoria(strAuditoria, Nothing, Me.oComando.ID_SECTOR, 0, oAutenticacion.Usuario, Constante.acciones.BarreraManual)
        nComando.Escribir(Me.oComando.id, oComandoEscribir)

        MessageBox.Show("Operación realizada con exito", "Comando", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub

    Private Sub btnHabilitarDeshabilitarSensorPosicion_Click(sender As Object, e As EventArgs) Handles btnHabilitarSensorPosicion.Click, btnDeshabilitarSensorPosicion.Click
        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog <> DialogResult.OK Then Return

        Dim btn As Button = sender
        Dim oComandoEscribir As Constante.DeshabHabSensorPosicionBarrera = DeshabHabSensorPosicionBarrera.Habilitar
        Select Case btn.Name
            Case btnHabilitarSensorPosicion.Name
                oComandoEscribir = DeshabHabSensorPosicionBarrera.Habilitar
            Case btnDeshabilitarSensorPosicion.Name
                oComandoEscribir = DeshabHabSensorPosicionBarrera.Deshabilitar
        End Select

        Dim nHabDeshabSensorPosicion As New DatoWordIntN
        nHabDeshabSensorPosicion.Escribir(Me.oHabSensorPosicion.id, oComandoEscribir)
        MessageBox.Show("Operación realizada con exito", "Comando", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()

    End Sub
End Class