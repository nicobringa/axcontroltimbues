﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports Entidades
Imports Negocio

Public Class CtrlCartelMultiLED
    Inherits CtrlAutomationObjectsBase
#Region "Variables de Instancia"
    Private WithEvents ofrmCartel As frmCartelMultiLED
    Public oCartel As Entidades.CARTEL_MULTILED
    Dim oCartelN As CartelMultiLedN
    Private hMonitoreo As Thread

    'Inicio de transmicion
    Dim IT1 As Decimal = 2 '1
    Dim IT2 As Decimal = 16 '2
    Dim FT1 As Decimal = 16
    Dim FT2 As Decimal = 3

    Dim CP As Decimal = 48 '0x30 : Cambiar programa actual
    Dim EV As Decimal = 49 '0x31 : Escribir variable.
    Dim CS As Decimal = 51 '0x33 : Cambio de Step
    Dim WDT As Decimal = 56 '0x38 : Config el Watchdog timer


    Dim Trama0 As Decimal = 50
    Dim Trama1 As Decimal = 48

    Public PasoActual As Integer = 1
    Dim PasoAnterior As Integer = 1

    Dim ProgramaActual = 0

    Dim boSeEstablecioWDT = False

    Public MensajeActual As String = "PRUEBA DE MENSAJE ACTUAL"


#End Region

    Private _Conectado As Boolean
    Public Property Conectado() As Boolean
        Get
            Return _Conectado
        End Get
        Set(ByVal value As Boolean)
            _Conectado = value
            If Conectado Then
                Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Conectado")
                Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Green)
            Else
                Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Desconectado")
                Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Red)
                Negocio.AuditoriaN.AddAuditoria("Cartel MultilED DESCONECTADO", "", Me.ID_SECTOR, 0, "SISTEMA", Constante.acciones.DesconexionPerifericos)
            End If
        End Set
    End Property

#Region "Propiedades"
    Public Sub New()

        InitializeComponent()


        oCartelN = New CartelMultiLedN

    End Sub
#End Region

#Region "Métodos"

    Private Sub Monitoreo()
        While True
            Try
                Dim udpClient As New Net.Sockets.UdpClient()
                If My.Computer.Network.Ping(oCartel.IP) Then
                    If Not oCartel.CONECTADO Then
                        oCartel.CONECTADO = True
                        oCartelN.Update_Config(oCartel)
                    End If

                    'Cálculo nro trama
                    If Trama1 > 57 Then
                        Trama1 = 48
                        Trama0 = Trama0 + 1
                    End If
                    If Trama0 > 57 Then
                        Trama0 = 50
                    End If

                    'Si recién arranca el porgrama, trato de cambiar el programa, y no continua hasta que lo logre
                    If ProgramaActual = 0 Then
                        If oCartelN.CambioPrograma(1, Trama0, Trama1, oCartel.IP, oCartel.PORT) Then
                            ProgramaActual = 1
                        End If
                    Else
                        'Cuando cambio el programa, configuro el WDT, y no continua hasta que lo logre
                        If Not boSeEstablecioWDT Then
                            boSeEstablecioWDT = oCartelN.EstablecerWatchDogTimer(2, Trama0, Trama1, oCartel.IP, oCartel.PORT)
                        Else
                            Select Case PasoActual
                                Case Constante.PasosCartelMultiled.TextoVariable
                                    If oCartelN.CambiarStep(Constante.PasosCartelMultiled.TextoVariable, Trama0, Trama1, oCartel.IP, oCartel.PORT) Then
                                        If oCartel.PASO_ACTUAL <> Convert.ToInt16(Constante.PasosCartelMultiled.TextoVariable) Then
                                            oCartel.PASO_ACTUAL = Convert.ToInt16(Constante.PasosCartelMultiled.TextoVariable)
                                            oCartelN.Update_Config(oCartel)
                                        End If
                                    End If
                                    Trama1 = Trama1 + 1
                                    If oCartelN.escribirVar(MensajeActual, 1, Trama0, Trama1, oCartel.IP, oCartel.PORT) Then
                                        If oCartel.MENSAJE_ACTUAL <> MensajeActual Then
                                            oCartel.MENSAJE_ACTUAL = MensajeActual
                                            oCartelN.Update_Config(oCartel)
                                        End If
                                    End If
                                Case Else
                                    If oCartelN.CambiarStep(PasoActual, Trama0, Trama1, oCartel.IP, oCartel.PORT) Then
                                        If oCartel.PASO_ACTUAL <> PasoActual Then
                                            oCartel.PASO_ACTUAL = PasoActual
                                            oCartelN.Update_Config(oCartel)
                                        End If
                                    End If

                            End Select
                        End If
                    End If

                    Trama1 = Trama1 + 1
                Else
                    'Hago de vuelta ping para volver a chequear
                    If Not My.Computer.Network.Ping(oCartel.IP) Then
                        If oCartel.CONECTADO Then
                            oCartel.CONECTADO = False
                            oCartelN.Update_Config(oCartel)
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
        End While
    End Sub

    Public Sub Inicializar(ByVal idSector As Integer)
        Try
            Me.lbTAG.Text = Me.Name
            Me.Tag = Me.Name

            oCartel = oCartelN.GetOneTag(Name, idSector)

            If Not IsNothing(hMonitoreo) Then
                If hMonitoreo.IsAlive Then hMonitoreo.Abort()
                hMonitoreo = Nothing
            End If
            hMonitoreo = New Thread(AddressOf Monitoreo)
            hMonitoreo.IsBackground = True
            hMonitoreo.Start()

        Catch ex As SocketException

        End Try

    End Sub


    ''' <summary>
    ''' Permite escribir un mensaje en el cartel
    ''' Si el efecto es pasante escribe en la variable 1 el mensaje y en el var2 escribe " "
    ''' Si el efecto es estatico escribe en la variable 2 el mensaje y en el var1 escribe " "
    ''' </summary>
    ''' <param name="mensaje"></param>

    Public Sub EscribirCartel(mensaje As String)
        Dim oControl As New Entidades.CONTROL_ACCESO
        Dim nControl As New Negocio.ControlAccesoN
        If IsNothing(oCartel.ID_CARTEL) Then Throw New Exception("El Cartel MultiLed no esta agregado en la base de datos")
        oControl = nControl.GetOne(oCartel.ID_CARTEL)
        If IsNothing(oCartel.IP) Then Throw New Exception(" El cartel multiled no esta cargado [" & Me.Name & "]")
        SetText(Me, txtMensaje, mensaje)
        oCartelN.Update_Mensaje(oCartel.TAG, mensaje, True)
        If My.Computer.Network.Ping(Me.oCartel.IP) Then
            If mensaje.Length > Constante.CantidadFijaMaxima Then ''sI tiene más de X caracteres se va a visualizar cómo marquesina
                EscribirCartelLED(mensaje, oCartel.IP, oCartel.PORT, Efecto.TEXTO_PASANTE)
                EscribirCartelLED(" ", oCartel.IP, oCartel.PORT, Efecto.TEXTO_ESTATICO)
            Else
                EscribirCartelLED(mensaje, oCartel.IP, oCartel.PORT, Efecto.TEXTO_ESTATICO)
                EscribirCartelLED(" ", oCartel.IP, oCartel.PORT, Efecto.TEXTO_PASANTE)
            End If
            'ModificarAlarma(oCartel.idControlAcceso, 1, oCartel.idControlAcceso, oControl.tipoControlAcceso)
        Else
            'GenerarAlarma("El cartel no se encuentra conectado", oCartel.idControlAcceso, oCartel.idControlAcceso, oControl.tipoControlAcceso)
        End If


    End Sub

    '' <summary>
    '' Configurado para el cartel t10
    '' </summary>
    '' <param name="mensaje"></param>
    '' <param name="IP"></param>
    '' <param name="PUERTO"></param>
    Private Sub EscribirCartelLED(ByVal mensaje As String, ByVal IP As String, ByVal PUERTO As String, efecto As Efecto)

        'Comando
        Dim EV As Decimal = 49 ' 0x31 : Escribir variable.  

        'Creo el vector de bytes con los parametros para el inicio del mensaje
        Dim sendBytes() As Byte = {IT1, IT2, EV}

        'Numero de variable a escribir
        Dim var1 As Decimal = 48
        Dim var2 As Decimal = If(efecto = Efecto.TEXTO_PASANTE, 49, 50) '= 49




        AgregarByteTrama(sendBytes, var1)
        AgregarByteTrama(sendBytes, var2)

        Dim sVal As Decimal
        'Agrego el mensaje
        While mensaje.Length > 0
            sVal = Strings.Asc(mensaje.Substring(0, 1).ToString())
            mensaje = mensaje.Substring(1, mensaje.Length - 1)
            Array.Resize(sendBytes, sendBytes.Length + 1)
            sendBytes(sendBytes.Length - 1) = sVal

        End While

        'Agrego el numero de trama
        Dim NumTrama1 As Integer '= 49
        Dim NumTrama2 As Integer '= 55

        CalcularNumeroTrama(20, NumTrama1, NumTrama2)

        AgregarByteTrama(sendBytes, NumTrama1)
        AgregarByteTrama(sendBytes, NumTrama2)

        'Calculo del checkSuma
        Dim CheckSum As Integer = CalcularChesckSum(sendBytes)

        Dim asciiCheck1 As Integer
        Dim asciiCheck2 As Integer

        CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

        AgregarByteTrama(sendBytes, asciiCheck1)
        AgregarByteTrama(sendBytes, asciiCheck2)

        'Fin de transmicion 
        Dim FT1 As Decimal = 16
        Dim FT2 As Decimal = 3


        AgregarByteTrama(sendBytes, FT1)
        AgregarByteTrama(sendBytes, FT2)

        'Si se le puedo hacer ping significa que esta conectado
        'Me.Conectado = My.Computer.Network.Ping(IP)
        'If Me.Conectado Then

        Dim udpClient As New Net.Sockets.UdpClient()

        udpClient.Connect(IP, PUERTO)

        udpClient.Send(sendBytes, sendBytes.Length)
        'udpClient.Send(sendBytes, sendBytes.Length)
        Threading.Thread.Sleep(500)

        'End If


    End Sub

    ''' <summary>
    ''' Permite agregar una byte a la trama
    ''' </summary>
    Private Sub AgregarByteTrama(ByRef Trama() As Byte, numAscii As Decimal)
        Array.Resize(Trama, Trama.Length + 1)
        Trama(Trama.Length - 1) = numAscii
    End Sub

    ''' <summary>
    ''' La suma de verificación seguirá el standard ISO1155 y se calculará sobre todos los bytes a partir del comando (inclusive), y sin incluir 
    ''' el final de trama (0x10 0x03). 
    ''' LRC = 0x31+0x30+0x31+0x48+0x6F+0x6C+0x61+0x31 + 0x37 = 0x027E 
    ''' LRC = 0x027E And 0xFF = 0x7E
    ''' LRC = ( (0x7E Xor 0xFF) +1 ) And 0xFF = 0x82 
    ''' </summary>
    ''' <param name="Trama"></param>
    Public Function CalcularChesckSum(ByRef Trama() As Byte) As Integer

        Dim LRC As Integer = 0

        For index As Integer = 2 To Trama.Length - 1
            LRC = (LRC + Trama(index))

        Next
        LRC = LRC And 255
        LRC = (((LRC Xor 255) + 1) And 255)

        Return LRC
    End Function

    Public Sub CalcularNumeroTrama(numTrama As Integer, ByRef NumTrama1 As Integer, ByRef NumTrama2 As Integer)

        Dim strHex As String = Hex(numTrama)

        Dim num1 As String = strHex.Substring(0, 1)
        Dim num2 As String = strHex.Substring(1, 1)


        NumTrama1 = Asc(num1)
        NumTrama2 = Asc(num2)

    End Sub

    Public Sub CheckSumTo2Byte(checkSum As Integer, ByRef asciiCheck1 As Integer, ByRef asciiCheck2 As Integer)

        Dim strHex As String = Hex(checkSum)

        Dim check1 As String = strHex.Substring(0, 1)
        Dim check2 As String
        If strHex.Length = 1 Then
            check2 = "0"
        Else
            check2 = strHex.Substring(1, 1)
        End If

        asciiCheck1 = Asc(check1)
        asciiCheck2 = Asc(check2)

    End Sub

    Private Sub btnConf_Click(sender As System.Object, e As System.EventArgs) Handles btnConf.Click
        ofrmCartel = New frmCartelMultiLED(Me)
        ofrmCartel.ShowDialog()
        ofrmCartel = Nothing
    End Sub

    Private Sub Update_(ByVal cartel As Entidades.CARTEL_MULTILED) Handles ofrmCartel.Update_
        oCartel = cartel
    End Sub

    Public Function escribirVar1(ByVal mensaje As String) As Boolean

        Try
            'Antes de escribir variable, selecciono el programa 1
            Dim EscribioCartel As Boolean = False
            Dim intentos As Integer = 0
            While EscribioCartel = False
                EscribioCartel = CambioPrograma(1)
                intentos = intentos + 1
                If intentos > 50 Then
                    EscribioCartel = True
                End If
            End While

            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {IT1, IT2, EV}

            'Variable
            Dim var0 As Decimal = 48 '0
            Dim var1 As Decimal = 49 '1

            'Agrego la variable 01, porque el cartel necesita 2 bytes para la variable
            AgregarByteTrama(sendBytes, var0)
            AgregarByteTrama(sendBytes, var1)

            'Recorro el mensaje y voy agregando caracter por caracter
            While mensaje.Length > 0
                Dim letraMsj As Decimal = Asc(mensaje.Substring(0, 1).ToString())
                AgregarByteTrama(sendBytes, letraMsj)
                mensaje = mensaje.Substring(1, mensaje.Length - 1)
            End While

            'Cálculo nro trama
            If Trama1 > 57 Then
                Trama1 = 48
                Trama0 = Trama0 + 1
            End If
            If Trama0 > 57 Then
                Trama0 = 50
            End If

            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            Trama1 = Trama1 + 1

            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)

            AgregarByteTrama(sendBytes, FT1)
            AgregarByteTrama(sendBytes, FT2)

            Dim boEnviar As Boolean = False

            If My.Computer.Network.Ping(Me.oCartel.IP) Then
                boEnviar = True
            Else
                boEnviar = False
            End If

            If Not boEnviar Then
                If My.Computer.Network.Ping(oCartel.IP) Then
                    Dim udpClient As New Net.Sockets.UdpClient()
                    udpClient.Connect(oCartel.IP, oCartel.PORT)
                    udpClient.Send(sendBytes, sendBytes.Length)
                    Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                    udpClient.Client.ReceiveTimeout = 1000
                    Try
                        Dim bytes As Byte() = udpClient.Receive(groupEP)
                        If bytes(2) = 54 Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Else
                Dim udpClient As New Net.Sockets.UdpClient()
                udpClient.Connect(oCartel.IP, oCartel.PORT)
                udpClient.Send(sendBytes, sendBytes.Length)
                Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                udpClient.Client.ReceiveTimeout = 1000
                Try
                    Dim bytes As Byte() = udpClient.Receive(groupEP)
                    If bytes(2) = 54 Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
                Return True
            End If
        Catch ex As Exception

        End Try


    End Function

    Public Function escribirVar2(ByVal mensaje As String) As Boolean

        Try
            'Antes de escribir variable, selecciono el programa 1
            Dim EscribioCartel As Boolean = False
            Dim intentos As Integer = 0
            While EscribioCartel = False
                EscribioCartel = CambioPrograma(1)
                intentos = intentos + 1
                If intentos > 50 Then
                    EscribioCartel = True
                End If
            End While
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {IT1, IT2, EV}

            'Variable
            Dim var0 As Decimal = 48 '0
            Dim var1 As Decimal = 50 '2

            'Agrego la variable 01, porque el cartel necesita 2 bytes para la variable
            AgregarByteTrama(sendBytes, var0)
            AgregarByteTrama(sendBytes, var1)

            'Recorro el mensaje y voy agregando caracter por caracter
            While mensaje.Length > 0
                Dim letraMsj As Decimal = Asc(mensaje.Substring(0, 1).ToString())
                AgregarByteTrama(sendBytes, letraMsj)
                mensaje = mensaje.Substring(1, mensaje.Length - 1)
            End While

            'Cálculo nro trama
            If Trama1 > 57 Then
                Trama1 = 48
                Trama0 = Trama0 + 1
            End If
            If Trama0 > 57 Then
                Trama0 = 50
            End If

            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            Trama1 = Trama1 + 1

            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)

            AgregarByteTrama(sendBytes, FT1)
            AgregarByteTrama(sendBytes, FT2)

            Dim boEnviar As Boolean = False

            If My.Computer.Network.Ping(oCartel.IP) Then
                boEnviar = True
            Else
                boEnviar = False
            End If

            If Not boEnviar Then
                If My.Computer.Network.Ping(oCartel.IP) Then
                    Dim udpClient As New Net.Sockets.UdpClient()
                    udpClient.Connect(oCartel.IP, oCartel.PORT)
                    udpClient.Send(sendBytes, sendBytes.Length)
                    Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                    udpClient.Client.ReceiveTimeout = 1000
                    Try
                        Dim bytes As Byte() = udpClient.Receive(groupEP)
                        If bytes(2) = 54 Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Else
                Dim udpClient As New Net.Sockets.UdpClient()
                udpClient.Connect(oCartel.IP, oCartel.PORT)
                udpClient.Send(sendBytes, sendBytes.Length)
                Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                udpClient.Client.ReceiveTimeout = 1000
                Try
                    Dim bytes As Byte() = udpClient.Receive(groupEP)
                    If bytes(2) = 54 Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
                Return True
            End If
        Catch ex As Exception

        End Try


    End Function

    Public Function escribirVar3(ByVal mensaje As String) As Boolean

        Try
            'Antes de escribir variable, selecciono el programa 1
            Dim EscribioCartel As Boolean = False
            Dim intentos As Integer = 0
            While EscribioCartel = False
                EscribioCartel = CambioPrograma(1)
                intentos = intentos + 1
                If intentos > 50 Then
                    EscribioCartel = True
                End If
            End While
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {IT1, IT2, EV}

            'Variable
            Dim var0 As Decimal = 48 '0
            Dim var1 As Decimal = 51 '1

            'Agrego la variable 01, porque el cartel necesita 2 bytes para la variable
            AgregarByteTrama(sendBytes, var0)
            AgregarByteTrama(sendBytes, var1)

            'Recorro el mensaje y voy agregando caracter por caracter
            While mensaje.Length > 0
                Dim letraMsj As Decimal = Asc(mensaje.Substring(0, 1).ToString())
                AgregarByteTrama(sendBytes, letraMsj)
                mensaje = mensaje.Substring(1, mensaje.Length - 1)
            End While

            'Cálculo nro trama
            If Trama1 > 57 Then
                Trama1 = 48
                Trama0 = Trama0 + 1
            End If
            If Trama0 > 57 Then
                Trama0 = 50
            End If

            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            Trama1 = Trama1 + 1

            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)

            AgregarByteTrama(sendBytes, FT1)
            AgregarByteTrama(sendBytes, FT2)

            Dim boEnviar As Boolean = False

            If My.Computer.Network.Ping(oCartel.IP) Then
                boEnviar = True
            Else
                boEnviar = False
            End If

            If Not boEnviar Then
                If My.Computer.Network.Ping(oCartel.IP) Then
                    Dim udpClient As New Net.Sockets.UdpClient()
                    udpClient.Connect(oCartel.IP, oCartel.PORT)
                    udpClient.Send(sendBytes, sendBytes.Length)
                    Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                    udpClient.Client.ReceiveTimeout = 1000
                    Try
                        Dim bytes As Byte() = udpClient.Receive(groupEP)
                        If bytes(2) = 54 Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Else
                Dim udpClient As New Net.Sockets.UdpClient()
                udpClient.Connect(oCartel.IP, oCartel.PORT)
                udpClient.Send(sendBytes, sendBytes.Length)
                Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                udpClient.Client.ReceiveTimeout = 1000
                Try
                    Dim bytes As Byte() = udpClient.Receive(groupEP)
                    If bytes(2) = 54 Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
                Return True
            End If
        Catch ex As Exception

        End Try


    End Function

    Public Function CambiarStep(ByVal nroPaso As String) As Boolean
        Try
            'Antes de escribir variable, selecciono el programa 1
            Dim EscribioCartel As Boolean = False
            Dim intentos As Integer = 0
            While EscribioCartel = False
                EscribioCartel = CambioPrograma(1)
                intentos = intentos + 1
                If intentos > 5 Then
                    EscribioCartel = True
                End If
            End While
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {IT1, IT2, CS}

            'Variable
            Dim NroStep0 As Decimal = 48 '0
            Dim NroStep1 As Decimal
            Select Case nroPaso
                Case "0" 'Fondo Negro Mensaje SISTEMA DETENIDO
                    NroStep1 = 48
                Case "1" 'Marco Blanco Mensaje ACA TIMBUES
                    NroStep1 = 49
                Case "2" 'Marco Verde Mensaje AVANCE
                    NroStep1 = 50
                Case "3" 'Marco Rojo Mensaje RETROCEDA
                    NroStep1 = 51
                Case "4" 'Marco Amarillo Mensaje ESPERE
                    NroStep1 = 52
                Case "5" 'Marco Blanco Mensaje Pasante Variable
                    NroStep1 = 53
                Case "6" 'TODO VERDE
                    NroStep1 = 54
                Case "7" 'TODO ROJO
                    NroStep1 = 55
                Case "8" 'FUERA DE SERVICIO
                    NroStep1 = 56
            End Select

            'Agrego la variable 01, porque el cartel necesita 2 bytes para la variable
            AgregarByteTrama(sendBytes, NroStep0)
            AgregarByteTrama(sendBytes, NroStep1)

            'Cálculo nro trama
            If Trama1 > 57 Then
                Trama1 = 48
                Trama0 = Trama0 + 1
            End If
            If Trama0 > 57 Then
                Trama0 = 50
            End If

            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            Trama1 = Trama1 + 1

            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)

            AgregarByteTrama(sendBytes, FT1)
            AgregarByteTrama(sendBytes, FT2)

            Dim boEnviar As Boolean = False

            If My.Computer.Network.Ping(oCartel.IP) Then
                boEnviar = True
            Else
                boEnviar = False
            End If

            If Not boEnviar Then
                If My.Computer.Network.Ping(oCartel.IP) Then
                    Dim udpClient As New Net.Sockets.UdpClient()
                    udpClient.Connect(oCartel.IP, oCartel.PORT)
                    udpClient.Send(sendBytes, sendBytes.Length)
                    Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                    udpClient.Client.ReceiveTimeout = 1000
                    Try
                        Dim bytes As Byte() = udpClient.Receive(groupEP)
                        If bytes(2) = 54 Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Else
                Dim udpClient As New Net.Sockets.UdpClient()
                udpClient.Connect(oCartel.IP, oCartel.PORT)
                udpClient.Send(sendBytes, sendBytes.Length)
                Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                udpClient.Client.ReceiveTimeout = 1000
                Try
                    Dim bytes As Byte() = udpClient.Receive(groupEP)
                    If bytes(2) = 54 Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
                Return True
            End If
        Catch ex As Exception

        End Try


    End Function

    Public Function fnComprobarEstado() As Boolean
        If My.Computer.Network.Ping(oCartel.IP) Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Establece en 2 min el tiempo del Watchdog, que hace que se vuelva al primer programa 
    ''' cuando no se le envíe nada al cartel por el tiempo establecido al principio (2 min).
    ''' Debería ejecutarse cada vez que se inicia el axControl.
    ''' </summary>
    ''' <returns></returns>
    Public Function EstablecerWatchDogTimer()
        Try
            'Antes de escribir variable, selecciono el programa 1
            Dim EscribioCartel As Boolean = False
            Dim intentos As Integer = 0
            While EscribioCartel = False
                EscribioCartel = CambioPrograma(1)
                intentos = intentos + 1
                If intentos > 50 Then
                    EscribioCartel = True
                End If
            End While
            'Creo el vector de bytes con los parametros para el inicio del mensaje
            Dim sendBytes() As Byte = {IT1, IT2, WDT}

            'Creo el decimal que ACTIVA el Watchdog Timer
            Dim Activado As Decimal = 49

            'Agrego el decimal de activación del Watchdog timer a la trama
            AgregarByteTrama(sendBytes, Activado)

            'Creo los minutos (dos bytes) para establecer tiempo de Watchdog timer
            Dim Min0 As Decimal = 48 '0x30 en HEX, es igual a 0
            Dim Min1 As Decimal = 49 '0x31 en HEX, es igual a 1

            'Agrego los minutos a la trama
            AgregarByteTrama(sendBytes, Min0)
            AgregarByteTrama(sendBytes, Min1)

            'Cálculo nro trama
            If Trama1 > 57 Then
                Trama1 = 48
                Trama0 = Trama0 + 1
            End If
            If Trama0 > 57 Then
                Trama0 = 50
            End If

            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            Trama1 = Trama1 + 1

            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)

            'Agrego el fin de la trama
            AgregarByteTrama(sendBytes, FT1)
            AgregarByteTrama(sendBytes, FT2)

            Dim boEnviar As Boolean = False

            If My.Computer.Network.Ping(oCartel.IP) Then
                boEnviar = True
            Else
                boEnviar = False
            End If

            If Not boEnviar Then
                If My.Computer.Network.Ping(oCartel.IP) Then
                    Dim udpClient As New Net.Sockets.UdpClient()
                    udpClient.Connect(oCartel.IP, oCartel.PORT)
                    udpClient.Send(sendBytes, sendBytes.Length)
                    Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                    udpClient.Client.ReceiveTimeout = 1000
                    Try
                        Dim bytes As Byte() = udpClient.Receive(groupEP)
                        If bytes(2) = 54 Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Else
                Dim udpClient As New Net.Sockets.UdpClient()
                udpClient.Connect(oCartel.IP, oCartel.PORT)
                udpClient.Send(sendBytes, sendBytes.Length)
                Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                udpClient.Client.ReceiveTimeout = 1000
                Try
                    Dim bytes As Byte() = udpClient.Receive(groupEP)
                    If bytes(2) = 54 Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Cambia el programa del cartel, se ejecuta siempre que se escribe una variable o se cambia el paso
    ''' </summary>
    ''' <returns></returns>
    Public Function CambioPrograma(NroPrograma As Integer)
        Try
            'Creo el vector de bytes con los parametros para el cambio de programa
            Dim sendBytes() As Byte = {IT1, IT2, CP}

            'Agrego los bytes del programa correspondiente
            Dim NroPrograma0 As Decimal = 48
            Dim NroPrograma1 As Decimal
            Select Case NroPrograma
                Case 0
                    NroPrograma1 = 48
                Case 1
                    NroPrograma1 = 49
            End Select

            'Agrego los nro de programa al vector
            AgregarByteTrama(sendBytes, NroPrograma0)
            AgregarByteTrama(sendBytes, NroPrograma1)

            'Cálculo nro trama
            If Trama1 > 57 Then
                Trama1 = 48
                Trama0 = Trama0 + 1
            End If
            If Trama0 > 57 Then
                Trama0 = 50
            End If

            'Agrego los números de trama
            AgregarByteTrama(sendBytes, Trama0)
            AgregarByteTrama(sendBytes, Trama1)
            Trama1 = Trama1 + 1

            'Calculo del checkSum
            Dim CheckSum As Integer = CalcularChesckSum(sendBytes)
            Dim asciiCheck1 As Integer
            Dim asciiCheck2 As Integer
            CheckSumTo2Byte(CheckSum, asciiCheck1, asciiCheck2)

            'Agrego el checksum a la trama
            AgregarByteTrama(sendBytes, asciiCheck1)
            AgregarByteTrama(sendBytes, asciiCheck2)

            AgregarByteTrama(sendBytes, FT1)
            AgregarByteTrama(sendBytes, FT2)

            Dim boEnviar As Boolean = False

            If My.Computer.Network.Ping(oCartel.IP) Then
                boEnviar = True
            Else
                boEnviar = False
            End If

            If Not boEnviar Then
                If My.Computer.Network.Ping(oCartel.IP) Then
                    Dim udpClient As New Net.Sockets.UdpClient()
                    udpClient.Connect(oCartel.IP, oCartel.PORT)
                    udpClient.Send(sendBytes, sendBytes.Length)
                    Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                    udpClient.Client.ReceiveTimeout = 1000
                    Try
                        Dim bytes As Byte() = udpClient.Receive(groupEP)
                        If bytes(2) = 54 Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Else
                Dim udpClient As New Net.Sockets.UdpClient()
                udpClient.Connect(oCartel.IP, oCartel.PORT)
                udpClient.Send(sendBytes, sendBytes.Length)
                Dim groupEP As New IPEndPoint(IPAddress.Parse(oCartel.IP), oCartel.PORT)
                udpClient.Client.ReceiveTimeout = 1000
                Try
                    Dim bytes As Byte() = udpClient.Receive(groupEP)
                    If bytes(2) = 54 Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Función para escribir las variables del control del cartel
    ''' </summary>
    ''' <param name="inNroPaso"></param>
    ''' <param name="strMensaje"></param>
    Public Sub fnEscribirCartel(inNroPaso As Integer, strMensaje As String)
        PasoActual = inNroPaso
        MensajeActual = strMensaje
    End Sub
#End Region

#Region "Enumerador"

    Public Enum Efecto
        TEXTO_PASANTE = 1
        TEXTO_ESTATICO = 2
    End Enum

#End Region

End Class
