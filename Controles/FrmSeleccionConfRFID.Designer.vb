﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSeleccionConfRFID
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbEntidad = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnIniciarLectura = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbEntidad
        '
        Me.cbEntidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(167, Byte), Integer), CType(CType(108, Byte), Integer))
        Me.cbEntidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbEntidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEntidad.ForeColor = System.Drawing.Color.White
        Me.cbEntidad.FormattingEnabled = True
        Me.cbEntidad.Location = New System.Drawing.Point(16, 36)
        Me.cbEntidad.Name = "cbEntidad"
        Me.cbEntidad.Size = New System.Drawing.Size(461, 28)
        Me.cbEntidad.TabIndex = 48
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(12, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(261, 24)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "Seleccione una configuración"
        '
        'btnIniciarLectura
        '
        Me.btnIniciarLectura.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(37, Byte), Integer))
        Me.btnIniciarLectura.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnIniciarLectura.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIniciarLectura.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIniciarLectura.ForeColor = System.Drawing.Color.White
        Me.btnIniciarLectura.Location = New System.Drawing.Point(145, 85)
        Me.btnIniciarLectura.Name = "btnIniciarLectura"
        Me.btnIniciarLectura.Size = New System.Drawing.Size(209, 38)
        Me.btnIniciarLectura.TabIndex = 49
        Me.btnIniciarLectura.Text = "Iniciar Lectura"
        Me.btnIniciarLectura.UseVisualStyleBackColor = False
        '
        'FrmSeleccionConfRFID
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Aqua
        Me.ClientSize = New System.Drawing.Size(499, 135)
        Me.Controls.Add(Me.btnIniciarLectura)
        Me.Controls.Add(Me.cbEntidad)
        Me.Controls.Add(Me.Label11)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "FrmSeleccionConfRFID"
        Me.Text = "Iniciar Lectura"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbEntidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnIniciarLectura As System.Windows.Forms.Button
End Class
