//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONFIG_LECTOR_RFID
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONFIG_LECTOR_RFID()
        {
            this.ANTENAS_RFID = new HashSet<ANTENAS_RFID>();
            this.PUERTO_RFID = new HashSet<PUERTO_RFID>();
        }
    
        public long ID_CONF_LECTOR_RFID { get; set; }
        public Nullable<int> ID_LECTOR_RFID { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<int> SELECT_READER_MODE { get; set; }
        public Nullable<int> SELECT_SEARCH { get; set; }
        public Nullable<int> SELECT_SESSION { get; set; }
        public Nullable<int> SELECT_REPORT_MODE { get; set; }
        public Nullable<bool> GPIO_PUERTO1 { get; set; }
        public Nullable<bool> GPIO_PUERTO2 { get; set; }
        public Nullable<bool> GPIO_PUERTO3 { get; set; }
        public Nullable<bool> GPIO_PUERTO4 { get; set; }
        public Nullable<bool> GPIO_HABILITADO { get; set; }
        public Nullable<int> RSSI { get; set; }
        public Nullable<bool> SELECCIONADO { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANTENAS_RFID> ANTENAS_RFID { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PUERTO_RFID> PUERTO_RFID { get; set; }
        public virtual LECTOR_RFID LECTOR_RFID { get; set; }
    }
}
