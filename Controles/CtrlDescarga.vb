﻿Imports System.Text
Imports Entidades
Imports Entidades.Constante
Imports Negocio

Public Class CtrlDescarga

#Region "Variables de Instancia"
    Dim oPLC As PLC
    Dim oPLCN As PlcN
    ''' <summary>
    ''' 1= Habilita la Lectura del TAG a la salida de la Descarga. 
    ''' AxControl lo lee y lo pone a 0
    ''' </summary>
    Public oHab_LecturaRfid_Salida As DATO_WORDINT
    ''' <summary>
    ''' 1= Ingreso de Camión OK(conforme para descargar) 
    ''' 2=Ingreso de Camión NO OK(no conforme para descargar). 
    ''' Está variable es escrita por axControl, 
    ''' luego el PLC la procesa y la pone en 0
    ''' </summary>
    Public oNuevoCamion As DATO_WORDINT

    Dim oDatoIntN As DatoWordIntN
    'Dim oHabilitacion As DatoBool
    ' Private WithEvents ofrmBarrera As frmBarrera

    'DATOS DEL CAMIÓN
    Public oPatenteChasis As DATO_BYTE
    Public oPatenteAcoplado As DATO_BYTE
    Public oTurno As DATO_DWORDDINT
    Public oSector As DATO_WORDINT
    Public oProducto As DATO_BYTE
    Public oMotivo As DATO_BYTE
    Public oHabilitado As DATO_BOOL
    Public oCalidad As DATO_BYTE

#End Region

#Region "Constante"



#End Region



#Region "Eventos"
    Public Event UsoManual()
#End Region

#Region "Métodos"
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oPLC = New PLC
        oPLCN = New PlcN

        'oEstado = New DatoWordInt
        'oComando = New DatoWordInt
        'oInfrarrojo = New DatoWordInt
        oDatoIntN = New DatoWordIntN
        'ofrmBarrera = New frmBarrera

    End Sub

    Public Overrides Sub Inicializar(ID_PLC As Integer?, ID_CONTROL_ACCESO As Integer, ID_SECTOR As Integer)
        Me.ID_PLC = ID_PLC
        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_SECTOR = ID_SECTOR

        oPLC = oPLCN.GetOne(Me.ID_PLC)
        If oPLC Is Nothing Then
            Me.EP.SetError(Me.lblTAG, "No existe el PLC " & PLC & ". Revise la configuración en axDriverS7.")
        Else
            Me.lblTAG.Text = Me.Name

            Dim nDatoBool As New Negocio.DatoBoolN
            Dim listDatoBool As New List(Of DATO_BOOL)

            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWord As New List(Of DATO_WORDINT)

            Dim nDatoDWord As New Negocio.DatoDWordDIntN
            Dim listDatoDWord As New List(Of DATO_DWORDDINT)

            Dim nDatoByte As New Negocio.DatoByteN
            Dim listDatoByte As New List(Of DATO_BYTE)

            'Busco los tag en el axDriver
            listDatoBool = nDatoBool.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)
            listDatoWord = nDatoWord.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)
            listDatoDWord = nDatoDWord.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)
            listDatoByte = nDatoByte.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)

            For Each itemTag In listDatoBool
                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
                If tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.HABILITADO) Then
                    oHabilitado = itemTag
                End If


            Next

            For Each itemTag As Entidades.DATO_WORDINT In listDatoWord
                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
                If tag.Equals(PROPIEDADES_TAG_PLC.HAB_LecturaRFID_SALIDA) Then
                    oHab_LecturaRfid_Salida = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC.NVO_CAMION) Then
                    oNuevoCamion = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.SECTOR) Then
                    oSector = itemTag

                End If
            Next

            For Each itemTag As Entidades.DATO_DWORDDINT In listDatoDWord
                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
                If tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.TURNO) Then
                    oTurno = itemTag

                End If
            Next

            For Each itemTag As Entidades.DATO_BYTE In listDatoByte
                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
                If tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.PATENTE_CHASIS) Then
                    oPatenteChasis = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.PATENTE_ACOPLADO) Then
                    oPatenteAcoplado = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.PRODUCTO) Then
                    oProducto = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.MOTIVO) Then
                    oMotivo = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC_DATOS_DESCARGA.calidad) Then
                    oCalidad = itemTag
                End If
            Next





            Dim tmp As String
            If IsNothing(oHab_LecturaRfid_Salida) And IsNothing(oNuevoCamion) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7", Me.Tag, PROPIEDADES_TAG_PLC.ESTADO, Me.PLC)
                Me.EP.SetError(Me.lblTAG, tmp)
                ' Negocio.modDelegado.SetEnable(Me, btnConfig, False)
                Return
            End If



            Me.Inicializado = True
        End If


    End Sub

    ''' <summary>
    ''' Permite actualizar el estado de las variables de la descarga
    ''' </summary>
    Public Sub SetEstado()
        If IsNothing(oHab_LecturaRfid_Salida) Then
            Inicializar(Me.ID_PLC, Me.ID_CONTROL_ACCESO, Me.ID_SECTOR)
            Return
        End If

        oHab_LecturaRfid_Salida = oDatoIntN.GetOne(ID_SECTOR, oHab_LecturaRfid_Salida.TAG)
        modDelegado.SetText(Me, txtHab_LecturaRFID_Salida, oHab_LecturaRfid_Salida.VALOR)
        modDelegado.SetTextLabel(lblHabLectura, lblHabLectura, oHab_LecturaRfid_Salida.VALOR)
        'Dim nDatoBool As New Negocio.DatoBoolN





    End Sub





    Public Sub EjectComandoNvoCamion(Comando As Entidades.Constante.DescargaNuevoCamionComando, axRespuesta As Entidades.AxRespuesta)
        Dim nDatoBool As New Negocio.DatoBoolN
        Dim nDatoWord As New Negocio.DatoWordIntN
        Dim nDatoDWord As New Negocio.DatoDWordDIntN
        Dim nDatoByte As New Negocio.DatoByteN

        nDatoWord.Escribir(Me.oNuevoCamion.ID, Comando)

        Dim oDatosDescarga = axRespuesta.datosDescarga
        If Not IsNothing(oDatosDescarga) Then
            Me.oPatenteChasis.VALOR = Encoding.ASCII.GetBytes(oDatosDescarga.patenteChasis).ToArray()
            Me.oPatenteAcoplado.VALOR = Encoding.ASCII.GetBytes(oDatosDescarga.patenteAcoplado).ToArray()
            Me.oProducto.VALOR = Encoding.ASCII.GetBytes(oDatosDescarga.producto).ToArray()
            Me.oCalidad.VALOR = Encoding.ASCII.GetBytes(oDatosDescarga.calidad).ToArray()
            Me.oMotivo.VALOR = Encoding.ASCII.GetBytes(axRespuesta.datosDescarga.motivo).ToArray()

            nDatoByte.Escribir(Me.oPatenteChasis.ID, Me.oPatenteChasis.VALOR)
            nDatoByte.Escribir(Me.oPatenteAcoplado.ID, Me.oPatenteAcoplado.VALOR)
            'nDatoDWord.Escribir(Me.oTurno.ID, oDatosDescarga.turno)
            nDatoWord.Escribir(Me.oSector.ID, oDatosDescarga.idSector)
            nDatoByte.Escribir(Me.oProducto.ID, Me.oProducto.VALOR)
            nDatoByte.Escribir(Me.oMotivo.ID, Me.oMotivo.VALOR)
            nDatoByte.Escribir(Me.oCalidad.ID, Me.oCalidad.VALOR)
            nDatoBool.Escribir(Me.oHabilitado.ID, Comando = Entidades.Constante.DescargaNuevoCamionComando.INGRESO_OK)

        End If

    End Sub
    Public Function getMotivoDescarga(motivo As Entidades.Constante.AxRespuestaMotivo) As String
        Select Case motivo
            Case 0
                Return "OK"
            Case Entidades.Constante.AxRespuestaMotivo.MAL_POSICIONADO
                Return "CAMION MAL POSICIONADO"
            Case Entidades.Constante.AxRespuestaMotivo.SIN_ALTA
                Return "SIN ALTA"
            Case Else
                Return "SIN MOTIVO"
        End Select
    End Function

    ''' <summary>
    ''' Permite volver a 0 el tag del plc que habilita la lectura RFID
    ''' </summary>
    Public Sub DesactivarLecturaRFID_PLC()
        Dim nDatoWord As New Negocio.DatoWordIntN
        nDatoWord.Escribir(Me.oHab_LecturaRfid_Salida.ID, 0)

    End Sub

#Region "Delegados"
    Delegate Sub SetFrmEstadoCallback(ByVal [frm] As System.Windows.Forms.Form, ByVal [estado] As Integer)


#End Region

#End Region

End Class
