﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace Entidades
{

    public class AxAnprHikVision
    {
        ax_ControlEntities db = new ax_ControlEntities();
        Thread threadAnpr;
        public static Constante constantes;
        configuracion_anpr confANPR;
        public enum tipoEvento
        {
            informacion = 1,
            error = 2
        }

        public delegate void EventoEventHandler(string evento, tipoEvento tipoEvento);
        public event EventoEventHandler evento;

        public delegate void PatenteLeidaEventHandler(string patente, string nombreCamara);
        public event PatenteLeidaEventHandler patenteLeida;



        public string sourcePath { get; set; }
        /// <summary>
        /// Direccion de la carpeta donde se ponen las imagenes procesadas con patente OK
        /// </summary>
        public string targetPathOK { get; set; }
        /// <summary>
        /// Dirección de la patente de la imagenes que NO tieneN patente
        /// </summary>
        public string targetPathNO { get; set; }

        public AxAnprHikVision()
        {
            this.confANPR = db.configuracion_anpr.FirstOrDefault();
            this.sourcePath = @"" + confANPR.rutaCapturas + "";
            this.targetPathOK = @"" + confANPR.rutaCapturasProcesadasOK + "";
            this.targetPathNO = @"" + confANPR.rutaCapturasProcesadasNO + "";

            using (new NetworkConnection(confANPR.rutaCapturas, new NetworkCredential(confANPR.usuarioNas, confANPR.passwordNas)))
            {
                // Creo las carpetas para las imagenes OK y NO
                if (!Directory.Exists(targetPathOK))
                {
                    Directory.CreateDirectory(targetPathOK);
                }

                if (!Directory.Exists(targetPathNO))
                {
                    Directory.CreateDirectory(targetPathNO);
                }
            }
               
          

        }


        public void startAnpr()
        {
            if (threadAnpr == null) threadAnpr = new Thread(runPatentesLeidas);

            if (threadAnpr.IsAlive) threadAnpr.Abort();

            threadAnpr.IsBackground = true;
            threadAnpr.Name = "runAnpr";
            threadAnpr.Start();

        }

        /// <summary>
        /// Permite obtener la pantente leida por las camaras
        /// </summary>
        /// <param name="nroCamara"></param>
        /// <returns>
        ///  ""(vacio) Si no leyo ninguna pante 
        /// </returns>
        public string pantenteLeida(string nombreCamaraBuscar)
        {
            //buscarPatenteLeida(nombreCamaraBuscar);
            return "";
        }


        private void runPatentesLeidas()
        {
            while (true)
            {
                try
                {
                    ////Creo la credenciales
                    //NetworkCredential credenciales = new NetworkCredential("admin", "axgolden");
                    ////Guardo las credenciales en la cache
                    //CredentialCache credencialesCache = new CredentialCache();
                    //credencialesCache.Add("\\192.168.226.19", "Negotiate", "Basic", credenciales);

                    //Busco la configuración del sistema anpr
                    var confANPR = db.configuracion_anpr.FirstOrDefault();
                    using (new NetworkConnection(confANPR.rutaCapturas, new NetworkCredential(confANPR.usuarioNas, confANPR.passwordNas)))
                    {
                        //File.Copy(@"\\server\read\file", @"\\server2\write\file");



                        string petenteReturn = "";



                        //Pregunto si esta habilitado el sistema
                        //if (confANPR.habiltadoLeer == true)
                        //{
                        //    evento("SISTEMA ANPR NO HABILITADO", tipoEvento.informacion);
                        //    //return "";
                        //}
                        String ruta = confANPR.rutaCapturas;

                        //Busco las imagenes en una carpeta predefinida
                        DirectoryInfo di = new DirectoryInfo(@"" + ruta + "");

                        Console.WriteLine("Imagenes encontradas en el directorio:");
                        var f1 = di.GetFiles("*" + confANPR.formatoImagen + "*");

                        //Busco las fotos tomadas camara ordenado fecha descendente
                        FileInfo[] files = di.GetFiles("*" + confANPR.formatoImagen + "*").OrderByDescending(o => o.CreationTime).ToArray();

                        //Recorro las imagenes capturados 
                        foreach (var fi in files)
                        {
                            string fecha = "";
                            string patente = "";
                            string nombreCamara;

                            //el dato completo va a ser el nombre de la imagen, menos la extención
                            var datoCompleto = fi.Name.Split('.');

                            var descripcion = datoCompleto[0];
                            //A la descripción la divido en tres partes: fecha, camara y patente.
                            var datos = descripcion.Split('_');
                            fecha = datos[0];

                            string fechaCompleta;

                            string año = fecha.Substring(0, 4);
                            string mes = fecha.Substring(4, 2);
                            string dia = fecha.Substring(6, 2);
                            string hora = fecha.Substring(8, 2);
                            string minutos = fecha.Substring(10, 2);

                            //Tomo el valor de camara
                            nombreCamara = datos[1];
                            //Tomo el valor de patente
                            patente = datos[2];


                            bool esPatente = esUnaPatente(patente);

                            if (esPatente) // si nombre del archivo tiene una patente
                            {
                                //if (nombreCamara == nombreCamaraBuscar) // la patente es de la camara que estoy consultando
                                //{
                                //    //Guardo la patente como respuesta del metodo
                                //    petenteReturn = patente;
                                //    //Salgo del for
                                //    break;
                                //}

                                patenteLeida(patente, nombreCamara);

                            }

                            //Creo el hilo para mover todas las imagenes de la carpeta

                            moverImagen(fi.FullName, fi.Name, esPatente);
                        }
                    }
                } //Try
                catch (Exception ex)
                {
                    evento(ex.StackTrace, tipoEvento.error);

                }
                Thread.Sleep(1000); // 1 segundo
            } // while

        }



        private bool esUnaPatente(string patente)
        {
            try
            {
                if (patente.Length > 5 && patente.Length < 9)
                {

                    char[] arr;
                    arr = patente.ToCharArray(0, patente.Length);

                    switch (patente.Length)
                    {
                        case 6:
                            //Ej: ABC123
                            if (!Char.IsLetter(arr[0])) { return false; }
                            if (!Char.IsLetter(arr[1])) { return false; }
                            if (!Char.IsLetter(arr[2])) { return false; }
                            if (!Char.IsDigit(arr[3])) { return false; }
                            if (!Char.IsDigit(arr[4])) { return false; }
                            if (!Char.IsDigit(arr[5])) { return false; }

                            break;
                        case 7:

                            //Ej: AA123BB
                            //o
                            //Ej: ABCd678


                            if (!Char.IsLetter(arr[0])) { return false; }
                            if (!Char.IsLetter(arr[1])) { return false; }


                            if (Char.IsLetter(arr[2]))
                            {
                                string letra = arr[3].ToString();
                                if (letra != "D" && letra != "T" && letra != "C" && letra != "Q" && letra != "S" && letra != "8" && letra != "9") { return false; }
                                if (!Char.IsDigit(arr[4])) { return false; }
                                if (!Char.IsDigit(arr[5])) { return false; }
                                if (!Char.IsDigit(arr[6])) { return false; }
                            }
                            else
                            {
                                if (!Char.IsDigit(arr[3])) { return false; }
                                if (!Char.IsDigit(arr[4])) { return false; }
                                if (!Char.IsLetter(arr[5])) { return false; }
                                if (!Char.IsLetter(arr[6])) { return false; }
                            }
                            break;
                        case 8:
                            //Ej: AA123dBB
                            if (!Char.IsLetter(arr[0])) { return false; }
                            if (!Char.IsLetter(arr[1])) { return false; }
                            if (!Char.IsDigit(arr[2])) { return false; }
                            if (!Char.IsDigit(arr[3])) { return false; }
                            if (!Char.IsDigit(arr[4])) { return false; }
                            string letra2 = arr[5].ToString();
                            if (letra2 != "D" && letra2 != "T" && letra2 != "C" && letra2 != "Q" && letra2 != "S" && letra2 != "8" && letra2 != "9") { return false; }
                            if (!Char.IsLetter(arr[6])) { return false; }
                            if (!Char.IsLetter(arr[7])) { return false; }
                            break;
                    }
                }
                else
                {
                    constantes = new Constante();
                    constantes.GenerarEvento("NO es una patente: " + patente, 116, Convert.ToInt16(Constante.TipoEventoLog.FailureAudit));
                    return false;
                }
                constantes = new Constante();
                constantes.GenerarEvento("Es una patente: " + patente, 116, Convert.ToInt16(Constante.TipoEventoLog.SuccessAudit));
                return true;
            }
            catch (Exception ex)
            {
                string mensaje = String.Format("ORIGEN: {1}{0}DETALLE: {2}{0}", Environment.NewLine, ex.StackTrace, ex.Message);
                constantes = new Constante();
                constantes.GenerarEvento(mensaje, 1016, Convert.ToInt16(Constante.TipoEventoLog.Error));
                return false;
            }

        }

        /// <summary>
        /// Metodo que permite mover las imagenes 
        /// </summary>
        /// <param name="nombreArchivoCompleto"> Nombre de archivo a mover</param>
        /// <param name="esUnaPatente"> bool si es una patente</param>
        private void moverImagen(string nombreArchivoCompleto, string nombreArchivo, bool esPatente)
        {


            string destFile = "";
            if (esPatente)
            {
                destFile = Path.Combine(targetPathOK, nombreArchivo);
            }
            else
            {
                destFile = Path.Combine(targetPathNO, nombreArchivo);
            }

            File.Move(nombreArchivoCompleto, destFile);


            //string[] sourcefiles = Directory.GetFiles(this.sourcePath);
            //string destFile = "";

            //foreach (string sourcefile in sourcefiles)
            //{

            //    string fileName = Path.GetFileName(sourcefile);
            //    var datoCompleto = fileName.Split('.');


            //    var descripcion = datoCompleto[0];
            //    var datos = descripcion.Split('_');

            //    Boolean result = esUnaPatente(datos[2]);
            //    if (result)
            //    {
            //        destFile = Path.Combine(targetPathOK, fileName);
            //    }
            //    else
            //    {
            //        destFile = Path.Combine(targetPathNO, fileName);
            //    }


            //    File.Move(sourcefile, destFile);
            //}





        }

        public class PantenteANPR
        {
            public DateTime fecha { get; set; }
            public string patente { get; set; }
            public string nombreCamara { get; set; }

        }

    }
}
