﻿Public Class CamaraN

    Public Function GetOne(ByVal Tag As String, ByVal idSector As Int32) As Entidades.Camara
        Dim oCamaraAD As New AccesoDatos.CamaraAD

        Return oCamaraAD.getOne(Tag, idSector)
    End Function

    Public Function GetOne(ByVal Id As Integer) As Entidades.camara
        Dim oCamaraAD As New AccesoDatos.CamaraAD
        Return oCamaraAD.getOne(Id)
    End Function

    Public Function Update(ByVal oCamara As Entidades.camara) As Entidades.camara
        Dim oCamaraAD As New AccesoDatos.CamaraAD
        Return oCamaraAD.update_config(oCamara)
    End Function

End Class
