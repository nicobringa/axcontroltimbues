﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class ErrorBD
    {
       private static string _msjError = null;

        public static string getMsjError()
        {
            string msj = _msjError;
            _msjError = null;
            return msj;
        }

        public static void setMsjError( string msj)
        {
            _msjError = msj;
        }
       
    }

    
}
