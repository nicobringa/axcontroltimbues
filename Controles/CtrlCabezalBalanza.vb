﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio

Imports System.Threading

Public Class CtrlCabezalBalanza

    Private COLOR_ERROR As Color = Color.Red
    Private COLOR_HABILITADO As Color = Color.GreenYellow
    Private COLOR_OCUPADA As Color = Color.Gray
    Private COLOR_MANUAL As Color = Color.Green
    Private COLOR_DESHABILITADO As Color = Color.Red
#Region "Variables de Instancia"
    Dim oPLC As Plc
    Dim oPLCN As PlcN

    Public oPeso As DATO_DWORDDINT
    Private _numeroBalanza As Integer
    Public Property numeroBalanza() As Integer
        Get
            Return _numeroBalanza
        End Get
        Set(ByVal value As Integer)
            _numeroBalanza = value
        End Set
    End Property
    Public oTiempoEstabilidad As DATO_WORDINT
    Public oDiferenciaEstabilidad As DATO_WORDINT
    Public oPesoMaximo As DATO_WORDINT
    Public oEstado As DATO_WORDINT
    Public oEstadoOld As DATO_WORDINT
    Public oComando As DATO_WORDINT

    Public oPesoNeto As DATO_BOOL
    Public oPesoNegativo As DATO_BOOL
    Public oFueraRango As DATO_BOOL
    Public oFueraEquilibrio As DATO_BOOL
    Public oPesoMayor500 As DATO_BOOL
    Public oPesoEstable As DATO_BOOL
    Public oPesoMenorDifEstabilidad As DATO_BOOL
    Public oPesoMaximoBal As DATO_BOOL
    Public oError As DATO_BOOL


    '  Dim oHabilitacion As DATO_WORDINT
    ' Dim oReset As DATO_BOOL

    ' Dim oDatoFloatN As DatoFloatN
    Dim validacionPesoBalanza As Boolean = False
    Dim PesoValidado As Decimal = 0
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Dim CtrlBalanza1 As New CtrlDinamismoBalanza

#End Region


#Region "PROPIEDADES"

#End Region


#Region "CONSTRUCTOR"
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oPLC = New PLC
        oPLCN = New PlcN

    End Sub
#End Region

#Region "EVENTOS FORM"

#End Region


#Region "METODOS"

    Public Overrides Sub Inicializar(ID_PLC As Integer?, ID_CONTROL_ACCESO As Integer, ID_SECTOR As Integer)
        Me.ID_PLC = ID_PLC
        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_SECTOR = ID_SECTOR

        oPLC = oPLCN.GetOne(Me.ID_PLC)

        If oPLC Is Nothing Then
            Me.EP.SetError(Me.lbTAG, "No existe el PLC " & PLC & ". Revise la configuración en axDriverS7.")
        Else
            Me.lbTAG.Text = Me.Name
            CargarTag()

            Dim tmp As String = "No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7"
            If IsNothing(oEstado) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.ESTADO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Negocio.modDelegado.SetEnable(Me, btnConfig, False)
                Return
            End If

            If IsNothing(oEstadoOld) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.ESTADO_OLD, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Negocio.modDelegado.SetEnable(Me, btnConfig, False)
                Return
            End If

            If IsNothing(oComando) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.ESTADO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPeso) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoMaximo) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_MAX, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oDiferenciaEstabilidad) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.DIFERENCIA_ESTABILIDAD, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oTiempoEstabilidad) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.TIEMPO_ESTABILIDAD, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoNeto) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_NETO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoNegativo) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_NEGATIVO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oFueraRango) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.FUERA_RANGO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oFueraEquilibrio) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.FUERA_EQUILIBRIO, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoMayor500) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_MAYOR_500, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoEstable) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_ESTABLE, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoMenorDifEstabilidad) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_MENOR_DIF_ESTABILIDAD, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oPesoMaximoBal) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.PESO_MENOR_DIF_ESTABILIDAD, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            If IsNothing(oError) Then
                tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.ERROR_BAL, Me.PLC)
                Me.EP.SetError(Me.btnConfig, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If
            'If IsNothing(oHabilitacion) Then
            '    tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.HABILITACION, Me.PLC)
            '    Me.EP.SetError(Me.btnConfig, tmp)
            '    Me.btnConfig.Enabled = False
            '    Return
            'End If

            'If IsNothing(oReset) Then
            '    tmp = String.Format(tmp, Me.Tag, PROPIEDADES_TAG_PLC.RESET, Me.PLC)
            '    Me.EP.SetError(Me.btnConfig, tmp)
            '    Me.btnConfig.Enabled = False
            '    Return
            'End If


            Me.Inicializado = True

        End If

    End Sub

    Private Sub CargarTag()

        Dim oTagDatoWords As New List(Of DATO_WORDINT)
        Dim oTagDatoWordIntN As New Negocio.DatoWordIntN

        oTagDatoWords = oTagDatoWordIntN.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)

        For Each itemTag As Entidades.DATO_WORDINT In oTagDatoWords
            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)

            If tag.Equals(PROPIEDADES_TAG_PLC.ESTADO) Then
                oEstado = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.ESTADO_OLD) Then
                oEstadoOld = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.COMANDO) Then
                oComando = itemTag
                'ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO) Then
                '    oPeso = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO_MAX) Then
                oPesoMaximo = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.TIEMPO_ESTABILIDAD) Then
                oTiempoEstabilidad = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.DIFERENCIA_ESTABILIDAD) Then
                oDiferenciaEstabilidad = itemTag
            End If


        Next

        'Dim oTagDatoFloats As New List(Of DatoFloat)
        'Dim oTagDatoFloatN As New Negocio.DatoFloatN

        'oTagDatoFloats = oTagDatoFloatN.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)

        Dim oTagDatosBools As New List(Of DATO_BOOL)
        Dim nTagDatoBool As New Negocio.DatoBoolN

        oTagDatosBools = nTagDatoBool.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)
        For Each itemTag As Entidades.DATO_BOOL In oTagDatosBools
            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
            If tag.Equals(PROPIEDADES_TAG_PLC.PESO_NETO) Then
                oPesoNeto = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO_NEGATIVO) Then
                oPesoNegativo = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.FUERA_RANGO) Then
                oFueraRango = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.FUERA_EQUILIBRIO) Then
                oFueraEquilibrio = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO_MAYOR_500) Then
                oPesoMayor500 = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO_ESTABLE) Then
                oPesoEstable = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO_MENOR_DIF_ESTABILIDAD) Then
                oPesoMenorDifEstabilidad = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.PESO_MAXIMO_BAL) Then
                oPesoMaximoBal = itemTag
            ElseIf tag.Equals(PROPIEDADES_TAG_PLC.ERROR_BAL) Then
                oError = itemTag
            End If
        Next

        Dim oTagDatoDWords As New List(Of DATO_DWORDDINT)
        Dim oTagDatoDWordIntN As New Negocio.DatoDWordDIntN

        oTagDatoDWords = oTagDatoDWordIntN.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)

        For Each itemTag As Entidades.DATO_DWORDDINT In oTagDatoDWords
            Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
            If tag.Equals(PROPIEDADES_TAG_PLC.PESO) Then
                oPeso = itemTag
            End If
        Next

    End Sub

    Public Sub Refrescar()
        Dim oDatoIntN As New Negocio.DatoWordIntN
        Dim oDatoDIntN As New Negocio.DatoDWordDIntN
        Dim oDatoBoolN As New Negocio.DatoBoolN
        If IsNothing(oEstado) Then
            Inicializar(Me.ID_PLC, Me.ID_CONTROL_ACCESO, Me.ID_SECTOR)
            Return
        End If
        oEstado = oDatoIntN.GetOne(ID_SECTOR, oEstado.TAG)
        oEstadoOld = oDatoIntN.GetOne(ID_SECTOR, oEstadoOld.TAG)
        ' Me.oHabilitacion = oDatoIntN.GetOne(oHabilitacion.id)
        'Actualizar estado de la balanza
        SetEstado(oEstado.valor)
        SetEstadoOld(oEstadoOld.valor)
        'Dim oDatoFloatN As New Negocio.DatoFloatN
        oPeso = oDatoDIntN.GetOne(oPeso.ID)
        modDelegado.SetText(Me, txtPeso, oPeso.valor & " Kg")
        oPesoMaximo = oDatoIntN.GetOne(oPesoMaximo.id)
        Dim PorcentajePeso As Decimal = 0

        If oPesoMaximo.valor > 0 Then PorcentajePeso = (oPeso.valor * 100) / oPesoMaximo.valor
        CtrlProgressBalanza1.SetProgressPeso(PorcentajePeso, Color.Gray)


        oPesoNeto = oDatoBoolN.GetOne(oPesoNeto.id)
        oPesoNegativo = oDatoBoolN.GetOne(oPesoNegativo.id)
        oFueraRango = oDatoBoolN.GetOne(oFueraRango.id)
        oFueraEquilibrio = oDatoBoolN.GetOne(oFueraEquilibrio.id)
        oPesoMayor500 = oDatoBoolN.GetOne(oPesoMayor500.id)
        oPesoEstable = oDatoBoolN.GetOne(oPesoEstable.id)
        oPesoMenorDifEstabilidad = oDatoBoolN.GetOne(oPesoMenorDifEstabilidad.id)
        oPesoMaximoBal = oDatoBoolN.GetOne(oPesoMaximoBal.id)
        oError = oDatoBoolN.GetOne(oError.id)
    End Sub

    Public Sub EjectComando(comando As Entidades.Constante.BalanzaComando)
        Dim nDatoWord As New Negocio.DatoWordIntN
        nDatoWord.Escribir(Me.oComando.id, comando)
    End Sub

    Public Sub SetEstado(ByVal EstadoBalanza As Entidades.ConstanteBalanza.EstadoBalanza)
        Dim TextEstado As String = ""

        'If oHabilitacion.valor = Constante.BalanzaHabilitacion.Deshabilitada Then
        '    Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Red)
        '    TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), "DESHABILITADA")
        '    Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)
        '    Return
        'End If

        If EstadoBalanza = Entidades.Constante.BalanzaEstado.BarrerasCerrada Then ' = 0

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_HABILITADO)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), "HABILITADA")
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.HabilitadaIngresarBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.HabilitadaIngresarBarrera2 Then

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.HABILITADA_INGRESAR)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.CierreBarreraEntradaBar1 Or
       EstadoBalanza = Entidades.Constante.BalanzaEstado.CierreBarreraEntradaBar2 Then

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.CIERRE_BARRERA_ENTRADA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.ErrorBalanza1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.ErrorBalanza2 Then '<--- BALANZA EN CERO O NO CONECTADA (15-65)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_ERROR)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.BALANZA_CERO_NO_CONECTADA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.AvanzarCamionBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.AvanzarCamionBarrera2 Then '<--- AVANZA CAMION(20-70)


            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.AVANZA_CAMION)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.RetrocedaCamionBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.RetrocedaCamionBarrera2 Then '<--- RETROCEDA CAMION(25-75)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.RETROCEDA_CAMION)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.EstabilizandoPesoBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.EstabilizandoPesoBarrera2 Then '<--- ESTABILIZANDO PESO(30-80)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.ESTABILIZANDO_PESO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)


        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.PesoMaximoBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.PesoMaximoBarrera2 Then '<---PESO MAXIMO(33-85)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_ERROR)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.PESO_MAXIMO_ALZANZADO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.TomarPesoBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.TomarPesoBarrera2 Then ' <--- TOMAR PESO(35-90)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.TOMAR_PESO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.PermitirSalirBarrera1 Or
     EstadoBalanza = Entidades.Constante.BalanzaEstado.PermitirSalirBarrera2 Then ' <--- PERMITIR SALIR

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.PERMITIR_SALIR_BALANZA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)


        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.BajaBarrera1Salida Or EstadoBalanza = Entidades.Constante.BalanzaEstado.BajaBarrera2Salida Then '<--- BAJAR BARRERA (45-100)

            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_OCUPADA)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.BAJA_BARRERA_SALIDA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.Manual Then
            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_MANUAL)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.MANUAL)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.Deshabilitada Then
            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, COLOR_DESHABILITADO)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.DESHABILITADO)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)
        End If


    End Sub

    Public Sub SetEstadoOld(ByVal EstadoBalanza As Entidades.ConstanteBalanza.EstadoBalanza)
        Dim TextEstado As String = ""

        'If oHabilitacion.valor = Constante.BalanzaHabilitacion.Deshabilitada Then
        '    Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Red)
        '    TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), "DESHABILITADA")
        '    Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.White)
        '    Return
        'End If

        If EstadoBalanza = Entidades.Constante.BalanzaEstado.BarrerasCerrada Then ' = 0
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), "HABILITADA")

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.HabilitadaIngresarBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.HabilitadaIngresarBarrera2 Then
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.HABILITADA_INGRESAR)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.CierreBarreraEntradaBar1 Or
       EstadoBalanza = Entidades.Constante.BalanzaEstado.CierreBarreraEntradaBar2 Then
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.CIERRE_BARRERA_ENTRADA)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.ErrorBalanza1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.ErrorBalanza2 Then '<--- BALANZA EN CERO O NO CONECTADA (15-65)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.BALANZA_CERO_NO_CONECTADA)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.AvanzarCamionBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.AvanzarCamionBarrera2 Then '<--- AVANZA CAMION(20-70)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.AVANZA_CAMION)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.RetrocedaCamionBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.RetrocedaCamionBarrera2 Then '<--- RETROCEDA CAMION(25-75)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.RETROCEDA_CAMION)


        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.EstabilizandoPesoBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.EstabilizandoPesoBarrera2 Then '<--- ESTABILIZANDO PESO(30-80)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.ESTABILIZANDO_PESO)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.PesoMaximoBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.PesoMaximoBarrera2 Then '<---PESO MAXIMO(33-85)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.PESO_MAXIMO_ALZANZADO)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.TomarPesoBarrera1 Or
            EstadoBalanza = Entidades.Constante.BalanzaEstado.TomarPesoBarrera2 Then ' <--- TOMAR PESO(35-90)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.TOMAR_PESO)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.PermitirSalirBarrera1 Or
     EstadoBalanza = Entidades.Constante.BalanzaEstado.PermitirSalirBarrera2 Then ' <--- PERMITIR SALIR

            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.PERMITIR_SALIR_BALANZA)
            Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstado, Color.Black)


        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.BajaBarrera1Salida Or EstadoBalanza = Entidades.Constante.BalanzaEstado.BajaBarrera2Salida Then '<--- BAJAR BARRERA (45-100)
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.BAJA_BARRERA_SALIDA)

        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.Manual Then
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.MANUAL)


        ElseIf EstadoBalanza = Entidades.Constante.BalanzaEstado.Deshabilitada Then
            TextEstado = String.Format("{0} - {1}", CType(EstadoBalanza, Integer), Entidades.ConstanteBalanza.DESHABILITADO)

        End If

        TextEstado = "Estado annterior: " & TextEstado
        Negocio.modDelegado.SetTextLabel(Me, TextEstado, lblEstadoOld, Color.White)
    End Sub

    Private Sub btnConfig_Click(sender As Object, e As EventArgs) Handles btnConfig.Click

        Dim frm As New FrmBalanza(Me)
        frm.ShowDialog()


    End Sub


    ''' <summary>
    ''' Permite realizar un reset en la balanza para que vuelva al estado barrera cerrada
    ''' </summary>
    Public Sub Reset()
        If Not IsNothing(oComando) Then
            Dim nDatoWord As New Negocio.DatoWordIntN
            nDatoWord.Escribir(oComando.id, Constante.BalanzaComando.Reset)
        End If
    End Sub


#End Region

#Region "SUBPROCESOS"

#End Region






End Class
