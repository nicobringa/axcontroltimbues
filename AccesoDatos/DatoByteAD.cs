﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class DatoByteAD
    {
        public DATO_BYTE add(DATO_BYTE oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    oContext.DATO_BYTE.Add(oDato);
                    oContext.SaveChanges();
                    return oDato;
                }
            }
            catch (Exception ex)
            {
                return new DATO_BYTE();
            }
        }

        public bool delete(DATO_BYTE oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.ID == oDato.ID).FirstOrDefault();
                    if (oConsulta != null)
                    {
                        oContext.DATO_BYTE.Remove(oConsulta);
                        oContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DATO_BYTE update(DATO_BYTE oDato)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_BYTE
                                     where tabla.ID == oDato.ID
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FK_PLC = oDato.FK_PLC;
                        oConsulta.FLAG_RW = oDato.FLAG_RW;
                        oConsulta.NUM_BYTE = oDato.NUM_BYTE;
                        oConsulta.NUM_DB = oDato.NUM_DB;
                        oConsulta.QC = oDato.QC;
                        oConsulta.TAG = oDato.TAG;
                        oConsulta.VALOR = oDato.VALOR;

                        oContext.SaveChanges();
                        return oDato;
                    }
                    else
                    {
                        return new DATO_BYTE();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_BYTE();
            }
        }

        public DATO_BYTE getOne(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.ID == id).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_BYTE();
            }
        }

        public DATO_BYTE getOne(int idplc, string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.TAG == tag && a.FK_PLC == idplc).SingleOrDefault();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new DATO_BYTE();
            }
        }

        public List<DATO_BYTE> getAll()
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BYTE>();
            }
        }

        public List<DATO_BYTE> getAll(int idplc)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.FK_PLC == idplc).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BYTE>();
            }
        }
        public List<DATO_BYTE> getAllPorSector(int idSector)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.ID_SECTOR == idSector).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BYTE>();
            }
        }
        public List<DATO_BYTE> getAllPorSector(int idSector, int nroDB)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.ID_SECTOR == idSector && a.NUM_DB == nroDB).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BYTE>();
            }
        }

        public DATO_BYTE leer(int id)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.ID == id).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        if (oConsulta.FLAG_RW == true)
                        {
                            return oConsulta;
                        }
                        else
                        {
                            return new DATO_BYTE();
                        }
                    }
                    else
                    {
                        return new DATO_BYTE();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_BYTE();
            }
        }

        public DATO_BYTE escribir(int id, byte[] valor)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {

                    var oConsulta = (from tabla in oContext.DATO_BYTE
                                     where tabla.ID == id
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.FLAG_RW = true;
                        oConsulta.VALOR = valor;

                        oContext.SaveChanges();
                        return oConsulta;
                    }
                    else
                    {
                        return new DATO_BYTE();
                    }

                }
            }
            catch (Exception ex)
            {
                return new DATO_BYTE();
            }


        }

        public List<DATO_BYTE> getAllTags(int idplc, string nombreTag, int ID_SECTOR)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DATO_BYTE.Where(a => a.FK_PLC == idplc && a.TAG.Contains(nombreTag) && a.ID_SECTOR == ID_SECTOR).OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<DATO_BYTE>();
            }
        }

    }
}
