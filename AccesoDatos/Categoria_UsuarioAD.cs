﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using System.Data.Objects;
using System.Data.Entity.Validation;
namespace AccesoDatos
{
   public class Categoria_UsuarioAD
    {

        public List<Entidades.CATEGORIA_USUARIO> getAll()
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.CATEGORIA_USUARIO
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

               // AxError.setError(ex);
                return null;
            }
        }
    }
}
