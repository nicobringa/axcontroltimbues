﻿Imports Entidades
Imports AccesoDatos
Public Class Antena_RfidN
    Public Function GetOne(ByVal numAntena As Integer, idConf As Integer) As ANTENAS_RFID
        Dim oAntenaAD As New Antena_RfidAD
        Return oAntenaAD.getOne(numAntena, idConf)
    End Function
    Public Function GetOneXsector(idSector As Integer) As ANTENAS_RFID
        Dim oAntenaAD As New Antena_RfidAD
        Return oAntenaAD.getOneXSector(idSector)
    End Function

    Public Function Update(ByVal AntenasRFID As Entidades.ANTENAS_RFID, idConfiguracion As Integer) As ANTENAS_RFID
        Dim oAntenaAD As New Antena_RfidAD
        Return oAntenaAD.Update(AntenasRFID, idConfiguracion)
    End Function


    Public Function GetAll(idSector As Integer) As List(Of Entidades.ANTENAS_RFID)
        Dim oAntenaAD As New Antena_RfidAD
        Return oAntenaAD.getAll(idSector)
    End Function
End Class
