﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Newtonsoft.Json;
using System.Data.Entity.Migrations;

namespace AxControlWeb.Controllers
{
    public class CARTEL_MULTILEDController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();

        /// <summary>
        /// Vista parcial del control, donde se muestra el mensaje acutal del cartel, y un botón que redirige a la configuración
        /// </summary>
        /// <param name="idCartel">El id del cartel es el mismo que el id del sector</param>
        /// <returns></returns>
        public PartialViewResult CartelMultiLED(int idCartel)
        {
            CARTEL_MULTILED oCartel = db.CARTEL_MULTILED.Where(a => a.ID_CARTEL == idCartel).FirstOrDefault();
            ViewBag.oCartel = oCartel;
            return PartialView();
        }

        [HttpGet]
        public ViewResult ConfigCartelMultiLED(int idCartel)
        {
            CARTEL_MULTILED oCartel = db.CARTEL_MULTILED.Where(a => a.ID_CARTEL == idCartel).FirstOrDefault();
            return View(oCartel);
        }

        [HttpPost]
        public ActionResult ConfigCartelMultiLED(CARTEL_MULTILED oCartel)
        {
            db.CARTEL_MULTILED.AddOrUpdate(oCartel);
            db.SaveChanges();
            return RedirectToAction("Control","ControlAcceso",new { idControl = oCartel.ID_CONTROL_ACCESO });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
