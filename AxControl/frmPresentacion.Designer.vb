﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPresentacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPresentacion))
        Me.picPantallaEspera = New System.Windows.Forms.PictureBox()
        Me.pbPresentacion = New System.Windows.Forms.ProgressBar()
        CType(Me.picPantallaEspera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picPantallaEspera
        '
        Me.picPantallaEspera.Image = Global.AxControl.My.Resources.Resources.PantallaEspera
        Me.picPantallaEspera.Location = New System.Drawing.Point(-9, -2)
        Me.picPantallaEspera.Name = "picPantallaEspera"
        Me.picPantallaEspera.Size = New System.Drawing.Size(1174, 399)
        Me.picPantallaEspera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picPantallaEspera.TabIndex = 1
        Me.picPantallaEspera.TabStop = False
        '
        'pbPresentacion
        '
        Me.pbPresentacion.Location = New System.Drawing.Point(422, 65)
        Me.pbPresentacion.Name = "pbPresentacion"
        Me.pbPresentacion.Size = New System.Drawing.Size(291, 37)
        Me.pbPresentacion.Step = 20
        Me.pbPresentacion.TabIndex = 2
        '
        'frmPresentacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1153, 398)
        Me.Controls.Add(Me.pbPresentacion)
        Me.Controls.Add(Me.picPantallaEspera)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPresentacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPresentacion"
        CType(Me.picPantallaEspera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents picPantallaEspera As PictureBox
    Friend WithEvents pbPresentacion As ProgressBar
End Class
