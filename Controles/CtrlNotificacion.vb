﻿


Public Class CtrlNotificacion

    Public Event ClickCerrarNotificacion(ByVal ctrl As Control)


    Delegate Sub DelSetColorCrtl(ByVal ctrl As Control, ByVal color As Color)

    Private Sub SetColorCtrl(ByVal ctrl As Control, ByVal color As Color)
        If ctrl.InvokeRequired Then
            Dim Delegado As New DelSetColorCrtl(AddressOf SetColorCtrl)
            Me.Invoke(Delegado, New Object() {ctrl, color})
        Else
            ctrl.BackColor = color
            ctrl.Refresh()

        End If
    End Sub


    Private _TipoNotificacion As Entidades.Constante.TipoNotificacion
    Public Property TipoNotificacion() As Entidades.Constante.TipoNotificacion
        Get
            Return _TipoNotificacion
        End Get
        Set(ByVal value As Entidades.Constante.TipoNotificacion)
            _TipoNotificacion = value

            Dim ColorFecha As Color
            Dim ColorMensaje As Color
            If value = Entidades.Constante.TipoNotificacion.xErrorDeve Then
                ColorFecha = Color.FromArgb(255, 150, 150)
                ColorMensaje = Color.FromArgb(255, 200, 200)
            ElseIf value = Entidades.Constante.TipoNotificacion.Informacion Then
                ColorFecha = Color.FromArgb(150, 150, 255)
                ColorMensaje = Color.FromArgb(200, 200, 255)

            ElseIf value = Entidades.Constante.TipoNotificacion.LecturaTag Then
                ColorFecha = Color.Yellow
                ColorMensaje = Color.FromArgb(255, 255, 192)

            ElseIf value = Entidades.Constante.TipoNotificacion.Suceso Then
                ColorFecha = Color.Green
                ColorMensaje = Color.GreenYellow

            ElseIf value = Entidades.Constante.TipoNotificacion.xErrorUser Then
                ColorFecha = Color.FromArgb(255, 150, 150)
                ColorMensaje = Color.FromArgb(255, 200, 200)
            End If

            SetColorCtrl(lblFecha, ColorFecha)
            SetColorCtrl(txtMensaje, ColorMensaje)

        End Set
    End Property


    Private _Fecha As DateTime
    Public Property Fecha() As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
            lblFecha.Text = value

        End Set
    End Property


    Private _Mensaje As String
    Public Property Mensaje() As String
        Get
            Return _Mensaje
        End Get
        Set(ByVal value As String)
            _Mensaje = value
            txtMensaje.Text = value
        End Set
    End Property


   

    Public Sub New(ByVal TipoNotificacion As Entidades.Constante.TipoNotificacion, ByVal Fecha As DateTime, ByVal Mensaje As String)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        Me.TipoNotificacion = TipoNotificacion
        Me.Mensaje = Mensaje
        Me.Fecha = Fecha

    End Sub


    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub



    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        RaiseEvent ClickCerrarNotificacion(Me)

    End Sub
End Class
