﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entidades
{
    /// <summary>
    /// Clase padre que contiene todas las clase que se usan para las rta de los web service 
    /// </summary>
    public class WsRta
    {
        /// <summary>
        /// Clase que permite generar el json para enviar en el web service RtaAltaModSilo PUBLICADO POR BIT 
        /// </summary>
        public class RtaAltaModSilo
        {
            /// <summary>
            /// Id generado por BIT. Utilizado por axControl para retornar la respuesta haciendo referencia al Id de comando generado por BIT en el web service AltaModSilo
            /// </summary>
            public Int16 idComandoRta { get; set; }
            /// <summary>
            /// 1= Procesado, 2=Error en axControl, 3=Error en PLC S71200, 4=Error en PCS7, 5=TimeOut 
            /// </summary>
            public Int16 idEstadoRta { get; set; }
            /// <summary>
            /// Mensaje para describir el estado de la respuesta
            /// </summary>
            public string mensajeRta { get; set; }

            /// <summary>
            /// ENUMERADOR DEL ID DE ESTADO DE LA RESPUESTA
            /// </summary>
            enum EstadoRta
            {
                PROCESADO = 1,
                ERROR_AX_CONTROL = 2,
                ERROR_PLC_S71200 = 3,
                ERROR_PLC_S7 = 4,
                TIME_OUT = 5
            }

        }

        /// <summary>
        /// Clase que permite generar el json para enviar en el web service AlarmaSilo PUBLICADO POR BIT
        /// Permite notificar a bit alarmar generadas por electro luz
        /// </summary>
        public class AlarmaSilo
        {
            /// <summary>
            /// Identificador único por cada silo de la planta
            /// </summary>
            public Int64 idSilo { get; set; }
            /// <summary>
            /// 1= Nivel Bajo en Silo, 2= Nivel Alto en Silo, ….. (a definir entre BIT y Electroluz)
            /// </summary>
            public Int16 idAlarma { get; set; }
            /// <summary>
            /// Mensaje de la Alarma
            /// </summary>
            public string mensaje { get; set; }
            /// <summary>
            /// Valor de la variable que genera la alarma
            /// </summary>
            public double valor { get; set; }
        }


    }
}