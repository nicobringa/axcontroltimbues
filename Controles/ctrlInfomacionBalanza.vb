﻿Public Class ctrlInfomacionBalanza

    Public Event ClickCAMION_PESANDO()
    Private _CAMION_PESANDO As Entidades.CAMION_PESANDO

    Public Sub CAMION_PESANDO(ByVal Pesando As Boolean, ByRef CAMION_PESANDO As Entidades.CAMION_PESANDO)
        Negocio.modDelegado.SetEnable(Me, btnCAMION_PESANDOIzq, Pesando)
        _CAMION_PESANDO = CAMION_PESANDO
    End Sub

    'Public Sub CamionEsperando(ByVal CamionEsperando As Boolean)
    '    Negocio.modDelegado.SetEnable(Me, btnCamionEsperandoIzq, CamionEsperando)
    'End Sub

    Public Sub Espera(ByVal Espera As Boolean)
        Negocio.modDelegado.SetEnable(Me, btnEspPesoDisplayIzq, Espera)
        If Espera = False Then

            Negocio.modDelegado.SetTextLabel(Me, lblCant, "")
        End If
    End Sub

    Public Sub setCant(cant As Integer)
        Negocio.modDelegado.SetTextLabel(Me, lblCant, cant)
    End Sub




    Private Sub btnCAMION_PESANDOIzq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCAMION_PESANDOIzq.Click
        If Not IsNothing(_CAMION_PESANDO) Then
            Dim frm As New FrmCAMION_PESANDO(_CAMION_PESANDO)
            frm.ShowDialog()
        End If


    End Sub
End Class
