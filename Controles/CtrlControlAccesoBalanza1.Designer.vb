﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlControlAccesoBalanza1
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoBalanza1))
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlInfomacionBalanza = New Controles.ctrlInfomacionBalanza()
        Me.S2_POSICION = New Controles.CtrlSensor()
        Me.BARRERA2 = New Controles.CtrlBarrera()
        Me.BARRERA1 = New Controles.CtrlBarrera()
        Me.S_POSICIONAMIENTO = New Controles.CtrlSensor()
        Me.S1_POSICION = New Controles.CtrlSensor()
        Me.S1_LECTURA = New Controles.CtrlSensor()
        Me.BALANZA1 = New Controles.CtrlCabezalBalanza()
        Me.CtrlAnt_Leyendo_BAR1 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CAM_BALALANZA1 = New Controles.CtrlCamara()
        Me.CartelLED_BALANZA1 = New Controles.CtrlCartelMultiLED()
        Me.CtrlTiempoEspera1 = New Controles.CtrlTiempoEspera()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.DinamismoBalanza1 = New Controles.CtrlDinamismoBalanza1()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnLecturaManualIngreso = New System.Windows.Forms.Button()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.CAM_BALALANZA2 = New Controles.CtrlCamara()
        Me.SuspendLayout()
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(-1, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(759, 43)
        Me.lblNombre.TabIndex = 166
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlInfomacionBalanza
        '
        Me.CtrlInfomacionBalanza.BackColor = System.Drawing.Color.White
        Me.CtrlInfomacionBalanza.Location = New System.Drawing.Point(187, 297)
        Me.CtrlInfomacionBalanza.Name = "CtrlInfomacionBalanza"
        Me.CtrlInfomacionBalanza.Size = New System.Drawing.Size(129, 36)
        Me.CtrlInfomacionBalanza.TabIndex = 177
        '
        'S2_POSICION
        '
        Me.S2_POSICION.BitMonitoreo = CType(0, Short)
        Me.S2_POSICION.Estado = False
        Me.S2_POSICION.GPIO = False
        Me.S2_POSICION.ID_CONTROL_ACCESO = 0
        Me.S2_POSICION.ID_PLC = 0
        Me.S2_POSICION.ID_SECTOR = 0
        Me.S2_POSICION.Inicializado = False
        Me.S2_POSICION.Location = New System.Drawing.Point(229, 60)
        Me.S2_POSICION.Name = "S2_POSICION"
        Me.S2_POSICION.PLC = Nothing
        Me.S2_POSICION.Size = New System.Drawing.Size(76, 45)
        Me.S2_POSICION.TabIndex = 176
        '
        'BARRERA2
        '
        Me.BARRERA2.BackColor = System.Drawing.Color.Transparent
        Me.BARRERA2.BitMonitoreo = CType(0, Short)
        Me.BARRERA2.Estado = False
        Me.BARRERA2.GPIO = False
        Me.BARRERA2.ID_CONTROL_ACCESO = 0
        Me.BARRERA2.ID_PLC = 0
        Me.BARRERA2.ID_SECTOR = 0
        Me.BARRERA2.Inicializado = False
        Me.BARRERA2.Location = New System.Drawing.Point(151, 167)
        Me.BARRERA2.Name = "BARRERA2"
        Me.BARRERA2.PLC = Nothing
        Me.BARRERA2.Size = New System.Drawing.Size(59, 50)
        Me.BARRERA2.TabIndex = 175
        '
        'BARRERA1
        '
        Me.BARRERA1.BackColor = System.Drawing.Color.Transparent
        Me.BARRERA1.BitMonitoreo = CType(0, Short)
        Me.BARRERA1.Estado = False
        Me.BARRERA1.GPIO = False
        Me.BARRERA1.ID_CONTROL_ACCESO = 0
        Me.BARRERA1.ID_PLC = 0
        Me.BARRERA1.ID_SECTOR = 0
        Me.BARRERA1.Inicializado = False
        Me.BARRERA1.Location = New System.Drawing.Point(38, 283)
        Me.BARRERA1.Name = "BARRERA1"
        Me.BARRERA1.PLC = Nothing
        Me.BARRERA1.Size = New System.Drawing.Size(64, 50)
        Me.BARRERA1.TabIndex = 174
        '
        'S_POSICIONAMIENTO
        '
        Me.S_POSICIONAMIENTO.BitMonitoreo = CType(0, Short)
        Me.S_POSICIONAMIENTO.Estado = False
        Me.S_POSICIONAMIENTO.GPIO = False
        Me.S_POSICIONAMIENTO.ID_CONTROL_ACCESO = 0
        Me.S_POSICIONAMIENTO.ID_PLC = 0
        Me.S_POSICIONAMIENTO.ID_SECTOR = 0
        Me.S_POSICIONAMIENTO.Inicializado = False
        Me.S_POSICIONAMIENTO.Location = New System.Drawing.Point(349, 210)
        Me.S_POSICIONAMIENTO.Name = "S_POSICIONAMIENTO"
        Me.S_POSICIONAMIENTO.PLC = Nothing
        Me.S_POSICIONAMIENTO.Size = New System.Drawing.Size(76, 45)
        Me.S_POSICIONAMIENTO.TabIndex = 173
        '
        'S1_POSICION
        '
        Me.S1_POSICION.BitMonitoreo = CType(0, Short)
        Me.S1_POSICION.Estado = False
        Me.S1_POSICION.GPIO = False
        Me.S1_POSICION.ID_CONTROL_ACCESO = 0
        Me.S1_POSICION.ID_PLC = 0
        Me.S1_POSICION.ID_SECTOR = 0
        Me.S1_POSICION.Inicializado = False
        Me.S1_POSICION.Location = New System.Drawing.Point(362, 297)
        Me.S1_POSICION.Name = "S1_POSICION"
        Me.S1_POSICION.PLC = Nothing
        Me.S1_POSICION.Size = New System.Drawing.Size(76, 45)
        Me.S1_POSICION.TabIndex = 170
        '
        'S1_LECTURA
        '
        Me.S1_LECTURA.BitMonitoreo = CType(0, Short)
        Me.S1_LECTURA.Estado = False
        Me.S1_LECTURA.GPIO = False
        Me.S1_LECTURA.ID_CONTROL_ACCESO = 0
        Me.S1_LECTURA.ID_PLC = 0
        Me.S1_LECTURA.ID_SECTOR = 0
        Me.S1_LECTURA.Inicializado = False
        Me.S1_LECTURA.Location = New System.Drawing.Point(382, 359)
        Me.S1_LECTURA.Name = "S1_LECTURA"
        Me.S1_LECTURA.PLC = Nothing
        Me.S1_LECTURA.Size = New System.Drawing.Size(76, 45)
        Me.S1_LECTURA.TabIndex = 169
        '
        'BALANZA1
        '
        Me.BALANZA1.BackColor = System.Drawing.Color.Gainsboro
        Me.BALANZA1.BitMonitoreo = CType(0, Short)
        Me.BALANZA1.GPIO = False
        Me.BALANZA1.ID_CONTROL_ACCESO = 0
        Me.BALANZA1.ID_PLC = 0
        Me.BALANZA1.ID_SECTOR = 0
        Me.BALANZA1.Inicializado = False
        Me.BALANZA1.Location = New System.Drawing.Point(151, 378)
        Me.BALANZA1.Name = "BALANZA1"
        Me.BALANZA1.numeroBalanza = 1
        Me.BALANZA1.PLC = Nothing
        Me.BALANZA1.Size = New System.Drawing.Size(191, 80)
        Me.BALANZA1.TabIndex = 167
        '
        'CtrlAnt_Leyendo_BAR1
        '
        Me.CtrlAnt_Leyendo_BAR1.AutoSize = True
        Me.CtrlAnt_Leyendo_BAR1.BackColor = System.Drawing.Color.White
        Me.CtrlAnt_Leyendo_BAR1.Location = New System.Drawing.Point(24, 339)
        Me.CtrlAnt_Leyendo_BAR1.Name = "CtrlAnt_Leyendo_BAR1"
        Me.CtrlAnt_Leyendo_BAR1.Size = New System.Drawing.Size(87, 69)
        Me.CtrlAnt_Leyendo_BAR1.TabIndex = 116
        Me.CtrlAnt_Leyendo_BAR1.UltTag = Nothing
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 454)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(533, 70)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 115
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(532, 40)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(225, 484)
        Me.CtrlPanelNotificaciones1.TabIndex = 114
        '
        'CAM_BALALANZA1
        '
        Me.CAM_BALALANZA1.AutoSize = True
        Me.CAM_BALALANZA1.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_BALALANZA1.BitMonitoreo = CType(0, Short)
        Me.CAM_BALALANZA1.frmCamara = Nothing
        Me.CAM_BALALANZA1.GPIO = False
        Me.CAM_BALALANZA1.ID_CONTROL_ACCESO = 0
        Me.CAM_BALALANZA1.ID_PLC = 0
        Me.CAM_BALALANZA1.ID_SECTOR = 0
        Me.CAM_BALALANZA1.Inicializado = False
        Me.CAM_BALALANZA1.Location = New System.Drawing.Point(38, 126)
        Me.CAM_BALALANZA1.Name = "CAM_BALALANZA1"
        Me.CAM_BALALANZA1.PLC = Nothing
        Me.CAM_BALALANZA1.Size = New System.Drawing.Size(78, 60)
        Me.CAM_BALALANZA1.TabIndex = 178
        '
        'CartelLED_BALANZA1
        '
        Me.CartelLED_BALANZA1.BitMonitoreo = CType(0, Short)
        Me.CartelLED_BALANZA1.Conectado = False
        Me.CartelLED_BALANZA1.GPIO = False
        Me.CartelLED_BALANZA1.ID_CONTROL_ACCESO = 0
        Me.CartelLED_BALANZA1.ID_PLC = 0
        Me.CartelLED_BALANZA1.ID_SECTOR = 0
        Me.CartelLED_BALANZA1.Inicializado = False
        Me.CartelLED_BALANZA1.Location = New System.Drawing.Point(349, 105)
        Me.CartelLED_BALANZA1.Name = "CartelLED_BALANZA1"
        Me.CartelLED_BALANZA1.PLC = Nothing
        Me.CartelLED_BALANZA1.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_BALANZA1.TabIndex = 179
        '
        'CtrlTiempoEspera1
        '
        Me.CtrlTiempoEspera1.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEspera1.Location = New System.Drawing.Point(229, 167)
        Me.CtrlTiempoEspera1.Name = "CtrlTiempoEspera1"
        Me.CtrlTiempoEspera1.SetVisible = False
        Me.CtrlTiempoEspera1.Size = New System.Drawing.Size(100, 59)
        Me.CtrlTiempoEspera1.TabIndex = 180
        Me.CtrlTiempoEspera1.Tiempo = Nothing
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, -1)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 113
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'DinamismoBalanza1
        '
        Me.DinamismoBalanza1.BackgroundImage = CType(resources.GetObject("DinamismoBalanza1.BackgroundImage"), System.Drawing.Image)
        Me.DinamismoBalanza1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.DinamismoBalanza1.Location = New System.Drawing.Point(0, 43)
        Me.DinamismoBalanza1.Name = "DinamismoBalanza1"
        Me.DinamismoBalanza1.Size = New System.Drawing.Size(533, 391)
        Me.DinamismoBalanza1.TabIndex = 0
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(446, 43)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 182
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnLecturaManualIngreso
        '
        Me.btnLecturaManualIngreso.BackColor = System.Drawing.Color.White
        Me.btnLecturaManualIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualIngreso.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualIngreso.Location = New System.Drawing.Point(53, 228)
        Me.btnLecturaManualIngreso.Name = "btnLecturaManualIngreso"
        Me.btnLecturaManualIngreso.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualIngreso.TabIndex = 183
        Me.btnLecturaManualIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualIngreso.UseVisualStyleBackColor = False
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(489, 43)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(43, 43)
        Me.btnAyuda.TabIndex = 184
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(382, 42)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 194
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CAM_BALALANZA2
        '
        Me.CAM_BALALANZA2.AutoSize = True
        Me.CAM_BALALANZA2.BackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.CAM_BALALANZA2.BitMonitoreo = CType(0, Short)
        Me.CAM_BALALANZA2.frmCamara = Nothing
        Me.CAM_BALALANZA2.GPIO = False
        Me.CAM_BALALANZA2.ID_CONTROL_ACCESO = 0
        Me.CAM_BALALANZA2.ID_PLC = 0
        Me.CAM_BALALANZA2.ID_SECTOR = 0
        Me.CAM_BALALANZA2.Inicializado = False
        Me.CAM_BALALANZA2.Location = New System.Drawing.Point(37, 60)
        Me.CAM_BALALANZA2.Name = "CAM_BALALANZA2"
        Me.CAM_BALALANZA2.PLC = Nothing
        Me.CAM_BALALANZA2.Size = New System.Drawing.Size(78, 60)
        Me.CAM_BALALANZA2.TabIndex = 195
        '
        'CtrlControlAccesoBalanza1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.Controls.Add(Me.CAM_BALALANZA2)
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnLecturaManualIngreso)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.CtrlTiempoEspera1)
        Me.Controls.Add(Me.CartelLED_BALANZA1)
        Me.Controls.Add(Me.CAM_BALALANZA1)
        Me.Controls.Add(Me.CtrlInfomacionBalanza)
        Me.Controls.Add(Me.S2_POSICION)
        Me.Controls.Add(Me.BARRERA2)
        Me.Controls.Add(Me.BARRERA1)
        Me.Controls.Add(Me.S_POSICIONAMIENTO)
        Me.Controls.Add(Me.S1_POSICION)
        Me.Controls.Add(Me.S1_LECTURA)
        Me.Controls.Add(Me.BALANZA1)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlAnt_Leyendo_BAR1)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.DinamismoBalanza1)
        Me.Name = "CtrlControlAccesoBalanza1"
        Me.Size = New System.Drawing.Size(758, 524)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DinamismoBalanza1 As CtrlDinamismoBalanza1
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents CtrlAnt_Leyendo_BAR1 As CtrlAntenaLeyendo
    Protected WithEvents lblNombre As Label
    Public WithEvents BALANZA1 As CtrlCabezalBalanza
    Friend WithEvents S1_LECTURA As CtrlSensor
    Friend WithEvents S1_POSICION As CtrlSensor
    Friend WithEvents S_POSICIONAMIENTO As CtrlSensor
    Public WithEvents BARRERA1 As CtrlBarrera
    Public WithEvents BARRERA2 As CtrlBarrera
    Friend WithEvents S2_POSICION As CtrlSensor
    Friend WithEvents CtrlInfomacionBalanza As ctrlInfomacionBalanza
    Friend WithEvents CAM_BALALANZA1 As CtrlCamara
    Friend WithEvents CartelLED_BALANZA1 As CtrlCartelMultiLED
    Friend WithEvents CtrlTiempoEspera1 As CtrlTiempoEspera
    Friend WithEvents btnReset As Button
    Friend WithEvents btnLecturaManualIngreso As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents btnAlarma As Button
    Friend WithEvents CAM_BALALANZA2 As CtrlCamara
End Class
