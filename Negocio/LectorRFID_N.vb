﻿Public Class LectorRFID_N
    Private oBD As New AccesoDatos.LectorRFID_AD

    Public Function GetAll(idPuestoTrabajo As Integer) As List(Of Entidades.Lector_RFID)
        Return oBD.getAll(idPuestoTrabajo)
    End Function

    Public Function GetOne(ipLector As String) As Entidades.Lector_RFID
        Return oBD.getOne(ipLector)
    End Function

    Public Function GetOne(idLector As Integer) As Entidades.Lector_RFID
        Return oBD.getOne(idLector)
    End Function
    Public Function GetOneSector(idSector As Integer) As Entidades.LECTOR_RFID
        Return oBD.getOneSector(idSector)
    End Function
    Public Function Update(LectorRFID) As Boolean

        Return oBD.update(LectorRFID)
    End Function

    Public Function updateEstadoLector(LectorRFID As Entidades.LECTOR_RFID, estado As Int32, leyendo As Int32) As Boolean

        Return oBD.updateEstadoLector(LectorRFID, estado, leyendo)
    End Function

End Class
