﻿using System;
using System.Linq;
using Entidades;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestAxControl
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ConexionBD()
        {
            try
            {
                AxControlORAEntities db = new AxControlORAEntities();
                var mara = db.CAMARA.ToList();

                if (mara != null)
                {
                     Assert.IsTrue(true, "Conectado");
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Assert.IsTrue(false, "Desconectado");
            }

         
           
        }

        [TestMethod]
        public void AddAudotriaPrueba()
        {
            try
            {
                AxControlORAEntities db = new AxControlORAEntities();
                AUDITORIA au = new AUDITORIA()
                {
                    
                    ACCION = 1,
                    FECHA = DateTime.Now,
                    DESCRIPCION = "AUDITORIA PRUEBA",
                    ID_SECTOR = 110,
                    TAG_RFID = "AAAADDDSADASDASD",
                    USUARIO = "AUMAX",
                    ID_AUDITORIA_WS  = 1,
                    ID_TRANSACCION = 1
                   
                };

                db.AUDITORIA.Add(au);
                db.SaveChanges();

                    if (au.ID_AUDITORIA > 0) Assert.IsTrue(true, "ID AUDITORIA : "  + au.ID_AUDITORIA);



            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Assert.IsTrue(false, "Desconectado");
            }



        }


        [TestMethod]
        public void GetControlAccesoID()
        {
            try
            {
                Negocio.ControlAccesoN nC = new Negocio.ControlAccesoN();

                var controlAcceso = nC.GetOne(100);

                if (controlAcceso != null)
                {
                    Assert.IsTrue(true, "Conectado");
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Assert.IsTrue(false, ex.Message);
            }
          

        }

        [TestMethod]
        public void iNTERFACE()
        {
            try
            {
                AxControlORAEntities db = new AxControlORAEntities();

                var lista = db.INTERFACE_WS.ToList();


                Console.WriteLine(lista.Count);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Assert.IsTrue(false, ex.Message);
            }


        }

        [TestMethod]
        public void toStringPrueba()
        {
            try
            {
                DATO_BYTE prueba;

                //Console.WriteLine(prueba.ToString());

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Assert.IsTrue(false, ex.Message);
            }


        }

    }
}
