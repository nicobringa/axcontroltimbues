﻿Imports System.Drawing

Public Class CtrlCamara
    Inherits CtrlAutomationObjectsBase
    Public oCamara As Entidades.camara
    Public WithEvents frmCamara As frmCamara
    Private ComandoAcceo As Boolean

    Public Event FotoTomada(ByVal LoadBitMap As Bitmap, ByVal Camara As Entidades.camara)
    Private oDatosNombreArchivo As DatosNombreArchivo


    Private Sub btnConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfig.Click

        Try
            If IsNothing(oCamara) Then
                MessageBox.Show("La camara no se encuentra agregada en la base de datos", "Camara", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If
            frmCamara = New frmCamara(oCamara)
            frmCamara.ShowDialog()

            'If (frmCamara.ShowDialog = Windows.Forms.DialogResult.OK) Then

            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub


    Public Sub Inicializar(ByVal ID_SECTOR As Int32)

        Me.Tag = Me.Name

        Dim CamaraN As New Negocio.CamaraN
        Me.oCamara = CamaraN.GetOne(Me.Tag, ID_SECTOR)

        If Not IsNothing(Me.oCamara) Then
            ' Me.lbTAG.Text = Me.Tag


        Else

            MsgBox("La camara " & Me.Name & " no existe en la base de datos. Solicite asistencia técnica.", MsgBoxStyle.Exclamation, "axControl")
            Negocio.modDelegado.SetEnable(Me, btnConfig, False)
        End If




    End Sub

    Public Sub SacarFoto(idTrasaccion As Long, ID_SECTOR As Integer, Optional fecha As String = "") Handles frmCamara.SacarFoto

        Try
            Dim oControl As New Entidades.CONTROL_ACCESO
            Dim nControl As New Negocio.ControlAccesoN
            oControl = nControl.GetOne(oCamara.ID_CONTROL_ACCESO)
            If My.Computer.Network.Ping(Me.oCamara.IP) Then
                oCamara.ESTADO = True
                'Sacar foto
                'Me.ComandoAcceo = ComandoAceeso
                Try
                    Dim urlCAm As String = "http://" & Me.oCamara.IP & ":" & Me.oCamara.puerto & "/Streaming/channels/1/picture?snapShotImageType=JPEG"


                    Dim request = System.Net.HttpWebRequest.Create(urlCAm)
                    request.Credentials = New Net.NetworkCredential(oCamara.USUARIO, oCamara.PASSWORD)

                    request.Proxy = Nothing

                    If fecha.Length > 0 Then
                        Me.oDatosNombreArchivo = New DatosNombreArchivo(idTrasaccion, ID_SECTOR, fecha)
                    Else
                        Me.oDatosNombreArchivo = New DatosNombreArchivo(idTrasaccion, ID_SECTOR)
                    End If


                    request.BeginGetResponse(New AsyncCallback(AddressOf RepuestaFoto), request)


                Catch ex As Exception
                    'RaiseEvent ConfirmacionFoto(idSeguimiento, BalanzaID, False, ex.Message)
                End Try
                'ModificarAlarma(oCamara.ID_CONTROL_ACCESO, 1, oCamara.ID_CONTROL_ACCESO, oControl.tipoControlAcceso)
            Else
                ' GenerarAlarma("La camara no se encuentra conectada", oCamara.ID_CONTROL_ACCESO, oCamara.ID_CONTROL_ACCESO, oControl.tipoControlAcceso)
                Negocio.AuditoriaN.AddAuditoria(Me.Name & " DESCONECTADA", "", Me.ID_SECTOR, 0, "SISTEMA", Entidades.Constante.acciones.DesconexionPerifericos)
                oCamara.ESTADO = False
            End If

        Catch ex As Exception
            oCamara.ESTADO = False
        Finally

            If oCamara.ESTADO Then
                Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Conectado")
                Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.DarkGreen)

                'SetLabelColor(Me, lblEstado, Drawing.Color.LimeGreen)
                ' lblEstado.Text = "Conectado"
                'lblEstado.BackColor = Drawing.Color.LimeGreen
                'btnConfig.BackColor = Drawing.Color.Transparent
            Else

                Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Desconectado")
                Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Red)
                'Setla(Me, lblEstado, Drawing.Color.Red)
                'lblEstado.Text = "Desconectado"
                'lblEstado.BackColor = Drawing.Color.Red
                'btnConfig.BackColor = Drawing.Color.Red
            End If

        End Try


    End Sub

    Public Sub SetEstado()

        If My.Computer.Network.Ping(Me.oCamara.IP) Then
            Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Conectado")
            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Green)

        Else

            Negocio.modDelegado.SetTextLabel(Me, lblEstado, "Desconectado")
            Negocio.modDelegado.SetBackColorCtrl(Me, lblEstado, Color.Red)
        End If



    End Sub

    Private Sub RepuestaFoto(ByVal resultado As IAsyncResult)

        Try
            Dim respuesta As System.Net.HttpWebResponse = TryCast(TryCast(resultado.AsyncState, System.Net.HttpWebRequest).EndGetResponse(resultado), System.Net.HttpWebResponse)

            Dim responseStream As System.IO.Stream = respuesta.GetResponseStream()
            Dim loadedBitmap As Bitmap = Nothing
            Using frame As New Bitmap(responseStream)
                If frame IsNot Nothing Then
                    loadedBitmap = DirectCast(frame.Clone(), Bitmap)
                End If
            End Using

            If Not IsNothing(frmCamara) Then
                'frmCamara.MostrarFoto(loadedBitmap)
                Dim frmFoto As New FrmFoto(loadedBitmap)
                frmFoto.ShowDialog()
            End If

            RaiseEvent FotoTomada(loadedBitmap, oCamara)

            Dim nombreFoto As String = String.Format("{0}-{1}-{2}", Me.oDatosNombreArchivo.ID_SECTOR, Me.oDatosNombreArchivo.idTrasaccion, DateTime.Now.ToString("ddMMyyyyhhmmss"))
            Dim direccionFoto As String = String.Format("{0}\{1}.jpg", Me.oCamara.RUTA_FOTO, nombreFoto)

            loadedBitmap.Save(direccionFoto, System.Drawing.Imaging.ImageFormat.Jpeg)

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try


    End Sub


    Public Class DatosNombreArchivo
        Public idTrasaccion As Long
        Public ID_SECTOR As Integer
        Public fecha As String

        Public Sub New(idTrasaccion As Long, ID_SECTOR As Integer, Optional fecha As String = "")

            Me.idTrasaccion = idTrasaccion
            Me.ID_SECTOR = ID_SECTOR
            Me.fecha = fecha
        End Sub


    End Class


End Class
