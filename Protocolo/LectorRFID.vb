﻿Imports Impinj.OctaneSdk
Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades
Imports System.Drawing

Public Class LectorRFID : Inherits ImpinjReader '  Hereda de la clase del fabricante Impinj




#Region "Propiedades"

    Dim DireccionArchivo As String = My.Application.Info.DirectoryPath 'Entidades.Constante.CARPETA_CONFIGURACION

    Public Shared COLOR_DESCONECTADO As Color = Color.Red
    Public Shared COLOR_CONECTADO As Color = Color.LimeGreen
    Public Shared COLOR_SIN_CONFIGURACION As Color = Color.LightYellow


    Public Shared COLOR_START As Color = Color.LightBlue
    Public Shared COLOR_STOP As Color = Color.Red

    Public Shared TEXT_CONECTADO As String = "Conectado"
    Public Shared TEXT_DESCONECTADO As String = "Desconectado"
    Public Shared TEXT_SIN_CONFIGURACION As String = "Sin Configuración"
    Public Shared TEXT_START As String = "Start"
    Public Shared TEXT_STOP As String = "Stop"




    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property


    Private _Ip As String
    Public Property Ip() As String
        Get
            Return _Ip
        End Get
        Set(ByVal value As String)
            _Ip = value
        End Set
    End Property


    ''' <summary>
    ''' Lista de configuraciones
    ''' </summary>
    ''' <remarks></remarks>
    Private _ListConfRFID As List(Of ConfRFID)
    Public Property ListConfRFID() As List(Of ConfRFID)
        Get
            Return _ListConfRFID
        End Get
        Set(ByVal value As List(Of ConfRFID))
            _ListConfRFID = value
        End Set
    End Property


    Private _LectorConfigurado As Boolean
    Public Property LectorConfigurado() As Boolean
        Get
            Return _LectorConfigurado
        End Get
        Set(ByVal value As Boolean)
            _LectorConfigurado = value
        End Set
    End Property


    Private _IsStar As Boolean
    Public Property IsStar() As Boolean
        Get
            Return _IsStar
        End Get
        Set(ByVal value As Boolean)
            _IsStar = value
        End Set
    End Property


    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property




#End Region

#Region "Enum"

    ''' <summary>
    ''' Los tipos de estado de conexion que tiene el lectOr RFID
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum EstadoConexion
        Conectado = 1
        Desconectado = 2
        ''' <summary>
        ''' Es cuando no se ah configurado el lector
        ''' </summary>
        ''' <remarks></remarks>
        SinConfiguracion = 3
    End Enum

    ''' <summary>
    ''' Estado de lectura del lector RFID
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum EstadoLectura
        Start = 1
        xStop = 2
    End Enum

#End Region

#Region "Mis Eventos"
    ''' <summary>
    ''' Evento que se ejecuta cuando una antena del lector leyo un TAG
    ''' </summary>
    ''' <param name="NumTag">Numer de tag o cod que contiene el tag</param>
    ''' <param name="NumAntena">Antena que leyo el TAG</param>
    ''' <param name="NombreLector">Nombre del lector RFID</param>
    ''' <remarks></remarks>
    Public Event TagLeido(ByVal NumTag As String, ByVal NumAntena As Int16, ByVal NombreLector As String)

#End Region

#Region "Contructor"

    ''' <summary>
    '''Crear un LECTOR RFID PARA PODER TRABAJAR 
    ''' Compatible con lectores SpeedWay 420 y 220 IMPINJ
    ''' </summary>
    ''' <param name="NombreLector"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal NombreLector As String)

        Me.Nombre = NombreLector
        'Busco el archivo de configuración
        Dim oLectorConf As LectorConfXml = GetConfLector()


        If Not IsNothing(oLectorConf) Then ' si encontro una configuración

            'Paso lo datos al objeto

            Me.Ip = oLectorConf.IP
            Me.ListConfRFID = oLectorConf.ListConfRFID

            LectorConfigurado = True
            ' Me.Connect(oLectorConf.IP) ' M Conecto

        Else 'No tiene un configuración establecida el lector

            'Throw New Exception("No tiene un configuración incial")
            LectorConfigurado = False

        End If



    End Sub
#End Region

#Region "Metodos"




    Public Sub GuardarConf()
        'Creo el objeto que permite guardo en el archivo
        Dim speedXml As New LectorConfXml
        'Paso los datos a guar en el archivo xml

        speedXml.NombreLector = Me.Nombre
        speedXml.IP = Me.Ip
        speedXml.ListConfRFID = Me.ListConfRFID
        speedXml.Descripcion = Me.Descripcion
        'My.Application.Info.DirectoryPath
        Dim serializer As New XmlSerializer(GetType(LectorConfXml))
        'Si no existe el archivo lo crea y si existe lo sobrecribe
        Dim textWriter As TextWriter = New StreamWriter(DireccionArchivo & "\" & "ConfLectorRIFID-" & Me.Nombre)
        serializer.Serialize(textWriter, speedXml)
        textWriter.Close()
        'SetAjustes()

    End Sub

    Public Function GetConfLector() As LectorConfXml
        Try
            Dim deserializer As New XmlSerializer(GetType(LectorConfXml))
            Dim textReader As TextReader = New StreamReader(DireccionArchivo & "\" & "ConfLectorRIFID-" & Me.Nombre)
            Dim oSpeedWayXml As LectorConfXml
            oSpeedWayXml = DirectCast(deserializer.Deserialize(textReader), LectorConfXml)
            textReader.Close()


            Return oSpeedWayXml
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Sub Conectar()
        Try
            If Not LectorConfigurado Then Throw New Exception("El lector no se encuentra configurado para conectarse")
            Me.Connect(Me.Ip) ' M Conecto
            Me.StartRead()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try




    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AplicarConfiguracion(ByVal NameConf As String)

        Me.StopRead()

        Dim Conf As Protocolo.ConfRFID = GetConf(NameConf)

        Dim readerSettings As Settings = Me.QueryDefaultSettings

        readerSettings.ReaderMode = Conf.SelectReaderMode
        readerSettings.SearchMode = Conf.SelectSearch
        readerSettings.Session = Conf.SelectSession
        readerSettings.Report.Mode = Conf.SelectReportMode

        'Deshabilito todas las atenas
        readerSettings.Antennas.DisableAll()

        'Habilito la tenas correspondiente y seteo su power y sensibilidad
        For Each antena As Protocolo.ConfRFID.Antenas In Conf.ListAntenas
            readerSettings.Antennas.GetAntenna(antena.NumeroAntena).IsEnabled = True
            readerSettings.Antennas.GetAntenna(antena.NumeroAntena).TxPowerInDbm = antena.TxPower
            readerSettings.Antennas.GetAntenna(antena.NumeroAntena).RxSensitivityInDbm = antena.RxSensitivity
        Next
        readerSettings.Report.IncludeAntennaPortNumber = True

        '#################PARA PODER LEER EL LUGAR DE MEMORIA TID#######################
        'Crear una operación de leer la etiqueta para la memoria TID .
        Dim ReadTID As New TagReadOp
        'Leer desde la memoria TID
        ReadTID.MemoryBank = MemoryBank.Tid
        ' Leer 3 ( 16 - bit) palabras
        ReadTID.WordCount = 3  '# LOS TAG DE URUGUAY OCUPAN 3 PALABRAS(WORDS)#
        ' A partir de la palabra 1 # LOS TAG DE URUGUAY EMPIEZAN APARTIR DE LA PABALA 1#
        ReadTID.WordPointer = 1
        'Añadir estas operaciones al lector como optimizados Leer ops .
        'Optimizados Leer ops se aplican a todas las etiquetas , a diferencia
        ' Tag Operación secuencias, que se puede aplicar a las etiquetas específicas.
        ' Revolución Speedway soporta hasta dos operaciones de lectura Optimizado .
        readerSettings.Report.OptimizedReadOps.Add(ReadTID)


        'Aplico los ajustes
        Me.ApplySettings(readerSettings)

    End Sub

    Private Function GetConf(ByVal NameConf As String) As Protocolo.ConfRFID
        For Each ConfLector As Protocolo.ConfRFID In Me.ListConfRFID
            If ConfLector.Nombre = NameConf Then
                Return ConfLector
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' Permite que el lector comience a leer
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StartRead()
        If Not isRunning() Then
            Me.Start()
            Me.IsStar = True
        End If
    End Sub

    ''' <summary>
    ''' Permite que el lector pare la lectura
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StopRead()
        If isRunning() Then
            Me.Stop()
            Me.IsStar = False
        End If
    End Sub

    ''' <summary>
    ''' Obtiene el estado de lectura del lector
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isRunning() As Boolean
        Dim Status As Status = Me.QueryStatus() ' Obtengo el estado del lector
        Return Status.IsSingulating()
    End Function


#End Region

#Region "Eventos Lector RFID"

    Private Sub Lector_ReporteTag(ByVal sender As ImpinjReader, ByVal report As TagOpReport) Handles Me.TagOpComplete
        For Each result As Impinj.OctaneSdk.TagOpResult In report ' Recorro todos los reportes de tag leidos
            Try

                If TypeOf result Is TagReadOpResult Then
                    Dim ReadResult As TagReadOpResult = result ' Resultado de los leidos

                    Dim NumAntena As Int16 = ReadResult.Tag.AntennaPortNumber 'Obtengo la antena que leyo el tag
                    Dim NumTag As String = ReadResult.Data.ToHexString() ' Obtengo el numero de tag leidos

                    RaiseEvent TagLeido(NumTag, NumAntena, Me.Nombre)


                End If

            Catch ex As Exception

            End Try
        Next
      



    End Sub
#End Region


    ''' <summary>
    ''' Objeto que se va guardar en el archivo xml
    ''' </summary>
    ''' <remarks></remarks>
    Public Class LectorConfXml

        Private _Ip As String
        Public Property IP As String
            Get
                Return _Ip
            End Get
            Set(ByVal value As String)
                _Ip = value
            End Set
        End Property


        Private _NombreLector As String
        Public Property NombreLector() As String
            Get
                Return _NombreLector
            End Get
            Set(ByVal value As String)
                _NombreLector = value
            End Set
        End Property

        Private _ListConfRFID As List(Of ConfRFID)
        Public Property ListConfRFID() As List(Of ConfRFID)
            Get
                Return _ListConfRFID
            End Get
            Set(ByVal value As List(Of ConfRFID))
                _ListConfRFID = value
            End Set
        End Property

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property



    End Class





End Class
