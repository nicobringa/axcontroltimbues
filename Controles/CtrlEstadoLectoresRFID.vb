﻿Public Class CtrlEstadoLectoresRFID
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private ListRFID_Invengo As List(Of Negocio.LectorRFID_Invengo)

    ''' <summary>
    ''' Permite carga los lectores rfid a la grilla
    ''' </summary>
    ''' <param name="ListRFID_SpeedWay"></param>
    Public Sub CargarLectores(ByRef ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay))

        Me.ListRFID_SpeedWay = ListRFID_SpeedWay

        dgEstadoLectores.Rows.Clear()
        For Each lectorRFID As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            dgEstadoLectores.Rows.Add(lectorRFID.Nombre, lectorRFID.Ip, "Conectando...", "Conectando...")
        Next


    End Sub

    Public Sub CargarLectores(ByRef ListRFID_Invengo As List(Of Negocio.LectorRFID_Invengo))

        Me.ListRFID_Invengo = ListRFID_Invengo

        dgEstadoLectores.Rows.Clear()
        If Not IsNothing(ListRFID_Invengo) Then
            For Each lectorRFIDInvengo As Negocio.LectorRFID_Invengo In Me.ListRFID_Invengo
                dgEstadoLectores.Rows.Add(lectorRFIDInvengo.Nombre, "Conectando...", "Conectando...")
            Next
        End If


    End Sub

    ''' <summary>
    ''' Permite refrescar el estado de los lectres
    ''' </summary>
    Public Sub RefrescarEstadosRFID()

        'If IsNothing(Me.ListRFID_SpeedWay) Then Return

        If Not IsNothing(Me.ListRFID_SpeedWay) Then
            For Each RFID_SpeedWay As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay

                For Each row As DataGridViewRow In dgEstadoLectores.Rows

                    If row.Cells(colIp.Index).Value = RFID_SpeedWay.Ip Then
                        Dim oLectorN As New Negocio.LectorRFID_N
                        Dim oLector As New Entidades.LECTOR_RFID
                        oLector = oLectorN.GetOne(RFID_SpeedWay.Ip)
                        If RFID_SpeedWay.LectorConfigurado Then 'SI ESTA CONFIGURADO

                            If RFID_SpeedWay.IsConnected Then 'SI ESTA CONECTADO 
                                row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_CONECTADO
                                row.Cells(colConexion.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_CONECTADO

                                'VERIFICO EL ESTADO DE LECTURA
                                If RFID_SpeedWay.isRunning Then
                                    oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.conectadoLeyendo, Entidades.Constante.EstadoLectura.Leyendo)
                                    row.Cells(colLectura.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_START
                                    row.Cells(colLectura.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_START
                                Else
                                    oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.conectadoSinLectura, Entidades.Constante.EstadoLectura.SinLectura)
                                    row.Cells(colLectura.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_STOP
                                    row.Cells(colLectura.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_STOP
                                End If

                                row.Cells(colAntenas.Index).Value = RFID_SpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0).ANTENAS_RFID.Count()
                            Else
                                'DESCONECTADO
                                oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.desconectado, Entidades.Constante.EstadoLectura.SinLectura)
                                row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_DESCONECTADO
                                row.Cells(colConexion.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_DESCONECTADO

                                row.Cells(colLectura.Index).Style.ForeColor = Color.White
                                row.Cells(colLectura.Index).Value = "-"
                            End If
                        Else
                            oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.sinConfiguracion, Entidades.Constante.EstadoLectura.SinLectura)
                            row.Cells(colConexion.Index).Style.ForeColor = Color.White
                            row.Cells(colConexion.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_SIN_CONFIGURACION

                        End If

                    End If
                Next
            Next


        Else


            If Not IsNothing(Me.ListRFID_Invengo) Then

                For Each RFID_Invengo As Negocio.LectorRFID_Invengo In Me.ListRFID_Invengo

                    For Each row As DataGridViewRow In dgEstadoLectores.Rows

                        If row.Cells(colNombre.Index).Value = RFID_Invengo.Nombre Then
                            Dim oLectorN As New Negocio.LectorRFID_N
                            Dim oLector As New Entidades.Lector_RFID
                            oLector = oLectorN.GetOne(RFID_Invengo.LectorRFID.ID_LECTOR_RFID)
                            If RFID_Invengo.LectorConfigurado Then 'SI ESTA CONFIGURADO


                                If RFID_Invengo.isConnected Then 'SI ESTA CONECTADO 
                                    row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_Invengo.COLOR_CONECTADO
                                    row.Cells(colConexion.Index).Value = Negocio.LectorRFID_Invengo.TEXT_CONECTADO

                                    'VERIFICO EL ESTADO DE LECTURA
                                    If RFID_Invengo.isReading Then
                                        oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.conectadoLeyendo, Entidades.Constante.EstadoLectura.Leyendo)
                                        row.Cells(colLectura.Index).Style.ForeColor = Negocio.LectorRFID_Invengo.COLOR_START
                                        row.Cells(colLectura.Index).Value = Negocio.LectorRFID_Invengo.TEXT_START
                                    Else
                                        oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.conectadoSinLectura, Entidades.Constante.EstadoLectura.SinLectura)
                                        row.Cells(colLectura.Index).Style.ForeColor = Negocio.LectorRFID_Invengo.COLOR_STOP
                                        row.Cells(colLectura.Index).Value = Negocio.LectorRFID_Invengo.TEXT_STOP
                                    End If

                                    'row.Cells(colAntenas.Index).Value = RFID_SpeedWay.LectorRFID.config_lector_rfid(0).ANTENAS_RFID.Count()
                                Else
                                    oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.desconectado, Entidades.Constante.EstadoLectura.SinLectura)
                                    'DESCONECTADO
                                    row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_Invengo.COLOR_DESCONECTADO
                                    row.Cells(colConexion.Index).Value = Negocio.LectorRFID_Invengo.TEXT_DESCONECTADO

                                    row.Cells(colLectura.Index).Style.ForeColor = Color.White
                                    row.Cells(colLectura.Index).Value = "-"
                                End If
                            Else
                                oLectorN.updateEstadoLector(oLector, Entidades.Constante.EstadoLector.sinConfiguracion, Entidades.Constante.EstadoLectura.SinLectura)
                                row.Cells(colConexion.Index).Style.ForeColor = Color.White
                                row.Cells(colConexion.Index).Value = Negocio.LectorRFID_Invengo.TEXT_SIN_CONFIGURACION

                            End If

                        End If
                    Next
                Next

            End If

        End If

    End Sub



    ''' <summary>
    ''' Permite carga los lectores rfid a la grilla
    ''' </summary>
    ''' <param name="ListRFID_SpeedWay"></param>
    Public Sub CargarLectoresCliente(ByRef ListRFID_SpeedWay As List(Of Entidades.Lector_RFID))

        Dim oLista As New List(Of Entidades.Lector_RFID)
        oLista = ListRFID_SpeedWay

        dgEstadoLectores.Rows.Clear()
        For Each lectorRFID As Entidades.Lector_RFID In oLista
            dgEstadoLectores.Rows.Add(lectorRFID.nombre, "Conectando...", "Conectando...")
        Next


    End Sub

    ''' <summary>
    ''' Permite refrescar el estado de los lectres
    ''' </summary>
    Public Sub RefrescarEstadosRFIDCliente(ByVal lista As List(Of Entidades.Lector_RFID))

        If IsNothing(lista) Then Return

        ' For Each RFID_SpeedWay As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
        For Each RFID_Speedway In lista
            For Each row As DataGridViewRow In dgEstadoLectores.Rows

                If row.Cells(colNombre.Index).Value = RFID_Speedway.nombre Then

                    'If RFID_Speedway.LectorConfigurado Then 'SI ESTA CONFIGURADO



                    Select Case RFID_Speedway.conectado
                        Case 0
                            row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_DESCONECTADO
                            row.Cells(colConexion.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_DESCONECTADO

                            row.Cells(colLectura.Index).Style.ForeColor = Color.White
                            row.Cells(colLectura.Index).Value = "-"
                        Case 1
                            row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_CONECTADO
                            row.Cells(colConexion.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_CONECTADO

                            row.Cells(colLectura.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_STOP
                            row.Cells(colLectura.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_STOP
                        Case 2
                            row.Cells(colConexion.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_CONECTADO
                            row.Cells(colConexion.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_CONECTADO

                            row.Cells(colLectura.Index).Style.ForeColor = Negocio.LectorRFID_SpeedWay.COLOR_START
                            row.Cells(colLectura.Index).Value = Negocio.LectorRFID_SpeedWay.TEXT_START
                    End Select


                End If
            Next
        Next
    End Sub
End Class
