﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class MultiLed
    {
        public enum EstiloLetra
        {
            Fina = 165,
            Gruesa = 166
        }

        public enum Efectos
        {
            Automatico = 201,
            Pasante = 202,
            PasanteConDetencionPalabra = 203,
            PasanteTitilando = 204,
            PasanteTitilandoDetencionPalabra = 205,
            Lluvia = 209,
            Spray = 2010,
            PalabraFija = 2011,
            PalabraFijaEncendiendoApagango = 2012,
            Hora = 220,
            Fecha = 221,
            Secuencia1 = 226,
            Secuencia2 = 227
        }

        public enum Velocidad
        {
            Velocidad1 = 171,
            Velocidad2 = 172,
            Velocidad3 = 173,
            Velocidad4 = 174,
            Velocidad5 = 175,
            Velocidad6 = 176

        }
    }
}
