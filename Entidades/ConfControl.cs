﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
     public class ConfControl
    {
         public IdBarrera _IdBarrera;
         public IdSectores _IdSectores;
         public int tiempoBorradoFiltro;

         public ConfControl()
         {
             _IdBarrera = new IdBarrera();
             _IdSectores = new IdSectores();
             tiempoBorradoFiltro = 0;
         }




         public class IdBarrera
         {

             public int IngresoPorteriaExplanada;
             public int EgresoPorteriaExplanada;
             public int Balanza1Barrera1Celda1y2;
             public int Balanza1Barrera2Celda1y2;
             public int Balanza2Barrera1Celda1y2;
             public int Balanza2Barrera2Celda1y2;
             public int BarreraFalsa1Celda1y2;

             public int BarreraFalsa2Celda1y2;
             public int IngresoPorteriaAcopio;
             public int EgresoPorteriaAcopio;
             public int Balanza1Barrera1Acopio;
             public int Balanza1Barrera2Acopio;
             public int Balanza2Barrera1Acopio;

             public int Balanza2Barrera2Acopio;
             public int BarreraFalsa1Acopio;
             public int BarreraFalsa2Acopio;
         }

         public class IdSectores
         {

             public int IngresoPorteriaExplanada ;

             public int EgresoPorteriaExplanada;

             public int Canal1Explanada;

             public int Canal2Explanada;

             public int Balanza1Celda1y2Barrera1;

             public int Balanza1Celda1y2Barrera2;

             public int Balanza2Celda1y2Barrera1 ;

             public int Balanza2Celda1y2Barrera2 ;

             public int IngresoBarreraFalsa1Celda1y2;

             public int EgresoBarreraFalsa1Celda1y2 ;

             public int IngresoBarreraFalsa2Celda1y2;

             public int EgresoBarreraFalsa2Celda1y2;

             public int IngresoPorteriaAcopio ;

             public int EgresoPorteriaAcopio ;

             public int Balanza1AcopioBarrera1 ;

             public int Balanza1AcopioBarrera2 ;

             public int Balanza2AcopioBarrera1 ;

             public int Balanza2AcopioBarrera2 ;

             public int IngresoBarreraFalsa1Acopio ;

             public int EgresoBarreraFalsa1Acopio ;

             public int IngresoBarreraFalsa2Acopio;

             public int EgresoBarreraFalsa2Acopio;


             public string getNombre(int IdSector)
             {
                 string nombreSector = "Error";
              


                 if (IdSector == IngresoPorteriaExplanada)
                 {
                     nombreSector = "Ingreso Portería Explanda";
                 }
                 else if (IdSector == EgresoPorteriaExplanada)
                 {
                     nombreSector = "Egreso Portería Explanada";
                 }
                 else if (IdSector == Canal1Explanada)
                 {
                     nombreSector = "Canal 1 Explanda";
                 }
                 else if (IdSector == Canal2Explanada)
                 {
                     nombreSector = "Canal 2 Explanada";
                 }
                 else if (IdSector == Balanza1Celda1y2Barrera1)
                 {
                     nombreSector = "Balanza 1 Barrera 1 Celda";
                 }
                 else if (IdSector == Balanza1Celda1y2Barrera2)
                 {
                     nombreSector = "Balanza 1 Barrera 2 Celda";
                 }
                 else if (IdSector == Balanza2Celda1y2Barrera1)
                 {
                     nombreSector = "Balanza 2 Barrera 1 Celda";
                 }
                 else if (IdSector == Balanza2Celda1y2Barrera2)
                 {
                     nombreSector = "Balanza 2 Barrera 2 Celda";
                 }
                 else if (IdSector == IngresoBarreraFalsa1Celda1y2)
                 {
                     nombreSector = "Ingreso Barrera Falsa 1 Celda";
                 }
                 else if (IdSector == EgresoBarreraFalsa1Celda1y2)
                 {
                     nombreSector = "Egreso Barrera Falsa 1 Celda";
                 }
                 else if (IdSector == IngresoBarreraFalsa2Celda1y2)
                 {
                     nombreSector = "Ingreso Barrera Falsa 2 Celda";
                 }
                 else if (IdSector == IngresoBarreraFalsa1Celda1y2)
                 {
                     nombreSector = "Egreso Barrera Falsa 2 Celda";
                 }
                 else if (IdSector == IngresoPorteriaAcopio)
                 {
                     nombreSector = "Ingreso Portería Acopio";
                 }
                 else if (IdSector == EgresoPorteriaAcopio)
                 {
                     nombreSector = "Egreso Portería Acopio";
                 }
                 else if (IdSector == Balanza1AcopioBarrera1)
                 {
                     nombreSector = "Balanza 1 Barrera 1 Acopio";
                 }
                 else if (IdSector == Balanza1AcopioBarrera2)
                 {
                     nombreSector = "Balanza 1 Barrera 2 Acopio";
                 }
                 else if (IdSector == Balanza2AcopioBarrera1)
                 {
                     nombreSector = "Balanza 2 Barrera 1 Acopio";
                 }
                 else if (IdSector == Balanza2AcopioBarrera2)
                 {
                     nombreSector = "Balanza 2 Barrera 2 Acopio";
                 }
                 else if (IdSector == IngresoBarreraFalsa1Acopio)
                 {
                     nombreSector = "Ingreso Barrera Falsa 1 Acopio";
                 }
                 else if (IdSector == EgresoBarreraFalsa1Acopio)
                 {
                     nombreSector = "Egreso Barrera Falsa 1 Acopio";
                 }
                 else if (IdSector == IngresoBarreraFalsa2Acopio)
                 {
                     nombreSector = "Ingreso Barrera Falsa 2 Acopio";
                 }
                 else if (IdSector == IngresoBarreraFalsa1Acopio)
                 {
                     nombreSector = "Egreso Barrera Falsa 2 Acopio";
                 }

                 return nombreSector;
             }

         }
     
     }
}
