﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
   public class BorrarBuffer
    {

       public Boolean  porteriaAcopioIngreso { get; set; }
       public Boolean porteriaAcopioEgreso { get; set; }

       public Boolean porteriaExplanadaIngreso { get; set; }
       public Boolean porteriaExplanadaEgreso { get; set; }

       public Boolean balanza1barrera1Acopio { get; set; }
       public Boolean balanza1barrera2Acopio { get; set; }
       public Boolean balanza2barrera1Acopio { get; set; }
       public Boolean balanza2barrera2Acopio { get; set; }

       public Boolean balanza1barrera1Celda { get; set; }
       public Boolean balanza1barrera2Celda { get; set; }
       public Boolean balanza2barrera1Celda { get; set; }
       public Boolean balanza2barrera2Celda { get; set; }

       public Boolean barrerafalsa1Acopio { get; set; }
       public Boolean barrerafalsa2Acopio { get; set; }

       public Boolean barrerafalsa1Celda { get; set; }
       public Boolean barrerafalsa2Celda { get; set; }
       
     




      
    }
}
