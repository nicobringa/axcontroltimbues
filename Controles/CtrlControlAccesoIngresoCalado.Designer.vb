﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlControlAccesoIngresoCalado
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CtrlDinamismoDobleBarrera1 = New Controles.CtrlDinamismoDobleBarrera()
        Me.CartelLED_INGRESO = New Controles.CtrlCartelMultiLED()
        Me.CAM_ING = New Controles.CtrlCamara()
        Me.S_BARRERA = New Controles.CtrlSensor()
        Me.S_LECTURA = New Controles.CtrlSensor()
        Me.B_2 = New Controles.CtrlBarrera()
        Me.B_1 = New Controles.CtrlBarrera()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlDinamismoDobleBarrera2 = New Controles.CtrlDinamismoDobleBarrera()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.CtrlAtenaLeyendo = New Controles.CtrlAntenaLeyendo()
        Me.btnLecturaManualIngreso = New System.Windows.Forms.Button()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.CAM_ING2 = New Controles.CtrlCamara()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CtrlDinamismoDobleBarrera1
        '
        Me.CtrlDinamismoDobleBarrera1.Location = New System.Drawing.Point(0, 0)
        Me.CtrlDinamismoDobleBarrera1.Name = "CtrlDinamismoDobleBarrera1"
        Me.CtrlDinamismoDobleBarrera1.Size = New System.Drawing.Size(811, 496)
        Me.CtrlDinamismoDobleBarrera1.TabIndex = 0
        '
        'CartelLED_INGRESO
        '
        Me.CartelLED_INGRESO.Conectado = False
        Me.CartelLED_INGRESO.GPIO = False
        Me.CartelLED_INGRESO.ID_CONTROL_ACCESO = 0
        Me.CartelLED_INGRESO.ID_PLC = 0
        Me.CartelLED_INGRESO.ID_SECTOR = 0
        Me.CartelLED_INGRESO.Inicializado = False
        Me.CartelLED_INGRESO.Location = New System.Drawing.Point(210, 62)
        Me.CartelLED_INGRESO.Name = "CartelLED_INGRESO"
        Me.CartelLED_INGRESO.PLC = Nothing
        Me.CartelLED_INGRESO.Size = New System.Drawing.Size(171, 99)
        Me.CartelLED_INGRESO.TabIndex = 189
        '
        'CAM_ING
        '
        Me.CAM_ING.AutoSize = True
        Me.CAM_ING.frmCamara = Nothing
        Me.CAM_ING.GPIO = False
        Me.CAM_ING.ID_CONTROL_ACCESO = 0
        Me.CAM_ING.ID_PLC = 0
        Me.CAM_ING.ID_SECTOR = 0
        Me.CAM_ING.Inicializado = False
        Me.CAM_ING.Location = New System.Drawing.Point(3, 62)
        Me.CAM_ING.Name = "CAM_ING"
        Me.CAM_ING.PLC = Nothing
        Me.CAM_ING.Size = New System.Drawing.Size(82, 60)
        Me.CAM_ING.TabIndex = 188
        '
        'S_BARRERA
        '
        Me.S_BARRERA.Estado = False
        Me.S_BARRERA.GPIO = False
        Me.S_BARRERA.ID_CONTROL_ACCESO = 0
        Me.S_BARRERA.ID_PLC = 0
        Me.S_BARRERA.ID_SECTOR = 0
        Me.S_BARRERA.Inicializado = False
        Me.S_BARRERA.Location = New System.Drawing.Point(481, 334)
        Me.S_BARRERA.Name = "S_BARRERA"
        Me.S_BARRERA.PLC = Nothing
        Me.S_BARRERA.Size = New System.Drawing.Size(81, 45)
        Me.S_BARRERA.TabIndex = 187
        '
        'S_LECTURA
        '
        Me.S_LECTURA.Estado = False
        Me.S_LECTURA.GPIO = False
        Me.S_LECTURA.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA.ID_PLC = 0
        Me.S_LECTURA.ID_SECTOR = 0
        Me.S_LECTURA.Inicializado = False
        Me.S_LECTURA.Location = New System.Drawing.Point(495, 274)
        Me.S_LECTURA.Name = "S_LECTURA"
        Me.S_LECTURA.PLC = Nothing
        Me.S_LECTURA.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA.TabIndex = 186
        '
        'B_2
        '
        Me.B_2.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_2.Estado = False
        Me.B_2.GPIO = False
        Me.B_2.ID_CONTROL_ACCESO = 0
        Me.B_2.ID_PLC = 0
        Me.B_2.ID_SECTOR = 0
        Me.B_2.Inicializado = False
        Me.B_2.Location = New System.Drawing.Point(443, 200)
        Me.B_2.Name = "B_2"
        Me.B_2.PLC = Nothing
        Me.B_2.Size = New System.Drawing.Size(62, 52)
        Me.B_2.TabIndex = 185
        '
        'B_1
        '
        Me.B_1.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.B_1.Estado = False
        Me.B_1.GPIO = False
        Me.B_1.ID_CONTROL_ACCESO = 0
        Me.B_1.ID_PLC = 0
        Me.B_1.ID_SECTOR = 0
        Me.B_1.Inicializado = False
        Me.B_1.Location = New System.Drawing.Point(0, 225)
        Me.B_1.Name = "B_1"
        Me.B_1.PLC = Nothing
        Me.B_1.Size = New System.Drawing.Size(62, 52)
        Me.B_1.TabIndex = 184
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, -1)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(844, 42)
        Me.lblNombre.TabIndex = 183
        Me.lblNombre.Text = "Nombre"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlDinamismoDobleBarrera2
        '
        Me.CtrlDinamismoDobleBarrera2.BackColor = System.Drawing.Color.LightGray
        Me.CtrlDinamismoDobleBarrera2.Location = New System.Drawing.Point(1, -1)
        Me.CtrlDinamismoDobleBarrera2.Name = "CtrlDinamismoDobleBarrera2"
        Me.CtrlDinamismoDobleBarrera2.Size = New System.Drawing.Size(596, 401)
        Me.CtrlDinamismoDobleBarrera2.TabIndex = 173
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(596, 38)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(248, 424)
        Me.CtrlPanelNotificaciones1.TabIndex = 190
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 395)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(597, 67)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 191
        '
        'CtrlAtenaLeyendo
        '
        Me.CtrlAtenaLeyendo.Location = New System.Drawing.Point(495, 112)
        Me.CtrlAtenaLeyendo.Name = "CtrlAtenaLeyendo"
        Me.CtrlAtenaLeyendo.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendo.TabIndex = 192
        Me.CtrlAtenaLeyendo.UltTag = Nothing
        '
        'btnLecturaManualIngreso
        '
        Me.btnLecturaManualIngreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(164, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.btnLecturaManualIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManualIngreso.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManualIngreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManualIngreso.Location = New System.Drawing.Point(259, 182)
        Me.btnLecturaManualIngreso.Name = "btnLecturaManualIngreso"
        Me.btnLecturaManualIngreso.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManualIngreso.TabIndex = 194
        Me.btnLecturaManualIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManualIngreso.UseVisualStyleBackColor = False
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(495, 61)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 193
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'CAM_ING2
        '
        Me.CAM_ING2.AutoSize = True
        Me.CAM_ING2.frmCamara = Nothing
        Me.CAM_ING2.GPIO = False
        Me.CAM_ING2.ID_CONTROL_ACCESO = 0
        Me.CAM_ING2.ID_PLC = 0
        Me.CAM_ING2.ID_SECTOR = 0
        Me.CAM_ING2.Inicializado = False
        Me.CAM_ING2.Location = New System.Drawing.Point(407, 62)
        Me.CAM_ING2.Name = "CAM_ING2"
        Me.CAM_ING2.PLC = Nothing
        Me.CAM_ING2.Size = New System.Drawing.Size(82, 60)
        Me.CAM_ING2.TabIndex = 195
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(0, 0)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 41)
        Me.btnLectorRfid.TabIndex = 196
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(801, 0)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(43, 40)
        Me.btnAyuda.TabIndex = 197
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(759, 0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 40)
        Me.btnReset.TabIndex = 198
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'CtrlControlAccesoIngresoCalado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.CAM_ING2)
        Me.Controls.Add(Me.btnLecturaManualIngreso)
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlAtenaLeyendo)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.CartelLED_INGRESO)
        Me.Controls.Add(Me.CAM_ING)
        Me.Controls.Add(Me.S_BARRERA)
        Me.Controls.Add(Me.S_LECTURA)
        Me.Controls.Add(Me.B_2)
        Me.Controls.Add(Me.B_1)
        Me.Controls.Add(Me.CtrlDinamismoDobleBarrera2)
        Me.Controls.Add(Me.CtrlDinamismoDobleBarrera1)
        Me.Name = "CtrlControlAccesoIngresoCalado"
        Me.Size = New System.Drawing.Size(844, 460)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlDinamismoDobleBarrera1 As CtrlDinamismoDobleBarrera
    Friend WithEvents CartelLED_INGRESO As CtrlCartelMultiLED
    Friend WithEvents CAM_ING As CtrlCamara
    Friend WithEvents S_BARRERA As CtrlSensor
    Friend WithEvents S_LECTURA As CtrlSensor
    Public WithEvents B_2 As CtrlBarrera
    Public WithEvents B_1 As CtrlBarrera
    Friend WithEvents lblNombre As Label
    Friend WithEvents CtrlDinamismoDobleBarrera2 As CtrlDinamismoDobleBarrera
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Friend WithEvents CtrlAtenaLeyendo As CtrlAntenaLeyendo
    Friend WithEvents btnAlarma As Button
    Friend WithEvents btnLecturaManualIngreso As Button
    Friend WithEvents CAM_ING2 As CtrlCamara
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents btnReset As Button
End Class
