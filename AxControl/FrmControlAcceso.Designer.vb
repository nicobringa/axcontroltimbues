﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmControlAcceso
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmControlAcceso))
        Me.flpPrincipal = New System.Windows.Forms.FlowLayoutPanel()
        Me.CtrlMenu1 = New Controles.CtrlMenu()
        Me.SuspendLayout()
        '
        'flpPrincipal
        '
        Me.flpPrincipal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flpPrincipal.AutoScroll = True
        Me.flpPrincipal.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.flpPrincipal.Location = New System.Drawing.Point(1, 59)
        Me.flpPrincipal.Name = "flpPrincipal"
        Me.flpPrincipal.Size = New System.Drawing.Size(1502, 678)
        Me.flpPrincipal.TabIndex = 1
        '
        'CtrlMenu1
        '
        Me.CtrlMenu1.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.CtrlMenu1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CtrlMenu1.HabilitarScroll = False
        Me.CtrlMenu1.Location = New System.Drawing.Point(1, 0)
        Me.CtrlMenu1.Name = "CtrlMenu1"
        Me.CtrlMenu1.Size = New System.Drawing.Size(1904, 53)
        Me.CtrlMenu1.TabIndex = 0
        '
        'FrmControlAcceso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1502, 749)
        Me.Controls.Add(Me.flpPrincipal)
        Me.Controls.Add(Me.CtrlMenu1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmControlAcceso"
        Me.Text = "axControl"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents CtrlMenu1 As Controles.CtrlMenu
    Friend WithEvents flpPrincipal As FlowLayoutPanel
End Class
