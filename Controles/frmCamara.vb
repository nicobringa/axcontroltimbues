﻿Imports System.Drawing
Imports Entidades
Imports Entidades.Constante
Public Class frmCamara


    Private oCamara As Entidades.CAMARA
    Public Event SacarFoto(idTrasaccion As Long, ID_SECTOR As Integer, fecha As String)


    Public Sub New(ByVal Camara As Entidades.CAMARA)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.oCamara = Camara

        SeleccionarDatos()

    End Sub


    Private Sub SeleccionarDatos()

        If Not IsNothing(oCamara) Then
            txtDescripcion.Text = oCamara.descripcion
            txtIP.Text = oCamara.IP
            txtRuta.Text = oCamara.RUTA_FOTO
            txtUsuario.Text = oCamara.usuario
            txtContraseña.Text = oCamara.password
            txtPuerto.Text = oCamara.puerto
        End If

    End Sub


    Private Sub btnExaminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminar.Click

        If fbdRutaCam.ShowDialog = DialogResult.OK Then
            txtRuta.Text = fbdRutaCam.SelectedPath

        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Administrador)
        If oAutenticacion.ShowDialog = DialogResult.OK Then
            If CamposObligatorios() Then
                Dim nCamaras As New Negocio.CamaraN
                Dim descripAuditoria As String
                Me.oCamara.ip = txtIP.Text
                Me.oCamara.puerto = txtPuerto.Text
                Me.oCamara.RUTA_FOTO = txtRuta.Text
                Me.oCamara.usuario = txtUsuario.Text
                Me.oCamara.password = txtContraseña.Text



                nCamaras.Update(oCamara)
                descripAuditoria = "Configuro camara: " & oCamara.ID_CAMARA
                Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", 0, 0, WS_ERRO_USUARIO, Constante.acciones.configuracionCamara, 0)
            End If
        Else
            Me.Close()
        End If
    End Sub


    Private Function CamposObligatorios() As Boolean
        Dim completo As Boolean = True
        If String.IsNullOrWhiteSpace(txtIP.Text) Then
            epCamara.SetError(txtIP, "Campo Obligatorio")
            completo = False
        Else
            epCamara.SetError(txtIP, Nothing)
        End If

        If String.IsNullOrWhiteSpace(txtRuta.Text) Then
            epCamara.SetError(txtRuta, "Campo Obligatorio")
            completo = False
        Else
            epCamara.SetError(txtRuta, Nothing)
        End If

        Return completo
    End Function

    Private Sub BtnSacarFoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSacarFoto.Click
        RaiseEvent SacarFoto(0, 0, DateTime.Now.ToString())
    End Sub

    Public Sub MostrarFoto(ByVal loadedBitmap As Bitmap)
        'pbFoto.Image = loadedBitmap
    End Sub

End Class