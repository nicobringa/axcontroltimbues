﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlDinamismoCalado
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.picSLecturaCortando = New System.Windows.Forms.PictureBox()
        Me.picSLecturaNoCortando = New System.Windows.Forms.PictureBox()
        CType(Me.picSLecturaCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSLecturaNoCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picSLecturaCortando
        '
        Me.picSLecturaCortando.Image = Global.Controles.My.Resources.Resources.SROJO
        Me.picSLecturaCortando.Location = New System.Drawing.Point(10, 231)
        Me.picSLecturaCortando.Name = "picSLecturaCortando"
        Me.picSLecturaCortando.Size = New System.Drawing.Size(275, 64)
        Me.picSLecturaCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picSLecturaCortando.TabIndex = 2
        Me.picSLecturaCortando.TabStop = False
        '
        'picSLecturaNoCortando
        '
        Me.picSLecturaNoCortando.Image = Global.Controles.My.Resources.Resources.SVERDE
        Me.picSLecturaNoCortando.Location = New System.Drawing.Point(7, 234)
        Me.picSLecturaNoCortando.Name = "picSLecturaNoCortando"
        Me.picSLecturaNoCortando.Size = New System.Drawing.Size(282, 63)
        Me.picSLecturaNoCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picSLecturaNoCortando.TabIndex = 1
        Me.picSLecturaNoCortando.TabStop = False
        '
        'CtrlDinamismoCalado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Controles.My.Resources.Resources.CaladoBase
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Controls.Add(Me.picSLecturaNoCortando)
        Me.Controls.Add(Me.picSLecturaCortando)
        Me.Name = "CtrlDinamismoCalado"
        Me.Size = New System.Drawing.Size(306, 400)
        CType(Me.picSLecturaCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSLecturaNoCortando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picSLecturaNoCortando As PictureBox
    Friend WithEvents picSLecturaCortando As PictureBox
End Class
