﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class objLectorRFID
    {
        public int id { get; set; }
        public LECTOR_RFID Lector_rfid { get; set; }

        public List<ANTENAS_RFID> ListAntena { get; set; }

        public List<ASIGNACION_ANTENA> Listasignacion_antena { get; set; }

        public CONFIG_LECTOR_RFID ConfigLector { get; set; }

        public MODELO_LECTOR_RFID ModeloLector { get; set; }
    }

}