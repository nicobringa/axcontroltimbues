﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
namespace AxControlWeb.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
         
                return View();
            

            
        }

        public PartialViewResult mapa()
        {
            try
            {
                AxControlORAEntities db = new AxControlORAEntities();


                var listaAlertas = (from N in db.NOTIFICACION where N.TIPO_NOTIFICACION == "1" select N).ToList();
                db.Dispose();
                db = null;
                ViewBag.T1 = "";
                ViewBag.T2 = "";
                ViewBag.T3 = "";
                ViewBag.T4 = "";
                ViewBag.T5 = "";
                ViewBag.T6 = "";
                ViewBag.T7 = "";
                ViewBag.T8 = "";
                ViewBag.T9 = "";
                ViewBag.T12 = "";

                foreach (var item in listaAlertas)
                {
                    switch ((int)item.ID_CONTROL_ACCESO)
                    {
                        case 100:
                            ViewBag.T1 = "ALERTA";
                            break;
                        case 200:
                            ViewBag.T2 = "ALERTA";
                            break;
                        case 300:
                            ViewBag.T3 = "ALERTA";
                            break;
                        case 400:
                            ViewBag.T4 = "ALERTA";
                            break;
                        case 500:
                            ViewBag.T5 = "ALERTA";
                            break;
                        case 600:
                            ViewBag.T6 = "ALERTA";
                            break;
                        case 700:
                            ViewBag.T7 = "ALERTA";
                            break;
                        case 800:
                            ViewBag.T8 = "ALERTA";
                            break;
                        case 900:
                            ViewBag.T9 = "ALERTA";
                            break;
                        case 1200:
                            ViewBag.T12 = "ALERTA";
                            break;
                    }


                }

                return PartialView();
            }
            catch (Exception ex)
            {

                return PartialView();
            }

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}