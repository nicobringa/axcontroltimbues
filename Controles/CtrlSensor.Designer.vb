﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlSensor


    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlSensor))
        Me.lbTAG = New System.Windows.Forms.Label()
        Me.btnConfig = New System.Windows.Forms.Button()
        Me.EP = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.pbSensorDeshab = New System.Windows.Forms.PictureBox()
        CType(Me.EP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbSensorDeshab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbTAG
        '
        Me.lbTAG.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbTAG.BackColor = System.Drawing.Color.MediumBlue
        Me.lbTAG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTAG.ForeColor = System.Drawing.Color.White
        Me.lbTAG.Location = New System.Drawing.Point(0, 0)
        Me.lbTAG.Name = "lbTAG"
        Me.lbTAG.Size = New System.Drawing.Size(119, 20)
        Me.lbTAG.TabIndex = 120
        Me.lbTAG.Text = "TAG"
        Me.lbTAG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnConfig
        '
        Me.btnConfig.AccessibleDescription = ""
        Me.btnConfig.BackColor = System.Drawing.Color.Red
        Me.btnConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConfig.Image = Global.Controles.My.Resources.Resources.Configuration19
        Me.btnConfig.Location = New System.Drawing.Point(0, 20)
        Me.btnConfig.Name = "btnConfig"
        Me.btnConfig.Size = New System.Drawing.Size(26, 25)
        Me.btnConfig.TabIndex = 121
        Me.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnConfig.UseVisualStyleBackColor = False
        '
        'EP
        '
        Me.EP.BlinkRate = 2000
        Me.EP.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.EP.ContainerControl = Me
        Me.EP.Icon = CType(resources.GetObject("EP.Icon"), System.Drawing.Icon)
        '
        'pbSensorDeshab
        '
        Me.pbSensorDeshab.AccessibleDescription = ""
        Me.pbSensorDeshab.BackColor = System.Drawing.Color.Transparent
        Me.pbSensorDeshab.Image = Global.Controles.My.Resources.Resources.DeshabilitarSensor
        Me.pbSensorDeshab.Location = New System.Drawing.Point(32, 20)
        Me.pbSensorDeshab.Name = "pbSensorDeshab"
        Me.pbSensorDeshab.Size = New System.Drawing.Size(26, 25)
        Me.pbSensorDeshab.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbSensorDeshab.TabIndex = 122
        Me.pbSensorDeshab.TabStop = False
        '
        'CtrlSensorLectura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pbSensorDeshab)
        Me.Controls.Add(Me.btnConfig)
        Me.Controls.Add(Me.lbTAG)
        Me.Name = "CtrlSensorLectura"
        Me.Size = New System.Drawing.Size(119, 45)
        CType(Me.EP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbSensorDeshab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lbTAG As Label
    Friend WithEvents btnConfig As Button
    Friend WithEvents EP As ErrorProvider
    Friend WithEvents pbSensorDeshab As PictureBox
End Class
