﻿Imports AccesoDatos
Imports Entidades

Public Class DatoBoolN

    Public Function Add(ByVal oDato As DATO_BOOL) As DATO_BOOL
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.add(oDato)
    End Function

    Public Function Delete(ByVal oDato As DATO_BOOL) As Boolean
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.delete(oDato)
    End Function

    Public Function Update(ByVal oDato As DATO_BOOL) As DATO_BOOL
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.update(oDato)
    End Function

    Public Function GetOne(ByVal id As Integer) As DATO_BOOL
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.getOne(id)
    End Function

    Public Function GetOne(ByVal idplc As Integer, ByVal tag As String) As DATO_BOOL
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.getOne(idplc, tag)
    End Function

    Public Function GetAll() As List(Of DATO_BOOL)
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.getAll
    End Function

    Public Function GetAll(ByVal idPLC As Integer) As List(Of DATO_BOOL)
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.getAll(idPLC)
    End Function

    'int idplc, string nombreTag, int idSector
    Public Function GetAllTags(ByVal idPLC As Integer, nombreTag As String, idSector As Integer) As List(Of DATO_BOOL)
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.getAllTags(idPLC, nombreTag, idSector)
    End Function

    Public Function Leer(ByVal idTAG As Integer) As DATO_BOOL
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.leer(idTAG)
    End Function

    Public Function Escribir(ByVal idTAG As Integer, ByVal valor As Boolean) As DATO_BOOL
        Dim oDatoAD As New DatoBoolAD
        Return oDatoAD.escribir(idTAG, valor)
    End Function

End Class
