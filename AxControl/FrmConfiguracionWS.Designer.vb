﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfiguracionWS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.lblNombreServidor = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtRuta = New System.Windows.Forms.TextBox()
        Me.cbxSimularRespuesta = New System.Windows.Forms.CheckBox()
        Me.gbRespuesta = New System.Windows.Forms.GroupBox()
        Me.rbFalse = New System.Windows.Forms.RadioButton()
        Me.rbTrue = New System.Windows.Forms.RadioButton()
        Me.gbRespuesta.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitulo
        '
        Me.lblTitulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(-2, 0)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(729, 44)
        Me.lblTitulo.TabIndex = 1
        Me.lblTitulo.Text = "Configuración Web Service"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNombreServidor
        '
        Me.lblNombreServidor.AutoSize = True
        Me.lblNombreServidor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreServidor.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreServidor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.lblNombreServidor.Location = New System.Drawing.Point(12, 67)
        Me.lblNombreServidor.Name = "lblNombreServidor"
        Me.lblNombreServidor.Size = New System.Drawing.Size(88, 24)
        Me.lblNombreServidor.TabIndex = 2
        Me.lblNombreServidor.Text = "Ruta WS:"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(2, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(37, Byte), Integer))
        Me.btnGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(161, Byte), Integer))
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.White
        Me.btnGuardar.Location = New System.Drawing.Point(261, 190)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(209, 50)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'txtRuta
        '
        Me.txtRuta.Location = New System.Drawing.Point(121, 71)
        Me.txtRuta.Name = "txtRuta"
        Me.txtRuta.Size = New System.Drawing.Size(576, 20)
        Me.txtRuta.TabIndex = 5
        '
        'cbxSimularRespuesta
        '
        Me.cbxSimularRespuesta.AutoSize = True
        Me.cbxSimularRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSimularRespuesta.Location = New System.Drawing.Point(16, 110)
        Me.cbxSimularRespuesta.Name = "cbxSimularRespuesta"
        Me.cbxSimularRespuesta.Size = New System.Drawing.Size(198, 24)
        Me.cbxSimularRespuesta.TabIndex = 8
        Me.cbxSimularRespuesta.Text = "Simular axRespuesta"
        Me.cbxSimularRespuesta.UseVisualStyleBackColor = True
        '
        'gbRespuesta
        '
        Me.gbRespuesta.Controls.Add(Me.rbFalse)
        Me.gbRespuesta.Controls.Add(Me.rbTrue)
        Me.gbRespuesta.Location = New System.Drawing.Point(14, 140)
        Me.gbRespuesta.Name = "gbRespuesta"
        Me.gbRespuesta.Size = New System.Drawing.Size(118, 50)
        Me.gbRespuesta.TabIndex = 9
        Me.gbRespuesta.TabStop = False
        Me.gbRespuesta.Text = "Respuesta"
        '
        'rbFalse
        '
        Me.rbFalse.AutoSize = True
        Me.rbFalse.Location = New System.Drawing.Point(59, 17)
        Me.rbFalse.Name = "rbFalse"
        Me.rbFalse.Size = New System.Drawing.Size(50, 17)
        Me.rbFalse.TabIndex = 11
        Me.rbFalse.TabStop = True
        Me.rbFalse.Text = "False"
        Me.rbFalse.UseVisualStyleBackColor = True
        '
        'rbTrue
        '
        Me.rbTrue.AutoSize = True
        Me.rbTrue.Location = New System.Drawing.Point(6, 17)
        Me.rbTrue.Name = "rbTrue"
        Me.rbTrue.Size = New System.Drawing.Size(47, 17)
        Me.rbTrue.TabIndex = 10
        Me.rbTrue.TabStop = True
        Me.rbTrue.Text = "True"
        Me.rbTrue.UseVisualStyleBackColor = True
        '
        'FrmConfiguracionWS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(722, 262)
        Me.Controls.Add(Me.gbRespuesta)
        Me.Controls.Add(Me.cbxSimularRespuesta)
        Me.Controls.Add(Me.txtRuta)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblNombreServidor)
        Me.Controls.Add(Me.lblTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "FrmConfiguracionWS"
        Me.Text = "Configuracion Web Service"
        Me.gbRespuesta.ResumeLayout(False)
        Me.gbRespuesta.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTitulo As Label
    Friend WithEvents lblNombreServidor As Label
    Friend WithEvents btnGuardar As Button
    Friend WithEvents txtRuta As TextBox
    Friend WithEvents cbxSimularRespuesta As CheckBox
    Friend WithEvents gbRespuesta As GroupBox
    Friend WithEvents rbFalse As RadioButton
    Friend WithEvents rbTrue As RadioButton
End Class
