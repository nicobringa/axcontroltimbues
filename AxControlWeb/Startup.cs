﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AxControlWeb.Startup))]
namespace AxControlWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
