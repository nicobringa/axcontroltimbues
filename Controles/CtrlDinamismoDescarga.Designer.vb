﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlDinamismoDescarga
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFallaB1 = New System.Windows.Forms.Label()
        Me.lblFallaB2 = New System.Windows.Forms.Label()
        Me.picSensor1Cortando = New System.Windows.Forms.PictureBox()
        Me.picSensorLecturaCortando = New System.Windows.Forms.PictureBox()
        Me.picSensorBar2Cortando = New System.Windows.Forms.PictureBox()
        Me.picBarrera1Abajo = New System.Windows.Forms.PictureBox()
        Me.picBarrera2Abajo = New System.Windows.Forms.PictureBox()
        Me.picBarrera1Arriba = New System.Windows.Forms.PictureBox()
        Me.picBarrera2Arriba = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.picPlataformaDescarga = New System.Windows.Forms.PictureBox()
        CType(Me.picSensor1Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensorLecturaCortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSensorBar2Cortando, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera1Abajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera2Abajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera1Arriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarrera2Arriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPlataformaDescarga, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFallaB1
        '
        Me.lblFallaB1.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaB1.AutoSize = True
        Me.lblFallaB1.BackColor = System.Drawing.Color.Red
        Me.lblFallaB1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaB1.ForeColor = System.Drawing.Color.White
        Me.lblFallaB1.Location = New System.Drawing.Point(29, 233)
        Me.lblFallaB1.Name = "lblFallaB1"
        Me.lblFallaB1.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaB1.TabIndex = 100
        Me.lblFallaB1.Text = "Falla"
        Me.lblFallaB1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFallaB2
        '
        Me.lblFallaB2.AccessibleDescription = "Falla Barrera Izquierda Ingreso"
        Me.lblFallaB2.AutoSize = True
        Me.lblFallaB2.BackColor = System.Drawing.Color.Red
        Me.lblFallaB2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFallaB2.ForeColor = System.Drawing.Color.White
        Me.lblFallaB2.Location = New System.Drawing.Point(35, 57)
        Me.lblFallaB2.Name = "lblFallaB2"
        Me.lblFallaB2.Size = New System.Drawing.Size(48, 20)
        Me.lblFallaB2.TabIndex = 101
        Me.lblFallaB2.Text = "Falla"
        Me.lblFallaB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picSensor1Cortando
        '
        Me.picSensor1Cortando.Image = Global.Controles.My.Resources.Resources.sensorPosicionB1Descarga
        Me.picSensor1Cortando.Location = New System.Drawing.Point(102, 221)
        Me.picSensor1Cortando.Name = "picSensor1Cortando"
        Me.picSensor1Cortando.Size = New System.Drawing.Size(110, 14)
        Me.picSensor1Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensor1Cortando.TabIndex = 107
        Me.picSensor1Cortando.TabStop = False
        '
        'picSensorLecturaCortando
        '
        Me.picSensorLecturaCortando.Image = Global.Controles.My.Resources.Resources.SensorLecturaRojo
        Me.picSensorLecturaCortando.Location = New System.Drawing.Point(68, 242)
        Me.picSensorLecturaCortando.Name = "picSensorLecturaCortando"
        Me.picSensorLecturaCortando.Size = New System.Drawing.Size(181, 111)
        Me.picSensorLecturaCortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensorLecturaCortando.TabIndex = 106
        Me.picSensorLecturaCortando.TabStop = False
        '
        'picSensorBar2Cortando
        '
        Me.picSensorBar2Cortando.Image = Global.Controles.My.Resources.Resources.S2PosicionCortando
        Me.picSensorBar2Cortando.Location = New System.Drawing.Point(126, 101)
        Me.picSensorBar2Cortando.Name = "picSensorBar2Cortando"
        Me.picSensorBar2Cortando.Size = New System.Drawing.Size(35, 10)
        Me.picSensorBar2Cortando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSensorBar2Cortando.TabIndex = 104
        Me.picSensorBar2Cortando.TabStop = False
        '
        'picBarrera1Abajo
        '
        Me.picBarrera1Abajo.Image = Global.Controles.My.Resources.Resources.Barrera1AbajoDescarga
        Me.picBarrera1Abajo.Location = New System.Drawing.Point(77, 194)
        Me.picBarrera1Abajo.Name = "picBarrera1Abajo"
        Me.picBarrera1Abajo.Size = New System.Drawing.Size(143, 26)
        Me.picBarrera1Abajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera1Abajo.TabIndex = 103
        Me.picBarrera1Abajo.TabStop = False
        '
        'picBarrera2Abajo
        '
        Me.picBarrera2Abajo.Image = Global.Controles.My.Resources.Resources.barrera2abajoDescarga
        Me.picBarrera2Abajo.Location = New System.Drawing.Point(115, 92)
        Me.picBarrera2Abajo.Name = "picBarrera2Abajo"
        Me.picBarrera2Abajo.Size = New System.Drawing.Size(54, 10)
        Me.picBarrera2Abajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera2Abajo.TabIndex = 102
        Me.picBarrera2Abajo.TabStop = False
        '
        'picBarrera1Arriba
        '
        Me.picBarrera1Arriba.Image = Global.Controles.My.Resources.Resources.Barrera1Arriba
        Me.picBarrera1Arriba.Location = New System.Drawing.Point(74, 19)
        Me.picBarrera1Arriba.Name = "picBarrera1Arriba"
        Me.picBarrera1Arriba.Size = New System.Drawing.Size(25, 230)
        Me.picBarrera1Arriba.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera1Arriba.TabIndex = 5
        Me.picBarrera1Arriba.TabStop = False
        '
        'picBarrera2Arriba
        '
        Me.picBarrera2Arriba.Image = Global.Controles.My.Resources.Resources.Barrera2Arriba
        Me.picBarrera2Arriba.Location = New System.Drawing.Point(111, 22)
        Me.picBarrera2Arriba.Name = "picBarrera2Arriba"
        Me.picBarrera2Arriba.Size = New System.Drawing.Size(15, 96)
        Me.picBarrera2Arriba.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picBarrera2Arriba.TabIndex = 6
        Me.picBarrera2Arriba.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Controles.My.Resources.Resources.sbv
        Me.PictureBox1.Location = New System.Drawing.Point(-22, -3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(363, 401)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'picPlataformaDescarga
        '
        Me.picPlataformaDescarga.Image = Global.Controles.My.Resources.Resources.plataformaArriba
        Me.picPlataformaDescarga.Location = New System.Drawing.Point(111, 36)
        Me.picPlataformaDescarga.Name = "picPlataformaDescarga"
        Me.picPlataformaDescarga.Size = New System.Drawing.Size(76, 179)
        Me.picPlataformaDescarga.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picPlataformaDescarga.TabIndex = 108
        Me.picPlataformaDescarga.TabStop = False
        '
        'CtrlDinamismoDescarga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.picBarrera1Abajo)
        Me.Controls.Add(Me.picPlataformaDescarga)
        Me.Controls.Add(Me.picSensor1Cortando)
        Me.Controls.Add(Me.picSensorLecturaCortando)
        Me.Controls.Add(Me.picSensorBar2Cortando)
        Me.Controls.Add(Me.picBarrera2Abajo)
        Me.Controls.Add(Me.picBarrera1Arriba)
        Me.Controls.Add(Me.lblFallaB2)
        Me.Controls.Add(Me.lblFallaB1)
        Me.Controls.Add(Me.picBarrera2Arriba)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "CtrlDinamismoDescarga"
        Me.Size = New System.Drawing.Size(336, 392)
        CType(Me.picSensor1Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensorLecturaCortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSensorBar2Cortando, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera1Abajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera2Abajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera1Arriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarrera2Arriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPlataformaDescarga, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents picBarrera1Arriba As PictureBox
    Friend WithEvents picBarrera2Arriba As PictureBox
    Friend WithEvents lblFallaB1 As Label
    Friend WithEvents lblFallaB2 As Label
    Friend WithEvents picBarrera2Abajo As PictureBox
    Friend WithEvents picBarrera1Abajo As PictureBox
    Friend WithEvents picSensorBar2Cortando As PictureBox
    Friend WithEvents picSensorLecturaCortando As PictureBox
    Friend WithEvents picSensor1Cortando As PictureBox
    Friend WithEvents picPlataformaDescarga As PictureBox
End Class
