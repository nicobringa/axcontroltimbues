﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CtrlAntenaLeyendo
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnBorrarBuffer = New System.Windows.Forms.Button()
        Me.lblTiempoBuffer = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Controles.My.Resources.Resources.LeerAntManual
        Me.PictureBox1.Location = New System.Drawing.Point(24, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(33, 34)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'btnBorrarBuffer
        '
        Me.btnBorrarBuffer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBorrarBuffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBorrarBuffer.Location = New System.Drawing.Point(3, 46)
        Me.btnBorrarBuffer.Name = "btnBorrarBuffer"
        Me.btnBorrarBuffer.Size = New System.Drawing.Size(79, 23)
        Me.btnBorrarBuffer.TabIndex = 2
        Me.btnBorrarBuffer.Text = "Borrar Buffer"
        Me.btnBorrarBuffer.UseVisualStyleBackColor = True
        '
        'lblTiempoBuffer
        '
        Me.lblTiempoBuffer.AutoSize = True
        Me.lblTiempoBuffer.Location = New System.Drawing.Point(8, 11)
        Me.lblTiempoBuffer.Name = "lblTiempoBuffer"
        Me.lblTiempoBuffer.Size = New System.Drawing.Size(10, 13)
        Me.lblTiempoBuffer.TabIndex = 3
        Me.lblTiempoBuffer.Text = "-"
        '
        'CtrlAntenaLeyendo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblTiempoBuffer)
        Me.Controls.Add(Me.btnBorrarBuffer)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "CtrlAntenaLeyendo"
        Me.Size = New System.Drawing.Size(86, 72)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnBorrarBuffer As System.Windows.Forms.Button
    Friend WithEvents lblTiempoBuffer As System.Windows.Forms.Label
End Class
