﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlCartelMultiLED
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtMensaje = New System.Windows.Forms.TextBox()
        Me.lbTAG = New System.Windows.Forms.Label()
        Me.btnConf = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblEstado
        '
        Me.lblEstado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEstado.BackColor = System.Drawing.Color.Red
        Me.lblEstado.ForeColor = System.Drawing.Color.White
        Me.lblEstado.Location = New System.Drawing.Point(33, 21)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(161, 20)
        Me.lblEstado.TabIndex = 85
        Me.lblEstado.Text = "Desconectado"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtMensaje
        '
        Me.txtMensaje.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMensaje.BackColor = System.Drawing.Color.Black
        Me.txtMensaje.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMensaje.ForeColor = System.Drawing.Color.Gold
        Me.txtMensaje.Location = New System.Drawing.Point(0, 42)
        Me.txtMensaje.Multiline = True
        Me.txtMensaje.Name = "txtMensaje"
        Me.txtMensaje.ReadOnly = True
        Me.txtMensaje.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.txtMensaje.Size = New System.Drawing.Size(194, 55)
        Me.txtMensaje.TabIndex = 87
        Me.txtMensaje.Text = "Renglon1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Renglon2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Renglon3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Renglon4"
        Me.txtMensaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbTAG
        '
        Me.lbTAG.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbTAG.BackColor = System.Drawing.Color.MediumBlue
        Me.lbTAG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTAG.ForeColor = System.Drawing.Color.White
        Me.lbTAG.Location = New System.Drawing.Point(0, 0)
        Me.lbTAG.Name = "lbTAG"
        Me.lbTAG.Size = New System.Drawing.Size(194, 20)
        Me.lbTAG.TabIndex = 88
        Me.lbTAG.Text = "TAG"
        Me.lbTAG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnConf
        '
        Me.btnConf.BackColor = System.Drawing.Color.Transparent
        Me.btnConf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConf.Image = Global.Controles.My.Resources.Resources.Configuration19
        Me.btnConf.Location = New System.Drawing.Point(0, 21)
        Me.btnConf.Name = "btnConf"
        Me.btnConf.Size = New System.Drawing.Size(32, 20)
        Me.btnConf.TabIndex = 84
        Me.btnConf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnConf.UseVisualStyleBackColor = False
        '
        'CtrlCartelMultiLED
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lbTAG)
        Me.Controls.Add(Me.txtMensaje)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnConf)
        Me.Name = "CtrlCartelMultiLED"
        Me.Size = New System.Drawing.Size(194, 100)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnConf As System.Windows.Forms.Button
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtMensaje As System.Windows.Forms.TextBox
    Friend WithEvents lbTAG As System.Windows.Forms.Label

End Class
