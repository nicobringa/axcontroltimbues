﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using System.Data.Objects;
using System.Data.Entity.Validation;


namespace AccesoDatos
{
    public class AuditoriaAD
    {
       

        public Entidades.AUDITORIA add(Entidades.AUDITORIA oAuditoria)
        {

            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {
                    oContext.AUDITORIA.Add(oAuditoria);
                    oContext.SaveChanges();
                    return oAuditoria;
                }
            }
            catch (Exception ex)
            {
                
                
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAll(DateTime fechaDesde, DateTime fechaHasta)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                        where EntityFunctions.TruncateTime(tabla.FECHA) >= EntityFunctions.TruncateTime(fechaDesde)
									&& EntityFunctions.TruncateTime(tabla.FECHA) <= EntityFunctions.TruncateTime(fechaHasta)
                                    orderby tabla.FECHA descending
                                        select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {
                
               // AxError.setError(ex);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAll(DateTime fechaDesde, DateTime fechaHasta , Constante.acciones ACCION)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    where EntityFunctions.TruncateTime(tabla.FECHA) >= EntityFunctions.TruncateTime(fechaDesde)
                                && EntityFunctions.TruncateTime(tabla.FECHA) <= EntityFunctions.TruncateTime(fechaHasta)
                                && tabla.ACCION == (int)ACCION
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

                //axError.setMsjError(ex, TAG);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAllGeneral()
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

                //axError.setMsjError(ex, TAG);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAllTraccion(DateTime fechaDesde, DateTime fechaHasta , int trasaccion)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    where EntityFunctions.TruncateTime(tabla.FECHA) >= EntityFunctions.TruncateTime(fechaDesde)
                                && EntityFunctions.TruncateTime(tabla.FECHA) <= EntityFunctions.TruncateTime(fechaHasta)
                                && tabla.ID_TRANSACCION == trasaccion
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

               // axError.setMsjError(ex, TAG);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAllUsuario(DateTime fechaDesde, DateTime fechaHasta, string usuario)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    where EntityFunctions.TruncateTime(tabla.FECHA) >= EntityFunctions.TruncateTime(fechaDesde)
                                && EntityFunctions.TruncateTime(tabla.FECHA) <= EntityFunctions.TruncateTime(fechaHasta)
                                && tabla.USUARIO == usuario
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

                //axError.setMsjError(ex, TAG);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAllTagRFID(DateTime fechaDesde, DateTime fechaHasta , string tagRFID)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    where EntityFunctions.TruncateTime(tabla.FECHA) >= EntityFunctions.TruncateTime(fechaDesde)
                                && EntityFunctions.TruncateTime(tabla.FECHA) <= EntityFunctions.TruncateTime(fechaHasta)
                                && tabla.TAG_RFID == tagRFID
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

               // axError.setMsjError(ex, TAG);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getFiltro(DateTime fechaDesde, DateTime fechaHasta, string tagRFID, int sector, int transaccion, int ACCION, string descripcion)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {
                    var oConsulta = from tabla in oContext.AUDITORIA select tabla;
                    //Agrego al tag la palabra contains para que busque similares cómo si fuera un like
                    if (tagRFID.Length > 0) { oConsulta = oConsulta.Where(t => t.TAG_RFID.Contains(tagRFID)); }
                    if (transaccion > 0) { oConsulta = oConsulta.Where(t => t.ID_TRANSACCION == transaccion); }
                    if (ACCION > 0) { oConsulta = oConsulta.Where(t => t.ACCION == ACCION); }
                    if (sector > 0) { oConsulta = oConsulta.Where(t => t.ID_SECTOR == sector); }
                    if (descripcion.Length > 0) { oConsulta = oConsulta.Where(t => t.DESCRIPCION.Contains(descripcion)); }
                    if (fechaDesde.Year > 2000) { oConsulta = oConsulta.Where(t => t.FECHA > fechaDesde); }
                    if (fechaHasta.Year > 2000) { oConsulta = oConsulta.Where(t => t.FECHA < fechaHasta); }
                    return oConsulta.AsEnumerable().Select(t => new Entidades.AUDITORIA
                    {
                        TAG_RFID = t.TAG_RFID,
                        ID_TRANSACCION = t.ID_TRANSACCION,
                        ACCION = t.ACCION,
                        ID_SECTOR = t.ID_SECTOR,
                        DESCRIPCION = t.DESCRIPCION,
                        FECHA = t.FECHA,
                        USUARIO = t.USUARIO
                    }).ToList();



               

                }
            }
            catch (Exception ex)
            {

                // axError.setMsjError(ex, TAG);
                return null;
            }
        }


        public List<Entidades.AUDITORIA> getAllDescripcion(DateTime fechaDesde, DateTime fechaHasta, string descripcion)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    where EntityFunctions.TruncateTime(tabla.FECHA) >= EntityFunctions.TruncateTime(fechaDesde)
                                && EntityFunctions.TruncateTime(tabla.FECHA) <= EntityFunctions.TruncateTime(fechaHasta)
                                && tabla.DESCRIPCION.Contains(descripcion)
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

                //axError.setMsjError(ex, TAG);
                return null;
            }
        }

        public List<Entidades.AUDITORIA> getAllPorSector(int sector)
        {
            try
            {
                using (Entidades.AxControlORAEntities oContext = new Entidades.AxControlORAEntities())
                {

                    var Consulta = (from tabla in oContext.AUDITORIA
                                    where tabla.ID_SECTOR == sector
                                    orderby tabla.FECHA descending
                                    select tabla
                                        ).ToList();

                    return Consulta;

                }
            }
            catch (Exception ex)
            {

                //axError.setMsjError(ex, TAG);
                return null;
            }
        }

        
    }
}
