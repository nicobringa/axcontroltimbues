﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ctrlInfomacionBalanza
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnEspPesoDisplayIzq = New System.Windows.Forms.Button()
        Me.btnCAMION_PESANDOIzq = New System.Windows.Forms.Button()
        Me.ToolTipInfoBalanza = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblCant = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnEspPesoDisplayIzq
        '
        Me.btnEspPesoDisplayIzq.AccessibleDescription = "Esperando peso display Balanza Izquieda"
        Me.btnEspPesoDisplayIzq.BackColor = System.Drawing.Color.White
        Me.btnEspPesoDisplayIzq.Enabled = False
        Me.btnEspPesoDisplayIzq.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEspPesoDisplayIzq.Image = Global.Controles.My.Resources.Resources.EsperandoPesoDisplay
        Me.btnEspPesoDisplayIzq.Location = New System.Drawing.Point(46, -1)
        Me.btnEspPesoDisplayIzq.Name = "btnEspPesoDisplayIzq"
        Me.btnEspPesoDisplayIzq.Size = New System.Drawing.Size(83, 38)
        Me.btnEspPesoDisplayIzq.TabIndex = 92
        Me.btnEspPesoDisplayIzq.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTipInfoBalanza.SetToolTip(Me.btnEspPesoDisplayIzq, "Esperando Respuesta WS")
        Me.btnEspPesoDisplayIzq.UseVisualStyleBackColor = False
        '
        'btnCAMION_PESANDOIzq
        '
        Me.btnCAMION_PESANDOIzq.BackColor = System.Drawing.Color.White
        Me.btnCAMION_PESANDOIzq.Enabled = False
        Me.btnCAMION_PESANDOIzq.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        'Me.btnCAMION_PESANDOIzq.Image = Global.Controles.My.Resources.Resources.CAMION_PESANDO32
        Me.btnCAMION_PESANDOIzq.Location = New System.Drawing.Point(0, -1)
        Me.btnCAMION_PESANDOIzq.Name = "btnCAMION_PESANDOIzq"
        Me.btnCAMION_PESANDOIzq.Size = New System.Drawing.Size(47, 38)
        Me.btnCAMION_PESANDOIzq.TabIndex = 90
        Me.btnCAMION_PESANDOIzq.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTipInfoBalanza.SetToolTip(Me.btnCAMION_PESANDOIzq, "Camión Pesando")
        Me.btnCAMION_PESANDOIzq.UseVisualStyleBackColor = False
        '
        'ToolTipInfoBalanza
        '
        Me.ToolTipInfoBalanza.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning
        '
        'lblCant
        '
        Me.lblCant.AutoSize = True
        Me.lblCant.BackColor = System.Drawing.Color.White
        Me.lblCant.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCant.Location = New System.Drawing.Point(116, -1)
        Me.lblCant.Name = "lblCant"
        Me.lblCant.Size = New System.Drawing.Size(10, 13)
        Me.lblCant.TabIndex = 93
        Me.lblCant.Text = "-"
        '
        'ctrlInfomacionBalanza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.lblCant)
        Me.Controls.Add(Me.btnEspPesoDisplayIzq)
        Me.Controls.Add(Me.btnCAMION_PESANDOIzq)
        Me.Name = "ctrlInfomacionBalanza"
        Me.Size = New System.Drawing.Size(129, 36)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnEspPesoDisplayIzq As System.Windows.Forms.Button
    Friend WithEvents btnCAMION_PESANDOIzq As System.Windows.Forms.Button
    Friend WithEvents ToolTipInfoBalanza As System.Windows.Forms.ToolTip
    Friend WithEvents lblCant As Label
End Class
