﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;


namespace AccesoDatos
{
    public class BalanzaAD
    {

        public double getpeso(string tag)
        {
            try
            {
                using (AxDriverORAEntities oContext = new AxDriverORAEntities())
                {
                    var oConsulta = oContext.DatosFloat.Where(a => a.tag == tag).Single();

                    return oConsulta.valor;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        //public ConstanteBalanza.Balanzas habPesoSistema(int idBalanza, Boolean hab)
        //{
        //    try
        //    {
        //        using (ax_ControlEntities oContext = new ax_ControlEntities())
        //        {

        //            var oConsulta = (from tabla in oContext.Balanzas
        //                             where tabla.id == idBalanza
        //                             select tabla).SingleOrDefault();
        //            if (oConsulta != null)
        //            {
        //                oConsulta.habPesoSistema = hab;

        //                oContext.SaveChanges();
        //                return oConsulta;
        //            }
        //            else
        //            {
        //                return new Balanza();
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Balanza();
        //    }
        //}

        //public Balanza update_config(Balanza obalanza)
        //{
        //    try
        //    {
        //        using (ax_ControlEntities oContext = new ax_ControlEntities())
        //        {

        //            var oConsulta = (from tabla in oContext.Balanzas
        //                             where tabla.id == obalanza.id
        //                             select tabla).SingleOrDefault();
        //            if (oConsulta != null)
        //            {
        //                oConsulta.bitsDatosRx = obalanza.bitsDatosRx;
        //                oConsulta.bitsDatosTx = obalanza.bitsDatosTx;
        //                oConsulta.bitsStopRx = obalanza.bitsStopRx;
        //                oConsulta.bitsStopTx = obalanza.bitsStopTx;
        //                oConsulta.ctrlFlujoRx = obalanza.ctrlFlujoRx;
        //                oConsulta.ctrlFlujoTx = obalanza.ctrlFlujoTx;
        //                oConsulta.descripcion = obalanza.descripcion;
        //                oConsulta.tag = obalanza.tag;
        //                oConsulta.tiempoMuestreo = obalanza.tiempoMuestreo;
        //                oConsulta.timeoutRx = obalanza.timeoutRx;
        //                oConsulta.timeoutTx = obalanza.timeoutTx;
        //                oConsulta.velocidadRx = obalanza.velocidadRx;
        //                oConsulta.velocidadTx = obalanza.velocidadTx;
        //                oConsulta.fkModeloBalanza = obalanza.fkModeloBalanza;
        //                oConsulta.paridadRx = obalanza.paridadRx;
        //                oConsulta.paridadTx = obalanza.paridadTx;
        //                oConsulta.pesoMax = obalanza.pesoMax;
        //                oConsulta.puertoRx = obalanza.puertoRx;
        //                oConsulta.puertoTx = obalanza.puertoTx;
        //                oConsulta.autoconexion = obalanza.autoconexion;
        //                oConsulta.habPesoSistema = obalanza.habPesoSistema;

        //                oContext.SaveChanges();
        //                return obalanza;
        //            }
        //            else
        //            {
        //                return new Balanza();
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Balanza();
        //    }
        //}

        //public bool update_peso(string tag, decimal pesorx, decimal pesotx, bool errorrx, bool errortx, bool estabilidad, bool muestreando)
        //{
        //    try
        //    {
        //        using (ax_ControlEntities oContext = new ax_ControlEntities())
        //        {

        //            var oConsulta = (from tabla in oContext.Balanzas
        //                             where tabla.tag == tag
        //                             select tabla).SingleOrDefault();
        //            if (oConsulta != null)
        //            {
        //                oConsulta.pesoRx = pesorx;
        //                oConsulta.pesoTx = pesotx;
        //                oConsulta.errorRx = errorrx;
        //                oConsulta.errorTx = errortx;
        //                oConsulta.estabilidad = estabilidad;


        //                oContext.SaveChanges();
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public Balanza getOne(int id)
        //{
        //    try
        //    {
        //        using (ax_ControlEntities oContext = new ax_ControlEntities())
        //        {
        //            var oConsulta = oContext.Balanzas.Where(a => a.id == id).Single();

        //            return oConsulta;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Balanza();
        //    }
        //}

        //public Balanza getOne(string tag)
        //{
        //    try
        //    {
        //        using (ax_ControlEntities oContext = new ax_ControlEntities())
        //        {
        //            var oConsulta = oContext.Balanzas.Where(a => a.tag == tag).Single();

        //            return oConsulta;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Balanza();
        //    }
        //}

        //public List<Balanza> getAll()
        //{
        //    try
        //    {
        //        using (ax_ControlEntities oContext = new ax_ControlEntities())
        //        {
        //            var oConsulta = oContext.Balanzas.OrderByDescending(a => a.tag).ToList();

        //            return oConsulta;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new List<Balanza>();
        //    }
        //}
    }
}
