﻿Imports AccesoDatos
Public Class InterfaceN

    Public Function Add(oInterfaceWs As Entidades.INTERFACE_WS) As Entidades.INTERFACE_WS
        Dim db As New InterfaceAD

        Return db.add(oInterfaceWs)

    End Function


    Public Function GetOne(idRegistro As Int64) As Entidades.INTERFACE_WS
        Dim db As New InterfaceAD

        Return db.getOne(idRegistro)

    End Function

    Public Function GetOne(nombreWs As String, estado As Entidades.Constante.ESTADO_REGISTRO, idComando As Integer) As Entidades.INTERFACE_WS
        Dim db As New InterfaceAD

        Return db.getOne(nombreWs, estado, idComando)

    End Function

    Public Function Update(oInterfaceWS As Entidades.INTERFACE_WS) As Entidades.INTERFACE_WS
        Dim db As New InterfaceAD

        Return db.update(oInterfaceWS)

    End Function

    Public Function GetAll(nombreWs As String, estado As Entidades.Constante.ESTADO_REGISTRO) As List(Of Entidades.INTERFACE_WS)
        Dim db As New InterfaceAD

        Return db.getAll(nombreWs, estado)

    End Function

    Public Sub AddEvento(idEvento As Entidades.Constante.ID_EVENTOS, mensaje As String, idRegistro As Int64?)
        Dim db As New InterfaceAD
        db.addEvento(idEvento, mensaje, idRegistro)
    End Sub

    Public Function GetIdComando() As Int64
        Dim db As New InterfaceAD
        Return db.getIdComando()
    End Function


End Class
