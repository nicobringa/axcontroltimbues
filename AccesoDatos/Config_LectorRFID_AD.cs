﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
namespace AccesoDatos
{
    public class Config_LectorRFID_AD
    {
        public CONFIG_LECTOR_RFID getOne(int idConfiguracion)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                return oContext.CONFIG_LECTOR_RFID.Find(idConfiguracion);
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;

            }
        }

        public Boolean update(CONFIG_LECTOR_RFID config_rfid)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.CONFIG_LECTOR_RFID.Find(config_rfid.ID_LECTOR_RFID);

                oConsulta.NOMBRE = config_rfid.NOMBRE;
                oConsulta.SELECT_READER_MODE = config_rfid.SELECT_READER_MODE;
                oConsulta.SELECT_REPORT_MODE = config_rfid.SELECT_REPORT_MODE;
                oConsulta.SELECT_SEARCH = config_rfid.SELECT_SEARCH;
                oConsulta.SELECT_SESSION = config_rfid.SELECT_SESSION;

                oContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

               // AxError.setError(ex);
                return false;

            }
        }

        /// <summary>
        /// Permite devolver todos los lectores rfid  de un puesto de trabajo
        /// </summary>
        /// <param name="idPuestoTrabajo"></param>
        /// <returns></returns>
        public List<CONFIG_LECTOR_RFID> getAll(int idLectorRFID)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = from tabla in oContext.CONFIG_LECTOR_RFID
                                where tabla.ID_LECTOR_RFID == idLectorRFID
                                select tabla;

                return oConsulta.ToList();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;

            }
        }

        /// <summary>
        /// permite devolver todos los lectores RFID
        /// </summary>
        /// <returns></returns>
        public List<LECTOR_RFID> getAll()
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = from tabla in oContext.LECTOR_RFID
                                select tabla;

                return oConsulta.ToList();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;

            }
        }

       
       
    }
}
