﻿Imports Negocio.modDelegado
Imports System.Threading
Public Class frmMapa


#Region "PROPIEDADES"

    Private WithEvents _LectorRFID_Invengo As Negocio.LectorRFID_Invengo
    Dim frm As New frmPresentacion
    Dim listControlesAcceso As New List(Of Controles.CtrlAutomationObjectsBase)
    Dim listMonitoreoBitVidaOld As New List(Of MonitoreoBitVidaOld)
    Dim threadMonitorBitVida As Thread

#End Region


#Region "CONSTRUCTOR"

#End Region

#Region "EVENTOS FORM"
    Public Sub New()
        Try


            frm.Show()

            progress(frm, frm.pbPresentacion)
            ' Esta llamada es exigida por el diseñador.
            InitializeComponent()
            ''Busco todos los abiertos y los agrego al listado
            CtrlMenu1.inicializar()
            progress(frm, frm.pbPresentacion)
            InicializarCtrl()

        Catch ex As Exception
            MessageBox.Show("[CONSTRUCTOR FRM MAPA]" & ex.Message & " " & ex.StackTrace)
        End Try

    End Sub
    'Private Sub frmMapa_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    '    frm.Show()


    'End Sub

    Private Sub CtrlMenu1_ClickMenu() Handles CtrlMenu1.ClickMenu
        FrmMenu.Show()
    End Sub

    Private Sub MostrarControl(ControlAcceso As Controles.CtrlAutomationObjectsBase) Handles CtrlAlarmaBalanzaEgreso.mostrarControl, CtrlAlarmaBalanzaIngreso.mostrarControl,
            ctrlAlarmaIngresoPorteria.mostrarControl, CtrlAlarmaPorteriaEgreso.mostrarControl

        PanelControlAcceso.Controls.Clear()
        PanelControlAcceso.Controls.Add(ControlAcceso)
    End Sub
#End Region


#Region "METODOS"

    Public Sub InicializarCtrl()

        'Obtengo los controles de acceso asignado al puesto de trabajo
        Dim listControlAcceso As List(Of Entidades.Control_Acceso) = Negocio.ModSesion.PUESTO_TRABAJO.control_acceso.ToList()
        'Recorro los controles de acceso
        For Each ControlAcceso As Entidades.Control_Acceso In listControlAcceso

            Select Case ControlAcceso.idControlAcceso
                Case 10
                    Me.ctrlAlarmaIngresoPorteria.InicializarCtrl(GetCtrlControlAcceso(ControlAcceso))
                Case 20
                    Me.CtrlAlarmaBalanzaIngreso.InicializarCtrl(GetCtrlControlAcceso(ControlAcceso))
                Case 30
                    Me.CtrlAlarmaBalanzaEgreso.InicializarCtrl(GetCtrlControlAcceso(ControlAcceso))
                Case 40
                    Me.CtrlAlarmaPorteriaEgreso.InicializarCtrl(GetCtrlControlAcceso(ControlAcceso))

            End Select
            progress(frm, frm.pbPresentacion)
            'Creo un tab con el controle de acceso
            'NuevoTabPage(ControlAcceso)

        Next

        Me.CtrlAlarmero1.InicializarCtrl(0)

        frm.Close()

        'threadMonitorBitVida = New Thread(AddressOf monitoreoBitVida)
        'threadMonitorBitVida.Name = "[Monitoreo BitVida]"
        'threadMonitorBitVida.IsBackground = True
        'threadMonitorBitVida.Start()
    End Sub



    ''' <summary>
    ''' Permite obtener el control de acceso correspondiente al tipo de control de acceso
    ''' </summary>
    ''' <returns></returns>
    Private Function GetCtrlControlAcceso(ControlAcceso As Entidades.Control_Acceso) As Controles.CtrlAutomationObjectsBase

        Dim ctrlControlAcceso As Controles.CtrlAutomationObjectsBase = Nothing
        Select Case ControlAcceso.tipoControlAcceso


            Case Else
                ctrlControlAcceso = Nothing
        End Select

        listControlesAcceso.Add(ctrlControlAcceso)
        listMonitoreoBitVidaOld.Add(New MonitoreoBitVidaOld(ctrlControlAcceso.idControlAcceso))
        Return ctrlControlAcceso
    End Function

    Private Sub frmMapa_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If MessageBox.Show("¿Desea cerrar la aplicación?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.CtrlAlarmero1.cerrar()
            Application.ExitThread() ' Cierra la aplicacion y todos procesos
        End If
    End Sub

    Private Sub ctrlAlarmaIngresoPorteria_Load(sender As Object, e As EventArgs) Handles ctrlAlarmaIngresoPorteria.Load

    End Sub


#End Region

#Region "SUBPROCESOS"


    Private Sub monitoreoBitVida()

        Threading.Thread.Sleep(5000)
        While True

            For Each itemControlAcceso As Controles.CtrlAutomationObjectsBase In listControlesAcceso
                Dim monitoerobitVidaOld As MonitoreoBitVidaOld = listMonitoreoBitVidaOld.Where(Function(o) o.idControlAcceso = itemControlAcceso.idControlAcceso).SingleOrDefault()

                If monitoerobitVidaOld.bitVidaOld = itemControlAcceso.BitMonitoreo Then

                    If monitoerobitVidaOld.intentos = 3 Then
                        monitoerobitVidaOld.intentos = 0
                        Console.WriteLine("Inicializar control idControlAcceso =  " & itemControlAcceso.idControlAcceso)
                        itemControlAcceso.Inicializar()
                    Else
                        monitoerobitVidaOld.intentos += 1
                    End If
                Else

                    monitoerobitVidaOld.bitVidaOld = itemControlAcceso.BitMonitoreo

                End If

            Next

            Threading.Thread.Sleep(2000)
        End While

    End Sub

    Private Sub frmMapa_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

#End Region

End Class


Public Class MonitoreoBitVidaOld

    Public Sub New(idControlAcceso As Int16)
        Me.idControlAcceso = idControlAcceso
    End Sub

    Private _idControlAcceso As Int16
    Public Property idControlAcceso() As Int16
        Get
            Return _idControlAcceso
        End Get
        Set(ByVal value As Int16)
            _idControlAcceso = value
        End Set
    End Property

    Private _BitVidaOld As Integer



    Public Property bitVidaOld() As Integer
        Get
            Return _BitVidaOld
        End Get
        Set(ByVal value As Integer)
            _BitVidaOld = value
        End Set
    End Property

    Private _intentos As Int16
    Public Property intentos() As Int16
        Get
            Return _intentos
        End Get
        Set(ByVal value As Int16)
            _intentos = value
        End Set
    End Property

End Class