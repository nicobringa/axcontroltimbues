﻿Public Class Ult_Tag_LeidoN


    Private oBD As New AccesoDatos.Ult_Tag_LeidoAD

    Public Sub New()
        oBD = New AccesoDatos.Ult_Tag_LeidoAD()
    End Sub
    ''' <summary>
    ''' Permite agregar un tag RFID
    ''' </summary>
    ''' <param name="tagRFID"></param>
    ''' <param name="idSector"></param>
    ''' <returns></returns>
    Private Function add(tagRFID As String, idSector As Integer) As Entidades.ULTIMO_TAG_LEIDO
        Dim oUltTagRFID As New Entidades.ULTIMO_TAG_LEIDO
        oUltTagRFID.FECHA = DateTime.Now
        oUltTagRFID.ID_SECTOR = idSector
        oUltTagRFID.TAG_RFID = tagRFID

        Return oBD.add(oUltTagRFID)


    End Function

    Public Function Delete(idSector As Integer) As Boolean
        Return oBD.delete(idSector)
    End Function

    ''' <summary>
    ''' Permite obtener el ultimo tag leido de un sector
    ''' </summary>
    ''' <param name="idSector"></param>
    ''' <returns></returns>
    Public Function GetLastTag(idSector As Integer)
        Return oBD.getLastTag(idSector)
    End Function

    Public Function ultimoTag() As String
        Return oBD.ultimoTag
    End Function

    Public Function Guardar(tagRFID As String, idSector As Integer) As Entidades.ULTIMO_TAG_LEIDO

        Dim oUltTagRFID As New Entidades.ULTIMO_TAG_LEIDO
        oUltTagRFID.fecha = DateTime.Now
        oUltTagRFID.ID_SECTOR = idSector
        oUltTagRFID.TAG_RFID = tagRFID
        'Primero elimino los ultimos tag leido de un sector
        oBD.delete(idSector)
        'Luego agrego el UltTagLeido
        Return oBD.add(oUltTagRFID)

    End Function

End Class
