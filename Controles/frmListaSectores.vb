﻿Imports Negocio
Public Class frmListaSectores


#Region "PROPIEDADES"
    Private oSector As New List(Of Entidades.Sector)
    Private oSectorN As New Negocio.SectorN
    Private oControl As New Entidades.Control_Acceso
    Private oControlN As New Negocio.ControlAccesoN
    Private oConfigN As New Negocio.Configuracion_LectorRfidN
    Private idAntena As Integer
    Private _Id As Integer
    Private nombreSector As String
    Private AsignarAntena As Boolean
    Private ID_CONTROL_ACCESO As Int32
#End Region


#Region "CONSTRUCTOR"
    Public Sub New(ByVal idLector As Integer, ByVal Id As Integer, AsignarAntena As Boolean)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        idAntena = Id

        Dim oLector As New Entidades.LECTOR_RFID
        Dim nLector As New Negocio.LectorRFID_N

        oLector = nLector.GetOne(idLector)

        ID_CONTROL_ACCESO = oLector.ID_CONTROL_ACCESO

        _Id = idLector
        Me.AsignarAntena = AsignarAntena
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        RefrescarFrilla()
    End Sub

#End Region




#Region "METODOS"

    ''' <summary>
    ''' Carga la grilla con todos los caontroles de accesos y sus respectivos sectores
    ''' </summary>

    Private Sub RefrescarFrilla()
        dgSector.Rows.Clear()

        Dim nSector As New Negocio.SectorN
        Dim listSectores As List(Of Entidades.SECTOR) = nSector.GetAllSectorControl(ID_CONTROL_ACCESO)
        If IsNothing(listSectores) Then Return

        Dim nombreControl As String
        For Each oAu As Entidades.SECTOR In listSectores
            oControl = oControlN.GetOne(oAu.ID_CONTROL_ACCESO)
            nombreControl = oControl.NOMBRE
            dgSector.Rows.Add(nombreControl, oAu.ID_SECTOR, oAu.NOMBRE)
        Next

    End Sub


    Private Sub dgSector_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgSector.CellDoubleClick


        Dim sectorElegido As Integer = 0

        sectorElegido = Me.dgSector.Item(1, dgSector.CurrentRow.Index).Value
        nombreSector = Me.dgSector.Item(2, dgSector.CurrentRow.Index).Value

        Dim nAntena As New Negocio.Antena_RfidN
        Dim nLector As New Negocio.LectorRFID_N
        Dim oLector As Entidades.LECTOR_RFID = nLector.GetOne(_Id)

        If AsignarAntena Then
            ' AsignarSector(sectorElegido, idAntena, _IdLector)
            Dim oAntena As Entidades.ANTENAS_RFID = nAntena.GetOne(idAntena, oLector.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)
            If Not IsNothing(oAntena) Then
                oAntena.ID_SECTOR = sectorElegido
                nAntena.Update(oAntena, oLector.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)
            Else
                MessageBox.Show(String.Format("El lector no tiene habilitado la antena {0} , habilitar la antena antes de asignar un sector",
                                              idAntena), "Seleccionar Sector", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Else 'Asignar puerto
            Dim nPuerto As New Negocio.Puerto_RfidN
            Dim oPuerto As Entidades.PUERTO_RFID = nPuerto.GetOne(idAntena, oLector.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)

            If IsNothing(oPuerto) Then
                oPuerto = New Entidades.PUERTO_RFID()
                oPuerto.NUM_PUERTO = idAntena
                oPuerto.ID_CONF_LECTOR_RFID = oLector.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID
                oPuerto.ID_SECTOR = sectorElegido
                oPuerto.TAG = InputBox("Ingrese el nombre del control (sensor o barrera) que va a trabajar con el puerto", "Nombre del control")
                oPuerto = nPuerto.Add(oPuerto)
                Me.Close()
                Return
            End If

            oPuerto.TAG = InputBox("Ingrese el nombre del control (sensor o barrera) que va a trabajar con el puerto", "Nombre del control")
            oPuerto.ID_SECTOR = sectorElegido
            nPuerto.Update(oPuerto)


        End If


        Me.Close()
    End Sub




#End Region

#Region "SUBPROCESOS"

#End Region
End Class