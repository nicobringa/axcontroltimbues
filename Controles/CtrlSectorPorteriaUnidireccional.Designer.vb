﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorPorteriaUnidireccional
    Inherits UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.S_BARRERA = New Controles.CtrlSensor()
        Me.S_LECTURA_RFID = New Controles.CtrlSensor()
        Me.BARRERA = New Controles.CtrlBarrera()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlAtenaLeyendo = New Controles.CtrlAntenaLeyendo()
        Me.btnLecturaManual = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'S_BARRERA
        '
        Me.S_BARRERA.BitMonitoreo = CType(0, Short)
        Me.S_BARRERA.Estado = False
        Me.S_BARRERA.GPIO = False
        Me.S_BARRERA.ID_CONTROL_ACCESO = 0
        Me.S_BARRERA.ID_PLC = 0
        Me.S_BARRERA.ID_SECTOR = 0
        Me.S_BARRERA.Inicializado = False
        Me.S_BARRERA.Location = New System.Drawing.Point(68, 200)
        Me.S_BARRERA.Name = "S_BARRERA"
        Me.S_BARRERA.patenteLeida = Nothing
        Me.S_BARRERA.PLC = Nothing
        Me.S_BARRERA.Size = New System.Drawing.Size(81, 45)
        Me.S_BARRERA.TabIndex = 163
        Me.S_BARRERA.tiempoPatenteLeidas = 0
        '
        'S_LECTURA_RFID
        '
        Me.S_LECTURA_RFID.BitMonitoreo = CType(0, Short)
        Me.S_LECTURA_RFID.Estado = False
        Me.S_LECTURA_RFID.GPIO = False
        Me.S_LECTURA_RFID.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA_RFID.ID_PLC = 0
        Me.S_LECTURA_RFID.ID_SECTOR = 0
        Me.S_LECTURA_RFID.Inicializado = False
        Me.S_LECTURA_RFID.Location = New System.Drawing.Point(155, 200)
        Me.S_LECTURA_RFID.Name = "S_LECTURA_RFID"
        Me.S_LECTURA_RFID.patenteLeida = Nothing
        Me.S_LECTURA_RFID.PLC = Nothing
        Me.S_LECTURA_RFID.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA_RFID.TabIndex = 162
        Me.S_LECTURA_RFID.tiempoPatenteLeidas = 0
        '
        'BARRERA
        '
        Me.BARRERA.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.BARRERA.BitMonitoreo = CType(0, Short)
        Me.BARRERA.Estado = False
        Me.BARRERA.GPIO = False
        Me.BARRERA.ID_CONTROL_ACCESO = 0
        Me.BARRERA.ID_PLC = 0
        Me.BARRERA.ID_SECTOR = 0
        Me.BARRERA.Inicializado = False
        Me.BARRERA.Location = New System.Drawing.Point(0, 200)
        Me.BARRERA.Name = "BARRERA"
        Me.BARRERA.patenteLeida = Nothing
        Me.BARRERA.PLC = Nothing
        Me.BARRERA.Size = New System.Drawing.Size(62, 52)
        Me.BARRERA.TabIndex = 161
        Me.BARRERA.tiempoPatenteLeidas = 0
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(242, 43)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 212)
        Me.CtrlPanelNotificaciones1.TabIndex = 167
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(-1, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(450, 43)
        Me.lblNombre.TabIndex = 168
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlAtenaLeyendo
        '
        Me.CtrlAtenaLeyendo.Location = New System.Drawing.Point(3, 46)
        Me.CtrlAtenaLeyendo.Name = "CtrlAtenaLeyendo"
        Me.CtrlAtenaLeyendo.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendo.TabIndex = 169
        Me.CtrlAtenaLeyendo.UltTag = Nothing
        '
        'btnLecturaManual
        '
        Me.btnLecturaManual.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual.Location = New System.Drawing.Point(187, 46)
        Me.btnLecturaManual.Name = "btnLecturaManual"
        Me.btnLecturaManual.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual.TabIndex = 170
        Me.btnLecturaManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual.UseVisualStyleBackColor = False
        '
        'CtrlSectorPorteria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnLecturaManual)
        Me.Controls.Add(Me.CtrlAtenaLeyendo)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.S_BARRERA)
        Me.Controls.Add(Me.S_LECTURA_RFID)
        Me.Controls.Add(Me.BARRERA)
        Me.Name = "CtrlSectorPorteria"
        Me.Size = New System.Drawing.Size(449, 255)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents S_BARRERA As CtrlSensor
    Friend WithEvents S_LECTURA_RFID As CtrlSensor
    Public WithEvents BARRERA As CtrlBarrera
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlAtenaLeyendo As CtrlAntenaLeyendo
    Friend WithEvents btnLecturaManual As Button
End Class
