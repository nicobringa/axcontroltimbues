﻿
Imports Controles.CtrlNotificacion
Imports Negocio

Public Class CtrlPanelNotificaciones



    Public Sub MostrarNotificacion(ByVal ID_CONTROL_ACCESO As Integer,
                                   ByVal TipoNotificacion As Entidades.Constante.TipoNotificacion, ByVal Mensaje As String,
                                   Optional ByVal ID_SECTOR As Integer = 0, Optional ByVal tag As String = "")
        Dim ctrlNotidicacion As New CtrlNotificacion(TipoNotificacion, DateTime.Now, Mensaje)
        modDelegado.AddCtrlFLP(Me, ctrlNotidicacion, flpNotificacion)

        'Elimino el la primera notificacion agregada para dejar siempre las ultimas 10 notificaciones
        If flpNotificacion.Controls.Count > 10 Then modDelegado.RemoveAtCtrlFLP(Me, 1, flpNotificacion)
        'Agrego el evento para que pueda cerrar la notificacion
        AddHandler ctrlNotidicacion.ClickCerrarNotificacion, AddressOf ClickCerrarNotificacion


        'Agrego la notficacion en la base de datos para las maquinas clientes
        Dim nNoti As New Negocio.NotificacionN
        nNoti.Add(TipoNotificacion, Mensaje, ID_CONTROL_ACCESO, ID_SECTOR, tag)

        Dim nNotificaciones As New Negocio.NotificacionN
        nNotificaciones.BorrarNotificaciones()
    End Sub

    Public Sub MostrarNotificacionFoto(NumBalanza As Integer, Image As Bitmap)
        Dim ctrlNotiFoto As New Controles.CtrlNotificacionFoto(NumBalanza, Image)
        AddHandler ctrlNotiFoto.ClickCerrarNotificacion, AddressOf ClickCerrarNotificacion
        modDelegado.AddCtrlFLP(Me, ctrlNotiFoto, flpNotificacion)
    End Sub

    Private Sub ClickCerrarNotificacion(ByVal ctrl As Control)
        modDelegado.RemoveCtrlFLP(Me, ctrl, flpNotificacion)
    End Sub


    Private Sub btnBorrarNotificaciones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrarNotificaciones.Click
        modDelegado.ClearCtrlFLP(Me, flpNotificacion)
    End Sub
End Class
