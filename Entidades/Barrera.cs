﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Barrera
    {
        string _Descripcion;
        public string Descripcion
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
            }
        }

        DATO_WORDINT _Estado;
        public DATO_WORDINT Estado
        {
            get
            {
                return _Estado;
            }
            set
            {
                _Estado = value;
            }
        }

        DATO_WORDINT _Comando;
        public DATO_WORDINT Comando
        {
            get
            {
                return _Comando;
            }
            set
            {
                _Comando = value;
            }
        }

        DATO_WORDINT _HabSenPosicion;
        public DATO_WORDINT HabSenPosicion
        {
            get
            {
                return _HabSenPosicion;
            }
            set
            {
                _HabSenPosicion = value;
            }
        }
    }
}
