﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlCabezalBalanza
    Inherits CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbTAG = New System.Windows.Forms.Label()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.lb1 = New System.Windows.Forms.Label()
        Me.btnConfig = New System.Windows.Forms.Button()
        Me.EP = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.CtrlProgressBalanza1 = New Controles.CtrlProgressBalanza()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblEstadoOld = New System.Windows.Forms.Label()
        CType(Me.EP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbTAG
        '
        Me.lbTAG.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbTAG.BackColor = System.Drawing.Color.MediumBlue
        Me.lbTAG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTAG.ForeColor = System.Drawing.Color.White
        Me.lbTAG.Location = New System.Drawing.Point(0, 0)
        Me.lbTAG.Name = "lbTAG"
        Me.lbTAG.Size = New System.Drawing.Size(290, 20)
        Me.lbTAG.TabIndex = 87
        Me.lbTAG.Text = "TAG"
        Me.lbTAG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtPeso
        '
        Me.txtPeso.BackColor = System.Drawing.Color.Black
        Me.txtPeso.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPeso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeso.ForeColor = System.Drawing.Color.LimeGreen
        Me.txtPeso.Location = New System.Drawing.Point(41, 26)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.ReadOnly = True
        Me.txtPeso.Size = New System.Drawing.Size(57, 13)
        Me.txtPeso.TabIndex = 88
        Me.txtPeso.Text = "12.000,00"
        Me.txtPeso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lb1
        '
        Me.lb1.BackColor = System.Drawing.SystemColors.ControlText
        Me.lb1.ForeColor = System.Drawing.Color.LimeGreen
        Me.lb1.Location = New System.Drawing.Point(38, 22)
        Me.lb1.Name = "lb1"
        Me.lb1.Size = New System.Drawing.Size(122, 21)
        Me.lb1.TabIndex = 89
        Me.lb1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnConfig
        '
        Me.btnConfig.BackColor = System.Drawing.Color.Transparent
        Me.btnConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConfig.Image = Global.Controles.My.Resources.Resources.Configuration19
        Me.btnConfig.Location = New System.Drawing.Point(0, 21)
        Me.btnConfig.Name = "btnConfig"
        Me.btnConfig.Size = New System.Drawing.Size(32, 20)
        Me.btnConfig.TabIndex = 85
        Me.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnConfig.UseVisualStyleBackColor = False
        '
        'EP
        '
        Me.EP.ContainerControl = Me
        '
        'CtrlProgressBalanza1
        '
        Me.CtrlProgressBalanza1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CtrlProgressBalanza1.ColorBarra = System.Drawing.Color.Empty
        Me.CtrlProgressBalanza1.Location = New System.Drawing.Point(166, 23)
        Me.CtrlProgressBalanza1.Name = "CtrlProgressBalanza1"
        Me.CtrlProgressBalanza1.Size = New System.Drawing.Size(125, 20)
        Me.CtrlProgressBalanza1.TabIndex = 94
        Me.CtrlProgressBalanza1.Value = CType(0, Short)
        '
        'lblEstado
        '
        Me.lblEstado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEstado.BackColor = System.Drawing.Color.Green
        Me.lblEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.ForeColor = System.Drawing.Color.White
        Me.lblEstado.Location = New System.Drawing.Point(0, 43)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(290, 34)
        Me.lblEstado.TabIndex = 95
        Me.lblEstado.Text = "TAG"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEstadoOld
        '
        Me.lblEstadoOld.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEstadoOld.BackColor = System.Drawing.Color.Black
        Me.lblEstadoOld.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoOld.ForeColor = System.Drawing.Color.White
        Me.lblEstadoOld.Location = New System.Drawing.Point(0, 77)
        Me.lblEstadoOld.Name = "lblEstadoOld"
        Me.lblEstadoOld.Size = New System.Drawing.Size(291, 19)
        Me.lblEstadoOld.TabIndex = 96
        Me.lblEstadoOld.Text = "TAG"
        Me.lblEstadoOld.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlCabezalBalanza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.Controls.Add(Me.lblEstadoOld)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.CtrlProgressBalanza1)
        Me.Controls.Add(Me.txtPeso)
        Me.Controls.Add(Me.lbTAG)
        Me.Controls.Add(Me.btnConfig)
        Me.Controls.Add(Me.lb1)
        Me.Name = "CtrlCabezalBalanza"
        Me.Size = New System.Drawing.Size(290, 97)
        CType(Me.EP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnConfig As System.Windows.Forms.Button
    Friend WithEvents lbTAG As System.Windows.Forms.Label
    Friend WithEvents txtPeso As System.Windows.Forms.TextBox
    Friend WithEvents lb1 As System.Windows.Forms.Label
    Friend WithEvents EP As System.Windows.Forms.ErrorProvider
    Friend WithEvents CtrlProgressBalanza1 As CtrlProgressBalanza
    Friend WithEvents lblEstado As Label
    Friend WithEvents lblEstadoOld As Label
End Class
