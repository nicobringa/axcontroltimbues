﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
   public  class TagLeidoManual
    {
        //(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.Lector_RFID, ByVal manual As Boolean
        public string tagRFID { get; set; }
        public Int16 numAntena { get; set; }
        public Entidades.LECTOR_RFID lectorRFID { get; set; }

        public TagLeidoManual(string tagRFID, Int16 numAntena,  LECTOR_RFID lectorRFID)
        {
            this.numAntena = numAntena;
            this.tagRFID = tagRFID;
            this.lectorRFID = lectorRFID;

        }

        


    }
}
