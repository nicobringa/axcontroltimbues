﻿
Imports Entidades.Constante
Imports Negocio
'Bar1Abajo 41; 264
'Bar1Arriba 197; 229
'Bar2Abajo 284; 58
'Bar2Arriba 284; 3

's1_lectura_verde 223; 488
's1_lectura_rojo 220; 486

'S1_BARRERA_VERDE 244; 401
's1_barrera_rojo 247; 401

'S1_POSICION_VERDE 258; 300
's1_POSICION_rojo

's2_lectura_verde 302; 68
's2_lectura_rojo 302; 66

'S2_BARRERA_VERDE 300; 81
's2_barrera_rojo 300; 81

'S2_POSICION_VERDE 295; 105
's2_POSICION_rojo 296; 106


Public Class CtrlDinamismoBalanza2

    Public Sub SetBarreraEstado(ByVal NumBarrera As Integer, ByVal _BarreraEstado As BarreraEstado)

        Dim picBarreraArriba As PictureBox = Nothing
        Dim picBarreraAbajo As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing



        If NumBarrera = 1 Then
            picBarreraArriba = pic_b1_arriba
            picBarreraAbajo = pic_b1_abajo
            lblFalla = lblFalla_BAR1

        ElseIf NumBarrera = 2 Then
            picBarreraArriba = pic_b2_arriba
            picBarreraAbajo = pic_b2_abajo
            lblFalla = lblFalla_BAR2

        End If


        If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then
            picVisible = picBarreraArriba
            picNoVisible = picBarreraAbajo
            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
            picVisible = picBarreraAbajo
            picNoVisible = picBarreraArriba
            modDelegado.setVisibleCtrl(Me, lblFalla, False)

        ElseIf _BarreraEstado = BarreraEstado.Falla Then
            picNoVisible = Nothing
            picVisible = Nothing
            modDelegado.setVisibleCtrl(Me, lblFalla, True)

        ElseIf _BarreraEstado = BarreraEstado.Deshabilitado Then
            picVisible = picBarreraAbajo
            picNoVisible = picBarreraArriba
            modDelegado.setVisibleCtrl(Me, lblFalla, False)
        End If


        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If


    End Sub

    Public Sub SetSensorEstado(TipoSensor As Entidades.Constante.tipoSensor, ByVal NumSensor As Integer, ByVal _SensorEstado As Integer)
        Dim picCortando As PictureBox = Nothing
        Dim picNoCortando As PictureBox = Nothing

        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing
        Dim lblFalla As Label = Nothing


        ''Sensor de lectura
        If TipoSensor = tipoSensor.Lectura Then

            If NumSensor = 1 Then
                picCortando = pic_s1_lectura_rojo
                picNoCortando = pic_s1_lectura_verde

            ElseIf NumSensor = 2 Then
                picCortando = pic_s2_lectura_rojo
                picNoCortando = pic_s2_lectura_verde

            End If
            ''Sensor de barrera
        ElseIf TipoSensor = tipoSensor.Barrera Then
            If NumSensor = 1 Then
                picCortando = pic_s1_barrera_rojo
                picNoCortando = pic_s1_barrera_verde

            ElseIf NumSensor = 2 Then
                picCortando = pic_s2_barrera_rojo
                picNoCortando = pic_s2_barrera_verde

            End If
        ElseIf TipoSensor = tipoSensor.Posicion Then

            If NumSensor = 1 Then
                picCortando = pic_s1_posicion_rojo
                picNoCortando = pic_s1_posicion_verde

            ElseIf NumSensor = 2 Then
                picCortando = pic_s2_posicion_rojo
                picNoCortando = pic_s2_posicion_verde

            End If

        End If





        If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
            picVisible = picNoCortando
            picNoVisible = picCortando

        ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
            picVisible = picCortando
            picNoVisible = picNoCortando
        ElseIf _SensorEstado = SensorInfrarojoEstado.Deshabilitado Then
            picVisible = Nothing
            picNoVisible = Nothing
        End If

        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)

        Else
            'Oculto las dos barreras
            picVisible = picCortando
            picNoVisible = picNoCortando

            modDelegado.setVisiblePic(Me, False, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)

        End If


    End Sub



End Class
