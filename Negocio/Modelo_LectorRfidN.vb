﻿Imports System.Windows.Forms
Public Class Modelo_LectorRfidN
    Public Function GetCantAnetnas(idModeloLector As Integer) As Integer
        Dim oBD As New AccesoDatos.Modelo_LectorRfidAD
        Return oBD.getCantAntenas(idModeloLector)
    End Function


    Public Sub CargarCombo(ByVal combo As ComboBox)
        combo.DataSource = Me.getAll()
        combo.DisplayMember = "nombre"
        combo.ValueMember = "id_Modelo_Lector_rfid"
    End Sub

    Public Function getAll() As List(Of Entidades.MODELO_LECTOR_RFID)
        Dim oModeloLectorAD As New AccesoDatos.Modelo_LectorRfidAD
        Return oModeloLectorAD.getAll()
    End Function
End Class
