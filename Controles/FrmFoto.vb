﻿Public Class FrmFoto
    Public Sub New(ByVal loadedBitmap As Bitmap)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        pbFoto.Image = loadedBitmap
    End Sub
End Class