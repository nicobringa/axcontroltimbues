﻿Public Class T20CtrlControlInterface
    Private _idControlAcceso As Integer = 0
    Public Property ID_CONTROL_ACCESO() As Integer
        Get
            Return _idControlAcceso
        End Get
        Set(ByVal value As Integer)
            _idControlAcceso = value
        End Set
    End Property

    Private _ID_PLC As Integer
    Public Property ID_PLC() As Integer
        Get
            Return _ID_PLC
        End Get
        Set(ByVal value As Integer)
            _ID_PLC = value
        End Set
    End Property

    Private ControlAcceso As Entidades.CONTROL_ACCESO
    Private ListaCtrlSectorInterface As List(Of CtrlSectorInterfaceSilo)
    Public Sub New(ID_CONTROL_ACCESO As Integer, ID_PLC As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_PLC = ID_PLC


    End Sub

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()



    End Sub


    Public Overrides Sub Inicializar()


        If Me.ID_CONTROL_ACCESO = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID un control de acceso")

            Return
        End If

        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)

        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If
        Dim nombreControlAcceso As String = String.Format("{0} - ({1})", Me.ControlAcceso.NOMBRE, Me.ControlAcceso.ID_CONTROL_ACCESO)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreControlAcceso)

        inicializarCtrl()
    End Sub


    Private Sub inicializarCtrl()
        'Recorro los sectores
        flpSectores.Controls.Clear()
        ListaCtrlSectorInterface = New List(Of CtrlSectorInterfaceSilo)
        Dim ctrlSectorInterface As CtrlSectorInterfaceSilo
        Dim ctrlSectorInterfaceDesc As CtrlSectorInterfaceDescarga
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR

            Select Case itemSector.ID_SECTOR
                Case 2100 ' Sector interfaz silo
                    ctrlSectorInterface = New CtrlSectorInterfaceSilo(itemSector)
                    flpSectores.Controls.Add(ctrlSectorInterface)
                   ' ListaCtrlSectorInterface.Add(ctrlSectorInterface)
                Case 2200 ' Sector Interfaz Descarga
                    'ctrlSectorInterfaceDesc = New CtrlSectorInterfaceDescarga(itemSector, Entidades.Constante.CANT_DB_PLC_INTERFACE.DESCARGA, Entidades.Constante.START_DB_PLC_INTERFACE.DESCARGA)
                    'flpSectores.Controls.Add(ctrlSectorInterfaceDesc)
                    ' ListaCtrlSectorInterface.Add(ctrlSectorInterfaceDesc)
                Case Else

            End Select

            ' AddHandler ctrlSectorPorteria.EventTagLeidoManual, AddressOf Me.TagLeidoManual
        Next
    End Sub

End Class
