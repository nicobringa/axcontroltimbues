﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Impinj.OctaneSdk
Public Class CtrlControlAccesoBalanza

#Region "PROPIEDADES"




    Private _RFID As Boolean
    Public Property RFID() As Boolean
        Get
            Return _RFID
        End Get
        Set(ByVal value As Boolean)
            _RFID = value
        End Set
    End Property

    Private ControlAcceso As Entidades.Control_Acceso
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
    Private hMonitoreo As Thread
    Private WithEvents BufferBal1Bar1 As Entidades.BufferTag
    Private WithEvents BufferBal1Bar2 As Entidades.BufferTag
    Private WithEvents BufferBal2Bar1 As Entidades.BufferTag
    Private WithEvents BufferBal2Bar2 As Entidades.BufferTag
    Private oLector As New Entidades.Lector_RFID
    Private nLector As New Negocio.LectorRFID_N


#End Region


#Region "CONSTRUCTOR"

    Public Sub New(ID_CONTROL_ACCESO As Integer, ID_PLC As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_PLC = ID_PLC

    End Sub


    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()



    End Sub
#End Region

#Region "EVENTOS FORM"
    Private Sub btnLectorRfid_Click(sender As Object, e As EventArgs) Handles btnLectorRfid.Click

        Dim frm As New FrmConfLectoresRFID(Me.ListRFID_SpeedWay)
        frm.ShowDialog()


    End Sub

#End Region


#Region "METODOS"
    Public Overrides Sub Inicializar()

        If Me.ID_CONTROL_ACCESO = 0 Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
              "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID de puesto de trabajo")

            Return
        End If

        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)

        If IsNothing(Me.ControlAcceso) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el control de acceso asignado")
            Return

        End If

        Negocio.modDelegado.SetTextLabel(Me, lblNombre, Me.ControlAcceso.NOMBRE)
        'Si el puesto de trabajo es servidor
        If ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            'Inicializo como servidor
            IniServidor()
        Else
            'De los contrario como cliente
            IniCliente()
        End If

        'Ejecuto el hilo de monitoreo
        If Not IsNothing(hMonitoreo) Then
            If hMonitoreo.IsAlive Then hMonitoreo.Abort()
            hMonitoreo = Nothing
        End If

        inicializarCtrl()
        SetBuffer()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()


    End Sub

    Public Sub InicializarWS()
        Dim nControlAcceso As New Negocio.ControlAccesoN
        Me.ControlAcceso = nControlAcceso.GetOne(Me.ID_CONTROL_ACCESO)
        inicializarCtrl()
    End Sub

    Private Sub inicializarCtrl()
        'Recorro los sectores
        For Each itemSector As Entidades.SECTOR In Me.ControlAcceso.SECTOR
            'De cada sector traigo los TAG del plc que tiene el control de acceso
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(itemSector.ID_SECTOR)

            For Each itemDato As Entidades.DATO_WORDINT In listDatoWordInt

                Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

                'Obtengo el nombre del tag quitandole la propiedad
                itemDato.TAG = Constante.getNombreTag(itemDato.TAG)
                'Si el tag contiene el nombre del control
                If itemDato.TAG.Equals(BAL1_BAR1.Name) Then

                    ctrlAutomatizacion = Me.BAL1_BAR1

                ElseIf itemDato.TAG.Equals(BAL1_BAR2.Name) Then

                    ctrlAutomatizacion = Me.BAL1_BAR2

                ElseIf itemDato.TAG.Equals(BAL2_BAR1.Name) Then
                    ctrlAutomatizacion = Me.BAL2_BAR1

                ElseIf itemDato.TAG.Equals(BAL2_BAR2.Name) Then
                    ctrlAutomatizacion = Me.BAL2_BAR2

                ElseIf itemDato.TAG.Equals(BAL1_S1.Name) Then
                    ctrlAutomatizacion = Me.BAL1_S1

                ElseIf itemDato.TAG.Equals(BAL1_S2.Name) Then
                    ctrlAutomatizacion = Me.BAL1_S2

                ElseIf itemDato.TAG.Equals(BAL2_S1.Name) Then
                    ctrlAutomatizacion = Me.BAL2_S1

                ElseIf itemDato.TAG.Equals(BAL2_S2.Name) Then
                    ctrlAutomatizacion = Me.BAL2_S2

                ElseIf itemDato.TAG.Equals(BALANZA1.Name) Then
                    ctrlAutomatizacion = Me.BALANZA1

                ElseIf itemDato.TAG.Equals(BALANZA2.Name) Then
                    ctrlAutomatizacion = Me.BALANZA2

                ElseIf itemDato.TAG.Equals(S_LECTURA_BAL1_BAR1.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_BAL1_BAR1

                ElseIf itemDato.TAG.Equals(S_LECTURA_BAL1_BAR2.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_BAL1_BAR2

                ElseIf itemDato.TAG.Equals(S_LECTURA_BAL2_BAR1.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_BAL2_BAR1

                ElseIf itemDato.TAG.Equals(S_LECTURA_BAL2_BAR2.Name) Then
                    ctrlAutomatizacion = Me.S_LECTURA_BAL2_BAR2

                Else
                    ctrlAutomatizacion = Nothing
                End If

                If Not IsNothing(ctrlAutomatizacion) Then
                    'Si todavia no esta inicializado
                    If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(Me.ControlAcceso.ID_PLC, Me.ControlAcceso.ID_CONTROL_ACCESO, itemSector.ID_SECTOR) ' BARRERA DE INGRESO
                End If
            Next

        Next

        CartelLED_BALANZA1.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CartelLED_BALANZA2.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CAM_BAL1.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)
        CAM_BAL2.Inicializar(Me.ControlAcceso.ID_CONTROL_ACCESO)

    End Sub

    Public Sub IniServidor()
        'Cargo los lectores RFID
        CargarLectoresRFID()

    End Sub

    Public Sub IniCliente()
        CargarLectorRFIDCliente()
    End Sub

    Private Sub CargarLectorRFIDCliente()

        Dim oLectores As New List(Of Entidades.LECTOR_RFID)
        oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
        CtrlEstadoLectoresRFID1.CargarLectoresCliente(oLectores)
    End Sub

    Private Sub CargarLectoresRFID()
        'Busco los lectores
        Dim nLectorEFID As New Negocio.LectorRFID_N

        Dim listLectorRFID As List(Of Entidades.LECTOR_RFID) = nLectorEFID.GetAll(Me.ID_CONTROL_ACCESO)
        'listLectorRFID.Clear()

        If IsNothing(listLectorRFID) Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN DE PROCEDIMIENTO
        End If

        If listLectorRFID.Count = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
             "[ " + DateTime.Now + " ] INFO: " + "No tiene lectores RFID asginados")

            Return 'FIN 
        End If

        'Recorro los lectores agregados para el puesto de trabajo
        For Each LectorRFID As Entidades.LECTOR_RFID In listLectorRFID
            'Creo el lector RFID SpeedWay para su utilización
            Dim RFID_SpeedWay As New Negocio.LectorRFID_SpeedWay(LectorRFID)

            If (IsNothing(Me.ListRFID_SpeedWay)) Then Me.ListRFID_SpeedWay = New List(Of Negocio.LectorRFID_SpeedWay)
            'Agrego el LectorRFID a la lista
            Me.ListRFID_SpeedWay.Add(RFID_SpeedWay)
            'Agrego los escuchadores para los evento del lector
            AddHandler RFID_SpeedWay.TagLeido, AddressOf Me.TagLeido
            AddHandler RFID_SpeedWay.ErrorRFID, AddressOf Me.ErrorRFID
            AddHandler RFID_SpeedWay.InfoRFID, AddressOf Me.InfoRFID


        Next

        CtrlEstadoLectoresRFID1.CargarLectores(Me.ListRFID_SpeedWay)
    End Sub

    Private Function getAntenaLeyendo(ID_SECTOR As Integer) As Controles.CtrlAntenaLeyendo
        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = Nothing

        Select Case ID_SECTOR
            Case BAL1_BAR1.ID_SECTOR
                ctrlAntLeyendo = CtrlAnt_Leyendo_BAL1_BAR1
            Case BAL1_BAR2.ID_SECTOR
                ctrlAntLeyendo = CtrlAnt_Leyendo_BAL1_BAR2
            Case BAL2_BAR1.ID_SECTOR
                ctrlAntLeyendo = CtrlAnt_Leyendo_BAL2_BAR1
            Case BAL2_BAR2.ID_SECTOR
                ctrlAntLeyendo = CtrlAnt_Leyendo_BAL2_BAR2
        End Select



        Return ctrlAntLeyendo
    End Function

    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <param name="ID_SECTOR"></param>
    ''' <returns></returns>
    Private Function getBufferAntena(ID_SECTOR As Integer) As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Nothing

        'Le pregunto el sector a las barreras 
        'Para saber si es de ingreso o de egreso
        If BAL1_BAR1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBal1Bar1
        ElseIf BAL1_BAR2.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBal1Bar2
        ElseIf BAL2_BAR1.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBal2Bar1
        ElseIf BAL2_BAR2.ID_SECTOR = ID_SECTOR Then
            Buffer = BufferBal2Bar2
        End If

        Return Buffer
    End Function

    Private Sub EstaHabilitado(ID_SECTOR As Integer, TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            Dim tmp As String '= String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            'CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)

            Dim CtrlCartel As Controles.CtrlCartelMultiLED = Nothing
            Dim ctrlCabezalBalanza As Controles.CtrlCabezalBalanza = Nothing
            Dim Comando As Entidades.Constante.BalanzaComando
            Dim CtrlInfoBalanza As ctrlInfomacionBalanza = Nothing
            Dim numBalanza As Integer
            'Ejecuto el comando dependiendo de la barrera 
            'Para saber si es de ingreso o de egreso

            Select Case ID_SECTOR
                Case BAL1_BAR1.ID_SECTOR, BAL1_BAR2.ID_SECTOR
                    ctrlCabezalBalanza = BALANZA1
                    CtrlCartel = CartelLED_BALANZA1
                    CtrlInfoBalanza = CtrlInfomacionBalanza1
                    numBalanza = 1
                Case BAL2_BAR1.ID_SECTOR, BAL2_BAR2.ID_SECTOR
                    ctrlCabezalBalanza = BALANZA2
                    CtrlCartel = CartelLED_BALANZA2
                    CtrlInfoBalanza = CtrlInfomacionBalanza2
                    numBalanza = 2
            End Select

            'Si la balanza esta en otro estado que no sea la barrera cerrada significa que la balanza no esta habilitada o esta ocupada
            If ctrlCabezalBalanza.oEstado.valor <> Constante.BalanzaEstado.BarrerasCerrada Then
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion,
                                                                String.Format("[" + DateTime.Now + "] INFO: La balanza {0} no se encuentra habilitada o esta ocupada", numBalanza))
                Return
            End If


            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)
            'tmp = String.Format("axRespuesta: {0}", axRespuesta.ToString())
            'CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)



            'Pregunto si esta habilitado
            If axRespuesta.habilitado Then
                'Si esta habilitado
                'Muestro el mensaje en el cartel
                tmp = "[ " + DateTime.Now + " ]  CAMIÓN HABILITADO: ID TRANSACCIÓN: " + axRespuesta.idTransaccion.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)
                Select Case ID_SECTOR
                    Case BAL1_BAR1.ID_SECTOR, BAL2_BAR1.ID_SECTOR
                        Comando = BalanzaComando.PermitirIngresoBarrera1
                    Case BAL1_BAR2.ID_SECTOR, BAL2_BAR2.ID_SECTOR
                        Comando = BalanzaComando.PermitirIngresoBarrera2
                End Select

                'Ejecuto el comando permitir ingresar en la barrera
                If Not IsNothing(ctrlCabezalBalanza) Then
                    Dim nConfiguracion As New Negocio.ConfiguracionN
                    'Dim CAMION_PESANDO As New CAMION_PESANDO(TagRFID, axRespuesta, ID_SECTOR, numBalanza)
                    'nConfiguracion.setCAMION_PESANDO(numBalanza, CAMION_PESANDO)

                    'If Not IsNothing(CtrlInfoBalanza) Then CtrlInfoBalanza.CAMION_PESANDO(True, CAMION_PESANDO)
                    ctrlCabezalBalanza.EjectComando(Comando)
                Else
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve,
                                                                 "Ningun contorl de barrera tiene asignado el sector " & ID_SECTOR)
                End If

            Else
                tmp = "[ " + DateTime.Now + " ]  CAMIÓN NO HABILITADO: " + axRespuesta.mensaje.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorUser, tmp)
                'Guardo el ultimo tag leido
                Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)

            End If


            If Not IsNothing(CtrlCartel) Then CtrlCartel.EscribirCartel(axRespuesta.mensaje)
        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End Try

    End Sub

    Private Sub RefrescarDinamismo()
        'BARRERAS
        If Me.BAL1_BAR1.Inicializado Then
            Me.BAL1_BAR1.SetEstado()
            CtrlDinamismoBalanza1.SetBarreraEstado(1, 1, BAL1_BAR1.oEstado.valor)

        End If
        If Me.BAL1_BAR2.Inicializado Then
            Me.BAL1_BAR2.SetEstado()
            CtrlDinamismoBalanza1.SetBarreraEstado(1, 2, BAL1_BAR2.oEstado.valor)
        End If

        If Me.BAL2_BAR1.Inicializado Then
            Me.BAL2_BAR1.SetEstado()
            CtrlDinamismoBalanza1.SetBarreraEstado(2, 1, BAL2_BAR1.oEstado.valor)
        End If
        If Me.BAL2_BAR2.Inicializado Then
            Me.BAL2_BAR2.SetEstado()
            CtrlDinamismoBalanza1.SetBarreraEstado(2, 2, BAL2_BAR2.oEstado.valor)
        End If

        'SENSORES
        If Me.BAL1_S1.Inicializado Then
            Me.BAL1_S1.SetEstadoPLC()
            CtrlDinamismoBalanza1.SetSensorEstado(1, 1, Me.BAL1_S1.oEstado.valor)
        End If
        If Me.BAL1_S2.Inicializado Then
            Me.BAL1_S2.SetEstadoPLC()
            CtrlDinamismoBalanza1.SetSensorEstado(1, 2, Me.BAL1_S2.oEstado.valor)
        End If
        If Me.BAL2_S1.Inicializado Then
            Me.BAL2_S1.SetEstadoPLC()
            CtrlDinamismoBalanza1.SetSensorEstado(2, 1, Me.BAL2_S1.oEstado.valor)
        End If
        If Me.BAL2_S2.Inicializado Then
            Me.BAL2_S2.SetEstadoPLC()
            CtrlDinamismoBalanza1.SetSensorEstado(2, 2, Me.BAL2_S2.oEstado.valor)
        End If
        'BALANZAS
        If Me.BALANZA1.Inicializado Then
            Me.BALANZA1.Refrescar()
        End If
        If Me.BALANZA2.Inicializado Then
            Me.BALANZA2.Refrescar()
        End If
        'SENSORES DE LECTURA
        If Me.S_LECTURA_BAL1_BAR1.Inicializado Then
            Me.S_LECTURA_BAL1_BAR1.SetEstadoPLC()
        End If
        If Me.S_LECTURA_BAL1_BAR2.Inicializado Then
            Me.S_LECTURA_BAL1_BAR2.SetEstadoPLC()
        End If
        If Me.S_LECTURA_BAL2_BAR1.Inicializado Then
            Me.S_LECTURA_BAL2_BAR1.SetEstadoPLC()
        End If
        If Me.S_LECTURA_BAL2_BAR2.Inicializado Then
            Me.S_LECTURA_BAL2_BAR2.SetEstadoPLC()
        End If

        'Dim nAlarma As New Negocio.AlarmaN
        ' modDelegado.setVisibleCtrl(Me, btnAlarma, nAlarma.mostrarAlarma(Me.ID_CONTROL_ACCESO))
    End Sub

    Private Sub ConectarLectoresRFID()

        'Recorro los lectores RFID Configurado
        For Each RFID_SpeedWay As LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
            'Si el lector no esta conectado
            If Not RFID_SpeedWay.IsConnected Then

                Try

                    oLector = nLector.GetOne(RFID_SpeedWay.Ip)
                    oLector.CONECTADO = RFID_SpeedWay.Conectarse()
                    nLector.Update(oLector)

                Catch ex As Exception
                    'Surgio un error en la conexion
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, ex.Message)
                End Try
            Else
                'El lector ya se encuentra conectado
                Dim tmp As String = "[ " + DateTime.Now + " ] INFO: " + String.Format("El lector {0} se encuentra conectado", RFID_SpeedWay.Nombre)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, tmp)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Le consulto al PLC si tengo que comenzar la lectura RFID
    ''' </summary>
    Private Sub VerEstadoLecturaRFID()

        Dim nDatoBool As New Negocio.DatoBoolN
        Dim nombreTag As String = String.Format("{0}.{1}", TIPO_CONTROL_ACCESO.BALANZA, PROPIEDADES_TAG_PLC.LECTURA_RFID)
        Dim oDatoLecturaRFID As DATO_BOOL = nDatoBool.GetOne(Me.ControlAcceso.ID_PLC, nombreTag)

        If Not IsNothing(oDatoLecturaRFID) Then
            'Recorro todos los lectores que tiene el control de acceso 
            For Each itemLector As Negocio.LectorRFID_SpeedWay In Me.ListRFID_SpeedWay
                If itemLector.IsConnected Then 'Si esta conectado
                    'Pregunto si tendria que estar leyendo
                    If oDatoLecturaRFID.valor = True Then
                        If Not itemLector.isRunning Then 'Si no esta leyendo 
                            itemLector.StartRead() 'Comienza a leer
                        End If

                    Else ' No tendria que estar leyendo
                        If itemLector.isRunning Then 'Si  esta leyendo 
                            itemLector.StopRead() ' Paro la lectura
                        End If

                    End If
                End If
            Next
        Else
            Dim msj As String = String.Format("No se encontro el tipo de dato {0}", nombreTag)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
        End If

    End Sub

    Private Sub SetBuffer()

        'Inicializo los buffer
        Me.BufferBal1Bar1 = New Entidades.BufferTag()
        Me.BufferBal1Bar2 = New Entidades.BufferTag()
        Me.BufferBal2Bar1 = New Entidades.BufferTag()
        Me.BufferBal2Bar2 = New Entidades.BufferTag()

        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.BufferBal1Bar1.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferBal1Bar2.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferBal2Bar1.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER
        Me.BufferBal2Bar2.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER


    End Sub
    Private hResetBal1 As Thread
    Private hResetBal2 As Thread
    Dim UltEstadoBalanza2 As ConstanteBalanza.EstadoBalanza = ConstanteBalanza.EstadoBalanza.BajaBarrera2Salida
    Private hEnviarPesoBal1 As Thread
    Private hEnviarPesoBal2 As Thread
    Private _CAMION_PESANDOBal1 As Entidades.CAMION_PESANDO
    Private _CAMION_PESANDOBal2 As Entidades.CAMION_PESANDO
    Private WithEvents bfBalanza1 As Entidades.BufferTag
    Private WithEvents bfBalanza2 As Entidades.BufferTag
    Dim UltEstadoBalanza1 As ConstanteBalanza.EstadoBalanza = ConstanteBalanza.EstadoBalanza.BajaBarrera1Salida
    Private _TiempoEsperaBal1 As Integer
    Private _TiempoEsperaBal2 As Integer
    Private Sub VerEstadoBalanza(ByVal numeroBalanza As Integer)
        Dim SUB_TAG As String = "VerEstadoBalanza"
        Try
            'SELECCIONO LA BALANZA
            'Dim _Balanza As Entidades.ConstanteBalanza.Balanzas
            Dim controlBalanza As New CtrlCabezalBalanza
            Dim nBalanza As New Negocio.BalanzaN
            Dim ctrlCartelLED As New CtrlCartelMultiLED
            Dim ctrlInformacionBalanza As ctrlInfomacionBalanza
            Dim ctrlTiempoEspera As CtrlTiempoEspera

            If numeroBalanza = 1 Then
                controlBalanza = BALANZA1
                ctrlCartelLED = Me.CartelLED_BALANZA1
                ctrlInformacionBalanza = Me.CtrlInfomacionBalanza1
                ctrlTiempoEspera = Me.CtrlTiempoEsperaBal1
            Else
                controlBalanza = BALANZA2
                ctrlCartelLED = Me.CartelLED_BALANZA2
                ctrlInformacionBalanza = Me.CtrlInfomacionBalanza2
                ctrlTiempoEspera = Me.CtrlTiempoEsperaBal2
            End If


            'Bandera para saber si el sistema tiene que escribir en el 
            'cartel por defector siempre escribo en el cartel
            Dim EscribirCartel As Boolean = True
            'Mensaje que debo escribir en el cartel
            Dim MensajeCartel As String = "AcaBio" 'DateTime.Now.ToString("HH:mm")
            Dim MensajePorDefector As String = DateTime.Now.ToString("HH:mm")
            Dim _EstadoBalanza As ConstanteBalanza.BarreraEstado = controlBalanza.oEstado.valor
            Dim CAMION_PESANDO As CAMION_PESANDO = If(numeroBalanza = ConstanteBalanza.Balanzas.Uno, _CAMION_PESANDOBal1, _CAMION_PESANDOBal2)
            Dim hEnviarPeso As Thread = If(numeroBalanza = ConstanteBalanza.Balanzas.Uno, hEnviarPesoBal1, hEnviarPesoBal2)
            Dim nombreEstado As String = ""
            'Obtengo el ultimo estado de la balanza
            Dim UltEstadoBalanza As Constante.BalanzaEstado = If(numeroBalanza = ConstanteBalanza.Balanzas.Uno, Me.UltEstadoBalanza1, Me.UltEstadoBalanza2)


            If numeroBalanza = ConstanteBalanza.Balanzas.Uno Then
                UltEstadoBalanza1 = _EstadoBalanza
            Else
                UltEstadoBalanza2 = _EstadoBalanza
            End If
            Dim oConfig As New Entidades.CONFIGURACION
            Dim oConfigN As New Negocio.ConfiguracionN
            oConfig = oConfigN.GetOne

            If controlBalanza.oEstado.valor = Constante.BalanzaEstado.BarrerasCerrada Then '<---- BARRERA CERRADA
                nombreEstado = "Barrera Cerrada"
                EscribirCartel = True
                MensajeCartel = "AcaBio"
                CAMION_PESANDO = Nothing

                'Si quedo el hilo de Enviar peso vivo lo aborto
                If Not IsNothing(hEnviarPeso) And Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
                    If hEnviarPeso.IsAlive Then hEnviarPeso.Abort()
                    hEnviarPeso = Nothing

                End If

                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                If numeroBalanza = ConstanteBalanza.Balanzas.Uno Then
                    _TiempoEsperaBal1 = 0
                    CtrlTiempoEsperaBal1.SetVisible = False
                    hEnviarPesoBal1 = Nothing
                Else ' Balanza 2
                    _TiempoEsperaBal2 = 0
                    CtrlTiempoEsperaBal2.SetVisible = False
                    hEnviarPesoBal2 = Nothing
                End If





            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.HabilitadaIngresarBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.HabilitadaIngresarBarrera2 Then '<--- HABILITADA INGRESAR
                'SI EL ESTADO DE LA BALANZA ES HABILITADA INGRESAR

                nombreEstado = "Habilitada Ingresar Barrera"
                'ESCRIBO AVANCE EN LOS CARTELES

                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_AVANCE
                'Habilito el camión pesando
                Dim nConfiguracion As New Negocio.ConfiguracionN
                'Obtengo el camion que esta pesando


                If numeroBalanza = 1 Then
                    _CAMION_PESANDOBal1 = CAMION_PESANDO
                Else
                    _CAMION_PESANDOBal2 = CAMION_PESANDO
                End If

                ctrlInformacionBalanza.CAMION_PESANDO(True, CAMION_PESANDO)
                Dim tiempoEspera As Integer = If(numeroBalanza = 1, _TiempoEsperaBal1, _TiempoEsperaBal2)

                If (tiempoEspera < oConfig.TIEMPO_ESPERA) Then
                    ctrlTiempoEspera.SetVisible = True
                    If numeroBalanza = 1 Then _TiempoEsperaBal1 += 1 Else _TiempoEsperaBal2 += 1
                    ctrlTiempoEspera.Tiempo = oConfig.TIEMPO_ESPERA - tiempoEspera

                Else ' Igual

                    Dim DescripcionAudit As String = "Reset de Balanza por tiempo agotado :  " & numeroBalanza
                    Negocio.AuditoriaN.AddAuditoria(DescripcionAudit, "", ID_SECTOR, 0, "Aumax", Constante.acciones.Reset_Balanza)
                    'Aqui realizar el reset de balanza
                    ctrlTiempoEspera.SetVisible = False

                    'Si la computadora es servidor aplica el reset
                    If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then controlBalanza.Reset()

                End If

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.ErrorBalanza1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.ErrorBalanza2 Then '<--- BALANZA EN CERO O NO CONECTADA
                'SI EL ESTADO ES BALANZA CERO
                'Selecciono el cartel que debo escribir,siempre escribo al cartel opuesto 
                'de la barrera por la cual accede el camion
                nombreEstado = "Balanza Cero NoConectada Barrera"
                'ESCRIBO EN EL CARTEL LLAMAR
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_LLAMAR

                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                If numeroBalanza = ConstanteBalanza.Balanzas.Uno Then
                    _TiempoEsperaBal1 = 0
                Else
                    _TiempoEsperaBal2 = 0
                End If
            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.AvanzarCamionBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.AvanzarCamionBarrera2 Then '<--- AVANZA CAMION
                'SI EL ESTADO ES AVANCE CAMION
                nombreEstado = "Avanzar Camion Barrera"
                'ESCRIBO EN EL CARTEL AVANCE
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_AVANCE
                ctrlTiempoEspera.SetVisible = False
                ''CtrlBalanza1.SetTiempoEspera(_Balanza, 0)

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.RetrocedaCamionBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.RetrocedaCamionBarrera2 Then '<--- RETROCEDA CAMION
                'SI EL ESTADO ES RETROCEDA CAMION
                nombreEstado = "RetrocedaCamionBarrera"
                'ESCRIBO EN EL CARTEL RETROCEDA
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_RETROCEDA


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.EstabilizandoPesoBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.EstabilizandoPesoBarrera2 Then '<--- ESTABILIZANDO PESO
                nombreEstado = "Estabilizando Peso Barrera"
                'ESCRIBO EN EL CARTEL LA HORA ACTUAL
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_ESPERE
                ctrlTiempoEspera.SetVisible = False

            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.PesoMaximoBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.PesoMaximoBarrera2 Then '<---PESO MAXIMO

                nombreEstado = "Peso Maximo Barrera"
                'ESCRIBO EN EL CARTEL LA HORA ACTUAL
                MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_PESO_MAXIMO


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.TomarPesoBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.TomarPesoBarrera2 Then ' <--- TOMAR PESO


                Dim nConfiguracion As New Negocio.ConfiguracionN

                If numeroBalanza = 1 Then
                    _CAMION_PESANDOBal1 = CAMION_PESANDO
                Else
                    _CAMION_PESANDOBal2 = CAMION_PESANDO
                End If

                'Si ya envio el peso espero hasta que el plc cambie de estado
                nombreEstado = "Tomar Peso Barrera"
                If CAMION_PESANDO.PESO_ENVIADO Then Return

                EscribirCartel = False
                ' si el hilo no esta en ejecucion  es porque se todavia no se envio el peso 
                'si la maquina es el servio envia el peso tomado por WS
                If IsNothing(hEnviarPeso) And Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then '
                    EscribirCartel = True
                    MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_ESPERE

                    hEnviarPeso = New Thread(AddressOf EnviarPeso)
                    hEnviarPeso.IsBackground = True
                    'Envio el hilo y espero la respuesta
                    If numeroBalanza = 1 Then
                        hEnviarPesoBal1 = hEnviarPeso
                    Else
                        hEnviarPesoBal2 = hEnviarPeso
                    End If
                    hEnviarPeso.Start(numeroBalanza)
                End If


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.PermitirSalirBarrera1 Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.PermitirSalirBarrera2 Then '<-- PERMITIR SALIR
                nombreEstado = "Permitir Salir Barrera"
                EscribirCartel = False


            ElseIf controlBalanza.oEstado.valor = Constante.BalanzaEstado.BajaBarrera1Salida Or controlBalanza.oEstado.valor = Constante.BalanzaEstado.BajaBarrera2Salida Then '<--- BAJAR BARRERA
                EscribirCartel = False
                CAMION_PESANDO = Nothing
                EscribirCartel = False
                nombreEstado = "Baja Barrera Salida"
                'Informo a los cliente a los clientes que que el camión dejo de pesar
                Dim nConfiguracion As New Negocio.ConfiguracionN

                If numeroBalanza = 1 Then
                    CtrlInfomacionBalanza1.CAMION_PESANDO(False, Nothing)
                Else ' 2
                    CtrlInfomacionBalanza2.CAMION_PESANDO(False, Nothing)
                End If


            End If



            If UltEstadoBalanza <> _EstadoBalanza And Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then  'Si cambio de estado y es servidor


                Dim idTrasaccion As Long = 0
                'Si tengo un camion pesando
                If Not IsNothing(CAMION_PESANDO) Then
                    'Coloco el idTrasaccion para la auditoria
                    'If Not IsNothing(CAMION_PESANDO.axRespuesta) Then idTrasaccion = CAMION_PESANDO.axRespuesta.idTransaccion
                End If

                Dim DescripcionAuditoria As String = "Cambio de estado en la balanza :  " & numeroBalanza & " a estado : " & nombreEstado
                Negocio.AuditoriaN.AddAuditoria(DescripcionAuditoria, "", ID_SECTOR, idTrasaccion, "Aumax", Constante.acciones.cambioEstadoBalaza)
                If EscribirCartel Then

                    ctrlCartelLED.EscribirCartel(MensajeCartel)

                End If


            End If

        Catch ex As Exception

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
        End Try

    End Sub

    Private Sub EnviarPeso(ByVal numBalanza As Integer)
        Dim nBalanza As New Negocio.BalanzaN
        Dim SUB_TAG As String = "[EnviarPeso]"
        Try
            Dim CabezalBalanza As Controles.CtrlCabezalBalanza
            Dim ctrlInfo As Controles.ctrlInfomacionBalanza
            Dim Cartel As Controles.CtrlCartelMultiLED
            Dim oCamara As Controles.CtrlCamara
            If numBalanza = 1 Then
                CabezalBalanza = BALANZA1
                ctrlInfo = CtrlInfomacionBalanza1
                Cartel = CartelLED_BALANZA1
                oCamara = Me.CAM_BAL1
            Else '2
                CabezalBalanza = BALANZA2
                ctrlInfo = CtrlInfomacionBalanza2
                Cartel = CartelLED_BALANZA2
                oCamara = Me.CAM_BAL2
            End If

            'Tomar el peso de la balanza
            Dim PesoBalanza As Integer = CabezalBalanza.oPeso.valor

            'CtrlBalanza1.SetEsperaWS(_Balanza, True)

            Dim CAMION_PESANDO As CAMION_PESANDO = If(CabezalBalanza.numeroBalanza = ConstanteBalanza.Balanzas.Uno, _CAMION_PESANDOBal1, _CAMION_PESANDOBal2)


            Dim Habilitado As Boolean = False
            Dim AxRespuesta As Entidades.AxRespuesta = Nothing
            Dim nConf As New Negocio.ConfiguracionN
            Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()
            Dim cant As Integer = 0

            While Habilitado = False

                ctrlInfo.Espera(True)
                Try
                    PesoBalanza = CabezalBalanza.oPeso.valor
                    'AxRespuesta = Negocio.WebServiceBitN.RegistrarPesada(CAMION_PESANDO.axRespuesta.ID_SECTOR, CAMION_PESANDO.tagRFID, PesoBalanza, CAMION_PESANDO.axRespuesta.idTransaccion)
                Catch ex As Exception
                    CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
                End Try


                If Not IsNothing(AxRespuesta) Then
                    Dim tmp = "[ " + DateTime.Now + " ] PESO ENVIADO :" + String.Format(" {0}", AxRespuesta.ToString())
                    CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Suceso, tmp)

                    CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.Informacion, "[ " + DateTime.Now + " ] INFO: " + " PESO OBTENIDO: " & AxRespuesta.ToString())

                    CAMION_PESANDO.PESO_ENVIADO = True
                    'CAMION_PESANDO.axRespuesta = AxRespuesta
                    ctrlInfo.CAMION_PESANDO(True, CAMION_PESANDO)
                    Habilitado = AxRespuesta.habilitado
                    If AxRespuesta.habilitado Then
                        CabezalBalanza.EjectComando(BalanzaComando.PermitirSalir)
                        Exit While
                    End If
                End If

                Thread.Sleep(oConf.TIEMPO_ENVIAR_PESO * 1000)
                cant += 1
                ctrlInfo.setCant(cant)
            End While
            'Si sale del bucle es porque la respuesta del ws es TRUE
            oCamara.SacarFoto(AxRespuesta.idTransaccion, AxRespuesta.idSector)
            ctrlInfo.Espera(False)

            If Not IsNothing(AxRespuesta) Then Cartel.EscribirCartel(AxRespuesta.mensaje)

            Thread.Sleep(2000) ' Duemo 2 segundo asi toma el cambio de estado el plc  
        Catch ex As Exception

            'axLog.AxLog.e(Tag, SUB_TAG & ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
            If numBalanza = 1 Then
                hEnviarPesoBal1 = Nothing
            Else
                hEnviarPesoBal2 = Nothing
            End If

        End Try



    End Sub

    Private Sub ResetBalanza(ByVal Balanza As ConstanteBalanza.Balanzas)

        Try
            CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.Informacion, "[ " + DateTime.Now + " ] INFO: " + "RESET BALANZA : " & Balanza.ToString)

            'AUDITO EL RESET DE BALANZA 2016-11-16 NFB
            Dim CAMION_PESANDO As CAMION_PESANDO = If(Balanza = ConstanteBalanza.Balanzas.Uno, _CAMION_PESANDOBal1, _CAMION_PESANDOBal2)
            Dim TagRFID As String = If(IsNothing(CAMION_PESANDO), "", CAMION_PESANDO.TAG_RFID)
            'Dim idTrasaccion As Integer = If(IsNothing(CAMION_PESANDO), 0, CAMION_PESANDO.axRespuesta.idTransaccion)
            'Dim ID_SECTOR As Integer = If(IsNothing(CAMION_PESANDO), 0, CAMION_PESANDO.axRespuesta.ID_SECTOR)
            Negocio.AuditoriaN.AddAuditoria("Reset Balanza : " & Balanza.ToString, TagRFID, ID_SECTOR, 0, Entidades.Constante.USUARIO_AUMAX, acciones.Reset_Balanza)


            ' Dim nTag As New Negocio.TagPlcN
            Dim NombrePlc As String = Entidades.Constante.getNombrePLC(Negocio.ModSesion.PUESTO_TRABAJO.NOMBRE)
            Dim NombreTagHabilitacion As String = If(Balanza = ConstanteBalanza.Balanzas.Uno, ConstanteBalanza.CMD_BALANZA_1,
                                         ConstanteBalanza.CMD_BALANZA_2)
            'deshabilito la balanza
            'Dim xRet As Boolean = nTag.SetValorTag(NombrePlc, NombreTagHabilitacion, ConstanteBalanza.ComandoBalanza.DeshabilitarBalanza)

            Thread.Sleep(2000) ' 3 segundos
            'Espero hasta que se deshabilite la balanza
            While True
                ' Entidades.Balanza.ESTADO_HABILITACION_BAL1 Or oTAG.nombre = Entidades.Balanza.ESTADO_HABILITACION_BAL2
                Dim NombreTagEstadoHabilitacion As String = If(Balanza = ConstanteBalanza.Balanzas.Uno, ConstanteBalanza.ESTADO_HABILITACION_BAL1, ConstanteBalanza.ESTADO_HABILITACION_BAL2)

                'Dim valor As Decimal = nTag.GetValorTag(NombrePlc, NombreTagEstadoHabilitacion)
                'Una vez que se deshabilito la vuelvo habilitar
                'If valor = ConstanteBalanza.EstadoHabilitacionBalanza.Deshabilitado Then
                '    'nTag.SetValorTag(NombrePlc, NombreTagHabilitacion, ConstanteBalanza.ComandoBalanza.HabilitarBalanza)
                '    Exit While
                'End If
                Thread.Sleep(1000) '1 seGundo
            End While



        Catch ex As Exception
            CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, "[ " + DateTime.Now + " ] INFO: " + "RESET BALANZA: " & ex.Message)


        End Try




    End Sub

    ''' <summary>
    ''' Permite saber si la antena esta habilitada a leer
    ''' verifica si el sensor del sector esta cortando
    ''' </summary>
    ''' <param name="ID_SECTOR"> Numero de sector para verficiar si el sensor esta cortando</param>
    ''' <returns>FALSE = No esta habilitado a leer TRUE = Habilitado a leer </returns>
    Private Function AntenaHabilitada(ID_SECTOR As Integer) As Boolean

        Dim ctrlSensorLectura As Controles.CtrlSensor = Nothing

        Select Case ID_SECTOR
            Case S_LECTURA_BAL1_BAR1.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_BAL1_BAR1

            Case S_LECTURA_BAL1_BAR2.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_BAL1_BAR2

            Case S_LECTURA_BAL2_BAR1.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_BAL2_BAR1

            Case S_LECTURA_BAL2_BAR2.ID_SECTOR
                ctrlSensorLectura = S_LECTURA_BAL2_BAR2

            Case Else
                Return False
        End Select
        'La antena va estar habilitada si el sensor va estar abierto
        Return ctrlSensorLectura.oEstado.valor = SensorInfrarojoEstado.ConPresencia

    End Function

#End Region

#Region "SUBPROCESOS"

    Public Sub Monitoreo()
        'Apenas comienza el hilo conecto los lectores
        If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
            If Not IsNothing(Me.ListRFID_SpeedWay) Then ConectarLectoresRFID()
        End If


        While True

            Try
                RefrescarDinamismo()


                If Negocio.ModSesion.PUESTO_TRABAJO.SERVIDOR Then
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFID()
                    VerEstadoLecturaRFID()
                Else
                    Dim oLectores As New List(Of Entidades.LECTOR_RFID)
                    oLectores = nLector.GetAll(ID_CONTROL_ACCESO)
                    CtrlEstadoLectoresRFID1.RefrescarEstadosRFIDCliente(oLectores)
                End If

                VerEstadoBalanza(1) ' Veo el estado de la balanza 1
                VerEstadoBalanza(2) ' Veo el estado de la balanza 1

            Catch ex As Exception
                Dim msj As String = String.Format("{0} - {1}", "[Monitoreo]", ex.Message)
                CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, msj)
            End Try
            Thread.Sleep(Me.ControlAcceso.MONITOREO)
        End While

    End Sub

#End Region

#Region "LECTOR RFID"

    ''' <summary>
    ''' Evento que se ejecuta cuando se leyo un tag 
    ''' </summary>
    ''' <param name="TagRFID"></param>
    ''' <param name="NumAntena"></param>
    ''' <remarks></remarks>
    Public Sub TagLeido(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim SUB_TAG As String = "[TagLeido]"
        Console.WriteLine("TAG RFID LEIDO: " & TagRFID)

        Dim nSector As New Negocio.SectorN
        Dim ID_SECTOR As Integer = 0

        'Busco el sector al que corresponde la antena
        Dim nAntena As New Negocio.Antena_RfidN
        Dim oAntena As Entidades.ANTENAS_RFID = nAntena.GetOne(NumAntena, LectorRFID.CONFIG_LECTOR_RFID(0).ID_CONF_LECTOR_RFID)

        If IsNothing(oAntena) Then
            Dim tmp As String = String.Format("{0} La antena {1} no esta habilitada", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        Else
            If Not IsNothing(oAntena.ID_SECTOR) Then ID_SECTOR = oAntena.ID_SECTOR

        End If


        If ID_SECTOR = 0 Then 'No tiene un sector asignado la antena
            Dim tmp As String = String.Format("{0} La antena {1} no tiene ningun sector asignado", SUB_TAG, NumAntena)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
            Return
        End If

        If Not manual Then ' Si no es manual (Manual = False) verifica si la antena esta habilitada para leer
            'Si la antena no esta habilitada
            If Not AntenaHabilitada(ID_SECTOR) Then Return
        End If


        Dim ctrlAntLeyendo As Controles.CtrlAntenaLeyendo = getAntenaLeyendo(ID_SECTOR)
        If Not IsNothing(ctrlAntLeyendo) Then
            ctrlAntLeyendo.TagLeyendo(TagRFID)
        Else
            Dim tmp As String = String.Format("{0} No se encontro el CtrlAntenaLeyendo", SUB_TAG)
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, tmp)
        End If

        'Obtengo el buffer
        Dim Buffer As Entidades.BufferTag = getBufferAntena(ID_SECTOR)

        'Agrego al buffer al tag leido y  obtengo el dialog cuando agregue al buffer el tagrfid
        Dim _BufferDIalog As Entidades.BufferTag.BufferDialog = Buffer.AddTagLeido(TagRFID)

        If _BufferDIalog = BufferTag.BufferDialog.TagLeido Then Return ' Termino el procedimiento

        'EJECUTO EL PROCEDIMINETO "EstaHabilitado" PARA SABER SI EL TAG LEIDO ESTA HABILITADO A INGRESAR O SALIR DE LA PORTERIA
        EstaHabilitado(ID_SECTOR, TagRFID)
    End Sub


    ''' <summary>
    ''' Evento que se ejecuta para  informa un error en el Lector RFID
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub ErrorRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.xErrorDeve, Msj)
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta cuando el lector envia un mensaje de información
    ''' </summary>
    ''' <param name="Msj"></param>
    ''' <param name="LectorRFID_SpeedWay"></param>
    Public Sub InfoRFID(Msj As String, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay, idSector As Integer)
        CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_CONTROL_ACCESO, TipoNotificacion.Informacion, Msj, idSector)
    End Sub

    Public Sub EventoGPO(Puerto As Integer, Estado As Boolean, LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay)

        If Estado = True Then 'Si el estado del sensor es TRUE (Cortando)

            If Not LectorRFID_SpeedWay.IsStar Then 'Y no esta leyendo

                LectorRFID_SpeedWay.StartRead() ' Comenzar a leer

            End If

        Else ' Estado = False (No cortando)
            'Verifico el estado de los demas puertos para saber si tengo que parar la lectura

            'Obtengo el estao del lector
            Dim Status As Status = LectorRFID_SpeedWay.QueryStatus()
            'Bandera para saber si tengo que parar la lectura
            Dim StopRead As Boolean = True

            Console.WriteLine("############# ESTADO PUERTO #############")
            Console.WriteLine("")
            For Each itemGpi As GpiStatus In Status.Gpis 'Recorro los puertos del lector
                Console.WriteLine(String.Format("Puerto:{0}|Estado: {1}", itemGpi.PortNumber, itemGpi.State))
                'Si esta en True
                If itemGpi.State = True Then
                    Dim PuertoHabilitado As Boolean
                    'Consulto si el puerto esta habilitado en la configuracion para trabajar
                    Select Case itemGpi.PortNumber
                        Case 1
                            PuertoHabilitado = LectorRFID_SpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO1
                        Case 2
                            PuertoHabilitado = LectorRFID_SpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO2
                        Case 3
                            PuertoHabilitado = LectorRFID_SpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO3
                        Case 4
                            PuertoHabilitado = LectorRFID_SpeedWay.LectorRFID.CONFIG_LECTOR_RFID(0).GPIO_PUERTO4
                    End Select

                    If PuertoHabilitado Then 'Si el puerto esta habilitado
                        'Si el puerto esta habilitado para trabajar y el estado del puerto es True NO PARO LA LECTURA
                        StopRead = False
                    End If
                End If

            Next
            Console.WriteLine("StopRead = " & StopRead)
            'TENGO QUE PARAR LA LECTURA DEL LECTOR
            If StopRead Then LectorRFID_SpeedWay.StopRead()

        End If


    End Sub

    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub


    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub

#End Region

#Region "Otros Eventos"

    Public Sub ClearBufferBAL1_BAR1() Handles BufferBal1Bar1.ClearBuffer
        Me.CtrlAnt_Leyendo_BAL1_BAR1.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferBAL1_BAR2() Handles BufferBal1Bar2.ClearBuffer
        Me.CtrlAnt_Leyendo_BAL1_BAR2.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferBAL2_BAR1() Handles BufferBal2Bar1.ClearBuffer
        Me.CtrlAnt_Leyendo_BAL2_BAR1.BorrarUltimosTagLeido()
    End Sub

    Public Sub ClearBufferBAL2_BAR2() Handles BufferBal2Bar2.ClearBuffer
        Me.CtrlAnt_Leyendo_BAL2_BAR2.BorrarUltimosTagLeido()
    End Sub

    Public Sub tiempoBufferBAL1_BAR1(ByVal Tiempo As Integer) Handles BufferBal1Bar1.ShowTiempoBuffer
        Me.CtrlAnt_Leyendo_BAL1_BAR1.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferBAL1_BAR2(ByVal Tiempo As Integer) Handles BufferBal1Bar2.ShowTiempoBuffer
        Me.CtrlAnt_Leyendo_BAL1_BAR2.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferBAL2_BAR1(ByVal Tiempo As Integer) Handles BufferBal2Bar1.ShowTiempoBuffer
        Me.CtrlAnt_Leyendo_BAL2_BAR1.setTiempoBuffer(Tiempo)
    End Sub

    Public Sub tiempoBufferBAL2_BAR2(ByVal Tiempo As Integer) Handles BufferBal2Bar2.ShowTiempoBuffer
        Me.CtrlAnt_Leyendo_BAL2_BAR2.setTiempoBuffer(Tiempo)
    End Sub

    Private Sub btnLecturaBal1Bar1_Click(sender As Object, e As EventArgs) Handles btnLecturaBal1Bar1.Click, btnLecturaBal1Bar2.Click, btnLecturaBal2Bar1.Click, btnLecturaBal2Bar2.Click

        Dim btn As Button = sender
        Dim ID_SECTOR As Integer
        Select Case btn.Name
            Case btnLecturaBal1Bar1.Name
                ID_SECTOR = BAL1_BAR1.ID_SECTOR
            Case btnLecturaBal1Bar2.Name
                ID_SECTOR = BAL1_BAR2.ID_SECTOR
            Case btnLecturaBal2Bar1.Name
                ID_SECTOR = BAL2_BAR1.ID_SECTOR
            Case btnLecturaBal2Bar2.Name
                ID_SECTOR = BAL2_BAR2.ID_SECTOR
        End Select

        Dim frm As New FrmLecturaManual(ID_SECTOR)
        AddHandler frm.TagLeidoManual, AddressOf TagLeidoManual
        frm.ShowDialog()
        frm.Dispose()
    End Sub

    Private Sub btnSubirBajarBarreraManual_Click(sender As Object, e As EventArgs) Handles btnSubirBal1Bar1.Click, btnSubirBal1Bar2.Click,
        btnSubirBal2Bar1.Click, btnSubirBal2Bar2.Click, btnBajarBal1Bar1.Click, BtnBajarBal1Bar2.Click,
        btnBajarBal2Bar1.Click, btnBajarBal2Bar2.Click

        Dim btn As Button = sender
        Dim ctrlBarrea As Controles.CtrlBarrera = Nothing
        Dim comando As BarreraComando = BarreraComando.Automatico
        Dim descripAuditoria As String = ""

        Select Case btn.Name
            Case btnSubirBal1Bar1.Name
                ctrlBarrea = BAL1_BAR1
                comando = BarreraComando.SubirManual
                descripAuditoria = "Subo Barrera 1 Balanza 1"
            Case btnSubirBal1Bar2.Name
                ctrlBarrea = BAL1_BAR2
                descripAuditoria = "Subo Barrera 2 Balanza 1"
                comando = BarreraComando.SubirManual
            Case btnSubirBal2Bar1.Name
                ctrlBarrea = BAL2_BAR1
                comando = BarreraComando.SubirManual
                descripAuditoria = "Subo Barrera 1 Balanza 2"
            Case btnSubirBal2Bar2.Name
                ctrlBarrea = BAL2_BAR2
                comando = BarreraComando.SubirManual
                descripAuditoria = "Subo Barrera 2 Balanza 2"
            Case btnBajarBal1Bar1.Name
                ctrlBarrea = BAL1_BAR1
                comando = BarreraComando.BajarManual
                descripAuditoria = "Subo Bajo 1 Balanza 1"
            Case BtnBajarBal1Bar2.Name
                ctrlBarrea = BAL1_BAR2
                comando = BarreraComando.BajarManual
                descripAuditoria = "Subo Bajo 2 Balanza 1"
            Case btnBajarBal2Bar1.Name
                ctrlBarrea = BAL2_BAR1
                comando = BarreraComando.BajarManual
                descripAuditoria = "Subo Bajo 2 Balanza 1"
            Case btnBajarBal2Bar2.Name
                ctrlBarrea = BAL2_BAR2
                comando = BarreraComando.BajarManual
                descripAuditoria = "Subo Bajo 2 Balanza 2"
        End Select

        If Not IsNothing(ctrlBarrea) Then
            Dim oAutenticacion = New FrmAutenticarUsuario(CategoriaUsuario.Operador)
            'If ctrlBarrea.Inicializado() Then
            If oAutenticacion.ShowDialog = DialogResult.OK Then
                ctrlBarrea.EjectComando(comando)
                Negocio.AuditoriaN.AddAuditoria(descripAuditoria, "", ctrlBarrea.ID_SECTOR, 0, WS_ERRO_USUARIO, Constante.acciones.suboBarreraEgreso, 0)
            End If
            'Else
            'MsgBox("No se encuentra inicializado el control barrera")
            'End If
        End If

    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Negocio.AuditoriaN.AddAuditoria("Reseteo Control de acceso Balanza", "", Me.ID_CONTROL_ACCESO, 0, WS_ERRO_USUARIO, Constante.acciones.ResetControlAcceso, 0)
        If Not IsNothing(ListRFID_SpeedWay) Then
            For Each lector As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
                lector.Disconnect()
            Next
            ListRFID_SpeedWay.Clear()
        End If
        Inicializar()
    End Sub

    Private Sub btnAyuda_Click(sender As Object, e As EventArgs) Handles btnAyuda.Click
        Help.ShowHelp(Me, Application.StartupPath & "\AyudaCA.chm", HelpNavigator.KeywordIndex, "PantallaBalanza")
    End Sub

    Private Sub btnAlarma_Click(sender As Object, e As EventArgs) Handles btnAlarma.Click
        ' Dim frm As New frmAlarmero(ID_CONTROL_ACCESO)
        'frm.ShowDialog()
    End Sub





    'Private Sub FotoTomada(image As Bitmap, camara As Camara) Handles CAM_BAL1.FotoTomada, CAM_BAL2.FotoTomada
    '    Dim numBalanza As Int16 = If(camara.tag = CAM_BAL1.Name, 1, 2)
    '    CtrlPanelNotificaciones1.MostrarNotificacionFoto(numBalanza, image)
    'End Sub


#End Region



End Class
