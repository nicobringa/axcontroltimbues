﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Entidades
{
    public class BufferTag
    {

#region Propiedades
        public  int TIEMPO_BORRAR = 10; // que va pasar para que se borre el buffer

        public int ultTagLeido { get; set; }
        public List<TagLeido> buffer;

        public Thread hBorrar;

        private int tiempoBuffer;

        public enum BufferDialog
        {
            NuevoTag = 0,
            TagLeido = 1
        }

        public enum TipoBorrado
        {
            Manual = 0,
            Automatico = 1
        }
    


#endregion


#region Contructor


        public BufferTag()
        {
            buffer = new List<TagLeido>();
        }


#endregion     


        #region MisEventos
        /// <summary>
        /// Eventos 
        /// </summary>
        public delegate void BufferClearEventHandler();
        public event BufferClearEventHandler ClearBuffer;
        public delegate void TiempoBufferEventHandler(int tiempo);
        public event TiempoBufferEventHandler ShowTiempoBuffer;
        

        #endregion

#region Metodos

      
    
        /// <summary>
        /// Permite Agregar un TagLeido al buffer o agregarge la ultima fecha de letura
        /// </summary>
        /// <param name="tagLeido"></param>
        /// <returns></returns>
        public BufferDialog AddTagLeido(string numTag)
        {

            // el tag ya existe en el buffer
            if (existeTag(numTag)) 
            {
                //IniciarReiniciarBorradoBuffer(); // ahora espera una x cantidad de tiempo y se borra no se reinicia el buffer
                return BufferDialog.TagLeido;
               

            }
            else // no existe el tag es nuevo
            {
                // Creo un nuevo tag leido
                TagLeido nvoTaglEido = new TagLeido(numTag);

                //Agrego el tag leido al buffer
                this.buffer.Add(nvoTaglEido);

                IniciarReiniciarBorradoBuffer();
                return BufferDialog.NuevoTag;
            }

        }

        /// <summary>
        /// Permite saber si el tag ya esta cargado en el buffer
        /// si esta cargado le actualiza la ultima fecha de lectura y le agrega la 
        /// </summary>
        /// <param name="numTag"></param>
        /// <returns></returns>
       public Boolean existeTag(string numTag)
       {
           foreach (TagLeido tagLeido in this.buffer)
           {
               if (tagLeido.numTag == numTag)
               {
                   //SUMO UNA LECTURA Y LE ASIGNO LA ULTIMA FECHA
                   tagLeido.lecturas += 1;
                   tagLeido.fecha = DateTime.Now;
                  
                   return true;
               } 
           }
           return false;
       }

        /// <summary>
        /// Permite iniciar o reiniciar  el borrado de buffer
        /// </summary>
       public void IniciarReiniciarBorradoBuffer()
       {
           if (hBorrar == null)
           {
               hBorrar = new Thread(EjecucionBorrarBuffer);
               hBorrar.IsBackground = true;
               hBorrar.Start();

           }
           else // ya se ejecuto el hilo
           {
               if (hBorrar.IsAlive) //Si esta corriendo
               {
                   tiempoBuffer = TIEMPO_BORRAR; //reinicio el tiempo de buffer
               }
               else    //Si no esta corriendo , lo ejecuto
               {
                   hBorrar = new Thread(EjecucionBorrarBuffer);
                   hBorrar.Start();
               }
           }
       }

       public void cerrar()
       {
          
       }

       private void EjecucionBorrarBuffer()
       {

           try
           {
               tiempoBuffer = TIEMPO_BORRAR; // Segundo

               //EL hilo va seguir vivo hasta que el tiempo del buffer llegue a cerro
               while (tiempoBuffer > 0)
               {
                   Thread.Sleep(1000); //1 Segundo
                   tiempoBuffer -= 1;

                     ShowTiempoBuffer(tiempoBuffer);

               }



               BorrarBuffer();
           }
           catch (Exception)
           {
               
             
           }

        
       }

       public void BorrarBuffer()
       {
           try
           {
               this.buffer.Clear();

                ClearBuffer(); //Ejecuto un evento

               ShowTiempoBuffer(0);
           }
           catch (Exception)
           {
             
           }

        
     
       }

        /// <summary>
        /// Cuando el buffer el borrado por el usuario
        /// </summary>
       public void BorrarBufferUsuario()
       {
           try
           {
               if (hBorrar != null)
               {
                   if (hBorrar.IsAlive) hBorrar.Abort();
                   hBorrar = null;

               }

               BorrarBuffer();
           }
           catch (Exception)
           {
               
              
           }
         
          

       }


        
#endregion



       #region Clases Hijas

       public class TagLeido
       {
           public DateTime fecha { get; set; }
           public string numTag { get; set; }
           public int lecturas { get; set; }


           public TagLeido(string numTag)
           {
               this.fecha = DateTime.Now;
               this.numTag = numTag;
               this.lecturas = 1;

           }

       }

       #endregion



    }

}
