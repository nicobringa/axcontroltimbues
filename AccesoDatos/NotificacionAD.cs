﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccesoDatos
{
    public class NotificacionAD
    {

        public  bool add(Entidades.NOTIFICACION notificacion )
        {
            try
            {
                Entidades.AxControlORAEntities context = new Entidades.AxControlORAEntities();
                context.NOTIFICACION.Add(notificacion);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
          
        }
          
        public List<Entidades.NOTIFICACION> getAll(long ultId , int idControlAcceso)
        {
            try
            {
                Entidades.AxControlORAEntities context = new Entidades.AxControlORAEntities();
                return context.NOTIFICACION.Where(n => n.ID_CONTROL_ACCESO == idControlAcceso &&
                n.ID_NOTIFICACION > ultId).OrderBy(m => m.ID_NOTIFICACION).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }



        /// <summary>
        /// Permite obtener el ultimo ID
        /// </summary>
        /// <returns></returns>
        public long getUltId()
        {
            try
            {
                Entidades.AxControlORAEntities context = new Entidades.AxControlORAEntities();
                Entidades.NOTIFICACION oNoti = context.NOTIFICACION.OrderByDescending(n => n.ID_NOTIFICACION).FirstOrDefault();
                return oNoti.ID_NOTIFICACION;
            }
            catch (Exception)
            {
               
                return -1;
            }
         
        }

        /// <summary>
        /// Permite borrar las notificaciones hasta la fecha especificada inclusivamente
        /// </summary>
        /// <param name="fecha"></param>
        public void borrarNotificaciones(DateTime fecha)
        {
            try
            {
                Entidades.AxControlORAEntities context = new Entidades.AxControlORAEntities();
                List<Entidades.NOTIFICACION> listNotificaciones = context.NOTIFICACION.Where(n => n.FECHA <= fecha).ToList();
                if (listNotificaciones != null) context.NOTIFICACION.RemoveRange(listNotificaciones);
                context.SaveChanges();
            }
            catch (Exception)
            {

                
            }

        }

    }
}
