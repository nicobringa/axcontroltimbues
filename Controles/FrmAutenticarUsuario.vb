﻿Imports Entidades
Imports Entidades.Constante
Public Class FrmAutenticarUsuario

    Private _Auditoria As Entidades.AUDITORIA


    Private _NivelCategoriaUsuario As Entidades.Constante.CategoriaUsuario

    Private Shared _UltUsuario As String
    Public Property UltUsuario() As String
        Get
            Return _UltUsuario
        End Get
        Set(ByVal value As String)
            _UltUsuario = value
        End Set
    End Property
    Public Usuario As String

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If CamposObligatorio() Then
            Dim nUsuario As New Negocio.UsuarioN
            Dim oUsuario As Entidades.usuario = nUsuario.getOne(txtUsuario.Text, txtContrasenia.Text)
            Dim Autorizado As Boolean


            If Not IsNothing(oUsuario) Then
                Me.Usuario = oUsuario.nombre
                Me.UltUsuario = oUsuario.NOMBRE
                'Si el usuario es supervisor
                If oUsuario.ID_CATEGORIA_USUARIO = Entidades.Constante.CategoriaUsuario.Administrador Then
                    'esta autorizado a realizar cualquier operacion
                    Autorizado = True

                ElseIf oUsuario.ID_CATEGORIA_USUARIO = Entidades.Constante.CategoriaUsuario.Supervisor Then
                    'Va estar autorizado si el nivel de categoria es supervisor o operador
                    Autorizado = _NivelCategoriaUsuario = CategoriaUsuario.Supervisor Or _NivelCategoriaUsuario = CategoriaUsuario.Operador
                Else ' El usuario es de categoria operador
                    'va estar autorizado si el nivel de categoria de usuario es operador
                    Autorizado = _NivelCategoriaUsuario = CategoriaUsuario.Operador

                End If
                ''Si esta autorizado el dialog resultado es ok
                'Me.DialogResult = If(Autorizado, DialogResult.OK, DialogResult.Cancel)

                If Autorizado Then
                    DialogResult = DialogResult.OK

                Else ' NO esta autorizado 

                    MessageBox.Show("No se ecuentra autorizado para realizar la operación", "Autenticación incorrecta", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    DialogResult = DialogResult.Cancel
                End If

            Else
                MessageBox.Show("Usuario o Contraseña  incorrecta", "Autenticación incorrecta", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                DialogResult = DialogResult.Cancel
            End If
        End If
    End Sub


    Public Sub New(ByVal NivelCategoriaUsuario As Entidades.Constante.CategoriaUsuario)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        _NivelCategoriaUsuario = NivelCategoriaUsuario
    End Sub

    Public Sub New(ByVal accion As Entidades.Constante.Acciones, ByVal auditoria As String)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        _Auditoria = New Entidades.AUDITORIA
        _Auditoria.accion = accion
        _Auditoria.descripcion = auditoria

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub FrmAutenticarUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not _UltUsuario = "" Then
            txtUsuario.Text = _UltUsuario
            txtContrasenia.SelectAll()
            txtContrasenia.Focus()

        End If
    End Sub

    Private Function CamposObligatorio() As Boolean

        If txtUsuario.Text = "" Or txtContrasenia.Text = "" Then
            MessageBox.Show("Por favor ingrese los datos solicitados", "Campos Obligatorios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        Return True
    End Function

    Private Sub txtUsuario_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUsuario.KeyDown, txtContrasenia.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnAceptar_Click(Nothing, Nothing)
        End If
    End Sub
End Class