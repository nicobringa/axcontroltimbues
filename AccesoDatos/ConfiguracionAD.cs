﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
   public  class ConfiguracionAD
    {
        public CONFIGURACION getOne()
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CONFIGURACION.Find(1);

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public Boolean Guardar(Entidades.CONFIGURACION oConfiguracion)  
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.CONFIGURACION.Find(oConfiguracion.ID_CONFIGURACION);

                oConsulta.TIEMPO_BUFFER = oConfiguracion.TIEMPO_BUFFER;
                oConsulta.TIEMPO_ESPERA = oConfiguracion.TIEMPO_ESPERA;
                oConsulta.RUTA_WS = oConfiguracion.RUTA_WS;
                oConsulta.SIMULACION_WS = oConfiguracion.SIMULACION_WS;
                oConsulta.RESPUES_WS = oConfiguracion.RESPUES_WS;
                oConsulta.TIEMPO_ABRIR_BARRERA = oConfiguracion.TIEMPO_ABRIR_BARRERA;
                oConfiguracion.TIEMPO_PERMITIR_SALIR = oConfiguracion.TIEMPO_PERMITIR_SALIR;
                oConsulta.FILTRO_EPC = oConfiguracion.FILTRO_EPC;
                oContext.SaveChanges();
            return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

     
    }
}
