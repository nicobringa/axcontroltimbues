﻿Imports System.Threading
Imports Negocio
Imports Entidades.Constante

Imports Negocio.LectorRFID_SpeedWay
Public Class CtrlDinamismoDobleBarrera

#Region "PROPIEDADES"

#End Region


#Region "CONSTRUCTOR"

#End Region

#Region "EVENTOS FORM"

#End Region


#Region "METODOS"

    Public Sub SetBarreraEstado(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _BarreraEstado As BarreraEstado)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing

        If _Barrera = Entidades.Constante.BarreraDoble.barrera1 Then

            If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.ArribaManual Then

                picVisible = picBarrera1_Arriba
                picNoVisible = picBarrera1_Abajo
                modDelegado.setVisibleCtrl(Me, lblFallaB1, False)
            ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo

                picVisible = picBarrera1_Abajo
                picNoVisible = picBarrera1_Arriba
                modDelegado.setVisibleCtrl(Me, lblFallaB1, False)
            ElseIf _BarreraEstado = BarreraEstado.Falla Then

                picNoVisible = Nothing
                picVisible = Nothing
                modDelegado.setVisibleCtrl(Me, lblFallaB1, True)

            End If


        Else

            If _BarreraEstado = BarreraEstado.ArribaAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then

                picVisible = picBarrera2_Arriba
                picNoVisible = picBarrera2_Abajo
                modDelegado.setVisibleCtrl(Me, lblFallaB2, False)

            ElseIf _BarreraEstado = BarreraEstado.AbajoAuto Or _BarreraEstado = BarreraEstado.AbajoManual Then ' Abajo
                picVisible = picBarrera2_Abajo
                picNoVisible = picBarrera2_Arriba
                modDelegado.setVisibleCtrl(Me, lblFallaB2, False)
            ElseIf _BarreraEstado = BarreraEstado.Falla Then
                picNoVisible = Nothing
                picVisible = Nothing
                modDelegado.setVisibleCtrl(Me, lblFallaB2, True)
            End If

        End If

        If Not IsNothing(picVisible) And Not IsNothing(picNoVisible) Then
            modDelegado.setVisiblePic(Me, True, picVisible)
            modDelegado.setVisiblePic(Me, False, picNoVisible)
        End If

    End Sub

    Public Sub SetSensorEstado(ByVal _Sensor As Entidades.Constante.IngresoEgreso, ByVal _SensorEstado As Integer)
        Dim picVisible As PictureBox = Nothing
        Dim picNoVisible As PictureBox = Nothing


        If _Sensor = Entidades.Constante.IngresoEgreso.Ingreso Then

            If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                picNoVisible = picSensor_IngresoCortando
            ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
                picVisible = picSensor_IngresoCortando
            End If


            If Not IsNothing(picVisible) Then
                modDelegado.setVisiblePic(Me, True, picVisible)
            ElseIf Not IsNothing(picNoVisible) Then
                modDelegado.setVisiblePic(Me, False, picNoVisible)
            End If

        Else

            If _SensorEstado = SensorInfrarojoEstado.Sinprecencia Then
                picNoVisible = picSensorLecturaCortando
            ElseIf _SensorEstado = SensorInfrarojoEstado.ConPresencia Then
                picVisible = picSensorLecturaCortando
            End If

            If Not IsNothing(picVisible) Then
                modDelegado.setVisiblePic(Me, True, picVisible)
            ElseIf Not IsNothing(picNoVisible) Then
                modDelegado.setVisiblePic(Me, False, picNoVisible)
            End If
        End If





    End Sub


#End Region

#Region "SUBPROCESOS"

#End Region

End Class
