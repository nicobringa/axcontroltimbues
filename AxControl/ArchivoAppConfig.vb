﻿Imports System
Imports System.Configuration

Public Class ArchivoAppConfig

    Public Function LeerClave(ByVal StrNombreClave As String) As String
        Dim oArchivoAppConfig As Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath) 'Dim oConfiguracion As ConfigurationSettings = ConfigurationManager.OpenExeConfiguration(Application.StartupPath & "\WindowsApplication1.exe")

        ' Obtenemos el objeto AppSettingsSectiion
        Dim oAppSetting As AppSettingsSection = oArchivoAppConfig.AppSettings

        ' Leemos el valor del elemento Area
        Return oAppSetting.Settings.Item(StrNombreClave).Value
    End Function

    Public Function GuardarClave(ByVal StrNombreClave As String, ByVal StrValorClave As String) As String
        Dim oArchivoAppConfig As Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath) 'Dim oConfiguracion As ConfigurationSettings = ConfigurationManager.OpenExeConfiguration(Application.StartupPath & "\WindowsApplication1.exe")

        ' Obtengo el objeto AppSettingsSectiion
        Dim oAppSetting As AppSettingsSection = oArchivoAppConfig.AppSettings

        'Establezco el nuevo valor
        oAppSetting.Settings.Item(StrNombreClave).Value = StrValorClave

        ' Guardo los valores del objeto Configuration en el archivo de configuración XML actual.
        oArchivoAppConfig.Save(ConfigurationSaveMode.Modified)

        'Retorno el nuevo valor
        Return Me.LeerClave(StrNombreClave)
    End Function

    Public Function LeerPuestoTrabajo()

        Return LeerClave("Puesto_Trabajo")

    End Function

End Class
