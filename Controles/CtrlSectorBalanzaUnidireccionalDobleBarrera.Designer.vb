﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorBalanzaUnidireccionalDobleBarrera
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.S1_BARRERA = New Controles.CtrlSensor()
        Me.S2_BARRERA = New Controles.CtrlSensor()
        Me.BARRERA2 = New Controles.CtrlBarrera()
        Me.BARRERA1 = New Controles.CtrlBarrera()
        Me.btnLecturaManual = New System.Windows.Forms.Button()
        Me.CtrlAtenaLeyendo = New Controles.CtrlAntenaLeyendo()
        Me.BALANZA = New Controles.CtrlCabezalBalanza()
        Me.CARTEL_BALANZA = New Controles.CtrlCartelMultiLED()
        Me.CtrlTiempoEspera1 = New Controles.CtrlTiempoEspera()
        Me.S1_POSICION = New Controles.CtrlSensor()
        Me.S2_POSICION = New Controles.CtrlSensor()
        Me.SuspendLayout()
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(0, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(880, 43)
        Me.lblNombre.TabIndex = 170
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(674, 40)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 286)
        Me.CtrlPanelNotificaciones1.TabIndex = 169
        '
        'S1_BARRERA
        '
        Me.S1_BARRERA.BitMonitoreo = CType(0, Short)
        Me.S1_BARRERA.Estado = False
        Me.S1_BARRERA.GPIO = False
        Me.S1_BARRERA.ID_CONTROL_ACCESO = 0
        Me.S1_BARRERA.ID_PLC = 0
        Me.S1_BARRERA.ID_SECTOR = 0
        Me.S1_BARRERA.Inicializado = False
        Me.S1_BARRERA.Location = New System.Drawing.Point(133, 198)
        Me.S1_BARRERA.Name = "S1_BARRERA"
        Me.S1_BARRERA.patenteLeida = Nothing
        Me.S1_BARRERA.PLC = Nothing
        Me.S1_BARRERA.Size = New System.Drawing.Size(67, 45)
        Me.S1_BARRERA.TabIndex = 172
        Me.S1_BARRERA.tiempoPatenteLeidas = 0
        '
        'S2_BARRERA
        '
        Me.S2_BARRERA.BitMonitoreo = CType(0, Short)
        Me.S2_BARRERA.Estado = False
        Me.S2_BARRERA.GPIO = False
        Me.S2_BARRERA.ID_CONTROL_ACCESO = 0
        Me.S2_BARRERA.ID_PLC = 0
        Me.S2_BARRERA.ID_SECTOR = 0
        Me.S2_BARRERA.Inicializado = False
        Me.S2_BARRERA.Location = New System.Drawing.Point(541, 198)
        Me.S2_BARRERA.Name = "S2_BARRERA"
        Me.S2_BARRERA.patenteLeida = Nothing
        Me.S2_BARRERA.PLC = Nothing
        Me.S2_BARRERA.Size = New System.Drawing.Size(62, 45)
        Me.S2_BARRERA.TabIndex = 173
        Me.S2_BARRERA.tiempoPatenteLeidas = 0
        '
        'BARRERA2
        '
        Me.BARRERA2.BackColor = System.Drawing.Color.Transparent
        Me.BARRERA2.BitMonitoreo = CType(0, Short)
        Me.BARRERA2.Estado = False
        Me.BARRERA2.GPIO = False
        Me.BARRERA2.ID_CONTROL_ACCESO = 0
        Me.BARRERA2.ID_PLC = 0
        Me.BARRERA2.ID_SECTOR = 0
        Me.BARRERA2.Inicializado = False
        Me.BARRERA2.Location = New System.Drawing.Point(530, 249)
        Me.BARRERA2.Name = "BARRERA2"
        Me.BARRERA2.patenteLeida = Nothing
        Me.BARRERA2.PLC = Nothing
        Me.BARRERA2.Size = New System.Drawing.Size(73, 52)
        Me.BARRERA2.TabIndex = 174
        Me.BARRERA2.tiempoPatenteLeidas = 0
        '
        'BARRERA1
        '
        Me.BARRERA1.BackColor = System.Drawing.Color.Transparent
        Me.BARRERA1.BitMonitoreo = CType(0, Short)
        Me.BARRERA1.Estado = False
        Me.BARRERA1.GPIO = False
        Me.BARRERA1.ID_CONTROL_ACCESO = 0
        Me.BARRERA1.ID_PLC = 0
        Me.BARRERA1.ID_SECTOR = 0
        Me.BARRERA1.Inicializado = False
        Me.BARRERA1.Location = New System.Drawing.Point(127, 249)
        Me.BARRERA1.Name = "BARRERA1"
        Me.BARRERA1.patenteLeida = Nothing
        Me.BARRERA1.PLC = Nothing
        Me.BARRERA1.Size = New System.Drawing.Size(73, 52)
        Me.BARRERA1.TabIndex = 175
        Me.BARRERA1.tiempoPatenteLeidas = 0
        '
        'btnLecturaManual
        '
        Me.btnLecturaManual.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual.Location = New System.Drawing.Point(4, 124)
        Me.btnLecturaManual.Name = "btnLecturaManual"
        Me.btnLecturaManual.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual.TabIndex = 177
        Me.btnLecturaManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual.UseVisualStyleBackColor = False
        '
        'CtrlAtenaLeyendo
        '
        Me.CtrlAtenaLeyendo.Location = New System.Drawing.Point(3, 46)
        Me.CtrlAtenaLeyendo.Name = "CtrlAtenaLeyendo"
        Me.CtrlAtenaLeyendo.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendo.TabIndex = 176
        Me.CtrlAtenaLeyendo.UltTag = Nothing
        '
        'BALANZA
        '
        Me.BALANZA.BackColor = System.Drawing.Color.Gainsboro
        Me.BALANZA.BitMonitoreo = CType(0, Short)
        Me.BALANZA.GPIO = False
        Me.BALANZA.ID_CONTROL_ACCESO = 0
        Me.BALANZA.ID_PLC = 0
        Me.BALANZA.ID_SECTOR = 0
        Me.BALANZA.Inicializado = False
        Me.BALANZA.Location = New System.Drawing.Point(95, 46)
        Me.BALANZA.Name = "BALANZA"
        Me.BALANZA.numeroBalanza = 0
        Me.BALANZA.patenteLeida = Nothing
        Me.BALANZA.PLC = Nothing
        Me.BALANZA.Size = New System.Drawing.Size(290, 97)
        Me.BALANZA.TabIndex = 178
        Me.BALANZA.tiempoPatenteLeidas = 0
        '
        'CARTEL_BALANZA
        '
        Me.CARTEL_BALANZA.BitMonitoreo = CType(0, Short)
        Me.CARTEL_BALANZA.Conectado = False
        Me.CARTEL_BALANZA.GPIO = False
        Me.CARTEL_BALANZA.ID_CONTROL_ACCESO = 0
        Me.CARTEL_BALANZA.ID_PLC = 0
        Me.CARTEL_BALANZA.ID_SECTOR = 0
        Me.CARTEL_BALANZA.Inicializado = False
        Me.CARTEL_BALANZA.Location = New System.Drawing.Point(455, 46)
        Me.CARTEL_BALANZA.Name = "CARTEL_BALANZA"
        Me.CARTEL_BALANZA.patenteLeida = Nothing
        Me.CARTEL_BALANZA.PLC = Nothing
        Me.CARTEL_BALANZA.Size = New System.Drawing.Size(194, 100)
        Me.CARTEL_BALANZA.TabIndex = 179
        Me.CARTEL_BALANZA.Tag = "CARTEL_BALANZA"
        Me.CARTEL_BALANZA.tiempoPatenteLeidas = 0
        '
        'CtrlTiempoEspera1
        '
        Me.CtrlTiempoEspera1.BackColor = System.Drawing.Color.White
        Me.CtrlTiempoEspera1.Location = New System.Drawing.Point(390, 134)
        Me.CtrlTiempoEspera1.Name = "CtrlTiempoEspera1"
        Me.CtrlTiempoEspera1.SetVisible = False
        Me.CtrlTiempoEspera1.Size = New System.Drawing.Size(100, 59)
        Me.CtrlTiempoEspera1.TabIndex = 181
        Me.CtrlTiempoEspera1.Tiempo = Nothing
        '
        'S1_POSICION
        '
        Me.S1_POSICION.BitMonitoreo = CType(0, Short)
        Me.S1_POSICION.Estado = False
        Me.S1_POSICION.GPIO = False
        Me.S1_POSICION.ID_CONTROL_ACCESO = 0
        Me.S1_POSICION.ID_PLC = 0
        Me.S1_POSICION.ID_SECTOR = 0
        Me.S1_POSICION.Inicializado = False
        Me.S1_POSICION.Location = New System.Drawing.Point(206, 253)
        Me.S1_POSICION.Name = "S1_POSICION"
        Me.S1_POSICION.patenteLeida = Nothing
        Me.S1_POSICION.PLC = Nothing
        Me.S1_POSICION.Size = New System.Drawing.Size(64, 45)
        Me.S1_POSICION.TabIndex = 182
        Me.S1_POSICION.tiempoPatenteLeidas = 0
        '
        'S2_POSICION
        '
        Me.S2_POSICION.BitMonitoreo = CType(0, Short)
        Me.S2_POSICION.Estado = False
        Me.S2_POSICION.GPIO = False
        Me.S2_POSICION.ID_CONTROL_ACCESO = 0
        Me.S2_POSICION.ID_PLC = 0
        Me.S2_POSICION.ID_SECTOR = 0
        Me.S2_POSICION.Inicializado = False
        Me.S2_POSICION.Location = New System.Drawing.Point(455, 253)
        Me.S2_POSICION.Name = "S2_POSICION"
        Me.S2_POSICION.patenteLeida = Nothing
        Me.S2_POSICION.PLC = Nothing
        Me.S2_POSICION.Size = New System.Drawing.Size(69, 45)
        Me.S2_POSICION.TabIndex = 183
        Me.S2_POSICION.tiempoPatenteLeidas = 0
        '
        'CtrlSectorBalanzaUnidireccionalDobleBarrera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.S2_POSICION)
        Me.Controls.Add(Me.S1_POSICION)
        Me.Controls.Add(Me.CtrlTiempoEspera1)
        Me.Controls.Add(Me.CARTEL_BALANZA)
        Me.Controls.Add(Me.BALANZA)
        Me.Controls.Add(Me.btnLecturaManual)
        Me.Controls.Add(Me.CtrlAtenaLeyendo)
        Me.Controls.Add(Me.BARRERA1)
        Me.Controls.Add(Me.BARRERA2)
        Me.Controls.Add(Me.S2_BARRERA)
        Me.Controls.Add(Me.S1_BARRERA)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Name = "CtrlSectorBalanzaUnidireccionalDobleBarrera"
        Me.Size = New System.Drawing.Size(880, 326)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents S1_BARRERA As CtrlSensor
    Friend WithEvents S2_BARRERA As CtrlSensor
    Friend WithEvents BARRERA2 As CtrlBarrera
    Friend WithEvents BARRERA1 As CtrlBarrera
    Friend WithEvents btnLecturaManual As Button
    Friend WithEvents CtrlAtenaLeyendo As CtrlAntenaLeyendo
    Friend WithEvents BALANZA As CtrlCabezalBalanza
    Friend WithEvents CARTEL_BALANZA As CtrlCartelMultiLED
    Friend WithEvents CtrlTiempoEspera1 As CtrlTiempoEspera
    Friend WithEvents S1_POSICION As CtrlSensor
    Friend WithEvents S2_POSICION As CtrlSensor
End Class
