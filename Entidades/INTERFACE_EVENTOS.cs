//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class INTERFACE_EVENTOS
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public Nullable<int> TIPO_EVENTO { get; set; }
        public string MENSAJE { get; set; }
        public Nullable<long> ID_INTERFACE { get; set; }
    
        public virtual INTERFACE_WS INTERFACE_WS { get; set; }
        public virtual INTERFACE_TIPO_EVENTO INTERFACE_TIPO_EVENTO { get; set; }
    }
}
