﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlMenu
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.listAlarma = New System.Windows.Forms.ListView()
        Me.pbFacebook = New System.Windows.Forms.PictureBox()
        Me.pbAumax = New System.Windows.Forms.PictureBox()
        Me.btnMenu = New System.Windows.Forms.Button()
        Me.btnDriverPLC = New System.Windows.Forms.Button()
        Me.lblComputadora = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.lblTipoPuestoTrabajo = New System.Windows.Forms.Label()
        CType(Me.pbFacebook, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbAumax, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'listAlarma
        '
        Me.listAlarma.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.listAlarma.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.listAlarma.Location = New System.Drawing.Point(427, 0)
        Me.listAlarma.Name = "listAlarma"
        Me.listAlarma.Size = New System.Drawing.Size(918, 53)
        Me.listAlarma.TabIndex = 2
        Me.listAlarma.UseCompatibleStateImageBehavior = False
        '
        'pbFacebook
        '
        Me.pbFacebook.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbFacebook.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbFacebook.Image = Global.Controles.My.Resources.Resources._1463765917_1
        Me.pbFacebook.Location = New System.Drawing.Point(222, 0)
        Me.pbFacebook.Name = "pbFacebook"
        Me.pbFacebook.Size = New System.Drawing.Size(66, 53)
        Me.pbFacebook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbFacebook.TabIndex = 4
        Me.pbFacebook.TabStop = False
        '
        'pbAumax
        '
        Me.pbAumax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbAumax.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbAumax.Image = Global.Controles.My.Resources.Resources.AumaxT
        Me.pbAumax.Location = New System.Drawing.Point(148, 0)
        Me.pbAumax.Name = "pbAumax"
        Me.pbAumax.Size = New System.Drawing.Size(75, 53)
        Me.pbAumax.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbAumax.TabIndex = 3
        Me.pbAumax.TabStop = False
        '
        'btnMenu
        '
        Me.btnMenu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMenu.Image = Global.Controles.My.Resources.Resources.menu
        Me.btnMenu.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMenu.Location = New System.Drawing.Point(0, 0)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(75, 53)
        Me.btnMenu.TabIndex = 1
        Me.btnMenu.Text = "Menu"
        Me.btnMenu.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnMenu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMenu.UseVisualStyleBackColor = True
        '
        'btnDriverPLC
        '
        Me.btnDriverPLC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDriverPLC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDriverPLC.Image = Global.Controles.My.Resources.Resources.PLC1200_32
        Me.btnDriverPLC.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDriverPLC.Location = New System.Drawing.Point(74, 0)
        Me.btnDriverPLC.Name = "btnDriverPLC"
        Me.btnDriverPLC.Size = New System.Drawing.Size(75, 53)
        Me.btnDriverPLC.TabIndex = 0
        Me.btnDriverPLC.Text = "PLC"
        Me.btnDriverPLC.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDriverPLC.UseVisualStyleBackColor = True
        '
        'lblComputadora
        '
        Me.lblComputadora.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComputadora.Location = New System.Drawing.Point(289, 0)
        Me.lblComputadora.Name = "lblComputadora"
        Me.lblComputadora.Size = New System.Drawing.Size(156, 53)
        Me.lblComputadora.TabIndex = 6
        Me.lblComputadora.Text = "Computadora Cliente"
        Me.lblComputadora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(445, 31)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(14, 20)
        Me.lblVersion.TabIndex = 7
        Me.lblVersion.Text = "-"
        '
        'lblTipoPuestoTrabajo
        '
        Me.lblTipoPuestoTrabajo.AutoSize = True
        Me.lblTipoPuestoTrabajo.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblTipoPuestoTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoPuestoTrabajo.Location = New System.Drawing.Point(445, 4)
        Me.lblTipoPuestoTrabajo.Name = "lblTipoPuestoTrabajo"
        Me.lblTipoPuestoTrabajo.Size = New System.Drawing.Size(14, 20)
        Me.lblTipoPuestoTrabajo.TabIndex = 8
        Me.lblTipoPuestoTrabajo.Text = "-"
        '
        'CtrlMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(166, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.Controls.Add(Me.lblTipoPuestoTrabajo)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblComputadora)
        Me.Controls.Add(Me.pbFacebook)
        Me.Controls.Add(Me.pbAumax)
        Me.Controls.Add(Me.listAlarma)
        Me.Controls.Add(Me.btnMenu)
        Me.Controls.Add(Me.btnDriverPLC)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "CtrlMenu"
        Me.Size = New System.Drawing.Size(1345, 53)
        CType(Me.pbFacebook, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbAumax, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnDriverPLC As System.Windows.Forms.Button
    Friend WithEvents btnMenu As System.Windows.Forms.Button
    Friend WithEvents listAlarma As System.Windows.Forms.ListView
    Friend WithEvents pbAumax As System.Windows.Forms.PictureBox
    Friend WithEvents pbFacebook As System.Windows.Forms.PictureBox
    Friend WithEvents lblComputadora As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents lblTipoPuestoTrabajo As Label
End Class
