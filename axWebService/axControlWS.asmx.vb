﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports Controles



' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://axControl.com/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class axControlWS
    Inherits System.Web.Services.WebService


    Public Sub New()

    End Sub


    'CARTELES LED
    Private WithEvents nCartelBal1Bar1 As Negocio.CartelMultiledN
    Private WithEvents nCartelBal2Bar1 As Negocio.CartelMultiledN
    Private WithEvents nCartelBal1Bar2 As Negocio.CartelMultiledN
    Private WithEvents nCartelBal2Bar2 As Negocio.CartelMultiLedN

    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hola a todos"
    'End Function

    '#Region "Web Service"



    ''' <summary>
    ''' WS Que permite operar abrir o cerrar una barrera manejada por el sistema de aumax
    ''' </summary>
    ''' <param name="idSector">Identificador de sector que se quiere operar en forma manual</param>
    ''' <param name="operacion">Operdacion manual a realizar 0= abrir 1 =Cerrar </param>
    ''' <param name="usuario">nombre  de usuario de que realiza la operacion</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod()>
    Public Function OperBarreraManual(ByVal idSector As Integer, ByVal operacion As Integer, ByVal usuario As String) As Boolean

        Dim mensaje As String
        Dim comandoBarrera As New Constante.BarreraComando
        Dim comandoAuditoria As New acciones
        If operacion = 0 Then
            mensaje = "Subo Barrera Manual"
            comandoBarrera = BarreraComando.SubirManual
        Else
            mensaje = "Bajo barrera Manual"
            comandoBarrera = BarreraComando.BajarManual
        End If
        Negocio.AuditoriaN.AddAuditoria(mensaje, "", idSector, 0, usuario, Constante.acciones.WS_OperBarreraManual, 0)
        Dim TAG As String = "[OperBarreraManual]"
        Dim idAuditoriaWS As Integer = 0 'AuditoriaOperBarreraManual(idSector, operacion, usuario)
        Dim PuestoTrabajo As Entidades.Constante.PuestoTrabajo = GetPuestoTRabajoBarrera(idSector)

        If PuestoTrabajo = Constante.PuestoTrabajo.Porteria Then 'PORTERIA
            'Dim nPorteria As New Negocio.PorteriaN
            Dim oSector As New Entidades.Sector
            Dim oSectorN As New Negocio.SectorN
            oSector = oSectorN.GetOne(idSector)
            Dim oControlAcc As New Entidades.Control_Acceso
            Dim oControlAccN As New Negocio.ControlAccesoN
            oControlAcc = oControlAccN.GetOne(oSector.ID_CONTROL_ACCESO)
            Dim control As New Controles.CtrlControlAccesoPorteria(oControlAcc.ID_CONTROL_ACCESO, oControlAcc.ID_PLC)
            control.InicializarWS()
            If control.B_INGRESO.ID_SECTOR = idSector Then

                control.B_INGRESO.EjectComando(comandoBarrera)

                If comandoBarrera = BarreraComando.BajarManual Then
                    comandoAuditoria = acciones.bajoBarreraIngreso
                Else
                    comandoAuditoria = acciones.suboBarreraIngreso
                End If
            Else

                control.B_EGRESO.EjectComando(comandoBarrera)

                If comandoBarrera = BarreraComando.BajarManual Then
                    comandoAuditoria = acciones.bajoBarreraEgreso
                Else
                    comandoAuditoria = acciones.suboBarreraEgreso
                End If
            End If
            Negocio.AuditoriaN.AddAuditoria(mensaje, "", idSector, 0, usuario, comandoAuditoria, 0)
            'Return nPorteria.OperBarreraManual(idSector, operacion, usuario, idAuditoriaWS)
        ElseIf PuestoTrabajo = Constante.PuestoTrabajo.Balanzas Then
            'Dim nBalanza As New Negocio.BalanzaN
            Dim contrlBalanza As New Controles.CtrlControlAccesoBalanza()
            contrlBalanza.InicializarWS()

            If contrlBalanza.BAL1_BAR1.ID_SECTOR = idSector Then
                contrlBalanza.BAL1_BAR1.EjectComando(comandoBarrera)
                If comandoBarrera = BarreraComando.BajarManual Then
                    mensaje = "Bajo Barrera 1 de la Balanza 1"
                    comandoAuditoria = acciones.bajoBarreraIngreso
                Else
                    mensaje = "Subo Barrera 1 de la Balanza 1"
                    comandoAuditoria = acciones.suboBarreraIngreso
                End If
            ElseIf contrlBalanza.BAL1_BAR2.ID_SECTOR = idSector Then
                contrlBalanza.BAL1_BAR2.EjectComando(comandoBarrera)
                If comandoBarrera = BarreraComando.BajarManual Then
                    mensaje = "Bajo Barrera 2 de la Balanza 1"
                    comandoAuditoria = acciones.bajoBarreraEgreso
                Else
                    mensaje = "Subo Barrera 2 de la Balanza 1"
                    comandoAuditoria = acciones.suboBarreraEgreso
                End If
            ElseIf contrlBalanza.BAL2_BAR1.ID_SECTOR = idSector Then
                contrlBalanza.BAL2_BAR1.EjectComando(comandoBarrera)
                If comandoBarrera = BarreraComando.BajarManual Then
                    mensaje = "Bajo Barrera 1 de la Balanza 2"
                    comandoAuditoria = acciones.bajoBarreraIngreso
                Else
                    mensaje = "Subo Barrera 1 de la Balanza 2"
                    comandoAuditoria = acciones.suboBarreraIngreso
                End If
            ElseIf contrlBalanza.BAL2_BAR2.ID_SECTOR = idSector Then
                contrlBalanza.BAL2_BAR1.EjectComando(comandoBarrera)
                If comandoBarrera = BarreraComando.BajarManual Then
                    mensaje = "Bajo Barrera 2 de la Balanza 2"
                    comandoAuditoria = acciones.bajoBarreraEgreso
                Else
                    mensaje = "Subo Barrera 2 de la Balanza 2"
                    comandoAuditoria = acciones.suboBarreraEgreso
                End If
            End If

            Negocio.AuditoriaN.AddAuditoria(mensaje, "", idSector, 0, usuario, comandoAuditoria, 0)
            'Return nBalanza.OperBarreraManual(idSector, operacion, usuario, idAuditoriaWS)
            ' Else 'Barrera Falsa
            ' Dim nBarreraFalsa As New Negocio.BarreraFalsaN
            ' Return nBarreraFalsa.OperBarreraManual(idSector, operacion, usuario, idAuditoriaWS)
        End If

        Return True

    End Function

    ''' <summary>
    ''' WS Que permite realizar una habilitado manual(como si se hubiera echo la lectura y el sistema ERP dio el habilitado)
    ''' </summary>
    ''' <param name="idSector">Identificador del sector que se quiere realizar operacion</param>
    ''' <param name="idTransaccion">Identificador del sector a la cual se le quiere aplicar un habilitado manual</param>
    ''' <param name="tagRFID">el Tag RFID del camion</param>
    ''' <param name="mensaje">el mensaje que se quiera mostrar en el cartel</param>
    ''' <param name="usuario">Mensaje que se quiere </param>
    ''' <returns>TRUE(el habilitado se realizo con existo) O FALSE(no se puedo realizar el la operacion del habilitado) </returns>
    ''' <remarks></remarks>
    <WebMethod()>
    Public Function EstaHabilitadoManual(ByVal idSector As Integer, ByVal idTransaccion As Long, ByVal tagRFID As String, ByVal mensaje As String, ByVal usuario As String) As Boolean


        Try
            Negocio.AuditoriaN.AddAuditoria(mensaje, tagRFID, idSector, idTransaccion, WS_ERRO_USUARIO, Constante.acciones.WS_HabilitadoManual, 0)
            Dim comandoBarrera As New Constante.BarreraComando
            Dim PuestoTrabajo As Entidades.Constante.PuestoTrabajo = GetPuestoTRabajoBarrera(idSector)
            If PuestoTrabajo = Constante.PuestoTrabajo.Porteria Then

                Dim oSector As New Entidades.Sector
                Dim oSectorN As New Negocio.SectorN
                oSector = oSectorN.GetOne(idSector)
                Dim oControlAcc As New Entidades.Control_Acceso
                Dim oControlAccN As New Negocio.ControlAccesoN
                oControlAcc = oControlAccN.GetOne(oSector.ID_CONTROL_ACCESO)
                Dim control As New Controles.CtrlControlAccesoPorteria(oControlAcc.ID_CONTROL_ACCESO, oControlAcc.ID_PLC)
                control.InicializarWS()

                comandoBarrera = Constante.BarreraComando.PermitirIngresar
                If control.B_INGRESO.ID_SECTOR = idSector Then
                    control.B_INGRESO.EjectComando(comandoBarrera)
                Else
                    control.B_EGRESO.EjectComando(comandoBarrera)
                End If

                Negocio.AuditoriaN.AddAuditoria(mensaje, "", idSector, 0, usuario, Constante.acciones.WS_EstaHabilitado, 0)
            ElseIf PuestoTrabajo = Constante.PuestoTrabajo.Balanzas Then
                Dim contrlBalanza As New Controles.CtrlControlAccesoBalanza()
                contrlBalanza.InicializarWS()
                comandoBarrera = Constante.BarreraComando.PermitirIngresar
                If contrlBalanza.BAL1_BAR1.ID_SECTOR = idSector Then
                    contrlBalanza.BAL1_BAR1.EjectComando(comandoBarrera)
                    mensaje = "Subo Barrera 1 de la Balanza 1"
                ElseIf contrlBalanza.BAL1_BAR2.ID_SECTOR = idSector Then
                    contrlBalanza.BAL1_BAR2.EjectComando(comandoBarrera)
                    mensaje = "Subo Barrera 2 de la Balanza 1"
                ElseIf contrlBalanza.BAL2_BAR1.ID_SECTOR = idSector Then
                    contrlBalanza.BAL2_BAR1.EjectComando(comandoBarrera)
                    mensaje = "Subo Barrera 1 de la Balanza 2"
                ElseIf contrlBalanza.BAL2_BAR2.ID_SECTOR = idSector Then
                    contrlBalanza.BAL2_BAR1.EjectComando(comandoBarrera)
                    mensaje = "Subo Barrera 2 de la Balanza 2"
                End If

                Negocio.AuditoriaN.AddAuditoria(mensaje, tagRFID, idSector, idTransaccion, usuario, Constante.acciones.WS_EstaHabilitado, 0)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try



    End Function

    ''' <summary>
    ''' Permite obtener el peso para registrar  en el sistema de forma manual
    ''' en el caso que no sea un peso confiable devuelve error
    ''' </summary>
    ''' <param name="idSector">sector de balanaza que identifica a la balanza que se quiere consultar el peso</param>
    ''' <param name="pesoEstable"> envio si voy a tomar o no el peso estable</param>
    ''' <param name="usuario">Usuario que solicita el peso de la balanza</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod()>
    Public Function GetPesoBalanza(ByVal idSector As Integer, ByVal pesoEstable As Boolean, ByVal usuario As String) As String
        Dim TAG As String = "[GetPesoBalanza]"
        Dim nBalanza As New Negocio.BalanzaN


        Try
            Dim Balanza As ConstanteBalanza.Balanzas = nBalanza.GetNumBalanza(idSector)

            Dim DescripcionAuditoria As String = String.Format("Se solicito el WS GetPesoBalanza Balanza {0} ", Balanza.ToString)
            ''Auditoria de que solicitaron el peso de la balanza
            Negocio.AuditoriaN.AddAuditoria(DescripcionAuditoria, "", idSector, 0, usuario, Constante.acciones.WS_GetPesoBalanza)

            Dim strPesoBalanza As String

            ''Verifico primero que no esté en falla

            If pesoEstable Then 'Si esta pidiendo un peso estable , verifica que la balanza no este en movimiento o en error
                    strPesoBalanza = If(nBalanza.controloFalla(idSector), "ERROR", nBalanza.GetPesoBalanza(Balanza))

                Else ' de lo contrario toma el peso que esta viendo el PLC
                strPesoBalanza = nBalanza.GetPesoBalanza(Balanza)
            End If

            Return strPesoBalanza

            ''Return "1000"
        Catch ex As Exception

            Return ex.Message
        End Try


    End Function

    <WebMethod()>
    Public Function PermitirSalirManual(ByVal idSector As Integer, ByVal mensaje As String, ByVal usuario As String) As Boolean


        Try

            Dim TAG As String = "[SalirBalanza]"
            Dim nBalanza As New Negocio.BalanzaN
            Dim Balanza As Entidades.ConstanteBalanza.Balanzas = nBalanza.GetNumBalanza(idSector)

            Dim oCartel As New CARTEL_MULTILED
            Dim oCartelN As New Negocio.CartelMultiLedN

            Dim sectorBal As Integer
            If Balanza = Constante.numeroBalanza.Balanza1 Then
                sectorBal = Constante.BarrerasBalanza.barreraIngresoBalanza1
            Else
                sectorBal = Constante.BarrerasBalanza.barreraIngresoBalanza2
            End If



            ''Escribir mensaje en los carteles



            ''Auditoria del peso enviado
            Dim DescripcionAuditoria As String = String.Format("WsSalirBalanza {0}", "Se ejecuto web service SalirBalanza")

            Dim oControlAcc As New Entidades.Control_Acceso
            Dim oControlAccN As New Negocio.ControlAccesoN


            Dim oSector As New Entidades.Sector
            Dim oSectorN As New Negocio.SectorN

            oSector = oSectorN.GetOne(idSector)

            oControlAcc = oControlAccN.GetOne(oSector.ID_CONTROL_ACCESO)
            Dim comando As New Constante.BalanzaComando
            Dim controlBal As New Controles.CtrlControlAccesoBalanza(oSector.ID_CONTROL_ACCESO, oControlAcc.ID_PLC)
            controlBal.InicializarWS()

            comando = Entidades.Constante.BalanzaComando.PermitirSalir

            If Balanza = Constante.numeroBalanza.Balanza1 Then
                controlBal.BALANZA1.EjectComando(comando)
            Else
                controlBal.BALANZA2.EjectComando(comando)
            End If
            ''controlBal.EjectComando(comando)

            'Dim cartelN As New Negocio.CartelMultiLedN
            'cartelN.EscribirCartel(mensaje, oCartel.ip, oCartel.port)

            Return True
        Catch ex As Exception

            Return False
        End Try

    End Function


    ''' <summary>
    ''' Permite obtener el ultimo tag leido de un sector
    ''' </summary>
    ''' <param name="idSector"></param>
    ''' <param name="Usuario"></param>
    ''' <param name="idTransaccion"></param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function TagLeido(idSector As Integer, Usuario As String, idTransaccion As Long) As String


        Dim oTag As Entidades.Ultimo_Tag_Leido
        Dim oTagN As New Negocio.Ult_Tag_LeidoN
        Dim ulTagLeido As String
        oTag = oTagN.GetLastTag(idSector)

        If IsNothing(oTag) Then
            ulTagLeido = " "
        Else
            ulTagLeido = oTag.TAG_RFID
        End If
        Dim mensaje As String = "Busco el último tag leido del sector: " & idSector & " Tag leido: " & ulTagLeido
        Negocio.AuditoriaN.AddAuditoria(mensaje, ulTagLeido, idSector, idTransaccion, Usuario, acciones.SinConexionRFID, 0)
        Return ulTagLeido


    End Function

    '#End Region

    '#Region "Metodos"
    Private Function AuditoriaOperBarreraManual(ByVal idSector As Integer, ByVal operacion As Integer, ByVal usuario As String) As Integer

        Dim textOperacion As String = If(operacion = Entidades.Constante.OperacionBarrera.SubirBarrera, "Subir Barrera", "Bajar Barrera")

        Dim DescripcionAuditoria As String = "Se Solicito Operacion Manual de Barrera IdBarrera = " & idSector & " Operacion = " & textOperacion

        Return Negocio.AuditoriaN.AddAuditoria(DescripcionAuditoria, "", idSector, 0, usuario, Constante.acciones.WS_OperBarreraManual).ID_AUDITORIA

    End Function



    Private Function GetPuestoTRabajoBarrera(ByVal idSector As Integer) As Entidades.Constante.PuestoTrabajo

        Dim oSector As New Entidades.Sector
        Dim oSectorN As New Negocio.SectorN

        oSector = oSectorN.GetOne(idSector)

        If Not IsNothing(oSector) Then
            If oSector.nombre.Contains("Porteria") Then
                Return Entidades.Constante.PuestoTrabajo.Porteria
            ElseIf oSector.nombre.Contains("Balanza") Then
                Return Entidades.Constante.PuestoTrabajo.Balanzas
            Else 'Barrera Falsa
                Return Entidades.Constante.PuestoTrabajo.BarreraFalse
            End If
        End If


    End Function


    <WebMethod()>
    Public Function EscribirCartelLED(idSector As Integer, idComando As Integer, Mensaje As String) As Boolean
        Try
            Dim nControlAcceso As New Negocio.ControlAccesoN
            Dim idContAcc As Integer = nControlAcceso.GetIdControlConIdSector(idSector)

            Dim oComando As New Entidades.COMANDO
            oComando.ID_CONTROL = idContAcc
            oComando.ID_SECTOR = idSector
            oComando.TAG = "EscribirCartel." + idComando.ToString()
            oComando.PROCESADO = 0
            oComando.MENSAJE = Mensaje
            Dim nComando As New Negocio.ComandoN

            Return nComando.AgregarComando(oComando)
        Catch ex As Exception
            Return False
        End Try
    End Function

    <WebMethod()>
    Public Function ComandoSemaforo(idSector As Integer, idComando As Integer) As Boolean
        Try
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim oListaDatoWordInt As List(Of DATO_WORDINT) = nDatoWord.GetAllPorSector(idSector)

            If Not IsNothing(oListaDatoWordInt) Then
                Dim oSemaforoComando As DATO_WORDINT = (From a In oListaDatoWordInt Where a.TAG.Contains("SEMAFORO") And a.TAG.Contains(".Comando") Order By a.TAG).FirstOrDefault()
                If Not IsNothing(oSemaforoComando) Then
                    oSemaforoComando.VALOR = idComando
                    oSemaforoComando.FLAG_RW = 1
                    nDatoWord.Update(oSemaforoComando)
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class