﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlConfRFID_Invengo
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboReaderMode = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.numUpDwTiempoLectura = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboPowerDbm = New System.Windows.Forms.ComboBox()
        Me.btnAgrConfiguracion = New System.Windows.Forms.Button()
        CType(Me.numUpDwTiempoLectura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(68, 153)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 13)
        Me.Label8.TabIndex = 42
        Me.Label8.Text = "Reader Mode"
        '
        'cboReaderMode
        '
        Me.cboReaderMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReaderMode.Enabled = False
        Me.cboReaderMode.FormattingEnabled = True
        Me.cboReaderMode.Items.AddRange(New Object() {"EPC_6C", "TID_6C", "EPC_TID_UserData_6C", "EPC_TID_UserData_6C_2", "ID_6B", "ID_UserData_6B", "EPC_6C_ID_6B", "TID_6C_ID_6B", "EPC_TID_UserData_6C_ID_UserData_6B", "EPC_TID_UserData_Reserved_6C_ID_UserData_6B", "EPC_PC_6C"})
        Me.cboReaderMode.Location = New System.Drawing.Point(146, 150)
        Me.cboReaderMode.Name = "cboReaderMode"
        Me.cboReaderMode.Size = New System.Drawing.Size(267, 21)
        Me.cboReaderMode.TabIndex = 41
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(68, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "Nombre"
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(146, 93)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(267, 22)
        Me.txtNombre.TabIndex = 40
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(430, 98)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 13)
        Me.Label1.TabIndex = 43
        Me.Label1.Text = "Tiempo de lectura de tag repetido:"
        '
        'numUpDwTiempoLectura
        '
        Me.numUpDwTiempoLectura.Location = New System.Drawing.Point(605, 96)
        Me.numUpDwTiempoLectura.Name = "numUpDwTiempoLectura"
        Me.numUpDwTiempoLectura.Size = New System.Drawing.Size(60, 20)
        Me.numUpDwTiempoLectura.TabIndex = 44
        Me.numUpDwTiempoLectura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(671, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "segundos."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(430, 153)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Power (dbm)"
        '
        'cboPowerDbm
        '
        Me.cboPowerDbm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPowerDbm.Enabled = False
        Me.cboPowerDbm.FormattingEnabled = True
        Me.cboPowerDbm.Location = New System.Drawing.Point(547, 150)
        Me.cboPowerDbm.Name = "cboPowerDbm"
        Me.cboPowerDbm.Size = New System.Drawing.Size(180, 21)
        Me.cboPowerDbm.TabIndex = 46
        '
        'btnAgrConfiguracion
        '
        Me.btnAgrConfiguracion.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAgrConfiguracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgrConfiguracion.ForeColor = System.Drawing.Color.Black
        Me.btnAgrConfiguracion.Location = New System.Drawing.Point(663, 222)
        Me.btnAgrConfiguracion.Name = "btnAgrConfiguracion"
        Me.btnAgrConfiguracion.Size = New System.Drawing.Size(165, 33)
        Me.btnAgrConfiguracion.TabIndex = 73
        Me.btnAgrConfiguracion.Text = "Agregar Configuración"
        Me.btnAgrConfiguracion.UseVisualStyleBackColor = False
        '
        'CtrlConfRFID_Invengo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboPowerDbm)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.numUpDwTiempoLectura)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboReaderMode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.btnAgrConfiguracion)
        Me.Name = "CtrlConfRFID_Invengo"
        Me.Size = New System.Drawing.Size(856, 277)
        CType(Me.numUpDwTiempoLectura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label8 As Label
    Friend WithEvents cboReaderMode As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents numUpDwTiempoLectura As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cboPowerDbm As ComboBox
    Friend WithEvents btnAgrConfiguracion As Button
End Class
