﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlPanelNotificaciones
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBorrarNotificaciones = New System.Windows.Forms.Button()
        Me.flpNotificacion = New System.Windows.Forms.FlowLayoutPanel()
        Me.SuspendLayout()
        '
        'btnBorrarNotificaciones
        '
        Me.btnBorrarNotificaciones.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBorrarNotificaciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnBorrarNotificaciones.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnBorrarNotificaciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBorrarNotificaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBorrarNotificaciones.ForeColor = System.Drawing.Color.White
        Me.btnBorrarNotificaciones.Location = New System.Drawing.Point(-1, 3)
        Me.btnBorrarNotificaciones.Name = "btnBorrarNotificaciones"
        Me.btnBorrarNotificaciones.Size = New System.Drawing.Size(294, 32)
        Me.btnBorrarNotificaciones.TabIndex = 7
        Me.btnBorrarNotificaciones.Text = "Borrar Notificaciones"
        Me.btnBorrarNotificaciones.UseVisualStyleBackColor = False
        '
        'flpNotificacion
        '
        Me.flpNotificacion.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flpNotificacion.AutoScroll = True
        Me.flpNotificacion.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.flpNotificacion.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp
        Me.flpNotificacion.Location = New System.Drawing.Point(-1, 35)
        Me.flpNotificacion.Margin = New System.Windows.Forms.Padding(0)
        Me.flpNotificacion.Name = "flpNotificacion"
        Me.flpNotificacion.Size = New System.Drawing.Size(293, 543)
        Me.flpNotificacion.TabIndex = 49
        Me.flpNotificacion.WrapContents = False
        '
        'CtrlPanelNotificaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.Controls.Add(Me.flpNotificacion)
        Me.Controls.Add(Me.btnBorrarNotificaciones)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "CtrlPanelNotificaciones"
        Me.Size = New System.Drawing.Size(294, 578)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnBorrarNotificaciones As System.Windows.Forms.Button
    Friend WithEvents flpNotificacion As System.Windows.Forms.FlowLayoutPanel

End Class
