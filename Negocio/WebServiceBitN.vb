﻿Imports Entidades.Constante
Imports Newtonsoft.Json

Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization

Public Class WebServiceBitN

    Private Shared aux As String

    Private Shared NombreArchivo As String = "ConfWebService"

    Public Shared DireccionArchivo As String = Entidades.Constante.CARPETA_CONFIGURACION

    Public Shared TAG As String = "WebServiceErroN"
    'Private Shared _wsPlantaErro As Negocio.WsPlantaErro.PlantaErroService

    ''' <summary>
    ''' WebService que se ejecuta cuando pregunta si el tag leido esta habilitado a pasar por el sector
    ''' </summary>
    ''' <param name="IdSector">sector que leyo el tag</param>
    ''' <param name="TagRFID">Tag RFID leido</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function HabilitarTag(ByVal IdSector As Integer, ByVal TagRFID As String, Optional SimularAxRespuesta As Boolean = False) As Entidades.AxRespuesta

        '_wsPlantaErro = New Negocio.WsPlantaErro.PlantaErroService
        Try

            Dim DescrAuditoria As String

            DescrAuditoria = String.Format("EstaHabilitado Enviado: idSector = {0} TagRFID = {1}", IdSector, TagRFID)

            Negocio.AuditoriaN.AddAuditoria(DescrAuditoria, TagRFID, IdSector, 0, Entidades.Constante.USUARIO_AUMAX, acciones.WS_EstaHabilitado)


            Dim wsHabilitar As New Negocio.WSBitReference.Servicio_Automata
            Dim oHabilitar = New Negocio.WSBitReference.reqHabilitar()
            oHabilitar.IdSector = IdSector
            oHabilitar.TAG = TagRFID

            Dim Respuesta As String

            Dim oConfiguracion As New Entidades.CONFIGURACION
            Dim oConfiguracionN As New Negocio.ConfiguracionN
            oConfiguracion = oConfiguracionN.GetOne()

            wsHabilitar.Url = oConfiguracion.RUTA_WS

            If oConfiguracion.SIMULACION_WS Then
                Respuesta = SimulacionAxRespuesta(IdSector, True, "Simulación wsEstaHabilitado", 226, TagRFID)
            Else
                Respuesta = wsHabilitar.HabilitarTag(oHabilitar)
            End If

            'Convierto el string JSON AL OBJETO AxRepuesta
            Dim axRespuesta As Entidades.AxRespuesta = JsonConvert.DeserializeObject(Of Entidades.AxRespuesta)(Respuesta)

            DescrAuditoria = String.Format("AxRespuesta: {0} ", axRespuesta.ToString())

            Negocio.AuditoriaN.AddAuditoria(DescrAuditoria, TagRFID, IdSector, axRespuesta.idTransaccion, Entidades.Constante.USUARIO_AUMAX, acciones.WS_EstaHabilitado)


            Return axRespuesta

        Catch ex As Exception
            'axLog.AxLog.e(TagRFID, ex.Message)
            Throw New Exception(ex.Message)
        End Try


    End Function

    '''' <summary>
    '''' WebService que se ejecuta cuando se ah obtenido el peso de la balanza y 
    '''' </summary>
    '''' <param name="IdSector">sector que se obtuvo el peso</param>
    '''' <param name="TagRFID">tag del camion que esta pesando</param>
    '''' <param name="Peso"></param>
    '''' <param name="IdTrasaccion">idTrasaccion del sistema Administrativo, del camion que esta pesando</param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    Public Shared Function RegistrarPesada(ByVal IdSector As Integer, ByVal TagRFID As String, ByVal Peso As String, ByVal IdTrasaccion As String, Optional SimularAxRespuesta As Boolean = False) As Entidades.AxRespuesta

        Dim DescrAuditoria As String

        DescrAuditoria = String.Format("PesoObtendido Enviado  : idSector = {0} TagRFID: = {1} Peso = {2} idTrasaccion = {3} ", IdSector, TagRFID, Peso, IdTrasaccion)

        Negocio.AuditoriaN.AddAuditoria(DescrAuditoria, TagRFID, IdSector, IdTrasaccion, Entidades.Constante.USUARIO_AUMAX, acciones.WS_PesoObtenido)

        Dim wsPesaje As New Negocio.WSBitReference.Servicio_Automata
        Dim oPesar = New Negocio.WSBitReference.reqPesada()
        oPesar.IdSector = IdSector
        oPesar.IdTransaccion = IdTrasaccion
        oPesar.Peso = Peso
        Dim Respuesta As String

        Dim oConfiguracion As New Entidades.CONFIGURACION
        Dim oConfiguracionN As New Negocio.ConfiguracionN
        oConfiguracion = oConfiguracionN.GetOne()

        wsPesaje.Url = oConfiguracion.RUTA_WS

        If oConfiguracion.SIMULACION_WS Then
            Respuesta = SimulacionAxRespuesta(IdSector, True, "PESO 44700 FILA 5", 226, TagRFID)
        Else
            'Ejecuto el WebService registrar pesada, y obtengo el axRespuesta en formato json
            Respuesta = wsPesaje.RegistrarPesada(oPesar)
        End If

        Dim axRespuesta As Entidades.AxRespuesta = JsonConvert.DeserializeObject(Of Entidades.AxRespuesta)(Respuesta)

        DescrAuditoria = String.Format("AxRespuesta: {0} ", axRespuesta.ToString())

        Negocio.AuditoriaN.AddAuditoria(DescrAuditoria, TagRFID, IdSector, axRespuesta.idTransaccion, Entidades.Constante.USUARIO_AUMAX, acciones.WS_PesoObtenido)

        Return axRespuesta

    End Function

    ''' <summary>
    ''' Permite ejecutar el ws RtaAltaModSilo creado por BIT
    ''' </summary>
    ''' <param name="jsonRtaAltaModSilo"></param>
    ''' <returns></returns>
    Public Shared Function RtaAltaModSilo(jsonRtaAltaModSilo As String) As String
        Dim oConfiguracion As New Entidades.CONFIGURACION
        Dim oConfiguracionN As New Negocio.ConfiguracionN
        oConfiguracion = oConfiguracionN.GetOne()
        Dim wsBit As New Negocio.WSBitReference.Servicio_Automata

        If Not oConfiguracion.SIMULACION_WS Then ' SI NO ESTOY SIMULANDO LOS WS
            Return wsBit.RtaAltaModSilo(jsonRtaAltaModSilo)
        Else
            Return "OK"
        End If


        '  Dim reqRtaAltaModSilo As New Negocio.WSBitReference.req

    End Function

    ''' <summary>
    ''' Permite ejecutar el ws RtaAltaModSilo creado por BIT
    ''' </summary>
    ''' <param name="jsonEventoOTDesc"></param>
    ''' <returns></returns>
    Public Shared Function EventoOTDesc(jsonEventoOTDesc As String) As String
        Dim oConfiguracion As New Entidades.CONFIGURACION
        Dim oConfiguracionN As New Negocio.ConfiguracionN
        oConfiguracion = oConfiguracionN.GetOne()
        Dim wsBit As New Negocio.WSBitReference.Servicio_Automata

        If Not oConfiguracion.SIMULACION_WS Then ' SI NO ESTOY SIMULANDO LOS WS
            Return wsBit.EventoOTDesc(jsonEventoOTDesc)
        Else
            Return "OK"
        End If




    End Function

    Public Shared Function AlarmaSilo(jsonAlarmaSilo As String) As String
        Dim oConfiguracion As New Entidades.CONFIGURACION
        Dim oConfiguracionN As New Negocio.ConfiguracionN
        oConfiguracion = oConfiguracionN.GetOne()
        Dim wsBit As New Negocio.WSBitReference.Servicio_Automata

        If Not oConfiguracion.SIMULACION_WS Then ' SI NO ESTOY SIMULANDO LOS WS
            Return wsBit.AlarmaSilo(jsonAlarmaSilo)
        Else
            Return IIf(oConfiguracion.RESPUES_WS = True, "OK", "") ' rentorno ok en al simulación
        End If

    End Function

    '''' <summary>
    '''' Permite ejecutar el ws RtaAltaModSilo creado por BIT
    '''' </summary>
    '''' <param name="jsonRtaAltaModSilo"></param>
    '''' <returns></returns>
    'Public Shared Function EventoOTDesc(jsonEventoOTDesc As String) As String
    '    Dim oConfiguracion As New Entidades.CONFIGURACION
    '    Dim oConfiguracionN As New Negocio.ConfiguracionN
    '    oConfiguracion = oConfiguracionN.GetOne()
    '    Dim wsBit As New Negocio.WSBitReference.Servicio_Automata

    '    If Not oConfiguracion.SIMULACION_WS Then ' SI NO ESTOY SIMULANDO LOS WS
    '        Return wsBit.eve(jsonEventoOTDesc)
    '    Else
    '        Return "OK"
    '    End If


    '    '  Dim reqRtaAltaModSilo As New Negocio.WSBitReference.req

    'End Function



    Private Shared Function SimulacionAxRespuesta(idSector As Integer, habilitado As Boolean, mensaje As String, idTrasaccion As Integer, TagRFID As String) As String
        Dim _axRespuesta As New Entidades.AxRespuesta(idSector, habilitado, mensaje, idTrasaccion)
        _axRespuesta.tagRFID = TagRFID
        _axRespuesta.motivo = 1

        Dim oDatosDescarga As New Entidades.DatosDescarga()
        oDatosDescarga.calidad = "PRUEBA AUMAX"
        oDatosDescarga.idSector = idSector
        oDatosDescarga.patenteAcoplado = "AAA 226"
        oDatosDescarga.patenteChasis = "XXX 226"
        oDatosDescarga.producto = "Producto aumax"
        oDatosDescarga.motivo = 1
        oDatosDescarga.calidad = "CALIDAD AUMAX"
        oDatosDescarga.turno = 226

        _axRespuesta.datosDescarga = oDatosDescarga
        Dim Respuesta As String = JsonConvert.SerializeObject(_axRespuesta)

        aux = Respuesta
        Return Respuesta
    End Function



End Class
