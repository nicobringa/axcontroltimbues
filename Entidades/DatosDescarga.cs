﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class DatosDescarga
    {
        public string patenteChasis { get; set; }
        public string patenteAcoplado { get; set; }
        public long turno { get; set; }
        public int idSector { get; set; }
        public string producto { get; set; }
        public string motivo { get; set; }
        public string calidad { get; set; }

    }
}
