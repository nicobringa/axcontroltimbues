﻿Public Class NotificacionN
    Dim oBD As New AccesoDatos.NotificacionAD

    Public Function Add(ByVal TipoNotificacion As Entidades.Constante.TipoNotificacion, ByVal Mensaje As String, idControlAcceso As Integer,
                        Optional ByVal idSector As Integer = 0, Optional ByVal tag As String = "") As Boolean

        Dim oNotificacion As New Entidades.Notificacion
        oNotificacion.mensaje = Mensaje
        oNotificacion.fecha = DateTime.Now
        oNotificacion.ID_CONTROL_ACCESO = idControlAcceso
        If idSector = 0 Then
            oNotificacion.ID_SECTOR = Nothing
        Else
            oNotificacion.ID_SECTOR = idSector
        End If

        oNotificacion.TIPO_NOTIFICACION = TipoNotificacion

        oNotificacion.tag = tag

        Return oBD.add(oNotificacion)

    End Function



    Public Function GetAll(ByRef UltId As Integer, idControlAcceso As Integer) As List(Of Entidades.Notificacion)
        If UltId = 0 Then
            UltId = oBD.getUltId()
        End If

        Return oBD.getAll(UltId, idControlAcceso)
    End Function


    Public Sub BorrarNotificaciones()
        oBD.borrarNotificaciones(DateTime.Now.AddDays(-7))
    End Sub





End Class
