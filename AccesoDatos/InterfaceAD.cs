﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace AccesoDatos
{
    public class InterfaceAD
    {

        public Entidades.INTERFACE_WS add(Entidades.INTERFACE_WS wsInterface)
        {
            try
            {
                Entidades.AxControlORAEntities db = new Entidades.AxControlORAEntities();
                  db.INTERFACE_WS.Add(wsInterface);

                int id =  db.SaveChanges();
                wsInterface = db.INTERFACE_WS.Take(1).OrderByDescending(o => o.ID_REGISTRO).FirstOrDefault();
                return wsInterface;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public Entidades.INTERFACE_WS update(Entidades.INTERFACE_WS wsInterface)
        {
            try
            {
                Entidades.AxControlORAEntities db = new Entidades.AxControlORAEntities();
                var updateInterface = db.INTERFACE_WS.Find(wsInterface.ID_REGISTRO);
                updateInterface.ID_ESTADO = wsInterface.ID_ESTADO;
                updateInterface.ID_COMANDO = wsInterface.ID_COMANDO;
                updateInterface.JSON_INPUT = wsInterface.JSON_INPUT;
                updateInterface.WEB_SERVICE = wsInterface.WEB_SERVICE;
                updateInterface.JSON_RPTA_PLC = wsInterface.JSON_RPTA_PLC;
                db.SaveChanges();

                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public Entidades.INTERFACE_WS getOne(Int64 idRegistro)
        {
            try
            {
                Entidades.AxControlORAEntities db = new Entidades.AxControlORAEntities();
                return db.INTERFACE_WS.Find(idRegistro);


            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public Entidades.INTERFACE_WS getOne(string nombreWS, Entidades.Constante.ESTADO_REGISTRO Estado, int idComando)
        {
            try
            {
                Entidades.AxControlORAEntities db = new Entidades.AxControlORAEntities();
               return  db.INTERFACE_WS.Where(o => o.WEB_SERVICE == nombreWS &&
                    o.ID_ESTADO == (int)Estado && o.ID_COMANDO == idComando).FirstOrDefault();

                
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<Entidades.INTERFACE_WS> getAll(string nombreWS , Entidades.Constante.ESTADO_REGISTRO Estado) 
        {
			try
			{
				Entidades.AxControlORAEntities db = new Entidades.AxControlORAEntities();
				List<INTERFACE_WS> listInterfaceWs = db.INTERFACE_WS.Where(o => o.WEB_SERVICE == nombreWS &&
					o.ID_ESTADO == (int)Estado).ToList();

				return listInterfaceWs;
			}
			catch (Exception ex)
			{

				return null;
			}
        }

     

        public void addEvento(Entidades.Constante.ID_EVENTOS idEvento, string mensaje, Int64? idRegistro)
        {
            Entidades.AxControlORAEntities db = new AxControlORAEntities();
            Entidades.INTERFACE_EVENTOS evento = new INTERFACE_EVENTOS()
            {
                FECHA = DateTime.Now,
                TIPO_EVENTO = (int)idEvento,
                MENSAJE = mensaje,
                ID_INTERFACE = idRegistro
            };
            db.INTERFACE_EVENTOS.Add(evento);
            db.SaveChanges();
        }
        /// <summary>
        /// Permite obtener el idcomando para los comandos del interface
        /// </summary>
        /// <returns></returns>
        public Int64  getIdComando()
        {
            Entidades.AxControlORAEntities db = new AxControlORAEntities();
            string query = "select INTERFACE_ID_COMANDO_SEQ.NEXTVAL from SYS.DUAL";
            var query_result = db.Database.SqlQuery<Int64>(query); //throws error "sequence not allowed here"
            return query_result.First();
        }
    }
}
