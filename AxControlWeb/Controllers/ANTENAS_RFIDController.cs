﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entidades;

namespace AxControlWeb.Controllers
{
    public class ANTENAS_RFIDController : Controller
    {
        private AxControlORAEntities db = new AxControlORAEntities();

        // GET: ANTENAS_RFID
        public ActionResult Index()
        {
            var aNTENAS_RFID = db.ANTENAS_RFID.Include(a => a.CONFIG_LECTOR_RFID).Include(a => a.SECTOR);
            return View(aNTENAS_RFID.ToList());
        }

        // GET: ANTENAS_RFID/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ANTENAS_RFID aNTENAS_RFID = db.ANTENAS_RFID.Find(id);
            if (aNTENAS_RFID == null)
            {
                return HttpNotFound();
            }
            return View(aNTENAS_RFID);
        }

        // GET: ANTENAS_RFID/Create
        public ActionResult Create()
        {
            ViewBag.ID_CONF_LECTOR_RFID = new SelectList(db.CONFIG_LECTOR_RFID, "ID_CONF_LECTOR_RFID", "NOMBRE");
            ViewBag.ID_SECTOR = new SelectList(db.SECTOR, "ID_SECTOR", "NOMBRE");
            return View();
        }

        // POST: ANTENAS_RFID/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NUM_ANTENA,TX_POWER,RX_SENSITIVITY,OBSERVACION,ID_CONF_LECTOR_RFID,ID_SECTOR")] ANTENAS_RFID aNTENAS_RFID)
        {
            if (ModelState.IsValid)
            {
                db.ANTENAS_RFID.Add(aNTENAS_RFID);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_CONF_LECTOR_RFID = new SelectList(db.CONFIG_LECTOR_RFID, "ID_CONF_LECTOR_RFID", "NOMBRE", aNTENAS_RFID.ID_CONF_LECTOR_RFID);
            ViewBag.ID_SECTOR = new SelectList(db.SECTOR, "ID_SECTOR", "NOMBRE", aNTENAS_RFID.ID_SECTOR);
            return View(aNTENAS_RFID);
        }

        // GET: antena_rfid/Edit/5
        public ActionResult Edit(int? numAntena, int? idConfiguracion, int idLectorRFID)
        {
            if (numAntena == null || idConfiguracion == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ANTENAS_RFID antena_rfid = db.ANTENAS_RFID.Where(a => a.NUM_ANTENA == numAntena && a.ID_CONF_LECTOR_RFID == idConfiguracion).FirstOrDefault();
            if (antena_rfid == null)
            {
                return HttpNotFound();
            }
            //ViewBag.idConfiguracion = new SelectList(db.config_lector_rfid, "idConfiguracion", "nombre", antena_rfid.idConfiguracion);
            //ViewBag.idSector = new SelectList(db.sector, "idSector", "nombre", antena_rfid.idSector);
            return View(antena_rfid);
        }

        // POST: antena_rfid/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ANTENAS_RFID antena_rfid, int idLectorRFID)
        {
            if (ModelState.IsValid)
            {
                db.Entry(antena_rfid).State = EntityState.Modified;
                db.SaveChanges();
                ComandosController oComC = new ComandosController();
                //oComC.fnGenerarComando("AplicarConfigRFID_10", "");
                return RedirectToAction("Editar", "LECTOR_RFID", new { id = idLectorRFID });
            }
            //ViewBag.idConfiguracion = new SelectList(db.config_lector_rfid, "idConfiguracion", "nombre", antena_rfid.idConfiguracion);
            ViewBag.idSector = new SelectList(db.SECTOR, "idSector", "nombre", antena_rfid.ID_SECTOR);
            //return RedirectToAction("Edit", "Lectores_RFID", idLectorRFID);
            return View(antena_rfid);
        }

        // GET: ANTENAS_RFID/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ANTENAS_RFID aNTENAS_RFID = db.ANTENAS_RFID.Find(id);
            if (aNTENAS_RFID == null)
            {
                return HttpNotFound();
            }
            return View(aNTENAS_RFID);
        }

        // POST: ANTENAS_RFID/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ANTENAS_RFID aNTENAS_RFID = db.ANTENAS_RFID.Find(id);
            db.ANTENAS_RFID.Remove(aNTENAS_RFID);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
