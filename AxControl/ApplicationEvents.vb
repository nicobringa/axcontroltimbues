﻿Namespace My
    ' Los siguientes eventos están disponibles para MyApplication:
    ' Inicio: se desencadena cuando se inicia la aplicación, antes de que se cree el formulario de inicio.
    ' Apagado: generado después de cerrar todos los formularios de la aplicación. Este evento no se genera si la aplicación termina de forma anómala.
    ' UnhandledException: generado si la aplicación detecta una excepción no controlada.
    ' StartupNextInstance: se desencadena cuando se inicia una aplicación de instancia única y la aplicación ya está activa. 
    ' NetworkAvailabilityChanged: se desencadena cuando la conexión de red está conectada o desconectada.

    Partial Friend Class MyApplication


        '''' <summary>
        '''' Lo primero que ejecuta la aplicacion
        '''' </summary>
        'Private Sub Inicio() Handles Me.Startup

        '    Dim ConexionEstablecida As Boolean = VerificarConexionBD()

        '    If True Then
        '        'Obtengo el nombre de la maquina para saber si existe como puesto de trabajo
        '        Dim NombreMaquina As String = Environment.MachineName

        '        Dim nPuestoTrabajo As New Negocio.Puesto_TrabajoN
        '        Dim PuestoTrabajo As Entidades.Puesto_Trabajo = nPuestoTrabajo.GetOne(NombreMaquina)

        '        If Not IsNothing(PuestoTrabajo) Then
        '            'Agrego el puesto de trabajo a la sesion
        '            Negocio.ModSesion.PUESTO_TRABAJO = PuestoTrabajo

        '        Else
        '            'Si no tiene puesto de trabajo muestro el formulario
        '            'para agregarlo y que seleccione los controles de acceso a trabajar

        '            Dim frm As New FrmNvoPuestoTrabajo(NombreMaquina)
        '            If frm.ShowDialog() = DialogResult.OK Then

        '            Else
        '                'Finalizo el sistema
        '                'Finalize()
        '            End If

        '        End If



        '        If PuestoTrabajo.servidor Then
        '            OnCreateMainForm(New frmMapa)
        '        Else

        '            OnCreateMainForm(New FrmControlAcceso)
        '        End If


        '    Else
        '        'Si no se pudo establecer conexion con las base de datos 
        '        'muestro el formulario de configuracion de base datos
        '        Dim oError As New Entidades.error
        '        Dim oErrorN As New Negocio.ErrorN
        '        oErrorN.Guardar(System.DateTime.Now, Environment.MachineName, "ApplicationEvents.vb", "VerificarConexionBD", 62, "No se pudo establecer, la comunicación")
        '        OnCreateMainForm(New FrmConfBD)
        '        'Finalize()

        '    End If

        'End Sub

        '''' <summary>
        '''' Permite verificar la conexion con las base de datos
        '''' AxControl y AxDriver
        '''' </summary>
        'Private Function VerificarConexionBD() As Boolean
        '    Dim strSinConexion As String = "No se pudo establecer conexión con la base de datos {0}. Comuníquese con el administrador de sistemas"

        '    If Not Negocio.ConexionN.BD_AxControlConectada Then
        '        MessageBox.Show(String.Format(strSinConexion, "ax_control"))
        '        MessageBox.Show("[Conexion axControl]" & Entidades.ErrorBD.getMsjError)
        '        Return False
        '    End If

        '    If Not Negocio.ConexionN.BD_DriverConectada Then
        '        MessageBox.Show(String.Format(strSinConexion, "ax_drivers7"))
        '        MessageBox.Show("[Conexion axDriver]" & Entidades.ErrorBD.getMsjError)
        '        Return False
        '    End If

        '    Return True
        'End Function

        'Private Sub Apagado() Handles Me.Shutdown



        'End Sub

        'Private Sub CierreInesperado() Handles Me.UnhandledException

        'End Sub

    End Class
End Namespace
