﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlControlAccesoTicket2Calles
    Inherits Controles.CtrlAutomationObjectsBase

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CtrlControlAccesoTicket2Calles))
        Me.CtrlDinamismoTicket2Calles1 = New Controles.CtrlDinamismoTicket2Calles()

        Me.S2 = New Controles.CtrlSensor()
        Me.S1 = New Controles.CtrlSensor()
        Me.btnLectorRfid = New System.Windows.Forms.Button()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.CtrlEstadoLectoresRFID1 = New Controles.CtrlEstadoLectoresRFID()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CtrlAtenaLeyendoCalle1 = New Controles.CtrlAntenaLeyendo()
        Me.CtrlAtenaLeyendoCalle2 = New Controles.CtrlAntenaLeyendo()
        Me.btnAlarma = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnAyuda = New System.Windows.Forms.Button()
        Me.btnLecturaManual1 = New System.Windows.Forms.Button()
        Me.btnLecturaManual2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CtrlDinamismoTicket2Calles1
        '
        Me.CtrlDinamismoTicket2Calles1.BackgroundImage = CType(resources.GetObject("CtrlDinamismoTicket2Calles1.BackgroundImage"), System.Drawing.Image)
        Me.CtrlDinamismoTicket2Calles1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CtrlDinamismoTicket2Calles1.ForeColor = System.Drawing.Color.White
        Me.CtrlDinamismoTicket2Calles1.GPIO = False
        Me.CtrlDinamismoTicket2Calles1.ID_CONTROL_ACCESO = 0
        Me.CtrlDinamismoTicket2Calles1.ID_PLC = 0
        Me.CtrlDinamismoTicket2Calles1.ID_SECTOR = 0
        Me.CtrlDinamismoTicket2Calles1.Inicializado = False
        Me.CtrlDinamismoTicket2Calles1.Location = New System.Drawing.Point(-145, -6)
        Me.CtrlDinamismoTicket2Calles1.Name = "CtrlDinamismoTicket2Calles1"
        Me.CtrlDinamismoTicket2Calles1.PLC = Nothing
        Me.CtrlDinamismoTicket2Calles1.Size = New System.Drawing.Size(708, 397)
        Me.CtrlDinamismoTicket2Calles1.TabIndex = 0
        '
        'IMPRESORA_2
        '
        Me.IMPRESORA_2.Conectado = False
        Me.IMPRESORA_2.Location = New System.Drawing.Point(277, 52)
        Me.IMPRESORA_2.Name = "IMPRESORA_2"
        Me.IMPRESORA_2.oImpresora = Nothing
        Me.IMPRESORA_2.Size = New System.Drawing.Size(111, 81)
        Me.IMPRESORA_2.TabIndex = 104
        '
        'IMPRESORA_1
        '
        Me.IMPRESORA_1.Conectado = False
        Me.IMPRESORA_1.Location = New System.Drawing.Point(160, 52)
        Me.IMPRESORA_1.Name = "IMPRESORA_1"
        Me.IMPRESORA_1.oImpresora = Nothing
        Me.IMPRESORA_1.Size = New System.Drawing.Size(111, 81)
        Me.IMPRESORA_1.TabIndex = 103
        '
        'S2
        '
        Me.S2.Estado = False
        Me.S2.GPIO = True
        Me.S2.ID_CONTROL_ACCESO = 0
        Me.S2.ID_PLC = 0
        Me.S2.ID_SECTOR = 0
        Me.S2.Inicializado = False
        Me.S2.Location = New System.Drawing.Point(359, 329)
        Me.S2.Name = "S2"
        Me.S2.PLC = Nothing
        Me.S2.Size = New System.Drawing.Size(65, 47)
        Me.S2.TabIndex = 105
        '
        'S1
        '
        Me.S1.Estado = False
        Me.S1.GPIO = True
        Me.S1.ID_CONTROL_ACCESO = 0
        Me.S1.ID_PLC = 0
        Me.S1.ID_SECTOR = 0
        Me.S1.Inicializado = False
        Me.S1.Location = New System.Drawing.Point(131, 329)
        Me.S1.Name = "S1"
        Me.S1.PLC = Nothing
        Me.S1.Size = New System.Drawing.Size(65, 47)
        Me.S1.TabIndex = 106
        '
        'btnLectorRfid
        '
        Me.btnLectorRfid.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.btnLectorRfid.FlatAppearance.BorderSize = 0
        Me.btnLectorRfid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLectorRfid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLectorRfid.ForeColor = System.Drawing.Color.White
        Me.btnLectorRfid.Image = Global.Controles.My.Resources.Resources.lector
        Me.btnLectorRfid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLectorRfid.Location = New System.Drawing.Point(3, 52)
        Me.btnLectorRfid.Name = "btnLectorRfid"
        Me.btnLectorRfid.Size = New System.Drawing.Size(151, 44)
        Me.btnLectorRfid.TabIndex = 107
        Me.btnLectorRfid.Text = "Lector RIFID"
        Me.btnLectorRfid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLectorRfid.UseVisualStyleBackColor = False
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(562, 36)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(254, 417)
        Me.CtrlPanelNotificaciones1.TabIndex = 108
        '
        'CtrlEstadoLectoresRFID1
        '
        Me.CtrlEstadoLectoresRFID1.Location = New System.Drawing.Point(0, 391)
        Me.CtrlEstadoLectoresRFID1.Name = "CtrlEstadoLectoresRFID1"
        Me.CtrlEstadoLectoresRFID1.Size = New System.Drawing.Size(563, 62)
        Me.CtrlEstadoLectoresRFID1.TabIndex = 109
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(-1, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(817, 43)
        Me.lblNombre.TabIndex = 166
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CtrlAtenaLeyendoCalle1
        '
        Me.CtrlAtenaLeyendoCalle1.AutoSize = True
        Me.CtrlAtenaLeyendoCalle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAtenaLeyendoCalle1.Location = New System.Drawing.Point(3, 283)
        Me.CtrlAtenaLeyendoCalle1.Name = "CtrlAtenaLeyendoCalle1"
        Me.CtrlAtenaLeyendoCalle1.Size = New System.Drawing.Size(111, 106)
        Me.CtrlAtenaLeyendoCalle1.TabIndex = 167
        Me.CtrlAtenaLeyendoCalle1.UltTag = Nothing
        '
        'CtrlAtenaLeyendoCalle2
        '
        Me.CtrlAtenaLeyendoCalle2.AutoSize = True
        Me.CtrlAtenaLeyendoCalle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.CtrlAtenaLeyendoCalle2.Location = New System.Drawing.Point(448, 283)
        Me.CtrlAtenaLeyendoCalle2.Name = "CtrlAtenaLeyendoCalle2"
        Me.CtrlAtenaLeyendoCalle2.Size = New System.Drawing.Size(111, 106)
        Me.CtrlAtenaLeyendoCalle2.TabIndex = 168
        Me.CtrlAtenaLeyendoCalle2.UltTag = Nothing
        '
        'btnAlarma
        '
        Me.btnAlarma.Image = Global.Controles.My.Resources.Resources.alarmaPresente
        Me.btnAlarma.Location = New System.Drawing.Point(511, 46)
        Me.btnAlarma.Name = "btnAlarma"
        Me.btnAlarma.Size = New System.Drawing.Size(52, 45)
        Me.btnAlarma.TabIndex = 194
        Me.btnAlarma.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Image = Global.Controles.My.Resources.Resources.ResetIcon
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(730, 1)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(43, 43)
        Me.btnReset.TabIndex = 205
        Me.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnAyuda
        '
        Me.btnAyuda.BackColor = System.Drawing.Color.White
        Me.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAyuda.Image = Global.Controles.My.Resources.Resources.Ayuda32Black
        Me.btnAyuda.Location = New System.Drawing.Point(773, 1)
        Me.btnAyuda.Name = "btnAyuda"
        Me.btnAyuda.Size = New System.Drawing.Size(43, 43)
        Me.btnAyuda.TabIndex = 204
        Me.btnAyuda.UseVisualStyleBackColor = False
        '
        'btnLecturaManual1
        '
        Me.btnLecturaManual1.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual1.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManual1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual1.Location = New System.Drawing.Point(78, 135)
        Me.btnLecturaManual1.Name = "btnLecturaManual1"
        Me.btnLecturaManual1.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual1.TabIndex = 206
        Me.btnLecturaManual1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual1.UseVisualStyleBackColor = False
        '
        'btnLecturaManual2
        '
        Me.btnLecturaManual2.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual2.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManual2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual2.Location = New System.Drawing.Point(425, 135)
        Me.btnLecturaManual2.Name = "btnLecturaManual2"
        Me.btnLecturaManual2.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual2.TabIndex = 207
        Me.btnLecturaManual2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual2.UseVisualStyleBackColor = False
        '
        'CtrlControlAccesoTicket2Calles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnLecturaManual2)
        Me.Controls.Add(Me.btnLecturaManual1)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnAyuda)
        Me.Controls.Add(Me.btnAlarma)
        Me.Controls.Add(Me.CtrlAtenaLeyendoCalle2)
        Me.Controls.Add(Me.CtrlAtenaLeyendoCalle1)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlEstadoLectoresRFID1)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.btnLectorRfid)
        Me.Controls.Add(Me.S1)
        Me.Controls.Add(Me.S2)
        Me.Controls.Add(Me.IMPRESORA_2)
        Me.Controls.Add(Me.IMPRESORA_1)
        Me.Controls.Add(Me.CtrlDinamismoTicket2Calles1)
        Me.Name = "CtrlControlAccesoTicket2Calles"
        Me.Size = New System.Drawing.Size(816, 453)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CtrlDinamismoTicket2Calles1 As CtrlDinamismoTicket2Calles

    Friend WithEvents S2 As CtrlSensor
    Friend WithEvents S1 As CtrlSensor
    Friend WithEvents btnLectorRfid As Button
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents CtrlEstadoLectoresRFID1 As CtrlEstadoLectoresRFID
    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlAtenaLeyendoCalle1 As CtrlAntenaLeyendo
    Friend WithEvents CtrlAtenaLeyendoCalle2 As CtrlAntenaLeyendo
    Friend WithEvents btnAlarma As Button
    Friend WithEvents btnReset As Button
    Friend WithEvents btnAyuda As Button
    Friend WithEvents btnLecturaManual1 As Button
    Friend WithEvents btnLecturaManual2 As Button
End Class
