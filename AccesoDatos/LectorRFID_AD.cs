﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using System.Data.Entity;

namespace AccesoDatos
{
    public class LectorRFID_AD
    {

        public LECTOR_RFID getOne(string ipLector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.LECTOR_RFID.Where(a => a.IP == ipLector).SingleOrDefault();

                return oConsulta;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }

        public LECTOR_RFID getOne(int idLector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.LECTOR_RFID.Where(a => a.ID_LECTOR_RFID == idLector).SingleOrDefault();

                return oConsulta;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }
        public LECTOR_RFID getOneSector(int idSector)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.LECTOR_RFID.Where(a => a.ID_LECTOR_RFID == idSector).SingleOrDefault();

                return oConsulta;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }
        public Boolean updateEstadoLector(LECTOR_RFID lectorRFID, Int32 estado, Int32 leyendo)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.LECTOR_RFID.Find(lectorRFID.ID_LECTOR_RFID);
                oConsulta.LEYENDO = leyendo;
                oConsulta.CONECTADO = estado;
                oContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return false;

            }
        }

        public Boolean update(LECTOR_RFID lectorRFID)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = oContext.LECTOR_RFID.Find(lectorRFID.ID_LECTOR_RFID);

                oConsulta.ID_CONTROL_ACCESO = lectorRFID.ID_CONTROL_ACCESO;
                oConsulta.IP = lectorRFID.IP;
                oConsulta.NOMBRE = lectorRFID.NOMBRE;
                oConsulta.DESCRIPCION = lectorRFID.DESCRIPCION;
                oConsulta.AUTO_CONECTARSE = lectorRFID.AUTO_CONECTARSE;
                oConsulta.AUTO_START = lectorRFID.AUTO_START;
                oConsulta.CONECTADO = lectorRFID.CONECTADO;
                oContext.SaveChanges();


                foreach (var itemConf in lectorRFID.CONFIG_LECTOR_RFID)
                {
                    Entidades.CONFIG_LECTOR_RFID oConf = oContext.CONFIG_LECTOR_RFID.Find(itemConf.ID_CONF_LECTOR_RFID);

                    if (oConf == null)
                    {
                        oContext.CONFIG_LECTOR_RFID.Add(itemConf);
                        oContext.SaveChanges();
                        oConf = oContext.CONFIG_LECTOR_RFID.Find(itemConf.ID_CONF_LECTOR_RFID);
                    }

                    oConf.NOMBRE = itemConf.NOMBRE;
                    oConf.SELECT_READER_MODE = itemConf.SELECT_READER_MODE;
                    oConf.SELECT_REPORT_MODE = itemConf.SELECT_REPORT_MODE;
                    oConf.SELECT_SEARCH = itemConf.SELECT_SEARCH;
                    oConf.SELECT_SESSION = itemConf.SELECT_SESSION;
                    oConf.GPIO_PUERTO1 = itemConf.GPIO_PUERTO1;
                    oConf.GPIO_PUERTO2 = itemConf.GPIO_PUERTO2;
                    oConf.GPIO_PUERTO3 = itemConf.GPIO_PUERTO3;
                    oConf.GPIO_PUERTO4 = itemConf.GPIO_PUERTO4;
                    oConf.GPIO_HABILITADO = itemConf.GPIO_HABILITADO;
                    oConf.RSSI = itemConf.RSSI;
                    oContext.SaveChanges();

                    //Elimino todas la antenas para cargarlas de vuelta con la antenas configurada por el usuario
                    AxControlORAEntities oContex2 = new AxControlORAEntities();
                    List<Entidades.ANTENAS_RFID> listAntUpdate = oContex2.ANTENAS_RFID.Where(a => a.ID_CONF_LECTOR_RFID == itemConf.ID_CONF_LECTOR_RFID).ToList();

                    if (listAntUpdate != null)
                    {
                        if (listAntUpdate.Count != itemConf.ANTENAS_RFID.Count) // si no mantiene la misma cantidad de antena
                        {
                            //elimino la cantidad de antena
                            oContex2.ANTENAS_RFID.RemoveRange(listAntUpdate);
                            oContex2.SaveChanges();
                            //Cargo nuevamente 
                            foreach (Entidades.ANTENAS_RFID itemAnt in itemConf.ANTENAS_RFID)
                            {
                                Entidades.ANTENAS_RFID newAntena = new ANTENAS_RFID();
                                newAntena.NUM_ANTENA = itemAnt.NUM_ANTENA;
                                newAntena.ID_CONF_LECTOR_RFID = itemAnt.ID_CONF_LECTOR_RFID;
                                newAntena.OBSERVACION = itemAnt.OBSERVACION;
                                newAntena.TX_POWER = itemAnt.TX_POWER;
                                newAntena.RX_SENSITIVITY = itemAnt.RX_SENSITIVITY;
                              

                                oContext.ANTENAS_RFID.Add(newAntena);
                                oContext.SaveChanges();
                            }
                        }
                        else
                        {
                            //Modifico el la configuracion de las antenas existentes
                            foreach (Entidades.ANTENAS_RFID  itemAnt in listAntUpdate)
                            {
                                Entidades.ANTENAS_RFID antena = itemConf.ANTENAS_RFID.Where(o => o.NUM_ANTENA == itemAnt.NUM_ANTENA).FirstOrDefault();

                                itemAnt.TX_POWER = antena.TX_POWER;
                                itemAnt.RX_SENSITIVITY = antena.RX_SENSITIVITY;
                                itemAnt.OBSERVACION = antena.OBSERVACION;
                            }
                            oContex2.SaveChanges();

                        }
                       
                    }
                    else//listAntRemove == null
                    {
                        //No tiene antenas cargadas


                        foreach (Entidades.ANTENAS_RFID itemAnt in itemConf.ANTENAS_RFID)
                        {
                            Entidades.ANTENAS_RFID newAntena = new ANTENAS_RFID();
                            newAntena.NUM_ANTENA = itemAnt.NUM_ANTENA;
                            newAntena.ID_CONF_LECTOR_RFID = itemAnt.ID_CONF_LECTOR_RFID;
                            newAntena.OBSERVACION = itemAnt.OBSERVACION;
                            newAntena.TX_POWER = itemAnt.TX_POWER;
                            newAntena.RX_SENSITIVITY = itemAnt.RX_SENSITIVITY;
                            newAntena.ID_CONF_LECTOR_RFID = oConf.ID_CONF_LECTOR_RFID;

                            oContext.ANTENAS_RFID.Add(newAntena);
                            oContext.SaveChanges();
                        }
                    }

                }
                return true;
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return false;

            }
        }



        /// <summary>
        /// Permite devolver todos los lectores rfid  de un puesto de trabajo
        /// </summary>
        /// <param name="idPuestoTrabajo"></param>
        /// <returns></returns>
        public List<LECTOR_RFID> getAll(int idControlAcceso)
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = from tabla in oContext.LECTOR_RFID
                                where tabla.ID_CONTROL_ACCESO == idControlAcceso
                                select tabla;

                return oConsulta.ToList();
            }
            catch (Exception ex)
            {

                // AxError.setError(ex);
                return null;

            }
        }

        /// <summary>
        /// permite devolver todos los lectores RFID
        /// </summary>
        /// <returns></returns>
        public List<LECTOR_RFID> getAll()
        {
            try
            {
                AxControlORAEntities oContext = new AxControlORAEntities();
                var oConsulta = from tabla in oContext.LECTOR_RFID
                                select tabla;

                return oConsulta.ToList();
            }
            catch (Exception ex)
            {

                //AxError.setError(ex);
                return null;

            }
        }

    }
}
