﻿Imports AccesoDatos

Public Class Config_lector_rfid_invengoN

    Public Function GetOne(ByVal id As Int32) As Entidades.Config_lector_rfid_invengo
        Dim oConfig_lector_rfid_invengoAD As New AccesoDatos.Config_lector_rfid_invengoAD

        Return oConfig_lector_rfid_invengoAD.getOne(id)
    End Function

    Public Function Update(ByVal oConfig_lector_rfid_invengo As Entidades.Config_lector_rfid_invengo) As Entidades.Config_lector_rfid_invengo

        Dim oConfig_lector_rfid_invengoAD As New AccesoDatos.Config_lector_rfid_invengoAD

        Return oConfig_lector_rfid_invengoAD.update_config(oConfig_lector_rfid_invengo)

    End Function

    Public Function GetAll() As List(Of Entidades.Config_lector_rfid_invengo)
        Dim oConfig_lector_rfid_invengoAD As New AccesoDatos.Config_lector_rfid_invengoAD
        Return oConfig_lector_rfid_invengoAD.getAll()
    End Function

End Class