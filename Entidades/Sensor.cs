﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Sensor
    {
        string _Descripcion;
        public string Descripcion
        {
            get
            {
                return _Descripcion;
            }
            set
            {
                _Descripcion = value;
            }
        }

        DATO_WORDINT _Estado;
        public DATO_WORDINT Estado
        {
            get
            {
                return _Estado;
            }
            set
            {
                _Estado = value;
            }
        }
        DATO_WORDINT _Habilitacion;
        public DATO_WORDINT Habilitacion
        {
            get
            {
                return _Habilitacion;
            }
            set
            {
                _Habilitacion = value;
            }
        }
        DATO_WORDINT _TiempoRet;
        public DATO_WORDINT TiempoRet
        {
            get
            {
                return _TiempoRet;
            }
            set
            {
                _TiempoRet = value;
            }
        }
    }
}
