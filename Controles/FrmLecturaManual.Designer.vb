﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLecturaManual
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLecturaManual))
        Me.txtTagRFID = New System.Windows.Forms.TextBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.dgAntenas = New System.Windows.Forms.DataGridView()
        Me.colNumAntena = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colLector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colItem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.dgAntenas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTagRFID
        '
        Me.txtTagRFID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTagRFID.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTagRFID.Location = New System.Drawing.Point(12, 12)
        Me.txtTagRFID.Name = "txtTagRFID"
        Me.txtTagRFID.Size = New System.Drawing.Size(306, 29)
        Me.txtTagRFID.TabIndex = 81
        Me.txtTagRFID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.ForeColor = System.Drawing.Color.White
        Me.btnAceptar.Location = New System.Drawing.Point(65, 152)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(196, 31)
        Me.btnAceptar.TabIndex = 82
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'dgAntenas
        '
        Me.dgAntenas.AllowUserToAddRows = False
        Me.dgAntenas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgAntenas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colNumAntena, Me.colLector, Me.colItem, Me.colSelect})
        Me.dgAntenas.Location = New System.Drawing.Point(12, 47)
        Me.dgAntenas.Name = "dgAntenas"
        Me.dgAntenas.Size = New System.Drawing.Size(306, 99)
        Me.dgAntenas.TabIndex = 83
        '
        'colNumAntena
        '
        Me.colNumAntena.HeaderText = "Antena"
        Me.colNumAntena.Name = "colNumAntena"
        Me.colNumAntena.Width = 50
        '
        'colLector
        '
        Me.colLector.HeaderText = "Lector"
        Me.colLector.Name = "colLector"
        Me.colLector.ReadOnly = True
        Me.colLector.Width = 150
        '
        'colItem
        '
        Me.colItem.HeaderText = "Item"
        Me.colItem.Name = "colItem"
        Me.colItem.Visible = False
        '
        'colSelect
        '
        Me.colSelect.HeaderText = ""
        Me.colSelect.Name = "colSelect"
        Me.colSelect.Width = 50
        '
        'FrmLecturaManual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 195)
        Me.Controls.Add(Me.dgAntenas)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtTagRFID)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmLecturaManual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lectura manual"
        CType(Me.dgAntenas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtTagRFID As TextBox
    Friend WithEvents btnAceptar As Button
    Friend WithEvents dgAntenas As DataGridView
    Friend WithEvents colNumAntena As DataGridViewTextBoxColumn
    Friend WithEvents colLector As DataGridViewTextBoxColumn
    Friend WithEvents colItem As DataGridViewTextBoxColumn
    Friend WithEvents colSelect As DataGridViewCheckBoxColumn
End Class
