﻿Imports Negocio
Imports System.Threading
Public Class CtrlAntenaLeyendo

    Public Event ClickBorrarBuffer()

    Private _UltTag As String
    Public Property UltTag() As String
        Get
            Return _UltTag
        End Get
        Set(ByVal value As String)
            _UltTag = value
            'modDelegado.SetTextLabel(Me, value, lblUltTag, Color.Black)
        End Set
    End Property

    Private hEfecto As Thread



#Region "Metodos"

    Public Sub TagLeyendo(ByVal TagRFID As String)

        UltTag = TagRFID

        Me.BackColor = Color.Yellow

        If IsNothing(hEfecto) Then
            IniciarEfecto()
        End If

        If Not hEfecto.IsAlive Then 'Si no esta en ejecucion el efecto
            'Lo pongo en ejecucion
            IniciarEfecto()
            hEfecto.Start()

        End If
    End Sub


    Private Sub IniciarEfecto()
        hEfecto = New Thread(AddressOf Efecto)
        hEfecto.IsBackground = True
        hEfecto.Name = "Hilo " & Me.Name
    End Sub

    Private Sub Efecto()

        Try
            While Me.BackColor.A > 0 ' Si la propiedad alpha rgb es > 0

                Me.BackColor = Color.FromArgb(Me.BackColor.A - 2, Color.Yellow)

                Dim Alpha As Integer = Me.BackColor.A - 15
                modDelegado.SetBackColorCtrl(Me, Me, Color.FromArgb(Alpha, Color.Yellow))

                modDelegado.SetRefreshCtrl(Me, Me)

                Thread.Sleep(100)

            End While
            '132; 162; 186
            Me.BackColor = Color.FromArgb(132, 162, 186)
        Catch ex As Exception

        End Try


    End Sub

    Public Sub BorrarUltimosTagLeido()
        UltTag = Nothing
    End Sub

    Private Sub Cerrar()

        Try
            If Not IsNothing(hEfecto) Then
                If hEfecto.IsAlive Then hEfecto.Abort()

            End If
        Catch ex As Exception

        End Try
    
    End Sub

    Public Sub setTiempoBuffer(ByVal tiempo As Integer)
        Negocio.modDelegado.SetTextLabel(Me, tiempo, lblTiempoBuffer, Color.Black)
    End Sub

#End Region


    Private Sub btnBorrarBuffer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrarBuffer.Click
        RaiseEvent ClickBorrarBuffer()
    End Sub
End Class
