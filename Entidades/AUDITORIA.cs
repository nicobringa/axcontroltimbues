//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class AUDITORIA
    {
        public long ID_AUDITORIA { get; set; }
        public string DESCRIPCION { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public Nullable<int> ID_SECTOR { get; set; }
        public string USUARIO { get; set; }
        public Nullable<int> ACCION { get; set; }
        public string TAG_RFID { get; set; }
        public Nullable<long> ID_TRANSACCION { get; set; }
        public Nullable<long> ID_AUDITORIA_WS { get; set; }
    
        public virtual SECTOR SECTOR { get; set; }
    }
}
