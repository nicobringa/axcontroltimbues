﻿Public Class Puesto_TrabajoN
    Dim oBD As New AccesoDatos.Puesto_TrabajoAD
    Public Function GetOne(Nombre As String) As Entidades.Puesto_Trabajo

        Return oBD.getOne(Nombre)
    End Function

    Public Function Guardar(PuestoTrabajo As Entidades.Puesto_Trabajo) As Entidades.PUESTO_TRABAJO
        If PuestoTrabajo.ID_PUESTO_TRABAJO = 0 Then
            'Agregar
            Return oBD.add(PuestoTrabajo)
        Else
            'Modificar
            Return Nothing
        End If
    End Function

End Class
