﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CtrlSectorCalado
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnLecturaManual = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.CARTEL_CALADO = New Controles.CtrlCartelMultiLED()
        Me.CtrlAtenaLeyendo = New Controles.CtrlAntenaLeyendo()
        Me.CtrlPanelNotificaciones1 = New Controles.CtrlPanelNotificaciones()
        Me.S_LECTURA = New Controles.CtrlSensor()
        Me.SuspendLayout()
        '
        'btnLecturaManual
        '
        Me.btnLecturaManual.BackColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.btnLecturaManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLecturaManual.Image = Global.Controles.My.Resources.Resources.IngresoManual32
        Me.btnLecturaManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLecturaManual.Location = New System.Drawing.Point(189, 47)
        Me.btnLecturaManual.Name = "btnLecturaManual"
        Me.btnLecturaManual.Size = New System.Drawing.Size(49, 38)
        Me.btnLecturaManual.TabIndex = 176
        Me.btnLecturaManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLecturaManual.UseVisualStyleBackColor = False
        '
        'lblNombre
        '
        Me.lblNombre.AutoEllipsis = True
        Me.lblNombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(1, 1)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(450, 43)
        Me.lblNombre.TabIndex = 174
        Me.lblNombre.Text = "Nombre Portería"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CARTEL_CALADO
        '
        Me.CARTEL_CALADO.BitMonitoreo = CType(0, Short)
        Me.CARTEL_CALADO.Conectado = False
        Me.CARTEL_CALADO.GPIO = False
        Me.CARTEL_CALADO.ID_CONTROL_ACCESO = 0
        Me.CARTEL_CALADO.ID_PLC = 0
        Me.CARTEL_CALADO.ID_SECTOR = 0
        Me.CARTEL_CALADO.Inicializado = False
        Me.CARTEL_CALADO.Location = New System.Drawing.Point(3, 157)
        Me.CARTEL_CALADO.Name = "CARTEL_CALADO"
        Me.CARTEL_CALADO.patenteLeida = Nothing
        Me.CARTEL_CALADO.PLC = Nothing
        Me.CARTEL_CALADO.Size = New System.Drawing.Size(194, 100)
        Me.CARTEL_CALADO.TabIndex = 180
        Me.CARTEL_CALADO.Tag = "CARTEL_BALANZA"
        Me.CARTEL_CALADO.tiempoPatenteLeidas = 0
        '
        'CtrlAtenaLeyendo
        '
        Me.CtrlAtenaLeyendo.Location = New System.Drawing.Point(5, 47)
        Me.CtrlAtenaLeyendo.Name = "CtrlAtenaLeyendo"
        Me.CtrlAtenaLeyendo.Size = New System.Drawing.Size(86, 72)
        Me.CtrlAtenaLeyendo.TabIndex = 175
        Me.CtrlAtenaLeyendo.UltTag = Nothing
        '
        'CtrlPanelNotificaciones1
        '
        Me.CtrlPanelNotificaciones1.AutoSize = True
        Me.CtrlPanelNotificaciones1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.CtrlPanelNotificaciones1.Location = New System.Drawing.Point(244, 44)
        Me.CtrlPanelNotificaciones1.Margin = New System.Windows.Forms.Padding(0)
        Me.CtrlPanelNotificaciones1.Name = "CtrlPanelNotificaciones1"
        Me.CtrlPanelNotificaciones1.Size = New System.Drawing.Size(207, 212)
        Me.CtrlPanelNotificaciones1.TabIndex = 173
        '
        'S_LECTURA
        '
        Me.S_LECTURA.BitMonitoreo = CType(0, Short)
        Me.S_LECTURA.Estado = False
        Me.S_LECTURA.GPIO = False
        Me.S_LECTURA.ID_CONTROL_ACCESO = 0
        Me.S_LECTURA.ID_PLC = 0
        Me.S_LECTURA.ID_SECTOR = 0
        Me.S_LECTURA.Inicializado = False
        Me.S_LECTURA.Location = New System.Drawing.Point(157, 91)
        Me.S_LECTURA.Name = "S_LECTURA"
        Me.S_LECTURA.patenteLeida = Nothing
        Me.S_LECTURA.PLC = Nothing
        Me.S_LECTURA.Size = New System.Drawing.Size(81, 45)
        Me.S_LECTURA.TabIndex = 171
        Me.S_LECTURA.tiempoPatenteLeidas = 0
        '
        'CtrlSectorCalado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.CARTEL_CALADO)
        Me.Controls.Add(Me.btnLecturaManual)
        Me.Controls.Add(Me.CtrlAtenaLeyendo)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.CtrlPanelNotificaciones1)
        Me.Controls.Add(Me.S_LECTURA)
        Me.Name = "CtrlSectorCalado"
        Me.Size = New System.Drawing.Size(454, 260)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnLecturaManual As Button
    Friend WithEvents CtrlAtenaLeyendo As CtrlAntenaLeyendo
    Protected WithEvents lblNombre As Label
    Friend WithEvents CtrlPanelNotificaciones1 As CtrlPanelNotificaciones
    Friend WithEvents S_LECTURA As CtrlSensor
    Friend WithEvents CARTEL_CALADO As CtrlCartelMultiLED
End Class
