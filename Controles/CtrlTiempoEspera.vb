﻿Public Class CtrlTiempoEspera

    Public Sub Inicializar()
        Dim oConfig As New Entidades.Configuracion
        Dim oConfigN As New Negocio.ConfiguracionN
        oConfig = oConfigN.GetOne
        Me.Tiempo = oConfig.TIEMPO_ESPERA
    End Sub

    Private _Tiempo As String
    Public Property Tiempo() As String
        Get
            Return _Tiempo
        End Get
        Set(ByVal value As String)
            _Tiempo = value
            Negocio.modDelegado.SetTextLabel(Me, value, lblTiempo, Color.Black)
        End Set
    End Property


    Private _SetVisible As Boolean
    Public Property SetVisible() As Boolean
        Get
            Return _SetVisible
        End Get
        Set(ByVal value As Boolean)
            _SetVisible = value

            Negocio.modDelegado.setVisibleCtrl(Me, Me, value)
        End Set
    End Property


End Class
