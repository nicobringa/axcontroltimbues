﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio

Public Class CtrlBarrera
    Inherits CtrlAutomationObjectsBase


#Region "Variables de Instancia"
    Dim oPLC As Plc
    Dim oPLCN As PlcN
    Public oEstado As DATO_WORDINT
    Public oComando As DATO_WORDINT
    Dim oHabSenPosicion As DATO_WORDINT
    Dim oDatoIntN As DatoWordIntN
    'Dim oHabilitacion As DatoBool
    Private WithEvents ofrmBarrera As frmBarrera
#End Region

#Region "Constante"



#End Region

#Region "Propiedades"
    Private EstadoBarrera_ As Boolean
    Public Property Estado() As Boolean
        Get
            Return EstadoBarrera_
        End Get
        Set(ByVal value As Boolean)
            EstadoBarrera_ = value
        End Set
    End Property



#End Region

#Region "Eventos"
    Public Event UsoManual()
#End Region

#Region "Métodos"
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        oPLC = New Plc
        oPLCN = New PlcN

        'oEstado = New DatoWordInt
        'oComando = New DatoWordInt
        'oInfrarrojo = New DatoWordInt
        oDatoIntN = New DatoWordIntN
        ofrmBarrera = New frmBarrera

    End Sub

    Public Overrides Sub Inicializar(ID_PLC As Integer?, ID_CONTROL_ACCESO As Integer, ID_SECTOR As Integer)
        Me.ID_PLC = ID_PLC
        Me.ID_CONTROL_ACCESO = ID_CONTROL_ACCESO
        Me.ID_SECTOR = ID_SECTOR

        oPLC = oPLCN.GetOne(Me.ID_PLC)
        If oPLC Is Nothing Then
            Me.EP.SetError(Me.lbTAG, "No existe el PLC " & PLC & ". Revise la configuración en axDriverS7.")
        Else
            Me.lbTAG.Text = Me.Name

            Dim nDatoBool As New Negocio.DatoBoolN
            Dim listDatoBool As New List(Of DATO_BOOL)
            Dim oTags As New List(Of DATO_WORDINT)

            Dim oTagN As New Negocio.DatoWordIntN


            oTags = oTagN.GetAllTags(Me.ID_PLC, Me.Name, Me.ID_SECTOR)


            For Each itemTag As Entidades.DATO_WORDINT In oTags
                Dim tag As String = Constante.getNombrePropiedad(itemTag.TAG)
                If tag.Equals(PROPIEDADES_TAG_PLC.ESTADO) Then
                    oEstado = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC.COMANDO) Then
                    oComando = itemTag
                ElseIf tag.Equals(PROPIEDADES_TAG_PLC.HABILITACION_SENSOR_POSICION) Then
                    oHabSenPosicion = itemTag
                End If
            Next



            Dim tmp As String
            If IsNothing(oEstado) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7", Me.Tag, PROPIEDADES_TAG_PLC.ESTADO, Me.PLC)
                Me.EP.SetError(Me.lbTAG, tmp)
                Negocio.modDelegado.SetEnable(Me, btnConfig, False)
                Return
            End If

            If IsNothing(oComando) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7", Me.Tag, PROPIEDADES_TAG_PLC.ESTADO, Me.PLC)
                Me.EP.SetError(Me.lbTAG, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If

            If IsNothing(oHabSenPosicion) Then
                tmp = String.Format("No existe el tag {0} .{1} en el PLC {2}. Revise la configuración en axDriverS7",
                                    Me.Tag, PROPIEDADES_TAG_PLC.HABILITACION_SENSOR_POSICION, Me.PLC)
                Me.EP.SetError(Me.lbTAG, tmp)
                Me.btnConfig.Enabled = False
                Return
            End If


            Me.Inicializado = True
        End If


    End Sub

    Public Sub SetEstado()
        If IsNothing(oEstado) Then
            Inicializar(Me.ID_PLC, Me.ID_CONTROL_ACCESO, Me.ID_SECTOR)
            Return
        End If
        oEstado = oDatoIntN.GetOne(ID_SECTOR, oEstado.TAG)
        oHabSenPosicion = oDatoIntN.GetOne(ID_SECTOR, oHabSenPosicion.TAG)
        Dim nDatoBool As New Negocio.DatoBoolN




        If oEstado Is Nothing Then
            ' Me.EP.SetError(Me.lbTAG, "No existe el tag " & Me.Tag & ".Estado en el PLC " & Me.PLC & ". Revise la configuración en axDriverS7.")
        Else


            'Me.EP.SetError(Me.lbTAG, Nothing)
            Select Case oEstado.valor


                Case BarreraEstado.Falla 'Falla
                        modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.Red)
                        'modDelegado.SetBackColorCtrl(Me, btnConfig,)
                        modDelegado.SetPictureVisible(Me, pbBarreraDeshab, False)
                    'Genero la alarma
                    Dim detalle As String = String.Format("{0} del control de acceso {1} se encuentra en el estado de falla", Me.Name, Me.ID_CONTROL_ACCESO)
                 '   GenerarAlarma(detalle, ID_CONTROL_ACCESO, ID_SECTOR, Me.Name)

                Case BarreraEstado.ArribaAuto, BarreraEstado.AbajoAuto
                    modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.GreenYellow)
                    Negocio.modDelegado.SetPictureVisible(Me, pbBarreraDeshab, False)

                 '   ModificarAlarma(ID_CONTROL_ACCESO, 0, ID_SECTOR, Me.Name)

                Case BarreraEstado.Deshabilitado
                    modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.White)
                    Negocio.modDelegado.SetPictureVisible(Me, pbBarreraDeshab, True)

                    '   ModificarAlarma(ID_CONTROL_ACCESO, 0, ID_SECTOR, Me.Name)
                Case Else
                    modDelegado.SetButtonColor(Me, btnConfig, Drawing.Color.Green)
                    Negocio.modDelegado.SetPictureVisible(Me, pbBarreraDeshab, False)

                    ' ModificarAlarma(ID_CONTROL_ACCESO, 0, ID_SECTOR, Me.Name)
            End Select


            End If

    End Sub

    Private Sub btnConfig_Click(sender As System.Object, e As System.EventArgs) Handles btnConfig.Click

        If Not IsNothing(oEstado) Then

            Dim frm As New frmBarrera(oEstado, oComando, oHabSenPosicion, Me.Name)
            frm.ShowDialog()

        Else
            MsgBox("No se pudo obtener el estado de la barrera , intente nuevamente", MsgBoxStyle.Exclamation, "axControl")
        End If

    End Sub

    Private Sub UsoManual_() Handles ofrmBarrera.UsoManual
        RaiseEvent UsoManual()
    End Sub

    Public Sub EjectComando(Comando As Entidades.Constante.BarreraComando)

        Dim nDatoWord As New Negocio.DatoWordIntN
        nDatoWord.Escribir(Me.oComando.id, Comando)

    End Sub

#Region "Delegados"
    Delegate Sub SetFrmEstadoCallback(ByVal [frm] As System.Windows.Forms.Form, ByVal [estado] As Integer)


#End Region

#End Region



End Class
