﻿Imports Entidades
Imports AccesoDatos
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Xml.Serialization
Imports Entidades.Constante

Public Class BalanzaN


    Public Function OperBarreraManual(ByVal idSector As Integer, ByVal operacion As Integer, ByVal usuario As String, ByVal idAuditoriaWS As Integer) As Boolean

        Dim Comando As Constante.BarreraComando = GetComandoOperacion(operacion)
        'Dim NombrePLC As String = Entidades.Constante.getNombrePLC(Constante.PuestoTrabajo.Porteria)

        If EjecComandoBarreras(idSector, Comando, usuario) Then

            Dim msjAuditoria As String = "Barrera Manual Ejecutada Correctamente idSector: " & idSector & " operacion: " & operacion & " Usuario: " & usuario

            Negocio.AuditoriaN.AddAuditoria(msjAuditoria, "", 0, 0, usuario, acciones.WS_OperBarreraManual, idAuditoriaWS)

            Return True
        Else
            Return False
        End If

    End Function


    ''' <summary>
    ''' Permite obtener el comando apartir de la operacion
    ''' </summary>
    ''' <param name="operacion">operacion que se quiere saber el comando</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetComandoOperacion(ByVal operacion As Integer) As Constante.BarreraComando

        Return If(operacion = Entidades.Constante.OperacionBarrera.SubirBarrera, Constante.BarreraComando.SubirManual, Constante.BarreraComando.SubirManual)
    End Function


    ''' <summary>
    ''' Permite ejecutar un comando de barrera
    ''' </summary>
    ''' <param name="idSector">Sector que se va ejecutar el comando(Ingreso o Egreso)</param>
    ''' <param name="_Comando">Tipo de comnado a ejecutar</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EjecComandoBarreras(ByVal idSector As Integer, ByVal _Comando As Constante.BarreraComando, ByVal Usuario As String) As Boolean
        Dim xRet As Boolean
        'Obtengo el nombre de tag segun la barrera que se quiere ejecutar el comando
        'Dim NombreTAG As String = If(_Sector = Entidades.Constante.IngresoEgreso.Ingreso, Porteria.COMANDO_BARRERA_INGRESO, Porteria.COMANDO_BARRERA_EGRESO)


        Dim datoWordIntAD As New AccesoDatos.DatoWordIntAD
        Dim DatoWordIntN As New Negocio.DatoWordIntN

        Dim oTag As Entidades.DATO_WORDINT = datoWordIntAD.getOneTags(idSector, Entidades.Constante.PROPIEDADES_TAG_PLC.COMANDO)

        If DatoWordIntN.Escribir(oTag.id, _Comando).id = 0 Then
            xRet = False
        Else
            AuditoriaEjecComando(idSector, _Comando, Usuario)
            xRet = True
        End If



        Return xRet
    End Function

    Public Sub AuditoriaEjecComando(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _Comando As Constante.BarreraComando, ByVal Usuario As String)

        Dim textComando As String
        Dim textBarrera As String

        If _Comando = BarreraComando.PermitirIngresar Then
            textComando = "Abrir Automatico"
        ElseIf _Comando = BarreraComando.SubirManual Then
            textComando = "Abrir Manual"
        Else ' Cerrar Manual
            textComando = "Cerrar Manual"
        End If

        textBarrera = If(_Barrera = Entidades.Constante.IngresoEgreso.Ingreso, "Ingreso", "Egreso")

        Dim MsjAuditoria As String = "Comando " & _Comando.ToString() & " ejecutado " & "Sector " & textBarrera

        Negocio.AuditoriaN.AddAuditoria(MsjAuditoria, "", 0, 0, Usuario, acciones.Ejec_Comando)


    End Sub


    ''' <summary>
    ''' Permite tomar el peso de la balanza  
    ''' </summary>
    ''' <param name="_Balanza">_</param>
    ''' <remarks></remarks>
    Public Function GetPesoBalanza(ByVal _Balanza As ConstanteBalanza.Balanzas) As Integer

        'Dim NombrePLC As String = getNombrePLC(Negocio.ModSesion.PUESTO_TRABAJO.nombre)
        'Dim textUnoDos As String = If(_Balanza = ConstanteBalanza.Balanzas.Uno, " 1 ", " 2 ")
        ''Dim nTagPLC As New Negocio.TagPlcN
        'Dim PesoSigno As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 1")
        'Dim PesoDato2 As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 2")
        'Dim PesoDato3 As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 3")
        'Dim PesoDato4 As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 4")
        'Dim PesoDato5 As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 5")
        'Dim PesoDato6 As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 6")
        'Dim PesoDato7 As Int16 '= nTagPLC.GetValorTag(NombrePLC, "Peso Balanza" & textUnoDos & "Datos 7")
        ''Armo el peso de la balanza
        'Dim Peso As String = Convert.ToChar(PesoDato2) & Convert.ToChar(PesoDato3) & Convert.ToChar(PesoDato4) &
        '                     Convert.ToChar(PesoDato5) & Convert.ToChar(PesoDato6) & Convert.ToChar(PesoDato7)

        Try
            Dim oDB As New AxControlORAEntities

            Dim PesoTomado As Int32
            Dim tag As String
            If _Balanza = ConstanteBalanza.Balanzas.Uno Then
                tag = "BALANZA1.Peso"
            Else
                tag = "BALANZA2.Peso"
            End If

            ' Dim datoBalanza As New AccesoDatos.BalanzaAD

            'PesoTomado = datoBalanza

            Return 0
        Catch ex As Exception
            Return 0
        End Try

        'If Int32.TryParse(Peso, PesoTomado) Then
        '    'Si tiene formato veo si el peso es negativo
        '    If Convert.ToChar(PesoSigno) = "-" Then
        '        Return PesoTomado * -1
        '    Else
        '        Return PesoTomado
        '    End If
        'Else
        '    Throw New Exception("[Peso Balanza " & textUnoDos & "] No tiene formato")
        'End If


    End Function

    Public Function EjecComandoBalanza(ByVal Balanza As ConstanteBalanza.Balanzas, ByVal ComandoBalanza As ConstanteBalanza.ComandoBalanza, ByVal Sector As Integer, ByVal Usuario As String) As Boolean

        Dim xRet As Boolean

        Dim datoWordIntAD As New AccesoDatos.DatoWordIntAD
        Dim DatoWordIntN As New Negocio.DatoWordIntN

        Dim oTag As Entidades.DATO_WORDINT = datoWordIntAD.getOneTags(Sector, Entidades.Constante.PROPIEDADES_TAG_PLC.COMANDO)

        If DatoWordIntN.Escribir(oTag.id, ComandoBalanza).id = 0 Then
            xRet = False
        Else
            AuditoriaEjecComando(Sector, ComandoBalanza, Usuario)
            xRet = True
        End If

        Dim DescAuditoria As String = String.Format("Se ejecuto el comando: {0} PLC: {1} - BALANZA - xRet: {2} ", ComandoBalanza.ToString(), Sector, xRet.ToString)
        Negocio.AuditoriaN.AddAuditoria(DescAuditoria, "", Sector, 0, Usuario, acciones.Ejec_Comando)

        Return xRet

    End Function

    Public Function controloFalla(ByVal idSector As Integer) As Boolean

        Try
            Dim datoWordIntAD As New AccesoDatos.DatoWordIntAD

            Dim oTag As Entidades.DATO_WORDINT = datoWordIntAD.getOneTags(idSector, ".COMANDO")
            If oTag.valor = Constante.BarreraEstado.Falla Then
                Return False ' si está en falla devuelvo falso
            Else
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function


    Public Function GetNumBalanza(ByVal idSector As Integer) As ConstanteBalanza.Balanzas
        If idSector = Constante.BarrerasBalanza.barreraEgresoBalanza1 Or idSector = Constante.BarrerasBalanza.barreraIngresoBalanza1 Then
            Return Constante.numeroBalanza.Balanza1
        Else
            Return Constante.numeroBalanza.Balanza2
        End If
    End Function

    ''' <summary>
    ''' Metodo que se ejecuta cuando se quiere ingresar un camion por un sector de forma manual
    ''' </summary>
    ''' <param name="idSector">id sector que se quiere ingresar</param>
    ''' <param name="idTrasaccion">id trasaccion para auditoria</param>
    ''' <param name="TagRFID">tag del camion</param>
    ''' <param name="msj">mensaje que se va mostrar en el cartel</param>
    ''' <remarks></remarks>
    Public Function EstaHabilitadoManual(ByVal idSector As Integer, ByVal idTrasaccion As Integer, ByVal TagRFID As String, ByVal msj As String, ByVal usuario As String) As Boolean

        Dim comandoBalanza

        If GetNumBalanza(idSector) = 1 Then
            comandoBalanza = Constante.BalanzaComando.PermitirIngresoBarrera1
        Else
            comandoBalanza = Constante.BalanzaComando.PermitirIngresoBarrera2
        End If

        If controloFalla(idSector) Then
            If EjecComandoBarreras(idSector, comandoBalanza, usuario) Then
                Dim msjAuditoria As String = String.Format("Habilitación Manual Ejecutada |TAG = {0} | Mensaje = {1} | SensorImpresora = {2} ",
                                                   TagRFID, msj)
                Negocio.AuditoriaN.AddAuditoria(msjAuditoria, TagRFID, idSector, idTrasaccion, usuario, acciones.WS_HabilitadoManual)
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
End Class
