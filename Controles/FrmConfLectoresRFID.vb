﻿Public Class FrmConfLectoresRFID



#Region "PROPIEDADES"
    Private ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay)
#End Region


#Region "CONSTRUCTOR"
    Public Sub New(ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay))

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        If Not IsNothing(ListRFID_SpeedWay) Then CargarLectores(ListRFID_SpeedWay)

    End Sub

    Public Sub New(ListRFID_Invengo As List(Of Negocio.LectorRFID_Invengo))

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        If Not IsNothing(ListRFID_Invengo) Then CargarLectores(ListRFID_Invengo)

    End Sub
#End Region

#Region "EVENTOS FORM"

#End Region


#Region "METODOS"
    Private Sub CargarLectores(ListRFID_SpeedWay As List(Of Negocio.LectorRFID_SpeedWay))

        For Each LectorRFID_SpeedWay As Negocio.LectorRFID_SpeedWay In ListRFID_SpeedWay
            Dim ctrlLectorRFID As New Controles.CtrlLectorAntena(LectorRFID_SpeedWay)
            Dim ctrlAntena As New Controles.CtrlAntenaSector()
            flpLectoresRFID.Controls.Add(ctrlLectorRFID)

        Next

    End Sub

    Private Sub CargarLectores(ListRFID_Invengo As List(Of Negocio.LectorRFID_Invengo))

        For Each LectorRFID_Invengo As Negocio.LectorRFID_Invengo In ListRFID_Invengo
            Dim ctrlLectorRFID As New Controles.CtrlLectorAntena(LectorRFID_Invengo)
            Dim ctrlAntena As New Controles.CtrlAntenaSector()
            flpLectoresRFID.Controls.Add(ctrlLectorRFID)

        Next

    End Sub
#End Region

#Region "SUBPROCESOS"

#End Region

End Class