﻿Imports Entidades
Imports Entidades.Constante
Imports Negocio
Imports System.Threading
Imports Newtonsoft

Public Class CtrlSectorBalanzaUnidireccionalDobleBarrera
#Region "PROPIEDADES"


    Private _idSector As Integer = 0
    Public Property ID_SECTOR() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Private Sector As SECTOR
    Public WithEvents Buffer As Entidades.BufferTag
    Private UltIdNotificacion As Integer

    ''' <summary>
    ''' Evento que se genera cuando se realiza una lectura manual RFID
    ''' </summary>
    Public Event EventTagLeidoManual(ByVal oTagLeidoManual As TagLeidoManual)

    Private hMonitoreo As Thread

    Private _CAMION_PESANDO As Entidades.CAMION_PESANDO
    Private hEnviarPesoBal As Thread
    Dim UltEstadoBalanza As ConstanteBalanza.EstadoBalanza

    Dim boSeGeneroNotifBitVidaPLC As Boolean = False

#End Region

#Region "CONSTRUCTOR"

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Inicializar()
    End Sub

    Public Sub New(oSector As SECTOR)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.Sector = oSector
        _idSector = Me.Sector.ID_SECTOR
        Inicializar()
    End Sub

#End Region

#Region "Eventos FORM"

    Public Event ClickBarreraManual(ByVal _Barrera As Entidades.Constante.IngresoEgreso, ByVal _BarreraComando As Entidades.Constante.BarreraComando)

#End Region

#Region "METODOS"
    ''' <summary>
    ''' Metodo para incializar todas las variables de estado del objeto
    ''' </summary>
    Public Sub Inicializar()
        ' InitializeComponent()
        If Me.ID_SECTOR = 0 Then
            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_SECTOR, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": Se debe asignar un ID un control de acceso")

            Return
        End If



        'Busco el control de acceso
        Dim nControlAcceso As New Negocio.ControlAccesoN

        If IsNothing(Me.Sector) Then

            CtrlPanelNotificaciones1.MostrarNotificacion(Me.ID_SECTOR, Entidades.Constante.TipoNotificacion.Informacion,
               "[ " + DateTime.Now + " ] INFO: " + Me.Name + ": No se encontro el sector")
            Return

        End If
        Dim nombreSector As String = String.Format("{0} - ({1})", Me.Sector.DESCRIPCION, Me.Sector.ID_SECTOR)
        Negocio.modDelegado.SetTextLabel(Me, lblNombre, nombreSector)

        inicializarCtrl()
        SetBuffer()
        SetVisualizar()

        hMonitoreo = New Thread(AddressOf Monitoreo)
        hMonitoreo.IsBackground = True
        hMonitoreo.Start()


    End Sub

    Private Sub inicializarCtrl()
        Try
            'Recorro los sectores
            'De cada sector traigo los TAG del plc que tiene el control de acceso
            Dim nDatoWord As New Negocio.DatoWordIntN
            Dim listDatoWordInt As List(Of Entidades.DATO_WORDINT) = nDatoWord.GetAllPorSector(Me.Sector.ID_SECTOR)

            For Each itemDato As Entidades.DATO_WORDINT In listDatoWordInt

                Dim ctrlAutomatizacion As New CtrlAutomationObjectsBase

                'Obtengo el nombre del tag quitandole la propiedad
                itemDato.TAG = Constante.getNombreTag(itemDato.TAG)
                'Si el tag contiene el nombre del control
                If itemDato.TAG.Equals(BARRERA1.Name) Then
                    ctrlAutomatizacion = Me.BARRERA1
                ElseIf itemDato.TAG.Equals(BARRERA2.Name) Then
                    ctrlAutomatizacion = Me.BARRERA2
                ElseIf itemDato.TAG.Equals(S1_POSICION.Name) Then
                    ctrlAutomatizacion = Me.S1_POSICION
                ElseIf itemDato.TAG.Equals(S2_POSICION.Name) Then
                    ctrlAutomatizacion = Me.S2_POSICION
                ElseIf itemDato.TAG.Equals(S1_BARRERA.Name) Then
                    ctrlAutomatizacion = Me.S1_BARRERA
                ElseIf itemDato.TAG.Equals(S2_BARRERA.Name) Then
                    ctrlAutomatizacion = Me.S2_BARRERA
                ElseIf itemDato.TAG.Equals(BALANZA.Name) Then
                    ctrlAutomatizacion = Me.BALANZA
                Else
                    ctrlAutomatizacion = Nothing
                End If

                If Not IsNothing(ctrlAutomatizacion) Then
                    'Si todavia no esta inicializado
                    If Not ctrlAutomatizacion.Inicializado Then ctrlAutomatizacion.Inicializar(itemDato.FK_PLC, Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, Me.Sector.ID_SECTOR) ' BARRERA DE INGRESO
                End If
            Next
            UltEstadoBalanza = BALANZA.oEstado.VALOR
            CARTEL_BALANZA.Inicializar(Me.Sector.ID_SECTOR)
        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Permite obtener 
    ''' </summary>
    ''' <returns></returns>
    Public Function getBufferAntena() As Entidades.BufferTag
        Dim Buffer As Entidades.BufferTag = Me.Buffer
        Return Buffer
    End Function

    Private Sub SetBuffer()
        'Inicializo los buffer
        Me.Buffer = New Entidades.BufferTag()

        Dim nConf As New Negocio.ConfiguracionN
        Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()

        Me.Buffer.TIEMPO_BORRAR = oConf.TIEMPO_BUFFER()
    End Sub

    Private Sub SetVisualizar()
        'Negocio.modDelegado.setVisibleCtrl(Me, S_LECTURA_INGRESO, Me.ControlAcceso.VISIBLE_BAR1)
        'Negocio.modDelegado.setVisibleCtrl(Me, S_INGRESO, Me.ControlAcceso.VISIBLE_BAR1)
        'Negocio.modDelegado.setVisibleCtrl(Me, B_INGRESO, Me.ControlAcceso.VISIBLE_BAR1)

        'Negocio.modDelegado.setVisibleCtrl(Me, CartelLED_INGRESO, Me.ControlAcceso.VISIBLE_CARTEL)
        'Negocio.modDelegado.setVisibleCtrl(Me, CAM_INGRESO, Me.ControlAcceso.VISIBLE_CAM)
        'CtrlDinamismoPorteriaIngreso1.SetVisibleBarrera(Me.ControlAcceso.VISIBLE_BAR1)

    End Sub

    ''' <summary>
    ''' Procedimeinto que consulta al WS EstaHabilitado si el tag leido esta habilitado a ingresar o salir
    ''' dependiendo del sector
    ''' Y procesa el axRespuesta para realizar las operaciones necesarias
    ''' </summary>
    ''' <param name="TagRFID">Tag leido</param>
    Public Sub EstaHabilitado(TagRFID As String)
        Dim SUB_TAG As String = "[EstaHabilitado]"
        Try
            Dim tmp As String ' = String.Format("EstaHabilidado ID_SECTOR = {0} | TagRFID = {1}", ID_SECTOR, TagRFID)
            'CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR,
            'TipoNotificacion:=TipoNotificacion.Informacion, Mensaje:=tmp)

            Dim CtrlCartel As Controles.CtrlCartelMultiLED = Nothing
            Dim ctrlCabezalBalanza As Controles.CtrlCabezalBalanza = Nothing
            Dim Comando As Entidades.Constante.BalanzaComando
            Dim CtrlInfoBalanza As ctrlInfomacionBalanza = Nothing
            Dim numBalanza As Integer
            'Ejecuto el comando dependiendo de la barrera 
            'Para saber si es de ingreso o de egreso

            Select Case ID_SECTOR
                Case BARRERA1.ID_SECTOR
                    ctrlCabezalBalanza = BALANZA
                    CtrlCartel = CARTEL_BALANZA
                    'CtrlInfoBalanza = ctrlInfomacionBalanza
                    numBalanza = 1

            End Select

            'Si la balanza esta en otro estado que no sea la barrera cerrada significa que la balanza no esta habilitada o esta ocupada
            If ctrlCabezalBalanza.oEstado.VALOR <> Constante.BalanzaEstado.BarrerasCerrada Then
                CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR,
                                                         TipoNotificacion:=TipoNotificacion.Informacion, Mensaje:=String.Format("[ " + DateTime.Now + " ] INFO: La balanza {0} no se encuentra habilitada o esta ocupada", numBalanza))
                Return
            End If


            'Pregunto si tagRFID esta habilitado a pasar por ese sector
            'Consumo el WS de Bit EstaHabilitado y obtengo el axRespuesta
            Dim axRespuesta As Entidades.AxRespuesta = WebServiceBitN.HabilitarTag(ID_SECTOR, TagRFID)




            'Pregunto si esta habilitado
            If axRespuesta.habilitado Then
                'Si esta habilitado
                'Muestro el mensaje en el cartel
                tmp = "[ " + DateTime.Now + " ]  CAMIÓN HABILITADO: ID TRANSACCIÓN: " + axRespuesta.idTransaccion.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR,
                                                         TipoNotificacion:=TipoNotificacion.Suceso, Mensaje:=tmp)
                Comando = BalanzaComando.PermitirIngresoBarrera1

                'Ejecuto el comando permitir ingresar en la barrera
                If Not IsNothing(ctrlCabezalBalanza) Then
                    SetCamionPesando(axRespuesta)
                    ctrlCabezalBalanza.EjectComando(Comando)
                Else
                    CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR,
                                                         TipoNotificacion:=TipoNotificacion.xErrorDeve, Mensaje:="Ningun contorl de barrera tiene asignado el sector " & ID_SECTOR)
                End If

            Else
                tmp = "[ " + DateTime.Now + " ]  CAMIÓN NO HABILITADO:  " + axRespuesta.mensaje.ToString
                CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR,
                                                         TipoNotificacion:=TipoNotificacion.xErrorUser, Mensaje:=tmp)
                'Guardo el ultimo tag leido
                Dim nUltTagLeido As New Negocio.Ult_Tag_LeidoN
                nUltTagLeido.Guardar(TagRFID, ID_SECTOR)

            End If


            If Not IsNothing(CtrlCartel) Then CtrlCartel.EscribirCartel(axRespuesta.mensaje)
        Catch ex As Exception
            ' SUB_TAG & ex.Message & "TAG LEIDO : " & TagRFID
            Dim tmp As String = String.Format("{0}| TagRFID: {1} | ID_SECTOR: {2} | Exception: {3} ",
                                              SUB_TAG, TagRFID, ID_SECTOR, ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR,
                                                         TipoNotificacion:=TipoNotificacion.xErrorDeve, Mensaje:=tmp)
        End Try
    End Sub

    Public Sub TagLeidoManual(ByVal TagRFID As String, ByVal NumAntena As Int16, ByVal LectorRFID As Entidades.LECTOR_RFID, ByVal manual As Boolean)
        Dim lm As New TagLeidoManual(TagRFID, NumAntena, LectorRFID)

        Dim hLecturaManual As New Thread(AddressOf TagLeidoManual)
        hLecturaManual.IsBackground = True
        hLecturaManual.Start(lm)

    End Sub

    ''' <summary>
    ''' Sub proceso que permite ejecutar el TagLeido de forma manual
    ''' </summary>
    ''' <param name="oTagLeidoManual"></param>
    Public Sub TagLeidoManual(oTagLeidoManual As Entidades.TagLeidoManual)
        RaiseEvent EventTagLeidoManual(oTagLeidoManual)
        'TagLeido(oTagLeidoManual.tagRFID, oTagLeidoManual.numAntena, oTagLeidoManual.lectorRFID, True)
    End Sub


    Private Sub VerEstadoBalanza()
        Dim SUB_TAG As String = "VerEstadoBalanza"
        Try
            'SELECCIONO LA BALANZA
            'Dim _Balanza As Entidades.ConstanteBalanza.Balanzas
            Dim controlBalanza As New CtrlCabezalBalanza
            Dim nBalanza As New Negocio.BalanzaN
            Dim ctrlCartelLED As New CtrlCartelMultiLED
            Dim ctrlTiempoEspera As CtrlTiempoEspera

            controlBalanza = BALANZA
            ctrlCartelLED = Me.CARTEL_BALANZA
            ctrlTiempoEspera = Me.CtrlTiempoEspera1

            'Mensaje que debo escribir en el cartel
            'Dim MensajeCartel As String = "ACA TIMBUES" 'DateTime.Now.ToString("HH:mm")
            Dim MensajePorDefector As String = DateTime.Now.ToString("HH:mm")
            Dim _EstadoBalanza As ConstanteBalanza.BarreraEstado = controlBalanza.oEstado.VALOR

            Dim nCAMION_PESANDO As New Negocio.CAMION_PESANDON
            Dim oCAMION_PESANDO As CAMION_PESANDO = nCAMION_PESANDO.GetOne(Me.ID_SECTOR)

            Dim hEnviarPeso As Thread = hEnviarPesoBal
            Dim nombreEstado As String = ""
            'Obtengo el ultimo estado de la balanza
            'Dim UltEstadoBalanza As Constante.BalanzaEstado = Me.UltEstadoBalanza
            Dim nConfiguracion As New Negocio.ConfiguracionN
            'UltEstadoBalanza = _EstadoBalanza

            Dim oConfig As New Entidades.CONFIGURACION
            Dim oConfigN As New Negocio.ConfiguracionN
            oConfig = oConfigN.GetOne

            Dim boEscribirCartel As Boolean = False
            Dim inPasoCartel As Integer = 0
            Dim strMensajeCartel As String = ""

            If controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.BarrerasCerrada Then '<---- BARRERA CERRADA
                nombreEstado = "Barrera Cerrada"
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoPorDefecto
                oCAMION_PESANDO = Nothing
                'Si quedo el hilo de Enviar peso vivo lo aborto
                If Not IsNothing(hEnviarPeso) Then
                    If hEnviarPeso.IsAlive Then hEnviarPeso.Abort()
                    hEnviarPeso = Nothing

                End If
                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                ctrlTiempoEspera.Tiempo = oConfig.TIEMPO_ESPERA
                ctrlTiempoEspera.SetVisible = False

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.HabilitadaIngresarBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.HabilitadaIngresarBarrera2 Then '<--- HABILITADA INGRESAR
                'SI EL ESTADO DE LA BALANZA ES HABILITADA INGRESAR
                nombreEstado = "Habilitada Ingresar Barrera"
                'ESCRIBO AVANCE EN LOS CARTELES

                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoAVANCE
                'Habilito el camión pesando
                'Obtengo el camion que esta pesando
                _CAMION_PESANDO = oCAMION_PESANDO

                If (ctrlTiempoEspera.Tiempo > 0) Then  'Si el tiempo de espera es mayor a cero
                    ctrlTiempoEspera.SetVisible = True
                    '_TiempoEsperaBal1 += 1
                    ctrlTiempoEspera.Tiempo = ctrlTiempoEspera.Tiempo - 1
                Else '  = 0
                    Dim DescripcionAudit As String = "Reset de Balanza por tiempo agotado :  "
                    Negocio.AuditoriaN.AddAuditoria(DescripcionAudit, "", ID_SECTOR, 0, "Aumax", Constante.acciones.Reset_Balanza)
                    'Aqui realizar el reset de balanza
                    ctrlTiempoEspera.SetVisible = False

                    'Si la computadora es servidor aplica el reset
                    controlBalanza.Reset()
                End If

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.ErrorBalanza1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.ErrorBalanza2 Then '<--- BALANZA EN CERO O NO CONECTADA
                'SI EL ESTADO ES BALANZA CERO
                'Selecciono el cartel que debo escribir,siempre escribo al cartel opuesto 
                'de la barrera por la cual accede el camion
                nombreEstado = "Balanza Cero NoConectada Barrera"
                'ESCRIBO EN EL CARTEL LLAMAR
                'ESCRIBIR VARIABLE = COMUNIQUESE CON OPERADOR
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoVariable
                strMensajeCartel = "COMUNIQUESE CON UN OPERADOR"
                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                ctrlTiempoEspera.Tiempo = oConfig.TIEMPO_ESPERA

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.CierreBarreraEntradaBar1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.CierreBarreraEntradaBar2 Then '<--- AVANZA CAMION
                nombreEstado = "Cierre barrera entrada"
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoESPERE
                ctrlTiempoEspera.SetVisible = False

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.AvanzarCamionBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.AvanzarCamionBarrera2 Then '<--- AVANZA CAMION
                'SI EL ESTADO ES AVANCE CAMION
                nombreEstado = "Avanzar Camion Barrera"
                'ESCRIBO EN EL CARTEL AVANCE
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoAVANCE
                ctrlTiempoEspera.SetVisible = False
                ''CtrlBalanza1.SetTiempoEspera(_Balanza, 0)

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.RetrocedaCamionBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.RetrocedaCamionBarrera2 Then '<--- RETROCEDA CAMION
                'SI EL ESTADO ES RETROCEDA CAMION
                nombreEstado = "RetrocedaCamionBarrera"
                'ESCRIBO EN EL CARTEL RETROCEDA
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoRETROCEDA

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.EstabilizandoPesoBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.EstabilizandoPesoBarrera2 Then '<--- ESTABILIZANDO PESO
                nombreEstado = "Estabilizando Peso Barrera"
                'ESCRIBO EN EL CARTEL LA HORA ACTUAL
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoESPERE
                ctrlTiempoEspera.SetVisible = False

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.PesoMaximoBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.PesoMaximoBarrera2 Then '<---PESO MAXIMO
                nombreEstado = "Peso Maximo Barrera"
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoVariable
                'ESCRIBIR VARIABLE = COMUNIQUESE CON OPERADOR

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.TomarPesoBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.TomarPesoBarrera2 Then ' <--- TOMAR PESO
                _CAMION_PESANDO = oCAMION_PESANDO
                'Si ya envio el peso espero hasta que el plc cambie de estado
                nombreEstado = "Tomar Peso Barrera"
                If oCAMION_PESANDO.PESO_ENVIADO Then
                    'MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_AVANCE
                    Return
                End If
                ' si el hilo no esta en ejecucion  es porque se todavia no se envio el peso 
                'si la maquina es el servio envia el peso tomado por WS
                If IsNothing(hEnviarPeso) Then
                    hEnviarPeso = New Thread(AddressOf EnviarPeso)
                    hEnviarPeso.IsBackground = True
                    'Envio el hilo y espero la respuesta
                    hEnviarPeso = hEnviarPeso
                    hEnviarPeso.Start()
                    If Not oCAMION_PESANDO.PESO_ENVIADO Then
                        oCAMION_PESANDO.PESO_ENVIADO = True
                        Dim nCamionPesando As New Negocio.CAMION_PESANDON
                        nCamionPesando.Update(oCAMION_PESANDO)
                    End If
                End If

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.PermitirSalirBarrera1 Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.PermitirSalirBarrera2 Then '<-- PERMITIR SALIR
                nombreEstado = "Permitir Salir Barrera"
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoAVANCE
                'ESCRIBIR MENSAJE DE AXRESPUESTA
                'MensajeCartel = oCAMION_PESANDO.AXRESPUESTA

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.BajaBarrera1Salida Or controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.BajaBarrera2Salida Then '<--- BAJAR BARRERA
                boEscribirCartel = False
                oCAMION_PESANDO = Nothing
                nombreEstado = "Baja Barrera Salida"
                'Informo a los cliente a los clientes que que el camión dejo de pesar

            ElseIf controlBalanza.oEstado.VALOR = Constante.BalanzaEstado.Deshabilitada Then '<--- BALANZA DESHABILITADA
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoFUERADESERVICIO

            End If


            If boEscribirCartel Then
                If Not IsNothing(CARTEL_BALANZA) Then
                    CARTEL_BALANZA.fnEscribirCartel(inPasoCartel, "")
                End If
            End If

            Dim idTrasaccion As Long = 0

            'Si tengo un camion pesando
            If Not IsNothing(oCAMION_PESANDO) Then
                'Coloco el idTrasaccion para la auditoria
                'If Not IsNothing(CAMION_PESANDO.axRespuesta) Then idTrasaccion = CAMION_PESANDO.axRespuesta.idTransaccion
            End If

            Dim DescripcionAuditoria As String = "Cambio de estado en la balanza : a estado : " & nombreEstado
            Negocio.AuditoriaN.AddAuditoria(DescripcionAuditoria, "", ID_SECTOR, idTrasaccion, "Aumax", Constante.acciones.cambioEstadoBalaza)

            Thread.Sleep(1000)
        Catch ex As Exception

            CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=Entidades.Constante.TipoNotificacion.xErrorDeve, Mensaje:=SUB_TAG & ex.Message)
        End Try

    End Sub

    Private Sub VerEstadoBalanzaNuevo()
        Dim SUB_TAG = "VerEstadoBalanza"
        Try
            Dim boEscribirCartel As Boolean = False
            Dim inPasoCartel As Integer = 0
            Dim strMensajeCartel As String = ""

            Dim nCAMION_PESANDO As New Negocio.CAMION_PESANDON
            Dim oCAMION_PESANDO As CAMION_PESANDO = nCAMION_PESANDO.GetOne(Me.ID_SECTOR)

            Dim hEnviarPeso As Thread = hEnviarPesoBal

            Dim oConfig As New Entidades.CONFIGURACION
            Dim oConfigN As New Negocio.ConfiguracionN
            oConfig = oConfigN.GetOne

            If BALANZA.oEstado.VALOR = Constante.BalanzaEstado.BarrerasCerrada Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoPorDefecto
                If Not IsNothing(hEnviarPeso) Then
                    If hEnviarPeso.IsAlive Then hEnviarPeso.Abort()
                    hEnviarPeso = Nothing
                End If
                ''Si cambia de estado pongo el tiempo de espera consumido nuevamente en cero
                Me.CtrlTiempoEspera1.Tiempo = oConfig.TIEMPO_ESPERA
                Me.CtrlTiempoEspera1.SetVisible = False

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.HabilitadaIngresarBarrera1 And (S1_BARRERA.oEstado.VALOR = Constante.EstadoSensor.SensorConPresencia Or Not BALANZA.oPesoMayor500.VALOR Or BALANZA.oFueraEquilibrio.VALOR) Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoAVANCE
                'Habilito el camión pesando
                _CAMION_PESANDO = oCAMION_PESANDO
                If (CtrlTiempoEspera1.Tiempo > 0) Then  'Si el tiempo de espera es mayor a cero
                    CtrlTiempoEspera1.SetVisible = True
                    '_TiempoEsperaBal1 += 1
                    CtrlTiempoEspera1.Tiempo = CtrlTiempoEspera1.Tiempo - 1
                Else '  = 0
                    Dim DescripcionAudit As String = "Reset de Balanza por tiempo agotado :  "
                    Negocio.AuditoriaN.AddAuditoria(DescripcionAudit, "", ID_SECTOR, 0, "Aumax", Constante.acciones.Reset_Balanza)
                    'Aqui realizar el reset de balanza
                    CtrlTiempoEspera1.SetVisible = False
                    'Reinicio la balanza si pasó el tiempo de espera
                    BALANZA.Reset()
                End If

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.HabilitadaIngresarBarrera1 And S1_BARRERA.oEstado.VALOR = Constante.EstadoSensor.SensorSinPresencia And Not BALANZA.oFueraEquilibrio.VALOR Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoESPERE
                'Habilito el camión pesando
                _CAMION_PESANDO = oCAMION_PESANDO

                If (Me.CtrlTiempoEspera1.Tiempo > 0) Then  'Si el tiempo de espera es mayor a cero
                    Me.CtrlTiempoEspera1.SetVisible = True
                    '_TiempoEsperaBal1 += 1
                    Me.CtrlTiempoEspera1.Tiempo = Me.CtrlTiempoEspera1.Tiempo - 1
                Else '  = 0
                    Dim DescripcionAudit As String = "Reset de Balanza por tiempo agotado :  "
                    Negocio.AuditoriaN.AddAuditoria(DescripcionAudit, "", ID_SECTOR, 0, "Aumax", Constante.acciones.Reset_Balanza)
                    'Aqui realizar el reset de balanza
                    Me.CtrlTiempoEspera1.SetVisible = False

                    'Si la computadora es servidor aplica el reset
                    BALANZA.Reset()
                End If

            ElseIf (BALANZA.oEstado.VALOR = Constante.BalanzaEstado.CierreBarreraEntradaBar1 Or BALANZA.oEstado.VALOR = Constante.BalanzaEstado.AvanzarCamionBarrera1) And S1_BARRERA.oEstado.VALOR = Constante.EstadoSensor.SensorConPresencia Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoAVANCE
                Me.CtrlTiempoEspera1.SetVisible = False

            ElseIf (BALANZA.oEstado.VALOR = Constante.BalanzaEstado.CierreBarreraEntradaBar1 Or BALANZA.oEstado.VALOR = Constante.BalanzaEstado.AvanzarCamionBarrera1) And S1_BARRERA.oEstado.VALOR = Constante.EstadoSensor.SensorSinPresencia Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoESPERE
                Me.CtrlTiempoEspera1.SetVisible = False

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.RetrocedaCamionBarrera1 Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoRETROCEDA

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.EstabilizandoPesoBarrera1 Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoESPERE
                Me.CtrlTiempoEspera1.SetVisible = False

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.PesoMaximoBarrera1 Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoVariable
                strMensajeCartel = "COMUNIQUESE CON UN OPERADOR"

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.TomarPesoBarrera1 Then
                'Enviar peso a BIT, escribir cartel en función EnviarPeso cuando se envía el comando PermitirSalir

                _CAMION_PESANDO = oCAMION_PESANDO
                If oCAMION_PESANDO.PESO_ENVIADO Then
                    'MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_AVANCE
                    Return
                End If
                If IsNothing(hEnviarPeso) Then '
                    'EscribirCartel = True
                    'MensajeCartel = ConstanteBalanza.MENSAJE_CARTEL_ESPERE
                    hEnviarPeso = New Thread(AddressOf EnviarPeso)
                    hEnviarPeso.IsBackground = True
                    'Envio el hilo y espero la respuesta
                    hEnviarPeso = hEnviarPeso
                    hEnviarPeso.Start()
                    If Not oCAMION_PESANDO.PESO_ENVIADO Then
                        oCAMION_PESANDO.PESO_ENVIADO = True
                        Dim nCamionPesando As New Negocio.CAMION_PESANDON
                        nCamionPesando.Update(oCAMION_PESANDO)
                    End If
                End If

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.ErrorBalanza1 Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoVariable
                strMensajeCartel = "COMUNIQUESE CON UN OPERADOR"

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.PermitirSalirBarrera1 Then

            ElseIf BALANZA.oEstado.VALOR = Constante.BalanzaEstado.Manual Or BALANZA.oEstado.VALOR = Constante.BalanzaEstado.Deshabilitada Then
                boEscribirCartel = True
                inPasoCartel = Constante.PasosCartelMultiled.TextoFijoFUERADESERVICIO

            End If

            If boEscribirCartel Then
                If Not IsNothing(CARTEL_BALANZA) Then
                    CARTEL_BALANZA.fnEscribirCartel(inPasoCartel, "")
                End If
            End If
            Thread.Sleep(1000)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub EnviarPeso()
        Dim nBalanza As New Negocio.BalanzaN
        Dim SUB_TAG As String = "[EnviarPeso]"
        Try
            Dim CabezalBalanza As Controles.CtrlCabezalBalanza
            Dim Cartel As Controles.CtrlCartelMultiLED
            CabezalBalanza = BALANZA
            Cartel = CARTEL_BALANZA
            'Tomar el peso de la balanza
            Dim PesoBalanza As Integer = CabezalBalanza.oPeso.VALOR

            Dim oCAMION_PESANDO As CAMION_PESANDO = _CAMION_PESANDO

            Dim Habilitado As Boolean = False
            Dim AxRespuesta As Entidades.AxRespuesta = Nothing
            Dim nConf As New Negocio.ConfiguracionN
            Dim oConf As Entidades.CONFIGURACION = nConf.GetOne()
            Dim nCamionPesando As New Negocio.CAMION_PESANDON
            Dim cant As Integer = 0
            Dim axRespCamionPesando As AxRespuesta = Json.JsonConvert.DeserializeObject(Of AxRespuesta)(oCAMION_PESANDO.AXRESPUESTA)
            While Habilitado = False
                Try
                    PesoBalanza = CabezalBalanza.oPeso.VALOR
                    AxRespuesta = Negocio.WebServiceBitN.RegistrarPesada(IdSector:=oCAMION_PESANDO.ID_SECTOR, TagRFID:=oCAMION_PESANDO.TAG_RFID, Peso:=PesoBalanza, IdTrasaccion:=axRespCamionPesando.idTransaccion)
                Catch ex As Exception
                    CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.xErrorDeve, SUB_TAG & ex.Message)
                End Try


                If Not IsNothing(AxRespuesta) Then
                    Dim tmp = "[ " + DateTime.Now + " ] CAMIÓN HABILITADO: ID TRANSACCIÓN: " + AxRespuesta.idTransaccion.ToString
                    CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=TipoNotificacion.Suceso, Mensaje:=tmp)
                    CtrlPanelNotificaciones1.MostrarNotificacion(PuestoTrabajo.Balanzas, Entidades.Constante.TipoNotificacion.Informacion, "[ " + DateTime.Now + " ] INFO: PesoObtenido AxRespuesta : " & AxRespuesta.ToString())

                    Habilitado = AxRespuesta.habilitado
                    If AxRespuesta.habilitado Then
                        CabezalBalanza.EjectComando(BalanzaComando.PermitirSalir)
                        'Escribir el cartel el msj de BIT, lo escribo 2 veces porque suele ser un msj corto
                        CARTEL_BALANZA.fnEscribirCartel(Constante.PasosCartelMultiled.TextoVariable, AxRespuesta.mensaje + "   -   " + AxRespuesta.mensaje)

                    Else
                        'ESCRIBIR EN EL CARTEL COMUNICATE CON EL OPERADOR
                        CARTEL_BALANZA.fnEscribirCartel(PasosCartelMultiled.TextoVariable, "COMUNIQUESE CON UN OPERADOR")
                        'Genero auditoría de error
                        Dim DescripcionAudit As String = "Tag no habilitado en respuesta de BIT."
                        Negocio.AuditoriaN.AddAuditoria(Descripcion:=DescripcionAudit,
                                                        TagRFID:="",
                                                        _Sector:=ID_SECTOR,
                                                        idTrasaccion:=AxRespuesta.idTransaccion,
                                                        usuario:="AUMAX", accion:=acciones.WS_EstaHabilitado)
                    End If
                    Exit While
                End If

                Thread.Sleep(oConf.TIEMPO_ENVIAR_PESO * 1000)
                cant += 1
            End While

            If Not IsNothing(AxRespuesta) Then Cartel.EscribirCartel(AxRespuesta.mensaje)

            Thread.Sleep(2000) ' Duemo 2 segundo asi toma el cambio de estado el plc  
        Catch ex As Exception

            'axLog.AxLog.e(Tag, SUB_TAG & ex.Message)
            CtrlPanelNotificaciones1.MostrarNotificacion(ID_SECTOR:=Me.Sector.ID_SECTOR, ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, TipoNotificacion:=Entidades.Constante.TipoNotificacion.xErrorDeve, Mensaje:=SUB_TAG & ex.Message)

            hEnviarPesoBal = Nothing

        End Try



    End Sub

    Public Sub ReiniciarSector()
        CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=TipoNotificacion.Informacion, Mensaje:="[ " + DateTime.Now + " ] INFO: " + "SE REINICIÓ SECTOR " + Sector.DESCRIPCION)
        Inicializar()
    End Sub

    Public Sub HabilitarTodo()
        Dim oDatoIntN As New DatoWordIntN
        Dim cmd As New DATO_WORDINT
        ''Barreras
        cmd = oDatoIntN.Escribir(BARRERA1.oComando.ID, Constante.BarreraComando.Automatico)
        cmd = oDatoIntN.Escribir(BARRERA2.oComando.ID, Constante.BarreraComando.Automatico)
        ''Cabezal de balanza
        cmd = oDatoIntN.Escribir(BALANZA.oComando.ID, Constante.BalanzaComando.Habilitar)
        Thread.Sleep(1500)
        cmd = oDatoIntN.Escribir(BALANZA.oComando.ID, Constante.BalanzaComando.Reset)
        CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=TipoNotificacion.Informacion, Mensaje:="[ " + DateTime.Now + " ] INFO: SE HABILITARON TODOS LOS CONTROLES EN SECTOR: " + Sector.DESCRIPCION)
    End Sub

    Private Sub SetCamionPesando(AxRespuesta As Entidades.AxRespuesta)
        Dim oCamionPesando As New Entidades.CAMION_PESANDO
        oCamionPesando.ID_SECTOR = AxRespuesta.idSector
        oCamionPesando.TAG_RFID = AxRespuesta.tagRFID
        oCamionPesando.PESO_ENVIADO = False
        oCamionPesando.ID_TRANSACCION = AxRespuesta.idTransaccion
        oCamionPesando.FECHA_INGRESO = DateTime.Now

        oCamionPesando.AXRESPUESTA = Json.JsonConvert.SerializeObject(AxRespuesta)
        Dim nCamionPesando As New Negocio.CAMION_PESANDON
        nCamionPesando.SetCamionPesando(oCamionPesando)
    End Sub

    Public Sub VerBitVidaPLC(ByRef BitVidaPLC As Integer, ByRef Intentos As Integer)
        Dim nDatoWord As New Negocio.DatoWordIntN
        Dim nombreTag As String = "PLC_BALANZA.BitVida"
        Dim oBitVidaPlc As DATO_WORDINT = nDatoWord.GetOne(Me.ID_SECTOR, nombreTag)

        If oBitVidaPlc.VALOR <> BitVidaPLC Then
            BitVidaPLC = oBitVidaPlc.VALOR
            Intentos = 0
            If Not boSeGeneroNotifBitVidaPLC Then
                boSeGeneroNotifBitVidaPLC = True
            End If
        Else ' no cambio el bit vida
            Intentos += 1 ' Sumo un intento
            If Intentos >= 60 Then 'Si llego a tres intento
                'Muestro el mensaje que el PLC esta desconectado
                'modDelegado.setVisibleCtrl(Me, lblPlcStop, True)
                If Not boSeGeneroNotifBitVidaPLC Then
                    boSeGeneroNotifBitVidaPLC = True
                    CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=TipoNotificacion.xErrorDeve, Mensaje:="EL PLC ESTÁ DETENIDO.")
                End If
            End If
        End If

    End Sub

    Private Sub fnIncrementarBItVidaAxControl()
        Dim DATOWORDINTn As New Negocio.DatoWordIntN
        Dim oDatoWordInt As Entidades.DATO_WORDINT = DATOWORDINTn.GetOneTagIdSector("AXCONTROL.BitVida", Me.ID_SECTOR)
        If oDatoWordInt.VALOR > 100 Then
            oDatoWordInt.VALOR = 0
        Else
            oDatoWordInt.VALOR = oDatoWordInt.VALOR + 1
        End If
        oDatoWordInt.FLAG_RW = 1
        DATOWORDINTn.Update(oDatoWordInt)
    End Sub
#End Region

#Region "Eventos"

    Public Sub ClearBufferIngreso() Handles Buffer.ClearBuffer
        Me.CtrlAtenaLeyendo.BorrarUltimosTagLeido()
        CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=TipoNotificacion.InformacionDeve,
                                                     Mensaje:="Se limpio el buffer ")
    End Sub

    Public Sub tiempoBufferIngreso(ByVal Tiempo As Integer) Handles Buffer.ShowTiempoBuffer
        Me.CtrlAtenaLeyendo.setTiempoBuffer(Tiempo)
    End Sub




#End Region

#Region "SUBPROCESOS"

    Public Sub Monitoreo()
        'Apenas comienza el hilo conecto los lectores
        Dim BitVidaPLC As Integer = 0
        Dim IntentosBitVida As Integer = 0
        While True
            Try
                If Me.BALANZA.Inicializado Then
                    BALANZA.Refrescar()
                End If
                If S1_BARRERA.Inicializado Then
                    S1_BARRERA.Refrescar()
                End If
                If S1_POSICION.Inicializado Then
                    S1_POSICION.Refrescar()
                End If
                If S2_BARRERA.Inicializado Then
                    S2_BARRERA.Refrescar()
                End If
                If S2_POSICION.Inicializado Then
                    S2_POSICION.Refrescar()
                End If
                VerEstadoBalanza()
                'VerEstadoBalanzaNuevo()
                If Me.ID_SECTOR = 410 Or Me.ID_SECTOR = 440 Then
                    VerBitVidaPLC(BitVidaPLC:=BitVidaPLC, Intentos:=IntentosBitVida)
                End If
                fnIncrementarBItVidaAxControl()
            Catch ex As Exception
                Dim msj As String = String.Format("{0} - {1}", "[Monitoreo]", ex.Message)
                CtrlPanelNotificaciones1.MostrarNotificacion(ID_CONTROL_ACCESO:=Me.Sector.CONTROL_ACCESO.ID_CONTROL_ACCESO, ID_SECTOR:=Me.Sector.ID_SECTOR, TipoNotificacion:=TipoNotificacion.xErrorDeve, Mensaje:=msj)
            End Try
            Thread.Sleep(Me.Sector.CONTROL_ACCESO.MONITOREO)
        End While

    End Sub

    Private Sub btnLecturaManual_Click(sender As Object, e As EventArgs) Handles btnLecturaManual.Click
        Dim frm As New FrmLecturaManual(ID_SECTOR)
        AddHandler frm.TagLeidoManual, AddressOf TagLeidoManual
        frm.ShowDialog()
        frm.Dispose()
    End Sub

#End Region

End Class
