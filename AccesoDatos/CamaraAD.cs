﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace AccesoDatos
{
    public class CamaraAD
    {
        public CAMARA update_config(CAMARA ocamara)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {

                    var oConsulta = (from tabla in oContext.CAMARA
                                     where tabla.ID_CAMARA == ocamara.ID_CAMARA
                                     select tabla).SingleOrDefault();
                    if (oConsulta != null)
                    {
                        oConsulta.DESCRIPCION = ocamara.DESCRIPCION;
                        oConsulta.IP = ocamara.IP;
                        oConsulta.PUERTO = ocamara.PUERTO;
                        oConsulta.RUTA_FOTO = ocamara.RUTA_FOTO;
                        oConsulta.USUARIO = ocamara.USUARIO;
                        oConsulta.PASSWORD = ocamara.PASSWORD;


                        oContext.SaveChanges();
                        return ocamara;
                    }
                    else
                    {
                        return new CAMARA();
                    }

                }
            }
            catch (Exception ex)
            {
                return new CAMARA();
            }
        }

        public CAMARA getOne(int id)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CAMARA.Where(a => a.ID_CAMARA == id).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new CAMARA();
            }
        }

        public CAMARA getOne(string tag, Int32 idControlAcceso)
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CAMARA.Where(a => a.TAG == tag && a.ID_CONTROL_ACCESO == idControlAcceso ).Single();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new CAMARA();
            }
        }

        public List<CAMARA> getAll()
        {
            try
            {
                using (AxControlORAEntities oContext = new AxControlORAEntities())
                {
                    var oConsulta = oContext.CAMARA.OrderByDescending(a => a.TAG).ToList();

                    return oConsulta;
                }
            }
            catch (Exception ex)
            {
                return new List<CAMARA>();
            }
        }
    }
}
